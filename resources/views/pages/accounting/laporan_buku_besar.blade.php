<link rel="stylesheet" href="{{ asset('/adminlte/plugins/select2/select2.min.css') }}">
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-body">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-md-2">Tahun</label>
                        <div class="col-md-3">
                            <select class="form-control" id="tahun">
                                <?php $tahun = date("Y"); ?>
                                @for($i=$tahun; $i >= 2016; $i--)
                                <option value="{{ $i }}">{{ $i }}</option>
                                @endfor
                            </select>
                        </div>
                        <label class="control-label col-md-1">Bulan</label>
                        <div class="col-md-3">
                            <select class="form-control" id="bulan">
                                <option value="">-- Pilih Bulan --</option>
                                <option value="1">Januari</option>
                                <option value="2">Februari</option>
                                <option value="3">Maret</option>
                                <option value="4">April</option>
                                <option value="5">Mei</option>
                                <option value="6">Juni</option>
                                <option value="7">Juli</option>
                                <option value="8">Agustus</option>
                                <option value="9">September</option>
                                <option value="10">Oktober</option>
                                <option value="11">November</option>
                                <option value="12">Desember</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2">Akun</label>
                        <div class="col-md-3">
                            <select class="form-control select2" data-placeholder="Pilih akun" style="width:100%" id="akun">
                                <option value=""></option>
                                <?php for($j = 0; $j < count($kode_akun); $j++) { ?>
                                <option value="{{ $kode_akun[$j]['id'] }}">{{ $kode_akun[$j]['kode'] }} - {{ $kode_akun[$j]['nama'] }}</option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-2 col-md-offset-2">
                            <button type="button" class="btn btn-primary flat" onclick="btn_lanjut()">
                                <span class="fa fa-arrow-right"></span> Lanjut
                            </button>
                        </div>
                    </div>
                </form>
                <div id="result">
                    
                </div>
            </div>
            <div class="overlay" id="loading" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>
@stop
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/select2/select2.full.min.js') }}"></script>
<script>
    $(function () {
        $(".select2").select2();
    });

    function btn_lanjut()
    {
        var tahun   = document.getElementById('tahun').value;
        var bulan   = document.getElementById('bulan').value;
        var akun    = document.getElementById('akun').value;

        if(bulan == ""){
            alert('pilih bulan terlebih dahulu');
            return;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        
        $.ajax({
            type: "POST",
            url: "{{ url('/ajax_laporan_buku_besar') }}",
            data: { tahun:tahun, bulan:bulan, akun:akun },
            beforeSend: function(){
                    document.getElementById('loading').style.display = "block";
                  },
            success: function (data) {
                $("#result").html(data);
                document.getElementById('loading').style.display = "none";
            },
            error: function (data) {
                alert('Terjadi kesalahan');
                document.getElementById('loading').style.display = "none";
            }
        });
    }
</script>
