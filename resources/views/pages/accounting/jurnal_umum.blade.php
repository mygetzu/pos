<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <a href="{{ url('/tambah_jurnal_umum') }}">
                    <button class='btn bg-purple flat'><span class='fa fa-plus'></span> Tambah Jurnal Umum</button>
                </a>
            </div>
            <div class="box-body">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <table id="mydatatables" class="table table-striped table-bordered" style="margin:0px">
                    <thead>
                        <th style="text-align:center">NO</th>
                        <th style="text-align:center">TANGGAL</th>
                        <th style="text-align:center">DAPAT DIEDIT</th>
                        <th style="text-align:center"></th>
                    </thead>
                    <tbody>
                        <?php $i=1; $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'); ?>
                        @foreach($jurnal_umum_header as $val)
                        <tr>
                            <td align="center">{{ $i++ }}</td>
                            <td><?php
                                    date_default_timezone_set("Asia/Jakarta");
                                    $tanggal = new DateTime($val->tanggal);
                                ?>
                                <?php echo $tanggal->format('d')." ".$bulan[(int)$tanggal->format('m')]." ".$tanggal->format('Y'); ?>
                            </td>
                            <td align="center">
                                @if($val->is_editable == 1) <span class="label label-success">YA</span>
                                @else <span class="label label-success">TIDAK</span>
                                @endif
                            </td>
                            <td align="center">
                                <a class="btn btn-info flat" href="{{ url('/detail_jurnal_umum/'.$val->id) }}" data-toggle="tooltip" title="detail" style="font-size: 15pt"><span class="fa fa-info-circle"></span></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script>
  $(function () {
    $("#mydatatables").DataTable();
  });
</script>
