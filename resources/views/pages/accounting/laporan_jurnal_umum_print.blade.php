<?php
    function rupiah($nominal)
    {
        $rupiah =  number_format($nominal,0, ",",".");
        $rupiah = "Rp "  . $rupiah;
        return $rupiah;
    }

    $arr_bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');

    $tanggal_awal   = new DateTime($first_day);
    $tanggal_akhir  = new DateTime($last_day);
?>
<hr>
<h3 align="center">Laporan Jurnal Umum</h3>
<h4 align="center"><?php echo $tanggal_awal->format('d')." ".$arr_bulan[(int)$tanggal_awal->format('m')]." ".$tanggal_awal->format('Y'); ?> - <?php echo $tanggal_akhir->format('d')." ".$arr_bulan[(int)$tanggal_akhir->format('m')]." ".$tanggal_akhir->format('Y'); ?></h4>
<hr>
<table class="table">
    <thead>
        <tr>
            <th style="text-align: center;">TANGGAL</th>
            <th style="text-align: center;">TRANSAKSI</th>
            <th style="text-align: center;">NO NOTA</th>
            <th style="text-align: center;">DEBIT</th>
            <th style="text-align: center;">KREDIT</th>
        </tr>
    </thead>
    <tbody>
        @foreach($jurnal_umum_detail as $key => $value)
        <tr>
            <td style="text-align:center">
                <?php 
                    $tanggal = new DateTime($value->tanggal);
                    echo $tanggal->format('d')." ".$arr_bulan[(int)$tanggal->format('m')]." ".$tanggal->format('Y'); 
                ?>
            </td>
            <td>{{ $value->master_coa->nama }}</td>
            <td>{{ $value->referensi }}</td>
            <td>{{ rupiah($value->debit) }}</td>
            <td>{{ rupiah($value->kredit) }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
<link rel="stylesheet" type="text/css" href="{{ URL::to('/css/nota.css') }}">
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function(){
          window.onload = function() { window.print(); }
      });
</script>