<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('/adminlte/plugins/select2/select2.min.css') }}">
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <a href="{{ url('/jurnal_umum') }}">
                    <button class='btn btn-default flat pull-right'><span class='fa fa-arrow-left'></span> Kembali</button>
                </a>
            </div>
            <div class="box-body">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <div class="form-horizontal">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <th style="text-align:center">NO</th>
                            <th style="text-align:center">NO JURNAL</th>
                            <th style="text-align:center">TANGGAL</th>
                            <th style="text-align:center">NO REFERENSI</th>
                            <th style="text-align:center">AKUN</th>
                            <th style="text-align:center">DEBIT</th>
                            <th style="text-align:center">KREDIT</th>
                            <th style="text-align:center"></th>
                        </thead>
                        <tbody>
                            <?php $i=1; $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'); ?>
                            @foreach($jurnal_umum_detail as $value)
                            <tr>
                                <td align="center">{{ $i++ }}</td>
                                <td>{{ $value->no_jurnal }}</td>
                                <td><?php
                                        date_default_timezone_set("Asia/Jakarta");
                                        $tanggal = new DateTime($value->tanggal);
                                        $tanggal = $tanggal->format('d')." ".$bulan[(int)$tanggal->format('m')]." ".$tanggal->format('Y');
                                    ?>
                                    <?php echo $tanggal; ?>
                                </td>
                                <td>{{ $value->referensi }}</td>
                                <td>{{ $value->akun_kode }} - {{ $value->akun_nama }}</td>
                                <td>
                                    {{ rupiah($value->debit) }}
                                </td>
                                <td>
                                    {{ rupiah($value->kredit) }}
                                </td>
                                <td align="center">
                                    <a data-toggle="modal" data-target="#ubah_jurnal" onclick="ubah_jurnal('{{ $value->id }}', '{{ $value->no_jurnal }}', '{{ $tanggal }}', '{{ $value->referensi }}', '{{ $value->akun_kode }}', '{{ $value->debit }}', '{{ $value->kredit }}')">
                                        <button class="btn btn-warning flat" data-toggle="tooltip" title="ubah" style="font-size: 15pt"><span class="fa fa-edit"></span>
                                        </button>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <button class="btn btn-primary flat pull-right" onclick="document.getElementById('loading').style.display = 'block'" style="display: none">
                        <span class="glyphicon glyphicon-floppy-disk"></span> Simpan
                    </button>
                </div>
            </div>
             <div class="overlay" id="loading" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ubah_jurnal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content box">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ubah Jurnal</h4>
            </div>
            <form class="form-horizontal" method="post" action="{{ url('/ubah_jurnal_umum_detail') }}" autocomplete="off">
            <div class="modal-body">
                {{ csrf_field() }}
                <input type="hidden" name="id" id="id">
                <input type="hidden" name="nominal" id="nominal">
                <div class="form-group">
                    <label class="col-sm-2 control-label">No Jurnal</label>
                    <div class="col-sm-10">
                        <input type="text" name="no_jurnal" id="no_jurnal" class="form-control" disabled="true">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">No Referensi</label>
                    <div class="col-sm-10">
                        <input type="text" name="no_referensi" id="no_referensi" disabled="true" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Tanggal</label>
                    <div class="col-sm-10">
                        <input type="text" name="tanggal" id="tanggal" disabled="true" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Akun</label>
                    <div class="col-sm-10">
                        <select class="form-control select2" data-placeholder="Pilih akun" style="width:100%" name="akun_kode" id="akun_kode">
                            <option value=""></option>
                            <?php for($j = 0; $j < count($kode_akun); $j++) { ?>
                            <option value="{{ $kode_akun[$j]['kode'] }}">{{ $kode_akun[$j]['kode'] }} - {{ $kode_akun[$j]['nama'] }}</option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nominal</label>
                    <div class="col-sm-10">
                        <input type="text" id="display_nominal" disabled="true" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Debit/Kredit</label>
                    <div class="col-sm-10">
                        <select class="form-control" id="d_or_k" name="d_or_k">
                            <option value="D">Debit</option>
                            <option value="K">Kredit</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default flat pull-left"><span class="glyphicon glyphicon-ban-circle"></span> Batal</button>
                <button type="submit" class="btn btn-primary flat" onclick="document.getElementById('loading2').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
            </div>
            </form>
            <div class="overlay" id="loading2" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>
@stop
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/select2/select2.full.min.js') }}"></script>
<script>
    $(function () {
        $("#mydatatables").DataTable();
        $(".select2").select2();
    });

    function ubah_jurnal(id, no_jurnal, tanggal, referensi, akun_kode, debit, kredit)
    {
        document.getElementById('id').value             = id;
        document.getElementById('no_jurnal').value      = no_jurnal;
        document.getElementById('tanggal').value        = tanggal;
        document.getElementById('no_referensi').value   = referensi;

        if(debit > kredit){
            document.getElementById('nominal').value            = debit;
            document.getElementById('display_nominal').value    = rupiah(debit);
            document.getElementById('d_or_k').value             = 'D';
        }
        else{
            document.getElementById('nominal').value            = kredit;
            document.getElementById('display_nominal').value    = rupiah(kredit);
            document.getElementById('d_or_k').value             = 'K';
        }        

        $(".select2").select2().val(akun_kode).trigger("change");
    }

    function rupiah(nominal)
    {
        //rumus konversi rupiah
        nominal = "Rp "+nominal.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
        return nominal;
    }
</script>
