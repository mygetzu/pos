<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <a href="{{ url('/jurnal_umum') }}">
                    <button class='btn btn-default flat pull-right'><span class='fa fa-arrow-left'></span> Kembali</button>
                </a>
            </div>
            <div class="box-body">
                <form class="form-horizontal" method="POST" action="{{ url('/do_tambah_jurnal_umum') }}" autocomplete="off">
                    {{ csrf_field() }}
                    <input type="hidden" name="total_data" value="{{ count($data) }}">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <th style="text-align:center">NO</th>
                            <!-- <th style="text-align:center">NO JURNAL</th> -->
                            <th style="text-align:center">TANGGAL</th>
                            <th style="text-align:center">NAMA TRANSAKSI</th>
                            <th style="text-align:center" width="13%">NO REFERENSI</th>
                            <th style="text-align:center">AKUN</th>
                            <th style="text-align:center" width="13%">DEBIT</th>
                            <th style="text-align:center" width="13%">KREDIT</th>
                            <th style="text-align:center">TAMBAHKAN</th>
                        </thead>
                        <tbody>
                            <?php $i=1; $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'); ?>
                            @foreach($data as $val)
                            <tr>
                                <td align="center">{{ $i++ }}</td>
                                <!-- <td align="center">{{ $val['no_jurnal'] }}</td> -->
                                <td><?php
                                        date_default_timezone_set("Asia/Jakarta");
                                        $tanggal = new DateTime($val['tanggal']);
                                    ?>
                                    <?php echo $tanggal->format('d')." ".$bulan[(int)$tanggal->format('m')]." ".$tanggal->format('Y'); ?>
                                </td>
                                <td>{{ ucwords($val['transaksi']) }}</td>
                                <td>{{ $val['referensi'] }}</td>
                                <td>{{ $val['akun_kode'] }} - {{ $val['akun_nama'] }}</td>
                                <td>
                                    {{ rupiah($val['debit']) }}
                                </td>
                                <td>
                                    {{ rupiah($val['kredit']) }}
                                </td>
                                <td align="center">
                                    <input type="hidden" name="no_jurnal{{ $i-1 }}" value="{{ $val['no_jurnal'] }}">
                                    <input type="hidden" name="tanggal{{ $i-1 }}" value="{{ $val['tanggal'] }}">
                                    <input type="hidden" name="referensi{{ $i-1 }}" value="{{ $val['referensi'] }}">
                                    <input type="hidden" name="akun_kode{{ $i-1 }}" value="{{ $val['akun_kode'] }}">
                                    <input type="hidden" name="akun_nama{{ $i-1 }}" value="{{ $val['akun_nama'] }}">
                                    <input type="hidden" name="debit{{ $i-1 }}" value="{{ $val['debit'] }}">
                                    <input type="hidden" name="kredit{{ $i-1 }}" value="{{ $val['kredit'] }}">
                                    <input type="checkbox" name="cek{{ $i-1 }}">
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <button class="btn btn-primary flat pull-right" onclick="document.getElementById('loading').style.display = 'block'">
                        <span class="glyphicon glyphicon-floppy-disk"></span> Simpan
                    </button>
                </form>
            </div>
             <div class="overlay" id="loading" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>
@stop
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script>
  $(function () {
    $("#mydatatables").DataTable();
  });
</script>
