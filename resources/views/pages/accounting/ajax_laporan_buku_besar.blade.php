<?php
    function rupiah($nominal)
    {
        $rupiah =  number_format($nominal,0, ",",".");
        $rupiah = "Rp "  . $rupiah;
        return $rupiah;
    }

    $arr_bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');

    $tanggal_awal   = new DateTime($first_day);
    $tanggal_akhir  = new DateTime($last_day);
?>
<hr>
<a href="{{ url('/laporan_buku_besar_print/'.$bulan.'/'.$tahun.'/'.$akun) }}" target="_blank" class="btn bg-orange flat"><i class="fa fa-print"></i> Cetak</a>
<a href="{{ url('/laporan_buku_besar_pdf/'.$bulan.'/'.$tahun.'/'.$akun) }}" target="_blank" class="btn btn-info flat" style="margin-left: 5px;"><i class="fa fa-download"></i> Unduh PDF</a>
<h3 align="center">Laporan Buku Besar - {{ $akun_nama }}</h3>
<h4 align="center"><?php echo $tanggal_awal->format('d')." ".$arr_bulan[(int)$tanggal_awal->format('m')]." ".$tanggal_awal->format('Y'); ?> - <?php echo $tanggal_akhir->format('d')." ".$arr_bulan[(int)$tanggal_akhir->format('m')]." ".$tanggal_akhir->format('Y'); ?></h4>
<hr>
<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th style="text-align: center;">TANGGAL</th>
            <th style="text-align: center;">TRANSAKSI</th>
            <th style="text-align: center;">NO NOTA</th>
            <th style="text-align: center;">DEBIT</th>
            <th style="text-align: center;">KREDIT</th>
        </tr>
    </thead>
    <tbody>
        @foreach($jurnal_umum_detail as $key => $value)
        <tr>
            <td align="center">
                <?php 
                    $tanggal = new DateTime($value->tanggal);
                    echo $tanggal->format('d')." ".$arr_bulan[(int)$tanggal->format('m')]." ".$tanggal->format('Y'); 
                ?>
            </td>
            <td>{{ $value->master_coa->nama }}</td>
            <td>{{ $value->referensi }}</td>
            <td>{{ rupiah($value->debit) }}</td>
            <td>{{ rupiah($value->kredit) }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script>
    $(function () {
        $("#mydatatables").DataTable();
    });
</script>