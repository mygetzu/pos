@extends('layouts.master_home')
@section('content')
@include('includes.base_function')
<div class="products-grids">
    <div class="col-md-8 products-grid-left">
        <div class="col-md-12">
            <form class="form-horizontal">
                <div class="form-group">
                    <label class="control-label col-md-8">Dipilih Berdasarkan</label>
                    <div class="col-md-4">
                        <select class="form-control pull-right" id="select_sorting">
                            <option value="1" <?php if(!empty($sorting)){ if($sorting == 1) echo "selected"; } ?>>Nama A-Z</option>
                            <option value="2" <?php if(!empty($sorting)){ if($sorting == 2) echo "selected"; } ?>>Nama Z-A</option>
                            <option value="3" <?php if(!empty($sorting)){ if($sorting == 3) echo "selected"; } ?>>Harga Terendah-Tertinggi</option>
                            <option value="4" <?php if(!empty($sorting)){ if($sorting == 4) echo "selected"; } ?>>Harga Tertinggi-Terendah</option>
                        </select>
                    </div>
                </div>
            </form>
        </div>
        @foreach($paket  as $val)
        <div class="col-sm-4" style="margin-bottom: 20px;" align="center">
            <div class="p-one simpleCart_shelfItem prd" style="height: 300px;">
                <a href="single.html">
                    <img src="{{ URL::asset('img/produk/'.$val['file_gambar']) }}" style="height:160px;object-fit: contain; padding: 20px" />
                </a>
                <h5><a href="{{ url('/paket_produk/'.$val['slug_nama']) }}">{{ $val['nama'] }}</a></h5>
                <div class="pro-grd">
                    @if((int)$val['stok_tersedia'] <= 0)
                    <a href="{{ url('/paket_produk/'.$val['slug_nama']) }}" onclick="return confirm('Stok barang saat ini tidak tersedia, apakah anda ingin melihat detail produk?')" class="btn btn-default flat"><span class="fa fa-shopping-cart"></span> Beli</a>
                    @else
                    <a href="{{ url('/paket_produk/'.$val['slug_nama']) }}" class="btn btn-success flat"><span class="fa fa-shopping-cart"></span> Beli</a>
                    @endif
                </div>
            </div>
        </div>
        @endforeach
        <div class="col-md-12">
            @if($paket->count() != 0)
            <div class="col-md-6">
                {{ $paket->links() }}
            </div>
            <div class="col-md-6" align="right" style="padding-top:20px">
               <p>menampilkan {{ $paket->count() }} dari {{ $paket->total() }} data</p>
            </div>
            @else
            <div class="col-md-12">
            <p align="left">Paket produk tidak ditemukan</p>
            </div>
            @endif
        </div>
    </div>
    <div class="col-md-4 products-grid-right">
        <div class="w_sidebar">
            <section  class="sky-form">
                <h4>Range Harga</h4>
                <div class="row1" align="center">
                    <form method="get" id="form_submit">
                        <input type="hidden" name="search" value="@if(!empty($search)){{ $search }}@endif">
                        <input id="range_1" type="text" name="range_harga" data-min="10000" data-max="30000000" 
                        data-from="{{ $range_min }}" data-to="{{ $range_max }}"><br>
                        <button class="btn btn-success flat"><span class="fa fa-arrow-right"></span> Submit Range Harga</button>
                        <input id="sorting" type="hidden" name="sorting" value='<?php if(!empty($sorting)) echo $sorting; else echo "1"; ?>'>
                    </form>
                </div>
            </section>
            <!-- <section  class="sky-form">
                <h4>Kategori</h4>
                <div class="row1 scroll-pane">
                    <div class="col col-4">
                        <form method="get" id="form_submit2">
                        foreach($cek_kategori as $val)
                            <label class="checkbox"><input type="checkbox" name="check_kategori{{-- $val['id'] --}}" onclick="check_klik()" <?php //if($val['checked'] == 1) echo "checked"; ?>><i></i>{{ $val['nama'] }}</label>
                        endforeach
                        </form>
                    </div>
                </div>
            </section> -->
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<script type="text/javascript">
    function check_klik()
    {
        document.getElementById('form_submit2').submit();
    }
</script>
@stop