@extends('layouts.master_home')
@section('content')
@include('includes.base_function')
@for($i=0; $i < count($paket_pilihan); $i++)
    <div style="margin-top: 20px;">
        <br><h3 style="color: #20BDD4"><b>{{ $paket_pilihan[$i]['nama'] }}</b></h3><br>
        <div class="container-fluid" style="background-color: white; border-top: 5px solid #20BDD4;padding: 0px;">
            <div class="col-md-3 banner-panel">
                <a href="{{ url('/paket-pilihan/'.$paket_pilihan[$i]['url']) }}">
                    <img src="{{ URL::asset('img/banner/'.$paket_pilihan[$i]['file_gambar']) }}" style="width: 100%; height: 320px;">
                </a>
            </div>
            <div class="col-md-9" style="padding: 0px;">
                <div class="span12 owl-produk">
                    <div class="owl-carousel owl-paket-pilihan">
                        <?php $data = $paket_pilihan[$i]['list_paket']; ?>
                        @for($j = 0; $j < count($data); $j++)
                            <div class="item" style="height: 320px; padding: 10px; border-right: 1px solid #9c9c9c; <?php if($j == 0) echo 'border-left: 1px solid #9c9c9c'; ?>" align="center">
                                <img src="{{ URL::asset('img/produk/'.$data[$j]['file_gambar']) }}" class="img-responsive" alt="a" style="height:160px;object-fit: contain; padding: 20px" />
                                <h5><a href="{{ url('/produk/'.$data[$j]['slug_nama']) }}">{{ $data[$j]['nama'] }}</a></h5>
                                <h4>{{ rupiah($data[$j]['harga']) }}</h4>
                                <div style="width: 90%;bottom:15px; position: absolute;" align="center">
                                    @if((int)$data[$j]['stok_tersedia'] <= 0)
                                    <a href="{{ url('/paket/'.$data[$j]['slug_nama']) }}" onclick="return confirm('Stok barang saat ini tidak tersedia, apakah anda ingin melihat detail paket?')" class="btn btn-default flat"><span class="fa fa-shopping-cart"></span> Beli</a>
                                    @else
                                    <a href="{{ url('/paket/'.$data[$j]['slug_nama']) }}" class="btn btn-success flat"><span class="fa fa-shopping-cart"></span> Beli</a>
                                    @endif
                                </div> 
                            </div>
                        @endfor
                    </div>
                </div>
            </div>
        </div>
    </div>
@endfor
@stop