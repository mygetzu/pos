<div style="margin-top: 20px;">
    <br><h3 style="color: #20BDD4"><b>Produk Baru</b></h3><br>
    <div class="container-fluid" style="background-color: white; border-top: 5px solid #20BDD4;padding: 0px;">
        <div class="col-md-12" style="padding: 0px;">
            <div class="span12 owl-produk">
                <div id="owl-produk2" class="owl-carousel">
                    @for($i=0; $i < count($produk_baru); $i++)
                    <div class="item" style="height: 320px; padding: 10px; border-right: 1px solid #9c9c9c; <?php if($i == 0) echo 'border-left: 1px solid #9c9c9c;'; ?>" align="center">
                        @if((int)$produk_baru[$i]['diskon'] != 0)
                            <div class="discount-label red"> <span>diskon {{ $produk_baru[$i]['diskon'] }}% @if($produk_baru[$i]['qty_beli_diskon'] > 1) untuk {{ $produk_baru[$i]['qty_beli_diskon'] }} pembelian @endif</span> </div>
                        @elseif((int)$produk_baru[$i]['cashback'] != 0)
                            <div class="discount-label blue"><span>cashback {{ rupiah($produk_baru[$i]['cashback']) }} @if($produk_baru[$i]['qty_beli_cashback'] > 1) untuk {{ $produk_baru[$i]['qty_beli_cashback'] }} pembelian @endif</span></div>
                        @elseif(!empty($produk_baru[$i]['hadiah']))
                            <div class="discount-label yellow"><span>berhadiah @if($produk_baru[$i]['qty_hadiah'] > 1) {{ $produk_baru[$i]['qty_hadiah'] }} unit @endif {{ $produk_baru[$i]['hadiah'] }} @if($produk_baru[$i]['qty_beli_hadiah'] > 1) untuk {{ $produk_baru[$i]['qty_beli_hadiah'] }} pembelian @endif</span></div>
                        @endif
                        <img src="{{ URL::asset('img/produk/'.$produk_baru[$i]['file_gambar']) }}" class="img-responsive" alt="a" style="height:160px;object-fit: contain; padding: 20px" />
                        <h5><a href="{{ url('/produk/'.$produk_baru[$i]['slug_nama']) }}">{{ $produk_baru[$i]['nama'] }}</a></h5>
                        @if((int)$produk_baru[$i]['diskon'] != 0 AND (int)$produk_baru[$i]['qty_beli_diskon'] === 1)
                            <h4 style="margin-bottom: 0px;">{{ rupiah($produk_baru[$i]['harga_retail']-($produk_baru[$i]['harga_retail']*(int)$produk_baru[$i]['diskon']/100)) }}</h4>
                            <p><del>{{ rupiah($produk_baru[$i]['harga_retail']) }}</del></p>
                        @elseif((int)$produk_baru[$i]['cashback'] != 0 AND (int)$produk_baru[$i]['qty_beli_cashback'] === 1)
                            <h4 style="margin-bottom: 0px;">{{ rupiah($produk_baru[$i]['harga_retail']-(int)$produk_baru[$i]['cashback']) }}</h4>
                            <p><del>{{ rupiah($produk_baru[$i]['harga_retail']) }}</del></p>
                        @else
                            <h4>{{ rupiah($produk_baru[$i]['harga_retail']) }}</h4>
                        @endif
                        <div style="width: 90%;bottom:15px; position: absolute;" align="center">
                            @if((int)$produk_baru[$i]['stok_tersedia'] <= 0)
                            <a href="{{ url('/produk/'.$produk_baru[$i]['slug_nama']) }}" onclick="return confirm('Stok barang saat ini tidak tersedia, apakah anda ingin melihat detail produk?')" class="btn btn-default flat"><span class="fa fa-shopping-cart"></span> Beli</a>
                            @else
                            <a href="{{ url('/produk/'.$produk_baru[$i]['slug_nama']) }}" class="btn btn-success flat"><span class="fa fa-shopping-cart"></span> Beli</a>
                            @endif
                        </div> 
                    </div>
                    @endfor
                </div>
            </div>
        </div>
    </div>
</div>