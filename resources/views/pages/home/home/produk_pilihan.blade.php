@for($i=0; $i < count($produk_pilihan); $i++)
    <div style="margin-top: 20px;">
        <br><h3 style="color: #20BDD4"><b>{{ $produk_pilihan[$i]['nama'] }}</b></h3><br>
        <div class="container-fluid" style="background-color: white; border-top: 5px solid #20BDD4;padding: 0px;">
            <div class="col-md-3 banner-panel">
                <a href="{{ url('/produk-pilihan/'.$produk_pilihan[$i]['url']) }}">
                    <img src="{{ URL::asset('img/banner/'.$produk_pilihan[$i]['file_gambar']) }}" style="width: 100%; height: 320px;">
                </a>
            </div>
            <div class="col-md-9" style="padding: 0px;">
                <div class="span12 owl-produk">
                    <div class="owl-carousel owl-produk-pilihan">
                        <?php $data = $produk_pilihan[$i]['list_produk']; ?>
                        @for($j = 0; $j < count($data); $j++)
                            <div class="item" style="height: 320px; padding: 10px; border-right: 1px solid #9c9c9c; <?php if($j == 0) echo 'border-left: 1px solid #9c9c9c'; ?>" align="center">
                                @if((int)$data[$j]['diskon'] != 0)
                                    <div class="discount-label red"> <span>diskon {{ $data[$j]['diskon'] }}% @if($data[$j]['qty_beli_diskon'] > 1) untuk {{ $data[$j]['qty_beli_diskon'] }} pembelian @endif</span> </div>
                                @elseif((int)$data[$j]['cashback'] != 0)
                                    <div class="discount-label blue"><span>cashback {{ rupiah($data[$j]['cashback']) }} @if($data[$j]['qty_beli_cashback'] > 1) untuk {{ $data[$j]['qty_beli_cashback'] }} pembelian @endif</span></div>
                                @elseif(!empty($data[$j]['hadiah']))
                                    <div class="discount-label yellow"><span>berhadiah @if($data[$j]['qty_hadiah'] > 1) {{ $data[$j]['qty_hadiah'] }} unit @endif {{ $data[$j]['hadiah'] }} @if($data[$j]['qty_beli_hadiah'] > 1) untuk {{ $data[$j]['qty_beli_hadiah'] }} pembelian @endif</span></div>
                                @endif
                                <img src="{{ URL::asset('img/produk/'.$data[$j]['file_gambar']) }}" class="img-responsive" alt="a" style="height:160px;object-fit: contain; padding: 20px" />
                                <h5><a href="{{ url('/produk/'.$data[$j]['slug_nama']) }}">{{ $data[$j]['nama'] }}</a></h5>
                                @if((int)$data[$j]['diskon'] != 0 AND (int)$data[$j]['qty_beli_diskon'] === 1)
                                    <h4 style="margin-bottom: 0px;">{{ rupiah($data[$j]['harga_retail']-($data[$j]['harga_retail']*(int)$data[$j]['diskon']/100)) }}</h4>
                                    <p><del>{{ rupiah($data[$j]['harga_retail']) }}</del></p>
                                @elseif((int)$data[$j]['cashback'] != 0 AND (int)$data[$j]['qty_beli_cashback'] === 1)
                                    <h4 style="margin-bottom: 0px;">{{ rupiah($data[$j]['harga_retail']-(int)$data[$j]['cashback']) }}</h4>
                                    <p><del>{{ rupiah($data[$j]['harga_retail']) }}</del></p>
                                @else
                                    <h4>{{ rupiah($data[$j]['harga_retail']) }}</h4>
                                @endif
                                <div style="width: 90%;bottom:15px; position: absolute;" align="center">
                                    @if((int)$data[$j]['stok_tersedia'] <= 0)
                                    <a href="{{ url('/produk/'.$data[$j]['slug_nama']) }}" onclick="return confirm('Stok barang saat ini tidak tersedia, apakah anda ingin melihat detail produk?')" class="btn btn-default flat"><span class="fa fa-shopping-cart"></span> Beli</a>
                                    @else
                                    <a href="{{ url('/produk/'.$data[$j]['slug_nama']) }}" class="btn btn-success flat"><span class="fa fa-shopping-cart"></span> Beli</a>
                                    @endif
                                </div> 
                            </div>
                        @endfor
                    </div>
                </div>
            </div>
        </div>
    </div>
@endfor