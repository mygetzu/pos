<div style="margin-top: 20px;">
    <br><h3 style="color: #20BDD4"><b>Produk Akan Datang</b></h3><br>
    <div class="container-fluid" style="background-color: white; border-top: 5px solid #20BDD4;padding: 0px;">
        <div class="col-md-12" style="padding: 0px;">
            <div class="span12 owl-produk">
                <div id="owl-produk3" class="owl-carousel">
                    @for($i=0; $i < count($produk_akan_datang); $i++)
                    <div class="item" style="height: 320px; padding: 10px; border-right: 1px solid #9c9c9c; <?php echo ($i == 0 ? 'border-left: 1px solid #9c9c9c' : '' ); ?>" align="center">
                        @if((int)$produk_akan_datang[$i]['diskon'] != 0)
                            <div class="discount-label red"> <span>diskon {{ $produk_akan_datang[$i]['diskon'] }}% @if($produk_akan_datang[$i]['qty_beli_diskon'] > 1) untuk {{ $produk_akan_datang[$i]['qty_beli_diskon'] }} pembelian @endif</span> </div>
                        @elseif((int)$produk_akan_datang[$i]['cashback'] != 0)
                            <div class="discount-label blue"><span>cashback {{ rupiah($produk_akan_datang[$i]['cashback']) }} @if($produk_akan_datang[$i]['qty_beli_cashback'] > 1) untuk {{ $produk_akan_datang[$i]['qty_beli_cashback'] }} pembelian @endif</span></div>
                        @elseif(!empty($produk_akan_datang[$i]['hadiah']))
                            <div class="discount-label yellow"><span>berhadiah @if($produk_akan_datang[$i]['qty_hadiah'] > 1) {{ $produk_akan_datang[$i]['qty_hadiah'] }} unit @endif {{ $produk_akan_datang[$i]['hadiah'] }} @if($produk_akan_datang[$i]['qty_beli_hadiah'] > 1) untuk {{ $produk_akan_datang[$i]['qty_beli_hadiah'] }} pembelian @endif</span></div>
                        @endif
                        <img src="{{ URL::asset('img/produk/'.$produk_akan_datang[$i]['file_gambar']) }}" class="img-responsive" alt="{{ $produk_akan_datang[$i]['nama'] }}" style="height:160px;object-fit: contain; padding: 20px" />
                        <h5><a href="{{ url('/produk/'.$produk_akan_datang[$i]['slug_nama']) }}">{{ $produk_akan_datang[$i]['nama'] }}</a></h5>
                        @if((int)$produk_akan_datang[$i]['diskon'] != 0 AND (int)$produk_akan_datang[$i]['qty_beli_diskon'] === 1)
                            <h4 style="margin-bottom: 0px;">{{ rupiah($produk_akan_datang[$i]['harga_retail']-($produk_akan_datang[$i]['harga_retail']*(int)$produk_akan_datang[$i]['diskon']/100)) }}</h4>
                            <p><del>{{ rupiah($produk_akan_datang[$i]['harga_retail']) }}</del></p>
                        @elseif((int)$produk_akan_datang[$i]['cashback'] != 0 AND (int)$produk_akan_datang[$i]['qty_beli_cashback'] === 1)
                            <h4 style="margin-bottom: 0px;">{{ rupiah($produk_akan_datang[$i]['harga_retail']-(int)$produk_akan_datang[$i]['cashback']) }}</h4>
                            <p><del>{{ rupiah($produk_akan_datang[$i]['harga_retail']) }}</del></p>
                        @else
                            <h4>{{ rupiah($produk_akan_datang[$i]['harga_retail']) }}</h4>
                        @endif
                        <div style="width: 90%;bottom:15px; position: absolute;" align="center">
                            @if((int)$produk_akan_datang[$i]['stok_tersedia'] <= 0)
                            <a href="{{ url('/produk/'.$produk_akan_datang[$i]['slug_nama']) }}" onclick="return confirm('Stok barang saat ini tidak tersedia, apakah anda ingin melihat detail produk?')" class="btn btn-default flat"><span class="fa fa-shopping-cart"></span> Beli</a>
                            @else
                            <a href="{{ url('/produk/'.$produk_akan_datang[$i]['slug_nama']) }}" class="btn btn-success flat"><span class="fa fa-shopping-cart"></span> Beli</a>
                            @endif
                        </div> 
                    </div>
                    @endfor
                </div>
            </div>
        </div>
    </div>
</div>