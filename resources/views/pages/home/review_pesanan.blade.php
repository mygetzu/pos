@extends('layouts.master_home2')
@section('content')
@include('includes.base_function')
<input type="hidden" name="base_path" id="base_path" value="{{ asset('/') }}">
<div class="col-sm-12" align="center" style="margin-bottom:30px">
    <h2 style="border-bottom: 3px solid; display:inline; color:#808080">Review Pesanan</h2>
</div>
<div class="col-sm-12">
@if(Session::has('message'))
<div class="alert alert-danger flat alert-dismissable" style="margin-left: 0px;">
    <i class="fa fa-ban"></i>
    {{ Session::get('message') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<div class="container">
    <table class="table table-striped table-bordered" id="myTable">
      <thead>
        <tr>
          <th width="5%" style="text-align:center">NO</th>
          <th style="text-align:center">NAMA PRODUK</th>                    
          <th style="text-align:center">HARGA</th>
          <th style="text-align:center">JUMLAH</th>
          <th style="text-align:center">TOTAL HARGA</th>
        </tr>
      </thead>
      <tbody>
      <?php $i = 1; $jumlah=0; $total_bayar=0;?>
      @foreach($cart_content as $cart)
        <tr>
          <td align="center">{{ $i++ }}</td>
          <td>
          @if($cart->name === 'voucher')
          <b>{{ $cart->name }}</b>
          @else
          {{ $cart->name }} 
            @if(strpos($cart->id, 'DIS') === 0) 
              <b>(*diskon)</b> 
            @elseif(strpos($cart->id, 'PKT') === 0) 
              <b>(*paket)</b> 
            @endif
          @endif
          </td>
          <td>{{ rupiah($cart->price) }}</td>
          <td align="center">{{ $cart->qty }}</td>
          <td style="padding-left:40px">{{ rupiah($cart->price*$cart->qty) }}</td>
        </tr>
        <?php 
          $jumlah += $cart->qty;
          $total_bayar += ($cart->price*$cart->qty);
        ?>
      @endforeach
        <tr>
          <td colspan="4" align="center"><b>Total Bayar</b></td>
          <td style="padding-left:40px"><label class="control-label" id="my_total_bayar">{{ rupiah($total_bayar) }}</label></td>
        </tr>
      </tbody>
      </table>
      <input type="hidden" name="jumlah" id="jumlah" value="{{ $jumlah }}">
      <div class="row">
          <div class="col-md-6">
              <form class="form-horizontal" method="post" autocomplete="off" action="{{ url('/cek_voucher') }}">
                  {{ csrf_field() }}
                  <input type="hidden" name="total_bayar" value="{{ $total_bayar }}">
                  <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">Kode Voucher :</label>
                    </div>
                    <div class="col-md-7">
                      <input type="" name="kode_voucher" id="kode_voucher" class="form-control">
                    </div>
                    <button class="btn btn-default" type="submit"><span class="fa fa-check"></span> Pilih</button>
                  </div>
              </form>
          </div>
          <div class="col-md-6">
              <form method="post" action="{{ url('/checkout_home') }}" class="form-horizontal">
                  {{csrf_field()}}
                  <input type="hidden" name="id_voucher" id="id_voucher" value="">
                  <input type="hidden" name="kode_unik" id="kode_unik" value="">
                  <input type="hidden" name="total_bayar" id="total_bayar" value="{{ $total_bayar }}">
                  <input type="hidden" name="nominal_voucher" id="nominal_voucher" value="0">
                  <div class="row">
                      <div class="col-md-2">
                          <label class="control-label">Catatan :</label>
                      </div>
                      <div class="col-md-10">
                        <textarea class="form-control" name="catatan" style="height:50px"></textarea>
                      </div>
                  </div>
                  <br>
                  <button class="btn btn-success pull-right flat"><span class="glyphicon glyphicon-check"></span> Konfirmasi</button>
              </form>
          </div>
    </div>   
</div>
</div>
<br><br>
@stop

<script type="text/javascript">
  function cek_voucher()
  {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })

        var url = "{{ url('/cek_voucher') }}";
        
        $.ajax({
            type: "POST",
            url: url,
            data: $("#form_cek_voucher").serialize(),
            dataType: 'json',
            success: function (data) {
              alert(data);
              // var table = document.getElementById("myTable");
              // var jumlah = document.getElementById("jumlah").value;
              // var row = table.insertRow(parseInt(jumlah));
              // var cell1 = row.insertCell(0);
              // var cell2 = row.insertCell(1);
              // var cell3 = row.insertCell(2);
              // var cell4 = row.insertCell(3);
              // var cell5 = row.insertCell(4);

              // cell2.innerHTML = "<b>VOUCHER</b>";
              // cell5.innerHTML = "<b>"+rupiah(data.nominal)+"</b>";
              // cell5.style.paddingLeft  = '40px';
              // document.getElementById("form_cek_voucher").style.display = 'none';

              // var total = document.getElementById("total_bayar").value;
              // var total_bayar = parseInt(total)-parseInt(data.nominal);
              // document.getElementById("total_bayar").value = total_bayar;
              // document.getElementById("id_voucher").value = data.id;
              // document.getElementById("kode_unik").value = data.kode_unik;
              // document.getElementById("nominal_voucher").value = data.nominal;
              // document.getElementById("my_total_bayar").innerHTML = "<label class='control-label'>"+rupiah(total_bayar)+"</label>";
            },
            error: function (data) {
                alert('gagal cek kode voucher');
            }
        });
  }
  function rupiah(nominal)
    {
        //rumus konversi rupiah
        nominal = "Rp "+nominal.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
        return nominal;
    }
</script>