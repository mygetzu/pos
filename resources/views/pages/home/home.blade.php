<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="">
    <meta name="author" content="">
    <title>POS</title>
    <link rel="stylesheet" href="{{ URL::to('bootstrap/css/bootstrap.min.css') }}">
    <meta name="keywords" content="Nuevo Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
    Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
    <script type="applijegleryion/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <link href="{{ URL::to('nuevo/css/bootstrap.css') }}" rel='stylesheet' type='text/css' />
    <!-- Custom Theme files -->
    <link href="{{ URL::to('nuevo/css/style.css') }}" rel='stylesheet' type='text/css' />  
    <script src="{{ URL::to('nuevo/js/jquery-1.11.1.min.js') }}"></script>
    <!-- start menu -->
    <link href="{{ URL::to('nuevo/css/megamenu.css') }}" rel="stylesheet" type="text/css" media="all" />
    <script type="text/javascript" src="{{ URL::to('nuevo/js/megamenu.js') }}"></script>
    <script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
    <script src="{{ URL::to('nuevo/js/menu_jquery.js') }}"></script>
    <script src="{{ URL::to('nuevo/js/simpleCart.min.js') }}"> </script>
    <!--web-fonts-->
     <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,300italic,600,700' rel='stylesheet' type='text/css'>
     <link href='//fonts.googleapis.com/css?family=Roboto+Slab:300,400,700' rel='stylesheet' type='text/css'>
    <!--//web-fonts-->
     <script src="{{ URL::to('nuevo/js/scripts.js') }}" type="text/javascript"></script>
    <script src="{{ URL::to('nuevo/js/modernizr.custom.js') }}"></script>
    <script type="text/javascript" src="{{ URL::to('nuevo/js/move-top.js') }}"></script>
    <script type="text/javascript" src="{{ URL::to('nuevo/js/easing.js') }}"></script>
    <!--/script-->
    <script type="text/javascript">
                jQuery(document).ready(function($) {
                    $(".scroll").click(function(event){     
                        event.preventDefault();
                        $('html,body').animate({scrollTop:$(this.hash).offset().top},900);
                    });
                });
    </script>
    <!-- owl-carousel2 -->
    <link rel="stylesheet" href="{{ URL::to('owl-carousel2/docs/assets/owlcarousel/assets/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ URL::to('owl-carousel2/docs/assets/owlcarousel/assets/owl.theme.default.min.css') }}">

    <!-- Prettify -->
    <link href="{{ URL::to('owl-carousel/assets/js/google-code-prettify/prettify.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Owl Carousel Assets -->
    <link href="{{ URL::to('owl-carousel/owl-carousel/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{ URL::to('owl-carousel/owl-carousel/owl.theme.css') }}" rel="stylesheet">
</head>
<body style="background-size: 200px;">
    <?php 
        $perusahaan = DB::table('tmst_perusahaan')->first(); 
        //sementara masih static
        $warna_1        = "#20BDD4";
        $warna_2        = "#808080";
        $warna_huruf    = "#4D4D4D";
        function rupiah($nominal)
        {
            $rupiah =  number_format($nominal,0, ",",".");
            $rupiah = "Rp "  . $rupiah;
            return $rupiah;
        }
    ?> 
    <style type="text/css">
        #owl-slider .item img {
        display: block;
        width: 100%;
        }
    </style>
    @include('includes.header_home')
    <div class="row">
        <div class="container">
            @include('includes.carousel_home')
        </div>
    </div>
    <div class="row">
        <div class="container">
        @include('includes.mini_banner')
        </div>
    </div>
    <div class="container">
        <style type="text/css">
            .owl-produk-pilihan .owl-theme .owl-nav .owl-next {
                color: #808080;
                right: 5px;
                top: 75px;
                font-size: 50pt;
                position: absolute;
                z-index: 100;
            }
            .owl-produk-pilihan .owl-theme .owl-nav .owl-prev {
                color: #808080;
                left: 5px;
                top: 75px;
                font-size: 50pt;
                position: absolute;
                z-index: 100;
            }

            .owl-produk .owl-theme .owl-controls .owl-page span{
                display: none;
            }

            .owl-produk  .owl-carousel .owl-wrapper-outer{
                border-left: 1px solid #9c9c9c;
                border-right: 1px solid #9c9c9c;
                border-bottom: 1px solid #9c9c9c;
            }
        </style>
        @include('pages.home.home.produk_pilihan')
        @include('pages.home.home.produk_baru')
        @include('pages.home.home.produk_akan_datang')
    </div>
    @include('includes.footer_home')
    @include('includes.base_chat')
    <script src="{{ URL::to('owl-carousel2/docs/assets/owlcarousel/owl.carousel.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#slider-banner').owlCarousel({
                items: 1,
                loop: true,
                center: true,
                margin: 10,
                callbacks: true,
                URLhashListener: true,
                autoplayHoverPause: true,
                startPosition: 'URLHash',
                nav:true,
                navText : ["<span class='fa fa-angle-left'></span>", "<span class='fa fa-angle-right'></span>"], 
                dotsData: true,
            });

            $(".owl-produk-pilihan").owlCarousel({
                loop:false,
                margin:10,
                nav:true,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:2
                    },
                    1000:{
                        items:4
                    }
                },
                navText : ["<span class='fa fa-angle-left'></span>", "<span class='fa fa-angle-right'></span>"], 
            });

            $("#owl-produk2").owlCarousel({
                items : 6,
                lazyLoad : true,
                navigation : true,
                rewindNav : false,
                navText : ["<span class='fa fa-angle-left'></span>", "<span class='fa fa-angle-right'></span>"], 
            });

            $("#owl-produk3").owlCarousel({
                items : 6,
                lazyLoad : true,
                navigation : true,
                rewindNav : false,
                navText : ["<span class='fa fa-angle-left'></span>", "<span class='fa fa-angle-right'></span>"], 
            });

            simpleCart.currency({
                code: "IDR" ,
                symbol: "Rp " ,
                delimiter: "." , 
                decimal: "," , 
                after: false ,
                accuracy: 0
            });
        });
    </script>
</body>
</html>