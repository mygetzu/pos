@extends('layouts.master_home2')
@section('content')
@include('includes.base_function')
<div class="col-sm-12">
    <h2 class="konten-title">Ubah Password</h2>
    @if(Session::has('message'))
    <div class="alert alert-success flat alert-dismissable" style="margin-left: 0px;">
      <i class="fa fa-check"></i>
      {{ Session::get('message') }}
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    @endif
    @if(Session::has('message_error'))
    <div class="alert alert-danger flat alert-dismissable" style="margin-left: 0px;">
      <i class="fa fa-ban"></i>
      {{ Session::get('message_error') }}
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    @endif
    <form class="form-horizontal" method="POST" action="{{ url('/ubah_password_pelanggan') }}">
        <input type="hidden" name="id" value="{{ Auth::user()->id }}">
        {{ csrf_field() }}
        <div class="form-group{{ $errors->has('password_old') ? ' has-error' : '' }}">
            <label class="control-label col-md-2">Password Lama</label>
            <div class="col-md-10">
                <div class="input-group" style="margin-bottom:0px">
                    <input type="password" class="form-control" name="password_old" id="password_old" value="">
                    <span class="input-group-btn">
                        <button class="btn btn-default" tabindex="-1" type="button" id="btn-pass"><span class="glyphicon glyphicon-eye-close" style="margin:3px;"></span></button>
                    </span>
                </div>
                @if ($errors->has('password_old'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password_old') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label class="control-label col-md-2">Password Baru</label>
            <div class="col-md-10">
                <div class="input-group" style="margin-bottom:0px">
                    <input type="password" class="form-control" name="password" id="password" value="">
                    <span class="input-group-btn">
                        <button class="btn btn-default" tabindex="-1" type="button" id="btn-pass2"><span class="glyphicon glyphicon-eye-close" style="margin:3px"></span></button>
                    </span>
                </div>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
            <label class="control-label col-md-2">Konfirmasi Password Baru</label>
            <div class="col-md-10">
                <div class="input-group" style="margin-bottom:0px">
                    <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" value="">
                    <span class="input-group-btn">
                        <button class="btn btn-default" tabindex="-1" type="button" id="btn-pass3"><span class="glyphicon glyphicon-eye-close" style="margin:3px"></span></button>
                    </span>
                </div>
                @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-10 col-md-offset-2">
                <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-disk"></span> simpan</button>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function(){

            $('#btn-pass').click(function(){
                var obj = document.getElementById('password_old');
                if(obj.type == "text")
                {
                    obj.type = "password"
                    $(this).find('span').removeClass('glyphicon glyphicon-eye-open');
                    $(this).find('span').addClass('glyphicon glyphicon-eye-close');
                }else{
                    obj.type = "text"
                    $(this).find('span').removeClass('glyphicon glyphicon-eye-close');
                    $(this).find('span').addClass('glyphicon glyphicon-eye-open');
                }
                   
            });

            $('#btn-pass2').click(function(){
                var obj = document.getElementById('password');
                if(obj.type == "text")
                {
                    obj.type = "password"
                    $(this).find('span').removeClass('glyphicon glyphicon-eye-open');
                    $(this).find('span').addClass('glyphicon glyphicon-eye-close');
                }else{
                    obj.type = "text"
                    $(this).find('span').removeClass('glyphicon glyphicon-eye-close');
                    $(this).find('span').addClass('glyphicon glyphicon-eye-open');
                }
                   
            });

            $('#btn-pass3').click(function(){
                var obj = document.getElementById('password_confirmation');
                if(obj.type == "text")
                {
                    obj.type = "password"
                    $(this).find('span').removeClass('glyphicon glyphicon-eye-open');
                    $(this).find('span').addClass('glyphicon glyphicon-eye-close');
                }else{
                    obj.type = "text"
                    $(this).find('span').removeClass('glyphicon glyphicon-eye-close');
                    $(this).find('span').addClass('glyphicon glyphicon-eye-open');
                }
                   
            });
      });
</script>

@stop