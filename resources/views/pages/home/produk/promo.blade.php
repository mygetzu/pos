@extends('layouts.master_home2')
@section('content')
@include('includes.base_function')
<input type="hidden" name="base_path" id="base_path" value="{{ asset('/') }}">
<div class="col-sm-12" align="center" style="margin-bottom:30px">
    <h2 class="konten-title">Promo Produk</h2>
    <div class="row" style="margin-top: 15px">
        <?php $btn_color = array(1 => 'btn-warning', 'btn-success', 'btn-info', 'btn-danger', 'btn-facebook', 'btn-twitter', 'btn-github', 'btn-linkedin', 'btn-primary', 'bg-navy')?>
        <?php
        foreach($get_category as $r_category) {
        $category_id = $r_category->id; ?>
        <div class="col-md-12" style="margin-top: 45px">
            <span class="btn btn-flat {{ $btn_color[$category_id] }} btn-block btn-lg">{{ $r_category->nama }}</span>
        </div>
        <div class="col-md-12" style="margin-top: 25px">
            <!--Promo hadiah-->
            <?php //if($promo_hadiah[$category_id) == NULL || $promo_diskon[category_id] == NULL || $promo_cashback[category_id] == NULL){ hidden_title }?>
            @if($promo_hadiah[$category_id] == NULL)
            <!--Tidak ada hadiah-->
            @else
                @foreach($promo_hadiah[$category_id] as $r_promo_hadiah)
                <div class="col-md-4">
                    <a href="{{ url('promo/hadiah/'.str_replace(" ", "-", strtolower($r_category->nama))) }}">
                        <img class="img-responsive" src="{{ url('img/banner/BIR-banner.jpg') }}" alt="Promo Hadiah {{ $r_category->nama }}"/>
                    </a>
                </div>
                @endforeach
            @endif

            <!--Promo diskon-->
            @if($promo_diskon[$category_id] == NULL)
                <!--Tidak ada diskon-->
            @else
                @foreach($promo_diskon[$category_id] as $r_promo_diskon)
                <div class="col-md-4">
                    <a href="{{ url('promo/diskon/'.str_replace(" ", "-", strtolower($r_category->nama))) }}">
                        <img class="img-responsive" src="{{ url('img/banner/BIR-banner.jpg') }}" alt="Promo Diskon {{ $r_category->nama }}"/>
                    </a>
                </div>
                @endforeach
            @endif

            <!--Promo cashback-->
            @if($promo_cashback[$category_id] == NULL)
            <!--Tidak ada cashback-->
            @else
                @foreach($promo_cashback[$category_id] as $r_promo_cashback)
                <div class="col-md-4">
                    <a href="{{ url('promo/cashback/'.str_replace(" ", "-", strtolower($r_category->nama))) }}">
                        <img class="img-responsive" src="{{ url('img/banner/BIR-banner.jpg') }}" alt="Promo Cashback {{ $r_category->nama }}"/>
                    </a>
                </div>
                @endforeach
            @endif
        </div>
        <?php 
        }
        ?>
    </div>
</div>
@stop