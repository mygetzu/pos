@extends('layouts.master_home2')
@section('content')
@include('includes.base_function')
<input type="hidden" name="base_path" id="base_path" value="{{ asset('/') }}">
<div class="col-sm-12" align="center" style="margin-bottom:30px">
    <h2 class="konten-title">Promo Produk</h2>
    <div class="row" style="margin-top: 15px">
        <?php $btn_color = array(1 => 'btn-warning', 'btn-success', 'btn-info', 'btn-danger', 'btn-facebook', 'btn-twitter', 'btn-github', 'btn-linkedin', 'btn-primary', 'bg-navy') ?>
        <div class="col-md-12" style="margin-top: 45px">
            @foreach($category as $r_category)
            <span class="btn btn-flat {{ $btn_color[$r_category->id] }} btn-block btn-lg">{{ $r_category->nama }}</span>
            @endforeach
        </div>
        <div class="col-md-12">
            @foreach($produk_promo as $r_produk)
            <div class="col-md-3">
                <div class="item" style="height: 320px; padding: 10px; border-right: 1px solid #9c9c9c; border-left: 1px solid #9c9c9c" align="center">
                    <img src="{{ url('img/produk/'.$r_produk->file_gambar) }}" class="img-responsive" alt="a" style="height:160px;object-fit: contain; padding: 20px" />
                    <h5>
                        <a href="{{ url('produk/'.$r_produk->slug_nama) }}">{{ $r_produk->nama }}</a>
                    </h5>
                    <h4>Rp {{ number_format($r_produk->harga_retail) }},-</h4>
                    <div style="width: 90%;bottom:15px; position: absolute;" align="center">
                        <a href="{{ url('produk/'.$r_produk->slug_nama) }}" <?php echo ($r_produk->stok == 0? 'onclick="return confirm(\'Stok barang saat ini tidak tersedia, apakah anda ingin melihat detail produk?\')"' : '' ) ?> class="btn btn-lg {{ ($r_produk->stok == 0 ? 'btn-default' : 'btn-success') }} btn-flat">
                            <span class="fa fa-shopping-cart"></span> Beli
                        </a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@stop
