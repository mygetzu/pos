@extends('layouts.master_home2')
@section('content')
@include('includes.base_function')
<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datepicker/datepicker3.css') }}">
<link rel="stylesheet" href="{{ asset('/adminlte/plugins/daterangepicker/daterangepicker-bs3.css') }}">
<div class="col-md-12"> 
    <h2 class="konten-title">Konfirmasi Pesanan #{{ $so_header->no_sales_order }}</h2>
    <a href="{{ url('/pesanan') }}">
        <button class='btn btn-default flat pull-right'><span class='fa fa-arrow-left'></span> kembali</button>
    </a>
    <br><br><br><br>
    <form class="form-horizontal" method="post" action="{{ url('/do_konfirmasi_pembayaran') }}">
        {{ csrf_field() }}
        <input type="hidden" name="so_header_id" value="{{ $so_header->id }}">
        <div class="form-group">
            <label class="control-label col-md-3">No Sales Order</label>
            <div class="col-md-8">
                <input type="text" class="form-control" value="{{ $so_header->no_sales_order }}" disabled="true">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3">Total Tagihan</label>
            <div class="col-md-8">
                <input type="text" class="form-control" value="{{ rupiah($so_header->total_tagihan) }}" disabled="true">
            </div>
        </div>
        <div class="form-group{{ $errors->has('tanggal') ? ' has-error' : '' }}">
            <label class="col-sm-3 control-label">Tanggal</label>
            <div class="col-sm-8">
                <div class="input-group">
                    <?php $date = new DateTime(); 
                    $tanggal = $date->format('d').'-'.$date->format('m').'-'.$date->format('Y'); ?>
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" id="tanggal" data-date-format="dd-mm-yyyy" name="tanggal" value="{{ $tanggal }}" placeholder="Masukkan tanggal">
                </div>
                @if ($errors->has('tanggal'))
                    <span class="help-block">
                        <strong>{{ $errors->first('tanggal') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('metode_pembayaran_id') ? ' has-error' : '' }}">
            <label class="control-label col-md-3">Metode Pembayaran</label>
            <div class="col-md-8">
                <select class="form-control" name="metode_pembayaran_id">
                    @foreach($metode_pembayaran as $value)
                    <option value="{{ $value->id }}" <?php if($value->id == $so_pembayaran->metode_pembayaran_id) echo "selected"; ?>>{{ $value->nama }}</option>
                    @endforeach
                </select>
                @if ($errors->has('metode_pembayaran_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('metode_pembayaran_id') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3">Bank</label>
            <div class="col-md-8">
                <select class="form-control" name="bank_id">
                    <option value="">-- Pilih Bank --</option>
                    @foreach($bank as $value)
                    <option value="{{ $value->id }}" <?php if($value->id == $so_pembayaran->bank_id) echo "selected"; ?>>{{ $value->nama }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group{{ $errors->has('nomor_pembayaran') ? ' has-error' : '' }}">
            <label class="control-label col-md-3">Nomor Pembayaran/No Rekening</label>
            <div class="col-md-8">
                <input type="text" name="nomor_pembayaran" id="nomor_pembayaran" class="form-control" value="{{ old('nomor_pembayaran') }}">
                @if ($errors->has('nomor_pembayaran'))
                    <span class="help-block">
                        <strong>{{ $errors->first('nomor_pembayaran') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('nominal') ? ' has-error' : '' }}">
            <label class="control-label col-md-3">Nominal</label>
            <div class="col-md-2">
                <input type="text" class="form-control" name="nominal" placeholder="masukkan nominal" id="nominal" data-thousands="." data-decimal="," style="text-align:right" value="{{ old('nominal') }}" />
                @if ($errors->has('nominal'))
                    <span class="help-block">
                        <strong>{{ $errors->first('nominal') }}</strong>
                    </span>
                @endif
            </div>
            <div class="col-md-3">
                <label class="control-label">(IDR)</label>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-8 col-md-offset-3">
                <button class="btn btn-primary flat"><span class="fa fa-floppy-o"></span> Simpan</button>
            </div>
        </div>
    </form>
</div> 
<script src="{{ asset('/adminlte/plugins/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ url('/js/jquery.maskMoney.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $(function () {
        $('#tanggal').datepicker({
            autoclose: true,
            todayHighlight: 1,
        });

        $("#nominal").maskMoney({precision:0});
    });
</script>
@stop