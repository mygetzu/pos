@extends('layouts.master_home2')
@section('content')
@include('includes.base_function')
<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<div class="col-md-12"> 
    <h2 class="konten-title">Pesanan</h2>
    @if(Session::has('message'))
    <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
      <i class="fa fa-check"></i>
      {{ Session::get('message') }}
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    @endif
    <table class="table table-striped table-bordered" id="mydatatables">
        <thead>
            <th style="text-align:center">NO</th>
            <th style="text-align:center">NO NOTA</th>
            <th style="text-align:center">TANGGAL</th>
            <th style="text-align:center">TOTAL TAGIHAN</th>
            <th style="text-align:center">STATUS PEMBAYARAN</th>
            <th style="text-align:center">STATUS PENGIRIMAN</th>
            <th style="text-align:center"></th>
        </thead>
        <tbody>
            <?php $i=1;  ?>
            @foreach($so_header as $val)
            <tr>
                <td align="center">{{ $i++ }}</td>
                <td align="center">#{{ $val['no_invoice'] }}</td>
                <td>{{ $val['tanggal'] }}</td>
                <td>{{ rupiah($val['total_tagihan']) }}</td>
                <td>
                    {{ $val['status_pembayaran'] }}
                    @if($val['flag_konfirmasi_bayar'] == 0)
                    <?php $id = $val['id']; ?>
                    <br><a href="{{ url('/konfirmasi_pembayaran/'.$id) }}" class="btn btn-success flat"><span class="fa fa-money"></span> Konfirmasi Pembayaran</a>
                    @endif
                </td>
                <td>{{ $val['status_pengiriman'] }}</td>
                <td align="center">
                    <?php $id = $val['id']; ?>
                    <a class="btn btn-info flat" href="{{ url('/detail_pesanan/'.$id) }}"><span class="fa fa-info-circle"></span> detail</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div> 
<script>
  $(function () {
    $("#mydatatables").DataTable();
  });
</script>

<input type="hidden" name="base_path" id="base_path" value="{{ asset('/') }}">
<script type="text/javascript">
    $(document).ready(function(){
    var url = window.location.href;
    var path =  document.getElementById('base_path').value;
    //alert(url)
    $("a").each(function(){
        if(url == (this.href)){
            $(this).addClass("active");
        }
    });
    if(url== path)
    {
        document.getElementById('beranda').className += " active";
    }
});
</script>
@stop