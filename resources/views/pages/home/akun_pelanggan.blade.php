@extends('layouts.master_home2')
@section('content')
@include('includes.base_function')
<div class="col-md-12">
    <h2 class="konten-title">Akun Saya</h2>
    @if(Session::has('message'))
    <div class="alert alert-success flat alert-dismissable" style="margin-left: 0px;">
      <i class="fa fa-check"></i>
      {{ Session::get('message') }}
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    @endif
    <div id="ubah_akun" style="display:block">
        <div class="">
          <button class="btn btn-primary" onclick="ubah_akun('{{ Auth::user()->hak_akses_id }}','{{ $my_kota->provinsi_id }}','{{ $my_kota->kota_id }}')"><span class='fa fa-edit'></span> ubah</button>
        </div>
    </div>
    <div id="ubah_batal" style="display:none">
        <div class="">
          <button class="btn btn-primary" style="background:#aaa" onclick="ubah_batal()"><span class='fa fa-ban'></span> batal</button>
        </div>
    </div>
    <form class="form-horizontal" method="POST" action="{{ url('/ubah_akun_pelanggan') }}" autocomplete="off">
        <input type="hidden" name="id" value="{{ Auth::user()->id }}">
        <input type="hidden" name="hak_akses" value="2">
        <input type="hidden" name="pelanggan_id" value="{{ $pelanggan->id }}">
        {{ csrf_field() }}
        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name" class="control-label col-md-2">Nama</label>
            <div class="col-md-10">
                <input id="name" type="text" class="form-control" name="name" id="name" value="{{ $pelanggan->nama }}" disabled="true" style="text-transform:capitalize">
                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>Kolom Nama Wajib Diisi</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email" class="control-label col-md-2">E-Mail</label>
            <div class="col-md-10">
                <input id="email" type="text" class="form-control" name="email" id="email" value="{{ $pelanggan->email }}" disabled="true">
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>Kolom Nama Wajib Diisi</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('alamat') ? ' has-error' : '' }}">
            <label for="alamat" class="control-label col-md-2">Alamat</label>
            <div class="col-md-10">
                <input id="alamat" type="text" class="form-control" name="alamat" id="alamat" value="{{ $pelanggan->alamat }}" disabled="true" style="text-transform:capitalize">
                @if ($errors->has('alamat'))
                    <span class="help-block">
                        <strong>Kolom Alamat Wajib Diisi</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('kota') ? ' has-error' : '' }}">
            <label class="control-label col-md-2">Kabupaten/Kota</label>
            <div class="col-md-10" id="kota_saya">
                <input type="text" class="form-control" value="{{ ucwords(strtolower($my_kota->nama)) }}" disabled="true" placeholder="Masukkan Kota">
                @if ($errors->has('provinsi') AND $errors->has('kota'))
                    <span class="help-block">
                        <strong>Pilihan Provinsi Serta Kabupaten/Kota Wajib Diisi</strong>
                    </span>
                @elseif ($errors->has('provinsi'))
                    <span class="help-block">
                        <strong>Pilihan Provinsi Wajib Diisi</strong>
                    </span>
                @elseif ($errors->has('kota'))
                    <span class="help-block">
                        <strong>Pilihan Kabupaten/Kota Wajib Diisi</strong>
                    </span>
                @endif
            </div>
            <div class="col-md-5" id="edit_kota_saya" style="display:none">
                <select class="form-control" id="provinsi" name="provinsi">
                    <option value="">-- Pilih Provinsi --</option>
                    @foreach($provinsi as $val)
                    <option value="{{ $val->kode }}">{{ $val->nama }}</option>
                    @endforeach
                </select>
                @if ($errors->has('provinsi'))
                    <span class="help-block">
                        <strong>Pilihan Provinsi Wajib Diisi</strong>
                    </span>
                @endif
            </div>
            <div class="col-md-5" id="edit_kota_saya2" style="display:none">
                <select class="form-control" id="kota" name="kota">
                    <option value="">-- Pilih Kabupaten/Kota --</option>
                    @foreach($kota as $val)
                    <option value="{{ $val->id }}">{{ $val->nama }}</option>
                    @endforeach
                </select>
                @if ($errors->has('kota'))
                    <span class="help-block">
                        <strong>Pilihan Kabupaten/Kota Wajib Diisi</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('kode_pos') ? ' has-error' : '' }}">
            <label for="kode_pos" class="control-label col-md-2">Kode POS</label>
            <div class="col-md-10">
                <input type="text" class="form-control" name="kode_pos" id="kode_pos" value="{{ $pelanggan->kode_pos }}" disabled="true">
                @if ($errors->has('kode_pos'))
                    <span class="help-block">
                        <strong>{{ $errors->first('kode_pos') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label for="telp1" class="control-label col-md-2">Telepon 1</label>
            <div class="col-md-4">
                <div class="input-group" style="margin-bottom:0px">
                    <div class="input-group-addon">
                        <i class="fa fa-phone"></i>
                    </div>
                    <input style="display:none" type="text" class="form-control" data-mask placeholder="contoh: (031)-9999999" name="telp1" id="telp1" value="{{ $pelanggan->telp1 }}">

                    <input type="text" class="form-control" placeholder="contoh: (031)-9999999" name="display_telp1" id="display_telp1" disabled="true" value="<?php echo str_replace('_', '', $pelanggan->telp1); ?>">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="telp2" class="control-label col-md-2">Telepon 2</label>
            <div class="col-md-4">
                <div class="input-group" style="margin-bottom:0px">
                    <div class="input-group-addon">
                        <i class="fa fa-phone"></i>
                    </div>
                    <input style="display:none" type="text" class="form-control" data-mask placeholder="contoh: (031)-9999999" name="telp2" id="telp2" value="{{ $pelanggan->telp2 }}">

                    <input type="text" class="form-control" placeholder="contoh: (031)-9999999" name="display_telp2" id="display_telp2" disabled="true" value="<?php echo str_replace('_', '', $pelanggan->telp2); ?>">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="hp_sales" class="control-label col-md-2">HP</label>
            <div class="col-md-4">
                <div class="input-group" style="margin-bottom:0px">
                    <div class="input-group-addon">
                        <i class="fa fa-mobile" style="width:12px"></i>
                    </div>
                    <input style="display:none" type="text" class="form-control" data-inputmask='"mask": "(+62)9999-9999999"' data-mask2 placeholder="contoh: (+62)8577-9999999" name="hp_sales" id="hp_sales" value="{{ $pelanggan->hp_sales }}">

                    <input type="text" class="form-control" placeholder="contoh: (+62)8577-9999999" name="display_hp_sales" id="display_hp_sales" disabled="true" value="<?php echo str_replace('_', '', $pelanggan->hp_sales); ?>">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="deskripsi" class="control-label col-md-2">Deskripsi</label>
            <div class="col-md-10">
                <textarea id="deskripsi" type="text" class="form-control" name="deskripsi" id="deskripsi" disabled="true" style="text-transform:capitalize">{{ $pelanggan->deskripsi }}</textarea>
                @if ($errors->has('deskripsi_pelanggan'))
                    <span class="help-block">
                        <strong>{{ $errors->first('deskripsi_pelanggan') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-10 col-md-offset-2" style="display:none" id="button_submit">
                <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-disk"></span> simpan</button>
            </div>
        </div>
    </form>
</div>
<input type="hidden" name="base_path" id="base_path" value="{{ asset('/') }}">
<!-- InputMask -->
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){
          $('#provinsi').change(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            var provinsi_kode = document.getElementById('provinsi').value;
            var path =  document.getElementById('base_path').value;
            var url = path+"get_kota/"+provinsi_kode;
            
            $.ajax({
                type: "POST",
                url: url,
                success: function (data) {
                    $("#kota").html(data);
                },
                error: function (data) {
                    $("#kota").html("-- Pilih Kabupaten/Kota --");
                }
            });
          });
      });

    $(function () {
    
        $("[data-mask]").inputmask({ mask: "(999[9])-9999999", greedy: false ,
        definitions: {
                '*': {
                validator: "[0-9-]",
                cardinality: 1,
                casing: "lower"
            }
        }
        });

    $("[data-mask2]").inputmask();
  });

    function ubah_akun($hak_akses,provinsi_id,kota_id)
    {
        document.getElementById("name").disabled = false;
        document.getElementById("email").disabled = false;
        document.getElementById("alamat").disabled = false;
        
        document.getElementById("telp1").style.display           = 'block';
        document.getElementById("display_telp1").style.display   = 'none';

        document.getElementById("telp2").style.display           = 'block';
        document.getElementById("display_telp2").style.display   = 'none';

        document.getElementById("kode_pos").disabled = false;
        
        document.getElementById("hp_sales").style.display           = 'block';
        document.getElementById("display_hp_sales").style.display   = 'none';

        document.getElementById("deskripsi").disabled = false;
        
        document.getElementById("ubah_akun").style.display = 'none';
        document.getElementById("ubah_batal").style.display = 'block';
        document.getElementById("button_submit").style.display = 'block';

        document.getElementById("kota_saya").style.display = 'none';
        document.getElementById("edit_kota_saya").style.display = 'block';
        document.getElementById("edit_kota_saya2").style.display = 'block';

        document.getElementById("provinsi").selectedIndex = provinsi_id;
        document.getElementById("kota").selectedIndex = kota_id;
    }
    function ubah_batal()
    {
        document.getElementById("name").disabled = true;
        document.getElementById("email").disabled = true;
        document.getElementById("alamat").disabled = true;
            
        document.getElementById("telp1").style.display           = 'none';
        document.getElementById("display_telp1").style.display   = 'block';

        document.getElementById("telp2").style.display           = 'none';
        document.getElementById("display_telp2").style.display   = 'block';

        document.getElementById("kode_pos").disabled = true;
        
        document.getElementById("hp_sales").style.display           = 'none';
        document.getElementById("display_hp_sales").style.display   = 'block';

        document.getElementById("deskripsi").disabled = true;
        document.getElementById("ubah_akun").style.display = 'block';
        document.getElementById("ubah_batal").style.display = 'none';
        document.getElementById("button_submit").style.display = 'none';

        document.getElementById("kota_saya").style.display = 'block';
        document.getElementById("edit_kota_saya").style.display = 'none';
        document.getElementById("edit_kota_saya2").style.display = 'none';
    }

    
</script>
@endsection
