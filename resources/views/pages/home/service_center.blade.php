@extends('layouts.master_home2')
@section('content')
@include('includes.base_function')
<input type="hidden" name="base_path" id="base_path" value="{{ asset('/') }}">
<div class="col-sm-12" align="center" style="margin-bottom:30px;">
    <h2 class="konten-title">Service Center</h2>
    <div class="row">
        <div class="col-md-12">
            <img class="img-responsive" alt="Service Center Indonusa Solutama" src="{{ asset('/img/service_center.jpg') }}"/>
        </div>
    </div>
    <div class="row" style="margin-top: 35px">
        <div class="col-md-4">
            <h4 style="font-weight: bold">Galerindo Service Center</h4>
        </div>
        <div class="col-md-4">
            <p>
                Hi Tech Mall Lt. Dasar Blok D No. 17-19
                <br/>
                Jl. Kusuma Bangsa No. 116-118. Surabaya
                <br/>
                60138 - Jawa Timur - Indonesia
            </p>
        </div>
        <div class="col-md-4">
            <p>
                Ph. +6231 5477085 | +6231 5348998
                <br/>
                Fax. +6231 5477093
            </p>
        </div>
    </div>
    <div class="row" style="margin-top: 15px">
        <div class="col-md-4">
            <h4 style="font-weight: bold">Indonusa Service Center</h4>
        </div>
        <div class="col-md-4">
            <p>
                Ruko City Pride | Palacio
                <br/>
                Jl. Nginden Semolo 42 No. 20B.
                <br/>
                Surabaya - Jawa Timur - Indonesia
            </p>
        </div>
        <div class="col-md-4">
            <p>
                Ph. +6231 5477085 | +6231 5348998
                <br/>
                Fax. +6231 5477093
            </p>
        </div>
    </div>
    <?php echo $service_center; ?>
</div>
@stop