@extends('layouts.master_home2')
@section('content')
@include('includes.base_function')

<input type="hidden" name="base_path" id="base_path" value="{{ asset('/') }}">
<div class="products-grids">
    <div class="row">
        <div class="col-md-5 grid-single">      
            @if(!empty($paket->file_gambar))
            <div class="view-product">
                <img src="{{ URL::asset('/img/produk/'.$paket->file_gambar) }}" alt="{{ $paket->nama }}" data-imagezoom="true" class="img-responsive"  id="gambar"/>
            </div>
            @else
                <img src="{{ URL::asset('img/'.$gambar_default->value) }}" alt="{{ $paket->nama }}" data-imagezoom="true" class="img-responsive"  id="gambar"/>
            @endif
            <!-- FlexSlider -->
            <script src="{{ URL::to('nuevo/js/imagezoom.js') }}"></script>
            <script defer src="{{ URL::to('nuevo/js/jquery.flexslider.js') }}"></script>
            <script>
            // Can also be used with $(document).ready()
            $(window).load(function() {
                $('.flexslider').flexslider({
                    animation: "slide",
                    controlNav: "thumbnails"
                });
            });
            </script>
        </div>  
        <div class="col-md-7 single-text">
            <div class="details-left-info simpleCart_shelfItem">
                <h3>{{ $paket->nama }}</h3>
                <p class="availability">Ketersediaan: <span class="color"><?php if((int)$paket->stok - (int)$paket->stok_dipesan > 0) { echo "Ada"; } else { echo "Kosong"; } ?></span></p>

                @if((int)$paket->stok - (int)$paket->stok_dipesan > 0)
                    <form method="post" action="{{ url('/home_tambah_cart_paket') }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="paket_id" value="{{ $paket->id }}">
                        <input type="hidden" name="jumlah_beli" value="1">
                        <button class="btn btn-success flat"><i class="fa fa-cart-plus"></i> Beli</button>
                    </form>
                @else
                    <p>Stok Kosong</p>
                @endif
                </form>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
</div>
<div class="panel-group collpse" id="accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Deskripsi
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
        <label class="control-label">Paket Produk terdiri dari produk berikut : </label><br>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th style="text-align: center;">NAMA PRODUK</th>
                    <th style="text-align: center;">QTY</th>
                    <th style="text-align: center;"></th>
                </tr>
            </thead>
            <tbody>
                @foreach($paket->paket_produk as $val)
                <tr>
                    <td>{{ $val->produk->nama }}</td>
                    <td align="center">{{ $val->qty_produk }}</td>
                    <td align="center">
                        @if(!empty($val->produk->produk_galeri_first))
                            <img src="{{ URL::asset('/img/produk/'.$val->produk->produk_galeri_first->file_gambar) }}" style="height: 70px;"></td>
                        @else
                            <img src="{{ URL::asset('/img/'.$gambar_default->value) }}" style="height: 70px;"></td>
                        @endif
                </tr>
                @endforeach
            </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    function rumus_harga_diskon(jumlah_beli,diskon,qty_beli_diskon,harga_retail)
    {
        jumlah_beli         = parseInt(jumlah_beli);
        diskon              = parseInt(diskon);
        qty_beli_diskon     = parseInt(qty_beli_diskon);
        harga_retail        = parseInt(harga_retail);

        var modulus         = jumlah_beli%qty_beli_diskon;
        var harga_akhir     = ((jumlah_beli-modulus)*(harga_retail-((diskon*harga_retail)/100)))+(modulus*harga_retail);

        return harga_akhir;
    }

    function rumus_harga_cashback(jumlah_beli,cashback,qty_beli_cashback,harga_retail)
    {
        jumlah_beli         = parseInt(jumlah_beli);
        cashback            = parseInt(cashback);
        qty_beli_cashback   = parseInt(qty_beli_cashback);
        harga_retail        = parseInt(harga_retail);

        var modulus         = jumlah_beli%qty_beli_cashback;
        var harga_akhir     = ((jumlah_beli-modulus)*(harga_retail-cashback))+(modulus*harga_retail);

        return harga_akhir;
    }

    function rupiah(nominal)
    {
        //rumus konversi rupiah
        nominal = "Rp "+nominal.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
        return nominal;
    }

    function img_click(file_gambar)
    {
        var path =  document.getElementById('base_path').value;
        document.getElementById("gambar").src = path+"/img/produk/"+ file_gambar;
    }

    function add_beli()
    {
        var jumlah_beli         = document.getElementById("jumlah_beli").value;
        var harga_retail        = document.getElementById("harga_retail").value;
        var diskon              = document.getElementById("diskon").value;
        var qty_beli_diskon     = document.getElementById("qty_beli_diskon").value;
        var cashback            = document.getElementById("cashback").value;
        var qty_beli_cashback   = document.getElementById("qty_beli_cashback").value;

        jumlah_beli = parseInt(jumlah_beli);
        jumlah_beli++;

        if(diskon != '0')
        {
            harga_akhir = rumus_harga_diskon(jumlah_beli,diskon,qty_beli_diskon,harga_retail);
        }
        else
        {
            harga_akhir = rumus_harga_cashback(jumlah_beli,cashback,qty_beli_cashback,harga_retail);
        }

        document.getElementById("jumlah_beli").value                = jumlah_beli;
        document.getElementById("display_jumlah_beli").value        = jumlah_beli;
        document.getElementById("display_harga_akhir").innerHTML    = rupiah(harga_akhir);

        if(jumlah_beli == 1)
        {
            document.getElementById("harga_coret").innerHTML = rupiah(harga_retail);
        }
        else
        {
            document.getElementById("harga_coret").innerHTML = jumlah_beli+" x "+rupiah(harga_retail)+" = "+rupiah(jumlah_beli*harga_retail);
        }
    }

    function min_beli()
    {
        var jumlah_beli         = document.getElementById("jumlah_beli").value;
        var harga_retail        = document.getElementById("harga_retail").value;
        var diskon              = document.getElementById("diskon").value;
        var qty_beli_diskon     = document.getElementById("qty_beli_diskon").value;
        var cashback            = document.getElementById("cashback").value;
        var qty_beli_cashback   = document.getElementById("qty_beli_cashback").value;

        jumlah_beli = parseInt(jumlah_beli);
        jumlah_beli--;
        if(jumlah_beli == 0) return;

        if(diskon != '0')
        {
            harga_akhir = rumus_harga_diskon(jumlah_beli,diskon,qty_beli_diskon,harga_retail);
        }
        else
        {
            harga_akhir = rumus_harga_cashback(jumlah_beli,cashback,qty_beli_cashback,harga_retail);
        }

        document.getElementById("jumlah_beli").value                = jumlah_beli;
        document.getElementById("display_jumlah_beli").value        = jumlah_beli;
        document.getElementById("display_harga_akhir").innerHTML    = rupiah(harga_akhir);

        if(jumlah_beli == 1)
        {
            document.getElementById("harga_coret").innerHTML = rupiah(harga_retail);
        }
        else
        {
            document.getElementById("harga_coret").innerHTML = jumlah_beli+" x "+rupiah(harga_retail)+" = "+rupiah(jumlah_beli*harga_retail);
        }
    }

    function btn_beli_klik()
    {
        document.getElementById("form_beli").submit();
    }
</script>
@stop