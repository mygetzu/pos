@extends('layouts.master_home2')
@section('content')
@include('includes.base_function')
<?php
$diskon = 0;
$qty_beli_diskon = 1;
$cashback = 0;
$qty_beli_cashback = 1;
$hadiah = "";
$qty_beli_hadiah = 1;
$qty_hadiah = 1;

if (!empty($pelanggan->kategori_pelanggan)) {
    if ($pelanggan->kategori_pelanggan->default_diskon > $diskon) {
        $diskon = $pelanggan->kategori_pelanggan->default_diskon;
    }
}

if (!empty($produk->produk_harga)) {
    if ($produk->produk_harga->diskon > $diskon) {
        $diskon = $produk->produk_harga->diskon;
    }
}

if (!empty($produk->promo_diskon)) {
    if ($produk->promo_diskon->diskon > $diskon) {
        $diskon = $produk->promo_diskon->diskon;
        $qty_beli_diskon = $produk->promo_diskon->qty_beli;
    }
}

if (!empty($produk->promo_cashback) && empty($produk->produk_harga) && empty($pelanggan->kategori_pelanggan)) {
    $cashback = $produk->promo_cashback->cashback;
    $qty_beli_cashback = $produk->promo_cashback->qty_beli;
}

if (!empty($produk->promo_hadiah)) {
    $hadiah = $produk->promo_hadiah->hadiah->nama;
    $qty_beli_hadiah = $produk->promo_hadiah->qty_beli;
    $qty_hadiah = $produk->promo_hadiah->qty_hadiah;
}
?>
<style type="text/css">
    .owl-produk .owl-theme .owl-controls .owl-buttons .owl-next {
        color: #808080;
        right: 5px;
        top: 75px;
        font-size: 50pt;
        position: absolute;
        z-index: 100;
    }
    .owl-produk .owl-theme .owl-controls .owl-buttons .owl-prev {
        color: #808080;
        left: 5px;
        top: 75px;
        font-size: 50pt;
        position: absolute;
        z-index: 100;
    }

    .owl-produk .owl-theme .owl-controls .owl-page span{
        display: none;
    }
</style>
<div class="products-grids">
    <div class="row">
        <div class="col-md-5 grid-single">      
            @if(!empty($produk->produk_galeri_first->file_gambar))
            <img src="{{ URL::asset('/img/produk/'.$produk->produk_galeri_first->file_gambar) }}" data-imagezoom="true" class="img-responsive zoom" alt="" style="background-color: white;" />
            @else
            <img src="{{ URL::asset('img/produk/'.$gambar_produk_default) }}" data-imagezoom="true" class="img-responsive zoom" alt="" style="background-color: white;" />
            @endif
            <div id="owl-thumb" class="owl-carousel">
                @foreach($produk->produk_galeri as $value)
                <div class="item"><img class="lazyOwl" data-src="{{ URL::asset('/img/produk/'.$value->file_gambar) }}" alt="Lazy Owl Image"></div>
                @endforeach
            </div>
            <!-- FlexSlider -->
            <script src="{{ URL::to('nuevo/js/imagezoom.js') }}"></script>
            <script defer src="{{ URL::to('nuevo/js/jquery.flexslider.js') }}"></script>
            <script>
// Can also be used with $(document).ready()
$(window).load(function () {
    $('.flexslider').flexslider({
        animation: "slide",
        controlNav: "thumbnails"
    });
});
            </script>
        </div>  
        <div class="col-md-7 single-text">
            <div class="details-left-info simpleCart_shelfItem">
                <h3>{{ $produk->nama }}</h3>
                <p class="availability">Ketersediaan: <span class="color"><?php
                        if (((int) $produk->stok - (int) $produk->stok_dipesan) > 0) {
                            echo "Ada";
                        } else {
                            echo "Kosong";
                        }
                        ?></span></p>

                @if((int)$diskon != 0 AND (int)$qty_beli_diskon === 1)
                <span class="reducedfrom">{{ rupiah($produk->harga_retail) }}</span><br>
                <span class="actual item_price" id="display_harga_akhir">{{ rupiah($produk->harga_retail-($produk->harga_retail*(int)$diskon/100)) }}</span>
                @elseif((int)$cashback != 0 AND (int)$qty_beli_cashback === 1)
                <span class="reducedfrom">{{ rupiah($produk->harga_retail) }}</span><br>
                <span class="actual item_price" id="display_harga_akhir">{{ rupiah($produk->harga_retail-(int)$cashback) }}</span>
                @else
                <span class="actual item_price" id="display_harga_akhir">{{ rupiah($produk->harga_retail) }}</span>
                @endif
                <input type="hidden" id="harga_retail" value="{{ $produk->harga_retail }}">
                <input type="hidden" id="diskon" value="{{ $diskon }}">
                <input type="hidden" id="qty_beli_diskon" value="{{ $qty_beli_diskon }}">
                <input type="hidden" id="cashback" value="{{ $cashback }}">
                <input type="hidden" id="qty_beli_cashback" value="{{ $qty_beli_cashback }}">

                <form method="post" action="{{ url('/home_tambah_cart') }}" id="form_beli_old" class="form-horizontal">
                    {{csrf_field()}}
                    <input type="hidden" name="produk_id" id="produk_id" value="{{ $produk->id }}">
                    <input type="hidden" name="jumlah_beli" id="jumlah_beli" value="1">
                    <div class="quantity_box">
                        <div class="form-group">
                            <label class="control-label col-md-2">Jumlah:</label>
                            <div class="col-md-3">
                                <input type="text" disabled="true" id="display_jumlah_beli" value="1" class="form-control">
                            </div>
                            <button type="button" onclick="add_beli()" class="btn btn-danger flat cart" style="margin-left: 0px;"><i class="fa fa-plus"></i></button>
                            <button type="button" onclick="min_beli()" class="btn btn-danger flat cart" style="margin-left: 0px;"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <?php if (((int) $produk->stok - (int) $produk->stok_dipesan) > 0) { ?>
                        <br>
                        <button type="submit" style="margin-left:0" class="btn btn-success btn-flat btn-lg cart"><i class="fa fa-shopping-cart"></i> Beli</button>
                        <br><br><?php } ?>
                </form>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
</div>
<div class="panel-group collpse" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    Deskripsi
                </a>
            </h4>
        </div>
        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
                <?php echo $produk->deskripsi; ?>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <div class="products-grids">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <img class="img-responsive" src="http://localhost/pos/public/img/produk/PRD7NEYU8R9.jpg"/>
                    </div>
                    <div class="col-md-6">
                        <h3>DELL INSPIRON 3458 I3-5005 UMA UBT - BLACK</h3>
                        <h3 style="color: skyblue">Rp. 5.500.000,- <span class="text-sm">(IDR)</span></h3>
                        <div class="" style="margin-top: 15px">
                            <h4>Spesifikasi Produk</h4>
                            <ul class="list-unstyled text-sm">
                                <li>Layar : </li>
                                <li>Processor : </li>
                                <li>Grafis : </li>
                                <li>Sistem Operasi : </li>
                                <li>Memori RAM : </li>
                                <li>Hard Disk : </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h4>Deskripsi Produk</h4>
                        </div>
                        <div class="box-body">
                            jksdbsjdch sdjfhlksafdmdskfhnsdc sdhvdhcpajcapsocjo wdqjd
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h4>Spesifikasi Produk</h4>
                        </div>
                        <div class="box-body" style="font-size: 14px">
                            <span>
                                <div class="col-md-3">Kapasitas Penyimpanan</div>
                                <div class="col-md-9">: 500 GB</div>
                            </span>
                            <span>
                                <div class="col-md-3">Kapasitas Penyimpanan</div>
                                <div class="col-md-9">: 500 GB</div>
                            </span>
                            <span>
                                <div class="col-md-3">Kapasitas Penyimpanan</div>
                                <div class="col-md-9">: 500 GB</div>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="col-md-12">
            <form method="post" action="{{ url('/home_tambah_cart') }}" id="form_beli" class="form-horizontal">
                <p>Ketersediaan : <span style="color: yellowgreen; font-weight: bold;">Ada</span></p>
                {{csrf_field()}}
                <input type="hidden" name="produk_id" id="produk_id" value="{{ $produk->id }}">
                <input type="hidden" name="jumlah_beli" id="jumlah_beli" value="1">
                <div class="quantity_box">
                    <div class="form-group">
                        <label class="control-label col-md-2">Jumlah:</label>
                        <div class="col-md-3">
                            <input type="text" disabled="true" id="display_jumlah_beli" value="1" class="form-control">
                        </div>
                        <button type="button" onclick="add_beli()" class="btn btn-danger flat cart" style="margin-left: 0px;"><i class="fa fa-plus"></i></button>
                        <button type="button" onclick="min_beli()" class="btn btn-danger flat cart" style="margin-left: 0px;"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <?php $sisa_stok = (((int) $produk->stok - (int) $produk->stok_dipesan)); ?>
                <div class="" style="margin: 0px 25px 0px 35px">
                    <button type="submit" class="btn btn-success btn-flat btn-block btn-lg {{ ($sisa_stok == 0? 'disabled' : '') }}" {{ ($sisa_stok == 0? 'disabled' : '') }}><i class="fa fa-cart-plus"></i> BELI</button>
                </div>
            </form>
            <form class="form-horizontal hidden">
                <div class="box-body" style="">
                    <div class="form-group" style="border: 1px solid #808080; padding: 5px">
                        <label for="inputEmail3" class="col-sm-2 control-label">Jumlah</label>
                        <div class="col-sm-10">
                            <input type="qty" class="form-control" id="inputEmail3">
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-success btn-flat btn-block btn-lg"><i class="fa fa-cart-plus"></i> BELI</button>
                </div>
            </form>
        </div>
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h4>Produk Terkait</h4>
                </div>
                <div class="box-body" style="font-size: 14px">
                    @for($i = 0; $i < 6; $i++)
                    <div class="col-md-12" style="margin-top: 25px">
                        <div class="col-md-4">
                            <img class="img-responsive" src="http://localhost/pos/public/img/produk/PRD7NEYU8R9.jpg" alt="Gambar bla"/>
                        </div>
                        <div class="col-md-8">
                            <h4 class="title" style="font-size: 14px">DELL INSPIRON 3458 I3-5005 UMA UBT - RED</h4>
                            <h4 class="description" style="font-size: 18px; font-weight: bold;">Rp. 8.500.000,- </h4>
                            <h4 class="text-sm" style="text-decoration: line-through">Rp. 7.500.000,-</h4>
                        </div>
                    </div>
                    @endfor
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h4>Produk Lainnya</h4>
            </div>
            <div class="box-body">
                @for($i = 0; $i < 6; $i++)
                <div class="col-md-2">
                    <img class="img-responsive" src="http://localhost/pos/public/img/produk/PRD7NEYU8R9.jpg" alt="DELL INSPIRON 3458 I3-5005 UMA UBT - RED"/>
                    <h4 style="font-size: 14px">DELL INSPIRON 3458 I3-5005 UMA UBT - RED</h4>
                    <h4 class="description" style="font-size: 18px; font-weight: bold;">Rp. 8.500.000,- </h4>
                    <h4 class="text-sm" style="text-decoration: line-through">Rp. 7.500.000,-</h4>
                </div>
                @endfor
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function rumus_harga_diskon(jumlah_beli, diskon, qty_beli_diskon, harga_retail)
    {
        jumlah_beli = parseInt(jumlah_beli);
        diskon = parseInt(diskon);
        qty_beli_diskon = parseInt(qty_beli_diskon);
        harga_retail = parseInt(harga_retail);

        var modulus = jumlah_beli % qty_beli_diskon;
        var harga_akhir = ((jumlah_beli - modulus) * (harga_retail - ((diskon * harga_retail) / 100))) + (modulus * harga_retail);

        return harga_akhir;
    }

    function rumus_harga_cashback(jumlah_beli, cashback, qty_beli_cashback, harga_retail)
    {
        jumlah_beli = parseInt(jumlah_beli);
        cashback = parseInt(cashback);
        qty_beli_cashback = parseInt(qty_beli_cashback);
        harga_retail = parseInt(harga_retail);

        var modulus = jumlah_beli % qty_beli_cashback;
        var harga_akhir = ((jumlah_beli - modulus) * (harga_retail - cashback)) + (modulus * harga_retail);

        return harga_akhir;
    }

    function rupiah(nominal)
    {
        //rumus konversi rupiah
        nominal = "Rp " + nominal.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
        return nominal;
    }

    function img_click(file_gambar)
    {
        var path = document.getElementById('base_path').value;
        document.getElementById("gambar").src = path + "/img/produk/" + file_gambar;
    }

    function add_beli()
    {
        var jumlah_beli = document.getElementById("jumlah_beli").value;
        var harga_retail = document.getElementById("harga_retail").value;
        var diskon = document.getElementById("diskon").value;
        var qty_beli_diskon = document.getElementById("qty_beli_diskon").value;
        var cashback = document.getElementById("cashback").value;
        var qty_beli_cashback = document.getElementById("qty_beli_cashback").value;

        jumlah_beli = parseInt(jumlah_beli);
        jumlah_beli++;

        if (diskon != '0')
        {
            harga_akhir = rumus_harga_diskon(jumlah_beli, diskon, qty_beli_diskon, harga_retail);
        } else
        {
            harga_akhir = rumus_harga_cashback(jumlah_beli, cashback, qty_beli_cashback, harga_retail);
        }

        document.getElementById("jumlah_beli").value = jumlah_beli;
        document.getElementById("display_jumlah_beli").value = jumlah_beli;
        document.getElementById("display_harga_akhir").innerHTML = rupiah(harga_akhir);

        if (jumlah_beli == 1)
        {
            document.getElementById("harga_coret").innerHTML = rupiah(harga_retail);
        } else
        {
            document.getElementById("harga_coret").innerHTML = jumlah_beli + " x " + rupiah(harga_retail) + " = " + rupiah(jumlah_beli * harga_retail);
        }
    }

    function min_beli()
    {
        var jumlah_beli = document.getElementById("jumlah_beli").value;
        var harga_retail = document.getElementById("harga_retail").value;
        var diskon = document.getElementById("diskon").value;
        var qty_beli_diskon = document.getElementById("qty_beli_diskon").value;
        var cashback = document.getElementById("cashback").value;
        var qty_beli_cashback = document.getElementById("qty_beli_cashback").value;

        jumlah_beli = parseInt(jumlah_beli);
        jumlah_beli--;
        if (jumlah_beli == 0)
            return;

        if (diskon != '0')
        {
            harga_akhir = rumus_harga_diskon(jumlah_beli, diskon, qty_beli_diskon, harga_retail);
        } else
        {
            harga_akhir = rumus_harga_cashback(jumlah_beli, cashback, qty_beli_cashback, harga_retail);
        }

        document.getElementById("jumlah_beli").value = jumlah_beli;
        document.getElementById("display_jumlah_beli").value = jumlah_beli;
        document.getElementById("display_harga_akhir").innerHTML = rupiah(harga_akhir);

        if (jumlah_beli == 1)
        {
            document.getElementById("harga_coret").innerHTML = rupiah(harga_retail);
        } else
        {
            document.getElementById("harga_coret").innerHTML = jumlah_beli + " x " + rupiah(harga_retail) + " = " + rupiah(jumlah_beli * harga_retail);
        }
    }

    function btn_beli_klik()
    {
        document.getElementById("form_beli").submit();
    }
</script>
@stop
