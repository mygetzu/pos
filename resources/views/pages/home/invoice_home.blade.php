@extends('layouts.master_home2')
@section('content')
@include('includes.base_function')
<input type="hidden" name="base_path" id="base_path" value="{{ asset('/') }}">
<div class="col-sm-12" align="center" style="margin-bottom:30px">
    <h2 style="border-bottom: 3px solid; display:inline; color:#808080">Review Pesanan</h2>
</div>
<div class="col-sm-12">
@if(Session::has('message'))
<div class="alert alert-danger flat alert-dismissable" style="margin-left: 0px;">
    <i class="fa fa-ban"></i>
    {{ Session::get('message') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<div class="container">
    <style type="text/css">

    #grid1 {
        float: left;
        text-align: left;
        margin-right: 40px;
        font-weight: bold;
    }

    #grid2 {
        float: left;
    }

    #grid2 span {
        color: #000;
        text-align: right;
        width: 72px;
        margin-right: 20px;
        display: inline-block;
        font-size: 0.8em;
        font-weight: bold;
    }

    #grid3 {
        float: right;
    }

    #grid3 span {
        color: #000;
        text-align: right;
        width: 80px;
        margin-right: 20px;
        display: inline-block;
        font-size: 0.8em;
        font-weight: bold;
    }

     #grid4 {
        float: right;
        text-align: right;
    }

    #grid4 span {
        color: #000;
        text-align: left;
        width: 150px;
        display: inline-block;
        font-size: 10pt;
        font-weight: bold;
    }

    #grid2 div,
    #grid1 div {
        white-space: nowrap;        
    }

    .table-produk {
        width: 100%;
        border-collapse: collapse;
        margin-bottom: 20px;
        border: 1px solid black;
        border-padding:30px
    }

    .table-produk th {
        padding: 5px 20px;
        color: #5D6975;
        border-bottom: 1px solid #C1CED9;
        white-space: nowrap;        
        font-weight: bold;
        text-align: center;
        border: 1px solid black;
        font-size:10pt;
    }

    .table-produk td {
        text-align: left;
        vertical-align: top;
        border: 1px solid black;
        padding: 10px;
        font-size:10pt;
    }
</style>
<?php $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'); 
                          $date1 = new DateTime($invoice->tanggal_invoice);
                          $date2 = new DateTime($invoice->jatuh_tempo); 
?>
        <header>
            <h1>INVOICE</h1>
            <div id="grid1" class="clearfix">
                <div>{{ $perusahaan->nama }}</div>
                <div>{{ $perusahaan->alamat }}, {{ ucwords(strtolower($perusahaan->kota->nama)) }}</div>
                <div>{{ str_replace('_', '', $perusahaan->telp) }}</div>
                <div>{{ $perusahaan->email }}</div>
            </div>
            <div id="grid2">
                <div><span>NO TRANSAKSI</span> #{{ $invoice->invoice_no }}</div>
                <div><span>TANGGAL</span> {{ $date1->format('d') }} {{ $bulan[(int)$date1->format('m')] }} {{ $date1->format('Y') }}</div>
                <div><span>PELANGGAN</span> {{ $pelanggan->nama }}</div>
                <div><span>ALAMAT</span> {{ $pelanggan->alamat }}, {{ ucwords(strtolower($pelanggan->kota->nama)) }}</div>
            </div>
            <div id="grid3">
                <!-- <div><span>JATUH TEMPO</span> {{ $date2->format('d') }} {{ $bulan[(int)$date2->format('m')] }} {{ $date2->format('Y') }}</div> -->
                <div><span>METODE BAYAR</span> <?php echo ucwords(str_replace('_', '', $bayar_invoice->metode)); ?></div>
            </div>
        </header>
        <table class="table-produk">
            <thead>
                <tr>
                    <th>NO</th>
                    <th>PRODUK</th>
                    <th>HARGA</th>
                    <th>JUMLAH</th>
                    <th>TOTAL</th>
                </tr>
            </thead>
            <tbody>
                <?php $i=1; $total=0;?>
                @foreach($so_detail as $val)
                <tr>
                    <td style="text-align:center">{{ $i++ }}</td>
                    <td>{{ $val->produk->nama }}</td>
                    <td style="text-align:right" style="padding-right:20px">{{ rupiah($val->harga) }}</td>
                    <td style="text-align:center">{{ $val->quantity }}</td>
                    <td style="text-align:right" style="padding-right:20px">{{  rupiah($val->harga*$val->quantity) }}</td>
                    <?php $total=$total+($val->harga*$val->quantity) ?>
                </tr>
                @endforeach
            </tbody>
        </table>
        <table style="font-size:10pt" width="100%">
            <tbody>
                <tr>
                    <td width="30%" valign="top">
                        <div><b>Catatan :</b></div>
                        <div>{{ $so_header->catatan }}</div><br>
                        <div><b>Terbilang :</b></div>
                        <div></div>
                    </td>
                    <td width="30%" valign="top">Jumlah Item : {{ count($so_detail) }}</td>
                    <td width="40%" valign="top">
                        <table style="font-size:10pt" width="100%">
                            <tbody>
                                <tr style="text-align:right">
                                    <td valign="top">
                                        <div><b>Total Akhir</b></div>
                                    </td>
                                    <td>:</td>
                                    <td width="50%" valign="top">
                                        {{ rupiah($total) }}
                                    </td>
                                </tr>
                                <tr style="text-align:right">
                                    <td valign="top">
                                        <div><b>Uang Muka</b></div>
                                    </td>
                                    <td>:</td>
                                    <td width="50%" valign="top">
                                        {{ rupiah(0) }}
                                    </td>
                                </tr>
                                <tr style="text-align:right">
                                    <td valign="top">
                                        <div><b>Tunai</b></div>
                                    </td>
                                    <td>:</td>
                                    <td width="50%" valign="top">
                                        {{ rupiah(0) }}
                                    </td>
                                </tr>
                                <tr style="text-align:right">
                                    <td valign="top">
                                        <div><b>Kartu Debit</b></div>
                                    </td>
                                    <td>:</td>
                                    <td width="50%" valign="top">
                                        {{ rupiah(0) }}
                                    </td>
                                </tr>
                                <tr style="text-align:right">
                                    <td valign="top">
                                        <div><b>Kartu Kredit</b></div>
                                    </td>
                                    <td>:</td>
                                    <td width="50%" valign="top">
                                        {{ rupiah(0) }}
                                    </td>
                                </tr>
                                <tr style="text-align:right">
                                    <td valign="top">
                                        <div><b>Kembali</b></div>
                                    </td>
                                    <td>:</td>
                                    <td width="50%" valign="top">
                                        {{ rupiah(0) }}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        <br>
                <div class="row no-print">
                    <div class="col-xs-12">
                        <a href="" target="_blank" class="btn bg-orange flat"><i class="fa fa-print"></i> Cetak</a>
                        <a href="" target="_blank" class="btn btn-info pull-right flat" style="margin-right: 5px;"><i class="fa fa-download"></i> Unduh PDF</a>
                    </div>
                 </div>
            </div>
</div>
<br><br>
@stop