@extends('layouts.master_home2')
@section('content')
@include('includes.base_function')
<input type="hidden" name="base_path" id="base_path" value="{{ asset('/') }}">
<div class="col-sm-12" align="center" style="margin-bottom:30px">
    <h2 class="konten-title">Lacak Pesanan</h2>
    <div class="row">
        <div class="col-md-12">
            <img class="img-responsive" alt="Service Center Indonusa Solutama" src="{{ asset('/img/lacak.jpg') }}"/>
        </div>
    </div>
    <div class="row" style="margin-top: 35px">
        <form class="form-horizontal" action="{{ url('lacak_pesanan') }}" method="GET">
            <div class="box-body">
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 control-label">No. Invoice</label>

                    <div class="col-sm-8">
                        <input name="no_invoice" type="text" class="form-control" id="inputEmail3" placeholder="Kode Invoice Anda" style="width: 60%; float: left" <?php echo ((Input::get('no_invoice') !== NULL)? 'value="'.strtoupper(strtolower(Input::get('no_invoice'))).'"' : '' ) ?>/>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <button type="submit" class="btn btn-info">Lacak Pesanan Saya</button>
            </div>
        </form>
        @if($search == FALSE)
        @else
            @if($result == FALSE)
            <div class="col-md-8 col-md-push-2">
                <div class="box box-warning box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Pesan</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        Maaf, Transaksi dengan kode invoice Anda tidak dapat ditemukan. <br/>
                        Mohon periksa kembali kode invoice Anda.
                    </div>
                </div>
            </div>
            @else
                @foreach($result as $r_result)
                <div class="col-md-12">
                    <div class="col-md-4" style="text-align: right;">
                        <p>Pengiriman Via</p>
                    </div>
                    <div class="col-md-8" style="text-align: left;">
                        <p>: {{ $r_result->kirim_via }}</p>
                    </div>
                    <div class="col-md-4" style="text-align: right;">
                        <h4>No. Resi</h4>
                    </div>
                    <div class="col-md-8" style="text-align: left;">
                        <h4>: {{ $r_result->no_resi }}</h4>
                    </div>
                    <div class="col-md-4" style="text-align: right;">
                        <p>Foto Resi</p>
                    </div>
                    <div class="col-md-8" style="text-align: left;">
                        <img class="img-responsive" alt="Foto Resi {{ $r_result->no_resi }}" src="{{ url('img/'.$r_result->foto_resi) }}" style="max-width: 80%"/>
                    </div>
                </div>
                @endforeach
            @endif
        @endif
    </div>
</div>
@stop