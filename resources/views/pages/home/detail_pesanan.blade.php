@extends('layouts.master_home2')
@section('content')
@include('includes.base_function')
<div class="col-md-12"> 
    <h2 class="konten-title">Detail Pesanan #{{ $no_nota }}</h2>
    <a href="{{ url('/pesanan') }}">
        <button class='btn btn-default flat pull-right'><span class='fa fa-arrow-left'></span> kembali</button>
    </a>
    <br><br><br><br>
    <table class="table table-striped table-bordered" id="mydatatables">
        <thead>
            <tr>
                <th style="text-align:center">NO</th>
                <th style="text-align:center">NAMA</th>
                <th style="text-align:center">SATUAN</th>
                <th style="text-align:center">HARGA</th>
                <th style="text-align:center">JUMLAH</th>
                <th style="text-align:center">TOTAL</th>
            </tr>
        </thead>
        <tbody>
            @foreach($so_detail as $i => $val)
            <tr>
                <td align="center">{{ $i+1 }}</td>
                <td align="left">{{ $val['nama'] }}</td>
                <td align="center">{{ $val['satuan'] }}</td>
                <td align="right">{{ rupiah($val['harga']) }}</td>
                <td align="center">{{ $val['quantity'] }}</td>
                <td align="right">{{ rupiah($val['quantity']*$val['harga']) }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div> 
@stop