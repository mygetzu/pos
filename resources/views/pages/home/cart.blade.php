@extends('layouts.master_home2')
@section('content')
@include('includes.base_function')
<div class="col-md-12">
    <input type="hidden" name="base_path" id="base_path" value="{{ asset('/') }}">
    <h2 class="konten-title">Keranjang Belanja</h2>
    @if(Session::has('message'))
    <div class="alert alert-danger flat alert-dismissable" style="margin-left: 0px;">
        <i class="fa fa-ban"></i>
        {{ Session::get('message') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <table class="table table-stripped">
        <thead>
            <tr>
                <th width="5%" style="text-align:center">NO</th>
                <th style="text-align:center">NAMA PRODUK</th>                    
                <th style="text-align:center">HARGA</th>
                <th width="10%" style="text-align:center">JUMLAH</th>
                <th style="text-align:center">TOTAL HARGA</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php $i = 1; $jumlah=0; $total_bayar=0; $row=1; ?>
            <form action="{{ url('/home_ubah_cart') }}" method="post" autocomplete="off" id="home_ubah_cart">
            {{ csrf_field() }}
            @foreach($cart_content as $cart)
            <tr>
                <td align="center">{{ $i++ }}<input type="hidden" name="rowid" value="{{ $cart->rowId }}">
                <input type="hidden" name="id{{ $row }}" value="{{ $cart->id }}"></td>
                <td>@if($cart->name === 'voucher')
                        <b>{{ $cart->name }}</b>
                    @else
                        {{ $cart->name }} 
                        @if(strpos($cart->id, 'DIS') === 0) 
                            <b>(*diskon)</b> 
                        @elseif(strpos($cart->id, 'CAS') === 0) 
                            <b>(*cashback)</b> 
                        @elseif(strpos($cart->id, 'PKT') === 0) 
                            <b>(*paket)</b> 
                        @elseif(strpos($cart->id, 'HDH') === 0) 
                            <b>(*hadiah)</b>
                        @endif
                    @endif
                </td>
                <td align="right" style="padding-right:20px">{{ rupiah($cart->price) }}</td>
                <td align="center">
                    @if(substr($cart->id, 0, 3) == "HDH")
                        {{ $cart->qty }}
                    @else
                        <input type="text" name="jumlah{{ $row }}" id="jumlah" class="form-control" value="{{ $cart->qty }}" style="text-align:right"> 
                    @endif
                </td>
                <td align="right" style="padding-right:20px">{{ rupiah($cart->price*$cart->qty) }}</td>
                <td align="center">
                    <a href="{{ url('/home_hapus_cart/'.$cart->rowId) }}" class="btn btn-danger flat"><span class="glyphicon glyphicon-trash"></span> Hapus</a>
                </td>
            </tr>
              <?php 
                $jumlah += $cart->qty;
                $total_bayar += ($cart->price*$cart->qty);
                $row++;
              ?>
            @endforeach
            <input type="hidden" name="total" value="{{ $row }}">
            </form>
            <tr>
                <td></td>
                <td><p><b>Jumlah Produk : {{ $jumlah }}</b></p></td>
            </tr>
            <tr>
                <td></td>
                <td><p><b>Total Bayar : {{ rupiah($total_bayar) }}</b></p></td>
            </tr>
        </tbody>
    </table>
    <br>
    <a href="{{ url('/') }}" class="btn btn-info flat"><span class="glyphicon glyphicon-shopping-cart"></span> Lanjutkan Belanja</a>
    <button class="btn btn-warning flat" onclick="btn_home_ubah_cart()"><span class="glyphicon glyphicon-refresh"></span> Update</button>
    @if($jumlah > 0)
        <a href="{{ url('/checkout/data_pengiriman') }}" class="btn btn-success flat pull-right"><span class="fa fa-arrow-right"></span> Checkout</a>
    @endif
    <br>
    <br>  
</div>
<script type="text/javascript">
    function btn_home_ubah_cart()
    {
        document.getElementById("home_ubah_cart").submit();
    }
</script>
@stop