@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<input type="hidden" name="base_path" id="base_path" value="{{ asset('/') }}">
<!-- DATA ASLI -->
<input type="hidden" name="ori_nama" id="ori_nama" value="{{ $perusahaan->nama }}">
<input type="hidden" name="ori_alamat" id="ori_alamat" value="{{ $perusahaan->alamat }}">
<input type="hidden" name="ori_kontak_person" id="ori_kontak_person" value="{{ $perusahaan->kontak_person }}">
<input type="hidden" name="ori_bidang_usaha" id="ori_bidang_usaha" value="{{ $perusahaan->bidang_usaha }}">
<input type="hidden" name="ori_jam_operasional" id="ori_jam_operasional" value="{{ $perusahaan->jam_operasional }}">
<input type="hidden" name="ori_email" id="ori_email" value="{{ $perusahaan->email }}">
<div class="row">
    <div class="col-md-12">
        <div class="nav-tabs-custom box tab-primary flat" style="border-top: 0">
            <ul class="nav nav-tabs" id="myTab">
              <li><a href="#tab_1" data-toggle="tab">Perusahaan</a></li>
              <li><a href="#tab_2" data-toggle="tab">Data Cabang</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane" id="tab_1">
                    <div class="row">
                        <div class='col-md-6'>
                            <div id="ubah_akun" style="display:block">
                                <button class="btn btn-warning flat" onclick="ubah_akun('{{ $perusahaan->kota->provinsi->id }}', '{{ $perusahaan->kota_id }}')"><span class='fa fa-edit'></span> Ubah</button>
                            </div>
                            <div id="ubah_batal" style="display:none">
                                <button class="btn btn-default flat" onclick="ubah_batal()"><span class='fa fa-ban'></span> Batal</button>
                            </div>
                        </div>
                    </div>
                    <br>
                    @if(Session::has('message1'))
                    <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                        <i class="fa fa-check"></i>
                        {{ Session::get('message1') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                    <form class="form-horizontal" method="POST" action="{{ url('/ubah_perusahaan') }}" autocomplete="off">
                        <div>
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{ Auth::user()->id }}">
                            <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                                <label class="col-sm-2 control-label">Nama Perusahaan<b id="ast1"></b></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="nama" id="nama" value="{{ $perusahaan->nama }}" disabled="true" placeholder="Masukkan Nama Perusahaan" style="text-transform:capitalize">
                                    @if ($errors->has('nama'))
                                        <span class="help-block">
                                            <strong>Kolom Nama Perusahaan Wajib Diisi</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label class="col-sm-2 control-label">Email<b id="ast7"></b></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="email" id="email" value="{{ $perusahaan->email }}" disabled="true" placeholder="Masukkan alamat email">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>Kolom Email Wajib Diisi</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('alamat') ? ' has-error' : '' }}">
                                <label class="col-sm-2 control-label">Alamat<b id="ast2"></b></label>
                                <div class="col-sm-8">
                                    <textarea type="text" class="form-control" name="alamat" id="alamat" disabled="true" placeholder="Masukkan Alamat" style="text-transform:capitalize">{{ $perusahaan->alamat }}</textarea>
                                    @if ($errors->has('alamat'))
                                        <span class="help-block">
                                            <strong>Kolom Alamat Wajib Diisi</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('kota') ? ' has-error' : '' }}">
                                <label class="col-sm-2 control-label">Kabupaten/Kota<b id="ast3"></b></label>
                                <div class="col-sm-8" id="kota_saya">
                                    @if(!empty($perusahaan->kota->nama))
                                    <input type="text" class="form-control" value="{{ ucwords(strtolower($perusahaan->kota->nama)) }}" disabled="true" placeholder="Masukkan Kota">
                                        @if ($errors->has('provinsi') AND $errors->has('kota'))
                                            <span class="help-block">
                                                <strong>Pilihan Provinsi Serta Kabupaten/Kota Wajib Diisi</strong>
                                            </span>
                                        @elseif ($errors->has('provinsi'))
                                            <span class="help-block">
                                                <strong>Pilihan Provinsi Wajib Diisi</strong>
                                            </span>
                                        @elseif ($errors->has('kota'))
                                            <span class="help-block">
                                                <strong>Pilihan Kabupaten/Kota Wajib Diisi</strong>
                                            </span>
                                        @endif
                                    @endif
                                </div>
                                <div class="col-md-4" id="edit_kota_saya" style="display:none">
                                    <select class="form-control" id="provinsi" name="provinsi">
                                        <option value="">-- Pilih Provinsi --</option>
                                        @foreach($provinsi as $val)
                                        <option value="{{ $val->kode }}">{{ $val->nama }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('provinsi'))
                                        <span class="help-block">
                                            <strong>Pilihan Provinsi Wajib Diisi</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-md-4" id="edit_kota_saya2" style="display:none">
                                    <select class="form-control" id="kota" name="kota">
                                        
                                    </select>
                                    @if ($errors->has('kota'))
                                        <span class="help-block">
                                            <strong>Pilihan Kabupaten/Kota Wajib Diisi</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('telp') ? ' has-error' : '' }}">
                                <label class="col-sm-2 control-label">Telepon 1<b id="ast4"></b></label>
                                <div class="col-sm-8">
                                    <input style="display:none" type="text" class="form-control" name="telp" id="telp" value="{{ $perusahaan->telp }}" data-mask data-inputmask='"mask": "(6231) -9999999"' placeholder="contoh: (6231) -9999999" style="text-transform:capitalize">
                                    <input style="display:block" type="text" class="form-control" id="display_telp" value="<?php echo str_replace('_', '', $perusahaan->telp); ?>" disabled="true" data-inputmask='"mask": "(6231) -9999999"' placeholder="contoh: (6231) -9999999">
                                    @if ($errors->has('telp'))
                                        <span class="help-block">
                                            <strong>Kolom Telepon Wajib Diisi</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Telepon 2</label>
                                <div class="col-sm-8">
                                    <input style="display:none" type="text" class="form-control" name="telp2" id="telp2" value="{{ $perusahaan->telp2 }}" data-mask placeholder="contoh: (031)-9999999" style="text-transform:capitalize">
                                    <input style="display:block" type="text" class="form-control" id="display_telp2" value="<?php echo str_replace('_', '', $perusahaan->telp2); ?>" disabled="true" placeholder="contoh: (031)-9999999">
                                    @if ($errors->has('telp2'))
                                        <span class="help-block">
                                            <strong>Kolom Telepon Wajib Diisi</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('kontak_person') ? ' has-error' : '' }}">
                                <label class="col-sm-2 control-label">Kontak Person<b id="ast5"></b></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="kontak_person" id="kontak_person" value="{{ $perusahaan->kontak_person }}" disabled="true" placeholder="Masukkan Nama kontak Person" style="text-transform:capitalize">
                                    @if ($errors->has('kontak_person'))
                                        <span class="help-block">
                                            <strong>Kolom Kontak Person Wajib Diisi</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('jam_operasional') ? ' has-error' : '' }}">
                                <label class="col-sm-2 control-label">Jam Operasional<b id="ast6"></b></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="jam_operasional" id="jam_operasional" value="{{ $perusahaan->jam_operasional }}" disabled="true" placeholder="Masukkan keterangan hari dan jam">
                                    @if ($errors->has('jam_oprasional'))
                                        <span class="help-block">
                                            <strong>Kolom Jam Operasional Wajib Diisi</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('bidang_usaha') ? ' has-error' : '' }}">
                                <label class="col-sm-2 control-label">Bidang Usaha<b id="ast8"></b></label>
                                <div class="col-sm-8">
                                    <textarea type="text" class="form-control" name="bidang_usaha" id="bidang_usaha" disabled="true" placeholder="Masukkan Bidang Usaha" style="text-transform:capitalize">{{ $perusahaan->bidang_usaha }}</textarea>
                                    @if ($errors->has('bidang_usaha'))
                                        <span class="help-block">
                                            <strong>Kolom Bidang Usaha Wajib Diisi</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div style="display:none" id="button_submit">
                                <div class="col-sm-8 col-sm-offset-2    ">
                                    <button type="submit" class="btn btn-primary flat" onclick="document.getElementById('loading').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
                                </div>
                            </div>
                        </div>
                        <label id="astket"></label>
                    </form>
                </div>
                <div class="tab-pane" id="tab_2">
                    <a data-toggle="modal" data-target="#tambah_cabang">
                        <button class="btn bg-purple flat" onclick="tambah_cabang('{{ $perusahaan->nama }}')"><span class="fa fa-plus"></span> Tambah Cabang</button>
                    </a>
                    <br><br>
                    @if(Session::has('message2'))
                    <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                        <i class="fa fa-check"></i>
                        {{ Session::get('message2') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                    @if ($errors->has('alamat_cabang'))
                    <div class="alert alert-danger alert-dismissable flat" style="margin-left: 0px;">
                        <i class="fa fa-check"></i>
                        Kolom Alamat Cabang Wajib Diisi
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                    @if ($errors->has('provinsi2'))
                    <div class="alert alert-danger alert-dismissable flat" style="margin-left: 0px;">
                        <i class="fa fa-check"></i>
                        Pilihan Provinsi Wajib Diisi
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                    @if ($errors->has('kota_id2'))
                    <div class="alert alert-danger alert-dismissable flat" style="margin-left: 0px;">
                        <i class="fa fa-check"></i>
                        Pilihan Kabupaten/Kota Wajib Diisi
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                    @if ($errors->has('telp_cabang'))
                    <div class="alert alert-danger alert-dismissable flat" style="margin-left: 0px;">
                        <i class="fa fa-check"></i>
                        Kolom Telepon Cabang Wajib Diisi
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                    @if ($errors->has('kontak_person_cabang'))
                    <div class="alert alert-danger alert-dismissable flat" style="margin-left: 0px;">
                        <i class="fa fa-check"></i>
                        Kolom Kontak Person Cabang Wajib Diisi
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                    <table id="mydatatables" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th style="text-align:center">NO</th>
                                <th style="text-align:center">NAMA CABANG</th>
                                <th style="text-align:center">ALAMAT</th>
                                <th style="text-align:center">KOTA</th>
                                <th style="text-align:center">TELEPON</th>
                                <th style="text-align:center">KONTAK PERSON</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            @foreach($perusahaan->cabang as $val)
                            <tr>
                                <td align="center">{{ $i++ }}</td>
                                <td>{{ $val->nama }}</td>
                                <td>{{ $val->alamat }}</td>
                                <td>{{ ucwords(strtolower($val->kota->nama)) }}</td>
                                <td><?php echo str_replace('_', '', $val->telp); ?></td>
                                <td>{{ $val->kontak_person }}</td>
                                <td align="center">
                                  <a data-toggle="modal" data-target="#ubah_cabang">
                                      <button class="btn btn-warning flat" onclick="ubah_cabang('{{ $val->id }}','{{ $val->nama }}','{{ $val->alamat }}','{{ $val->kota_id }}', '{{ $val->telp }}', '{{ $val->kontak_person }}','{{ $val->kota->provinsi->id }}')" data-toggle="tooltip" title="ubah" style="font-size: 15pt"><span class="fa fa-edit"></span></button>
                                  </a>
                                </td>
                                <td align="center">
                                    <form id="form_hapus{{ $val->id }}" action="{{ url('/hapus_cabang') }}" method="post">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="id_cabang" value="{{ $val->id }}">
                                        <button class="btn btn-danger flat" type="button" onclick="btn_hapus('{{ $val->id }}')" data-toggle="tooltip" title="hapus" style="font-size: 15pt"><span class="fa fa-trash"></span></button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="overlay" id="loading" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="tambah_cabang" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content box">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah Cabang</h4>
            </div>
            <form class="form-horizontal" method="post" action="{{ url('/tambah_cabang') }}" autocomplete="off">
            <div class="modal-body">
                <input type="hidden" name="perusahaan_id" id="perusahaan_id" value="{{ $perusahaan->id }}">
                {{ csrf_field() }}
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama Perusahaan</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="nama2" disabled="true">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama Cabang</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="nama_cabang" placeholder="Masukkan nama cabang" style="text-transform:capitalize">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Alamat Cabang*</label>
                    <div class="col-sm-10">
                        <textarea type="text" class="form-control" name="alamat_cabang" placeholder="Masukkan alamat cabang" style="text-transform:capitalize"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="kota" class="col-md-2 control-label">Kabupaten/Kota*</label>
                    <div class="col-md-5">
                        <select class="form-control" id="provinsi2" name="provinsi2">
                            <option value="">-- Pilih Provinsi --</option>
                            @foreach($provinsi as $val)
                            <option value="{{ $val->kode }}">{{ $val->nama }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('provinsi'))
                            <span class="help-block">
                                <strong>Pilihan Provinsi Wajib Diisi</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-5">
                        <select class="form-control" id="kota_id2" name="kota_id2">
                            <option value="">-- Pilih Kabupaten/Kota --</option>
                        </select>
                        @if ($errors->has('kota_id'))
                            <span class="help-block">
                                <strong>Pilihan Kabupaten/Kota Wajib Diisi</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Telepon*</label>
                    <div class="col-md-4">
                        <div class="input-group" style="margin-bottom:0px">
                            <div class="input-group-addon">
                                <i class="fa fa-phone"></i>
                            </div>
                            <input type="text" class="form-control" data-mask placeholder="contoh: 0321-9999999" name="telp_cabang" value="{{ old('telp') }}">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Kontak Person*</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="kontak_person_cabang" placeholder="Masukkan kontak person" style="text-transform:capitalize">
                    </div>
                </div>
                <p>Keterangan (*) : Wajib Diisi</p>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default flat pull-left"><span class="glyphicon glyphicon-ban-circle"></span> Batal</button>
                <button type="submit" class="btn btn-primary flat" onclick="document.getElementById('loading2').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
            </div>
            </form>
            <div class="overlay" id="loading2" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="ubah_cabang" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content box">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ubah Cabang</h4>
            </div>
            <form class="form-horizontal" method="post" action="{{ url('/ubah_cabang') }}" autocomplete="off">
            <div class="modal-body">
                <input type="hidden" name="perusahaan_id" id="perusahaan_id" value="{{ $perusahaan->id }}">
                <input type="hidden" name="ubah_id_cabang" id="ubah_id_cabang" value="{{ $perusahaan->id }}">
                {{ csrf_field() }}
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nama Perusahaan</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="nama3" disabled="true" value="{{ $perusahaan->nama }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nama Cabang*</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="ubah_nama_cabang" id="ubah_nama_cabang" placeholder="Masukkan nama cabang" style="text-transform:capitalize">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Alamat Cabang*</label>
                        <div class="col-sm-10">
                            <textarea type="text" class="form-control" name="ubah_alamat_cabang" id="ubah_alamat_cabang" placeholder="Masukkan alamat cabang" style="text-transform:capitalize"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="kota" class="col-md-2 control-label">Kabupaten/Kota*</label>
                        <div class="col-md-5">
                            <select class="form-control" id="provinsi3" name="provinsi3">
                                <option value="">-- Pilih Provinsi --</option>
                                @foreach($provinsi as $val)
                                <option value="{{ $val->kode }}">{{ $val->nama }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('provinsi'))
                                <span class="help-block">
                                    <strong>Pilihan Provinsi Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                        <div class="col-md-5">
                            <select class="form-control" id="kota_id3" name="kota_id3">
                                <option value="">-- Pilih Kabupaten/Kota --</option>
                            </select>
                            @if ($errors->has('kota_id'))
                                <span class="help-block">
                                    <strong>Pilihan Kabupaten/Kota Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Telepon*</label>
                        <div class="col-md-4">
                            <div class="input-group" style="margin-bottom:0px">
                                <div class="input-group-addon">
                                    <i class="fa fa-phone"></i>
                                </div>
                                <input type="text" class="form-control" data-mask placeholder="contoh: 0321-9999999" name="ubah_telp_cabang" id="ubah_telp_cabang" value="{{ old('telp') }}" placeholder="Masukkan Telepon">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Kontak Person*</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="ubah_kontak_person_cabang" id="ubah_kontak_person_cabang" placeholder="Masukkan kontak person" style="text-transform:capitalize">
                        </div>
                    </div>
                </div>
                <p>Keterangan (*) : Wajib Diisi</p>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default pull-left flat"><span class="glyphicon glyphicon-ban-circle"></span> Batal</button>
                <button type="submit" class="btn btn-primary flat" onclick="document.getElementById('loading3').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
            </div>
            </form>
            <div class="overlay" id="loading3" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

@stop

<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<!-- jQuery 2.2.0 -->
<script src="{{ asset('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('/adminlte/plugins/select2/select2.full.min.js') }}"></script>
<!-- InputMask -->
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();
    //Money Euro
    $("[data-mask]").inputmask({ mask: "(999[9])-9999999", greedy: false ,
        definitions: {
                '*': {
                validator: "[0-9-]",
                cardinality: 1,
                casing: "lower"
            }
        }
    });

    $('[data-toggle="tooltip"]').tooltip(); 
  });
</script>
<script>
  $(function () {
    $("#mydatatables").DataTable();
  });
</script>

<script type="text/javascript">
    function tambah_cabang(nama)
    {
        document.getElementById('nama2').value = nama;
    }
    function ubah_cabang(id,nama_cabang,alamat,kota_id,telp,kontak_person,provinsi_id)
    {
        document.getElementById('ubah_id_cabang').value     = id;
        document.getElementById('ubah_nama_cabang').value   = nama_cabang;
        document.getElementById('ubah_alamat_cabang').value = alamat;
        document.getElementById('ubah_telp_cabang').value   = telp;
        document.getElementById('ubah_kontak_person_cabang').value = kontak_person;
        document.getElementById("provinsi3").selectedIndex  = provinsi_id;
        $("#kota_id3").html("");
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })

        var kode_provinsi = document.getElementById('provinsi3').value;
        var path =  document.getElementById('base_path').value;
        var url = path+"get_kota2/"+kode_provinsi;

        $.ajax({
            type: "POST",
            url: url,
            dataType: 'json',
            success: function (data) {
                var my_obj = Object.keys(data).length;
                var my_index = 0;

                for (var i = 0; i < my_obj; i++) {
                    var z = document.createElement("option");
                    $.each(data[i], function(key,value){
                        if(key=='id'){
                         z.setAttribute("value", value);
                        }
                        else if(key == 'nama')
                        {
                            var t = document.createTextNode(value);
                            z.appendChild(t);
                        }
                        if(key == 'id' && value == kota_id)
                        { 
                            my_index = i;
                        }
                    });
                    document.getElementById("kota_id3").appendChild(z);
                }
                document.getElementById("kota_id3").selectedIndex = my_index;
            },
            error: function (data) {
                alert('ooo');
            }
        });
    }

    function ubah_akun(provinsi_id, kota_id)
    {
        document.getElementById("nama").disabled                = false;
        document.getElementById("alamat").disabled              = false;
        document.getElementById("kontak_person").disabled       = false;
        document.getElementById("bidang_usaha").disabled        = false;
        document.getElementById("jam_operasional").disabled     = false;
        document.getElementById("email").disabled               = false;

        document.getElementById("telp").style.display           = 'block';
        document.getElementById("display_telp").style.display   = 'none';

        document.getElementById("telp2").style.display           = 'block';
        document.getElementById("display_telp2").style.display   = 'none';
        
        document.getElementById("ubah_akun").style.display      = 'none';
        document.getElementById("ubah_batal").style.display     = 'block';
        document.getElementById("button_submit").style.display  = 'block';

        document.getElementById("astket").innerHTML = "Keterangan (*) : Wajib Diisi";
        document.getElementById("ast1").innerHTML = "*";
        document.getElementById("ast2").innerHTML = "*";
        document.getElementById("ast3").innerHTML = "*";
        document.getElementById("ast4").innerHTML = "*";
        document.getElementById("ast5").innerHTML = "*";
        document.getElementById("ast6").innerHTML = "*";
        document.getElementById("ast7").innerHTML = "*";
        document.getElementById("ast8").innerHTML = "*";

        document.getElementById("kota_saya").style.display = 'none';
        document.getElementById("edit_kota_saya").style.display = 'block';
        document.getElementById("edit_kota_saya2").style.display = 'block';

        document.getElementById("provinsi").selectedIndex = provinsi_id;
        $("#kota").html("");

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })

        var kode_provinsi   = document.getElementById('provinsi').value;
        var path            =  document.getElementById('base_path').value;
        var url             = path+"get_kota2/"+kode_provinsi;
        
        $.ajax({
            type: "POST",
            url: url,
            dataType: 'json',
            success: function (data) {
                var my_obj = Object.keys(data).length;
                var my_index = 0;

                for (var i = 0; i < my_obj; i++) {
                    var z = document.createElement("option");
                    $.each(data[i], function(key,value){
                        if(key=='id'){
                            z.setAttribute("value", value);
                        }
                        else if(key == 'nama')
                        {
                            var t = document.createTextNode(value);
                            z.appendChild(t);
                        }
                        if(key == 'id' && value == kota_id)
                        { 
                            my_index = i;
                        }
                    });
                    document.getElementById("kota").appendChild(z);
                }
                document.getElementById("kota").selectedIndex = my_index;
            },
            error: function (data) {
                alert('ooo');
            }
        });
    }

    function ubah_batal()
    {
        document.getElementById("nama").value       = document.getElementById("ori_nama").value;
        document.getElementById("alamat").value      = document.getElementById("ori_alamat").value;
        document.getElementById("kontak_person").value     = document.getElementById("ori_kontak_person").value;
        document.getElementById("bidang_usaha").value   = document.getElementById("ori_bidang_usaha").value;
        document.getElementById("jam_operasional").value   = document.getElementById("ori_jam_operasional").value;
        document.getElementById("email").value   = document.getElementById("ori_email").value;

        document.getElementById("nama").disabled     = true;
        document.getElementById("alamat").disabled              = true;
        document.getElementById("kontak_person").disabled       = true;
        document.getElementById("bidang_usaha").disabled        = true;
        document.getElementById("jam_operasional").disabled        = true;
        document.getElementById("email").disabled        = true;

        document.getElementById("telp").style.display           = 'none';
        document.getElementById("display_telp").style.display   = 'block';

        document.getElementById("telp2").style.display           = 'none';
        document.getElementById("display_telp2").style.display   = 'block';

        document.getElementById("ubah_akun").style.display = 'block';
        document.getElementById("ubah_batal").style.display = 'none';
        document.getElementById("button_submit").style.display = 'none';

        document.getElementById("kota_saya").style.display = 'block';
        document.getElementById("edit_kota_saya").style.display = 'none';
        document.getElementById("edit_kota_saya2").style.display = 'none';

        document.getElementById("astket").innerHTML = "";
        document.getElementById("ast1").innerHTML = "";
        document.getElementById("ast2").innerHTML = "";
        document.getElementById("ast3").innerHTML = "";
        document.getElementById("ast4").innerHTML = "";
        document.getElementById("ast5").innerHTML = "";
        document.getElementById("ast6").innerHTML = "";
        document.getElementById("ast7").innerHTML = "";
        document.getElementById("ast8").innerHTML = "";
    }
    function btn_hapus(id)
    {
        var cek = confirm("Apakah Anda Yakin?");

        if(cek)
        {
            document.getElementById('loading').style.display='block';
            document.getElementById("form_hapus".concat(id)).submit();
        }
    }

    $(document).ready(function(){
          $('#provinsi').change(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            var kode_provinsi = document.getElementById('provinsi').value;
            if(kode_provinsi == "")
                kode_provinsi = "KOSONG"
            var path =  document.getElementById('base_path').value;
            var url = path+"get_kota/"+kode_provinsi;
            
            $.ajax({
                type: "POST",
                url: url,
                data: $("#form_provinsi").serialize(),
                success: function (data) {
                    $("#kota").html(data);
                },
                error: function (data) {
                    alert('ooo');
                }
            });
          });

          $('#provinsi2').change(function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            var kode_provinsi = document.getElementById('provinsi2').value;
            if(kode_provinsi == "")
            {
                kode_provinsi = "ALL";
            }
            var path =  document.getElementById('base_path').value;
            var url = path+"get_kota/"+kode_provinsi;

            $.ajax({
                type: "POST",
                url: url,
                success: function (data) {
                    $("#kota_id2").html(data);
                },
                error: function (data) {
                    alert('ooo');
                }
            });
          });

          $('#provinsi3').change(function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            var kode_provinsi = document.getElementById('provinsi3').value;
            if(kode_provinsi == "")
            {
                kode_provinsi = "ALL";
            }
            var path =  document.getElementById('base_path').value;
            var url = path+"get_kota/"+kode_provinsi;

            $.ajax({
                type: "POST",
                url: url,
                success: function (data) {
                    $("#kota_id3").html(data);
                },
                error: function (data) {
                    alert('ooo');
                }
            });
          });

           // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // save the latest tab; use cookies if you like 'em better:
                localStorage.setItem('lastTab', $(this).attr('href'));
            });

            // go to the latest tab, if it exists:
            var lastTab = localStorage.getItem('lastTab');
            if (lastTab) {
                $('[href="' + lastTab + '"]').tab('show');
            }
            else
            {
                lastTab = "#tab_1";
                $('[href="' + lastTab + '"]').tab('show');
            }
      });
</script>