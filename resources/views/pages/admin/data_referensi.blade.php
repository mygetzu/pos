@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="nav-tabs-custom box tab-primary flat" style="border-top: 0">
            <ul class="nav nav-tabs" id="myTab">
                <li><a href="#tab_1" data-toggle="tab">Format Nomor</a></li>
                <li><a href="#tab_2" data-toggle="tab">Metode Pembayaran</a></li>
                <li><a href="#tab_3" data-toggle="tab">Metode Pengiriman</a></li>
                <li><a href="#tab_4" data-toggle="tab">Bank</a></li>
                <li><a href="#tab_5" data-toggle="tab">Syarat & Ketentuan</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane" id="tab_1">
                    @if(Session::has('message1'))
                    <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                        <i class="fa fa-check"></i>
                        {{ Session::get('message1') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                    <table id="" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th style="text-align:center">NO</th>
                                <th style="text-align:center">NAMA</th>
                                <th style="text-align:center">FORMAT</th>
                                <th style="text-align:center"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            @foreach($nomor_invoice as $val)
                            <tr>
                                <td align="center">{{ $i++ }}</td>
                                <td>{{ $val->nama }}</td>
                                <td>{{ $val->format }}</td>
                                <td align="center">
                                    <a data-toggle='modal' data-target='#ubah_nomor_invoice'>
                                        <button class="btn btn-warning flat" onclick="ubah_nomor_invoice('{{ $val->id }}', '{{ $val->nama }}', '{{ $val->format }}')" data-toggle="tooltip" title="ubah" style="font-size: 15pt"><span class="fa fa-edit"></span></button>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                            @foreach($gudang as $val)
                            <tr>
                                <td align="center">{{ $i++ }}</td>
                                <td>Mutasi Gudang : {{ $val->nama }}</td>
                                <td>{{ $val->format_no_mutasi }}</td>
                                <td align="center">
                                    <a data-toggle='modal' data-target='#ubah_nomor_mutasi_gudang'>
                                        <button class="btn btn-warning flat" onclick="ubah_nomor_mutasi_gudang('{{ $val->id }}', '{{ $val->nama }}', '{{ $val->format_no_mutasi }}')" data-toggle="tooltip" title="ubah" style="font-size: 15pt"><span class="fa fa-edit"></span></button>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="tab_2">
                    <a data-toggle='modal' data-target='#tambah_metode_pembayaran'>
                        <button class='btn bg-purple flat'><span class='fa fa-plus'></span> Tambah Metode Pembayaran</button>
                    </a>
                    <br><br>
                    @if(Session::has('message2'))
                    <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                        <i class="fa fa-check"></i>
                        {{ Session::get('message2') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                    @if ($errors->has('nama'))
                    <div class="alert alert-danger alert-dismissable flat" style="margin-left: 0px;">
                        <i class="fa fa-ban"></i>
                        {{ $errors->first('nama') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                    @if ($errors->has('jenis'))
                    <div class="alert alert-danger alert-dismissable flat" style="margin-left: 0px;">
                        <i class="fa fa-ban"></i>
                        {{ $errors->first('jenis') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                    <table id="mydatatables2" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th style="text-align:center">NO</th>
                                <th style="text-align:center">NAMA</th>
                                <th style="text-align:center">JENIS</th>
                                <th style="text-align:center">KETERANGAN</th>
                                <th style="text-align:center">GAMBAR</th>
                                <th style="text-align:center"></th>
                                <th style="text-align:center"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            @foreach($metode_pembayaran as $val)
                            <tr>
                                <td align="center">{{ $i++ }}</td>
                                <td>{{ $val->nama }}</td>
                                <td>
                                    @if($val->jenis === 'tunai')
                                    Tunai
                                    @elseif($val->jenis === 'non_tunai')
                                    Non Tunai
                                    @endif
                                </td>
                                <td>{{ $val->keterangan }}</td>
                                <td align="center">
                                    <img src="{{ asset('/img/ref/'.$val->file_gambar) }}" style="height: 40px; width: auto;">
                                </td>
                                <td align="center">
                                    <label class="switch">
                                        <input type="checkbox" onchange='window.location.href ="{{ url('/ubah_status_metode_pembayaran/'.$val->id) }}"' <?php
                                        if ($val->is_aktif == 1)
                                            echo "checked";
                                        else
                                            echo "";
                                        ?>>
                                        <div class="slider" data-toggle="tooltip" title="status aktif"></div>
                                    </label>
                                </td>
                                <td align="center">
                                    <a data-toggle='modal' data-target='#ubah_metode_pembayaran'>
                                        <button class="btn btn-warning flat" onclick="ubah_metode_pembayaran('{{ $val->id }}', '{{ $val->nama }}', '{{ $val->jenis }}', '{{ $val->keterangan }}')" data-toggle="tooltip" title="ubah" style="font-size: 15pt"><span class="fa fa-edit"></span></button>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="tab_3">
                    <a data-toggle='modal' data-target='#tambah_metode_pengiriman'>
                        <button class='btn bg-purple flat'><span class='fa fa-plus'></span> Tambah Metode Pengiriman</button>
                    </a>
                    <br><br>
                    @if(Session::has('message3'))
                    <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                        <i class="fa fa-check"></i>
                        {{ Session::get('message3') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                    @if ($errors->has('nama'))
                    <div class="alert alert-danger alert-dismissable flat" style="margin-left: 0px;">
                        <i class="fa fa-ban"></i>
                        {{ $errors->first('nama') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                    <table id="mydatatables3" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th style="text-align:center">NO</th>
                                <th style="text-align:center">NAMA</th>
                                <th style="text-align:center">TARIF DASAR</th>
                                <th style="text-align:center">KETERANGAN</th>
                                <th style="text-align:center">GAMBAR</th>
                                <th style="text-align:center">STATUS</th>
                                <th style="text-align:center"></th>
                            </tr>
                        </thead>
                        <tbody>
<?php $i = 1; ?>
                            @foreach($metode_pengiriman as $val)
                            <tr>
                                <td align="center">{{ $i++ }}</td>
                                <td>{{ $val->nama }}</td>
                                <td>{{ rupiah($val->tarif_dasar) }}</td>
                                <td>{{ $val->keterangan }}</td>
                                <td align="center">
                                    <img src="{{ asset('/img/ref/'.$val->file_gambar) }}" style="height: 40px; width: auto;">
                                </td>
                                <td align="center">
                                    <label class="switch">
                                        <input type="checkbox" onchange='window.location.href ="{{ url('/ubah_status_metode_pengiriman/'.$val->id) }}"' <?php
                                        if ($val->is_aktif == 1)
                                            echo "checked";
                                        else
                                            echo "";
                                        ?>>
                                        <div class="slider" data-toggle="tooltip" title="status aktif"></div>
                                    </label>
                                </td>
                                <td align="center">
                                    <a data-toggle='modal' data-target='#ubah_metode_pengiriman'>
                                        <button class="btn btn-warning flat" onclick="ubah_metode_pengiriman('{{ $val->id }}', '{{ $val->nama }}', '{{ $val->tarif_dasar }}', '{{ $val->keterangan }}')" data-toggle="tooltip" title="ubah" style="font-size: 15pt"><span class="fa fa-edit"></span></button>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div> 
                <div class="tab-pane" id="tab_4">
                    <a data-toggle='modal' data-target='#tambah_bank'>
                        <button class='btn bg-purple flat'><span class='fa fa-plus'></span> Tambah Bank</button>
                    </a>
                    <br><br>
                    @if(Session::has('message4'))
                    <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                        <i class="fa fa-check"></i>
                        {{ Session::get('message4') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                    @if ($errors->has('nama'))
                    <div class="alert alert-danger alert-dismissable flat" style="margin-left: 0px;">
                        <i class="fa fa-ban"></i>
                        {{ $errors->first('nama') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                    <table id="mydatatables4" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th style="text-align:center">NO</th>
                                <th style="text-align:center">KODE BANK</th>
                                <th style="text-align:center">NAMA</th>
                                <th style="text-align:center">NAMA SINGKAT</th>
                                <th style="text-align:center">KETERANGAN</th>
                                <th style="text-align:center">GAMBAR</th>
                                <th style="text-align:center">STATUS</th>
                                <th style="text-align:center"></th>
                            </tr>
                        </thead>
                        <tbody>
<?php $i = 1; ?>
                            @foreach($bank as $val)
                            <tr>
                                <td align="center">{{ $i++ }}</td>
                                <td>{{ $val->kode }}</td>
                                <td>{{ $val->nama }}</td>
                                <td>{{ $val->nama_singkat }}</td>
                                <td>{{ $val->keterangan }}</td>
                                <td align="center">
                                    <img src="{{ asset('/img/ref/'.$val->file_gambar) }}" style="height: 40px; width: auto;">
                                </td>
                                <td align="center">
                                    <label class="switch">
                                        <input type="checkbox" onchange='window.location.href ="{{ url('/ubah_status_bank/'.$val->id) }}"' <?php
                                        if ($val->is_aktif == 1)
                                            echo "checked";
                                        else
                                            echo "";
                                        ?>>
                                        <div class="slider" data-toggle="tooltip" title="status aktif"></div>
                                    </label>
                                </td>
                                <td align="center">
                                    <a data-toggle='modal' data-target='#ubah_bank'>
                                        <button class="btn btn-warning flat" onclick="ubah_bank('{{ $val->id }}', '{{ $val->kode }}', '{{ $val->nama }}', '{{ $val->nama_singkat }}', '{{ $val->keterangan }}')" data-toggle="tooltip" title="ubah" style="font-size: 15pt"><span class="fa fa-edit"></span></button>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="tab_5">
                    <h4>Syarat & Ketentuan</h4>
                    <a data-toggle='modal' data-target='#tambah_syarat_ketentuan'>
                        <button class="btn btn-warning btn-flat" data-toggle="tooltip" title="Tambah Syarat & Ketentuan" style="font-size: 15pt"><span class="fa fa-plus"></span> Tambah Syarat & Ketentuan</button>
                    </a>
                    <table id="mydatatables4" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th style="text-align:center">NO</th>
                                <th style="text-align:center">PERIHAL</th>
                                <th style="text-align:center">SYARAT & KETENTUAN</th>
                                <th style="text-align:center"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            @foreach($syarat_ketentuan as $r_sk)
                            <tr>
                                <td align="center">{{ $i++ }}</td>
                                <td>{{ $r_sk->perihal }}</td>
                                <td>{{ $r_sk->konten }}</td>
                                <td align="center">
                                    <a data-toggle='modal' data-target='#ubah_syarat_ketentuan'>
                                        <button class="btn btn-warning flat" onclick="ubah_syarat_ketentuan('{{ $r_sk->id }}', '{{ $r_sk->perihal }}', '{{ $r_sk->konten }}')" data-toggle="tooltip" title="Ubah Syarat & Ketentuan" style="font-size: 15pt"><span class="fa fa-edit"></span></button>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="overlay" id="loading" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="tambah_bank" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content box">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="riwayatheader">Tambah Bank</h4>
            </div>
            <form class="form-horizontal" method="post" action="{{ url('/tambah_bank') }}" autocomplete="off" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Kode*</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="kode" placeholder="Masukkan Kode Bank">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Nama*</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="nama" placeholder="Masukkan Nama Bank" style="text-transform:capitalize">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Nama Singkat*</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="nama_singkat" placeholder="Masukkan Nama Singkat Bank" style="text-transform:capitalize">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Keterangan</label>
                        <div class="col-sm-10">
                            <textarea type="text" class="form-control" name="keterangan" placeholder="Masukkan Keterangan" style="text-transform:capitalize"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Gambar</label>
                        <div class="col-sm-10">
                            <input type="file" name="file_gambar" class="form-control">
                        </div>
                    </div>
                    <p>Keterangan (*) : Wajib Diisi</p>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default pull-left flat"><span class="glyphicon glyphicon-ban-circle"></span> Batal</button>
                    <button type="submit" class="btn btn-primary pull-right flat" onclick="document.getElementById('loading2').style.display = 'block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
                </div>
            </form>
            <div class="overlay" id="loading2" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ubah_nomor_invoice" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content box">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="riwayatheader">Ubah Format Nomor</h4>
            </div>
            <form class="form-horizontal" method="post" action="{{ url('/ubah_nomor_invoice') }}" autocomplete="off">
                <input type="hidden" class="form-control" name="id" id="ubah_nomor_invoice_id">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Nama</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="nama" id="ubah_nomor_invoice_nama" disabled="true">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Format*</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="format" id="ubah_nomor_invoice_format" placeholder="Masukkan Format Nomor" style="text-transform:uppercase;">
                        </div>
                    </div>
                    <p>Keterangan (*) : Wajib Diisi</p>
                    <p>Untuk menambahkan template tanggal: gunakan tag 'DD' (hari), 'MM' (bulan), 'YY' (tahun), 'YYYY' (tahun)</p>
                    <p>Contoh : DD-MM-YYYY</p>
                    <p>Untuk menambahkan template angka: gunakan tag #</p>
                    <p>Contoh template angka 4 digit : ####</p>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default pull-left flat"><span class="glyphicon glyphicon-ban-circle"></span> Batal</button>
                    <button type="submit" class="btn btn-primary pull-right flat" onclick="document.getElementById('loading3').style.display = 'block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
                </div>
            </form>
            <div class="overlay" id="loading3" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="tambah_metode_pembayaran" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content box">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="riwayatheader">Tambah Metode Pembayaran</h4>
            </div>
            <form class="form-horizontal" method="post" action="{{ url('/tambah_metode_pembayaran') }}" autocomplete="off" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Nama*</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="nama" placeholder="Masukkan nama metode pembayaran" style="text-transform:capitalize">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Jenis*</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="jenis">
                                <option value="">-- Pilih Jenis --</option>
                                <option value="tunai">Tunai</option>
                                <option value="non_tunai">Non Tunai</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Keterangan</label>
                        <div class="col-sm-10">
                            <textarea type="text" class="form-control" name="keterangan" placeholder="Masukkan Keterangan" style="text-transform:capitalize"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Gambar</label>
                        <div class="col-sm-10">
                            <input type="file" name="file_gambar" class="form-control">
                        </div>
                    </div>
                    <p>Keterangan (*) : Wajib Diisi</p>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default pull-left flat"><span class="glyphicon glyphicon-ban-circle"></span> Batal</button>
                    <button type="submit" class="btn btn-primary pull-right flat" onclick="document.getElementById('loading4').style.display = 'block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
                </div>
            </form>
            <div class="overlay" id="loading4" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ubah_metode_pembayaran" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content box">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="riwayatheader">Ubah Metode Pembayaran</h4>
            </div>
            <form class="form-horizontal" method="post" action="{{ url('/ubah_metode_pembayaran') }}" autocomplete="off" enctype="multipart/form-data">
                <input type="hidden" name="id" id="ubah_metode_pembayaran_id">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Nama*</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="nama" id="ubah_metode_pembayaran_nama" placeholder="Masukkan Nama Bank" style="text-transform:capitalize">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Jenis*</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="jenis" id="ubah_metode_pembayaran_jenis">
                                <option value="">-- Pilih Jenis --</option>
                                <option value="tunai">Tunai</option>
                                <option value="non_tunai">Non Tunai</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Keterangan</label>
                        <div class="col-sm-10">
                            <textarea type="text" class="form-control" name="keterangan" id="ubah_metode_pembayaran_keterangan" placeholder="Masukkan Keterangan" style="text-transform:capitalize"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Gambar</label>
                        <div class="col-sm-10">
                            <input type="file" name="file_gambar" class="form-control">
                        </div>
                    </div>
                    <p>Keterangan (*) : Wajib Diisi</p>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default pull-left flat"><span class="glyphicon glyphicon-ban-circle"></span> Batal</button>
                    <button type="submit" class="btn btn-primary pull-right flat" onclick="document.getElementById('loading5').style.display = 'block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
                </div>
            </form>
            <div class="overlay" id="loading5" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="tambah_metode_pengiriman" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content box">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="riwayatheader">Tambah Metode Pengiriman</h4>
            </div>
            <form class="form-horizontal" method="post" action="{{ url('/tambah_metode_pengiriman') }}" autocomplete="off" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Nama*</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="nama" placeholder="Masukkan nama metode pengiriman" style="text-transform:capitalize">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Tarif dasar</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="tarif_dasar" placeholder="Masukkan tarif dasar" id="tarif_dasar" data-thousands="." data-decimal="," style="text-align:right" value="{{ old('tarif_dasar') }}" />
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">(IDR)</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Keterangan</label>
                        <div class="col-sm-10">
                            <textarea type="text" class="form-control" name="keterangan" placeholder="Masukkan Keterangan" style="text-transform:capitalize"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Gambar</label>
                        <div class="col-sm-10">
                            <input type="file" name="file_gambar" class="form-control">
                        </div>
                    </div>
                    <p>Keterangan (*) : Wajib Diisi</p>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default pull-left flat"><span class="glyphicon glyphicon-ban-circle"></span> Batal</button>
                    <button type="submit" class="btn btn-primary pull-right flat" onclick="document.getElementById('loading6').style.display = 'block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
                </div>
            </form>
            <div class="overlay" id="loading6" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ubah_metode_pengiriman" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content box">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="riwayatheader">Ubah Metode Pengiriman</h4>
            </div>
            <form class="form-horizontal" method="post" action="{{ url('/ubah_metode_pengiriman') }}" autocomplete="off" enctype="multipart/form-data">
                <input type="hidden" name="id" id="ubah_metode_pengiriman_id">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Nama*</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="nama" id="ubah_metode_pengiriman_nama" placeholder="Masukkan Nama Bank" style="text-transform:capitalize">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Tarif dasar</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="tarif_dasar" placeholder="Masukkan tarif dasar" id="ubah_metode_pengiriman_tarif_dasar" data-thousands="." data-decimal="," style="text-align:right" value="{{ old('ubah_metode_pengiriman_tarif_dasar') }}" />
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">(IDR)</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Keterangan</label>
                        <div class="col-sm-10">
                            <textarea type="text" class="form-control" name="keterangan" id="ubah_metode_pengiriman_keterangan" placeholder="Masukkan Keterangan" style="text-transform:capitalize"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Gambar</label>
                        <div class="col-sm-10">
                            <input type="file" name="file_gambar" class="form-control">
                        </div>
                    </div>
                    <p>Keterangan (*) : Wajib Diisi</p>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default pull-left flat"><span class="glyphicon glyphicon-ban-circle"></span> Batal</button>
                    <button type="submit" class="btn btn-primary pull-right flat" onclick="document.getElementById('loading7').style.display = 'block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
                </div>
            </form>
            <div class="overlay" id="loading7" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ubah_bank" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content box">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="riwayatheader">Ubah Bank</h4>
            </div>
            <form class="form-horizontal" method="post" action="{{ url('/ubah_bank') }}" autocomplete="off" enctype="multipart/form-data">
                <input type="hidden" name="id" id="ubah_bank_id">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Kode*</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="kode" id="ubah_bank_kode" placeholder="Masukkan Nama Bank">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Nama</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="nama" id="ubah_bank_nama" placeholder="Masukkan tarif dasar" style="text-transform:capitalize">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Nama Singkat</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="nama_singkat" id="ubah_bank_nama_singkat" placeholder="Masukkan tarif dasar" style="text-transform:capitalize">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Keterangan</label>
                        <div class="col-sm-10">
                            <textarea type="text" class="form-control" name="keterangan" id="ubah_bank_keterangan" placeholder="Masukkan Keterangan" style="text-transform:capitalize"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Gambar</label>
                        <div class="col-sm-10">
                            <input type="file" name="file_gambar" class="form-control">
                        </div>
                    </div>
                    <p>Keterangan (*) : Wajib Diisi</p>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default pull-left flat"><span class="glyphicon glyphicon-ban-circle"></span> Batal</button>
                    <button type="submit" class="btn btn-primary pull-right flat" onclick="document.getElementById('loading8').style.display = 'block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
                </div>
            </form>
            <div class="overlay" id="loading8" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ubah_nomor_mutasi_gudang" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content box">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="riwayatheader">Ubah Format Nomor</h4>
            </div>
            <form class="form-horizontal" method="post" action="{{ url('/ubah_nomor_mutasi_gudang') }}" autocomplete="off">
                <input type="hidden" class="form-control" name="id" id="ubah_nomor_mutasi_gudang_id">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Nama</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="nama" id="ubah_nomor_mutasi_gudang_nama" disabled="true">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Format*</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="format" id="ubah_nomor_mutasi_gudang_format" placeholder="Masukkan Format Nomor" style="text-transform:uppercase;">
                        </div>
                    </div>
                    <p>Keterangan (*) : Wajib Diisi</p>
                    <p>Untuk menambahkan template tanggal: gunakan tag 'DD' (hari), 'MM' (bulan), 'YY' (tahun), 'YYYY' (tahun)</p>
                    <p>Contoh : DD-MM-YYYY</p>
                    <p>Untuk menambahkan template angka: gunakan tag #</p>
                    <p>Contoh template angka 4 digit : ####</p>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default pull-left flat"><span class="glyphicon glyphicon-ban-circle"></span> Batal</button>
                    <button type="submit" class="btn btn-primary pull-right flat" onclick="document.getElementById('loading10').style.display = 'block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
                </div>
            </form>
            <div class="overlay" id="loading10" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ubah_syarat_ketentuan" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content box">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="riwayatheader">Ubah Syarat & Ketentuan</h4>
            </div>
            <form class="form-horizontal" method="post" action="{{ url('/ubah_syarat_ketentuan') }}" autocomplete="off">
                <input type="hidden" class="form-control" name="id" id="ubah_syarat_ketentuan_id">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">Penggunaan</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="nama" id="ubah_syarat_ketentuan_nama" disabled="true">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">Syarat & Ketentuan</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" name="konten" placeholder="Konten Syarat & Ketentuan" id="ubah_syarat_ketentuan_konten">
                            </textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default pull-left flat"><span class="glyphicon glyphicon-ban-circle"></span> Batal</button>
                    <button type="submit" class="btn btn-primary pull-right flat" onclick="document.getElementById('loading10').style.display = 'block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
                </div>
            </form>
            <div class="overlay" id="loading10" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

@endsection


<style type="text/css">
    .tooltip-inner {
        white-space:nowrap;
        max-width:none;
    }
</style>


<script src="{{ asset('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ url('/js/jquery.maskMoney.js') }}" type="text/javascript"></script>
<script type="text/javascript">
                        $(document).ready(function(){
                        // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
                        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                        // save the latest tab; use cookies if you like 'em better:
                        localStorage.setItem('lastTab_data_referensi', $(this).attr('href'));
                        });
                        // go to the latest tab, if it exists:
                        var lastTab_data_referensi = localStorage.getItem('lastTab_data_referensi');
                        if (lastTab_data_referensi) {
                        $('[href="' + lastTab_data_referensi + '"]').tab('show');
                        }
                        else
                        {
                        lastTab_data_referensi = "#tab_1";
                        $('[href="' + lastTab_data_referensi + '"]').tab('show');
                        }

                        $("#mydatatables").DataTable();
                        $("#mydatatables2").DataTable();
                        $("#mydatatables3").DataTable();
                        $("#mydatatables4").DataTable();
                        $('[data-toggle="tooltip"]').tooltip();
                        $("#tarif_dasar").maskMoney({precision:0});
                        $("#ubah_metode_pengiriman_tarif_dasar").maskMoney({precision:0});
                        });
                        function ubah_nomor_invoice(id, nama, format)
                        {
                        document.getElementById('ubah_nomor_invoice_id').value = id;
                        document.getElementById('ubah_nomor_invoice_nama').value = nama;
                        document.getElementById('ubah_nomor_invoice_format').value = format;
                        }

                        function ubah_nomor_mutasi_gudang(id, nama, format)
                        {
                        document.getElementById('ubah_nomor_mutasi_gudang_id').value = id;
                        document.getElementById('ubah_nomor_mutasi_gudang_nama').value = nama;
                        document.getElementById('ubah_nomor_mutasi_gudang_format').value = format;
                        }

                        function ubah_metode_pembayaran(id, nama, jenis, keterangan)
                        {
                        document.getElementById('ubah_metode_pembayaran_id').value = id;
                        document.getElementById('ubah_metode_pembayaran_nama').value = nama;
                        document.getElementById('ubah_metode_pembayaran_jenis').value = jenis;
                        document.getElementById('ubah_metode_pembayaran_keterangan').value = keterangan;
                        }

                        function ubah_metode_pengiriman(id, nama, tarif_dasar, keterangan)
                        {
                        document.getElementById('ubah_metode_pengiriman_id').value = id;
                        document.getElementById('ubah_metode_pengiriman_nama').value = nama;
                        document.getElementById('ubah_metode_pengiriman_tarif_dasar').value = tarif_dasar;
                        document.getElementById('ubah_metode_pengiriman_keterangan').value = keterangan;
                        }

                        function ubah_bank(id, kode, nama, nama_singkat, keterangan)
                        {
                        document.getElementById('ubah_bank_id').value = id;
                        document.getElementById('ubah_bank_kode').value = kode;
                        document.getElementById('ubah_bank_nama').value = nama;
                        document.getElementById('ubah_bank_nama_singkat').value = nama_singkat;
                        document.getElementById('ubah_bank_keterangan').value = keterangan;
                        }

                        function ubah_syarat_ketentuan(id, nama, konten){
                        document.getElementById('ubah_syarat_ketentuan_id').value = id;
                        document.getElementById('ubah_syarat_ketentuan_nama').value = nama;
                        document.getElementById('ubah_syarat_ketentuan_konten').value = konten;
                        }
</script>