<style type="text/css">
    .table.table-borderless td, .table.table-borderless th {
        border: 0 !important;
    }
</style>
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-body">
                <h1 style="font-family: cambria">@if(!empty($profil)){{ $profil->nama }}@endif</h1>
                <div class="col-md-5">
                    <h3>Data Perusahaan</h3>
                    <table class='table table-borderless'>
                        <tr>
                            <th width="30%"><dt>Alamat</dt></th>
                            <th width="2%">:</th>
                            <td>
                                <dd>@if(!empty($profil)){{ $profil->alamat }}, {{ ucwords(strtolower($profil->nama_kota)) }}@endif</dd>
                            </td>
                        </tr>
                        <tr>
                            <th><dt>Telepon</dt></th>
                            <th>:</th>
                            <td>
                                <dd>@if(!empty($profil))<?php echo str_replace('_', '', $profil->telp); ?>@endif</dd>
                            </td>
                        </tr>
                        <tr>
                            <th><dt>Kontak Person</dt></th>
                            <th>:</th>
                            <td>
                                <dd>@if(!empty($profil)){{ $profil->kontak_person }}@endif</dd>
                            </td>
                        </tr>
                        <tr>
                            <th><dt>Bidang Usaha</dt></th>
                            <th>:</th>
                            <td>
                                <dd>@if(!empty($profil)){{ $profil->bidang_usaha }}@endif</dd>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-7">
                    <h3>Data Cabang</h3>
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th style="text-align:center">NO</th>
                                <th style="text-align:center">ALAMAT</th>
                                <th style="text-align:center">TELEPON</th>
                                <th style="text-align:center">KONTAK PERSON</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            @if(!empty($cabang))
                            @foreach($cabang as $val)
                            <tr>
                                <td align="center">{{ $i++ }}.</td>
                                <td>{{ $val->alamat }}, {{ ucwords(strtolower($val->nama_kota)) }}</td>
                                <td><?php echo str_replace('_', '', $val->telp); ?></td>
                                <td>{{ $val->kontak_person }}</td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-aqua flat">
            <div class="inner">
                <h3>{{ decimal($data['pelanggan_aktif']) }}</h3>
                <p>Pelanggan Aktif</p>
            </div>
            <div class="icon">
                <i class="fa fa-users" style="padding-top:15px"></i>
            </div>
            <a href="{{ url('/pelanggan') }}" class="small-box-footer">Info lebih lanjut 
                <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-green flat">
            <div class="inner">
                <h3>{{ decimal($data['produk_aktif']) }}</h3>
                <p>Produk Aktif</p>
            </div>
            <div class="icon">
                <i class="fa fa-archive" style="padding-top:15px"></i>
            </div>
            <a href="{{ url('/produk') }}" class="small-box-footer">Info lebih lanjut 
                <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-yellow flat">
            <div class="inner">
                <h3>{{ decimal($data['stok_tersedia']) }}</h3>
                <p>Stok Tersedia</p>
            </div>
            <div class="icon">
                <i class="fa fa-cube" style="padding-top:15px"></i>
            </div>
            <a href="{{ url('/stok_opname') }}" class="small-box-footer">Info lebih lanjut 
                <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-red flat">
            <div class="inner">
                <h3>{{ decimal($data['sales_order_bulan_ini']) }}</h3>
                <p>Sales Order Bulan Ini</p>
            </div>
            <div class="icon">
                <i class="fa fa-shopping-bag" style="padding-top:15px"></i>
            </div>
            <a href="{{ url('/sales_order') }}" class="small-box-footer">Info lebih lanjut 
                <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
</div>
@stop
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>