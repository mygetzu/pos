@extends('layouts.master_home2')
@section('content')
@include('includes.base_function')
<div class="col-md-12">
    <h2 class="konten-title">Data Pembayaran</h2>
    <h4 class="box-title">Pilih opsi pembayaran</h4>
    <form class="form-horizontal" action="{{ url('/checkout/review_pesanan') }}" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="temp_alamat_pengiriman_id" value="{{ $temp_alamat_pengiriman_id }}">
        <div class="form-group">
            <label class="col-sm-2 control-label">Metode Pembayaran</label>
            <div class="col-sm-10">
                <select class="form-control" name="metode_pembayaran">
                    <option value="">-- Pilih metode pembayaran --</option>
                    @foreach($metode_pembayaran as $val)
                    <option value="{{ $val->id }}">{{ $val->nama }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="hp_sales" class="col-md-2 control-label"></label>
            <div class="col-md-4">
                <button class="btn btn-primary"><span class="fa fa-check"></span> Konfirmasi</button>
            </div>
        </div>
      <!-- /.box-footer -->
    </form>
</div>
<script src="{{ asset('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
@stop