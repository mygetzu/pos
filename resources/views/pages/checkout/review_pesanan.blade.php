@extends('layouts.master_home2')
@section('content')
@include('includes.base_function')
<div class="col-md-12">
    <input type="hidden" name="base_path" id="base_path" value="{{ asset('/') }}">
    <h2 style="border-bottom: 3px solid; display:inline; color:#808080">Review Pesanan</h2>
    @if(Session::has('message'))
    <div class="alert alert-danger flat alert-dismissable" style="margin-left: 0px;">
        <i class="fa fa-ban"></i>
        {{ Session::get('message') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <table class="table table-striped table-bordered" id="myTable">
        <thead>
            <tr>
                <th width="5%" style="text-align:center">NO</th>
                <th style="text-align:center">NAMA PRODUK</th>                    
                <th style="text-align:center">HARGA</th>
                <th style="text-align:center">JUMLAH</th>
                <th style="text-align:center">TOTAL HARGA</th>
            </tr>
        </thead>
        <tbody>
            <?php $i = 1; $jumlah=0; $total_bayar=0;?>
            @foreach($cart_content as $cart)
            <tr>
                <td align="center">{{ $i++ }}</td>
                <td>
                    @if($cart->name === 'voucher')
                    <b>{{ $cart->name }}</b>
                    @else
                        {{ $cart->name }} 
                        @if(strpos($cart->id, 'DIS') === 0) 
                            <b>(*diskon)</b> 
                        @elseif(strpos($cart->id, 'CAS') === 0) 
                            <b>(*diskon)</b> 
                        @elseif(strpos($cart->id, 'PKT') === 0) 
                            <b>(*paket)</b> 
                        @endif
                    @endif
                </td>
                <td>{{ rupiah($cart->price) }}</td>
                <td align="center">{{ $cart->qty }}</td>
                <td style="padding-left:40px">{{ rupiah($cart->price*$cart->qty) }}</td>
            </tr>
            <?php 
                $jumlah += $cart->qty;
                $total_bayar += ($cart->price*$cart->qty);
            ?>
            @endforeach
            @foreach($hadiah as $val)
            <tr>
                <td align="center">{{ $i++ }}</td>
                <td>{{ $val['nama'] }}<b>(*hadiah)</b></td>
                <td>0</td>
                <td align="center">{{ $val['quantity'] }}</td>
                <td style="padding-left:40px">{{ 0 }}</td>
            </tr>
            @endforeach
            <tr>
                <td colspan="4" align="center"><b>Total Bayar</b></td>
                <td style="padding-left:40px"><label class="control-label" id="my_total_bayar">{{ rupiah($total_bayar) }}</label></td>
            </tr>
        </tbody>
    </table>
    <input type="hidden" name="nominal_voucher" id="nominal_voucher">
    <div align="col-md-12">
        <label class="control-label col-md-6 pull-right">Voucher : </label>
        <div class="col-md-6 pull-right" id="display_nominal_voucher">
        </div>
    </div>
    
    <input type="hidden" name="jumlah" id="jumlah" value="{{ $jumlah }}">
    <div class="row">
        <div class="col-md-6">
            <input type="hidden" name="total_bayar" id="total_bayar" value="{{ $total_bayar }}">
            <div class="row">
                <div class="col-md-3">
                    <label class="control-label">Kode Voucher :</label>
                </div>
                <div class="col-md-7">
                    <input type="" name="kode_voucher" id="kode_voucher" class="form-control">
                </div>
                <button class="btn btn-default" type="button" onclick="cek_voucher()"><span class="fa fa-check"></span> Pilih</button>
            </div>
        </div>
        <div class="col-md-6">
            <form method="post" action="{{ url('/checkout/proses_pesanan') }}" class="form-horizontal">
                {{csrf_field()}}
                <input type="hidden" name="temp_alamat_pengiriman_id" value="{{ $temp_alamat_pengiriman_id }}">
                <input type="hidden" name="temp_pembayaran_id" value="{{ $temp_pembayaran_id }}">
                <input type="hidden" name="input_kode_voucher" id="input_kode_voucher" value="{{ $kode_voucher }}">
                <div class="row">
                    <div class="col-md-2">
                        <label class="control-label">Catatan :</label>
                    </div>
                    <div class="col-md-10">
                        <textarea class="form-control" name="catatan" style="height:50px"></textarea>
                    </div>
                </div>
                <br>
                <button type="submit" class="btn btn-success pull-right flat"><span class="glyphicon glyphicon-check"></span> Konfirmasi</button>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    function cek_voucher()
    { 
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })

        var total_bayar     = document.getElementById('total_bayar').value;
        var kode_voucher    = document.getElementById('kode_voucher').value;

        var url = "{{ url('/cek_voucher_home') }}";
        
        $.ajax({
            type: "POST",
            url: url,
            dataType: 'JSON',
            data: { total_bayar:total_bayar, kode_voucher:kode_voucher },
            success: function (data) {
                if(data.nominal == ""){
                    alert('voucher tidak dapat digunakan');
                    document.getElementById('kode_voucher').value = "";
                }
                else{
                    //cek apakah voucher sudah digunakan saat ini
                    var input_kode_voucher = document.getElementById('input_kode_voucher').value;
                    var arr_input_kode_voucher = input_kode_voucher.split(",");

                    var is_pakai = 0;
                    for (var i = arr_input_kode_voucher.length - 1; i >= 0; i--) {
                        if(arr_input_kode_voucher[i] == data.kode){
                            is_pakai = 1;
                        }
                    }

                    if(is_pakai == 1){
                        alert('voucher tidak dapat digunakan');
                        document.getElementById('kode_voucher').value = "";
                    }
                    else{
                        var nominal = document.getElementById('nominal_voucher').value;
                        if(nominal == ""){
                            nominal = 0;
                        }
                        else{
                            nominal = parseInt(nominal);
                        }

                        var data_nominal = data.nominal;
                        data_nominal = parseInt(data_nominal);

                        nominal = nominal+data_nominal;

                        document.getElementById('nominal_voucher').value = nominal;
                        document.getElementById('display_nominal_voucher').innerHTML = rupiah(nominal);
                        var input_kode_voucher = document.getElementById('input_kode_voucher').value;
                        document.getElementById('input_kode_voucher').value = input_kode_voucher+","+data.kode;
                        document.getElementById('kode_voucher').value = "";
                    }
                }
            },
            error: function (data) {
                alert('gagal cek kode voucher');
            }
        });
  }
  function rupiah(nominal)
    {
        //rumus konversi rupiah
        nominal = "Rp "+nominal.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
        return nominal;
    }

    function btn_proses_pesanan()
    {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })

        var url = "{{ url('/checkout/proses_pesanan') }}";
        
        $.ajax({
            type: "POST",
            url: url,
            data: $("#form_proses_pesanan").serialize(),
            success: function (data) {
                document.getElementById('form_invoice_pelanggan').submit();
            },
            error: function (data) {
                alert('error');
            }
        });
    }
</script>
@stop