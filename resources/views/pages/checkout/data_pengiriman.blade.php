@extends('layouts.master_home2')
@section('content')
@include('includes.base_function')
<div class="col-md-12">
    <h2 class="konten-title">Data Pengiriman</h2>
    <h4>Tentukan alamat dan metode pengiriman</h4>
    <form class="form-horizontal" action="{{ url('/checkout/data_pembayaran') }}" method="post">
        {{ csrf_field() }}
        <div class="form-group">
            <label class="col-sm-2 control-label">Nama</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" value="@if(!empty($pelanggan)){{ $pelanggan->nama }}@endif" name="nama" placeholder="Masukkan nama">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Email</label>
            <div class="col-sm-10">
                <input type="email" class="form-control" value="@if(!empty($pelanggan)){{ $pelanggan->email }}@endif" name="email" placeholder="Masukkan email">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Alamat</label>
            <div class="col-sm-10">
                <textarea type="text" class="form-control" name="alamat" placeholder="Masukkan alamat">@if(!empty($pelanggan)){{ $pelanggan->alamat }}@endif</textarea>
            </div>
        </div>
        <div class="form-group{{ $errors->has('kota_id') ? ' has-error' : '' }}">
            <label for="kota" class="col-md-2 control-label">Kabupaten/Kota</label>
            <div class="col-md-5">
                <select class="form-control" id="provinsi" name="provinsi">
                    <option value="">-- Pilih Provinsi --</option>
                    @foreach($provinsi as $val)
                    <option value="{{ $val->kode }}" @if(!empty($pelanggan))@if($pelanggan->kota->provinsi_kode === $val->kode) selected @endif @endif>{{ $val->nama }}</option>
                    @endforeach
                </select>
                @if ($errors->has('provinsi'))
                    <span class="help-block">
                        <strong>Pilihan Provinsi Wajib Diisi</strong>
                    </span>
                @endif
            </div>
            <div class="col-md-5">
                <select class="form-control" id="kota_id" name="kota">
                    <option value="">-- Pilih Kota --</option>
                    @if(!empty($pelanggan))
                    @foreach($kota as $val)
                    <option value="{{ $val->id }}" @if($pelanggan->kota_id === $val->id) selected @endif >{{ $val->nama }}</option>
                    @endforeach
                    @endif
                </select>
                @if ($errors->has('kota_id'))
                    <span class="help-block">
                        <strong>Pilihan Kabupaten/Kota Wajib Diisi</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('kode_pos') ? ' has-error' : '' }}">
            <label for="kode_pos" class="col-md-2 control-label">Kode Pos</label>
            <div class="col-md-2">
                <input id="kode_pos" type="text" class="form-control" name="kode_pos" placeholder="Masukkan Kode Pos" onKeyPress="return numbersonly(this, event)" maxlength="5" value="@if(!empty($pelanggan)){{ $pelanggan->kode_pos }}@endif">
                @if ($errors->has('kode_pos'))
                    <span class="help-block">
                        <strong>Kolom Kode Pos Wajib Diisi</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('hp') ? ' has-error' : '' }}">
            <label for="hp_sales" class="col-md-2 control-label">Nomor Handphone</label>
            <div class="col-md-4">
                <div class="input-group" style="margin-bottom:0px">
                    <div class="input-group-addon">
                        <i class="fa fa-mobile" style="width:12px"></i>
                    </div>
                    <input type="text" class="form-control" data-inputmask='"mask": "(+62)999-9999-9999"' data-mask2 placeholder="contoh: (+62)857-9999-9999" name="hp" value="@if(!empty($pelanggan)){{ $pelanggan->hp_sales }}@endif">
                </div>
            </div>
        </div>
        <div class="form-group{{ $errors->has('metode_pengiriman') ? ' has-error' : '' }}">
            <label for="hp_sales" class="col-md-2 control-label">Metode Pengiriman</label>
            <div class="col-md-10">
                <select class="form-control" name="metode_pengiriman">
                    <option value="">-- Pilih metode pengiriman --</option>
                    @foreach($metode_pengiriman as $val)
                    <option value="{{ $val->id }}">{{ $val->nama }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="hp_sales" class="col-md-2 control-label"></label>
            <div class="col-md-4">
                <button class="btn btn-primary"><span class="fa fa-check"></span> Konfirmasi</button>
            </div>
        </div>
      <!-- /.box-footer -->
    </form>
</div>
<input type="hidden" name="base_path" id="base_path" value="{{ asset('/') }}">
<script src="{{ asset('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
<script>
  $(function () {
    $("[data-mask2]").inputmask();
  });
  $(document).ready(function(){
        $('#provinsi').change(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            var kode_provinsi = document.getElementById('provinsi').value;
            if(kode_provinsi == "")
            {
                kode_provinsi = "ALL";
            }
            var path =  document.getElementById('base_path').value;
            var url = path+"get_kota/"+kode_provinsi;
            
            $.ajax({
                type: "POST",
                url: url,
                success: function (data) {
                    $("#kota_id").html(data);
                },
                error: function (data) {
                    alert('error 3');
                }
            });
        });
    });
</script>
@stop
