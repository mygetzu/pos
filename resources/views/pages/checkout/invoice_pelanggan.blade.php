@extends('layouts.master_home2')
@section('content')
@include('includes.base_function')
<div class="col-md-12">
    <div class=""><br><br>
        <section id="header">
            <div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="title">{{ $judul }}</div>
                        <div class="company">
                            {{ $perusahaan->nama }} <br>
                            {{ $perusahaan->alamat }} <br>
                            T. {{ $perusahaan->telp }} <br>
                            E. {{ $perusahaan->email }}
                        </div>
                    </div>
                    <div class="col-md-5">
                        <table class="detail">
                            <tr>
                                <td class="title-detail">No. Transaksi</td>
                                <td class="titik">&nbsp;:&nbsp;</td>
                                <td class="data-detail">{{ $so_header->no_sales_order }}</td>
                            </tr>
                            <tr>
                                <td class="title-detail">Tanggal</td>
                                <td class="titik">&nbsp;:&nbsp;</td>
                                <td class="data-detail">
                                    <?php $tanggal = new DateTime($so_header->tanggal); 
                                    echo $tanggal->format('d/m/Y G:i:s');
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="title-detail">Pelanggan</td>
                                <td class="titik">&nbsp;:&nbsp;</td>
                                <td class="data-detail user">{{ $so_header->pelanggan->nama }}</td>
                            </tr>
                            <tr>
                                <td class="title-detail" style="vertical-align: top;">Alamat </td>
                                <td class="titik" style="vertical-align: top;">&nbsp;:&nbsp;</td>
                                <td class="data-detail user">{{ $so_alamat_pengiriman->alamat }} {{ ucwords(strtolower($so_alamat_pengiriman->kota->nama)) }}<br>
                                T. {{ $so_alamat_pengiriman->telp }}<br>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-3">
                        <table class="detail">
                            <tr>
                                <td class="title-detail">Tempo</td>
                                <td class="titik">&nbsp;:&nbsp;</td>
                                <td class="data-detail"></td>
                            </tr>
                            <tr>
                                <td class="title-detail">Jatuh Tempo</td>
                                <td class="titik">&nbsp;:&nbsp;</td>
                                <td class="data-detail"></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </section>
        <section id="table">
            <div>
                <table class="table">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Produk</th>
                            <th>Satuan</th>
                            <th>Harga</th>
                            <th>Jumlah</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $jumlah_item=0; $total_akhir=0; ?>
                        @foreach($so_detail as $i => $val)
                        <tr>
                            <td align="center">{{ $i+1 }}</td>
                            <td align="left">{{ $val['nama'] }}</td>
                            <td align="center">{{ $val['satuan'] }}</td>
                            <td align="right">{{ rupiah($val['harga']) }}</td>
                            <td align="center">{{ $val['jumlah'] }}</td>
                            <td align="right">{{ rupiah($val['jumlah']*$val['harga']) }}</td>
                        </tr>
                        <?php $jumlah_item=$jumlah_item+$val['jumlah']; 
                            $total_akhir = $total_akhir+ ($val['jumlah']*$val['harga']);
                        ?>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </section>
        <section id="footer" style="background-color: transparent;">
            <div>
                <div class="row">
                    <div class="col-md-4">
                        <table class="keterangan">
                            <tr>
                                <td class="title-detail" style="vertical-align: top;">Keterangan</td>
                                <td class="titik" style="vertical-align: top;">&nbsp;:&nbsp;</td>
                                <td class="data-detail">
                                {{ $so_header->catatan }}
                                <br><br></td>
                            </tr>
                            <tr>
                                <td class="title-detail" style="vertical-align: top;">Terbilang</td>
                                <td class="titik" style="vertical-align: top;">&nbsp;:&nbsp;</td>
                                <td class="data-detail">{{ dibaca($terbilang) }} Rupiah</td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-3">
                        <div class="jml">
                            <table class="detail">
                                <tr>
                                    <td class="title-detail price" width="100px;">Jml Item</td>
                                    <td class="titik price" width="100px;">&nbsp;:&nbsp;</td>
                                    <td class="data-detail right price">{{ $jumlah_item }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-5 right">
                        <table class="detail">
                            <tr>
                                <td class="title-detail price" width="160px;">Total Akhir</td>
                                <td class="titik price" width="100px;">&nbsp;:&nbsp;</td>
                                <td class="data-detail right price">{{ rupiah($total_akhir) }}</td>
                            </tr>
                            @foreach($so_voucher as $val)
                            <tr>
                                <td class="title-detail price" width="160px;">Voucher</td>
                                <td class="titik" width="100px;">&nbsp;:&nbsp;</td>
                                <td class="data-detail right price">
                                    {{ rupiah($val->nominal) }}
                                </td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ URL::to('/css/invoice.css') }}">
@stop