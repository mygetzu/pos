@extends('layouts.master_home2')
@section('content')
@include('includes.base_function')
<div class="col-md-12">
    <h2 align="center">Lanjut sebagai Guest atau login terlebih dahulu</h2>
    @if(Session::has('message'))
    <div class="alert alert-danger flat alert-dismissable" style="margin-left: 0px;">
        <i class="fa fa-ban"></i>
        {{ Session::get('message') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <br>
    <div class="col-md-8" style="padding: 0">
        <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
            {{ csrf_field() }}
            <input type="hidden" name="link" value="checkout/data_pengiriman">
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="col-md-2 control-label">Email</label>
                <div class="col-md-8">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="col-md-2 control-label">Password</label>
                <div class="col-md-8">
                    <div class="input-group" style="margin-bottom:0px">
                        <input id="password" type="password" class="form-control" name="password">
                        <span class="input-group-btn">
                            <button class="btn btn-default" tabindex="-1" type="button" id="btn-pass"><span class="glyphicon glyphicon-eye-close" style="margin:3px"></span></button>
                        </span>
                    </div>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-8 col-md-offset-2">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="remember"> Ingat Saya
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-8 col-md-offset-2">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-sign-in"></i> Login
                    </button>
                    <a class="btn btn-link" href="{{ url('/password/reset') }}" style="padding-top:20px">Lupa Password</a>
                </div>
            </div>
            <div class="form-group">
                Belum punya akun, silahkan <a class="btn btn-link" href="{{ url('/registrasi') }}">Registrasi</a>
            </div>
        </form>
    </div>
    <div class="col-md-4" style="background-color: #eee;min-height: 240px; padding: 20px;">
        <p>Pesanan akan tetap diproses tanpa harus login terlebih dahulu.</p>
        <form method="get" action="{{ url('/checkout/data_pengiriman') }}">
            <input type="hidden" name="lanjut" value="1">
            <button class="btn btn-primary flat"><span class="fa fa-arrow-right"></span> Lanjut Tanpa Login</button>
        </form>
    </div>    
</div>

<script src="{{ asset('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function(){

            $('#btn-pass').click(function(){
                var obj = document.getElementById('password');
                if(obj.type == "text")
                {
                    obj.type = "password"
                    $(this).find('span').removeClass('glyphicon glyphicon-eye-open');
                    $(this).find('span').addClass('glyphicon glyphicon-eye-close');
                }else{
                    obj.type = "text"
                    $(this).find('span').removeClass('glyphicon glyphicon-eye-close');
                    $(this).find('span').addClass('glyphicon glyphicon-eye-open');
                }
                   
            });
      });
    
</script>
@endsection