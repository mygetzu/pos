<table class="table table-striped table-bordered" id="ajax_produk_tabless">
    <thead>
        <th style="text-align:center">NO</th>
        <th style="text-align:center">NAMA PRODUK</th>
        <th style="text-align:center">DISKON</th>
        <th style="text-align:center">GAMBAR PRODUK</th>
        <th style="text-align:center">AWAL PERIODE</th>
        <th style="text-align:center">AKHIR PERIODE</th>
        <th style="text-align:center"></th>
        <th style="text-align:center"></th>
    </thead>
    <tbody>
        <?php $i=1; $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');?>
        @foreach($produk_harga as $val)
        <tr>
            <td align="center">{{ $i++ }}</td>
            <td>{{ $val->produk->nama }}
                <?php
                date_default_timezone_set("Asia/Jakarta");
                $date1 = new DateTime($val->awal_periode);
                $date2 = new DateTime($val->akhir_periode);
                $curr_date = new DateTime();
                $curr_dates = $curr_date->format('Y-m-d');
                $date1s = $date1->format('Y-m-d');
                $date2s = $date2->format('Y-m-d');
                if( $curr_dates >= $date1s && $curr_dates <= $date2s){
                    echo "<span class='label label-success'>Aktif</span>";
                }else{
                    echo "<span class='label label-danger'>Tidak Aktif</span>";
                }
                ?>
            </td>
            <td align="center">{{ $val->diskon }}%</td>
            <td align="center">
                @if(empty($val->produk->produk_galeri_first))
                <img src="{{ URL::asset('img/'.$gambar_default->value) }}" style="height:130px;width:auto;padding: 15px 15px 0px 15px" class="img-responsive" alt="">
                @else
                <img src="{{ URL::asset('img/produk/'.$val->produk->produk_galeri_first->file_gambar) }}" style="height:130px;width:auto;padding: 15px 15px 0px 15px" class="img-responsive" alt="">
                @endif
            </td>           
            <td align="center"><?php echo $date1->format('d')." ".$bulan[(int)$date1->format('m')]." ".$date1->format('Y'); ?></td>
            <td align="center"><?php echo $date2->format('d')." ".$bulan[(int)$date2->format('m')]." ".$date2->format('Y'); ?></td>
            <td align="center">
                <a data-toggle="modal" data-target="#ubah_produk_harga">
                    <button class="btn btn-warning flat" onclick="ubah_produk_harga('{{ $val->id }}','{{ $val->produk->nama }}','{{ $val->diskon }}','{{ $date1s }}', '{{ $date2s }}')" data-toggle="tooltip" title="ubah" style="font-size: 15pt"><span class="fa fa-edit"></span></button>
                </a>
            </td>
            <td align="center">
                <button class="btn btn-danger flat" onclick="hapus_produk_harga('{{ $val->id }}')" data-toggle="tooltip" title="hapus" style="font-size: 15pt"><span class="fa fa-trash"></span></button>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
  $(function () {
    $("#ajax_produk_tabless").DataTable();
  });
</script>