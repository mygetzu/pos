<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <a href="{{ url('/pelanggan') }}" class="pull-right">
                        <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                    </a>
            </div>
            <div class="box-body">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <table id="mydatatables" class="table table-striped table-bordered" style="margin:0px">
                    <thead>
                        <th style="text-align:center">NO</th>
                        <th style="text-align:center">NO NOTA</th>
                        <th style="text-align:center">TANGGAL</th>
                        <th style="text-align:center">NAMA PELANGGAN</th>
                        <th style="text-align:center">JATUH TEMPO</th>
                        <th style="text-align:center">TOTAL TAGIHAN</th>
                        <th style="text-align:center"></th>
                    </thead>
                    <tbody>
                        <?php $i=1; $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'); ?>
                        @foreach($so_header as $val)
                        <tr>
                            <td align="center">{{ $i++ }}</td>
                            <td align="center">{{ $val->no_sales_order }}</td>
                            <td><?php
                                    date_default_timezone_set("Asia/Jakarta");
                                    $tanggal = new DateTime($val->tanggal);
                                    $jatuh_tempo = new DateTime($val->due_date);
                                    $tanggals = $tanggal->format('Y-m-d');
                                    $jatuh_tempos = $jatuh_tempo->format('Y-m-d');
                                ?>
                                <?php echo $tanggal->format('d')." ".$bulan[(int)$tanggal->format('m')]." ".$tanggal->format('Y')." ".$tanggal->format(' G:i:s'); ?>
                            </td>
                            <td>{{ $val->pelanggan->nama }}</td>
                            <td><?php echo $jatuh_tempo->format('d')." ".$bulan[(int)$jatuh_tempo->format('m')]." ".$jatuh_tempo->format('Y'); ?></td>
                            <td>{{ rupiah($val->total_tagihan) }}</td>
                            <td align="center">
                                <a class="btn btn-info flat" href="{{ url('/transaksi_pelanggan_detail/'.$val->id) }}" data-toggle="tooltip" title="detail" style="font-size: 15pt"><span class="fa fa-info-circle"></span></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="overlay" id="loading" style="display:none">
              <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>
@stop
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script>
  $(function () {
    $("#mydatatables").DataTable();
  });
</script>
