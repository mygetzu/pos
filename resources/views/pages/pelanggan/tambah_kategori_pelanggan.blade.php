@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <div class="pull-right">
                    <a href="{{ url('/kategori_pelanggan') }}">
                        <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                    </a>
                </div>
            </div>
            <div class="box-body">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <form class="form-horizontal" method="POST" action="{{ url('/do_tambah_kategori_pelanggan') }}" autocomplete="off">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                        <label for="name" class="control-label col-md-2">Nama*</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="nama" id="nama" value="{{ old('nama') }}" placeholder="Masukkan Nama Kategori Pelanggan" style="text-transform:capitalize">
                            @if ($errors->has('nama'))
                                <span class="help-block">
                                    @if(strpos($errors->first('nama'), 'required') !== false)
                                    <strong>Kolom Nama Wajib Diisi</strong>
                                    @else
                                    <strong>Nama Kategori Pelanggan Sudah Terpakai</strong>
                                    @endif
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('default_diskon') ? ' has-error' : '' }}">
                        <label class="control-label col-md-2">Diskon*</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control" value="{{ old('default_diskon') }}" name="default_diskon" id="default_diskon" onKeyPress="return numbersonly(this, event)" maxlength="3" placeholder="Diskon" style="text-align:right">
                            @if ($errors->has('default_diskon'))
                                <span class="help-block">
                                    <strong>Kolom Diskon Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                        <label class="control-label col-md-1">%</label>
                    </div>
                    <div class="form-group{{ $errors->has('deskripsi') ? ' has-error' : '' }}">
                        <label class="control-label col-md-2">Deskripsi*</label>
                        <div class="col-md-8">
                            <textarea type="text" class="form-control" name="deskripsi" id="deskripsi" placeholder="Masukkan Deskripsi Kategori Pelanggan">{{ old('deskripsi') }}</textarea>
                            @if ($errors->has('deskripsi'))
                                <span class="help-block">
                                    <strong>Kolom Deskripsi Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-2" id="button_submit">
                            <button type="submit" class="btn btn-primary flat" onclick="document.getElementById('loading').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
                        </div>
                    </div>
                </form>
                <p>Keterangan (*) : Wajib Diisi</p>
            </div>
            <div class="overlay" id="loading" style="display:none">
              <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>
@stop
<!-- jQuery 2.2.0 -->
<script src="{{ asset('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>