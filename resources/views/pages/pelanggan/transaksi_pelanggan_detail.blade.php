<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <a href="{{ url('/transaksi_pelanggan/'.$pelanggan_id) }}" class="pull-right">
                    <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                </a>
            </div>
            <div class="box-body">
                <table class="table table-striped table-bordered" id="mydatatables">
                    <thead>
                        <th style="text-align:center">NO</th>
                        <th style="text-align:center">NAMA PRODUK</th>
                        <th style="text-align:center">JUMLAH</th>
                        <th style="text-align:center">HARGA</th>
                    </thead>
                    <tbody>
                        <?php $i=1; ?>
                        @foreach($so_detail as $val)
                        <tr>
                            <td align="center">{{ $i++ }}</td>
                            <td>
                                @if($val->jenis_barang_id == 1)
                                {{ $val->produk->nama }}
                                @elseif($val->jenis_barang_id == 2)
                                {{ $val->hadiah->nama }}
                                @elseif($val->jenis_barang_id == 3)
                                {{ $val->paket->nama }}
                                @endif
                            </td>
                            <td align="center">{{ $val->jumlah }}</td>
                            <td align="center">{{ rupiah($val->harga) }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="overlay" id="loading" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>
@stop
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script>
  $(function () {
    $("#mydatatables").DataTable();
  });
</script>