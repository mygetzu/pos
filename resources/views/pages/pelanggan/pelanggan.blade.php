@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
            </div>
            <div class="box-body">
              @if(Session::has('message'))
              <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                <i class="fa fa-check"></i>
                {{ Session::get('message') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              @endif
              @if(Session::has('message_fail'))
              <div class="alert alert-danger alert-dismissable flat" style="margin-left: 0px;">
                <i class="fa fa-check"></i>
                {{ Session::get('message_fail') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              @endif
              <table id="mydatatables" class="table table-striped table-bordered">
                <thead>
                    <th style="text-align:center">NO</th>
                    <th style="text-align:center">NAMA</th>
                    <th style="text-align:center">EMAIL</th>
                    <th style="text-align:center">KATEGORI PELANGGAN</th>
                    <!-- <th style="text-align:center">STATUS</th> -->
                    <th style="text-align:center"></th>
                    <th style="text-align:center"></th>
                </thead>
                <tbody>
                    <?php $i=1; ?>
                    @foreach($pelanggan as $val)
                    <tr>
                        <td align="center">{{ $i++ }}</td>
                        <td>{{ $val->nama }}</td>
                        <td>{{ $val->email }}</td>
                        <td>@if($val->kategori_pelanggan_id != '0'){{ $val->kategori_pelanggan_nama }}@endif</td>
                        <!-- <td align="center">
                          <input style="width:122px" name="my-checkbox" id="switch-state" type="checkbox" data-on-text='<small>Aktif</small>' data-off-text='<small>Non-Aktif</small>' onchange='window.location.href="{{ url('/ubah_status_pelanggan/'.$val->id) }}"' <?php //if($val->is_aktif == 1) echo "checked"; else echo ""; ?>>
                        </td> -->
                        <td align="center">
                          @if($val->kategori_pelanggan_id === '0')
                          <a data-toggle="modal" data-target="#add_pelanggan_kategori">
                            <button class='btn bg-purple flat' onclick="add_pelanggan_kategori_button('{{ $val->id }}')" data-toggle="tooltip" title="tambah kategori pelanggan" style="font-size: 15pt"><span class='fa fa-plus'></span></button>
                          </a>
                          @else
                          <a data-toggle="modal" data-target="#ubah_pelanggan_kategori">
                            <button class='btn btn-warning flat' onclick="ubah_pelanggan_kategori_button('{{ $val->id }}')" data-toggle="tooltip" title="ubah kategori pelanggan" style="font-size: 15pt"><span class='fa fa-edit'></span></button>
                          </a>
                          @endif
                        </td>
                        <td align="center">
                            <a class="btn bg-olive flat" href="{{ url('/transaksi_pelanggan/'.$val->id) }}" data-toggle="tooltip" title="riwayat transaksi" style="font-size: 15pt"><i class="fa fa-history"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="add_pelanggan_kategori" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content box">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="riwayatheader">Tambah Kategori Pelanggan</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="post" action="{{ url('/tambah_kategori_untuk_pelanggan') }}">
        {{ csrf_field() }}
          <input type="hidden" class="form-control" name="add_pelanggan_id" id="add_pelanggan_id" value="">
            <div class="form-group">
              <label class="col-sm-3 control-label">Nama Kategori Pelanggan</label>
              <div class="col-sm-9">
                <select class="form-control" name="kategori_pelanggan_id" id="kategori_pelanggan_id">
                  <option value="">-- Pilih Kategori Pelanggan --</option>
                @foreach($kategori_pelanggan as $val2)
                  <option value="{{ $val2->id }}">{{ $val2->nama }}</option>
                @endforeach
                </select>
              </div>
          </div><!-- /.box-body -->
          <div class="box-footer">
            <button data-dismiss="modal" class="btn btn-default flat"><span class="glyphicon glyphicon-ban-circle"></span> Batal</button>
            <button type="submit" class="btn btn-primary pull-right flat" onclick="document.getElementById('loading').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
          </div><!-- /.box-footer -->
        </form>
      </div>
      <div class="overlay" id="loading" style="display:none">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
    </div>
  </div>
</div>

<div class="modal fade" id="ubah_pelanggan_kategori" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content box">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="riwayatheader">Ubah Kategori Pelanggan</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="post" action="{{ url('/ubah_kategori_untuk_pelanggan') }}">
        {{ csrf_field() }}
          <input type="hidden" class="form-control" name="ubah_pelanggan_id" id="ubah_pelanggan_id" value="">
            <div class="form-group">
              <label class="col-sm-3 control-label">Nama Kategori Pelanggan</label>
              <div class="col-sm-9">
                <select class="form-control" name="kategori_pelanggan_id" id="kategori_pelanggan_id">
                  <option value="">-- Pilih Kategori Pelanggan --</option>
                @foreach($kategori_pelanggan as $val2)
                  <option value="{{ $val2->id }}">{{ $val2->nama }}</option>
                @endforeach
                </select>
              </div>
          </div><!-- /.box-body -->
          <div class="box-footer">
            <button data-dismiss="modal" class="btn btn-default flat"><span class="glyphicon glyphicon-ban-circle"></span> Batal</button>
            <button type="submit" class="btn btn-primary pull-right flat" onclick="document.getElementById('loading2').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
          </div><!-- /.box-footer -->
        </form>
      </div>
      <div class="overlay" id="loading2" style="display:none">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
    </div>
  </div>
</div>

@stop



<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<link href="{{ URL::to('/switch/dist/css/bootstrap3/bootstrap-switch.css') }}" rel="stylesheet">
<script src="{{ URL::to('/switch/docs/js/highlight.js') }}"></script>
<script src="{{ URL::to('/switch/dist/js/bootstrap-switch.js') }}"></script>
<script src="{{ URL::to('/switch/docs/js/main.js') }}"></script>
<script>
  $(function () {
    $("#mydatatables").DataTable();
  });
</script>
<script type="text/javascript">
    function add_pelanggan_kategori_button(pelanggan_id)
    {
      document.getElementById("add_pelanggan_id").value           = pelanggan_id;
    }

    function ubah_pelanggan_kategori_button(pelanggan_id)
    {
      document.getElementById("ubah_pelanggan_id").value           = pelanggan_id;
    }
    function ubah_status(id,nama,is_aktif)
    {
      document.getElementById("us_pelanggan_id").value   = id;
      document.getElementById("us_nama_pelanggan").value   = nama;
      if(is_aktif == '1'){
        document.getElementById("us_status_pelanggan").selectedIndex = '0';
      }else{
        document.getElementById("us_status_pelanggan").selectedIndex = '1';
      }
    }
</script>