<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
          <div class="box-header with-border">
            <a href="{{ url('/tambah_kategori_pelanggan') }}">
              <button class='btn bg-purple flat'><span class='fa fa-plus'></span> Tambah Kategori Pelanggan</button>
            </a>
          </div>
          <div class="box-body">
            @if(Session::has('message'))
            <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
              <i class="fa fa-check"></i>
              {{ Session::get('message') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            @endif
            <table class="table table-striped table-bordered" id="mydatatables" >
                <thead>
                    <tr>
                        <th style="text-align:center">NO</th>
                        <th style="text-align:center">NAMA</th>
                        <th style="text-align:center">DISKON</th>
                        <th style="text-align:center">JUMLAH PELANGGAN</th>
                        <th style="text-align:center">JUMLAH PROMO AKTIF</th>
                        <th style="text-align:center"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i=1; ?>
                    @foreach($kategori_pelanggan as $val)
                    <tr>
                        <td align="center">{{ $i++ }}</td>
                        <td>{{ $val->nama }}</td>
                        <td align="center">{{ $val->default_diskon }}%</td>
                        <td align="center">{{ $val->jumlah }}</td>
                        <td align="center">
                            {{ $val->jumlah_promo }}
                        </td>
                        <td align="center">
                            <a class="btn btn-info flat" href="{{ url('/detail_kategori_pelanggan/'.$val->id) }}" data-toggle="tooltip" title="detail" style="font-size: 15pt"><i class="fa fa-info-circle"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
          </div>
        </div>
    </div>
</div>
@stop
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script>
  $(function () {
    $("#mydatatables").DataTable();
  });
</script>