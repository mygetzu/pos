<link rel="stylesheet" href="{{ asset('/adminlte/plugins/select2/select2.min.css') }}">
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<input type="hidden" name="ori_nama" id="ori_nama" value="{{ $kategori_pelanggan->nama }}">
<input type="hidden" name="ori_default_diskon" id="ori_default_diskon" value="{{ $kategori_pelanggan->default_diskon }}">
<input type="hidden" name="ori_deskripsi" id="ori_deskripsi" value="{{ $kategori_pelanggan->deskripsi }}">
<div class="row">
    <div class="col-md-12">
        <div class="nav-tabs-custom box tab-primary flat" style="border-top: 0">
            <ul class="nav nav-tabs">
                <li><a href="#tab_1" data-toggle="tab">Detail Kategori Pelanggan</a></li>
                <li><a href="#tab_2" data-toggle="tab">Data Pelanggan</a></li>
                <li><a href="#tab_3" data-toggle="tab">Harga Promosi</a></li>
                <li class="pull-right"><a href="{{ url('/kategori_pelanggan') }}" style="padding:0px">
                        <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                </a>
                </li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane" id="tab_1">
                <div id="ubah" style="display:block">
                    <div class="">
                      <button class="btn btn-warning flat" onclick="ubah()"><span class='fa fa-edit'></span> Ubah</button>
                    </div>
                </div>
                <div id="batal" style="display:none">
                    <div class="">
                      <button class="btn btn-default flat" onclick="batal()"><span class='fa fa-ban'></span> Batal</button>
                    </div>
                </div>
                <br>
                @if(Session::has('message1'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message1') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <form class="form-horizontal" action="{{ url('/ubah_kategori_pelanggan') }}" method="post" autocomplete="off">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ $kategori_pelanggan->id }}">
                    <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                        <label class="control-label col-md-2">Nama <span id="ast1" class="control-label"></span></label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="nama" id="nama" value="{{ $kategori_pelanggan->nama }}" disabled="true" placeholder="Masukkan Nama Kategori Pelanggan" style="text-transform:capitalize">
                            @if ($errors->has('nama'))
                                <span class="help-block">
                                    <strong>Kolom Nama Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('default_diskon') ? ' has-error' : '' }}">
                        <label class="control-label col-md-2">Diskon <span id="ast2" class="control-label"></span></label>
                        <div class="col-md-2">
                            <input type="text" class="form-control" name="default_diskon" id="default_diskon" value="{{ $kategori_pelanggan->default_diskon }}" disabled="true" onKeyPress="return numbersonly(this, event)" maxlength="3" placeholder="Masukkan Nilai Diskon">
                            @if ($errors->has('default_diskon'))
                                <span class="help-block">
                                    <strong>Kolom Default Diskon Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                        <label class="control-label col-md-1">%</label>
                    </div>
                    <div class="form-group{{ $errors->has('deskripsi') ? ' has-error' : '' }}">
                        <label class="control-label col-md-2">Deskripsi <span id="ast3" class="control-label"></span></label>
                        <div class="col-md-8">
                            <textarea class="form-control" name="deskripsi" id="deskripsi" disabled="true" placeholder="Masukkan Deskripsi Kategori Pelanggan">{{ $kategori_pelanggan->deskripsi }}</textarea>
                            @if ($errors->has('deskripsi'))
                                <span class="help-block">
                                    <strong>Kolom Deskripsi Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-2">
                            <button type="submit" class="btn btn-primary flat" id="button_submit" style="display:none" onclick="document.getElementById('loading').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
                        </div>
                    </div>
                </form>
                <label id="astket" class="control-label"></label>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
                <table class="table table-striped table-bordered" id="mydatatables">
                    <thead>
                        <th style="text-align:center">NO</th>
                        <th style="text-align:center">NAMA</th>
                        <th style="text-align:center">ALAMAT</th>
                        <th style="text-align:center">KOTA</th>
                        <th style="text-align:center">NAMA SALES</th>
                        <th style="text-align:center">DESKRIPSI</th>
                    </thead>
                    <tbody>
                        <?php $i=1; ?>
                        @foreach($pelanggan as $val)
                        <tr>
                            <td align="center">{{ $i++ }}</td>
                            <td>{{ $val->nama }}</td>
                            <td>{{ $val->alamat }}</td>
                            <td>{{ $val->nama_kota }}</td>
                            <td>{{ $val->nama_sales }}</td>
                            <td>{{ $val->deskripsi }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_3">
                <div>
                <a data-toggle="modal" data-target="#tambah_harga_promosi">
                    <button class="btn bg-purple flat" onclick=""><i class="fa fa-plus"></i> Tambah Harga Promosi</button>
                  </a>
                  <div class="pull-right">
                  <form id="form_produk_harga" name="form_produk_harga">
                    <select class="form-control" id="status_promo" name="status_promo">
                        <option value="0">-- Semua Produk --</option>
                        <option value="1">Promosi Aktif</option>
                        <option value="2">Promosi Tidak Aktif</option>
                    </select>
                    <input type="hidden" name="kategori_pelanggan_id" value="{{ $kategori_pelanggan->id }}">
                    </form>
                  </div>

                  </div>
                  <br>
                  @if(Session::has('message3'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message3') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                @if ($errors->has('produk_id'))
                <div class="alert alert-danger flat">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    Pilihan Produk Wajib Diisi
                </div>
                @endif
                @if ($errors->has('diskon'))
                <div class="alert alert-danger flat">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    Kolom Diskon Wajib Diisi
                </div>
                @endif
                @if ($errors->has('awal_periode'))
                <div class="alert alert-danger flat">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    Kolom Awal Periode Wajib Diisi
                </div>
                @endif
                @if ($errors->has('akhir_periode'))
                <div class="alert alert-danger flat">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    Kolom Akhir Periode Wajib Diisi
                </div>
                @endif
                  <div id="content_produk_harga">
                <table class="table table-striped table-bordered" id="mydatatables2">
                    <thead>
                        <th style="text-align:center">NO</th>
                        <th style="text-align:center">NAMA PRODUK</th>
                        <th style="text-align:center">DISKON</th>
                        <th style="text-align:center">GAMBAR PRODUK</th>
                        <th style="text-align:center">AWAL PERIODE</th>
                        <th style="text-align:center">AKHIR PERIODE</th>
                        <th style="text-align:center"></th>
                        <th style="text-align:center"></th>
                    </thead>
                    <tbody>
                        <?php $i=1; $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');?>
                        @foreach($produk_harga as $val)
                        <tr>
                            <td align="center">{{ $i++ }}</td>
                            <td>{{ $val->produk->nama }}
                                <?php
                                date_default_timezone_set("Asia/Jakarta");
                                $date1 = new DateTime($val->awal_periode);
                                $date2 = new DateTime($val->akhir_periode);
                                $curr_date = new DateTime();
                                $curr_dates = $curr_date->format('Y-m-d');
                                $date1s = $date1->format('Y-m-d');
                                $date2s = $date2->format('Y-m-d');
                                if( $curr_dates >= $date1s && $curr_dates <= $date2s){
                                    echo "<span class='label label-success'>Aktif</span>";
                                }else{
                                    echo "<span class='label label-danger'>Tidak Aktif</span>";
                                }
                                ?>
                            </td>
                            <td align="center">{{ $val->diskon }}%</td>
                            <td align="center">
                                @if(empty($val->produk->produk_galeri_first))
                                <img src="{{ URL::asset('img/'.$gambar_default->value) }}" style="height:130px;width:auto;padding: 15px 15px 0px 15px" class="img-responsive" alt="">
                                @else
                                <img src="{{ URL::asset('img/produk/'.$val->produk->produk_galeri_first->file_gambar) }}" style="height:130px;width:auto;padding: 15px 15px 0px 15px" class="img-responsive" alt="">
                                @endif
                            </td>           
                            <td align="center"><?php echo $date1->format('d')." ".$bulan[(int)$date1->format('m')]." ".$date1->format('Y'); ?></td>
                            <td align="center"><?php echo $date2->format('d')." ".$bulan[(int)$date2->format('m')]." ".$date2->format('Y'); ?></td>
                            <td align="center">
                                <a data-toggle="modal" data-target="#ubah_produk_harga">
                                    <button class="btn btn-warning flat" onclick="ubah_produk_harga('{{ $val->id }}','{{ $val->produk->nama }}','{{ $val->diskon }}','{{ $date1s }}', '{{ $date2s }}')" data-toggle="tooltip" title="ubah" style="font-size: 15pt"><span class="fa fa-edit"></span></button>
                                </a>
                            </td>
                            <td align="center">
                                <button class="btn btn-danger flat" onclick="hapus_produk_harga('{{ $val->id }}')" data-toggle="tooltip" title="hapus" style="font-size: 15pt"><span class="fa fa-trash"></span></button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
                <div class="overlay" id="loading" style="display:none">
              <i class="fa fa-refresh fa-spin"></i>
            </div>
          </div>
    </div>
</div>

<form action="{{ url('/hapus_produk_harga') }}" method="post" id="form_hapus_produk_harga">
    {{ csrf_field() }}
    <input type="hidden" name="kategori_pelanggan_id" value="{{ $kategori_pelanggan->id }}">
    <input type="hidden" name="produk_harga_id" id="hph_produk_harga_id">
</form>

<div class="modal fade" id="tambah_harga_promosi" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content box">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="riwayatheader">Tambah Harga Promosi</h4>
            </div>
            <form class="form-horizontal" id="form_produk" method="post" action="{{ url('/tambah_produk_harga') }}" autocomplete="off">
            <div class="modal-body">
                {{ csrf_field() }}
                <input type="hidden" name="kategori_pelanggan_id" value="{{ $kategori_pelanggan->id }}">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Kategori Produk</label>
                    <div class="col-sm-6">
                        <select class="form-control select2" data-placeholder="Pilih Kategori Produk" style="width:100%" name="kategori_produk_id" id="kategori_produk_id">
                            <option value=""></option>
                            @foreach($kategori_produk as $val)
                            <option value="{{ $val->id }}">{{ $val->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama Produk</label>
                    <div class="col-sm-6">
                        <select class="form-control select2" data-placeholder="Pilih produk" style="width:100%" name="produk_id" id="produk_id">
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Awal Periode</label>
                    <div class="col-sm-6">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control pull-right" id="datepicker" name="awal_periode" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Akhir Periode</label>
                    <div class="col-sm-6">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control pull-right" id="datepicker2" name="akhir_periode" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Diskon</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="diskon" placeholder="Masukkan Nilai Diskon" onKeyPress="return numbersonly(this, event)" maxlength="3">
                    </div>
                    <label class="control-label">%</label>
                </div>
            </div><!-- /.box-body -->
            <div class="box-footer">
                <button data-dismiss="modal" class="btn btn-default flat"><span class="glyphicon glyphicon-ban-circle"></span> Batal</button>
                <button type="submit" class="btn btn-primary pull-right flat" onclick="document.getElementById('loading2').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
            </div><!-- /.box-footer -->
            </form>
            <div class="overlay" id="loading2" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ubah_produk_harga" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content box">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="riwayatheader">Ubah Produk Harga</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="post" action="{{ url('/ubah_produk_harga') }}" autocomplete="off">
            <input type="hidden" name="kategori_pelanggan_id" value="{{ $kategori_pelanggan->id }}">
            <input type="hidden" name="produk_harga_id" id="uph_produk_harga_id">
            {{ csrf_field() }}
          <div class="box-body">
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Nama Produk</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" disabled="true" id="uph_produk_nama">
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">Diskon*</label>
              <div class="col-sm-3">
                <input type="text" class="form-control" name="diskon" id="uph_diskon" placeholder="Masukkan Diskon" onKeyPress="return numbersonly(this, event)" style="text-align:right">
              </div>
              <label class="control-label">%</label>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Awal Periode*</label>
              <div class="col-sm-6">
                <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" id="uph_awal_periode" data-date-format="dd-mm-yyyy" name="awal_periode" placeholder="Masukkan awal periode">
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Akhir Periode*</label>
              <div class="col-sm-6">
                <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" id="uph_akhir_periode" data-date-format="dd-mm-yyyy" name="akhir_periode" placeholder="Masukkan akhir periode">
                </div>
              </div>
            </div>
          </div><!-- /.box-body -->
          <p>Keterangan (*) : Wajib Diisi</p>
          <div class="box-footer">
            <button data-dismiss="modal" class="btn btn-default flat"><span class="glyphicon glyphicon-ban-circle"></span> Batal</button>
            <button type="submit" class="btn btn-primary pull-right flat" onclick="document.getElementById('loading3').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
          </div><!-- /.box-footer -->
        </form>
      </div>
      <div class="overlay" id="loading3" style="display:none">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
    </div>
  </div>
</div>

@stop

<style>
.datepicker{z-index:1151 !important;}
</style>
<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<script src="{{ asset('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/select2/select2.full.min.js') }}"></script>
<script>
    $(function () {
        $("#mydatatables").DataTable();
        $("#mydatatables2").DataTable();
        $("#mydatatables3").DataTable();

        //Date picker
        $('#datepicker').datepicker({
          autoclose: true,
          todayHighlight: 1,
        });

        $('#datepicker2').datepicker({
          autoclose: true,
          todayHighlight: 1,
        });

        $(".select2").select2();

        $('#kategori_produk_id').change(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            var kategori_produk_id = document.getElementById('kategori_produk_id').value;

            $("#produk_id").val(null).trigger("change");

            $.ajax({
                type: "POST",
                url: "{{ url('/get_kategori_produk_harga_promo') }}",
                data: { kategori_produk_id:kategori_produk_id },
                success: function (data) {
                    $("#produk_id").html(data);
                },
                error: function (data) {
                    alert('terjadi kesalahan');
                }
            });
        });

        $('#uph_awal_periode').datepicker({
            autoclose: true,
            todayHighlight: 1,
        });

        $('#uph_akhir_periode').datepicker({
            autoclose: true,
            todayHighlight: 1,
        });
    });

    function ubah_produk_harga(id, produk_nama, diskon, date1, date2)
    {
        document.getElementById('uph_produk_harga_id').value    = id;
        document.getElementById('uph_produk_nama').value        = produk_nama;
        document.getElementById('uph_diskon').value             = diskon;
        document.getElementById('uph_awal_periode').value       = date1;
        document.getElementById('uph_akhir_periode').value      = date2;
    }

    function hapus_produk_harga(id)
    {
        var cek = confirm("Apakah anda yakin menghapus harga promosi?");

        if(cek){
            document.getElementById('hph_produk_harga_id').value    = id;
            document.getElementById('loading').style.display        = 'block';
            document.getElementById('form_hapus_produk_harga').submit();
        }
    }
</script>
<script type="text/javascript">
    function ubah()
    {
        document.getElementById("nama").disabled = false;
        document.getElementById("default_diskon").disabled = false;
        document.getElementById("deskripsi").disabled = false;
        
        document.getElementById("ubah").style.display = 'none';
        document.getElementById("batal").style.display = 'block';
        document.getElementById("button_submit").style.display = 'block';

        document.getElementById("astket").innerHTML = "Keterangan (*) : Wajib Diisi";
        document.getElementById("ast1").innerHTML = "*";
        document.getElementById("ast2").innerHTML = "*";
        document.getElementById("ast3").innerHTML = "*";
    }
    function batal()
    {
        document.getElementById("nama").value = document.getElementById("ori_nama").value;
        document.getElementById("default_diskon").value = document.getElementById("ori_default_diskon").value;
        document.getElementById("deskripsi").value = document.getElementById("ori_deskripsi").value;

        document.getElementById("nama").disabled = true;
        document.getElementById("default_diskon").disabled = true;
        document.getElementById("deskripsi").disabled = true;
        
        document.getElementById("ubah").style.display = 'block';
        document.getElementById("batal").style.display = 'none';
        document.getElementById("button_submit").style.display = 'none';

        document.getElementById("astket").innerHTML = "";
        document.getElementById("ast1").innerHTML = "";
        document.getElementById("ast2").innerHTML = "";
        document.getElementById("ast3").innerHTML = "";
    }
    function pilih_produk_promo(id,nama)
    {
        document.getElementById("produk_id").value = id;
        document.getElementById("produk_nama").value = nama;
    }

    $(document).ready(function(){
          $('#kategori_id').change(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            var url = "{{ url('/get_produk_table2') }}";

              $.ajax({
                type: "POST",
                url: url,
                data: $("#form_produk").serialize(),
                beforeSend: function(){
                        document.getElementById('loading2').style.display = "block";
                      },
                success: function (data) {
                    $("#content_produk").html(data);
                    document.getElementById('loading2').style.display = "none";
                },
                error: function (data) {
                    alert('ooo');
                }
            });
          });

          $('#target_pelanggan').change(function(){
            if(document.getElementById("target_pelanggan").value == 1){
              document.getElementById("label1_pilih_pelanggan").style.display = 'none';
              document.getElementById("label2_pilih_pelanggan").style.display = 'none';
              document.getElementById("button_pilih_pelanggan").style.display = 'none';
            }
            else{
              document.getElementById("label1_pilih_pelanggan").style.display = 'block';
              document.getElementById("label2_pilih_pelanggan").style.display = 'block';
              document.getElementById("button_pilih_pelanggan").style.display = 'block';
            }
          });

            $('#status_promo').change(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            var url = "{{ url('/get_produk_harga') }}";

              $.ajax({
                type: "POST",
                url: url,
                data: $("#form_produk_harga").serialize(),
                beforeSend: function(){
                        document.getElementById('loading').style.display = "block";
                      },
                success: function (data) {
                    $("#content_produk_harga").html(data);
                    document.getElementById('loading').style.display = "none";
                },
                error: function (data) {
                    alert('ooo');
                }
            });
          });

            // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // save the latest tab; use cookies if you like 'em better:
                localStorage.setItem('lastTab_detail_kategori_pelanggan1', $(this).attr('href'));
            });

            // go to the latest tab, if it exists:
            var lastTab_detail_kategori_pelanggan1 = localStorage.getItem('lastTab_detail_kategori_pelanggan1');
            
            if (lastTab_detail_kategori_pelanggan1) {
                $('[href="' + lastTab_detail_kategori_pelanggan1 + '"]').tab('show');
            }
            else
            {
                lastTab_detail_kategori_pelanggan1="#tab_1";
                $('[href="' + lastTab_detail_kategori_pelanggan1 + '"]').tab('show');
            }
      });
</script>