<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<input type="hidden" name="base_path" id="base_path" value="{{ asset('/') }}">
<input type="hidden" name="user_id_aktif" id="user_id_aktif" value="{{ $user_id_aktif }}">
<div class="row">
    <div class="col-md-12">
        <div class="box box-success flat">
            <div class="box-header with-border">
                <i class="fa fa-comments-o"></i>
                <h3 class="box-title" id="nama"></h3>
            </div>
            <div class="box-body " style="height: 400px;">
                <div class="col-md-9 chat" id="content_chat">
                    <span class="fa fa-comment-o" style="margin-top:100px; margin-left: 300px; font-size: 70pt; opacity: 0.3;">
                    </span>
                </div>
                <div class="col-md-3" style="background-color: #6b6b6b; height: 100%">
                    <input id="tags" class="form-control" style="margin-top: 10px;margin-bottom: 20px;" placeholder="cari user ...">
                    <div class="nav-tabs-custom box tab-danger flat" style="border-top: 0; height: 295px;overflow:auto;">
                        <ul class="nav nav-tabs" id="myTab">
                            <li class="active"><a href="#tab_1" data-toggle="tab">Administrator</a></li>
                            <li><a href="#tab_2" data-toggle="tab">Pelanggan</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1">
                                <table class="table table-striped">
                                    <tbody>
                                        @foreach($admin as $val)
                                        <input type="hidden" name="chat_user{{ $val->id }}" id="chat_user{{ $val->id }}" value="0">
                                        <tr>
                                            <td width="80%"><button class="btn bg-navy flat" style="width:100%" onclick="btn_chat('{{ $val->id }}', '{{ $val->name }}')">{{ $val->name }}</button></td>
                                            <td></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                              </table>
                            </div>
                            <div class="tab-pane" id="tab_2">
                                <table class="table table-striped">
                                    <tbody>
                                        @foreach($pelanggan as $val)
                                        <input type="hidden" name="chat_user{{ $val->id }}" id="chat_user{{ $val->id }}" value="0">
                                        <tr>
                                            <td width="80%"><button class="btn bg-navy flat" style="width:100%" onclick="btn_chat('{{ $val->id }}', '{{ $val->name }}')">{{ $val->name }}</button></td>
                                            <td></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
<script src="{{ asset('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script type="text/javascript">
    // $(document).ready(function(){
    //     var user_id_aktif = document.getElementById('user_id_aktif').value;
    //     if(user_id_aktif != '0')
    //     {
    //       btn_chat(user_id_aktif);
    //     }
    // });

    var output;

    function btn_chat(id, nama) 
    { 
        document.getElementById('nama').innerHTML   = nama;
        document.getElementById('tags').value       = nama;
        var cek = document.getElementById("chat_user".concat(id)).value;
        if(cek == '0')
        {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });

            $.ajax({
                type: "POST",
                url: "{{ url('/get_chat_admin') }}",
                data: { id:id },
                success: function (data) {
                    $("#content_chat").html(data);
                    document.getElementById("boxbody".concat(id)).scrollTop = document.getElementById("boxbody".concat(id)).scrollHeight;
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }
    }

  function chat_close(id)
  {
    document.getElementById("chat_user".concat(id)).value = '0';
  }

  function del_notif(id)
  { 
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    })
    var path =  document.getElementById('base_path').value;
    //save message
      var url = "{{ url('/delete_notif') }}";
      $.ajax({
          type: "POST",
          url: url,
          data : {id:id},
          dataType: 'json',
          success: function (data) {
           
            jml_notif = 0;
            $('#data_notif').html("");
            for (var i = 0; i < data.length; i++) {
                jml_notif++
                $('#data_notif').append('<li><a href="'+path+'show_chat/'+data[i].id+'">\
                <b style="color:blue">('+data[i].tanggal+') </b><b>'+ data[i].nama +': \
                </b>'+ data[i].pesan +'</a></li>');
              }
              document.getElementById('notif').innerHTML = jml_notif;
              if(jml_notif == 0)
                document.getElementById('notif_header').innerHTML = "";
              else
                document.getElementById('notif_header').innerHTML = "Anda memiliki "+jml_notif+" pesan baru";
          },
          error: function (data) {
              alert('gagal menyimpan notifikasi');
          }
      });
  }

    $( function() {
        $( "#tags" ).autocomplete({
            source: function (request, response) {
                var pelanggan = JSON.parse('<?php echo json_encode($pelanggan); ?>');
                var pelangganArray = [];

                for (var i = pelanggan.length - 1; i >= 0; i--) {
                    pelangganArray.push({ id : pelanggan[i]['id'], name : pelanggan[i]['name'], value : pelanggan[i]['name'] });
                }

                var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( request.term ), "i" );
                
                response( $.grep( pelangganArray, function( item ){
                    return matcher.test( item.name );
                }) );
            },

            select: function (e, ui) {
                btn_chat(ui.item.id, ui.item.name);
            },
        });
    });
</script>