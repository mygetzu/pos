<div class="direct-chat direct-chat-primary">
    <div class="box-body" style="height:300px;padding:10px;overflow-y: scroll;" id="boxbody{{ $users->id }}">
        @if(!empty($data))
        @for($i = 0; $i < count($data) ; $i++)
            @if(Auth::user()->id == $data[$i]['user_id'])
                <div class="direct-chat-msg right">
                    <div class="direct-chat-info clearfix">
                        <span class="direct-chat-name pull-right">{{ $data[$i]['nama'] }}</span>
                        <span class="direct-chat-timestamp pull-left">{{ $data[$i]['tanggal'] }}</span>
                    </div>
                    <div class="direct-chat-text" style="margin-right:5px; margin-left:40px;">{{ $data[$i]['pesan'] }}</div>
                </div>
            @else
                <div class="direct-chat-msg">
                    <div class="direct-chat-info clearfix">
                        <span class="direct-chat-name pull-left">{{ $data[$i]['nama'] }}</span>
                        <span class="direct-chat-timestamp pull-right">{{ $data[$i]['tanggal'] }}</span>
                    </div>
                    <div class="direct-chat-text" style="margin-left:5px; margin-right:40px;">{{ $data[$i]['pesan'] }}</div>
                </div>
            @endif
        @endfor
        @endif
        <div id="log_chatbox{{ $users->id }}"></div>
        <div id="chatbox{{ $users->id }}"></div>
    </div>
    <div id="wkwk"></div>
    <!-- /.direct-chat-msg -->
    <div class="box-footer">
        <div class="input-group">
            <input name="chatname{{ $users->id }}" type="hidden" id="chatname{{ $users->id }}" value="{{ Auth::user()->name }}" />
            <input type="text" name="msg{{ $users->id }}" id="msg{{ $users->id }}" placeholder="Tulis Pesan ..." class="form-control" onkeyup="if(event.keyCode == 13) doSend('{{ $users->id }}')" onclick="del_notif('{{ $users->id }}')">
            <span class="input-group-btn">
                <button type="submit" name="sendmsg" id="sendmsg" class="btn btn-success btn-flat" onclick="doSend('{{ $users->id }}')">Kirim</button>
            </span>
        </div>
    </div>
</div>