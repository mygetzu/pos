<table class="table table-striped" id="ajax_produk_tabless">
    <thead>
        <tr>
            <th style="text-align:center">NO</th>
            <th style="text-align:center">NAMA PRODUK</th>
            <th style="text-align:center">STOK TERCATAT</th>
            <th style="text-align:center"></th>
        </tr>
    </thead>
    <tbody>
        @foreach($produk_gudang as $key => $val)
        <tr>
            <td align="center">{{ $key+1 }}</td>
            <td>{{ $val->produk->nama }}</td>
            <td align="center">{{ $val->stok }}</td>
            <td align="center">
                <button class="btn bg-maroon flat" data-dismiss="modal" onclick="btn_pilih_produk('{{ $val->produk_id }}', '{{ $val->produk->nama }}')"><span class="fa fa-check"></span> Pilih</button>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
  $(function () {
    $("#ajax_produk_tabless").DataTable();
  });
</script>