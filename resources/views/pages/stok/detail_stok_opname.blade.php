@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<?php $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
    date_default_timezone_set("Asia/Jakarta");
    $tanggal   = new DateTime($stok_opname_header->tanggal);
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <a href="{{ url('/stok_opname') }}" class="pull-right" style="padding:0px">
                    <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                </a>
                <a href="#" target="_blank" class="btn bg-orange flat"><i class="fa fa-print"></i> Cetak</a>
                <a href="#" target="_blank" class="btn btn-info flat" style="margin-left: 5px;"><i class="fa fa-download"></i> Unduh PDF</a>
            </div>
            <div class="box-body">
                <h2 align="center">Stok Opname {{ $stok_opname_header->no_stok_opname }}</h2>
                <h4 align="center">{{ $stok_opname_header->gudang->nama }}</h4>
                <h4 align="center">{{ $tanggal->format('d')." ".$bulan[(int)$tanggal->format('m')]." ".$tanggal->format('Y') }}
                </h4>
                @foreach($produk as $key => $value)
                <hr>
                <h3>{{ $value->produk->nama }}</h3>
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th style="text-align:center">Serial Number</th>
                            <th style="text-align:center">Jumlah</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($serial_number[$key] as $value2)
                        <tr>
                            <td>{{ $value2->serial_number }}</td>
                            <td align="center">{{ $value2->jumlah }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <br>
                @endforeach
            </div>
            <div class="overlay" id="loading" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
  $(function () {
    $("#ajax_produk_tabless3").DataTable();

    $('#tanggal').datepicker({
      autoclose: true,
      todayHighlight: 1,
    });
  });
</script>
@stop