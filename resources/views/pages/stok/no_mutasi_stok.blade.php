@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <a href="{{ url('/mutasi_stok') }}" class="pull-right" style="padding:0px">
                    <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                </a>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="form-horizontal">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Gudang Asal</label>
                                <div class="col-sm-9">
                                    <input type="text" value="{{ $mutasi_stok_header->gudang_asal->nama }}" class="form-control" disabled="true">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Gudang Tujuan</label>
                                <div class="col-sm-9">
                                    <input type="text" value="{{ $mutasi_stok_header->gudang_tujuan->nama }}" class="form-control" disabled="true">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                                <label class="col-sm-3 control-label">No Mutasi Stok</label>
                                <div class="col-sm-9">
                                    <input type="text" value="{{ $mutasi_stok_header->no_mutasi_stok }}" class="form-control" disabled="true">
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                                <label class="col-sm-3 control-label">Tanggal</label>
                                <div class="col-sm-9">
                                    <?php $date = new DateTime($mutasi_stok_header->tanggal); 
                                    $tanggal = $date->format('d').'-'.$date->format('m').'-'.$date->format('Y'); ?>
                                    <input type="text" class="form-control pull-right" id="tanggal" data-date-format="dd-mm-yyyy" name="tanggal" value="{{ $tanggal }}" placeholder="Masukkan tanggal" disabled="true">
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                </div>
                <?php $indeks = 0;?>
                @foreach($produk as $val)
                    <h4>{{ $val->produk->nama }}</h4>
                    <table class="table table-striped table-bordered" id="ajax_produk_tabless3">
                        <thead>
                            <tr>
                                <th style="text-align:center">NO</th>
                                <th style="text-align:center">SERIAL NUMBER</th>
                                <th style="text-align:center">JUMLAH</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=1; ?>
                            @foreach($serial_number[$indeks] as $val2)
                            <tr>
                                <td align="center">{{ $i++ }}</td>
                                <td>{{ $val2->serial_number }}</td>
                                <td align="center">{{ $val2->jumlah }}</td>
                            </tr>
                            @endforeach
                            <?php $indeks++; ?>
                        </tbody>
                    </table>    
                @endforeach
            </div>
            <div class="overlay" id="loading" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
  $(function () {
    $(".table-bordered").DataTable();
  });
</script>
@stop