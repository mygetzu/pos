<table class="table table-striped table-bordered" id="ajax_produk_tabless">
    <thead>
        <th style="text-align:center">NO</th>
        <th style="text-align:center">NAMA PRODUK</th>
        <th style="text-align:center">STOK</th>
        <th style="text-align:center"></th>
    </thead>
    <tbody>
        @foreach($produk_gudang as $key => $value)
        <tr>
            <td align="center">{{ $key+1 }}</td>
            <td>{{ $value->produk->nama }}</td>
            <td align="center">{{ $value->stok }}</td>
            <td align="center">
                <button class="btn bg-maroon" onclick="btn_pilih_produk_klik('{{ $value->produk->id }}', '{{ $value->produk->nama }}')" data-toggle="tooltip" title="pilih" style="font-size: 15pt" data-dismiss="modal"><span class="fa fa-check"></span></button>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
  $(function () {
    $("#ajax_produk_tabless").DataTable();
  });
</script>