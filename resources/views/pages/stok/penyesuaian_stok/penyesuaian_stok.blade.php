<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <div>
                  <a href="{{ url('/tambah_penyesuaian_stok') }}">
                  <button class='btn bg-purple flat'><span class='fa fa-plus'></span> Tambah Penyesuaian Stok</button>
                </a>
                </div>
            </div>
            <div class="box-body">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <table id="mydatatables" class="table table-striped table-bordered" style="margin:0px">
                  <thead>
                      <th style="text-align:center">NO</th>
                      <th style="text-align:center">NO PENYESUAIAN STOK</th>
                      <th style="text-align:center">GUDANG</th>
                      <th style="text-align:center">TANGGAL</th>
                      <!-- <th style="text-align:center">APPROVAL BY</th>
                      <th style="text-align:center">RECEIVED BY</th>
 -->                      <th style="text-align:center"></th>
                  </thead>
                  <tbody>
                      <?php $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'); ?>
                      @foreach($penyesuaian_stok_header as $key => $value)
                      <tr>
                          <td align="center">{{ $key+1 }}</td>
                          <td align="center">{{ $value->no_penyesuaian_stok }}</td>
                          <td>{{ $value->gudang->nama }}</td>
                          <td><?php
                                  date_default_timezone_set("Asia/Jakarta");
                                  $tanggal = new DateTime($value->tanggal);
                                  $tanggals = $tanggal->format('Y-m-d');
                              ?>
                              <?php echo $tanggal->format('d')." ".$bulan[(int)$tanggal->format('m')]." ".$tanggal->format('Y')." "; ?>
                          </td>
                          <!-- <td></td>
                          <td></td> -->
                          <td align="center">
                              <a class="btn btn-info flat" href="{{ url('/detail_penyesuaian_stok/'.$value->id) }}" data-toggle="tooltip" title="detail" style="font-size: 15pt"><span class="fa fa-info-circle"></span></a>
                          </td>
                      </tr>
                      @endforeach
                  </tbody>
              </table>
            </div>
        </div>
    </div>
</div>

<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script>
  $(function () {
    $("#mydatatables").DataTable();
  });
</script>

@stop