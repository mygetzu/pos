@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<?php $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'); ?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <a href="{{ url('/penyesuaian_stok') }}" class="pull-right" style="padding:0px">
                    <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                </a>
            </div>
            <div class="box-body">
                <div class="box box-solid box-default">
                    <div class="box-header">
                        <h3 class="box-title">Penyesuaian Stok</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <th width="20%">No Penyesuaian Stok</th>
                                    <th width="5%">:</th>
                                    <td>{{ $penyesuaian_stok_header->no_penyesuaian_stok }}</td>
                                </tr>
                                <tr>
                                    <th>Gudang</th>
                                    <th>:</th>
                                    <td>{{ $penyesuaian_stok_header->gudang->id }}</td>
                                </tr>
                                <tr>
                                    <th>Tanggal</th>
                                    <th>:</th>
                                    <td><?php
                                  date_default_timezone_set("Asia/Jakarta");
                                  $tanggal = new DateTime($penyesuaian_stok_header->tanggal);
                                  $tanggals = $tanggal->format('Y-m-d');
                              ?>
                              <?php echo $tanggal->format('d')." ".$bulan[(int)$tanggal->format('m')]." ".$tanggal->format('Y')." "; ?></td>
                                </tr>
                                <tr>
                                    <th>Keterangan</th>
                                    <th>:</th>
                                    <td>{{ $penyesuaian_stok_header->keterangan }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="text-align:center">NO</th>
                            <th style="text-align:center">NAMA PRODUK</th>
                            <th style="text-align:center">SERIAL NUMBER</th>
                            <th style="text-align:center">STOK AWAL</th>
                            <th style="text-align:center">PENYESUAIAN STOK</th>
                            <th style="text-align:center">STOK AKHIR</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($penyesuaian_stok_detail as $key => $value)
                        <tr>
                            <td align="center">{{ $key+1 }}</td>
                            <td>{{ $value->produk->nama }}</td>
                            <td>{{ $value->serial_number }}</td>
                            <td>{{ $value->stok_awal }}</td>
                            <td>{{ $value->penyesuaian_stok }}</td>
                            <td>{{ $value->stok_akhir }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
@endsection