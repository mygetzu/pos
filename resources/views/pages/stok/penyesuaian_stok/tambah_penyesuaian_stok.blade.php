@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<?php
    $tanggal = date('d-m-Y');
?>
<input type="hidden" name="jumlah_pesanan" id="jumlah_pesanan" value="">
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <a href="{{ url('/penyesuaian_stok') }}" class="pull-right" style="padding:0px">
                    <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                </a>
            </div>
            <div class="box-body">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <div class="form-horizontal" autocomplete="off">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('no_penyesuaian_stok') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label">No Penyesuaian</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" placeholder="Masukkan no penyesuaian stok" id="no_penyesuaian_stok" value="{{ $no_penyesuaian_stok }}">
                                    @if ($errors->has('no_penyesuaian_stok'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('no_penyesuaian_stok') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Gudang</label>
                                <div class="col-md-9">
                                    <select class="form-control" id="gudang_id">
                                        <option value="">-- Pilih Gudang --</option>
                                        @foreach($gudang as $val)
                                            <option value="{{ $val->id }}">{{ $val->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-9 col-md-offset-3">
                                    <button class="btn btn-primary" onclick="btn_lanjut_klik()" id="btn_lanjut"><span class="fa fa-arrow-right"></span> Lanjut</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Tanggal</label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right" id="tanggal" data-date-format="dd-mm-yyyy" value="{{ $tanggal }}" placeholder="Masukkan tanggal">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Keterangan</label>
                                <div class="col-md-9">
                                    <textarea class="form-control" id="keterangan"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
                <hr>
                <div class="form-horizontal">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-md-2 control-label">Produk</label>
                            <div class="col-md-4">
                                <input type="text" id="display_produk_nama" class="form-control" disabled="true">
                                <input type="hidden" name="produk_id" id="produk_id">
                                <input type="hidden" name="produk_nama" id="produk_nama">
                            </div>
                            <div class="col-md-4">
                                <a data-toggle="modal" data-target="#modal_pilih_produk" class="btn bg-maroon"><span class="fa fa-cube"></span> Pilih Produk</a>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Serial Number</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" onkeyup="if(event.keyCode == 13) tambah_serial_number()" placeholder="Masukkan serial number, tekan enter" id="input_serial_number">
                            </div>
                            <div class="col-md-4">
                                <a data-toggle="modal" data-target="#modal_pilih_serial_number" class="btn bg-maroon"><span class="fa fa-barcode"></span> Pilih Serial Number</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <button class="btn btn-success pull-right" onclick="btn_selesai_klik()"><span class="fa fa-check"></span> Selesai</button>
                </div>
                <hr>
                <div class="col-md-12">
                    <form action="{{ url('do_tambah_penyesuaian_stok') }}" method="post" id="form_do_tambah_penyesuaian_stok">
                    <input type="hidden" name="no_penyesuaian_stok" id="form_no_penyesuaian_stok">
                    <input type="hidden" name="gudang_id" id="form_gudang_id">
                    <input type="hidden" name="tanggal" id="form_tanggal">
                    <input type="hidden" name="keterangan" id="form_keterangan">
                    <input type="hidden" name="counter" id="counter" value="0">
                    <table class="table table-striped table-bordered" id="tabel_penyesuaian_stok">
                        <thead>
                            <tr>
                                <th style="text-align:center">NAMA PRODUK</th>
                                <th style="text-align:center">SERIAL NUMBER</th>
                                <th style="text-align:center">STOK AWAL</th>
                                <th style="text-align:center" width="15%">PENYESUAIAN STOK</th>
                                <th style="text-align:center"></th>
                            </tr>
                        </thead>
                    </table>
                    </form>
                </div>
            </div>
            <div class="overlay" id="loading" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_pilih_produk" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content box">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Produk</h4>
            </div>
            <div class="modal-body">
                <div id="data_penyesuaian_stok_get_produk">
                </div>
            </div>
            <div class="overlay" id="loading2" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_pilih_serial_number" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content box">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Serial Number</h4>
            </div>
            <div class="modal-body">
                <div id="data_penyesuaian_stok_get_serial_number">
                </div>
            </div>
            <div class="overlay" id="loading3" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<div>
    <form>
        
    </form>
</div>

<div id="data_serial_number"></div>

<script src="{{ asset('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script type="text/javascript">
    function btn_lanjut_klik()
    {
        var no_penyesuaian_stok = document.getElementById('no_penyesuaian_stok').value;
        var gudang_id           = document.getElementById('gudang_id').value;
        var tanggal             = document.getElementById('tanggal').value;
        var keterangan          = document.getElementById('keterangan').value;

        if(no_penyesuaian_stok == "" || gudang_id == ""){
            alert('No penyesuaian stok dan gudang wajib diisi');
            return;
        }

        document.getElementById('form_no_penyesuaian_stok').value   =  no_penyesuaian_stok;
        document.getElementById('form_gudang_id').value             =  gudang_id;
        document.getElementById('form_tanggal').value               =  tanggal;
        document.getElementById('form_keterangan').value            =  keterangan;

        document.getElementById('no_penyesuaian_stok').disabled = true;
        document.getElementById('gudang_id').disabled           = true;
        document.getElementById('tanggal').disabled             = true;
        document.getElementById('keterangan').disabled          = true;
        document.getElementById('btn_lanjut').disabled          = true;

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $.ajax({
            type: "POST",
            url: "{{ url('/penyesuaian_stok_get_produk') }}",
            data: { gudang_id: gudang_id },
            success: function (data) {
                $("#data_penyesuaian_stok_get_produk").html(data);
            },
            error: function (data) {
                //alert('terjadi kesalahan pengambilan data');
            }
        });
    }

    function btn_pilih_produk_klik(id, nama)
    {
        document.getElementById('display_produk_nama').value    = nama;
        document.getElementById('produk_id').value              = id;
        document.getElementById('produk_nama').value            = nama;
        var gudang_id = document.getElementById('form_gudang_id').value;

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $.ajax({
            type: "POST",
            url: "{{ url('/penyesuaian_stok_get_serial_number') }}",
            data: { gudang_id : gudang_id,  produk_id : id},
            success: function (data) {
                $("#data_penyesuaian_stok_get_serial_number").html(data);
            },
            error: function (data) {
                //alert('terjadi kesalahan pengambilan data');
            }
        });
    }

    $(function () {
        $('#tanggal').datepicker({
            autoclose: true,
            todayHighlight: 1,
        });
    });

    function btn_pilih_serial_number_klik(serial_number, stok)
    {
        var produk_nama = document.getElementById('produk_nama').value;
        var produk_id   = document.getElementById('produk_id').value;
        var counter     = document.getElementById('counter').value;
        counter = parseInt(counter);

        //cek produk_id dan serial_number jika sudah ada maka gk perlu ditambahkan
        var cek = 0;
        for (var i = 0; i < counter; i++) {
            if(document.getElementById('tabel_produk_id'.concat(i))){
                cek_produk_id = document.getElementById('tabel_produk_id'.concat(i)).value;
                if(cek_produk_id == produk_id){
                    cek = 1;
                }
            }

            if(document.getElementById('tabel_serial_number'.concat(i))){
                cek_serial_number = document.getElementById('tabel_serial_number'.concat(i)).value;
                if(cek == 1 && cek_serial_number == serial_number){
                    return;
                }
            }
        }

        //tambah bari baru
        var table   = document.getElementById("tabel_penyesuaian_stok");
        var row     = table.insertRow(1);

        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        var cell4 = row.insertCell(3);
        var cell5 = row.insertCell(4);

        cell1.innerHTML = produk_nama;
        cell2.innerHTML = serial_number;
        cell3.innerHTML = stok;
        cell4.innerHTML = '<input type="hidden" name="produk_id'+counter+'" id="tabel_produk_id'+counter+'" value="'+produk_id+'">\
        <input type="hidden" name="serial_number'+counter+'" id="tabel_serial_number'+counter+'" value="'+serial_number+'">\
        <input type="number" name="stok_ubah'+counter+'" id="" class="form-control" value="0">';
        cell5.innerHTML = '<button class="btn btn-danger" onclick="hapus_penyesuaian_stok(this)"><span class="fa fa-trash-o"><span> Hapus</button>';
        
        cell3.setAttribute("align", "center");
        cell4.setAttribute("align", "center");
        cell5.setAttribute("align", "center");

        counter++;
        document.getElementById('counter').value = counter;
    }

    function hapus_penyesuaian_stok(obj)
    {
        var cek = confirm('apakah anda yakin?');

        if(cek){
            //hapus row table
            var row = obj.closest("tr");
            document.getElementById("tabel_penyesuaian_stok").deleteRow(row.rowIndex);
        }
    }

    function tambah_serial_number()
    {
        var produk_nama = document.getElementById('produk_nama').value;
        var produk_id   = document.getElementById('produk_id').value;
        var counter     = document.getElementById('counter').value;
        counter = parseInt(counter);

        var serial_number = document.getElementById('input_serial_number').value;

        if(serial_number == ""){
            alert('masukkan serial number terlebih dahulu');
            return;
        }

        //cek produk_id dan serial_number jika sudah ada maka gk perlu ditambahkan
        var cek = 0;
        for (var i = 0; i < counter; i++) {
            if(document.getElementById('tabel_produk_id'.concat(i))){
                cek_produk_id = document.getElementById('tabel_produk_id'.concat(i)).value;
                if(cek_produk_id == produk_id){
                    cek = 1;
                }
            }

            if(document.getElementById('tabel_serial_number'.concat(i))){
                cek_serial_number = document.getElementById('tabel_serial_number'.concat(i)).value;
                if(cek == 1 && cek_serial_number == serial_number){
                    document.getElementById('input_serial_number').value = "";
                    return;
                }
            }
        }

        var gudang_id = document.getElementById('form_gudang_id').value;

        //cek dari produk by ajax
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $.ajax({
            type: "POST",
            url: "{{ url('/penyesuaian_stok_cek_serial_number') }}",
            data: { gudang_id : gudang_id,  produk_id : produk_id, serial_number : serial_number },
            success: function (data) { 
                if(data == "null"){
                    var cek2 = confirm("serial number tidak ditemukan, apakah anda ingin menambahkan?");

                    if(!cek2){
                        return;
                    }

                    stok = 0;
                }
                else{
                    stok = data;
                }

                //tambah bari baru
                var table   = document.getElementById("tabel_penyesuaian_stok");
                var row     = table.insertRow(1);

                var cell1 = row.insertCell(0);
                var cell2 = row.insertCell(1);
                var cell3 = row.insertCell(2);
                var cell4 = row.insertCell(3);
                var cell5 = row.insertCell(4);

                cell1.innerHTML = produk_nama;
                cell2.innerHTML = serial_number;
                cell3.innerHTML = stok;
                cell4.innerHTML = '<input type="text" name="produk_id'+counter+'" id="tabel_produk_id'+counter+'" value="'+produk_id+'">\
                <input type="text" name="serial_number'+counter+'" id="tabel_serial_number'+counter+'" value="'+serial_number+'">\
                <input type="number" name="stok_ubah'+counter+'" class="form-control" value="0">';
                cell5.innerHTML = '<button class="btn btn-danger" onclick="hapus_penyesuaian_stok(this)"><span class="fa fa-trash-o"><span> Hapus</button>';
                
                cell3.setAttribute("align", "center");
                cell4.setAttribute("align", "center");
                cell5.setAttribute("align", "center");

                counter++;
                document.getElementById('counter').value = counter;

                document.getElementById('input_serial_number').value = "";
            },
            error: function (data) {
                document.getElementById('input_serial_number').value = "";
            }
        });
    }

    function btn_selesai_klik()
    {
        var cek = confirm("apakah penyesuaian stok sudah selesai?");

        if(cek){
            document.getElementById('form_do_tambah_penyesuaian_stok').submit();
            document.getElementById('loading').style.display = 'block';
        }
    }
</script>
@stop