<table class="table table-striped table-bordered" id="ajax_produk_tabless2">
    <thead>
        <th style="text-align:center">NO</th>
        <th style="text-align:center">SERIAL NUMBER</th>
        <th style="text-align:center">STOK</th>
        <th style="text-align:center"></th>
    </thead>
    <tbody>
        @foreach($produk_serial_number as $key => $value)
        <tr>
            <td align="center">{{ $key+1 }}</td>
            <td>{{ $value->serial_number }}</td>
            <td align="center">{{ $value->stok }}</td>
            <td align="center">
                <button class="btn bg-maroon" onclick="btn_pilih_serial_number_klik('{{ $value->serial_number }}', '{{ $value->stok }}')" data-toggle="tooltip" title="pilih" style="font-size: 15pt" data-dismiss="modal"><span class="fa fa-check"></span></button>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
  $(function () {
    $("#ajax_produk_tabless2").DataTable();
  });
</script>