<?php $i=1; $j=1; $produk_id = ""; ?>
<hr>
{{ csrf_field() }}
<table class="table table-striped" id="ajax_produk_tabless">
    <thead>
        <tr>
            <th style="text-align:center">NO</th>
            <th style="text-align:center">NAMA PRODUK</th>
            <th style="text-align:center">STOK</th>
            <th style="text-align:center"></th>
        </tr>
    </thead>
    <tbody>
        @foreach($produk_gudang as $val)
        <tr>
            <td align="center">{{ $i++ }}</td>
            <td>{{ $val->nama }}</td>
            <td align="center">
                <label class="control-label" id="jumlah_stok{{ $i }}">0</label>
            </td>
            <td align="center">
                <div id="div_tambah_stok{{ $i }}" style="display:block">
                <a data-toggle="modal" data-target="#tambah_stok">
                    <button class="btn bg-purple flat" onclick="tambah_stok('{{ $val->produk_id }}','{{ $i }}')"><span class="fa fa-plus-circle"></span> Tambah Stok</button>
                </a>
                </div>
                <div id="div_detail_stok{{ $i }}" style="display:none">
                <a data-toggle="modal" data-target="#tambah_stok">
                    <button class="btn btn-info flat" onclick="detail_stok('{{ $val->produk_id }}','{{ $i }}')"><span class="fa fa-plus-circle"></span> Detail Stok</button>
                </a>
                </div>
                <input type="hidden" id="status_produk{{ $i }}" value="0">
            </td>
        </tr>
        <?php $j++  ?>
        @endforeach
    </tbody>
</table>
<input type="hidden" id="total_produk" value="{{ (int)$i-1 }}">
<br>
<form action="{{ url('/do_tambah_stok_opname') }}" method="post">
{{ csrf_field() }}
<input type="hidden" name="gudang_id" id="gudang_id" value="{{ $id }}">
<input type="hidden" name="form_id_tmp" id="form_id_tmp">
<div id="button_simpan" style="display:none">
    <button class="btn btn-primary flat pull-right" type="submit" onclick="document.getElementById('loading').style.display = 'block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
</div>
</form>
<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
  $(function () {
    $("#ajax_produk_tabless").DataTable();
  });
</script>