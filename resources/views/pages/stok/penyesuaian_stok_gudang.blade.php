<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<style>  

input[type=number]::-webkit-inner-spin-button {
    opacity: 1
}

</style>
<script type="text/javascript">
    function btn_simpan()
    {
        var cek = confirm('Apakah Data Sudah Sesuai?');

        if(cek)
        {
            document.getElementById("form_penyesuaian_stok").submit();
            document.getElementById('loading').style.display='block'
        }
    }
</script>
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <a class="pull-right" href="{{ url('/penyesuaian_stok') }}">
                      <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                  </a>
            </div>
            <div class="box-body">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                @if ($errors->has('gudang_id_baru'))
                <div class="alert alert-danger flat">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    Pilihan Lokasi Wajib Diisi
                </div>
                @endif
                <?php $i=1; ?>
                <form action="{{ url('/do_penyesuaian_stok') }}" method="post" id="form_penyesuaian_stok">
                {{ csrf_field() }}
                <input type="hidden" name="gudang_id" value="{{ $my_gudang_id }}">
                <table id="mydatatables" class="table table-striped table-bordered">
                    <thead>
                        <th style="text-align:center">NO</th>
                        <th style="text-align:center">NAMA PRODUK</th>
                        <th style="text-align:center">SERIAL NUMBER</th>
                        <th style="text-align:center">STOK</th>
                        <th style="text-align:center">PENYESUAIAN STOK</th>
                    </thead>
                    <tbody>
                        @foreach($produk_serial_number as $val)
                        <tr>
                            <td align="center">{{ $i++ }}</td>
                            <td>{{ $val->nama }}</td>
                            <td>{{ $val->serial_number }}</td>
                            <td align="center">{{ $val->stok }}</td>
                            <td align="center">
                                <input type="hidden" name="produk_id{{ $i }}" value="{{ $val->produk_id }}">
                                <input type="hidden" name="serial_number{{ $i }}" value="{{ $val->serial_number }}">
                                <input type="hidden" name="stok_awal{{ $i }}" value="{{ $val->stok }}">
                                <input type="number" class="form-control" name="stok_ubah{{ $i }}" value="0" style="opacity: 1;">
                            </td></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                </form>
                <hr>
                <button class="btn btn-primary flat pull-right" type="button" onclick="btn_simpan()" style="margin-right:45px"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
                <br><br><br><br>
            </div>
            <div class="overlay" id="loading" style="display:none">
              <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>
@stop
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script>
  $(function () {
    $("#mydatatables").DataTable({
      "paging": true});
  });
</script>