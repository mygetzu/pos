@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <a href="{{ url('/mutasi_stok') }}" class="pull-right" style="padding:0px">
                    <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                </a>
            </div>
            <div class="box-body">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <div class="form-horizontal">
                    {{ csrf_field() }}
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Gudang Asal</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="gudang_asal" id="gudang_asal">
                                    <option value="">-- Pilih Gudang Asal --</option>
                                    @foreach($gudang as $val)
                                        <option value="{{ $val->id }}">{{ $val->nama }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Gudang Tujuan</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="gudang_tujuan" id="gudang_tujuan">
                                    <option value="">-- Pilih Gudang Tujuan --</option>
                                    @foreach($gudang as $val)
                                        <option value="{{ $val->id }}">{{ $val->nama }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3">
                                <button class="btn btn-primary flat" onclick="btn_lanjut()" id="btn_lanjut"><span class="fa fa-arrow-right"></span> Lanjut</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                            <label class="col-sm-3 control-label">No Mutasi Stok</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Masukkan no mutasi stok" name="no_mutasi_stok" id="no_mutasi_stok" value="">
                                @if ($errors->has('no_surat_jalan'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('no_surat_jalan') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                            <label class="col-sm-3 control-label">Tanggal</label>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <?php $date = new DateTime(); 
                                    $tanggal = $date->format('d').'-'.$date->format('m').'-'.$date->format('Y'); ?>
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="tanggal" data-date-format="dd-mm-yyyy" name="tanggal" value="{{ $tanggal }}" placeholder="Masukkan tanggal">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>         
            </div>
            <div class="overlay" id="loading" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<div class="row" id="display_mutasi_stok" style="display: none;">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-body">
                <div class="form-horizontal">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Produk</label>
                            <div class="col-sm-9">
                                <input type="text" id="display_produk_nama" class="form-control" disabled="true">
                                <input type="hidden" name="produk_id" id="produk_id">
                                <input type="hidden" name="produk_nama" id="produk_nama">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Serial Number</label>
                            <div class="col-sm-9">
                                <input type="text" name="serial_number" class="form-control" onkeyup="if(event.keyCode == 13) do_mutasi()" placeholder="Masukkan serial number, tekan enter" id="serial_number">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Jumlah Mutasi</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" value="1" id="jumlah_mutasi" onKeyPress="return numbersonly(this, event)" >
                                <input type="hidden" id="maks_jumlah_mutasi">
                            </div>
                            <label class="col-sm-3 control-label">Total Stok</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" id="stok_serial_number" disabled="true">
                            </div>
                        </div>  
                        <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3">
                                <button type="button" class="btn btn-primary flat" onclick="do_mutasi()"><span class="fa fa-file-text-o"></span> Masukkan List</button>
                            </div>
                        </div>    
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <a data-toggle="modal" data-target="#modal_pilih_produk" class="btn bg-maroon flat"><span class="fa fa-cube"></span> Pilih Produk</a>
                        </div>
                        <div class="form-group">
                            <a data-toggle="modal" data-target="#modal_pilih_serial_number" class="btn btn-primary flat"><span class="fa fa-plus-circle"></span> Pilih Serial Number</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <button type="button" class="btn btn-success flat pull-right" onclick="btn_selesai()"><span class="fa fa-check"></span> Selesai</button>
                    <br>
                    <hr>
                </div>
                <div class="col-md-12">
                    <table class="table table-striped table-bordered" id="table_mutasi_stok">
                        <thead>
                            <tr>
                                <th style="text-align:center">NAMA PRODUK</th>
                                <th style="text-align:center">SERIAL NUMBER</th>
                                <th style="text-align:center">JUMLAH MUTASI</th>
                                <th style="text-align:center"></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="overlay" id="loading2" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_pilih_produk" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content box">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Produk</h4>
            </div>
            <div class="modal-body">
                <div id="mutasi_stok_get_produk">
                </div>
            </div>
            <div class="overlay" id="loading3" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_pilih_serial_number" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content box">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Serial Number</h4>
            </div>
            <div class="modal-body">
                <div id="mutasi_stok_get_serial_number">
                </div>
            </div>
            <div class="overlay" id="loading3" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<form method="post" action="{{ url('/do_tambah_mutasi_stok') }}" id="form_do_tambah_mutasi_stok">
    {{ csrf_field() }}
    <input type="hidden" name="gudang_asal" id="input_gudang_asal">
    <input type="hidden" name="gudang_tujuan" id="input_gudang_tujuan">
    <input type="hidden" name="no_mutasi_stok" id="input_no_mutasi_stok">
    <input type="hidden" name="tanggal" id="input_tanggal">
    <input type="hidden" name="total_mutasi" id="total_mutasi" value="0">
    <div id="produk_serial_number_mutasi"></div>
</form>


<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
  $(function () {
    $("#ajax_produk_tabless3").DataTable();

    $('#tanggal').datepicker({
      autoclose: true,
      todayHighlight: 1,
    });
  });

    function btn_lanjut()
    {
        var gudang_asal     = document.getElementById('gudang_asal').value;
        var gudang_tujuan   = document.getElementById('gudang_tujuan').value;

        if(gudang_asal == gudang_tujuan){
            alert('gudang asal dan gudang tujuan tidak boleh sama');
        }
        else{
            document.getElementById('input_gudang_asal').value      = gudang_asal;
            document.getElementById('input_gudang_tujuan').value    = gudang_tujuan;
            document.getElementById('input_no_mutasi_stok').value   = document.getElementById('no_mutasi_stok').value;
            document.getElementById('input_tanggal').value          = document.getElementById('tanggal').value;

            document.getElementById('gudang_asal').disabled     = 'true';
            document.getElementById('gudang_tujuan').disabled   = 'true';
            document.getElementById('no_mutasi_stok').disabled  = 'true';
            document.getElementById('tanggal').disabled         = 'true';
            document.getElementById('btn_lanjut').disabled      = 'true';

            document.getElementById('display_mutasi_stok').style.display = 'block';

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            var url = "{{ url('/mutasi_stok_get_produk') }}";

            $.ajax({
                type: "POST",
                url: url,
                data: { gudang_asal:gudang_asal },
                success: function (data) {
                    $("#mutasi_stok_get_produk").html(data);
                },
                error: function (data) {
                    alert('ooo');
                }
            });   
        }
    }

    function btn_pilih_produk(produk_id, produk_nama)
    {
        document.getElementById('produk_id').value              = produk_id;
        document.getElementById('produk_nama').value            = produk_nama;
        document.getElementById('display_produk_nama').value    = produk_nama;

        var gudang_asal = document.getElementById('input_gudang_asal').value;

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })

        var url = "{{ url('/mutasi_stok_get_serial_number') }}";

        $.ajax({
            type: "POST",
            url: url,
            data: { gudang_asal:gudang_asal, produk_id:produk_id },
            success: function (data) {
                $("#mutasi_stok_get_serial_number").html(data);
            },
            error: function (data) {
                alert('ooo');
            }
        });   
    }

    function btn_pilih_serial_number(serial_number, stok)
    {
        document.getElementById('serial_number').value = serial_number;
        document.getElementById('maks_jumlah_mutasi').value = stok;
        document.getElementById('stok_serial_number').value = stok;
    }

    function do_mutasi()
    {
        var produk_id           = document.getElementById('produk_id').value;
        var produk_nama         = document.getElementById('produk_nama').value;
        var serial_number       = document.getElementById('serial_number').value;
        var jumlah_mutasi       = document.getElementById('jumlah_mutasi').value;
        var maks_jumlah_mutasi  = document.getElementById('maks_jumlah_mutasi').value;

        jumlah_mutasi       = parseInt(jumlah_mutasi);
        maks_jumlah_mutasi  = parseInt(maks_jumlah_mutasi);
        
        //cek serial number terdaftar atau tidak
        var total_serial_number = document.getElementById('total_serial_number').value;
        total_serial_number = parseInt(total_serial_number);

        var cek_ada_serial_number = 0;
        for (var i = total_serial_number - 1; i >= 0; i--) {
            var list_serial_number = document.getElementById('list_serial_number'.concat(i)).value;
            if(list_serial_number == serial_number){
                cek_ada_serial_number = 1;
                break;
            }
        }
        
        if(cek_ada_serial_number == 0){
            alert('serial number tidak terdaftar');
            document.getElementById('serial_number').value = "";
            document.getElementById('jumlah_mutasi').value = 1;
            return;
        }

        //cek jumlah mutasi melebihi stok atau tidak
        var total_mutasi = document.getElementById("total_mutasi").value;
        total_mutasi = parseInt(total_mutasi);
        var cek_jumlah = 0;

        for (var i = total_mutasi - 1; i >= 0; i--) {
            var cek_produk_id       = document.getElementById("mutasi_produk".concat(i)).value;
            var cek_serial_number   = document.getElementById("mutasi_serial_number".concat(i)).value;
            
            if(cek_produk_id == produk_id && cek_serial_number == serial_number){
                var cek_jumlah_mutasi = document.getElementById("mutasi_jumlah".concat(i)).value;
                cek_jumlah_mutasi = parseInt(cek_jumlah_mutasi);
                cek_jumlah = cek_jumlah + cek_jumlah_mutasi;
                
            }
        }
        
        if(jumlah_mutasi+cek_jumlah > maks_jumlah_mutasi){
            alert('jumlah mutasi tidak boleh melebihi stok');
            document.getElementById('serial_number').value = "";
            document.getElementById('jumlah_mutasi').value = 1;
            return;
        }

        var total_mutasi = document.getElementById("total_mutasi").value;
        total_mutasi = parseInt(total_mutasi);

        //tambah input di form yang isinya produk, serial number, jumlah mutasi
        $("#produk_serial_number_mutasi").append("<input type='hidden' name='mutasi_produk"+total_mutasi+"' id='mutasi_produk"+total_mutasi+"' value='"+produk_id+"'>\
            <input type='hidden' name='mutasi_serial_number"+total_mutasi+"' id='mutasi_serial_number"+total_mutasi+"' value='"+serial_number+"'>\
            <input type='hidden' name='mutasi_jumlah"+total_mutasi+"' id='mutasi_jumlah"+total_mutasi+"' value='"+jumlah_mutasi+"'>");

        //tampilkan di tabel
        var table   = document.getElementById("table_mutasi_stok");
        var row     = table.insertRow(1);

        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        var cell4 = row.insertCell(3);

        cell1.innerHTML = produk_nama;
        cell2.innerHTML = serial_number;
        cell3.innerHTML = jumlah_mutasi;
        cell4.innerHTML = '<button class="btn btn-danger flat" onclick="hapus_mutasi_klik(\''+total_mutasi+'\',this)"><span class="fa fa-trash-o"><span> Hapus</button>';
        
        cell3.setAttribute("align", "center");
        cell4.setAttribute("align", "center");

        document.getElementById('serial_number').value = "";
        document.getElementById('jumlah_mutasi').value = 1;

        total_mutasi++;
        document.getElementById("total_mutasi").value = total_mutasi;
    }

    function hapus_mutasi_klik(id, obj)
    {
        var cek = confirm('apakah anda yakin?');

        if(cek){
            //hapus row table
            var row = obj.closest("tr");
            document.getElementById("table_mutasi_stok").deleteRow(row.rowIndex);

            //kosongkan value input form
            document.getElementById('mutasi_produk'.concat(id)).value           = "";
            document.getElementById('mutasi_serial_number'.concat(id)).value    = "";
            document.getElementById('mutasi_jumlah'.concat(id)).value           = "";
        }
    }

    function btn_selesai()
    {
        var cek = confirm('apakah mutasi sudah sesuai?');

        if(cek){
            document.getElementById('loading').style.display = 'block';
            document.getElementById('loading2').style.display = 'block';

            document.getElementById('form_do_tambah_mutasi_stok').submit();
        }
    }

    $(document).ready(function(){
        $('#gudang_asal').change(function(){ 
            var gudang_id = document.getElementById('gudang_asal').value;

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            $.ajax({
                type: "POST",
                url: "{{ url('/get_format_no_mutasi') }}",
                data: { gudang_id : gudang_id },
                success: function (data) {
                    document.getElementById('no_mutasi_stok').value = data;
                },
                error: function (data) {
                    alert('ooo');
                }
            });    
        });
    });
</script>
@stop