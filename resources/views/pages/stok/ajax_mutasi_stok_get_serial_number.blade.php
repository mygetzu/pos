<table class="table table-striped">
    <thead>
        <tr>
            <th style="text-align:center">NO</th>
            <th style="text-align:center">SERIAL NUMBER</th>
            <th style="text-align:center">STOK</th>
            <th style="text-align:center"></th>
        </tr>
    </thead>
    <tbody>
        @foreach($produk_serial_number as $key => $val)
        <tr>
            <td align="center">{{ $key+1 }}</td>
            <td>
                {{ $val->serial_number }}
                <input type="hidden" id="list_serial_number{{ $key }}" value="{{ $val->serial_number }}">
            </td>
            <td align="center">{{ $val->stok }}</td>
            <td>
                <button class="btn bg-maroon flat" data-dismiss="modal" onclick="btn_pilih_serial_number('{{ $val->serial_number }}', '{{ $val->stok }}')"><span class="fa fa-check"></span> Pilih</button>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<input type="hidden" id="total_serial_number" value="{{ count($produk_serial_number) }}">

<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
  $(function () {
    $("#ajax_produk_tabless2").DataTable();
  });
</script>