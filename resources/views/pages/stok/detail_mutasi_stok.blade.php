<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="nav-tabs-custom tab-primary flat">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">Retur Jual</a></li>
              <li><a href="#tab_2" data-toggle="tab"></a></li>
              <li class="pull-right">
                <a href="{{ url('/retur_jual') }}" class="pull-right" style="padding:0px">
                    <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                </a>
              </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                    <table class="table table-striped">
                    <tr>
                        <th width="20%">No Mutasi</th>
                        <th width="5%">:</th>
                        <td>
                            <div class="col-md-6 row">
                                <input type="text" class="form-control" value="{{ $mutasi_stok_header->no_mutasi_stok }}" disabled="true">
                            </div>
                            <div class="col-md-4">
                                <a href="{{ url('/sj_mutasi_stok/'.$mutasi_stok_header->id) }}" target="_blank" class="btn btn-info flat"><i class="fa fa-file-text"></i> Surat Jalan Mutasi</a>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>Tanggal</th>
                        <th>:</th>
                            <?php
                            $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
                            date_default_timezone_set("Asia/Jakarta");
                            $tanggal = new DateTime($mutasi_stok_header->tanggal);
                            ?>
                        <td><input type="text" class="form-control" name="alamat" id="alamat" value="{{ $tanggal->format('d') }} {{ $bulan[(int)$tanggal->format('m')] }} {{ $tanggal->format('Y') }} {{ $tanggal->format('G') }}:{{ $tanggal->format('i') }}:{{ $tanggal->format('s') }}" disabled="true"></td>
                    </tr>
                    <tr>
                        <th>Gudang Asal</th>
                        <th>:</th>
                        <td><input type="text" class="form-control" value="{{ $mutasi_stok_header->gudang_asal->nama }}" disabled="true"></td>
                    </tr>
                    <tr>
                        <th>Gudang Tujuan</th>
                        <th>:</th>
                        <td><input type="text" class="form-control" value="{{ $mutasi_stok_header->gudang_tujuan->nama }}" disabled="true"></td>
                    </tr>
                </table>
                <hr>
                <?php $indeks = 0;?>
                @foreach($produk as $val)
                    <h4>{{ $val->produk->nama }}</h4>
                    <table class="table table-striped table-bordered" id="ajax_produk_tabless3">
                        <thead>
                            <tr>
                                <th style="text-align:center">NO</th>
                                <th style="text-align:center">SERIAL NUMBER</th>
                                <th style="text-align:center">JUMLAH</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=1; ?>
                            @foreach($serial_number[$indeks] as $val2)
                            <tr>
                                <td align="center">{{ $i++ }}</td>
                                <td>{{ $val2->serial_number }}</td>
                                <td align="center">{{ $val2->jumlah }}</td>
                            </tr>
                            @endforeach
                            <?php $indeks++; ?>
                        </tbody>
                    </table>    
                @endforeach
                </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
        
    </div>
</div>
@stop
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script>
  $(function () {
    $("#mydatatables").DataTable();
    $(".table-bordered").DataTable();
  });
</script>