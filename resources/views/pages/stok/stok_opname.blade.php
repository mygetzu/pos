<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<?php $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
    date_default_timezone_set("Asia/Jakarta");
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <a href="{{ url('/tambah_stok_opname') }}">
                  <button class='btn bg-purple flat'><span class='fa fa-plus'></span> Tambah Stok Opname</button>
                </a>
            </div>
            <div class="box-body">
                <table id="mydatatables" class="table table-striped table-bordered">
                    <thead>
                        <th style="text-align:center">NO</th>
                        <th style="text-align:center">GUDANG</th>
                        <th style="text-align:center">TANGGAL</th>
                        <th style="text-align:center">TOTAL STOK</th>
                        <th style="text-align:center">APPROVAL BY</th>
                        <th></th>
                    </thead>
                    <tbody>
                        <?php $i=1; ?>
                        @foreach($stok_opname as $val)
                        <tr>
                            <td align="center">{{ $i++ }}</td>
                            <td>{{ $val->gudang->nama }}</td>
                            <td><?php $tanggal   = new DateTime($val->tanggal); ?>
                            {{ $tanggal->format('d')." ".$bulan[(int)$tanggal->format('m')]." ".$tanggal->format('Y') }}</td>
                            <td align="center">{{ $val->total_stok }}</td>
                            <td>{{ $val->name }}</td>
                            <td align="center">
                              <a class="btn btn-info flat" href="{{ url('/detail_stok_opname/'.$val->id) }}" data-toggle="tooltip" title="detail" style="font-size: 15pt"><i class="fa fa-info-circle"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script>
  $(function () {
    $("#mydatatables").DataTable();
  });

  function edit_button(kode,nama,alamat,kota,telp1,telp2,nama_sales,hp_sales,deskripsi,nomor_rekening,umur_hutang)
    {
        document.getElementById("edit_kode").value          = kode;
        document.getElementById("edit_nama").value          = nama;
        document.getElementById("edit_alamat").value        = alamat;
        document.getElementById("edit_kota").value          = kota;
        document.getElementById("edit_telp1").value         = telp1;
        document.getElementById("edit_telp2").value         = telp2;
        document.getElementById("edit_nama_sales").value    = nama_sales;
        document.getElementById("edit_hp_sales").value      = hp_sales;
        document.getElementById("edit_deskripsi").value     = deskripsi;
        document.getElementById("edit_nomor_rekening").value    = nomor_rekening;
        document.getElementById("edit_umur_hutang").value       = umur_hutang;
    }
</script>