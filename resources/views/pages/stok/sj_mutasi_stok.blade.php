@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <a href="{{ url('/purchase_order') }}" class="pull-right" style="padding:0px">
                    <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                </a>
                <a href="{{ url('/sj_mutasi_stok_print/'.$mutasi_stok_header->id) }}" target="_blank" class="btn bg-orange flat"><i class="fa fa-print"></i> Cetak</a>
                <a href="{{ url('/sj_mutasi_stok_pdf/'.$mutasi_stok_header->id) }}" target="_blank" class="btn btn-info flat" style="margin-right: 5px;"><i class="fa fa-download"></i> Unduh PDF</a>
            </div>
            <div class="box-body">
                <div class="area">
                    <section id="header">
                        <div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="title">{{ $judul }}</div>
                                    <div class="company">
                                        <h4>Gudang Asal</h4>
                                        {{ $mutasi_stok_header->gudang_asal->nama }} <br>
                                        {{ $mutasi_stok_header->gudang_asal->alamat }} <br>
                                        T. {{ $mutasi_stok_header->gudang_asal->telp }} <br>
                                        {{ $mutasi_stok_header->gudang_asal->supervisor }}
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <table class="detail" style="margin-top: 33px">
                                        <tr>
                                            <td class="title-detail">No. Surat Jalan Mutasi Stok</td>
                                            <td class="titik">&nbsp;:&nbsp;</td>
                                            <td class="data-detail">{{ $mutasi_stok_header->no_mutasi_stok }}</td>
                                        </tr>
                                        <tr>
                                            <td class="title-detail">Tanggal</td>
                                            <td class="titik">&nbsp;:&nbsp;</td>
                                            <td class="data-detail">
                                                <?php $tanggal = new DateTime($mutasi_stok_header->tanggal); 
                                                echo $tanggal->format('d/m/Y G:i:s');
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="title-detail">Gudang Tujuan</td>
                                            <td class="titik">&nbsp;:&nbsp;</td>
                                            <td class="data-detail user">{{ $mutasi_stok_header->gudang_asal->nama }}</td>
                                        </tr>
                                        <tr>
                                            <td class="title-detail" style="vertical-align: top;">Alamat </td>
                                            <td class="titik" style="vertical-align: top;">&nbsp;:&nbsp;</td>
                                            <td class="data-detail user">{{ $mutasi_stok_header->gudang_asal->alamat }}<br>
                                            T. {{ $mutasi_stok_header->gudang_asal->telp }} <br>
                                            {{ $mutasi_stok_header->gudang_asal->supervisor }}
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section id="table">
                        <div>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th width="5%">No.</th>
                                        <th>Nama Produk</th>
                                        <th>Jumlah</th>
                                        <th width="40%">Serial Number</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $jumlah_item=0; $total_akhir=0; ?>
                                    @foreach($mutasi_stok_detail as $key => $val)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>
                                            {{ $val->produk->nama }}
                                        </td>
                                        <td align="center">{{ $val->jumlah }}<?php $jumlah_item=$jumlah_item+$val->jumlah ?></td>
                                        <td>
                                            <?php
                                                foreach($produk_sn[$key] as $val2)
                                                {
                                                    echo "$val2->serial_number, ";
                                                }
                                            ?>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </section>
                    <section id="footer">
                        <div>
                            <div class="row">
                                <div class="col-md-4">
                                    
                                </div>
                                <div class="col-md-2">
                                    <div class="jml">
                                        <table class="detail">
                                            <tr>
                                                <td class="title-detail price" width="100px;">Jml Item</td>
                                                <td class="titik price" width="100px;">&nbsp;:&nbsp;</td>
                                                <td class="data-detail right price">{{ $jumlah_item }}</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div style="margin-top: 20px;">
                                        <div class="col-md-offset-6" style="text-align: center">
                                            <strong>Prepared By</strong>
                                        </div>
                                    </div>
                                    <div style="margin-top: 100px;">
                                        <div class="col-md-offset-6" style="text-align: center">
                                            <strong>(.........................)</strong>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div style="margin-top: 20px;">
                                        <div class="col-md-offset-6" style="text-align: center">
                                            <strong>Received By</strong>
                                        </div>
                                    </div>
                                    <div style="margin-top: 100px;">
                                        <div class="col-md-offset-6" style="text-align: center">
                                            <strong>(.........................)</strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <?php echo $print_time->format('d/m/Y G:i:s'); ?>
            </div>
        </div>
    </div>
</div>
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ URL::to('/css/invoice.css') }}">
@stop