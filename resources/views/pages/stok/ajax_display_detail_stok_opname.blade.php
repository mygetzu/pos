<hr>
<?php $jumlah_stok=0; ?>
<table class="table table-striped" id="ajax_produk_tabless">
    <thead>
        <tr>
            <th style="text-align:center">SERIAL NUMBER</th>
            <th style="text-align:center">STOK</th>
        </tr>
    </thead>
    <tbody>
        @foreach($temp_stok_opname as $val)
        <tr>
            <td style="padding-left:30px">{{ $val->serial_number }}</td>
            <td align="center">{{ $val->stok }}</td>
            <?php $jumlah_stok=(int)$jumlah_stok+(int)$val->stok; ?>
        </tr>
        @endforeach
    </tbody>
</table>
<input type="hidden" name="jumlah_stok_produk" id="jumlah_stok_produk" value="{{ $jumlah_stok }}">
<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
  $(function () {
    $("#ajax_produk_tabless").DataTable();
  });
</script>