<link rel="stylesheet" type="text/css" href="{{ URL::to('/css/nota.css') }}">
@include('includes.base_function')
<style type="text/css">
    [class*="col-"] {
    float: left;
}
</style>
<div class="area">
    <section id="header">
            <div class="col-6">
                <div class="title">{{ $judul }}</div>
                <div class="company">
                    <h4>Gudang Asal</h4>
                    {{ $mutasi_stok_header->gudang_asal->nama }} <br>
                    {{ $mutasi_stok_header->gudang_asal->alamat }} <br>
                    T. {{ $mutasi_stok_header->gudang_asal->telp }} <br>
                    {{ $mutasi_stok_header->gudang_asal->supervisor }}
                </div>
            </div>
            <div class="col-6">
                <table class="detail" style="margin-top: 33px">
                    <tr>
                        <td class="title-detail">No. Surat Jalan Mutasi Stok</td>
                        <td class="titik">&nbsp;:&nbsp;</td>
                        <td class="data-detail">{{ $mutasi_stok_header->no_mutasi_stok }}</td>
                    </tr>
                    <tr>
                        <td class="title-detail">Tanggal</td>
                        <td class="titik">&nbsp;:&nbsp;</td>
                        <td class="data-detail">
                            <?php $tanggal = new DateTime($mutasi_stok_header->tanggal); 
                            echo $tanggal->format('d/m/Y G:i:s');
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="title-detail">Gudang Tujuan</td>
                        <td class="titik">&nbsp;:&nbsp;</td>
                        <td class="data-detail user">{{ $mutasi_stok_header->gudang_asal->nama }}</td>
                    </tr>
                    <tr>
                        <td class="title-detail" style="vertical-align: top;">Alamat </td>
                        <td class="titik" style="vertical-align: top;">&nbsp;:&nbsp;</td>
                        <td class="data-detail user">{{ $mutasi_stok_header->gudang_asal->alamat }}<br>
                        T. {{ $mutasi_stok_header->gudang_asal->telp }} <br>
                        {{ $mutasi_stok_header->gudang_asal->supervisor }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </section>
    <section id="table">
        <table class="table">
            <thead>
                <tr>
                    <th width="5%">No.</th>
                    <th>Nama Produk</th>
                    <th>Jumlah</th>
                    <th width="40%">Serial Number</th>
                </tr>
            </thead>
            <tbody>
                <?php $jumlah_item=0; $total_akhir=0; ?>
                @foreach($mutasi_stok_detail as $key => $val)
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td>
                        {{ $val->produk->nama }}
                    </td>
                    <td align="center">{{ $val->jumlah }}<?php $jumlah_item=$jumlah_item+$val->jumlah ?></td>
                    <td>
                        <?php
                            foreach($produk_sn[$key] as $val2)
                            {
                                echo "$val2->serial_number, ";
                            }
                        ?>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </section>
    <section id="footer">
        <div class="row">
            <div class="col-4">
                
            </div>
            <div class="col-2">
                <div class="jml">
                    <table>
                        <tr>
                            <td class="title-detail price" width="100px;">Jml Item</td>
                            <td class="titik price" width="100px;">&nbsp;:&nbsp;</td>
                            <td class="data-detail right price">{{ $jumlah_item }}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-3" align="right">
                <div style="margin-top: 20px;">
                    <div style="text-align: center">
                        <strong>Prepared By</strong>
                    </div>
                </div>
                <div style="margin-top: 100px;">
                    <div style="text-align: center">
                        <strong>(.........................)</strong>
                    </div>
                </div>
            </div>
            <div class="col-3" align="right">
                <div style="margin-top: 20px;">
                    <div style="text-align: center">
                        <strong>Received By</strong>
                    </div>
                </div>
                <div style="margin-top: 100px;">
                    <div style="text-align: center">
                        <strong>(.........................)</strong>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <br>
    <div class="col-12">
        <?php echo $print_time->format('d/m/Y G:i:s'); ?>
    </div>
</div>
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function(){
          window.onload = function() { window.print(); }
      });
</script>