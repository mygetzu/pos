@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <a href="{{ url('/stok_opname') }}" class="pull-right" style="padding:0px">
                    <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                </a>
            </div>
            <div class="box-body">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <div class="form-horizontal">
                    {{ csrf_field() }}
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Gudang</label>
                            <div class="col-md-9">
                                <select class="form-control" name="gudang" id="gudang">
                                    <option value="">-- Pilih Gudang --</option>
                                    @foreach($gudang as $val)
                                        <option value="{{ $val->id }}">{{ $val->nama }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-9 col-md-offset-3">
                                <button class="btn btn-primary flat" onclick="btn_lanjut()" id="btn_lanjut"><span class="fa fa-arrow-right"></span> Lanjut</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                            <label class="col-md-3 control-label">No Stok Opname</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" placeholder="Masukkan no invoice PO" name="no_stok_opname" id="no_stok_opname" value="{{ $no_stok_opname }}">
                                @if ($errors->has('no_stok_opname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('no_stok_opname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                            <label class="col-md-3 control-label">Tanggal</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <?php $date = new DateTime(); 
                                    $tanggal = $date->format('d').'-'.$date->format('m').'-'.$date->format('Y'); ?>
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="tanggal" data-date-format="dd-mm-yyyy" name="tanggal" value="{{ $tanggal }}" placeholder="Masukkan tanggal">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>         
            </div>
            <div class="overlay" id="loading" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<div class="row" id="display_stok_opname" style="display: none;">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-body">
                <div class="form-horizontal">
                    <div class="col-md-9">
                        <div class="form-group">
                            <label class="col-md-2 control-label">Total Stok</label>
                            <div class="col-md-2">
                                <input type="text" id="display_total_stok" class="form-control" disabled="true" value="0">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Produk</label>
                            <div class="col-md-8">
                                <input type="text" id="display_produk_nama" class="form-control" disabled="true">
                                <input type="hidden" name="produk_id" id="produk_id">
                                <input type="hidden" name="produk_nama" id="produk_nama">
                            </div>
                            <div class="col-md-2">
                                <a data-toggle="modal" data-target="#modal_pilih_produk" class="btn bg-maroon flat"><span class="fa fa-cube"></span> Pilih Produk</a>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Serial Number</label>
                            <div class="col-md-8">
                                <input type="text" name="serial_number" class="form-control" onkeyup="if(event.keyCode == 13) do_stok_opname()" placeholder="Masukkan serial number, tekan enter" id="serial_number">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <button type="button" class="btn btn-success flat pull-right" onclick="btn_selesai()"><span class="fa fa-check"></span> Selesai</button>
                    <br>
                    <hr>
                </div>
                <div class="col-md-12">
                    <table class="table table-striped table-bordered" id="tabel_stok_opname">
                        <thead>
                            <tr>
                                <th style="text-align:center">NAMA PRODUK</th>
                                <th style="text-align:center">SERIAL NUMBER</th>
                                <th style="text-align:center">STOK</th>
                                <th style="text-align:center"></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="overlay" id="loading2" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_pilih_produk" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content box">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Produk</h4>
            </div>
            <div class="modal-body">
                <div id="stok_opname_get_produk">
                </div>
            </div>
            <div class="overlay" id="loading3" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<div id="stok_opname_get_serial_number">
</div>

<form method="post" action="{{ url('/do_tambah_stok_opname') }}" id="form_do_tambah_stok_opname">
    {{ csrf_field() }}
    <input type="hidden" name="gudang" id="input_gudang">
    <input type="hidden" name="no_stok_opname" id="input_no_stok_opname">
    <input type="hidden" name="tanggal" id="input_tanggal">
    <input type="hidden" name="total_stok" id="total_stok" value="0">
    <input type="hidden" name="count_stok_opname" id="count_stok_opname" value="0">
    <div id="produk_serial_stok_opname"></div>
</form>


<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
  $(function () {
    $("#ajax_produk_tabless3").DataTable();

    $('#tanggal').datepicker({
      autoclose: true,
      todayHighlight: 1,
    });
  });

    function btn_lanjut()
    {
        var gudang     = document.getElementById('gudang').value;

        if(gudang == ""){
            alert('gudang wajib diisi');
        }
        else{
            document.getElementById('input_gudang').value      = gudang;
            document.getElementById('input_no_stok_opname').value   = document.getElementById('no_stok_opname').value;
            document.getElementById('input_tanggal').value          = document.getElementById('tanggal').value;

            document.getElementById('gudang').disabled          = 'true';
            document.getElementById('no_stok_opname').disabled  = 'true';
            document.getElementById('tanggal').disabled         = 'true';
            document.getElementById('btn_lanjut').disabled      = 'true';

            document.getElementById('display_stok_opname').style.display = 'block';

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            var url = "{{ url('/stok_opname_get_produk') }}";

            $.ajax({
                type: "POST",
                url: url,
                data: { gudang:gudang },
                success: function (data) {
                    $("#stok_opname_get_produk").html(data);
                },
                error: function (data) {
                    alert('ooo');
                }
            });   
        }
    }

    function btn_pilih_produk(produk_id, produk_nama)
    {
        document.getElementById('produk_id').value              = produk_id;
        document.getElementById('produk_nama').value            = produk_nama;
        document.getElementById('display_produk_nama').value    = produk_nama;

        var gudang = document.getElementById('input_gudang').value;

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })

        var url = "{{ url('/stok_opname_get_serial_number') }}";

        $.ajax({
            type: "POST",
            url: url,
            data: { gudang:gudang, produk_id:produk_id },
            success: function (data) {
                $("#stok_opname_get_serial_number").html(data);
            },
            error: function (data) {
                alert('ooo');
            }
        });   
    }

    function do_stok_opname()
    { 
        var produk_id           = document.getElementById('produk_id').value;
        var produk_nama         = document.getElementById('produk_nama').value;
        var serial_number       = document.getElementById('serial_number').value;
        
        if(produk_id == ""){
            alert('pilih produk terlebih dahulu');
            document.getElementById('serial_number').value = "";
            return;
        }
        //cek serial number terdaftar atau tidak
        var total_serial_number = document.getElementById('total_serial_number').value;
        total_serial_number = parseInt(total_serial_number);

        var cek_ada_serial_number = 0;
        for (var i = total_serial_number - 1; i >= 0; i--) {
            var list_serial_number = document.getElementById('list_serial_number'.concat(i)).value;
            if(list_serial_number == serial_number){
                cek_ada_serial_number = 1;
                break;
            }
        }
        
        if(cek_ada_serial_number == 0){
            alert('serial number tidak terdaftar');
            document.getElementById('serial_number').value = "";
            return;
        }


        var total_stok = document.getElementById("total_stok").value;
        total_stok = parseInt(total_stok);
        total_stok++;
        document.getElementById("total_stok").value = total_stok;
        document.getElementById("display_total_stok").value = total_stok;

        var count_stok_opname = document.getElementById("count_stok_opname").value;
        count_stok_opname = parseInt(count_stok_opname);

        var sudah_ada   = 0;

        //cek produk_id dan serial_number sudah ada atau belum, jika sudah ada maka cukup tambah jumlah, jika belum buat baru
        for (var i = 0; i < count_stok_opname; i++) {
            var cek_produk_id       = document.getElementById('stok_opname_produk_id'.concat(i)).value;
            var cek_serial_number   = document.getElementById('stok_opname_serial_number'.concat(i)).value;

            if(cek_produk_id == produk_id && cek_serial_number == serial_number){ 
                var cek_jumlah = document.getElementById('stok_opname_jumlah'.concat(i)).value;
                cek_jumlah = parseInt(cek_jumlah);
                cek_jumlah++;
                document.getElementById('stok_opname_jumlah'.concat(i)).value = cek_jumlah;
                sudah_ada = 1;
                //update jumlah
                document.getElementById('display_stok_opname_jumlah'.concat(i)).value = cek_jumlah;
                break;
            }
        }

        if(sudah_ada == 0){
            //tambah input di form yang isinya produk, serial number, jumlah stok opname
            $("#produk_serial_stok_opname").append("<input type='hidden' name='stok_opname_produk_id"+count_stok_opname+"' id='stok_opname_produk_id"+count_stok_opname+"' value='"+produk_id+"'>\
                <input type='hidden' name='stok_opname_serial_number"+count_stok_opname+"' id='stok_opname_serial_number"+count_stok_opname+"' value='"+serial_number+"'>\
                <input type='hidden' name='stok_opname_jumlah"+count_stok_opname+"' id='stok_opname_jumlah"+count_stok_opname+"' value='"+1+"'>");

            //tampilkan di tabel
            var table   = document.getElementById("tabel_stok_opname");
            var row     = table.insertRow(1);

            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            var cell3 = row.insertCell(2);
            var cell4 = row.insertCell(3);

            cell1.innerHTML = produk_nama;
            cell2.innerHTML = serial_number;
            cell3.innerHTML = "<input type='text' class='form-control' name='display_stok_opname_jumlah"+count_stok_opname+"' id='display_stok_opname_jumlah"+count_stok_opname+"' value='"+1+"' disabled='true'>";
            cell4.innerHTML = '<button class="btn btn-danger flat" onclick="hapus_stok_opname_list(\''+count_stok_opname+'\',this)"><span class="fa fa-trash-o"><span> Hapus</button>';
            
            cell3.setAttribute("align", "center");
            cell4.setAttribute("align", "center");

            count_stok_opname++;
            document.getElementById("count_stok_opname").value = count_stok_opname;
        }

        document.getElementById('serial_number').value = "";
    }

    function hapus_stok_opname_list(id, obj)
    {
        var cek = confirm('apakah anda yakin?');

        if(cek){
            //hapus row table
            var row = obj.closest("tr");
            document.getElementById("tabel_stok_opname").deleteRow(row.rowIndex);

            //kosongkan value input form
            document.getElementById('stok_opname_produk_id'.concat(id)).value       = "";
            document.getElementById('stok_opname_serial_number'.concat(id)).value   = "";
            document.getElementById('stok_opname_jumlah'.concat(id)).value          = "";
        }
    }

    function btn_selesai()
    {
        var cek = confirm('apakah stok opname sudah sesuai?');

        if(cek){
            document.getElementById('loading').style.display = 'block';
            document.getElementById('loading2').style.display = 'block';

            document.getElementById('form_do_tambah_stok_opname').submit();
        }
    }
</script>
@stop