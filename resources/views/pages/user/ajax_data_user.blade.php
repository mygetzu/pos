<table class="table table-striped table-bordered" id="ajax_produk_tabless">
    <thead>
        <tr>
            <th style="text-align:center">NO</th>
            <th style="text-align:center">NAMA</th>
            <th style="text-align:center">EMAIL</th>
            <th style="text-align:center">HAK AKSES</th>
            <th style="text-align:center">KATEGORI</th>
            <th style="text-align:center"></th>
            <th style="text-align:center"></th>
            <th style="text-align:center"></th>
        </tr>
    </thead>
    <tbody>
        <?php $i=1; ?>
        @foreach($data_user as $val)
        @if($val->id === Auth::user()->id)
        @continue
        @endif
        <tr>
            <form class="form-horizontal" method="POST" action="{{ url('/konfirmasi_user') }}">
            <input type="hidden" name="id" value="{{ $val->id }}">
            <input type="hidden" name="nama{{ $val->id }}" id="nama{{ $val->id }}" value="{{ $val->name }}">
            {{ csrf_field() }}
            <td align="center">{{ $i++ }}</td>
            <td>{{ $val->name }}</td>
            <td>{{ $val->email }}</td>
            <td>
                @if($val->hak_akses === 1)
                <p>administrator</p>
                @elseif($val->hak_akses === 2)
                <p>pelanggan</p>
                @elseif($val->hak_akses === 0 || $val->hak_akses === '0')
                <select class="form-control" name="hak_akses{{ $val->id }}" id="hak_akses{{ $val->id }}" onchange="change_hak_akses('{{ $val->id }}')">
                    <option value="">--Pilih Hak Akses--</option>
                    @foreach($hak_akses as $akses)
                    <option value="{{ $akses->id }}">{{ $akses->nama }}</option>
                    @endforeach
                </select>
                <select class="form-control" name="pil_kategori{{ $val->id }}" id="pil_kategori{{ $val->id }}" style="display:none;margin-top:10px">
                    <option value="">-- Pilih Kategori Pelanggan --</option>
                    @foreach($kategori_pelanggan as $kat)
                    <option value="{{ $kat->id }}">{{ $kat->nama }}</option>
                    @endforeach
                </select>
                @else
                    {{ $val->hak_akses }}
                @endif
            </td>
            <td>
                @if(!empty($val->kategori))
                  @if($val->kategori != '-'){{ $val->kategori }}@endif
                @endif
            </td>
            <td align="center">
                <label class="switch">
                    <input type="checkbox" onchange='window.location.href="{{ url('/ubah_status_user/'.$val->id) }}"' <?php if($val->is_aktif == 1) echo "checked"; else echo ""; ?>>
                    <div class="slider" data-toggle="tooltip" title="status aktif"></div>
                </label>
            </td>
            <td align="center">
                @if($val->hak_akses != '0')
                <a href="{{ url('/detail_user/'.$val->id) }}">
                  <button class='btn btn-info flat' type="button" data-toggle="tooltip" title="detail" style="font-size: 15pt;"><span class='fa fa-info-circle'></span></button>
                </a>
                @else
                <button class="btn btn-success flat" type="submit" onclick="klik_btn('{{ $val->id }}')" data-toggle="tooltip" title="konfirmasi user" style="font-size: 15pt;"><span class="fa fa-check"></span></button>
                @endif
            </td>
            </form>
            
            <td align="center">
                <button class="btn btn-danger flat" type="button" onclick="confirm('Apakah Anda Yakin?');alert('Password Telah Direset, Data Dikirim ke Email User')" data-toggle="tooltip" title="reset password" style="font-size: 15pt;"><span class="fa fa-refresh"></span></button>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
  $(function () {
    $("#ajax_produk_tabless").DataTable();
  });
</script>