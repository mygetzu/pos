<link href="{{ URL::to('/switch/dist/css/bootstrap3/bootstrap-switch.css') }}" rel="stylesheet">
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<form id="hidden_form" method="POST" action="{{ url('/konfirmasi_user2') }}">
{{ csrf_field() }}
<input type="hidden" name="hidden_id" id="hidden_id" value="">
<input type="hidden" name="hidden_nama" id="hidden_nama" value="">
<input type="hidden" name="hidden_hak_akses" id="hidden_hak_akses" value="">
<input type="hidden" name="hidden_kategori_pelanggan" id="hidden_kategori_pelanggan" value="">
</form>
<input type="hidden" name="base_path" id="base_path" value="{{ asset('/') }}">
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
            <a data-toggle="modal" data-target="#tambah_akun">
                <button class="btn bg-purple flat" style="margin-right:10px"><span class="fa fa-plus-circle"></span> Tambah Akun</button>
            </a>
            <div class="pull-right">
                <select class="form-control" name="filter_user" id="filter_user">
                    <option value="0">Semua User</option>
                    <option value="-1">User Belum Dikonfirmasi</option>
                    <option value="1">Administrator</option>
                    <option value="2">Pelanggan</option>
                    <option value="3">Teknisi</option>
                </select>
            </div>
            </div>
            <div class="box-body">
                @if ($errors->has('name'))
                <div class="alert alert-danger flat">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    {{ $errors->first('name') }}
                </div>
                @endif
                @if ($errors->has('email'))
                <div class="alert alert-danger flat">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    {{ $errors->first('email') }}
                </div>
                @endif
                @if ($errors->has('alamat'))
                <div class="alert alert-danger flat">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    {{ $errors->first('alamat') }}
                </div>
                @endif
                @if ($errors->has('provinsi'))
                <div class="alert alert-danger flat">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    {{ $errors->first('provinsi') }}
                </div>
                @endif
                @if ($errors->has('kota_id'))
                <div class="alert alert-danger flat">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    {{ $errors->first('kota_id') }}
                </div>
                @endif
                @if ($errors->has('kode_pos'))
                <div class="alert alert-danger flat">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    {{ $errors->first('kode_pos') }}
                </div>
                @endif
                @if ($errors->has('hak_akses'))
                <div class="alert alert-danger flat">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    Pilihan Hak Akses Wajib Diisi
                </div>
                @endif
                @if ($errors->has('hidden_hak_akses'))
                <div class="alert alert-danger flat">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    Pilihan Hak Akses Wajib Diisi
                </div>
                @endif
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                @if ($errors->has('kategori_id'))
                <div class="alert alert-danger flat">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    Pilihan Kategori Pelanggan Wajib Diisi
                </div>
                @endif
                <div id="content">
                <table class="table table-striped table-bordered" id="mydatatables2">
                    <thead>
                        <tr>
                            <th style="text-align:center">NO</th>
                            <th style="text-align:center">NAMA</th>
                            <th style="text-align:center">EMAIL</th>
                            <th style="text-align:center">HAK AKSES</th>
                            <th style="text-align:center">KATEGORI</th>
                            <th style="text-align:center"></th>
                            <th style="text-align:center"></th>
                            <th style="text-align:center"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i=1; ?>
                        @foreach($data_user as $val)
                        @if($val->id === Auth::user()->id)
                        @continue
                        @endif
                        <tr>
                            <form class="form-horizontal" method="POST" action="{{ url('/konfirmasi_user') }}">
                            <input type="hidden" name="id" value="{{ $val->id }}">
                            <input type="hidden" name="nama" value="{{ $val->name }}">
                            {{ csrf_field() }}
                            <td align="center">{{ $i++ }}</td>
                            <td>{{ $val->name }}</td>
                            <td>{{ $val->email }}</td>
                            <td>
                                @if($val->hak_akses != '0')
                                {{ $val->hak_akses }}
                                @else
                                <select class="form-control" name="hak_akses" id="hak_akses">
                                    <option value="">--Pilih Hak Akses--</option>
                                    @foreach($hak_akses as $akses)
                                    <option value="{{ $akses->id }}">{{ $akses->nama }}</option>
                                    @endforeach
                                </select>
                                <select class="form-control" name="pil_kategori" id="pil_kategori" style="display:none;margin-top:10px">
                                    <option value="">-- Pilih Kategori Pelanggan --</option>
                                    @foreach($kategori_pelanggan as $kat)
                                    <option value="{{ $kat->id }}">{{ $kat->nama }}</option>
                                    @endforeach
                                </select>
                                @endif
                            </td>
                            <td>
                                @if(!empty($val->kategori))
                                  @if($val->kategori != '-'){{ $val->kategori }}@endif
                                @endif
                            </td>
                            <td align="center">
                                <label class="switch">
                                    <input type="checkbox" onchange='window.location.href="{{ url('/ubah_status_user/'.$val->id) }}"' <?php if($val->is_aktif == 1) echo "checked"; else echo ""; ?>>
                                    <div class="slider" data-toggle="tooltip" title="status aktif"></div>
                                </label>
                            </td>
                            <td align="center">
                                @if($val->hak_akses != '0')
                                <a href="{{ url('/detail_user/'.$val->id) }}">
                                  <button class='btn btn-info flat' type="button" data-toggle="tooltip" title="detail"><span class='fa fa-info-circle' style="font-size: 15pt"></span></button>
                                </a>
                                @else
                                <button class="btn btn-success flat" type="submit" onclick="document.getElementById('loading').style.display='block'" data-toggle="tooltip" title="konfirmasi user"><span class="fa fa-check" style="font-size: 15pt"></span></button>
                                @endif
                            </td>
                            </form>
                            <td align="center">
                                <button class="btn btn-danger flat" type="button" onclick="reset('{{ $val->id }}')" data-toggle="tooltip" title="reset password"><span class="fa fa-refresh" style="font-size: 15pt"></span></button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>
            <div class="overlay" id="loading" style="display:none">
              <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="tambah_akun" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content box">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="riwayatheader">Tambah Akun</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="post" action="{{ url('/tambah_akun') }}" autocomplete="off">
        {{ csrf_field() }}
            <div class="form-group">
                <label class="col-sm-2 control-label">Nama</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Masukkan nama" style="text-transform: capitalize;">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Email</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="email" name="email" placeholder="Masukkan email">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Alamat</label>
                <div class="col-sm-10">
                <textarea type="text" class="form-control" id="alamat" name="alamat" placeholder="Masukkan alamat (min 4 karakter)" style="text-transform: capitalize;"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Kabupaten/Kota</label>
                <div class="col-md-5">
                    <select class="form-control" id="provinsi" name="provinsi">
                        <option value="">-- Pilih Provinsi --</option>
                        @foreach($provinsi as $val)
                        <option value="{{ $val->kode }}">{{ $val->nama }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-5">
                    <select class="form-control" id="kota_id" name="kota_id">
                        <option value="">-- Pilih Kabupaten/Kota --</option>
                    </select>
                </div>
            </div><!-- /.box-body -->
            <div class="form-group">
                <label class="col-sm-2 control-label">Kode Pos</label>
                <div class="col-sm-4">
                    <input type="text" name="kode_pos" class="form-control" placeholder="Masukkan kode pos">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">No Telepon</label>
                <div class="col-sm-4">
                    <div class="input-group" style="margin-bottom:0px">
                        <div class="input-group-addon">
                            <i class="fa fa-phone"></i>
                        </div>
                        <input type="text" class="form-control" data-mask placeholder="contoh: (031)-9999999" name="telp" value="{{ old('telp') }}">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">No Handphone</label>
                <div class="col-sm-4">
                   <div class="input-group" style="margin-bottom:0px">
                        <div class="input-group-addon">
                            <i class="fa fa-mobile" style="width:12px"></i>
                        </div>
                        <input type="text" class="form-control" data-inputmask='"mask": "(+62)999-9999-9999"' data-mask2 placeholder="contoh: (+62)857-9999-9999" name="hp" value="{{ old('hp') }}">
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button data-dismiss="modal" class="btn btn-default flat"><span class="glyphicon glyphicon-ban-circle"></span> Batal</button>
                <button type="submit" class="btn btn-primary pull-right flat" onclick="document.getElementById('loading2').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
          </div><!-- /.box-footer -->
        </form>
      </div>
      <div class="overlay" id="loading2" style="display:none">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
    </div>
  </div>
</div>

@endsection
<style type="text/css">
    .tooltip-inner {
        white-space:nowrap;
        max-width:none;
    }
</style>
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<!-- InputMask -->
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
<script>
  $(function () {
    $("#mydatatables").DataTable();
    $("#mydatatables2").DataTable();
    $('[data-toggle="tooltip"]').tooltip(); 

    $("[data-mask]").inputmask({ mask: "(999[9])-9999999", greedy: false ,
        definitions: {
                '*': {
                validator: "[0-9-]",
                cardinality: 1,
                casing: "lower"
            }
        }
    });

    $("[data-mask2]").inputmask();
  });
</script>
<script type="text/javascript">
    function klik_btn(id)
    { document.getElementById('loading').style.display = "block";
        document.getElementById("hidden_id").value = id;
        document.getElementById("hidden_nama").value        = document.getElementById('nama'.concat(id)).value;
        document.getElementById("hidden_hak_akses").value   = document.getElementById('hak_akses'.concat(id)).value;
        hak_akses = document.getElementById('hak_akses'.concat(id)).value;
        if(hak_akses == '2')
        {
            document.getElementById("hidden_kategori_pelanggan").value = document.getElementById('pil_kategori'.concat(id)).value;
        }

        document.getElementById("hidden_form").submit();
    }

    function change_hak_akses(id)
    {
        hak_akses   = document.getElementById('hak_akses'.concat(id)).value;
        if(hak_akses == '2')
        {
            document.getElementById('pil_kategori'.concat(id)).style.display = 'block';
        }
        else
        {
            document.getElementById('pil_kategori'.concat(id)).style.display = 'none';
        }
        
    }

    function reset(id)
    {
        var cek = confirm("Apakah Anda Yakin untuk mereset password?");

        if(cek)
        {   
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            var path    = document.getElementById('base_path').value;
            var url     = path+"reset_password/"+id;
            
            $.ajax({
                type: "POST",
                url: url,
                beforeSend: function(){
                        document.getElementById('loading').style.display = "block";
                      },
                success: function (data) {
                   alert("Password Telah Direset, Data Dikirim ke Email User"+data);
                   document.getElementById('loading').style.display = "none";
                },
                error: function (data) {
                    alert('gagal reset password');
                    document.getElementById('loading').style.display = "none";
                }
            });
        }
    }

    $(document).ready(function(){
          $('#filter_user').change(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            var filter = document.getElementById("filter_user").value;
            var path =  document.getElementById('base_path').value;
            var url = path+"get_user_data/"+filter;
            
             $.ajax({
                type: "POST",
                url: url,
                beforeSend: function(){
                        document.getElementById('loading').style.display = "block";
                      },
                success: function (data) {
                   $("#content").html(data);
                   document.getElementById('loading').style.display = "none";
                },
                error: function (data) {
                    // alert('ooo');
                }
            });
              
          });

          $('#hak_akses').change(function(){
            var val = document.getElementById('hak_akses').value;

            if(val == '2')
            {
                document.getElementById('pil_kategori').style.display = 'block';
            }
            else{
                document.getElementById('pil_kategori').style.display = 'none';
            }
          });

          $('#provinsi').change(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            var kode_provinsi = document.getElementById('provinsi').value;
            if(kode_provinsi == "")
            {
                kode_provinsi = "ALL";
            }
            var path =  document.getElementById('base_path').value;
            var url = path+"get_kota/"+kode_provinsi;

            $.ajax({
                type: "POST",
                url: url,
                success: function (data) {
                    $("#kota_id").html(data);
                },
                error: function (data) {
                    // alert('error 3');
                }
            });
          });
  //         $('input[name="my-checkbox"]').on('switchChange.bootstrapSwitch', function(event, state) {
  //             console.log(this); // DOM element
  // console.log(event); // jQuery event
  // console.log(state); // true | false
  //         });
      });
</script>