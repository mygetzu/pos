<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <a href="{{ url('/tambah_hak_akses') }}">
                  <button class='btn bg-purple flat'><span class='fa fa-plus'></span> Tambah Hak Akses</button>
                </a>
            </div>
            <div class="box-body">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                @if($errors->has('nama'))
                <div class="alert alert-danger alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-ban"></i>
                  {{ $errors->first('nama') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <table id="mydatatables" class="table table-striped table-bordered">
                    <thead>
                        <th style="text-align:center">NO</th>
                        <th style="text-align:center">NAMA</th>
                        <th style="text-align:center">DESKRIPSI</th>
                        <th style="text-align:center">MENU AKSES</th>
                        <th style="text-align:center"></th>
                        <th></th>
                    </thead>
                    <tbody>
                        <?php $i=1; ?>
                        @foreach($hak_akses as $val)
                        <tr>
                            <td align="center">{{ $i++ }}</td>
                            <td>{{ $val['nama'] }}</td>
                            <td>{{ $val['deskripsi'] }}</td>
                            <td>{{ $val['menu_akses_nama'] }}</td>
                            <td align="center">
                                <?php $id = $val['id']; $nama=$val['nama']; $deskripsi=$val['deskripsi']; $menu_akses_id = $val['menu_akses_id']?>
                                <a data-toggle="modal" data-target="#ubah_hak_akses">
                                    <button class="btn btn-warning flat" onclick="ubah_hak_akses('{{ $id }}', '{{ $nama }}', '{{ $deskripsi }}', '{{ $menu_akses_id }}')" data-toggle="tooltip" title="ubah" style="font-size: 15pt"><i class="fa fa-edit"></i>
                                    </button>
                                </a>
                            </td>
                            <td align="center">
                                @if($val['id'] != 1 AND $val['id'] != 2 AND $val['id'] != Auth::user()->hak_akses_id)
                                <button class="btn btn-danger flat" onclick="hapus_hak_akses('{{ $id }}')" data-toggle="tooltip" title="hapus" style="font-size: 15pt"><span class="fa fa-trash-o"></span></button>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="overlay" id="loading" style="display:none">
              <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ubah_hak_akses" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content box">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ubah Hak Akses</h4>
            </div>
            <form class="form-horizontal" method="post" action="{{ url('/ubah_hak_akses') }}" autocomplete="off">
            <div class="modal-body">
                <input type="hidden" name="id" id="ubah_hak_akses_id">
                {{ csrf_field() }}
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama Hak Akses*</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="nama" id="ubah_hak_akses_nama" placeholder="Masukkan nama hak akses">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Deskripsi</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" id="ubah_hak_akses_deskripsi" name="deskripsi" placeholder="Masukkan deskripsi"></textarea>
                    </div>
                </div>
                <div class="form-group" id="display_menu_akses">
                    <label class="col-sm-2 control-label">Menu Akses</label>
                    <div class="col-sm-8">
                        @foreach($menu as $key => $value)
                        <div class="checkbox col-sm-4">
                            <label><input type="checkbox" name="hak_akses_menu{{ $key }}" id="hak_akses_menu{{ $key+1 }}" value="{{ $value->id }}">{{ $value->nama }}</label>
                        </div>
                        @endforeach
                    </div>
                </div>
                <p>Keterangan (*) : Wajib Diisi</p>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default flat pull-left"><span class="glyphicon glyphicon-ban-circle"></span> Batal</button>
                <button type="submit" class="btn btn-primary flat" onclick="document.getElementById('loading2').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
            </div>
            </form>
            <div class="overlay" id="loading2" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>
<form action="{{ url('/hapus_hak_akses') }}" method="post" id="form_hapus_hak_akses">
    {{ csrf_field() }}
    <input type="hidden" name="id" id="hapus_hak_akses_id">
</form>

<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script>
  $(function () {
    $("#mydatatables").DataTable();
  });

    function ubah_hak_akses(id, nama, deskripsi, menu_akses_id)
    {
        document.getElementById('ubah_hak_akses_id').value          = id;
        document.getElementById('ubah_hak_akses_nama').value        = nama;
        document.getElementById('ubah_hak_akses_deskripsi').value   = deskripsi;

        for (var i = 36; i > 0; i--) {
            document.getElementById("hak_akses_menu"+i).checked = false;
        }

        if(id != '1' && id != '2'){
            document.getElementById('display_menu_akses').style.display = 'block';
            var menu_akses_id = menu_akses_id.split("-");
            $.each( menu_akses_id, function( key, value ) {
                if(document.getElementById("hak_akses_menu"+value) != null){

                    document.getElementById("hak_akses_menu"+value).checked = true;
                }
            });
        }
        else{
            document.getElementById('display_menu_akses').style.display = 'none';
        }
    }

    function hapus_hak_akses(id)
    {
        var cek = confirm('Apakah anda yakin menghapus hak akses?');

        if(cek)
        {
            document.getElementById('hapus_hak_akses_id').value = id;
            document.getElementById('loading').style.display    = 'block';
            document.getElementById('form_hapus_hak_akses').submit();
        }
    }
</script>
@stop