@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<input type="hidden" name="base_path" id="base_path" value="{{ asset('/') }}">
<!-- DATA ASLI -->
<input type="hidden" name="ori_name" id="ori_name" value="{{ $user->name }}">
<input type="hidden" name="ori_email" id="ori_email" value="{{ $user->email }}">
<input type="hidden" name="ori_alamat" id="ori_alamat" value="{{ $user->alamat }}">
<input type="hidden" name="ori_kota_id" id="ori_kota_id" value="{{ $user->kota_id }}">
<input type="hidden" name="ori_kode_pos" id="ori_kode_pos" value="{{ $user->kode_pos }}">
<input type="hidden" name="ori_telp" id="ori_telp" value="{{ $user->telp }}">
<input type="hidden" name="ori_hp" id="ori_hp" value="{{ $user->hp }}">
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <div class="pull-right">
                    <a href="{{ url('/data_user') }}">
                        <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                    </a>
                </div>
                <div id="ubah_akun" style="display:block">
                    <div class="">
                      <button class="btn btn-warning flat" onclick="ubah_akun('{{ $my_kota->provinsi_id }}','{{ $my_kota->kota_id }}')"><span class='fa fa-edit'></span> Ubah</button>
                    </div>
                </div>
                <div id="ubah_batal" style="display:none">
                    <div class="">
                      <button class="btn btn-default flat" onclick="ubah_batal()"><span class='fa fa-ban'></span> Batal</button>
                    </div>
                </div>
            </div>
            <div class="box-body">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                    <i class="fa fa-check"></i>
                    {{ Session::get('message') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                <form class="form-horizontal" method="POST" action="{{ url('/ubah_user') }}" autocomplete="off">
                    <input type="hidden" name="id" value="{{ $user->id }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="control-label col-md-2">Tanggal Registrasi</label>
                        <div class="col-md-4">
                            <?php $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'); $date1 = new DateTime($user->tanggal_register); ?>
                            <input type="text" class="form-control" value="<?php echo $date1->format('d').' '.$bulan[(int)$date1->format('m')].' '.$date1->format('Y'); ?>" disabled="true">
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label class="control-label col-md-2">Nama <span id="ast1" class="control-label"></span></label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="name" id="name" value="{{ $user->name }}" disabled="true" placeholder="Masukkan Nama" style="text-transform:capitalize">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>Kolom Nama Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="control-label col-md-2">E-Mail <span id="ast2" class="control-label"></span></label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="email" id="email" value="{{ $user->email }}" disabled="true" placeholder="Masukkan Alamat E-Mail">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>Kolom E-Mail Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('alamat') ? ' has-error' : '' }}">
                        <label class="control-label col-md-2">Alamat <span id="ast3" class="control-label"></span></label>
                        <div class="col-md-8">
                            <textarea type="text" class="form-control" name="alamat" id="alamat" disabled="true" placeholder="Masukkan Alamat" style="text-transform:capitalize">{{ $user->alamat }}</textarea>
                            @if ($errors->has('alamat'))
                                <span class="help-block">
                                    <strong>Kolom Alamat Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('kota') ? ' has-error' : '' }}">
                        <label class="control-label col-md-2">Kabupaten/Kota <span id="ast4" class="control-label"></span></label>
                        <div class="col-md-8" id="kota_saya">
                            @if(!empty($my_kota))
                            <input type="text" class="form-control" value="{{ ucwords(strtolower($my_kota->nama)) }}" disabled="true" placeholder="Masukkan Kota">
                            @if ($errors->has('provinsi') AND $errors->has('kota'))
                                    <span class="help-block">
                                        <strong>Pilihan Provinsi Serta Kabupaten/Kota Wajib Diisi</strong>
                                    </span>
                            @elseif ($errors->has('provinsi'))
                                <span class="help-block">
                                    <strong>Pilihan Provinsi Wajib Diisi</strong>
                                </span>
                            @elseif ($errors->has('kota'))
                                <span class="help-block">
                                    <strong>Pilihan Kabupaten/Kota Wajib Diisi</strong>
                                </span>
                            @endif
                            @endif
                        </div>
                        <div class="col-md-4" id="edit_kota_saya" style="display:none">
                            <select class="form-control" id="provinsi" name="provinsi">
                                <option value="">-- Pilih Provinsi --</option>
                                @foreach($provinsi as $val)
                                <option value="{{ $val->kode }}">{{ $val->nama }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('provinsi'))
                                <span class="help-block">
                                    <strong>Pilihan Provinsi Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                        <div class="col-md-4" id="edit_kota_saya2" style="display:none">
                            <select class="form-control" id="kota" name="kota">
                                
                            </select>
                            @if ($errors->has('kota'))
                                <span class="help-block">
                                    <strong>Pilihan Kabupaten/Kota Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('kode_pos') ? ' has-error' : '' }}">
                        <label class="control-label col-md-2">Kode Pos <span id="ast5" class="control-label"></span></label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="kode_pos" id="kode_pos" value="{{ $user->kode_pos }}" disabled="true" placeholder="Masukkan Kode Pos">
                            @if ($errors->has('kode_pos'))
                                <span class="help-block">
                                    <strong>Kolom Kode Pos Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2">Telepon</label>
                        <div class="col-md-4">
                            <div class="input-group" style="margin-bottom:0px">
                                <div class="input-group-addon">
                                    <i class="fa fa-phone"></i>
                                </div>
                                <input style="display:none" type="text" class="form-control" data-mask placeholder="contoh: (031)-9999999" name="telp" id="telp" value="{{ $user->telp }}">

                                <input style="display:block" type="text" class="form-control" placeholder="contoh: (031)-9999999" id="display_telp" disabled="true" value="<?php echo str_replace('_', '', $user->telp); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2">HP</label>
                        <div class="col-md-4">
                            <div class="input-group" style="margin-bottom:0px">
                                <div class="input-group-addon">
                                    <i class="fa fa-mobile" style="padding:2px"></i>
                                </div>
                                <input style="display:none" type="text" class="form-control" data-inputmask='"mask": "(+62)999-9999-9999"' data-mask2 placeholder="contoh: (+62)857-9999-9999" name="hp" id="hp" value="{{ $user->hp }}">
                                <input style="display:block" type="text" class="form-control" id="display_hp" placeholder="contoh: (+62)857-9999-9999" disabled="true" value="<?php echo str_replace('_', '', $user->hp); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-2" style="display:none" id="button_submit">
                            <button type="submit" class="btn btn-primary flat" onclick="document.getElementById('loading').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
                        </div>
                    </div>
                </form>
                <label id="astket"></label>
            </div>
            <div class="overlay" id="loading" style="display:none">
              <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>
@stop
<!-- jQuery 2.2.0 -->
<script src="{{ asset('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('/adminlte/plugins/select2/select2.full.min.js') }}"></script>
<!-- InputMask -->
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();
    
    $("[data-mask]").inputmask({ mask: "(999[9])-9999999", greedy: false ,
        definitions: {
                '*': {
                validator: "[0-9-]",
                cardinality: 1,
                casing: "lower"
            }
        }
    });
    
    $("[data-mask2]").inputmask();
  });
</script>
<script type="text/javascript">
    function ubah_akun(provinsi_id,kota_id)
    {
        document.getElementById("name").disabled        = false;
        document.getElementById("email").disabled       = false;
        document.getElementById("alamat").disabled      = false;
        document.getElementById("kode_pos").disabled    = false;
        document.getElementById("telp").style.display           = 'block';
        document.getElementById("display_telp").style.display   = 'none';
        document.getElementById("hp").style.display             = 'block';
        document.getElementById("display_hp").style.display     = 'none';
        
        document.getElementById("ubah_akun").style.display      = 'none';
        document.getElementById("ubah_batal").style.display     = 'block';
        document.getElementById("button_submit").style.display  = 'block';

        document.getElementById("astket").innerHTML = "Keterangan (*) : Wajib Diisi";
        document.getElementById("ast1").innerHTML = "*";
        document.getElementById("ast2").innerHTML = "*";
        document.getElementById("ast3").innerHTML = "*";
        document.getElementById("ast4").innerHTML = "*";
        document.getElementById("ast5").innerHTML = "*";

        document.getElementById("kota_saya").style.display = 'none';
        document.getElementById("edit_kota_saya").style.display = 'block';
        document.getElementById("edit_kota_saya2").style.display = 'block';

        document.getElementById("provinsi").selectedIndex = provinsi_id;
        $("#kota").html("");

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })

        var kode_provinsi = document.getElementById('provinsi').value;
        var path =  document.getElementById('base_path').value;
        var url = path+"get_kota2/"+kode_provinsi;

        $.ajax({
            type: "POST",
            url: url,
            dataType: 'json',
            success: function (data) {
                var my_obj = Object.keys(data).length;
                var my_index = 0;

                for (var i = 0; i < my_obj; i++) {
                    var z = document.createElement("option");
                    $.each(data[i], function(key,value){
                        if(key=='id'){
                         z.setAttribute("value", value);
                        }
                        else if(key == 'nama')
                        {
                            var t = document.createTextNode(value);
                            z.appendChild(t);
                        }
                        if(key == 'id' && value == kota_id)
                        { 
                            my_index = i;
                        }
                    });
                    document.getElementById("kota").appendChild(z);
                }
                document.getElementById("kota").selectedIndex = my_index;
            },
            error: function (data) {
                alert('ooo');
            }
        });

    }
    function ubah_batal()
    {
        document.getElementById("name").value       = document.getElementById("ori_name").value;
        document.getElementById("email").value      = document.getElementById("ori_email").value;
        document.getElementById("alamat").value     = document.getElementById("ori_alamat").value;
        document.getElementById("kode_pos").value   = document.getElementById("ori_kode_pos").value;
        document.getElementById("telp").value       = document.getElementById("ori_telp").value;
        document.getElementById("hp").value         = document.getElementById("ori_hp").value;

        document.getElementById("name").disabled        = true;
        document.getElementById("email").disabled       = true;
        document.getElementById("alamat").disabled      = true;
        document.getElementById("kode_pos").disabled    = true;
        document.getElementById("telp").style.display           = 'none';
        document.getElementById("display_telp").style.display   = 'block';
        document.getElementById("hp").style.display             = 'none';
        document.getElementById("display_hp").style.display     = 'block';

        document.getElementById("ubah_akun").style.display = 'block';
        document.getElementById("ubah_batal").style.display = 'none';
        document.getElementById("button_submit").style.display = 'none';

        document.getElementById("kota_saya").style.display = 'block';
        document.getElementById("edit_kota_saya").style.display = 'none';
        document.getElementById("edit_kota_saya2").style.display = 'none';

        document.getElementById("astket").innerHTML = "";
        document.getElementById("ast1").innerHTML = "";
        document.getElementById("ast2").innerHTML = "";
        document.getElementById("ast3").innerHTML = "";
        document.getElementById("ast4").innerHTML = "";
        document.getElementById("ast5").innerHTML = "";
    }
    $(document).ready(function(){
          $('#provinsi').change(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            var kode_provinsi = document.getElementById('provinsi').value;
            if(kode_provinsi == "")
                kode_provinsi = "KOSONG"
            var path =  document.getElementById('base_path').value;
            var url = path+"get_kota/"+kode_provinsi;
            
            $.ajax({
                type: "POST",
                url: url,
                data: $("#form_provinsi").serialize(),
                success: function (data) {
                    $("#kota").html(data);
                },
                error: function (data) {
                    alert('ooo');
                }
            });
          });
      });
</script>