<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<style type="text/css">
    .table.table-borderless td, .table.table-borderless th {
        border: 0 !important;
    }
</style>
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<input type="hidden" name="base_path" id="base_path" value="{{ asset('/') }}">
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <a data-toggle='modal' data-target='#pengaturan_so' class="pull-right">
                    <button class="btn bg-navy flat"><span class="fa fa-gear"></span> Pengaturan</button>
                </a>
                <a data-toggle='modal' data-target='#ubah_format_no_nota_sales_order'>
                    <button class="btn btn-warning flat pull-right" type="button" style="margin-right:10px"><span class="fa fa-edit"></span> Ubah Format No Invoice</button>
                </a>
                <a data-toggle="modal" data-target="#tambah_pelanggan">
                    <button class="btn bg-purple pull-right flat" style="margin-right:10px"><span class="fa fa-plus-circle"></span> Tambah Pelanggan Baru</button>
                </a>
            </div>
            <div class="box-body">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                @if($errors->has('nama'))
                <div class="alert alert-danger alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-ban"></i>
                  {{ $errors->first('nama') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                @if($errors->has('email'))
                <div class="alert alert-danger alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-ban"></i>
                  {{ $errors->first('email') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                @if($errors->has('alamat'))
                <div class="alert alert-danger alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-ban"></i>
                  {{ $errors->first('alamat') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                @if($errors->has('provinsi'))
                <div class="alert alert-danger alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-ban"></i>
                  {{ $errors->first('provinsi') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                @if($errors->has('kota_id'))
                <div class="alert alert-danger alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-ban"></i>
                  kolom isian kota wajib diisi
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/so_proses') }}" id="form_so" autocomplete="off">
                    {{ csrf_field() }}
                    <input type="hidden" name="pelanggan_id" id="pelanggan_id">
                    <input type="hidden" name="jumlah_produk" id="jumlah_produk" value="0">
                    <input type="hidden" name="input_kode_voucher" id="input_kode_voucher">
                    <input type="hidden" name="tunai" id="input_tunai">
                    <input type="hidden" name="k_debit" id="input_k_debit">
                    <input type="hidden" name="k_kredit" id="input_k_kredit">
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('no_nota') ? ' has-error' : '' }}">
                            <label class="col-sm-3 control-label">No Sales Order*</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Masukkan no sales order" name="no_nota" value="{{ $no_invoice }}">
                                @if ($errors->has('no_nota'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('no_nota') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('nama_pelanggan') ? ' has-error' : '' }}">
                                <label class="col-sm-3 control-label">Nama</label>
                                <div class="col-sm-6">
                                    <input type="text" name="nama_pelanggan" id="nama_pelanggan" class="form-control" disabled="true">
                                </div>
                                <div class="com-md-3">
                                    <a data-toggle="modal" data-target="#pilih_pelanggan">
                                        <button class="btn bg-maroon pull-right flat"><span class="fa fa-user"></span> Pilih Pelanggan</button>
                                    </a>
                                </div>
                            </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Catatan</label>
                            <div class="col-sm-9">
                                <textarea name="catatan" class="form-control" placeholder="Masukkan Catatan" style="text-transform:capitalize">{{ old('catatan') }}</textarea>
                            </div>
                        </div>
                        <!-- <div class="form-group">
                            <label class="col-sm-3 control-label">Jatuh Tempo</label>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="jatuh_tempo" data-date-format="dd-mm-yyyy" name="jatuh_tempo" value="" placeholder="Masukkan jatuh_tempo">
                                </div>
                            </div>
                        </div> -->
                        <p>Keterangan (*) : Wajib diisi</p>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Diskon</label>
                            <div class="col-sm-9">
                                <input type="text" name="diskon_pelanggan" id="diskon_pelanggan" class="form-control" disabled="true" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Voucher</label>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="" name="kode_voucher" id="4" class="form-control" placeholder="Masukkan Kode">
                                    </div><button class="btn bg-purple flat" type="button" onclick="cek_voucher()"><span class="fa fa-plus-circle"></span> Tambah Voucher</button>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Syarat Ketentuan</label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="syarat_ketentuan">@if(!empty($pengaturan_so)){{ $pengaturan_so->syarat_ketentuan }}@endif</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">PPn</label>
                            <div class="col-sm-3">
                                <input type="text" name="ppn" class="form-control" value="<?php if(!empty($pengaturan_so->ppn)){ echo $pengaturan_so->ppn; } ?>">
                            </div>
                            <label class="control-label col-md-1">%</label>
                        </div>
                    </div>
                </form>
                <hr>
                <h4>Transaksi<a data-toggle="modal" data-target="#pilih_produk" >
                        <button class="btn bg-maroon pull-right flat" style="display:none" id="btn_pilih_produk"><span class="fa fa-archive"></span> Pilih Produk</button></a>
                </h4>
                <hr>
                <div class="box-body">
                    <div id="admin_cart">
                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                    <button class="btn btn-warning flat" style="display:none" id="update" onclick="update()"><span class="glyphicon glyphicon-refresh"></span> Update</button>
                    </div>
                    <div class="col-md-6">
                        <button class="btn btn-success flat pull-right" type="submit" style="display:none" id="checkout" onclick="btn_proses_klik()"><span class="fa fa-arrow-circle-right"></span> Proses</button>
                    </div>
                </div>
            </div>
            <div class="overlay" id="loading" style="display:none">
              <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="pilih_pelanggan" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="riwayatheader">Pilih Pelanggan</h4>
      </div>
      <div class="modal-body">
          <div class="box-body">
            <table class="table table-striped table-bordered" id="mydatatables2">
                <thead>
                    <th style="text-align:center">NO</th>
                    <th style="text-align:center">NAMA</th>
                    <th style="text-align:center">ALAMAT</th>
                    <th style="text-align:center">DISKON</th>
                    <th style="text-align:center"></th>
                </thead>
                <tbody>
                    <?php $i=1; ?>
                    @foreach($pelanggan as $val)
                    <tr>
                        <td align="center">{{ $i++ }}</td>
                        <td>{{ $val->nama }}</td>
                        <td>{{ $val->alamat }}, <b>{{ ucwords(strtolower($val->kota_nama)) }}</b></td>
                        <td align="center">
                            <?php $default_diskon = 0; ?>
                            @if(!empty($val->kategori_pelanggan->default_diskon))
                                {{ $val->kategori_pelanggan->default_diskon }}%
                                <?php $default_diskon=$val->kategori_pelanggan->default_diskon; ?>
                            @else 
                            {{ '-' }}
                            @endif</td>
                        <td align="center">
                            <button class="btn bg-maroon flat" type="button" onclick="pilih_pelanggan('{{ $val->id }}', '{{ $val->nama }}', '{{ $default_diskon }}')" data-dismiss="modal"><span class="fa fa-check"></span> Pilih</button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
          </div><!-- /.box-body -->
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="pilih_produk" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="riwayatheader">Pilih Produk</h4>
            </div>
            <div class="modal-body">
                <div class="box-body" id="so_produk">
                    
                </div>
            </div>
        </div>
  </div>
</div>

<div class="modal fade" id="tambah_pelanggan" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content box">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="riwayatheader">Tambah Pelanggan</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="post" action="{{ url('/tambah_pelanggan_baru') }}" autocomplete="off">
        {{ csrf_field() }}
            <div class="form-group">
                <label class="col-sm-2 control-label">Nama</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukkan nama">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Email</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="email" name="email" placeholder="Masukkan email">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Alamat</label>
                <div class="col-sm-10">
                <textarea type="text" class="form-control" id="alamat" name="alamat" placeholder="Masukkan alamat (min 4 karakter)"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Kabupaten/Kota</label>
                <div class="col-md-5">
                    <select class="form-control" id="provinsi" name="provinsi">
                        <option value="">-- Pilih Provinsi --</option>
                        @foreach($provinsi as $val)
                        <option value="{{ $val->kode }}">{{ $val->nama }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-5">
                    <select class="form-control" id="kota_id" name="kota_id">
                        <option value="">-- Pilih Kabupaten/Kota --</option>
                    </select>
                </div>
            </div><!-- /.box-body -->
            <div class="form-group">
                <label class="col-sm-2 control-label">No Handphone</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" data-inputmask='"mask": "(+62)999-9999-9999"' data-mask2 placeholder="contoh: (+62)857-9999-9999" name="hp" value="{{ old('hp') }}">
                </div>
            </div>
            <div class="box-footer">
                <button data-dismiss="modal" class="btn btn-default flat"><span class="glyphicon glyphicon-ban-circle"></span> Batal</button>
                <button type="submit" class="btn btn-primary pull-right flat" onclick="document.getElementById('loading3').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
          </div><!-- /.box-footer -->
        </form>
      </div>
      <div class="overlay" id="loading3" style="display:none">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
    </div>
  </div>
</div>

<div class="modal fade" id="ubah_format_no_nota_sales_order" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content box">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="riwayatheader">Ubah Format No Invoice</h4>
            </div>
            <form method="post" action="{{ url('/ubah_format_no_nota_sales_order') }}" autocomplete="off" class="form-horizontal">
            {{ csrf_field() }}
            <div class="modal-body">
                <div class="form-group">
                    <label class="control-label col-md-2">Format</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="format" value="{{ $ori_format_invoice }}"  style="text-transform:uppercase;">
                    </div>
                </div>
                <br>
                <p>Untuk menambahkan template tanggal: gunakan tag 'DD' (hari), 'MM' (bulan), 'YY' (tahun), 'YYYY' (tahun)</p>
                <p>Contoh : DD-MM-YYYY</p>
                <p>Untuk menambahkan template angka: gunakan tag #</p>
                <p>Contoh template angka 4 digit : ####</p>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default pull-left flat"><span class="glyphicon glyphicon-ban-circle"></span> batal</button>
                <button type="submit" class="btn btn-primary pull-right flat" onclick="document.getElementById('loading4').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
            </div><!-- /.box-footer -->
            </form>
            <div class="overlay" id="loading4" style="display:none">
              <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>
<script src="{{ url('js/jquery.maskMoney.js') }}" type="text/javascript"></script>
<div class="modal fade" id="proses_pembayaran" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content box">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Proses</h4>
            </div>
            <div class="form-horizontal">
            <div class="modal-body">
                {{ csrf_field() }}
                <div class="form-group">
                    <label class="col-sm-3 control-label">Total Tagihan</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control" disabled="true" id="display_total_tagihan">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Tunai</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control" name="tunai" placeholder="tunai" id="tunai" data-thousands="." data-decimal="," style="text-align:right;"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">K. Debit</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control" name="k_debit" placeholder="kartu kredit" id="k_debit" data-thousands="." data-decimal="," style="text-align:right;"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">K. Kredit</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control" name="k_kredit" placeholder="kartu kredit" id="k_kredit" data-thousands="." data-decimal="," style="text-align:right;"/>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default flat pull-left"><span class="glyphicon glyphicon-ban-circle"></span> Batal</button>
                <button type="submit" class="btn btn-success flat" onclick="btn_proses_klik()"><span class="fa fa-arrow-circle-right"></span> Proses</button>
            </div>
            </div>
            <div class="overlay" id="loading5" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="pengaturan_so" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content box">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="riwayatheader">Pengaturan SO</h4>
            </div>
            <form class="form-horizontal" action="{{ url('/pengaturan_so') }}" autocomplete="off" method="post">
            {{ csrf_field() }}
            <div class="modal-body">
                <div class="form-group">
                    <label class="control-label col-md-2">Syarat dan Ketentuan</label>
                    <div class="col-md-8">
                        <textarea class="form-control" name="syarat_ketentuan">@if(!empty($pengaturan_so)){{ $pengaturan_so->syarat_ketentuan }}@endif</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2">PPN</label>
                    <div class="col-md-2">
                        <div class="checkbox" >
                            <label >
                              <input name="checkbox_ppn" id="checkbox_ppn" type="checkbox" <?php if(!empty($pengaturan_so->ppn)){ echo "checked";  }?> value="checked"> Include PPN
                            </label>
                        </div>
                    </div>
                    <div class="col-md-2" id="display_ppn">
                        <input type="txt" name="ppn" class="form-control" onKeyPress="return numbersonly(this, event)" <?php if(!empty($pengaturan_so->ppn)){ ?> value='{{ $pengaturan_so->ppn }}' <?php }?>>
                    </div>
                    <div class="col-md-2" id="display_persen">
                    <label class="control-label">%</label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default flat pull-left"><span class="glyphicon glyphicon-ban-circle"></span> Batal</button>
                <button type="submit" class="btn btn-primary flat" onclick="document.getElementById('loading6').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
            </div>
            </form>
            <div class="overlay" id="loading6" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

@stop

<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
<script>
  $(function () {
    $("#mydatatables").DataTable();
    $("#mydatatables2").DataTable();

    $('#jatuh_tempo').datepicker({
      autoclose: true,
      todayHighlight: 1,
    });

    $("#tunai").maskMoney({precision:0});
    $("#k_debit").maskMoney({precision:0});
    $("#k_kredit").maskMoney({precision:0});
  });

    function pilih_pelanggan(id,nama,diskon)
    {
        document.getElementById("pelanggan_id").value       = id;
        document.getElementById("nama_pelanggan").value     = nama;
        document.getElementById("diskon_pelanggan").value   = diskon;
        document.getElementById('btn_pilih_produk').style.display = "block";
        
        //UBAH NILAI SO_PRODUK BERDASARKAN pelanggan
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })

        var path =  document.getElementById('base_path').value;
        var url = "{{ url('/get_so_produk') }}";
        
        $.ajax({
            type: "POST",
            url: url,
            data: { id:id },
            success: function (data) {
                $("#so_produk").html(data);
                document.getElementById("admin_cart").innerHTML = '';
                document.getElementById("checkout").style.display = 'none';
            },
            error: function (data) {
                alert('terjadi kesalahan');
            }
        });
    }

    function pilih_produk(produk_id)
    {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })

        var pelanggan_id = document.getElementById("pelanggan_id").value;
        var url = "{{ url('/so_tambah_cart') }}";
        
        $.ajax({
            type: "POST",
            url: url,
            data: { produk_id:produk_id, pelanggan_id:pelanggan_id },
            beforeSend: function(){
                        document.getElementById('loading').style.display = "block";
                      },
            success: function (data) {
                $("#admin_cart").html(data);
                document.getElementById("checkout").style.display = 'block';
                document.getElementById("update").style.display = 'block';
                document.getElementById('loading').style.display = "none";
            },
            error: function (data) {
                alert('terjadi kesalahan');
            }
        });
    }

    function jml_klik(rowid,qty)
    {
        var jumlah = prompt("Jumlah", qty);
        if(jumlah != null)
        {
            //update cart
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });


            var path =  document.getElementById('base_path').value;
            var url = path+"/so_cart_jumlah/"+rowid+"/"+jumlah;
            
            $.ajax({
                type: "POST",
                url: url,
                beforeSend: function(){
                            document.getElementById('loading').style.display = "block";
                          },
                success: function (data) {
                    $("#admin_cart").html(data);
                    document.getElementById('loading').style.display = "none";
                },
                error: function (data) {
                    alert('gagal menambahkan produk promo ke keranjang belanja');
                }
            });
        }
  }

  $(document).ready(function(){
          $('#kategori_id').change(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            var url = "{{ url('/get_produk_grid') }}";

              $.ajax({
                type: "POST",
                url: url,
                data: $("#form_produk").serialize(),
                beforeSend: function(){
                        document.getElementById('loading').style.display = "block";
                      },
                success: function (data) {
                   document.getElementById("content").innerHTML = data;
                   document.getElementById('loading').style.display = "none";
                },
                error: function (data) {
                    alert('ooo');
                }
            });
          });
          $(document).on('click', '.pagination a', function (e) {
            getPosts($(this).attr('href').split('page=')[1]);
            e.preventDefault();
        });

          $('#provinsi').change(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            var provinsi_kode = document.getElementById('provinsi').value;
            if(provinsi_kode == "")
            {
                provinsi_kode = "ALL";
            }
            var path =  document.getElementById('base_path').value;
            var url = path+"get_kota/"+provinsi_kode;

            $.ajax({
                type: "POST",
                url: url,
                success: function (data) {
                    $("#kota_id").html(data);
                },
                error: function (data) {
                    alert('ooo');
                }
            });
          });
          $("[data-mask2]").inputmask();
    });
    function getPosts(page) {
        var path =  document.getElementById('base_path').value;
        var url = path+'test?page=' + page;
        $.ajax({
            url : url,
            data: $("#form_produk").serialize(),
            dataType: 'json',
        }).done(function (data) {
            document.getElementById("content").innerHTML = data;
            location.hash = page;
        }).fail(function () {
            alert('Posts could not be loaded.');
        });
    }
    function cek_voucher()
    {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })

        var url = "{{ url('/cek_voucher_admin') }}";
        
        $.ajax({
            type: "POST",
            url: url,
            data: $("#form_cek_voucher").serialize(),
            beforeSend: function(){
                        document.getElementById('loading3').style.display = "block";
                      },
            success: function (data) {
                if(data == '1')
                    alert('Kode voucher tidak sesuai');
                else
                    $("#admin_cart").html(data);

                document.getElementById('loading3').style.display = "none";
            },
            error: function (data) {
                alert('gagal menambahkan kode voucher');
            }
        });
    }

    function update()
    {
        document.getElementById('cart_pelanggan_id').value = document.getElementById('pelanggan_id').value;

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })

        var url = "{{ url('/so_update_cart') }}";
        
        $.ajax({
            type: "POST",
            url: url,
            data: $("#admin_ubah_cart").serialize(),
            beforeSend: function(){
                document.getElementById('loading').style.display = "block";
            },
            success: function (data) {
                $("#admin_cart").html(data);
                document.getElementById('loading').style.display = "none";
            },
            error: function (data) {
                alert('ooo');
            }
        });
    }

    function hapus(rowId)
    {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })
        
        var url = "{{ url('/so_hapus_cart') }}";
        
        $.ajax({
            type: "POST",
            url: url,
            data: { rowId:rowId },
            beforeSend: function(){
                document.getElementById('loading').style.display = "block";
            },
            success: function (data) {
                $("#admin_cart").html(data);
                document.getElementById('loading').style.display = "none";
            },
            error: function (data) {
                alert('ooo');
            }
        });
    }

    function proses_pembayaran()
    {
        document.getElementById("display_total_tagihan").value = document.getElementById('total_tagihan').value;
    }

    function btn_proses_klik()
    {
        document.getElementById('loading').style.display   ='block'
        document.getElementById('loading5').style.display   ='block'
        document.getElementById('input_tunai').value        = document.getElementById('tunai').value;
        document.getElementById('input_k_debit').value      = document.getElementById('k_debit').value;
        document.getElementById('input_k_kredit').value     = document.getElementById('k_kredit').value;
        document.getElementById('form_so').submit();
    }
</script>
