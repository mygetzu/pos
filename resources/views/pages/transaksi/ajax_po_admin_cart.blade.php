<?php
    function rupiah($nominal)
    {
        $rupiah =  number_format($nominal,0, ",",".");
        $rupiah = "Rp "  . $rupiah;
        return $rupiah;
    }
?>
<script src="{{ url('js/jquery.maskMoney.js') }}" type="text/javascript"></script>
<form id="admin_ubah_cart" autocomplete="off">
    {{ csrf_field() }}
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <th width="5%" style="text-align:center">NO</th>
                <th style="text-align:center">NAMA PRODUK</th> 
                <th style="text-align:center">SPESIFIKASI</th>                    
                <th style="text-align:center">HARGA</th>
                <th width="10%" style="text-align:center">JUMLAH</th>
                <th style="text-align:center">TOTAL HARGA</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php $i = 1; $jumlah=0; $total_tagihan=0; $row=1; ?>
            @foreach($cart_content as $cart)
            <tr>
                <td align="center">{{ $i++ }}</td>
                <td>
                    <?php $name = explode('---', $cart->name) ?>
                    @if($cart->name === 'voucher')
                    <b>{{ $name[0] }}</b>
                    @else
                        {{ $name[0] }} 
                        @if(strpos($cart->id, 'DIS') === 0) 
                        <b>(*diskon)</b> 
                        @elseif(strpos($cart->id, 'PKT') === 0) 
                        <b>(*paket)</b> 
                        @elseif(strpos($cart->id, 'HDH') === 0) 
                        <b>(*hadiah)</b>
                        @endif
                    @endif
                </td>
                <td>
                    <textarea name="spesifikasi{{ $row }}" class="form-control">@if(!empty($name[1])){{ $name[1] }}@endif</textarea>
                </td>
                <td align="right" width="12%">
                    <input type="hidden" name="rowid{{ $row }}" value="{{ $cart->rowId }}">
                    <input type="text" class="form-control" name="harga{{ $row }}" placeholder="Masukkan Harga" id="harga{{ $cart->rowId }}" data-thousands="." data-decimal="," style="text-align:right;margin-left:-15px" value="{{ $cart->price }}" />
                    <script type="text/javascript">$("#harga{{ $cart->rowId }}").maskMoney({precision:0});</script>
                </td>
                <td align="center">
                    <input type="text" name="jumlah{{ $row }}" id="jumlah" class="form-control" value="{{ $cart->qty }}" style="text-align:right">
                </td>
                <td align="right" style="padding-right:20px" width="15%">{{ rupiah($cart->price*$cart->qty) }}</td>
                <td align="center">
                    <button type="button" class="btn btn-danger flat" onclick="hapus('{{ $cart->rowId }}')"><span class="glyphicon glyphicon-trash"></span> Hapus</button>
                </td>
            </tr>
            <?php 
                $jumlah += $cart->qty;
                $total_tagihan += ($cart->price*$cart->qty);
                $row++;
            ?>
            @endforeach
            <tr>
                <td></td>
                <td colspan="3" align="center">TOTAL</td>
                <td align="right"><p><b>{{ $jumlah }}</b></p></td>
                <td align="right"><p><b>{{ rupiah($total_tagihan) }}</b></p></td>
                <td></td>
            </tr>
        </tbody>
    </table>
    <input type="hidden" name="total" value="{{ $row }}">
</form>