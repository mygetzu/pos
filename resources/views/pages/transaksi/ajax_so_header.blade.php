<?php
    function rupiah($nominal)
    {
        $rupiah =  number_format($nominal,0, ",",".");
        $rupiah = "Rp "  . $rupiah;
        return $rupiah;
    }
?>
<table class="table table-striped table-bordered" id="ajax_produk_tabless2">
    <thead>
        <tr>
            <th style="text-align:center">NO</th>
            <th style="text-align:center">NO NOTA</th>
            <th style="text-align:center">NAMA PELANGGAN</th>
            <th style="text-align:center">TANGGAL</th>
            <th style="text-align:center"></th>
        </tr>
    </thead>
    <tbody>
        <?php for($i=0; $i<count($nota_jual); $i++) { ?>
        <tr>
            <td align="center">{{ $i+1 }}</td>
            <td>{{ $nota_jual[$i]['no_nota'] }}</td>
            <td>{{ $nota_jual[$i]['nama_pelanggan'] }}</td>
            <td>{{ $nota_jual[$i]['tanggal'] }}</td>
            <td align="center">
                <?php $id_nota_jual = $nota_jual[$i]['id']; $no_nota = $nota_jual[$i]['no_nota'];  ?>
                <button class="btn bg-maroon flat" data-dismiss='modal' type="button" onclick="btn_pilih_nota('{{ $id_nota_jual }}', '{{ $no_nota }}')"><span class="fa fa-check"></span> Pilih</button>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>
<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
  $(function () {
    $("#ajax_produk_tabless2").DataTable();
  });
</script>