<?php
    function rupiah($nominal)
    {
        $rupiah =  number_format($nominal,0, ",",".");
        $rupiah = "Rp "  . $rupiah;
        return $rupiah;
    }
?>
<table class="table table-striped table-bordered" id="ajax_produk_tabless">
    <thead>
        <tr>
            <th>NO</th>
            <th>NAMA</th>
            <th>HARGA RETAIL</th>
            <th>HARGA SUPPLIER</th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php $i=1; ?>
        @foreach($produk as $key => $val)
        <tr>
            <td align="center">{{ $key+1 }}</td>
            <td>{{ $val->nama }}</td>
            <td>{{ rupiah($val->harga_retail) }}</td>
            <td>{{ rupiah($val->harga_terakhir) }}</td>
            <td>
                @if(empty($val->file_gambar))
                <img src="{{ URL::asset('img/produk/'.$gambar_produk_default) }}" style="height:50px;object-fit: contain;width:auto;padding: 5px 5px 0px 5px" class="img-responsive" alt="">
                @else
                <img src="{{ URL::asset('img/produk/'.$val->file_gambar) }}" style="height:50px;object-fit: contain;width:auto;padding: 5px 5px 0px 5px" class="img-responsive" alt="">
                @endif
            </td>
            <td align="center">
                <button class="btn bg-maroon flat" type="button" onclick="pilih_produk('{{ $val->id }}', '{{ $val->jenis }}')" data-dismiss="modal"><span class="fa fa-check"></span> Pilih</button>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
  $(function () {
    $("#ajax_produk_tabless").DataTable();
  });
</script>