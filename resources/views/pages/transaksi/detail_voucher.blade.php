@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <div class="pull-right">
                    <a href="{{ url('/voucher') }}">
                        <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                    </a>
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
                        <th width="20%">Kode Voucher</th>
                        <th width="5%">:</th>
                        <td><input type="text" class="form-control" value="{{ $voucher->kode }}" disabled="true"></td>
                    </tr>
                    <tr>
                        <th>Nominal</th>
                        <th>:</th>
                        <td><input type="text" class="form-control" value="{{ rupiah($voucher->nominal) }}" disabled="true"></td>
                    </tr>
                    <tr>
                        <th>Awal Periode</th>
                        <th>:</th>
                            <?php
                            $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
                            date_default_timezone_set("Asia/Jakarta");
                            $awal_periode = new DateTime($voucher->awal_periode);
                            ?>
                        <td><input type="text" class="form-control" name="alamat" id="alamat" value="{{ $awal_periode->format('d') }} {{ $bulan[(int)$awal_periode->format('m')] }} {{ $awal_periode->format('Y') }}" disabled="true"></td>
                    </tr>
                    <tr>
                        <th>Akhir Periode</th>
                        <th>:</th>
                            <?php
                            $akhir_periode = new DateTime($voucher->akhir_periode);
                            ?>
                        <td><input type="text" class="form-control" name="alamat" id="alamat" value="{{ $akhir_periode->format('d') }} {{ $bulan[(int)$akhir_periode->format('m')] }} {{ $akhir_periode->format('Y') }}" disabled="true"></td>
                    </tr>
                    <tr>
                        <th>Status</th>
                        <th>:</th>
                        <td><input type="text" class="form-control" name="telp" id="telp" value="<?php if($voucher->is_aktif == 1) echo 'aktif'; else echo 'tidak aktif'; ?>" disabled="true"></td>
                    </tr>
                    <tr>
                        <th>Tanggal Dipakai</th>
                        <th>:</th>
                            <?php
                            $tanggal_dipakai = new DateTime($voucher->tanggal_dipakai);
                            ?>
                        <td><input type="text" class="form-control" name="alamat" id="alamat" value="{{ $tanggal_dipakai->format('d') }} {{ $bulan[(int)$tanggal_dipakai->format('m')] }} {{ $tanggal_dipakai->format('Y') }}" disabled="true"></td>
                    </tr>
                    @if(!empty($pelanggan_nama_target))
                    <tr>
                        <th>Nama Pelanggan Target Voucher</th>
                        <th>:</th>
                        <td><input type="text" class="form-control" value="{{ $pelanggan_nama_target->nama }}" disabled="true"></td>
                    </tr>
                    @endif
                    @if(!empty($pelanggan_nama_pakai))
                    <tr>
                        <th>Nama Pelanggan Pemakai Voucher</th>
                        <th>:</th>
                        <td><input type="text" class="form-control" value="{{ $pelanggan_nama_pakai->nama }}" disabled="true"></td>
                    </tr>
                    @endif
                </table>
            </div>
        </div>
    </div>
</div>
@stop
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>