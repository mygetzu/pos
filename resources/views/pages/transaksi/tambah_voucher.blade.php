<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <div class="pull-right">
                    <a href="{{ url('/voucher') }}">
                        <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                    </a>
                </div>
            </div>
            <div class="box-body">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <form class="form-horizontal" method="POST" action="{{ url('/do_tambah_voucher') }}" autocomplete="off">
                    <table class="table table-striped" id="table_voucher">
                        <br>
                        {{ csrf_field() }}
                        <tr class="{{ $errors->has('kode_voucher') ? ' has-error' : '' }}">
                            <th width="22%"><label class="control-label">Kode Voucher</label></th>
                            <th width="5%">:</th>
                            <td>
                            <div class="row container-fluid">
                            <div class="col-md-8">
                                <input type="text" name="kode_voucher_display" id="kode_voucher_display" class="form-control" disabled="true">
                                <input type="hidden" name="kode_voucher" id="kode_voucher" class="form-control">
                                @if ($errors->has('kode_voucher'))
                                    <span class="help-block">
                                        <strong>Generate Kode Voucher Terlebih Dahulu</strong>
                                    </span>
                                @endif
                            </div>
                            <button class="btn bg-navy flat" type="button" onclick="generate_kode()"><span class="glyphicon glyphicon-refresh"></span> Generate</button>
                            </div>
                            </td>
                        </tr>
                        <tr class="{{ $errors->has('nominal') ? ' has-error' : '' }}">
                            <th width="22%"><label class="control-label">Nominal</label></th>
                            <th width="5%">:</th>
                            <td>
                            <div class="col-md-3">
                                <input value="{{ old('nominal') }}"  type="text" name="nominal" id="nominal" class="form-control" placeholder="Masukkan Nominal" data-thousands="." data-decimal="," style="text-align:right" >
                            <script type="text/javascript">$("#nominal").maskMoney({precision:0});</script>
                                @if ($errors->has('nominal'))
                                    <span class="help-block">
                                        <strong>Kolom Nominal Wajib Diisi</strong>
                                    </span>
                                @endif
                            </div>
                            <label class="control-label">(IDR)</label>
                            </td>
                        </tr>
                        <tr class="{{ $errors->has('awal_periode') ? ' has-error' : '' }}">
                            <th width="22%"><label class="control-label">Awal Periode</label></th>
                            <th width="5%">:</th>
                            <td>
                            <div class="input-group container-fluid" style="margin-bottom:0px">
                                <div class="input-group-addon">
                                  <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="awal_periode" data-date-format="dd-mm-yyyy" name="awal_periode" value="{{ old('awal_periode') }}" placeholder="Masukkan awal periode">
                            </div>
                                @if ($errors->has('awal_periode'))
                                    <span class="help-block">
                                        <strong>Kolom Awal Periode Wajib Diisi</strong>
                                    </span>
                                @endif
                            </td>
                        </tr>
                        <tr class="{{ $errors->has('akhir_periode') ? ' has-error' : '' }}">
                            <th width="22%"><label class="control-label">Akhir Periode</label></th>
                            <th width="5%">:</th>
                            <td>
                            <div class="input-group container-fluid" style="margin-bottom:0px">
                                <div class="input-group-addon">
                                  <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="akhir_periode" data-date-format="dd-mm-yyyy" name="akhir_periode" value="{{ old('akhir_periode') }}" placeholder="Masukkan akhir periode">
                            </div>
                            @if ($errors->has('akhir_periode'))
                                    <span class="help-block">
                                        <strong>Kolom Akhir Periode Wajib Diisi</strong>
                                    </span>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th width="22%"><label class="control-label">Tipe</label></th>
                            <th width="5%">:</th>
                            <td>
                            <div class="col-md-12">
                                <select class="form-control" id="is_sekali_pakai" name="is_sekali_pakai"> 
                                    <option value="1">Sekali Pakai</option>
                                    <option value="0">Dapat dipakai berkali-kali</option>
                                </select>
                                @if ($errors->has('is_sekali_pakai'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('is_sekali_pakai') }}</strong>
                                    </span>
                                @endif
                            </div>
                            </td>
                        </tr>
                        <tr>
                            <th width="22%"><label class="control-label">Target</label></th>
                            <th width="5%">:</th>
                            <td>
                            <div class="col-md-12">
                                <select class="form-control" id="target_pelanggan" name="target_pelanggan"> 
                                    <option value="1">Semua pelanggan</option>
                                    <option value="2">pelanggan Tertentu</option>
                                </select>
                                @if ($errors->has('pelanggan_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('pelanggan_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                            </td>
                        </tr>
                        <tr>
                            <th><label class="control-label" id="label1_pilih_pelanggan" style="display:none;text-align:left">Pilih pelanggan</label></th>
                            <td><label class="control-label" id="label2_pilih_pelanggan" style="display:none;text-align:left">:</label></td>
                            <td>
                            <div class="row container-fluid" id="button_pilih_pelanggan" style="display:none">
                            <input type="hidden" name="pelanggan_id" id="pelanggan_id" class="form-control">
                            <div class="col-md-4" id="div_pelanggan_nama" style="display:none">
                                <input type="text" name="pelanggan_nama" id="pelanggan_nama" class="form-control" disabled="true">
                            </div>
                            <div class="col-md-4">
                                <a data-toggle="modal" data-target="#pilih_pelanggan">
                                    <button class="btn bg-maroon flat" type="button" id="button_pilih"><span class="fa fa-user"></span> Pilih pelanggan</button>
                                </a>
                            </div>
                            </div>
                            </td>
                        </div>
                        <tr>
                            <th><label class="control-label"></label></th>
                            <th></th>
                            <td>
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary flat" onclick="document.getElementById('loading').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
                            </div>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
            <div class="overlay" id="loading" style="display:none">
              <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="pilih_pelanggan" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="riwayatheader">Pilih pelanggan</h4>
      </div>
      <div class="modal-body">
          <div class="box-body">
            <table class="table table-striped table-bordered" id="mydatatables">
                <thead>
                    <th style="text-align:center">NO</th>
                    <th style="text-align:center">NAMA</th>
                    <th style="text-align:center">ALAMAT</th>
                    <th style="text-align:center">KOTA</th>
                    <th style="text-align:center"></th>
                </thead>
                <tbody>
                    <?php $i=1; ?>
                    @foreach($pelanggan as $val)
                    <tr>
                        <td align="center">{{ $i++ }}</td>
                        <td>{{ $val->nama }}</td>
                        <td>{{ $val->alamat }}</td>
                        <td>{{ $val->kota_nama }}</td>
                        <td align="center">
                            <button class="btn bg-maroon flat" type="button" onclick="pilih_pelanggan('{{ $val->id }}', '{{ $val->nama }}')" data-dismiss="modal"><span class="fa fa-check"></span> Pilih</button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
          </div><!-- /.box-body -->
      </div>
    </div>
  </div>
</div>

@stop

<script src="{{ asset('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ url('js/jquery.maskMoney.js') }}" type="text/javascript"></script>
<script type="text/javascript">
  $(function () {
    $("#mydatatables").DataTable();

    $('#awal_periode').datepicker({
      autoclose: true,
      todayHighlight: 1,
    });

    $('#akhir_periode').datepicker({
      autoclose: true,
      todayHighlight: 1,
    });
  });

    function generate_kode()
    { 
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })

        var url = "{{ url('/generate_kode_voucher') }}";
        
        $.ajax({
            type: "POST",
            url: url,
            beforeSend: function(){
                document.getElementById('loading').style.display = "block";
            },
            success: function (data) {
                document.getElementById("kode_voucher_display").value   = data;
                document.getElementById("kode_voucher").value           = data;
                document.getElementById('loading').style.display        = "none";
            },
            error: function (data) {
                alert('gagal generate kode voucher');
            }
        });
    }

    function pilih_pelanggan(id, nama)
    {
        document.getElementById("pelanggan_id").value               = id;
        document.getElementById("pelanggan_nama").value             = nama;
        document.getElementById("div_pelanggan_nama").style.display = 'block';
    }

    $(document).ready(function(){
          $('#kode_kategori').change(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            var url = "{{ url('/get_produk') }}";

              $.ajax({
                type: "POST",
                url: url,
                data: $("#form_produk").serialize(),
                success: function (data) {
                    $("select#kode_produk").html(data);;
                },
                error: function (data) {
                    alert('ooo');
                }
            });
          });

          $('#target_pelanggan').change(function(){
            if(document.getElementById("target_pelanggan").value == 1){
              document.getElementById("label1_pilih_pelanggan").style.display = 'none';
              document.getElementById("label2_pilih_pelanggan").style.display = 'none';
              document.getElementById("button_pilih_pelanggan").style.display = 'none';
            }
            else{
              document.getElementById("label1_pilih_pelanggan").style.display = 'block';
              document.getElementById("label2_pilih_pelanggan").style.display = 'block';
              document.getElementById("button_pilih_pelanggan").style.display = 'block';
            }
          });

      });
</script>