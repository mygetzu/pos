<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="nav-tabs-custom tab-primary flat">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">Retur Jual</a></li>
              <li><a href="#tab_2" data-toggle="tab"></a></li>
              <li class="pull-right">
                <a href="{{ url('/retur_jual') }}" class="pull-right" style="padding:0px">
                    <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                </a>
              </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                    <table class="table table-striped">
                    <tr>
                        <th width="20%">No Nota</th>
                        <th width="5%">:</th>
                        <td>
                            <div class="col-md-6 row">
                                <input type="text" class="form-control" value="{{ $rj_header->no_nota }}" disabled="true">
                            </div>
                            <div class="col-md-4">
                                <a href="{{ url('/nota_retur_jual/'.$rj_header->id) }}" target="_blank" class="btn btn-info flat"><i class="fa fa-file-text"></i> Nota Retur Jual</a>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>Nama Pelanggan</th>
                        <th>:</th>
                        <td><input type="text" class="form-control" name="nama" id="nama" value="{{ $rj_header->pelanggan->nama }}" disabled="true"></td>
                    </tr>
                    <tr>
                        <th>Tanggal</th>
                        <th>:</th>
                            <?php
                            $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
                            date_default_timezone_set("Asia/Jakarta");
                            $tanggal = new DateTime($rj_header->tanggal);
                            ?>
                        <td><input type="text" class="form-control" name="alamat" id="alamat" value="{{ $tanggal->format('d') }} {{ $bulan[(int)$tanggal->format('m')] }} {{ $tanggal->format('Y') }} {{ $tanggal->format('G') }}:{{ $tanggal->format('i') }}:{{ $tanggal->format('s') }}" disabled="true"></td>
                    </tr>
                    <tr>
                    </tr>
                    <tr>
                        <th>Catatan</th>
                        <th>:</th>
                        <td><textarea class="form-control" disabled="true">{{ $rj_header->catatan }}</textarea></td>
                    </tr>
                    <tr>
                        <th>Potongan Retur</th>
                        <th>:</th>
                        <td><input type="text" class="form-control" value="{{ rupiah($rj_header->potongan_retur) }}" disabled="true"></td>
                    </tr>
                    <tr>
                        <th>Total Retur</th>
                        <th>:</th>
                        <td><input type="text" class="form-control" value="{{ rupiah($rj_header->total_retur) }}" disabled="true"></td>
                    </tr>
                </table>
                </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
        
    </div>
</div>
@stop
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script>
  $(function () {
    $("#mydatatables").DataTable();
    $("#mydatatables2").DataTable();
  });
</script>