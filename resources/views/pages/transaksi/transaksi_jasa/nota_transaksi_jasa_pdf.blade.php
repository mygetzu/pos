<link rel="stylesheet" type="text/css" href="{{ URL::to('/css/nota.css') }}">
@include('includes.base_function')
<?php
    function rupiah2($nominal)
    {
        $rupiah =  number_format($nominal,0, ",",".");
        $rupiah = $rupiah.',00';
        return $rupiah;
    }
?>
<div class="area">
    <section id="header">
        <div class="row">
            <div class="col-5">
                <div class="title">{{ $judul }}</div>
                <div class="company">
                    {{ $perusahaan->nama }} <br>
                    {{ $perusahaan->alamat }} <br>
                    Telp. {{ $perusahaan->telp }} <br>
                    Email. {{ $perusahaan->email }}
                </div>
            </div>
            <div class="col-1"></div>
            <div class="col-6">
                <table class="detail" style="margin-top: 33px">
                    <tr>
                        <td class="title-detail">No. Transaksi</td>
                        <td class="titik">&nbsp;:&nbsp;</td>
                        <td class="data-detail">{{ $transaksi_jasa->no_nota }}</td>
                    </tr>
                    <tr>
                        <td class="title-detail">Tanggal</td>
                        <td class="titik">&nbsp;:&nbsp;</td>
                        <td class="data-detail">
                            <?php $tanggal = new DateTime($transaksi_jasa->tanggal); 
                            echo $tanggal->format('d/m/Y G:i:s');
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="title-detail">Pelanggan</td>
                        <td class="titik">&nbsp;:&nbsp;</td>
                        <td class="data-detail user">{{ $transaksi_jasa->pelanggan->nama }}</td>
                    </tr>
                    <tr>
                        <td class="title-detail" style="vertical-align: top;">Alamat </td>
                        <td class="titik" style="vertical-align: top;">&nbsp;:&nbsp;</td>
                        <td class="data-detail user">{{ $transaksi_jasa->pelanggan->alamat }}<br>
                        Telp. {{ $transaksi_jasa->pelanggan->telp }}<br>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </section>
    <section id="table">
        <div>
            <table class="table">
                <thead>
                    <tr>
                        <th>Deskripsi</th>
                        <th>Biaya</th>
                    </tr>
                </thead>
                <tbody>
                    <td>{{ $transaksi_jasa->deskripsi }}</td>
                    <td style="text-align: center;">{{ rupiah($transaksi_jasa->biaya) }}</td>
                </tbody>
            </table>
        </div>
    </section>
    <section id="footer">
        <div class="col-6">
            <table class="keterangan">
                <tr>
                    <td class="title-detail" style="vertical-align: top;">Terbilang</td>
                    <td class="titik" style="vertical-align: top;">&nbsp;:&nbsp;</td>
                    <td class="data-detail">{{ dibaca($transaksi_jasa->biaya) }} Rupiah</td>
                </tr>
            </table>
        </div>
        <div class="col-6" align="center">
            <div class="col-6">
                <div style="margin-top: 70px;">
                    <div style="text-align: center">
                        <strong>Penerima</strong>
                    </div>
                </div>
                <div style="margin-top: 100px;">
                    <div style="text-align: center">
                        <strong>(.........................)</strong>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div style="margin-top: 70px;">
                    <div style="text-align: center">
                        <strong>Hormat Kami</strong>
                    </div>
                </div>
                <div style="margin-top: 100px;">
                    <div style="text-align: center">
                        <strong>(.........................)</strong>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php echo $print_time->format('d/m/Y G:i:s'); ?>
</div>
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function(){
          window.onload = function() { window.print(); }
      });
</script>