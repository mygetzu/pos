@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<?php
    function rupiah2($nominal)
    {
        $rupiah =  number_format($nominal,0, ",",".");
        $rupiah = $rupiah.',00';
        return $rupiah;
    }
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <div class="pull-right">
                    <a href="{{ url('/transaksi_jasa') }}">
                        <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                    </a>
                </div>
                <a href="{{ url('/nota_transaksi_jasa_print/'.$transaksi_jasa->id) }}" target="_blank" class="btn bg-orange flat"><i class="fa fa-print"></i> Cetak</a>
                <a href="{{ url('/nota_transaksi_jasa_pdf/'.$transaksi_jasa->id) }}" target="_blank" class="btn btn-info flat" style="margin-right: 5px;"><i class="fa fa-download"></i> Unduh PDF</a>
            </div>
            <div class="box-body">
                <div class="area">
                    <section id="header">
                        <div>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="title">{{ $judul }}</div>
                                    <div class="company">
                                        {{ $perusahaan->nama }} <br>
                                        {{ $perusahaan->alamat }} <br>
                                        Telp. {{ $perusahaan->telp }} <br>
                                        Email. {{ $perusahaan->email }}
                                    </div>
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-6">
                                    <table class="detail" style="margin-top: 33px">
                                        <tr>
                                            <td class="title-detail">No. Transaksi</td>
                                            <td class="titik">&nbsp;:&nbsp;</td>
                                            <td class="data-detail">{{ $transaksi_jasa->no_nota }}</td>
                                        </tr>
                                        <tr>
                                            <td class="title-detail">Tanggal</td>
                                            <td class="titik">&nbsp;:&nbsp;</td>
                                            <td class="data-detail">
                                                <?php $tanggal = new DateTime($transaksi_jasa->tanggal); 
                                                echo $tanggal->format('d/m/Y G:i:s');
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="title-detail">Pelanggan</td>
                                            <td class="titik">&nbsp;:&nbsp;</td>
                                            <td class="data-detail user">{{ $transaksi_jasa->pelanggan->nama }}</td>
                                        </tr>
                                        <tr>
                                            <td class="title-detail" style="vertical-align: top;">Alamat </td>
                                            <td class="titik" style="vertical-align: top;">&nbsp;:&nbsp;</td>
                                            <td class="data-detail user">{{ $transaksi_jasa->pelanggan->alamat }}<br>
                                            Telp. {{ $transaksi_jasa->pelanggan->telp }}<br>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section id="table">
                        <div>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Deskripsi</th>
                                        <th>Biaya</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <td>{{ $transaksi_jasa->deskripsi }}</td>
                                    <td align="center">{{ rupiah($transaksi_jasa->biaya) }}</td>
                                </tbody>
                            </table>
                        </div>
                    </section>
                    <section id="footer">
                        <div class="row">
                            <div class="col-md-6">
                                <table class="keterangan">
                                    <tr>
                                    <tr>
                                        <td class="title-detail" style="vertical-align: top;">Terbilang</td>
                                        <td class="titik" style="vertical-align: top;">&nbsp;:&nbsp;</td>
                                        <td class="data-detail">{{ dibaca($transaksi_jasa->biaya) }} Rupiah</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6" align="center">
                                <div class="col-md-6">
                                    <div style="margin-top: 70px;">
                                        <div style="text-align: center">
                                            <strong>Penerima</strong>
                                        </div>
                                    </div>
                                    <div style="margin-top: 100px;">
                                        <div style="text-align: center">
                                            <strong>(.........................)</strong>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div style="margin-top: 70px;">
                                        <div style="text-align: center">
                                            <strong>Hormat Kami</strong>
                                        </div>
                                    </div>
                                    <div style="margin-top: 100px;">
                                        <div style="text-align: center">
                                            <strong>(.........................)</strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php echo $print_time->format('d/m/Y G:i:s'); ?>
                    </section>
            </div>
        </div>
    </div>
</div>
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ URL::to('/css/invoice.css') }}">
@stop