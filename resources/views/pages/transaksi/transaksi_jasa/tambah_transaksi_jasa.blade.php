@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<input type="hidden" name="base_path" id="base_path" value="{{ asset('/') }}">
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <div class="pull-right">
                    <a href="{{ url('/transaksi_jasa') }}">
                        <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                    </a>
                </div>
                <a data-toggle="modal" data-target="#ubah_format_no_transaksi_jasa">
                    <button class="btn btn-warning flat" style="margin-right:10px"><span class="fa fa-edit"></span> Ubah No. Transaksi Jasa</button>
                </a>
            </div>
            <div class="box-body">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <form class="form-horizontal" method="POST" action="{{ url('/do_tambah_transaksi_jasa') }}" autocomplete="off">
                    <input type="hidden" name="pelanggan_id" id="pelanggan_id">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('no_nota') ? ' has-error' : '' }}">
                        <label class="control-label col-md-2">No. Transaksi Jasa</label>
                        <div class="col-md-10">
                            <input type="text" name="no_nota" class="form-control" value="{{ $no_nota }}" placeholder="Masukkan no service">
                            @if ($errors->has('no_nota'))
                                <span class="help-block">
                                    <strong>Kolom Nomor Transaksi Jasa wajib diisi</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('pelanggan_id') ? ' has-error' : '' }}">
                        <label class="control-label col-md-2">Nama</label>
                        <div class="col-md-6">
                            <input type="text" id="pelanggan_nama" class="form-control" disabled="true">
                            @if ($errors->has('pelanggan_id'))
                                <span class="help-block">
                                    <strong>Kolom Pelanggan wajib diisi</strong>
                                </span>
                            @endif
                        </div>
                        <div class="col-md-4">
                            <a data-toggle="modal" data-target="#pilih_pelanggan">
                                <button class="btn bg-maroon flat" style="margin-right:10px"><span class="fa fa-user"></span> Pilih Pelanggan</button>
                            </a>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('tanggal') ? ' has-error' : '' }}">
                        <label class="control-label col-md-2">Tanggal</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <?php $date = new DateTime(); 
                                $tanggal = $date->format('d').'-'.$date->format('m').'-'.$date->format('Y'); ?>
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="tanggal" data-date-format="dd-mm-yyyy" name="tanggal" value="{{ $tanggal }}" placeholder="Masukkan tanggal">
                            </div>
                            @if ($errors->has('tanggal'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('tanggal') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('deskripsi') ? ' has-error' : '' }}">
                        <label class="control-label col-md-2">Deskripsi</label>
                        <div class="col-md-8">
                            <textarea name="deskripsi" class="form-control">{{ old('deskripsi') }}</textarea>
                            @if ($errors->has('deskripsi'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('deskripsi') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('biaya') ? ' has-error' : '' }}">
                        <label class="control-label col-md-2">Biaya</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="biaya" placeholder="masukkan biaya transaksi jasa" id="biaya" data-thousands="." data-decimal="," style="text-align:right" value="{{ old('biaya') }}" />
                            @if ($errors->has('biaya'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('biaya') }}</strong>
                                </span>
                            @endif
                        </div>
                        <label class="control-label col-md-1" style="text-align: left;">(IDR)</label>
                    </div>
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-2">
                            <button class="btn btn-primary flat" onclick="document.getElementById('loading').style.display='block'"><span class="fa fa-floppy-o"></span> Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="overlay" id="loading" style="display:none">
              <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="pilih_pelanggan" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="riwayatheader">Pilih Pelanggan</h4>
      </div>
      <div class="modal-body">
          <div class="box-body">
            <table class="table table-striped table-bordered" id="mydatatables">
                <thead>
                    <th style="text-align:center">NO</th>
                    <th style="text-align:center">NAMA</th>
                    <th style="text-align:center">ALAMAT</th>
                    <th style="text-align:center">KOTA</th>
                    <th style="text-align:center"></th>
                </thead>
                <tbody>
                    <?php $i=1; ?>
                    @foreach($pelanggan as $val)
                    <tr>
                        <td align="center">{{ $i++ }}</td>
                        <td>{{ $val->nama }}</td>
                        <td>{{ $val->alamat }}</td>
                        <td>{{ $val->kota_nama }}</td>
                        <td align="center">
                            <button class="btn bg-maroon flat" type="button" onclick="pilih_pelanggan('{{ $val->id }}', '{{ $val->nama }}')" data-dismiss="modal"><span class="fa fa-check"></span> Pilih</button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
          </div><!-- /.box-body -->
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="ubah_format_no_transaksi_jasa" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content box">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="riwayatheader">Ubah Format No Transaksi Jasa</h4>
            </div>
            <form method="post" action="{{ url('/ubah_format_no_transaksi_jasa') }}" autocomplete="off" class="form-horizontal">
            {{ csrf_field() }}
            <div class="modal-body">
                <div class="row">
                    <label class="control-label col-md-2">Format</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="format" value="{{ $ori_no_nota }}" style="text-transform:uppercase;">
                    </div>
                </div>
                <br>
                <p>Untuk menambahkan template tanggal: gunakan tag 'DD' (hari), 'MM' (bulan), 'YY' (tahun), 'YYYY' (tahun)</p>
                <p>Contoh : DD-MM-YYYY</p>
                <p>Untuk menambahkan template angka: gunakan tag #</p>
                <p>Contoh template angka 4 digit : ####</p>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default pull-left flat"><span class="glyphicon glyphicon-ban-circle"></span> batal</button>
                <button type="submit" class="btn btn-primary pull-right flat" onclick="document.getElementById('loading9').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
            </div><!-- /.box-footer -->
            </form>
            <div class="overlay" id="loading9" style="display:none">
              <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

@stop

<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
<script src="{{ url('/js/jquery.maskMoney.js') }}" type="text/javascript"></script>
<script>
    $(function () {
        $("#mydatatables").DataTable();

        $('#tanggal').datepicker({
          autoclose: true,
          todayHighlight: 1,
        });

        $("#biaya").maskMoney({precision:0});
    });

    function pilih_pelanggan(id, nama)
    {
        document.getElementById("pelanggan_id").value   = id;
        document.getElementById("pelanggan_nama").value = nama;
    }
</script>