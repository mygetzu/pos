<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <div>
                  <a href="{{ url('/tambah_retur_beli') }}">
                  <button class='btn bg-purple flat'><span class='fa fa-plus'></span> Tambah Retur Beli</button>
                </a>
                </div>
            </div>
            <div class="box-body">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <table id="mydatatables" class="table table-striped table-bordered">
                  <thead>
                      <th style="text-align:center">NO</th>
                      <th style="text-align:center">TANGGAL</th>
                      <th style="text-align:center">NAMA SUPPLIER</th>
                      <th style="text-align:center">POTONGAN RETUR</th>
                      <th style="text-align:center">TOTAL RETUR</th>
                      <th></th>
                      <th></th>
                  </thead>
                  <tbody>
                    <?php $i=1; ?>
                    @foreach($rb_header as $val)
                      <tr>
                        <td align="center">{{ $i++ }}</td>
                        <td>{{ $val->tanggal }}</td>
                        <td>{{ $val->supplier->nama }}</td>
                        <td>{{ rupiah($val->potongan_retur) }}</td>
                        <td>{{ rupiah($val->total_retur) }}</td>
                        <td align="center">
                            <a href="{{ url('/nota_retur_beli/'.$val->id) }}" class="btn btn-info flat" data-toggle="tooltip" title="Nota Retur Beli" style="font-size: 15pt"><i class="fa fa-print"></i></a>
                        </td>
                        <td align="center"><a class="btn btn-info flat" href="{{ url('/detail_retur_beli/'.$val->id) }}" data-toggle="tooltip" title="detail" style="font-size: 15pt"><span class="fa fa-info-circle"></span></a></td>
                      </tr>
                    @endforeach
                  </tbody>
              </table>
            </div>
        </div>
    </div>
</div>
@stop
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script>
  $(function () {
    $("#mydatatables").DataTable();
  });
</script>