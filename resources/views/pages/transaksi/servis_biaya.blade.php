<style type="text/css">
    .table.table-borderless td, .table.table-borderless th {
        border: 0 !important;
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        @foreach($data['invoice_detail'] as $key => $value)
            $('#biaya_<?php echo ++$key ?>').maskMoney({
            prefix: 'Rp. ',
            thousands: '.',
            decimal: ',',
            precision: 0
        );
        @endforeach
    });
</script>
@extends('layouts.master_admin')
@section('content')
    @include('includes.base_function')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary flat">
                <div class="box-body">
                    <div class="col-md-12">
                        <h3>Data Perbaikan Produk</h3>
                    </div>
                    <div class="row" style="margin: 15px">
                        <div class="col-md-2">
                            <p>Nama Pelanggan</p>
                            <p>Model Produk</p>
                            <p>Serial number</p>
                            <p>Status Garansi</p>
                            <p>Keluhan Pelanggan</p>
                            <p>Deskripsi Produk</p>
                        </div>
                        <div class="col-md-4">
                            @foreach($data['service'] as $r_servis)
                                <p>{{ DB::table('tmst_pelanggan')->where('id', $r_servis->pelanggan_id)->value('nama') }}</p>
                                <p>{{ $r_servis->model_produk }}</p>
                                <p>{{ $r_servis->serial_number }}</p>
                                <p>{{ ($r_servis->status_garansi_produk == 1 ? 'Ya': 'Tidak') }}</p>
                                <p>{{ $r_servis->keluhan_pelanggan }}</p>
                                <p>{{ $r_servis->deskripsi_produk }}</p>
                            @endforeach
                        </div>
                        <div class="col-md-6">
                            <p>Catatan</p>
                            <p>
                                @if($data['catatan'] == NULL)
                                    <i>Tidak ada catatan</i>
                                @else
                                    {{ $data['catatan'] }}
                                @endif
                            </p>
                        </div>
                        <div class="row" style="margin-top: 25px">
                            <form action="{{ url('/servis_harga_submit') }}" method="POST" id="form-biaya">
                                <table id="" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th width="15px">No.</th>
                                        <th>Tindakan</th>
                                        <th class="col-md-2">Biaya (IDR)</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data['service'] as $a => $val)
                                        <input name="id_service" type="hidden" value="{{ $r_servis->id }}"/>
                                        @if($data['invoice_header_id'] == NULL)
                                            <tr>
                                                <td colspan="3">Tidak ada tindakan perbaikan</td>
                                            </tr>
                                        @else
                                            @foreach($data['invoice_detail'] as $key => $value)
                                                <tr>
                                                    <td>{{ ++$key }}</td>
                                                    <td>{{ $value->tindakan }}</td>
                                                    <td class="form-group" style="max-width: 10%">
                                                        <input name="id_invoice_detail_{{ $key }}" type="hidden"
                                                               value="{{ $value->id }}"/>
                                                        <input name="biaya_invoice_detail_{{ $key }}" type="text"
                                                               id="biaya_{{ $key }}"
                                                               class="form-control"
                                                               data-prefix="Rp. "
                                                               data-thousands="."
                                                               data-decimal=","
                                                               placeholder="Biaya Servis"
                                                               required/>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                                <div class="row">
                                    <div class="col-md-2 pull-right">
                                        <button class="btn btn-info btn-flat btn-block" onclick="">
                                            <b>Submit Biaya</b>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
    <script type="text/javascript">


        document.querySelector('#form-biaya').addEventListener('submit', function (e) {
                var form = this;
                e.preventDefault();
                swal({
                        title: "Anda yakin dengan biaya servis?",
                        text: "Setelah Anda men-submit, pelanggan akan menerima email notifikasi tagihan!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ya, saya yakin!",
                        cancelButtonText: "Batal",
                        closeOnConfirm: true,
                        html: false
                    },
                    function () {
                        form.submit();
                    }
                );
            }
        );

        function deleteCheck() {
            swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Hapus!",
                    cancelButtonText: "Batal",
                    closeOnConfirm: true,
                    html: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        swal({
                            title: 'Shortlisted!',
                            text: 'Candidates are successfully shortlisted!',
                            type: 'success'
                        }, function () {
                            form.submit();
                        });

                    } else {
                        swal("Cancelled", "Your imaginary file is safe :)", "error");
                    }
                }
            );
        }

        $(function () {
            $('#mydatatables').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": false,
                "info": true,
                "autoWidth": false
            });
        });
    </script>
@stop
