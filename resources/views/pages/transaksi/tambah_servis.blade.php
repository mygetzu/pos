@extends('layouts.master_admin')
@section('content')
    @include('includes.base_function')
    <input type="hidden" name="base_path" id="base_path" value="{{ asset('/') }}">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary flat">
                <div class="box-header with-border">
                    <div class="pull-right">
                        <a href="{{ url('/servis') }}">
                            <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                        </a>
                    </div>
                    <a data-toggle="modal" data-target="#tambah_pelanggan" class="hidden">
                        <button class="btn bg-purple flat" style="margin-right:10px"><span
                                    class="fa fa-plus-circle"></span> Tambah Pelanggan Baru
                        </button>
                    </a>
                    <a data-toggle="modal" data-target="#ubah_format_no_invoice_servis">
                        <button class="btn btn-warning flat" style="margin-right:10px"><span class="fa fa-edit"></span>
                            Ubah No. Service
                        </button>
                    </a>
                </div>
                <div class="box-body">
                    @if(Session::has('message'))
                        <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                            <i class="fa fa-check"></i>
                            {{ Session::get('message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    @if ($errors->has('format'))
                        <div class="alert alert-danger alert-dismissable flat" style="margin-left: 0px;">
                            <i class="fa fa-ban"></i>
                            {{ $errors->first('format') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    <div class="callout callout-info">
                        <p>
                            <i class="fa fa-info-circle"></i>
                            Jika perangkat yang akan diperbaiki dibeli dari Galerindo Teknologi, tekan tombol '<a
                                    data-toggle="modal" data-target="#pilih_transaksi_jual" style="cursor: pointer;">Temukan
                                dari Transaksi Penjualan</a>' kemudian pilih perangkat.
                        </p>
                    </div>
                    <form class="form-horizontal" method="POST" action="{{ url('/servis_checkout') }}"
                          autocomplete="off">
                        <h4>Informasi Pelanggan</h4>
                        <div class="row hidden" style="margin-bottom: 25px;">
                            <div class="col-md-6">
                                <div class="col-md-6 col-md-push-3">
                                    <a data-toggle="modal" data-target="#pilih_pelanggan">
                                        <button class="btn bg-maroon btn-flat btn-block" style="margin-right:10px"><span
                                                    class="fa fa-user"></span> Temukan Pelanggan
                                        </button>
                                    </a>
                                </div>
                            </div>
                        </div>
                        {{ csrf_field() }}
                        <input type="hidden" name="pelanggan_id" id="pelanggan_id">
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('pelanggan_nama') ? ' has-error' : '' }}">
                                <label class="control-label col-md-3">Nama</label>
                                <div class="col-md-7">
                                    <input type="text" name="pelanggan_nama" id="pelanggan_nama" class="form-control"
                                           placeholder="Masukkan Nama Pelanggan" required="true"
                                           value="{{ old('pelanggan_nama') }}"/>
                                </div>
                                <div class="col-md-2">
                                    <a data-toggle="modal" data-target="#pilih_pelanggan">
                                        <button class="btn bg-maroon btn-flat btn-block" style="margin-right:10px; font-size: 15pt;"><span
                                                    class="fa fa-user"></span>
                                        </button>
                                    </a>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('pelangan_email') ? ' has-error' : '' }}">
                                <label class="control-label col-md-3">Email</label>
                                <div class="col-md-9">
                                    <input type="text" name="pelanggan_email" id="pelanggan_email" class="form-control"
                                           placeholder="Masukkan Email Pelanggan" required="true"
                                           value="{{ old('pelangan_email') }}"/>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('pelanggan_ponsel') ? ' has-error' : '' }}">
                                <label class="control-label col-md-3">No. Ponsel</label>
                                <div class="col-md-9">
                                    <input type="text" name="pelanggan_ponsel" id="pelanggan_ponsel"
                                           class="form-control"
                                           placeholder="Masukkan Nomor pelanggan yang dapat dihubungi" required="true"
                                           value="{{ old('pelanggan_ponsel') }}"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('provinsi_pelanggan') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label">Provinsi</label>
                                <div class="col-md-9">
                                    <select class="form-control" id="provinsi_pelanggan" name="provinsi_pelanggan">
                                        <option value="">-- Pilih Provinsi --</option>
                                        @foreach($provinsi as $val)
                                            <option value="{{ $val->kode }}">{{ $val->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('kota_pelanggan') ? ' has-error' : '' }}">
                                <label class="control-label col-md-3">Kabupaten/Kota</label>
                                <div class="col-md-9">
                                    <select class="form-control" id="kota_pelanggan" name="kota_pelanggan">
                                        <option value="">-- Pilih Kabupaten/Kota --</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('pelanggan_alamat') ? ' has-error' : '' }}">
                                <label class="control-label col-md-3">Alamat</label>
                                <div class="col-md-9">
                                    <textarea class="form-control" name="pelanggan_alamat"
                                              placeholder="Masukkan alamat pelanggan" id="pelanggan_alamat"
                                              required="true">{{ old('pelanggan_alamat') }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <hr>
                            <h4>Informasi Unit</h4>
                            <div class="form-group{{ $errors->has('no_service') ? ' has-error' : '' }}">
                                <label class="control-label col-md-2">No.Servis</label>
                                <div class="col-md-8">
                                    <input type="text" name="no_service" class="form-control" value="{{ $no_invoice }}"
                                           placeholder="Masukkan no service" required="true"/>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('serial_number') ? ' has-error' : '' }}">
                                <label class="control-label col-md-2">Serial Number</label>
                                <div class="col-md-5">
                                    <input id="serial_number" type="text" name="serial_number" class="form-control"
                                           placeholder="Masukkan serial number" required="true"
                                           value="{{ old('serial_number') }}" autofocus="true"
                                           style="text-transform: uppercase"/>
                                </div>
                                <div class="col-md-2 hidden">
                                    <a data-toggle="modal" data-target="#pilih_pelanggan">
                                        <button class="btn bg-maroon btn-flat btn-block" style="margin-right:10px">
                                            <span class="fa fa-search-plus"></span> Temukan Pelanggan
                                        </button>
                                    </a>
                                </div>
                                <div class="col-md-3">
                                    <a data-toggle="modal" data-target="#pilih_transaksi_jual">
                                        <button class="btn bg-maroon btn-flat btn-block" style="margin-right:10px">
                                            <span class="fa fa-search-plus"></span>&nbsp;Temukan dari Transaksi
                                            Penjualan
                                        </button>
                                    </a>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('model_produk') ? ' has-error' : '' }}">
                                <label class="control-label col-md-2">Model</label>
                                <div class="col-md-8">
                                    <input id="model_perangkat" type="text" name="model_produk" class="form-control"
                                           placeholder="Masukkan model" required="true"
                                           value="{{ old('model_produk') }}"/>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('status_garansi') ? ' has-error' : '' }}">
                                <label class="control-label col-md-2">Garansi</label>
                                <div class="col-md-8">
                                    <select name="status_garansi" class="form-control {{ $errors->has('kelengkapan_wifi') || $errors->has('kelengkapan_modem') || $errors->has('kelengkapan_lan') || $errors->has('kelengkapan_bluetooth') ? ' has-error' : '' }}" required="true">
                                        <option value=""
                                                <?php (old('status_garansi') == '' ? '' : 'selected') ?> disabled>
                                            --Status Garansi--
                                        </option>
                                        <option value="0" <?php (old('status_garansi') == '0' ? 'selected' : '') ?>>
                                            Tidak
                                        </option>
                                        <option value="1" <?php (old('status_garansi') == '1' ? 'selected' : '') ?>>Ya
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <hr>
                            <h4>Kelengkapan Unit</h4>
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('kelengkapan_baterai') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Baterai</label>
                                    <div class="col-md-9">
                                        <input type="text" id="kelengkapan_baterai" name="kelengkapan_baterai"
                                               class="form-control" value="{{ old('kelengkapan_baterai') }}"
                                               placeholder="Masukkan model baterai"/>
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('kelengkapan_adapter') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Adapter Charger</label>
                                    <div class="col-md-9">
                                        <input type="text" id="kelengkapan_adapter" name="kelengkapan_adapter"
                                               class="form-control" value="{{ old('kelengkapan_adapter') }}"
                                               placeholder="Masukkan model baterai unit"/>
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('kelengkapan_memory') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Memory (RAM)</label>
                                    <div class="col-md-9">
                                        <input type="text" id="kelengkapan_memory" name="kelengkapan_memory"
                                               class="form-control" value="{{ old('kelengkapan_memory') }}"
                                               placeholder="Masukkan kapasitas & jenis memori unit"/>
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('kelengkapan_harddisk') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Harddisk (HDD)</label>
                                    <div class="col-md-9">
                                        <input type="text" id="kelengkapan_harddisk" name="kelengkapan_harddisk"
                                               class="form-control" value="{{ old('kelengkapan_harddisk') }}"
                                               placeholder="Masukkan kapasitas & model harddisk unit"/>
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('kelengkapan_other') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Lainnya</label>
                                    <div class="col-md-9">
                                        <input type="text" id="kelengkapan_other" name="kelengkapan_other"
                                               class="form-control" value="{{ old('kelengkapan_other') }}"
                                               placeholder="Masukkan kelengkapan unit yang belum disebutkan"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('kelengkapan_odd') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">ODD</label>
                                    <div class="col-md-9">
                                        <input type="text" id="kelengkapan_odd" name="kelengkapan_odd"
                                               class="form-control" value="{{ old('kelengkapan_odd') }}"
                                               placeholder="Masukkan model baterai" required="true"/>
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('kelengkapan_scrb') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">SC + RB</label>
                                    <div class="col-md-9">
                                        <input type="text" id="kelengkapan_scrb" name="kelengkapan_scrb"
                                               class="form-control" value="{{ old('kelengkapan_scrb') }}"
                                               placeholder="Masukkan SC + RB unit"/>
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('kelengkapan_defect') ? ' has-error' : '' }} hidden">
                                    <label class="control-label col-md-3">Defect</label>
                                    <div class="col-md-9">
                                        <textarea type="text"
                                                  id="kelengkapan_defect"
                                                  name="kelengkapan_defect" class="form-control"
                                                  placeholder="Masukkan defect unit"
                                                  rows="3">{{ old('kelengkapan_defect') }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('kelengkapan_wifi') || $errors->has('kelengkapan_modem') || $errors->has('kelengkapan_lan') || $errors->has('kelengkapan_bluetooth') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3">Lainnya</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <select class="form-control" id="kelengkapan_wifi" name="kelengkapan_wifi" required>
                                                    <option value="" {{ (old('kelengkapan_wifi') == NULL ? 'selected' : '') }} disabled>--Wifi--</option>
                                                    <option value="0" {{ (old('kelengkapan_wifi') == '0' ? 'selected' : '') }}>Tidak ada</option>
                                                    <option value="1" {{ (old('kelengkapan_wifi') == '1' ? 'selected' : '') }}>Ada</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <select class="form-control" id="kelengkapan_modem" name="kelengkapan_modem" required>
                                                    <option value="" {{ (old('kelengkapan_modem') == NULL ? 'selected' : '') }} disabled>--Modem--</option>
                                                    <option value="0" {{ (old('kelengkapan_modem') == '0' ? 'selected' : '') }}>Tidak ada</option>
                                                    <option value="1" {{ (old('kelengkapan_modem') == '1' ? 'selected' : '') }}>Ada</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <select class="form-control" id="kelengkapan_lan" name="kelengkapan_lan" required>
                                                    <option value="" {{ (old('kelengkapan_lan') == NULL ? 'selected' : '') }} disabled>--LAN--</option>
                                                    <option value="0" {{ (old('kelengkapan_lan') == '0' ? 'selected' : '') }}>Tidak ada</option>
                                                    <option value="1" {{ (old('kelengkapan_lan') == '1' ? 'selected' : '') }}>Ada</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <select class="form-control" id="kelengkapan_bluetooth" name="kelengkapan_bluetooth" required>
                                                    <option value="" {{ (old('kelengkapan_bluetooth') == NULL ? 'selected' : '') }} disabled>--Bluetooth--</option>
                                                    <option value="0" {{ (old('kelengkapan_bluetooth') == '0' ? 'selected' : '') }}>Tidak ada</option>
                                                    <option value="1" {{ (old('kelengkapan_bluetooth') == '1' ? 'selected' : '') }}>Ada</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <hr>
                            <h4>Keluhan Pelanggan</h4>
                            <div class="form-group{{ $errors->has('keluhan_pelanggan') ? ' has-error' : '' }}">
                                <label class="control-label col-md-2">Keluhan</label>
                                <div class="col-md-8">
                                    <textarea class="form-control col-md-11" name="keluhan_pelanggan"
                                              placeholder="Masukkan keluhan"
                                              rows="5">{{ old('keluhan_pelanggan') }}</textarea>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('deskripsi_produk') ? ' has-error' : '' }}">
                                <label class="control-label col-md-2">Deskripsi Produk</label>
                                <div class="col-md-8">
                                    <textarea class="form-control col-md-11" name="deskripsi_produk"
                                              placeholder="Masukkan deskripsi produk" required="true"
                                              rows="5">{{ old('deskripsi_produk') }}</textarea>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('kelengkapan_defect') ? ' has-error' : '' }}">
                                <label class="control-label col-md-2">Catatan</label>
                                <div class="col-md-8">
                                    <textarea type="text"
                                              id="catatan_pelanggan"
                                              name="catatan_pelanggan" class="form-control"
                                              placeholder="Masukkan catatan dari pelanggan"
                                              rows="3">{{ old('catatan_pelanggan') }}</textarea>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('tanggal_diterima') ? ' has-error' : '' }}">
                                <label class="control-label col-md-2">Tanggal Diterima</label>
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <?php $date = new DateTime();
                                        $tanggal = $date->format('d') . '-' . $date->format('m') . '-' . $date->format('Y');
                                        ?>
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right" id="tanggal"
                                               data-date-format="dd-M-yyyy" name="tanggal_diterima"
                                               value="{{ date('d-M-Y', strtotime($tanggal)) }}"
                                               placeholder="Masukkan tanggal" required="true"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-2">
                                    <button class="btn btn-primary flat"><span class="fa fa-floppy-o"></span> Simpan
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="overlay" id="loading" style="display:none">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="pilih_pelanggan" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="riwayatheader">Pilih Pelanggan</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <table class="table table-striped table-bordered" id="mydatatables">
                            <thead>
                            <th style="text-align:center">NO</th>
                            <th style="text-align:center">NAMA</th>
                            <th style="text-align:center">ALAMAT</th>
                            <th style="text-align:center">KOTA</th>
                            <th style="text-align:center"></th>
                            </thead>
                            <tbody>
                            <?php $i = 1; ?>
                            @foreach($pelanggan as $val)
                                <tr>
                                    <td align="center">{{ $i++ }}</td>
                                    <td>{{ $val->nama }}</td>
                                    <td>{{ $val->alamat }}</td>
                                    <td>{{ $val->kota->nama }}</td>
                                    <td align="center">
                                        <button class="btn bg-maroon flat" type="button"
                                                onclick="pilih_pelanggan('{{ $val->id }}', '{{ $val->nama }}', '{{ $val->email }}', '{{ $val->telp2 }}', '{{ DB::table('tref_kota')->where('id', $val->kota_id)->value('provinsi_kode') }}', '{{ $val->kota_id }}', '{{ $val->alamat }}')"
                                                data-dismiss="modal"><span class="fa fa-check"></span> Pilih
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="pilih_transaksi_jual" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="riwayatheader">Pilih Transaksi Penjualan</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <table class="table table-striped table-bordered" id="tabel_pilih_jual">
                            <thead>
                            <th style="text-align:center">NO</th>
                            <th style="text-align:center">NAMA PELANGGAN</th>
                            <th style="text-align:center">EMAIL PELANGGAN</th>
                            <th style="text-align:center">SERIAL NUMBER</th>
                            <th style="text-align:center">MODEL PRODUK</th>
                            <th style="text-align:center; width: 10%"></th>
                            </thead>
                            <tbody>
                            <?php $i = 1; ?>
                            @if($data['get_sj_keluar_detail'] == NULL)
                            @else
                                @foreach($data['get_sj_keluar_detail'] as $key => $value)
                                    <tr>
                                        <td align="center">{{ $i++ }}</td>
                                        <td>{{ $data[$key]['pelanggan_nama'] }}</td>
                                        <td>{{ $data[$key]['pelanggan_email'] }}</td>
                                        <td>{{ $data[$key]['serial_number'] }}</td>
                                        <td>{{ $data[$key]['produk_nama'] }}</td>
                                        <td align="center">
                                            <button class="btn bg-maroon flat" type="button"
                                                    onclick="pilih_transaksi_jual('{{ $data[$key]['serial_number'] }}', '{{ $data[$key]['produk_nama'] }}', '{{ $data[$key]['pelanggan_id'] }}', '{{ $data[$key]['pelanggan_nama'] }}', '{{ $data[$key]['pelanggan_email'] }}', '{{ ($data[$key]['pelanggan_telp1'] == NULL ? $data[$key]['pelanggan_telp2'] : '') }}', '{{ $data[$key]['pelanggan_kota_id'] }}', '{{ $data[$key]['pelanggan_provinsi_id'] }}', '{{ $data[$key]['pelanggan_alamat'] }}')"
                                                    data-dismiss="modal"><span class="fa fa-check"></span> Pilih
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="tambah_pelanggan" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content box">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="riwayatheader">Tambah Pelanggan</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" method="post" action="{{ url('/tambah_pelanggan_baru_servis') }}"
                          autocomplete="off">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Nama</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="nama" name="nama">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="email" name="email">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Alamat</label>
                            <div class="col-sm-10">
                                <textarea type="text" class="form-control" id="alamat" name="alamat"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Kabupaten/Kota</label>
                            <div class="col-md-5">
                                <select class="form-control" id="provinsi_pelanggan" name="provinsi_pelanggan">
                                    <option value="">-- Pilih Provinsi --</option>
                                    @foreach($provinsi as $val)
                                        <option value="{{ $val->kode }}">{{ $val->nama }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-5">
                                <select class="form-control" id="kota_pelanggan" name="kota_pelanggan">
                                    <option value="">-- Pilih Kabupaten/Kota --</option>
                                </select>
                            </div>
                        </div><!-- /.box-body -->
                        <div class="form-group">
                            <label class="col-sm-2 control-label">No Handphone</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" data-inputmask='"mask": "(+62)999-9999-9999"'
                                       data-mask2 placeholder="contoh: (+62)857-9999-9999" name="hp"
                                       value="{{ old('hp') }}">
                            </div>
                        </div>
                        <div class="box-footer">
                            <button data-dismiss="modal" class="btn btn-default flat"><span
                                        class="glyphicon glyphicon-ban-circle"></span> Batal
                            </button>
                            <button type="submit" class="btn btn-primary pull-right flat"
                                    onclick="document.getElementById('loading3').style.display = 'block'"><span
                                        class="glyphicon glyphicon-floppy-disk"></span> Simpan
                            </button>
                        </div><!-- /.box-footer -->
                    </form>
                </div>
                <div class="overlay" id="loading3" style="display:none">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="ubah_format_no_invoice_servis" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content box">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="riwayatheader">Ubah Format No Invoice PO</h4>
                </div>
                <form method="post" action="{{ url('/ubah_format_no_invoice_servis') }}" autocomplete="off"
                      class="form-horizontal">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div class="row">
                            <label class="control-label col-md-2">Format</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="format" value="{{ $ori_format_invoice }}"
                                       style="text-transform:uppercase;">
                            </div>
                        </div>
                        <br>
                        <p>Untuk menambahkan template tanggal: gunakan tag 'dd' (hari), 'mm' (bulan) 'yyyy' (tahun)</p>
                        <p>Contoh : dd-mm-yyyy</p>
                        <p>Untuk menambahkan template angka: gunakan tag #</p>
                        <p>Contoh template angka 4 digit : ####</p>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default pull-left flat"><span
                                    class="glyphicon glyphicon-ban-circle"></span> batal
                        </button>
                        <button type="submit" class="btn btn-primary pull-right flat"
                                onclick="document.getElementById('loading9').style.display = 'block'"><span
                                    class="glyphicon glyphicon-floppy-disk"></span> Simpan
                        </button>
                    </div><!-- /.box-footer -->
                </form>
                <div class="overlay" id="loading9" style="display:none">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>
            </div>
        </div>
    </div>

@stop

<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
<script>
    $(function () {
        $("#mydatatables").DataTable();
        $("#mydatatables2").DataTable();
        $('#tabel_pilih_jual').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": false,
            "info": true,
            "autoWidth": false
        });
        $('#tanggal').datepicker({
            autoclose: true,
            todayHighlight: 1,
        });
        $('#tanggal_selesai').datepicker({
            autoclose: true,
            todayHighlight: 1,
        });
        $("[data-mask2]").inputmask();

        $('#provinsi_pelanggan').change(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            var provinsi_kode = document.getElementById('provinsi_pelanggan').value;
            if (provinsi_kode == "") {
                provinsi_kode = "ALL";
            }
            var path = document.getElementById('base_path').value;
            var url = path + "get_kota/" + provinsi_kode;
            $.ajax({
                type: "POST",
                url: url,
                success: function (data) {
                    $("#kota_pelanggan").html(data);
                },
                error: function (data) {
                    swal('Oops, something wrong.');
                }
            });
        });
    });

    $('#status_kelengkapan_baterai').change(function () {
        if ($(this).find('option:selected').value() != '1') {
            $('#kelengkapan_baterai').attr('disabled', 'disabled');
        } else {
            $('#kelengkapan_baterai').attr('disabled', '')
        }
    });


    function pilih_pelanggan(id, nama, email, ponsel, provinsi, kota, alamat) {
        document.getElementById("pelanggan_id").value = id;
        document.getElementById("pelanggan_nama").value = nama;
        document.getElementById("pelanggan_email").value = email;
        document.getElementById("pelanggan_alamat").value = alamat;
        document.getElementById("pelanggan_ponsel").value = ponsel;
        document.getElementById("provinsi_pelanggan").value = provinsi_pelanggan;
        document.getElementById("kota_pelanggan").value = kota;
    }

    function pilih_transaksi_jual(serial_number, model_perangkat, id, nama, email, ponsel, provinsi, kota, alamat) {
        document.getElementById("serial_number").value = serial_number;
        document.getElementById("model_perangkat").value = model_perangkat;
        document.getElementById("pelanggan_id").value = id;
        document.getElementById("pelanggan_nama").value = nama;
        document.getElementById("pelanggan_email").value = email;
        document.getElementById("pelanggan_alamat").value = alamat;
        document.getElementById("pelanggan_ponsel").value = ponsel;
        document.getElementById("provinsi_pelanggan").selectedIndex = provinsi;
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })

        var provinsi_kode = provinsi;
        if (provinsi_kode == "") {
            provinsi_kode = "ALL";
        }
        var path = document.getElementById('base_path').value;
        var url = path + "get_kota/" + provinsi_kode;
        $.ajax({
            type: "POST",
            url: url,
            success: function (data) {
                $("#kota_pelanggan").html(data);
                document.getElementById("kota_pelanggan").value = kota;
            },
            error: function (data) {
                swal('Oops, something wrong.');
            }
        });
    }

    $(document).ready(function () {
        $('#metode_pembayaran').change(function () {
            if (document.getElementById("metode_pembayaran").value == 2) {
                document.getElementById("label_bank").style.display = 'block';
                document.getElementById("titik_bank").style.display = 'block';
                document.getElementById("input_bank").style.display = 'block';
            }
            else {
                document.getElementById("label_bank").style.display = 'none';
                document.getElementById("titik_bank").style.display = 'none';
                document.getElementById("input_bank").style.display = 'none';
            }
        });
    });
</script>