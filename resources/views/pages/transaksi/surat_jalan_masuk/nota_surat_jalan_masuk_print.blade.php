<link rel="stylesheet" type="text/css" href="{{ URL::to('/css/nota.css') }}">
@include('includes.base_function')
<style type="text/css">
    [class*="col-"] {
        float: left;
    }

    pre {
        font-family: sans-serif;
        background-color: transparent;
        border: none;
        overflow-x: hidden;
        white-space: pre-wrap;       /* css-3 */
        white-space: -moz-pre-wrap;  /* Mozilla, since 1999 */
        white-space: -pre-wrap;      /* Opera 4-6 */
        white-space: -o-pre-wrap;    /* Opera 7 */
        word-wrap: break-word; 
        padding: 0px;
    }

    @font-face {
        font-family: mycalibri;
        src: url("{{ asset('fonts/Calibri.ttf') }}");
    }

    @font-face {
        font-family: mycalibri;
        src: url("{{ asset('fonts/Calibri-Bold.ttf') }}");
        font-weight: bold;
    }

    .area{
        font-family: mycalibri;
    }
</style>
<div class="area">
    <section id="header">
        <div>
            <div class="col-4">
                <div class="title"><span style="border-bottom: 1px solid black;">{{ strtoupper($judul) }}</span></div>
                <div class="company" style="margin-top: 20px;">
                    {{ strtoupper($perusahaan->nama) }} <br>
                    {{ $perusahaan->alamat }} {{ $kota_perusahaan }} <br>
                    Telp. {{ $perusahaan->telp }} | {{ $perusahaan->telp2 }} <br>
                    E. {{ $perusahaan->email }}
                </div>
            </div>
            <div class="col-4">
                <table class="detail" style="margin-bottom: 10px;">
                    <tr>
                        <td class="title-detail" width="34%">No. Surat Jalan</td>
                        <td class="titik" style="vertical-align: top;">&nbsp;:&nbsp;</td>
                        <td class="data-detail">{{ $sj_masuk_header->no_nota }}</td>
                    </tr>
                    <tr>
                        <td class="title-detail">Tanggal</td>
                        <td class="titik">&nbsp;:&nbsp;</td>
                        <td class="data-detail">
                            <?php $tanggal = new DateTime($sj_masuk_header->tanggal); 
                            echo $tanggal->format('d/m/Y G:i:s');
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="title-detail">Supplier</td>
                        <td class="titik">&nbsp;:&nbsp;</td>
                        <td class="data-detail user">{{ $supplier['nama'] }}</td>
                    </tr>
                    <tr>
                        <td class="title-detail" style="vertical-align: top;">Alamat </td>
                        <td class="titik" style="vertical-align: top;">&nbsp;:&nbsp;</td>
                        <td class="data-detail user">{{ $supplier['alamat'] }}<br>
                        Telp. {{ $supplier['telp'] }}<br>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-4">
                <div class="detail">
                    <b>Barang dikirim ke: </b>
                    <pre>{{ $po_alamat_pengiriman->alamat }}</pre>
                </div>
            </div>
        </div>
    </section>
    <section id="table">
        <div>
            <table class="table">
                <thead>
                    <tr>
                        <th width="5%">No.</th>
                        <th>Nama Produk</th>
                        <th>Jumlah</th>
                        <th width="40%">Serial Number</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $jumlah_item=0; $total_akhir=0; ?>
                    @foreach($sj_masuk_detail as $key => $val)
                    <tr>
                        <td style="text-align: center;">{{ $key+1 }}</td>
                        <td>
                            @if($val->jenis_barang_id == 1)
                            {{ $val->produk->nama }}
                            @elseif($val->jenis_barang_id == 2)
                            {{ $val->hadiah->nama }}
                            @endif
                        </td>
                        <td style="text-align: center;">{{ $val->jumlah_total }}<?php $jumlah_item=$jumlah_item+$val->jumlah_total ?></td>
                        <td style="text-align: center;">
                            <?php
                                $indeks = 1;
                                foreach($produk_sn[$key] as $val2)
                                {
                                    if($indeks == $val->jumlah_total){
                                        echo "$val2->serial_number";
                                    }
                                    elseif(!empty($val2->serial_number)){
                                        echo "$val2->serial_number, ";
                                    }

                                    $indeks++;
                                }
                            ?>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </section>
    <section id="footer">
        <div>
            <div class="col-4">
                <table>
                    <tr>
                        <td class="title-detail" style="vertical-align: top;">Keterangan</td>
                        <td class="titik" style="vertical-align: top;">&nbsp;:&nbsp;</td>
                        <td class="data-detail">
                        {{ $po_header->catatan }}
                        <br><br></td>
                    </tr>
                </table>
                <p style="position: absolute;bottom: 0;"><?php echo $print_time->format('d/m/Y G:i:s'); ?></p>
            </div>
            <div class="col-3">
                <table width="100%">
                    <tr style="text-align: center;">
                        <td class="title-detail keterangan" style="width: 28%">Jml Item</td>
                        <td class="titik keterangan" style="vertical-align: top; width: 5%">&nbsp;:&nbsp;</td>
                        <td class="data-detail keterangan" style="width: 12%">{{ $jumlah_item }}</td>
                    </tr>
                </table>
            </div>
            <div class="col-5" style="padding: 0;">
                <div class="col-6">
                    <div>
                        <div style="text-align: center">
                            <strong>Penerima</strong>
                        </div>
                    </div>
                    <div style="margin-top: 100px;">
                        <div style="text-align: center">
                            <strong>(.........................)</strong>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div>
                        <div style="text-align: center">
                            <strong>Hormat Kami</strong>
                        </div>
                    </div>
                    <div style="margin-top: 100px;">
                        <div style="text-align: center">
                            <strong>(.........................)</strong>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function(){
          window.onload = function() { window.print(); }
      });
</script>