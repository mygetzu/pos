<style type="text/css">
    .table.table-borderless td, .table.table-borderless th {
        border: 0 !important;
    }
</style>
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<input type="hidden" name="base_path" id="base_path" value="{{ asset('/') }}">
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <h3 class="box-title">Supplier</h3>
                <a data-toggle='modal' data-target='#ubah_format_no_invoice' >
                <button class="btn btn-warning flat pull-right" type="button" style="margin-right: 20px;"><span class="fa fa-edit"></span> Ubah Format No. Retur Jual</button>
                </a>
            </div>
            <div class="box-body">
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/retur_beli_checkout') }}" id="form_retur_beli" autocomplete="off">
                    <input type="hidden" class="form-control" name="no_retur" value="{{ $no_retur_beli }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="nota_beli_id" id="nota_beli_id" class="form-control">
                    <input type="hidden" name="supplier_id" id="supplier_id">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-2">No. Retur</label>
                            <div class="col-md-10">
                                <input type="text" name="no_retur" class="form-control" placeholder="Masukkan nomor retur jual" value="{{ $no_retur_beli }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Nama</label>
                            <div class="col-md-6">
                                <input type="text" name="supplier_nama" id="supplier_nama" class="form-control" disabled="true">
                            </div>
                            <div class="col-md-4">
                                <a data-toggle="modal" data-target="#pilih_supplier">
                                    <button class="btn bg-maroon flat"><span class="fa fa-user"></span> Pilih Supplier</button>
                                </a>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Nota</label>
                            <div class="col-md-6" id="div_no_nota" style="display:none;">
                                <input type="text" name="no_nota" id="no_nota" class="form-control" disabled="true">
                            </div>
                            <div class="col-md-4">
                                <a data-toggle="modal" data-target="#pilih_no_nota">
                                    <button class="btn bg-maroon flat" type="button" id="button_pilih_nota" onclick="btn_pilih_nota_klik()" disabled="true"><span class="fa fa-file"></span> Pilih No Nota</button>
                                </a>
                            </div>
                            @if ($errors->has('no_nota'))
                                <span class="help-block">
                                    <strong>Pilih No Nota Terlebih Dahulu</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">                        
                        <div class="form-group">
                            <label class="control-label col-md-2">Potongan Retur</label>
                            <div class="col-md-7">
                                <input type="text" style="text-align: right;" class="form-control" name="potongan_retur" placeholder="Masukkan potongan retur" id="potongan_retur" data-thousands="." data-decimal="," value="{{ old('potongan_retur') }}" />
                                <script type="text/javascript">$("#potongan_retur").maskMoney({precision:0});</script>
                            </div>
                            <label class="control-label col-md-2">(IDR)</label>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Catatan</label>
                            <div class="col-md-10">
                                <textarea name="catatan" class="form-control" placeholder="Masukkan Catatan" style="text-transform:capitalize">{{ old('catatan') }}</textarea>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="col-md-12">
                    <hr>
                    <h4>Retur
                    <a data-toggle="modal" data-target="#pilih_produk" >
                        <button class="btn bg-maroon pull-right flat" onclick="pilih_produk()" style="display:none" id="button_pilih_produk"><span class="fa fa-archive"></span> Pilih Produk</button>
                    </a></h4>
                    <hr>
                    <div id="admin_retur">
                            
                    </div>
                </div>
                <button class="btn btn-success flat pull-right" type="submit" style="display:none" id="checkout" onclick="btn_proses_klik()"><span class="fa fa-arrow-circle-right"></span> Proses</button>
            </div>
            <div class="overlay" id="loading" style="display:none">
              <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="pilih_supplier" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="riwayatheader">Pilih Supplier</h4>
      </div>
      <div class="modal-body">
          <div class="box-body">
            <table class="table table-striped table-bordered" id="mydatatables">
                <thead>
                    <th style="text-align:center">NO</th>
                    <th style="text-align:center">NAMA</th>
                    <th style="text-align:center">ALAMAT</th>
                    <th style="text-align:center">KOTA</th>
                    <th style="text-align:center"></th>
                </thead>
                <tbody>
                    <?php $i=1; ?>
                    @foreach($supplier as $val)
                    <tr>
                        <td align="center">{{ $i++ }}</td>
                        <td>{{ $val->nama }}</td>
                        <td>{{ $val->alamat }}</td>
                        <td>{{ $val->kota_nama }}</td>
                        <td align="center">
                            <button class="btn bg-maroon flat" type="button" onclick="pilih_supplier('{{ $val->id }}','{{ $val->nama }}')" data-dismiss="modal"><span class="fa fa-check"></span> Pilih</button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
          </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="pilih_no_nota" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="riwayatheader">Pilih No Nota</h4>
      </div>
      <div class="modal-body">
          <div class="box-body">
            <div id="content_nota">
            </div>
          </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="pilih_produk" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="riwayatheader">Pilih Produk/Paket</h4>
      </div>
      <div class="modal-body">
          <div class="box-body">
            <div id="content_produk">
            </div>
          </div><!-- /.box-body -->
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="ubah_format_no_invoice" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content box">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="riwayatheader">Ubah Format No. Retur Beli</h4>
            </div>
            <form method="post" action="{{ url('/ubah_format_no_invoice_rb') }}" autocomplete="off">
            {{ csrf_field() }}
            <div class="modal-body">
                <div class="form-group">
                    <label class="control-label col-md-2">Format</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" name="format" value="{{ $ori_format_invoice }}" style="text-transform:uppercase;">
                    </div>
                </div>
                <br>
                <p>Untuk menambahkan template tanggal: gunakan tag 'dd' (hari), 'mm' (bulan) 'yyyy' (tahun)</p>
                <p>Contoh : dd-mm-yyyy</p>
                <p>Untuk menambahkan template angka: gunakan tag #</p>
                <p>Contoh template angka 4 digit : ####</p>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default pull-left flat"><span class="glyphicon glyphicon-ban-circle"></span> batal</button>
                <button type="submit" class="btn btn-primary pull-right flat" onclick="document.getElementById('loading9').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
            </div><!-- /.box-footer -->
            </form>
            <div class="overlay" id="loading9" style="display:none">
              <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

@stop

<script src="{{ asset('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ url('js/jquery.maskMoney.js') }}" type="text/javascript"></script>
<script>
    $(function () {
        $("#mydatatables").DataTable();
    });

    function pilih_supplier(supplier_id,supplier_nama)
    {
        document.getElementById("supplier_id").value            = supplier_id;
        document.getElementById("supplier_nama").value          = supplier_nama;
        document.getElementById('button_pilih_nota').disabled   = false;
        document.getElementById('div_no_nota').style.display            = 'none';
        document.getElementById('no_nota').value                        = "";
        document.getElementById('nota_beli_id').value                       = "";
        document.getElementById('button_pilih_produk').style.display    = 'none';

        document.getElementById("admin_retur").innerHTML    = "";
        document.getElementById("checkout").style.display   = 'none';

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })

        var url = "{{ url('/get_nota_supplier') }}";
        
        $.ajax({
            type: "POST",
            url: url,
            data: { supplier_id: supplier_id },
            success: function (data) {
                $("#content_nota").html(data);
            },
            error: function (data) {
                alert('ooo');
            }
        });
    }

    function btn_pilih_nota(nota_beli_id, no_invoice)
    {
        document.getElementById('div_no_nota').style.display            = 'block';
        document.getElementById('nota_beli_id').value                   = nota_beli_id;
        document.getElementById('no_nota').value                        = no_invoice;
        document.getElementById('button_pilih_produk').style.display    = 'block';

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })

        var url = "{{ url('/get_po_detail') }}";

        $.ajax({
            type: "POST",
            url: url,
            data: { nota_beli_id: nota_beli_id },
            success: function (data) {
                $("#content_produk").html(data);
            },
            error: function (data) {
                alert('ooo');
            }
        });
    }

    function btn_pilih_produk(produk_id,serial_number,harga,jenis_barang_id, gudang_id)
    {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })

        var path =  document.getElementById('base_path').value;
        var url = "{{ url('/retur_beli_tambah_cart') }}";
        var nota_beli_id = document.getElementById('nota_beli_id').value;

        $.ajax({
            type: "POST",
            url: url,
            data: { nota_beli_id:nota_beli_id, produk_id:produk_id, serial_number:serial_number, harga:harga, jenis_barang_id:jenis_barang_id, gudang_id:gudang_id },
            beforeSend: function(){
                        document.getElementById('loading').style.display = "block";
                      },
            success: function (data) {
                $("#admin_retur").html(data);
                document.getElementById("checkout").style.display = 'block';
                document.getElementById('loading').style.display = "none";
            },
            error: function (data) {
                alert('terjadi kesalahan');
            }
        });
    }

    function pilih_produk()
    {
        no_nota = document.getElementById('no_nota').value;

        
    }

    function jml_klik(rowid,qty)
    {
        var jumlah = prompt("Jumlah", qty);
        if(jumlah != null)
        {
            //update cart
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });

            no_nota = document.getElementById('no_nota').value;
            var url = "{{ url('/retur_cart_jumlah') }}";
            
            $.ajax({
                type: "POST",
                url: url,
                data : { rowid:rowid, jumlah:jumlah, no_nota:no_nota },
                beforeSend: function(){
                            document.getElementById('loading').style.display = "block";
                          },
                success: function (data) {
                    $("#admin_retur").html(data);
                    document.getElementById('loading').style.display = "none";
                },
                error: function (data) {
                    alert('gagal menambahkan produk promo ke keranjang belanja');
                }
            });
        }
  }
  function hapus(rowId)
    {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })
        
        var url = "{{ url('/retur_cart_hapus') }}";
        
        $.ajax({
            type: "POST",
            url: url,
            data : { rowId:rowId },
            beforeSend: function(){
                        document.getElementById('loading').style.display = "block";
                      },
            success: function (data) {
                $("#admin_retur").html(data);
                document.getElementById('loading').style.display = "none";
            },
            error: function (data) {
                alert('ooo');
            }
        });
    }

    $(document).ready(function(){
          $('#kategori_produk_id').change(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            var url = "{{ url('/get_produk_table') }}";

              $.ajax({
                type: "POST",
                url: url,
                data: $("#form_produk").serialize(),
                success: function (data) {
                    $("#content_produk").html(data);
                },
                error: function (data) {
                    alert('ooo');
                }
            });
          });

          // $('#potongan_retur').inputmask('999 999 999 999', { numericInput: true, placeholder: '' });
          // $('#total_retur').inputmask('999 999 999 999', { numericInput: true, placeholder: '' });
    });

    function btn_pilih_nota_klik()
    {
        var supplier_id = document.getElementById('supplier_id').value;

        //get transaksi

        
    }
    function btn_proses_klik()
      {
        document.getElementById('loading').style.display='block'
        document.getElementById('form_retur_beli').submit();
      }
</script>