<link rel="stylesheet" type="text/css" href="{{ URL::to('/css/nota.css') }}">
@include('includes.base_function')
<div class="area">
    <section id="header">
        <div class="row">
            <div class="col-4">
                <div class="title">{{ $judul }}</div>
                <div class="company">
                    {{ $perusahaan->nama }} <br>
                    {{ $perusahaan->alamat }} <br>
                    T. {{ $perusahaan->telp }} <br>
                    E. {{ $perusahaan->email }}
                </div>
            </div>
            <div class="col-5">
                <table class="detail">
                    <tr>
                        <td class="title-detail">No. Invoice</td>
                        <td class="titik">&nbsp;:&nbsp;</td>
                        <td class="data-detail">{{ $so_header->no_sales_order }}</td>
                    </tr>
                    <tr>
                        <td class="title-detail">Tanggal</td>
                        <td class="titik">&nbsp;:&nbsp;</td>
                        <td class="data-detail">
                            <?php $tanggal = new DateTime($so_header->tanggal); 
                            echo $tanggal->format('d/m/Y G:i:s');
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="title-detail">Pelanggan</td>
                        <td class="titik">&nbsp;:&nbsp;</td>
                        <td class="data-detail user">{{ $so_alamat_pengiriman->nama }}</td>
                    </tr>
                    <tr>
                        <td class="title-detail" style="vertical-align: top;">Alamat </td>
                        <td class="titik" style="vertical-align: top;">&nbsp;:&nbsp;</td>
                        <td class="data-detail user">{{ $so_alamat_pengiriman->nama }}<br>
                        Email. {{ $so_alamat_pengiriman->email }}<br>
                        HP. {{ $so_alamat_pengiriman->hp }}<br>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-2">
            </div>
        </div>
    </section>
    <section id="table">
        <div class="row">
            <table class="table">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Nama Produk</th>
                        <th>Satuan</th>
                        <th>Harga</th>
                        <th>Jumlah</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $jumlah_item=0; $total_akhir=0; ?>
                    @foreach($so_detail as $i => $val)
                    <tr>
                        <td align="center">{{ $i+1 }}</td>
                        <td>
                            @if(!empty($val->produk))
                            {{ $val->produk->nama }}
                            @elseif(!empty($val->hadiah))
                            {{ $val->hadiah->nama }}
                            @endif
                        </td>
                        <td align="center">
                            @if(!empty($val->produk))
                            {{ $val->produk->satuan }}
                            @elseif(!empty($val->hadiah))
                            {{ $val->hadiah->satuan }}
                            @endif
                        </td>
                        <td class="harga">{{ rupiah($val->harga) }}</td>
                        <td align="center">{{ $val->jumlah }}</td>
                        <td class="harga">{{ rupiah($val->jumlah*$val->harga) }}</td>
                    </tr>
                    <?php $jumlah_item=$jumlah_item+$val->jumlah; 
                        $total_akhir = $total_akhir+ ($val->jumlah*$val->harga);
                    ?>
                    @endforeach
                </tbody>
            </table>
        </div>
    </section>
    <section id="footer">
        <div class="row">
            <div class="col-4">
                <table>
                    <tr>
                        <td class="title-detail" style="vertical-align: top;">Keterangan</td>
                        <td class="titik" style="vertical-align: top;">&nbsp;:&nbsp;</td>
                        <td class="data-detail">
                        {{ $so_header->catatan }}
                        <br><br></td>
                    </tr>
                    <tr>
                        <td class="title-detail" style="vertical-align: top;">Terbilang</td>
                        <td class="titik" style="vertical-align: top;">&nbsp;:&nbsp;</td>
                        <td class="data-detail">{{ dibaca($so_header->total_tagihan) }} Rupiah</td>
                    </tr>
                </table>
            </div>
            <div class="col-3">
                <div class="jml">
                    <table>
                        <tr>
                            <td class="title-detail price">Jml Item</td>
                            <td class="titik price">&nbsp;:&nbsp;</td>
                            <td class="data-detail right price">{{ $jumlah_item }}</td>
                        </tr>
                    </table>
                </div>
                <div style="margin-top: 20px;">
                    <div class="col-offset-6" style="text-align: center">
                        <strong>Hormat Kami</strong>
                    </div>
                </div>
                <div style="margin-top: 100px;">
                    <div class="col-offset-6" style="text-align: center">
                        <strong>(.........................)</strong>
                    </div>
                </div>
            </div>
            <div class="col-5 right">
                    <table width="100%">
                        <tr>
                            <td class="title-detail price" width="160px;">Total Akhir</td>
                            <td class="titik price" width="100px;">&nbsp;:&nbsp;</td>
                            <td class="data-detail right price">{{ rupiah($total_akhir) }}</td>
                        </tr>
                        @if(!empty($so_voucher))
                        @foreach($so_voucher as $val)
                        <tr>
                            <td class="title-detail price" width="160px;">Voucher</td>
                            <td class="titik" width="100px;">&nbsp;:&nbsp;</td>
                            <td class="data-detail right price">{{ $$val->nominal }}</td>
                        </tr>
                        @endforeach
                        @endif
                    </table>
                <div class="ket">Harga sudah termasuk PPN 10%</div>
            </div>
        </div>
    </section>
    <br>
    <div class="col-12">
        <?php echo $print_time->format('d/m/Y G:i:s'); ?>
    </div>
</div>
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function(){
          window.onload = function() { window.print(); }
      });
</script>