@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                @if(!empty($invoice_detail_sj))
                <a href="{{ url('/nota_surat_jalan/'.$invoice_detail_sj->no_nota) }}" target="_blank" class="btn btn-info flat"><i class="fa fa-file-text"></i> Nota Surat Jalan</a>
                @endif
                <a href="{{ url('/purchase_order') }}" class="pull-right" style="padding:0px">
                    <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                </a>
            </div>
            <div class="box-body">
                <?php $indeks = 0; $indeks2 = 0; ?>
                @foreach($gudang as $val)
                    <h3>{{ $val->gudang->nama }}</h3>
                    <hr>
                    @foreach($produk[$indeks] as $val2)
                        @if($val2->jenis_barang_id === '1')
                        <h4>{{ $val2->produk->nama }}</h4>
                        @elseif($val2->jenis_barang_id === '2')
                        <h4>{{ $val2->hadiah->nama }}</h4>
                        @endif
                        <table class="table table-striped table-bordered" id="ajax_produk_tabless3">
                            <thead>
                                <tr>
                                    <th style="text-align:center">NO</th>
                                    <th style="text-align:center">SERIAL NUMBER</th>
                                    <th style="text-align:center">JUMLAH</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=1; ?>
                                @foreach($serial_number[$indeks2] as $val3)
                                <tr>
                                    <td align="center">{{ $i++ }}</td>
                                    <td>{{ $val3->serial_number }}</td>
                                    <td align="center">{{ $val3->quantity }}</td>
                                </tr>
                                @endforeach
                                <?php $indeks2++; ?>
                            </tbody>
                        </table>    
                    @endforeach
                    <?php $indeks++; ?>
                @endforeach
            </div>
            <div class="overlay" id="loading" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
  $(function () {
    $(".table-bordered").DataTable();
  });
</script>
@stop