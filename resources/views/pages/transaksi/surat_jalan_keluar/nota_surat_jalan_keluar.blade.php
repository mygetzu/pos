@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<style type="text/css">
    pre {
        font-family: sans-serif;
        background-color: transparent;
        border: none;
        overflow-x: hidden;
        white-space: pre-wrap;       /* css-3 */
 white-space: -moz-pre-wrap;  /* Mozilla, since 1999 */
 white-space: -pre-wrap;      /* Opera 4-6 */
 white-space: -o-pre-wrap;    /* Opera 7 */
 word-wrap: break-word; 
 padding: 0px;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <a href="{{ url('/surat_jalan_keluar') }}" class="pull-right" style="padding:0px">
                    <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                </a>
                <a href="{{ url('/nota_surat_jalan_keluar_print/'.$sj_keluar_header->id) }}" target="_blank" class="btn bg-orange flat"><i class="fa fa-print"></i> Cetak</a>
                <a href="{{ url('/nota_surat_jalan_keluar_pdf/'.$sj_keluar_header->id) }}" target="_blank" class="btn btn-info flat" style="margin-right: 5px;"><i class="fa fa-download"></i> Unduh PDF</a>
            </div>
            <div class="box-body">
                <div class="area">
                    <section id="header">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="title"><span style="border-bottom: 1px solid black;">{{ strtoupper($judul) }}</span></div>
                                <div class="company">
                                    {{ strtoupper($perusahaan->nama) }} <br>
                                    {{ $perusahaan->alamat }} {{ $kota_perusahaan }} <br>
                                    Telp. {{ $perusahaan->telp }} | {{ $perusahaan->telp2 }} <br>
                                    E. {{ $perusahaan->email }}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <table class="detail">
                                    <tbody style="vertical-align: text-top;">
                                        <tr>
                                            <td class="title-detail" width="32%">No. Surat Jalan</td>
                                            <td class="titik" width="5%">&nbsp;:&nbsp;</td>
                                            <td class="data-detail">{{ $sj_keluar_header->no_surat_jalan }}</td>
                                        </tr>
                                        <tr>
                                            <td class="title-detail">Tanggal</td>
                                            <td class="titik">&nbsp;:&nbsp;</td>
                                            <td class="data-detail">
                                                <?php $tanggal = new DateTime($sj_keluar_header->tanggal); 
                                                echo $tanggal->format('d/m/Y G:i:s');
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="title-detail">Pelanggan</td>
                                            <td class="titik">&nbsp;:&nbsp;</td>
                                            <td class="data-detail user">{{ $pelanggan['nama'] }}</td>
                                        </tr>
                                        <tr>
                                            <td class="title-detail" style="vertical-align: top;">Alamat </td>
                                            <td class="titik" style="vertical-align: top;">&nbsp;:&nbsp;</td>
                                            <td class="data-detail user">{{ $pelanggan['alamat'] }}<br>
                                            Telp. {{ $pelanggan['telp'] }}<br>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-4">
                                <div class="detail">
                                    <b>Barang dikirim ke: </b><br>
                                    <table width="100%">
                                    <tr>
                                        <td class="title-detail" style="vertical-align: top;">Alamat </td>
                                        <td class="titik" style="vertical-align: top;">&nbsp;:&nbsp;</td>
                                        <td>{{ $so_alamat_pengiriman->alamat }} {{ ucwords(strtolower($so_alamat_pengiriman->kota->nama)) }}<br>
                                        Telp. {{ $so_alamat_pengiriman->telp }}<br>
                                        </td>
                                    </tr>
                                    </table>
                                    </div>
                                </div>
                            </div>
                    </section>
                    <section id="table">
                        <div>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th width="5%">No.</th>
                                        <th>Nama Produk</th>
                                        <th>Jumlah</th>
                                        <th width="40%">Serial Number</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $jumlah_item=0; $total_akhir=0; ?>
                                    @foreach($sj_keluar_detail as $key => $val)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>
                                            @if($val->jenis_barang_id == 1)
                                            {{ $val->produk->nama }}
                                            @elseif($val->jenis_barang_id == 2)
                                            {{ $val->hadiah->nama }}
                                            @elseif($val->jenis_barang_id == 3)
                                            {{ $val->paket->nama }}
                                            @endif
                                        </td>
                                        <td align="center">{{ $val->jumlah_total }}<?php $jumlah_item=$jumlah_item+$val->jumlah_total ?></td>
                                        <td style="text-align: center;">
                                            <?php
                                                $indeks = 1;
                                                foreach($produk_sn[$key] as $val2)
                                                {
                                                    if($indeks == $val->jumlah_total){
                                                        echo "$val2->serial_number";
                                                    }
                                                    elseif(!empty($val2->serial_number)){
                                                        echo "$val2->serial_number, ";
                                                    }

                                                    $indeks++;
                                                }
                                            ?>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </section>
                    <section id="footer">
                        <div class="row">
                            <div class="col-md-4">
                                <table class="keterangan">
                                    <tr>
                                        <td class="title-detail" style="vertical-align: top;">Keterangan</td>
                                        <td class="titik" style="vertical-align: top;">&nbsp;:&nbsp;</td>
                                        <td class="data-detail">
                                        {{ $so_header->catatan }}
                                        <br><br></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-3">
                                <table class="keterangan" style="margin-bottom: 50px;" width="96%">
                                    <tr style="text-align: center;">
                                        <td class="title-detail" style="width: 30%">Jml Item</td>
                                        <td class="titik" style="vertical-align: top; width: 5%">&nbsp;:&nbsp;</td>
                                        <td class="data-detail" style="width: 12%">{{ $jumlah_item }}</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-5" style="padding: 0;">
                                <div class="col-md-6">
                                    <div>
                                        <div style="text-align: center">
                                            <strong>Penerima</strong>
                                        </div>
                                    </div>
                                    <div style="margin-top: 100px;">
                                        <div style="text-align: center">
                                            <strong>(.........................)</strong>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div>
                                        <div style="text-align: center">
                                            <strong>Hormat Kami</strong>
                                        </div>
                                    </div>
                                    <div style="margin-top: 100px;">
                                        <div style="text-align: center">
                                            <strong>(.........................)</strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <?php echo $print_time->format('d/m/Y G:i:s'); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ URL::to('/css/invoice.css') }}">
@stop