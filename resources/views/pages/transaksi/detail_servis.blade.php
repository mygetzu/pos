<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<script src="{{ asset('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ url('js/jquery.maskMoney.js') }}" type="text/javascript"></script>
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                @if($service_order->flag === 0)
                    <a data-toggle="modal" data-target="#servis_selesai">
                        <button class='btn bg-navy flat'><span class='fa fa-file-o'></span> Jadikan Status Selesai</button>
                    </a>
                @endif
                <span class="pull-left" style="font-size: 26px;">Data Perbaikan</span>
                <div class="pull-right">
                    <a href="{{ url('/servis') }}">
                        <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                    </a>
                </div>
            </div>
            <div class="box-body">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                @endif
                <table class="table table-striped">
                    <tr>
                        <th width="20%">No Nota</th>
                        <th width="5%">:</th>
                        <td><input type="text" class="form-control" value="{{ $service_order->no_nota }}" disabled="true"></td>
                    </tr>
                    <tr>
                        <th>Status</th>
                        <th>:</th>
                        <td style="font-weight: bold; text-align: left;">
                            @if($service_order->status_service == 'Belum')
                                @if($service_order->teknisi_id == NULL)
                                    <?php $status = 'Belum Memilih Teknisi'?>
                                @endif
                            @elseif($service_order->status_service == 'Proses')
                                @if($invoice_header['count'] == 0)
                                    <?php $status = 'Proses Pengecekan Kerusakan oleh Teknisi'?>
                                @else
                                    @if($invoice_header['biaya'] == 0)
                                        <?php $status = 'Pengisian Biaya Servis oleh Admin'?>
                                    @else
                                        @if($service_order->tanggal_acc == NULL)
                                            @if($service_order->is_check == 0)
                                                <?php $status = 'Menunggu Persetujuan oleh Pelanggan (pelanggan belum mengecek email)'?>
                                            @else
                                                <?php $status = 'Menunggu Persetujuan oleh Pelanggan (pelanggan telah mengecek email)'?>
                                            @endif
                                        @else
                                            <?php $status = 'Proses Perbaikan oleh Teknisi'?>
                                        @endif
                                    @endif
                                @endif
                            @elseif($service_order->status_service == 'Selesai')
                                @if($invoice_header['status_bayar'] == 0)
                                    <?php $status = 'Perbaikan Selesai dan Belum Dibayar'?>
                                @else
                                    @if($service_order->tanggal_diambil == NULL)
                                        <?php $status = 'Perbaikan Selesai dan Sudah Dibayar dan Belum Diambil oleh Pelanggan'?>
                                    @else
                                        <?php $status = 'Perbaikan Selesai dan Sudah Dibayar dan Sudah Diambil oleh Pelanggan'?>
                                    @endif
                                @endif
                            @endif
                            <input type="text" class="form-control" name="hp_supervisor" id="" value="{{ $status }}" disabled="true">
                        </td>
                    </tr>
                    <tr>
                        <th>Tanggal Diterima</th>
                        <th>:</th>
                            <?php
                            $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
                            date_default_timezone_set("Asia/Jakarta");
                            $tanggal = new DateTime($service_order->tanggal_diterima);
                            $tanggal_selesai = new DateTime($service_order->tanggal_selesai);
                            $tanggal_diambil = new DateTime($service_order->tanggal_diambil);
                            ?>
                        <td><input type="text" class="form-control" name="alamat" id="alamat" value="{{ $tanggal->format('d') }} {{ $bulan[(int)$tanggal->format('m')] }} {{ $tanggal->format('Y') }}" disabled="true"></td>
                    </tr>
                    <tr>
                        <th>Nama Pelanggan</th>
                        <th>:</th>
                        <td><input type="text" class="form-control" value="{{ $service_order->pelanggan->nama }}" disabled="true"></td>
                    </tr>
                    <tr>
                        <th>Nama Teknisi</th>
                        <th>:</th>
                        <td><input type="text" class="form-control" value="{{ ($service_order['nama_teknisi'] == NULL? '-' : $service_order['nama_teknisi'] ) }}" disabled="true"></td>
                    </tr>
                    <tr>
                        <th>Serial Number</th>
                        <th>:</th>
                        <td><input type="text" class="form-control" name="telp" id="telp" value="{{ $service_order->serial_number }}" onKeyPress="return numbersonly(this, event)" disabled="true"></td>
                    </tr>
                    <tr>
                        <th>Keluhan</th>
                        <th>:</th>
                        <td><textarea type="text" class="form-control" disabled="true">{{ $service_order->keluhan_pelanggan }}</textarea></td>
                    </tr>
                    <tr>
                        <th>Deskripsi Produk</th>
                        <th>:</th>
                        <td><textarea type="text" class="form-control" disabled="true">{{ $service_order->deskripsi_produk }}</textarea></td>
                    </tr>
                    <tr class="hidden">
                        <th>Deskripsi Penyelesaian</th>
                        <th>:</th>
                        <td><textarea type="text" class="form-control" disabled="true">{{ $service_order->deskripsi_penyelesaian }}</textarea></td>
                    </tr>
                    <tr>
                        <th>Tanggal Cek Kerusakan</th>
                        <th>:</th>
                        <td><input type="   text" class="form-control" name="hp_supervisor" id="" value="<?php echo ($service_order->tanggal_cek == NULL ? 'Belum Dicek' : $tanggal_selesai->format('d') .' '. $bulan[(int)$tanggal_selesai->format('m')] .' '. $tanggal_selesai->format('Y')) ?>" disabled="true"></td>
                    </tr>
                    <tr>
                        <th>Tanggal ACC Pelanggan</th>
                        <th>:</th>
                        <td><input type="text" class="form-control" name="hp_supervisor" id="" value="<?php echo ($service_order->tanggal_acc == NULL ? 'Belum Di-ACC' : $tanggal_selesai->format('d') .' '. $bulan[(int)$tanggal_selesai->format('m')] .' '. $tanggal_selesai->format('Y')) ?>" disabled="true"></td>
                    </tr>
                    <tr>
                        <th>Tanggal Diperbaiki</th>
                        <th>:</th>
                        <td><input type="text" class="form-control" name="hp_supervisor" id="" value="<?php echo ($service_order->tanggal_diperbaiki == NULL ? 'Belum Diperbaiki' : $tanggal_selesai->format('d') .' '. $bulan[(int)$tanggal_selesai->format('m')] .' '. $tanggal_selesai->format('Y')) ?>" disabled="true"></td>
                    </tr>
                    <tr>
                        <th>Tanggal Selesai</th>
                        <th>:</th>
                        <td><input type="text" class="form-control" name="hp_supervisor" id="" value="<?php echo ($service_order->tanggal_selesai == NULL ? 'Belum Selesai' : $tanggal_selesai->format('d') .' '. $bulan[(int)$tanggal_selesai->format('m')] .' '. $tanggal_selesai->format('Y')) ?>" disabled="true"></td>
                    </tr>
                    <tr>
                        <th>Tanggal Diambil</th>
                        <th>:</th>
                        <td><input type="text" class="form-control" name="hp_supervisor" id="" value="<?php echo ($service_order->tanggal_diambil == NULL ? 'Belum Diambil' : $tanggal_diambil->format('d') .' '. $bulan[(int)$tanggal_diambil->format('m')] .' '. $tanggal_diambil->format('Y')) ?>" disabled="true"></td>
                    </tr>
                    <tr>
                        <th>Total Biaya Servis</th>
                        <th>:</th>
                        <td><input type="text" class="form-control" name="telp" id="telp" value="{{ rupiah($invoice_header['biaya']) }}" onKeyPress="return numbersonly(this, event)" disabled="true"></td>
                    </tr>
                </table>
                @if($invoice_detail == NULL)
                @else
                    <hr>
                    <span class="pull-left" style="font-size: 26px;">Detail Perbaikan</span>
                    <table class="table table-stripped">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Tindakan</th>
                                <th>Biaya</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($invoice_detail as $key => $value)
                                <tr>
                                    <td>{{ ++$key }}</td>
                                    <td>{{ $value->tindakan }}</td>
                                    <td>{{ rupiah($value->biaya) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="servis_selesai" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content box">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="riwayatheader">Servis Selesai</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="post" action="{{ url('/servis_selesai') }}" autocomplete="off">
        <input type="hidden" name="no_nota" value="{{ $service_order->no_nota }}">
        <input type="hidden" name="pelanggan_id" value="{{ $service_order->pelanggan_id }}">
        {{ csrf_field() }}
          <div class="box-body">
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Tanggal Selesai</label>
              <div class="col-sm-10">
                <?php $ts = new DateTime($service_order->tanggal_selesai);?>
                <input name="tanggal_selesai" id="tanggal_selesai" type="text" class="form-control pull-right datetimepicker flat" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" value='{{ $ts->format("d-m-Y") }}'>
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Deskripsi Penyelesaian</label>
              <div class="col-sm-10">
                <textarea class="form-control" name="deskripsi_penyelesaian"></textarea>
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Biaya Jasa Servis</label>
              <div class="col-sm-3">
                <input type="text" class="form-control" name="jasa_servis" placeholder="Masukkan biaya jasa servis" id="jasa_servis" data-thousands="." data-decimal="," style="text-align:right" value="{{ old('jasa_servis') }}" />
                <script type="text/javascript">$("#jasa_servis").maskMoney({precision:0});</script>
              </div>
              <label class="control-label">(IDR)</label>
            </div>
          </div><!-- /.box-body -->
          <div class="box-footer">
            <button data-dismiss="modal" class="btn btn-default flat"><span class="glyphicon glyphicon-ban-circle"></span> batal</button>
            <button type="submit" class="btn btn-primary pull-right flat" onclick="document.getElementById('loading3').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
          </div><!-- /.box-footer -->
        </form>
      </div>
      <div class="overlay" id="loading3" style="display:none">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
    </div>
  </div>
</div>
@stop

<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<script type="text/javascript">
    $(function () {

        $('#tanggal_selesai').datepicker({
            autoclose: true,
            todayHighlight: 1,
        });
    });
</script>