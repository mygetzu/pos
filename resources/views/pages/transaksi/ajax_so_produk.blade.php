<?php
    function rupiah($nominal)
    {
        $rupiah =  number_format($nominal,0, ",",".");
        $rupiah = "Rp "  . $rupiah;
        return $rupiah;
    }
?>
<table class="table table-striped table-bordered" id="ajax_produk_tabless">
    <thead>
        <tr>
            <th style="text-align:center">NO</th>
            <th style="text-align:center">NAMA</th>
            <th style="text-align:center" width="15%">HARGA</th>
            <th style="text-align:center">PROMO</th>
            <th style="text-align:center">STOK TERSEDIA</th>
            <th style="text-align:center"></th>
            <th style="text-align:center"></th>
        </tr>
    </thead>
    <tbody>
        <?php $i=1; ?>
        @foreach($produk as $val)
        <?php //cari diskon yang paling besar diantara default_diskon, produk_harga_diskon dan diskon
            $diskon             = 0; 
            $qty_beli_diskon    = 1;
            $cashback           = 0; 
            $qty_beli_cashback  = 1;
            $hadiah             = "";
            $qty_beli_hadiah    = 1;
            $qty_hadiah         = 1;

            if(!empty($pelanggan->kategori_pelanggan)){
                if($pelanggan->kategori_pelanggan->default_diskon > $diskon){
                    $diskon = $pelanggan->kategori_pelanggan->default_diskon;
                }
            }
            
            if(!empty($val->produk_harga)){
                if($val->produk_harga->diskon > $diskon){
                    $diskon = $val->produk_harga->diskon;
                }
            }
            
            if(!empty($val->promo_diskon)){
                if($val->promo_diskon->diskon > $diskon){
                    $diskon = $val->promo_diskon->diskon;
                    $qty_beli_diskon= $val->promo_diskon->qty_beli;
                }
            }

            if(!empty($val->promo_cashback) && empty($val->produk_harga) && empty($pelanggan->kategori_pelanggan))
            {
                $cashback           = $val->promo_cashback->cashback;
                $qty_beli_cashback  = $val->promo_cashback->qty_beli;
            }
            
            if(!empty($val->promo_hadiah))
            {
                $hadiah             = $val->promo_hadiah->hadiah->nama;
                $qty_beli_hadiah    = $val->promo_hadiah->qty_beli;
                $qty_hadiah         = $val->promo_hadiah->qty_hadiah;
            }
        ?>
        <tr>
            <td align="center">{{ $i++ }}</td>
            <td>{{ $val->nama }}</td>
            <td width="15%">
                @if((int)$diskon != 0 AND (int)$qty_beli_diskon === 1)
                    <span><del>{{ rupiah($val->harga_retail) }}</del></span><br>
                    <span>{{ rupiah($val->harga_retail-($val->harga_retail*(int)$diskon/100)) }}</span>
                @elseif((int)$cashback != 0 AND (int)$qty_beli_cashback === 1)
                    <span><del>{{ rupiah($val->harga_retail) }}</del></span><br>
                    <span>{{ rupiah($val->harga_retail-(int)$cashback) }}</span>
                @else
                <span>{{ rupiah($val->harga_retail) }}</span>
                @endif
            </td>
            <td align="center">
            @if((int)$diskon != 0)
                <span class="label label-success flat" style="font-size:10pt;margin-right:-5px">diskon {{ $diskon }}% @if($qty_beli_diskon > 1) untuk {{ $qty_beli_diskon }} pembelian @endif</span>
            @elseif((int)$cashback != 0)
                <span class="label label-success flat" style="font-size:10pt;margin-right:-5px">cashback {{ rupiah($cashback) }} @if($qty_beli_cashback > 1) untuk {{ $qty_beli_cashback }} pembelian @endif</span>
            @elseif(!empty($hadiah))
                <span class="label label-success flat" style="font-size:10pt;margin-right:-5px">berhadiah @if($qty_hadiah > 1) {{ $qty_hadiah }} unit @endif {{ $hadiah }} @if($qty_beli_hadiah > 1) untuk {{ $qty_beli_hadiah }} pembelian @endif</span>
            @endif
            </td>
            <td align="center">{{ (int)$val->stok - (int)$val->stok_dipesan }}</td>
            <td>
                @if(empty($val->file_gambar))
                <img src="{{ URL::asset('img/produk/'.$gambar_produk_default) }}" style="height:50px;object-fit: contain;width:auto;padding: 5px 5px 0px 5px" class="img-responsive" alt="">
                @else
                <img src="{{ URL::asset('img/produk/'.$val->file_gambar) }}" style="height:50px;object-fit: contain;width:auto;padding: 5px 5px 0px 5px" class="img-responsive" alt="">
                @endif
            </td>
            <td align="center">
                <button class="btn bg-maroon flat" type="button" onclick="pilih_produk('{{ $val->id }}')" data-dismiss="modal"><span class="fa fa-check"></span> Pilih</button>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
  $(function () {
    $("#ajax_produk_tabless").DataTable();
  });
</script>