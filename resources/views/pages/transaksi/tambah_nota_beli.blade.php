@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<input type="hidden" name="jumlah_pesanan" id="jumlah_pesanan" value="">
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <a href="{{ url('/nota_beli') }}" class="pull-right" style="padding:0px">
                    <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                </a>
            </div>
            <div class="box-body">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <form class="form-horizontal" autocomplete="off" method="post" action="{{ url('/do_tambah_nota_beli') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="sj_masuk_header_id" id="sj_masuk_header_id">
                    <div class="form-group{{ $errors->has('display_no_surat_jalan') ? ' has-error' : '' }}">
                        <label class="control-label col-sm-2">No Surat Jalan Masuk</label>
                        <div class="col-sm-4">
                            <input type="text" id="display_no_surat_jalan" class="form-control" disabled="true" value="{{ old('no_surat_jalan') }}">
                            @if ($errors->has('no_surat_jalan'))
                                <span class="help-block">
                                    <strong>Pilih No Surat Jalan Masuk Terlebih Dahulu</strong>
                                </span>
                            @endif
                        </div>
                        <div class="col-md-4">
                            <a data-toggle="modal" data-target="#pilih_no_surat_jalam_masuk">
                                <button class="btn bg-maroon flat" type="button" id="button_pilih_nota"><span class="fa fa-file"></span> Pilih No Surat Jalan Masuk</button>
                            </a>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('nota_beli') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label">Nota Beli</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Masukkan nota beli" name="nota_beli" id="nota_beli" value="{{ $no_nota_beli }}">
                            @if ($errors->has('nota_beli'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('nota_beli') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('tanggal') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label">Tanggal</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <?php $date = new DateTime(); 
                                $tanggal = $date->format('d').'-'.$date->format('m').'-'.$date->format('Y'); ?>
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="tanggal" data-date-format="dd-mm-yyyy" name="tanggal" value="{{ $tanggal }}" placeholder="Masukkan tanggal">
                            </div>
                            @if ($errors->has('tanggal'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('tanggal') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('total_tagihan') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label">Tagihan</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" name="total_tagihan" id="total_tagihan" disabled="true">
                        </div>
                    </div>
                    <div class="form-group" id="div_ppn" style="display: none">
                        <label class="col-sm-2 control-label" id="label_ppn">PPN</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" name="ppn" id="ppn" disabled="true">
                        </div>
                    </div>
                    <div class="form-group" id="div_diskon" style="display: none">
                        <label class="col-sm-2 control-label">Diskon</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" name="diskon" id="diskon" disabled="true">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Jatuh Tempo</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="jatuh_tempo" data-date-format="dd-mm-yyyy" name="jatuh_tempo" value="{{ old('jatuh_tempo') }}" placeholder="Masukkan tanggal jatuh tempo">
                            </div>
                            @if ($errors->has('jatuh_tempo'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('jatuh_tempo') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                        <div class="col-sm-8 col-sm-offset-2">
                            <button class="btn btn-primary flat" onclick="document.getElementById('loading').style.display = 'block'"><span class="fa fa-floppy-o"></span> Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="overlay" id="loading" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="pilih_no_surat_jalam_masuk" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="riwayatheader">Pilih Surat Jalan</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered" id="produk_table">
                    <thead>
                        <tr>
                            <th style="text-align: center;">NO</th>
                            <th style="text-align: center;">NO SURAT JALAN</th>
                            <th style="text-align: center;">NAMA SUPPLIER</th>
                            <th style="text-align: center;">TANGGAL</th>
                            <th style="text-align: center;"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @for($i=0; $i < $indeks; $i++)
                        <tr>
                            <td align="center">{{ $i+1 }}</td>
                            <td>{{ $surat_jalan[$i]['no_surat_jalan'] }}</td>
                            <td>{{ $surat_jalan[$i]['supplier'] }}</td>
                            <td>
                                {{ $surat_jalan[$i]['tanggal'] }}
                            </td>
                            <td align="center">
                                <?php $sj_masuk_header_id = $surat_jalan[$i]['sj_masuk_header_id'];
                                $no_surat_jalan = $surat_jalan[$i]['no_surat_jalan']; ?>
                                <button class="btn bg-maroon flat" data-dismiss="modal" onclick="btn_pilih_surat_jalan('{{ $sj_masuk_header_id }}', '{{ $no_surat_jalan }}')"><span class="fa fa-check"></span> Pilih</button>
                            </td>
                        </tr>
                        @endfor
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ asset('/js/jquery.maskMoney.js') }}" type="text/javascript"></script>
<script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
    $(function () {
        $("#produk_table").DataTable();

        $('#tanggal').datepicker({
            autoclose: true,
            todayHighlight: 1,
        });

        $('#jatuh_tempo').datepicker({
            autoclose: true,
            todayHighlight: 1,
        });
    });

    function btn_pilih_surat_jalan(sj_masuk_header_id, no_surat_jalan)
    {
        document.getElementById('sj_masuk_header_id').value     = sj_masuk_header_id;
        document.getElementById('display_no_surat_jalan').value = no_surat_jalan;

        document.getElementById('div_ppn').style.display = 'none';
        document.getElementById('div_diskon').style.display = 'none';

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })

        $.ajax({
            type: "POST",
            url: "{{ url('/nb_get_data') }}",
            data: { sj_masuk_header_id: sj_masuk_header_id },
            dataType: 'JSON',
            success: function (data) {
                document.getElementById('total_tagihan').value          = rupiah(data.total_tagihan);

                var tagihan = parseInt(data.total_tagihan);
                var diskon  = parseInt(data.diskon);
                var ppn     = parseInt(data.ppn);

                if(ppn > 0){
                    document.getElementById('label_ppn').innerHTML   = "PPN "+data.ppn+"%";
                    document.getElementById('ppn').value             = rupiah(tagihan*ppn/100);
                    document.getElementById('div_ppn').style.display = 'block';
                }

                if(diskon > 0){ 
                    document.getElementById('diskon').value             = rupiah(data.diskon); 
                    document.getElementById('div_diskon').style.display = 'block';
                }
            },
            error: function (data) {
                // alert('error 1');
            }
        });
    }

    function rupiah(nominal)
    {
        //rumus konversi rupiah
        nominal = "Rp "+nominal.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
        return nominal;
    }
</script>
@stop