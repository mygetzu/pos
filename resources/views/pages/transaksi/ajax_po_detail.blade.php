<?php
    function rupiah($nominal)
    {
        $rupiah =  number_format($nominal,0, ",",".");
        $rupiah = "Rp "  . $rupiah;
        return $rupiah;
    }
?>
<table class="table table-striped table-bordered" id="ajax_produk_tabless3">
    <thead>
        <tr>
            <th style="text-align:center">NO</th>
            <th style="text-align:center">NAMA PRODUK</th>
            <th style="text-align:center">SERIAL NUMBER</th>
            <th style="text-align:center">HARGA SATUAN</th>
            <th style="text-align:center">JUMLAH</th>
            <th style="text-align:center"></th>
        </tr>
    </thead>
    <tbody>
        <?php $i=1; ?>
        @foreach($po_detail as $val)
        <tr>
            <td align="center">{{ $i++ }}</td>
            <td>{{ $val['nama'] }}</td>
            <td>{{ $val['serial_number'] }}</td>
            <td>{{ rupiah($val['harga']) }}</td>
            <td align="center">{{ $val['jumlah'] }}</td>
            <td align="center">
                <?php   $produk_id          = $val['produk_id'];
                        $serial_number      = $val['serial_number'];
                        $harga              = $val['harga'];
                        $jenis_barang_id    = $val['jenis_barang_id'];
                        $gudang_id          = $val['gudang_id']; ?>
                <button class="btn bg-maroon flat" data-dismiss='modal' type="button" onclick="btn_pilih_produk('{{ $produk_id }}', '{{ $serial_number }}', '{{ $harga }}', '{{ $jenis_barang_id }}', '{{ $gudang_id }}')"><span class="fa fa-check"></span> Pilih</button>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
  $(function () {
    $("#ajax_produk_tabless3").DataTable();
  });
</script>