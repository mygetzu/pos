@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<input type="hidden" name="total_pesanan_entri" id="total_pesanan_entri" value="{{ $total_pesanan_entri }}">
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <a data-toggle='modal' data-target='#modal_po_detail' style="padding:0px">
                    <button class='btn btn-info flat'><span class='fa fa-info-circle'></span> Purchase Order Detail</button>
                </a>
                <a href="{{ url('/surat_jalan_masuk') }}" class="pull-right" style="padding:0px">
                    <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                </a>
            </div>
            <div class="box-body">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <form class="form-horizontal" autocomplete="off" id="form_serial_number">
                    <input type="hidden" name="no_nota" id="no_nota" value="{{ $po_header->no_nota }}">
                    <input type="hidden" name="jumlah_produk" id="jumlah_produk" value="0">
                    <input type="hidden" name="supplier_id" id="supplier_id" class="form-control">
                    {{ csrf_field() }}
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Gudang</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="gudang" id="gudang">
                                    <option value="">-- Pilih Gudang --</option>
                                    @foreach($gudang as $val)
                                        <option value="{{ $val->id }},{{ $val->nama }}">{{ $val->nama }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Produk</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="produk" id="produk">
                                    <option value="">-- Pilih Produk --</option>
                                    @foreach($produk as $val)
                                        <option value="{{ $val['id'] }},{{ $val['nama'] }},{{ $val['jenis'] }}">{{ $val['nama'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                            <label class="col-sm-3 control-label">No Surat Jalan</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="no_surat_jalan" id="input_no_surat_jalan" value="{{ $no_surat_jalan }}" placeholder="masukkan no surat jalan">
                                @if ($errors->has('no_surat_jalan'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('no_surat_jalan') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                            <label class="col-sm-3 control-label">Total Pesanan Entri</label>
                            <div class="col-sm-9">
                                <label class="control-label">{{ $total_pesanan_entri }}</label>
                            </div>
                        </div>
                    </div>
                </form>           
            </div>
            <div class="overlay" id="loading" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-body">
                <div class="form-horizontal">
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                            <label class="col-sm-3 control-label">Serial Number</label>
                            <div class="col-sm-9">
                                <input type="text" name="serial_number" class="form-control" onkeyup="if(event.keyCode == 13) sj_tambah_stok()" placeholder="Masukkan serial number, tekan enter" id="serial_number">
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                            <label class="col-sm-3 control-label">Jumlah SN</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" value="1" id="jumlah_sn" onKeyPress="return numbersonly(this, event)" >
                            </div>
                        </div> 
                        <div class="form-group" id="display_button_list">
                            <div class="col-sm-9 col-sm-offset-3">
                                <button type="button" class="btn btn-primary flat" onclick="sj_tambah_stok()"><span class="fa fa-file-text-o"></span> Masukkan List</button>
                            </div>
                        </div>   
                    </div>
                    <div class="col-md-6">
                        <a data-toggle="modal" data-target="#tambah_50_sn">
                            <button class="btn btn-primary flat"><span class="fa fa-plus"></span> Masukkan 50 Serial Number</button>
                        </a>
                    </div>
                </div>
                <div class="col-sm-12">
                    <button type="button" class="btn btn-success flat pull-right" onclick="btn_selesai()"><span class="fa fa-check"></span> Selesai</button>
                    <br>
                    <hr>
                </div>
                <div class="col-md-12">
                    <table class="table table-striped table-bordered" id="table_surat_jalan">
                        <thead>
                            <tr>
                                <th style="text-align:center">GUDANG</th>
                                <th style="text-align:center">NAMA PRODUK/HADIAH</th>
                                <th style="text-align:center">JENIS</th>
                                <th style="text-align:center">SERIAL NUMBER</th>
                                <th style="text-align:center">JUMLAH</th>
                                <th style="text-align:center"></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="overlay" id="loading2" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>
@foreach($arr_po_detail as $key => $val)
    <input type="hidden" id="arr_po_detail{{ $val['produk_id'] }}-{{ $val['jenis_barang_id'] }}" value="{{ $val['quantity'] }}">
@endforeach

<form action="{{ url('/surat_jalan_masuk_proses') }}" method="post" id="form_surat_jalan_proses" >
    {{ csrf_field() }}
    <input type="hidden" name="no_surat_jalan" id="no_surat_jalan">
    <input type="hidden" name="no_nota_po" value="{{ $po_header->no_nota }}">
    <input type="hidden" name="jumlah_pesanan" value="{{ $total_pesanan_entri }}">
@foreach($arr_serial_number as $key => $val)
    <input type="hidden" name="arr_serial_number_id{{ $key }}" id="arr_serial_number_id{{ $key }}" value="{{ $val['id'] }}">
    <input type="hidden" name="arr_serial_number_gudang_id{{ $key }}" id="arr_serial_number_gudang_id{{ $key }}" value="{{ $val['gudang_id'] }}">
    <input type="hidden" name="arr_serial_number_produk_id{{ $key }}" id="arr_serial_number_produk_id{{ $key }}" value="{{ $val['produk_id'] }}">
    <input type="hidden" name="arr_serial_number_jenis_barang_id{{ $key }}" id="arr_serial_number_jenis_barang_id{{ $key }}" value="{{ $val['jenis_barang_id'] }}">
    <input type="hidden" name="arr_serial_number_serial_number{{ $key }}" id="arr_serial_number_serial_number{{ $key }}" value="{{ $val['serial_number'] }}">
@endforeach
</form>

<div class="modal fade" id="modal_po_detail" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content box">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">PO Detail</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-bordered" id="ajax_produk_tabless3">
                    <thead>
                        <tr>
                            <th style="text-align:center">NO</th>
                            <th style="text-align:center">NAMA PRODUK</th>
                            <th style="text-align:center">SERIAL NUMBER</th>
                            <th style="text-align:center">HARGA SATUAN</th>
                            <th style="text-align:center">JUMLAH</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i=1; ?>
                        @foreach($po_detail as $val)
                        <tr>
                            <td align="center">{{ $i++ }}</td>
                            <td>
                                @if(!empty($val->produk))
                                {{ $val->produk->nama }}
                                @elseif(!empty($val->hadiah))
                                {{ $val->hadiah->nama }}
                                @endif
                            </td>
                            <td>{{ $val->serial_number }}</td>
                            <td>{{ rupiah($val->harga) }}</td>
                            <td align="center">{{ $val->quantity }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="overlay" id="loading3" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="tambah_50_sn" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content box">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Serial Number</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <td><input type="text" id="sn1" class="form-control" placeholder="Masukkan SN"></td>
                            <td><input type="text" id="sn2" class="form-control" placeholder="Masukkan SN"></td>
                            <td><input type="text" id="sn3" class="form-control" placeholder="Masukkan SN"></td>
                            <td><input type="text" id="sn4" class="form-control" placeholder="Masukkan SN"></td>
                            <td><input type="text" id="sn5" class="form-control" placeholder="Masukkan SN"></td>
                        </tr>
                        <tr>
                            <td><input type="text" id="sn6" class="form-control" placeholder="Masukkan SN"></td>
                            <td><input type="text" id="sn7" class="form-control" placeholder="Masukkan SN"></td>
                            <td><input type="text" id="sn8" class="form-control" placeholder="Masukkan SN"></td>
                            <td><input type="text" id="sn9" class="form-control" placeholder="Masukkan SN"></td>
                            <td><input type="text" id="sn10" class="form-control" placeholder="Masukkan SN"></td>
                        </tr>
                        <tr>
                            <td><input type="text" id="sn11" class="form-control" placeholder="Masukkan SN"></td>
                            <td><input type="text" id="sn12" class="form-control" placeholder="Masukkan SN"></td>
                            <td><input type="text" id="sn13" class="form-control" placeholder="Masukkan SN"></td>
                            <td><input type="text" id="sn14" class="form-control" placeholder="Masukkan SN"></td>
                            <td><input type="text" id="sn15" class="form-control" placeholder="Masukkan SN"></td>
                        </tr>
                        <tr>
                            <td><input type="text" id="sn16" class="form-control" placeholder="Masukkan SN"></td>
                            <td><input type="text" id="sn17" class="form-control" placeholder="Masukkan SN"></td>
                            <td><input type="text" id="sn18" class="form-control" placeholder="Masukkan SN"></td>
                            <td><input type="text" id="sn19" class="form-control" placeholder="Masukkan SN"></td>
                            <td><input type="text" id="sn20" class="form-control" placeholder="Masukkan SN"></td>
                        </tr>
                        <tr>
                            <td><input type="text" id="sn21" class="form-control" placeholder="Masukkan SN"></td>
                            <td><input type="text" id="sn22" class="form-control" placeholder="Masukkan SN"></td>
                            <td><input type="text" id="sn23" class="form-control" placeholder="Masukkan SN"></td>
                            <td><input type="text" id="sn24" class="form-control" placeholder="Masukkan SN"></td>
                            <td><input type="text" id="sn25" class="form-control" placeholder="Masukkan SN"></td>
                        </tr>
                        <tr>
                            <td><input type="text" id="sn26" class="form-control" placeholder="Masukkan SN"></td>
                            <td><input type="text" id="sn27" class="form-control" placeholder="Masukkan SN"></td>
                            <td><input type="text" id="sn28" class="form-control" placeholder="Masukkan SN"></td>
                            <td><input type="text" id="sn29" class="form-control" placeholder="Masukkan SN"></td>
                            <td><input type="text" id="sn30" class="form-control" placeholder="Masukkan SN"></td>
                        </tr>
                        <tr>
                            <td><input type="text" id="sn31" class="form-control" placeholder="Masukkan SN"></td>
                            <td><input type="text" id="sn32" class="form-control" placeholder="Masukkan SN"></td>
                            <td><input type="text" id="sn33" class="form-control" placeholder="Masukkan SN"></td>
                            <td><input type="text" id="sn34" class="form-control" placeholder="Masukkan SN"></td>
                            <td><input type="text" id="sn35" class="form-control" placeholder="Masukkan SN"></td>
                        </tr>
                        <tr>
                            <td><input type="text" id="sn36" class="form-control" placeholder="Masukkan SN"></td>
                            <td><input type="text" id="sn37" class="form-control" placeholder="Masukkan SN"></td>
                            <td><input type="text" id="sn38" class="form-control" placeholder="Masukkan SN"></td>
                            <td><input type="text" id="sn39" class="form-control" placeholder="Masukkan SN"></td>
                            <td><input type="text" id="sn40" class="form-control" placeholder="Masukkan SN"></td>
                        </tr>
                        <tr>
                            <td><input type="text" id="sn41" class="form-control" placeholder="Masukkan SN"></td>
                            <td><input type="text" id="sn42" class="form-control" placeholder="Masukkan SN"></td>
                            <td><input type="text" id="sn43" class="form-control" placeholder="Masukkan SN"></td>
                            <td><input type="text" id="sn44" class="form-control" placeholder="Masukkan SN"></td>
                            <td><input type="text" id="sn45" class="form-control" placeholder="Masukkan SN"></td>
                        </tr>
                        <tr>
                            <td><input type="text" id="sn46" class="form-control" placeholder="Masukkan SN"></td>
                            <td><input type="text" id="sn47" class="form-control" placeholder="Masukkan SN"></td>
                            <td><input type="text" id="sn48" class="form-control" placeholder="Masukkan SN"></td>
                            <td><input type="text" id="sn49" class="form-control" placeholder="Masukkan SN"></td>
                            <td><input type="text" id="sn50" class="form-control" placeholder="Masukkan SN"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default flat pull-left"><span class="glyphicon glyphicon-ban-circle"></span> Batal</button>
                <button type="button" class="btn btn-primary flat" onclick="sn_50_klik()" data-dismiss="modal" ><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
            </div>
            <div class="overlay" id="loading5" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
            </div>
        </div>
    </div>
</div>

<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
  $(function () {
    $("#ajax_produk_tabless3").DataTable();
  });
</script>
<script type="text/javascript">
    function sj_tambah_stok()
    {
        var gudang      = document.getElementById('gudang').value;
        var produk      = document.getElementById('produk').value;

        if(gudang == "" || produk == ""){
            alert('masukkan gudang dan produk terlebih dahulu');
            document.getElementById('serial_number').value  = "";
            document.getElementById('jumlah_sn').value      = 1;
        }
        else{ 
            gudang              = gudang.split(',');
            var gudang_id       = gudang[0];
            var gudang_nama     = gudang[1];

            produk              = produk.split(',');
            var produk_id       = produk[0];
            var produk_nama     = produk[1];
            var jenis_barang_id = produk[2];

            var serial_number   = document.getElementById('serial_number').value;
            var jumlah_sn       = document.getElementById('jumlah_sn').value;
            jumlah_sn = parseInt(jumlah_sn);

            if(serial_number == "" || jumlah_sn == ""){
                alert('masukkan serial number dan jumlah terlebih dahulu');
                document.getElementById('serial_number').value  = "";
                document.getElementById('jumlah_sn').value      = 1;
                return;
            }

            //cek arr_po_detail
            var cek_quantity = document.getElementById('arr_po_detail'.concat(produk_id).concat('-').concat(jenis_barang_id)).value;
            cek_quantity = parseInt(cek_quantity);

            if(cek_quantity - jumlah_sn < 0){
                if(cek_quantity <= 0){
                    alert('Jumlah serial number yang ditambahkan sudah sesuai dengan jumlah PO');
                    document.getElementById('serial_number').value  = "";
                    document.getElementById('jumlah_sn').value      = 1;
                }
                else{
                    alert('jumlah serial number tidak mencukupi');
                    document.getElementById('serial_number').value  = "";
                    document.getElementById('jumlah_sn').value      = 1;
                }
            }
            else{
                //kurangi quantity arr_po_detail
                document.getElementById('arr_po_detail'.concat(produk_id).concat('-').concat(jenis_barang_id)).value = cek_quantity-jumlah_sn;

                //tambah ke tabel
                var table   = document.getElementById("table_surat_jalan");
                var row     = table.insertRow(1);

                var cell1 = row.insertCell(0);
                var cell2 = row.insertCell(1);
                var cell3 = row.insertCell(2);
                var cell4 = row.insertCell(3);
                var cell5 = row.insertCell(4);
                var cell6 = row.insertCell(5);

                var jenis = "";

                if(jenis_barang_id == 1){
                    jenis = "PRODUK";
                }
                else{
                    jenis = "HADIAH";
                }

                cell1.innerHTML = gudang_nama;
                cell2.innerHTML = produk_nama;
                cell3.innerHTML = jenis;
                cell4.innerHTML = serial_number;
                cell5.innerHTML = jumlah_sn;
                cell6.innerHTML = '<button class="btn btn-danger flat" onclick="hapus_sn_klik(\''+gudang_id+'\',\''+produk_id+'\',\''+jenis_barang_id+'\',\''+serial_number+'\',\''+jumlah_sn+'\',this)"><span class="fa fa-trash-o"><span> Hapus</button>';
                
                cell3.setAttribute("align", "center");
                cell5.setAttribute("align", "center");
                cell6.setAttribute("align", "center");

                document.getElementById('serial_number').value  = "";
                document.getElementById('jumlah_sn').value      = 1;

                //tambahkan ke array
                var total_pesanan_entri = document.getElementById('total_pesanan_entri').value;
                total_pesanan_entri = parseInt(total_pesanan_entri);

                for (var i = 0; i < total_pesanan_entri; i++) {
                    if(jumlah_sn <= 0){
                        break;
                    }

                    var cek_arr_sn = document.getElementById('arr_serial_number_serial_number'.concat(i)).value;
                    
                    if(cek_arr_sn == ""){
                        document.getElementById('arr_serial_number_gudang_id'.concat(i)).value          = gudang_id;
                        document.getElementById('arr_serial_number_produk_id'.concat(i)).value          = produk_id;
                        document.getElementById('arr_serial_number_jenis_barang_id'.concat(i)).value    = jenis_barang_id;
                        document.getElementById('arr_serial_number_serial_number'.concat(i)).value      = serial_number;
                        jumlah_sn--;
                    }
                }
            } 
        }
    }

    function po_detail()
    { 
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });

        var no_nota     = document.getElementById("no_nota").value;
        var url = "{{ url('/get_po_serial_number') }}";
        
        $.ajax({
            type: "POST",
            url: url,
            data: { no_nota:no_nota },
            success: function (data) {
                
                $("#po_detail").html(data);
                document.getElementById('loading3').style.display = "none";
            },
            error: function (data) {
                alert('ooo');
            }
        });
    }

    function btn_selesai()
    {
        $cek = confirm('apakah produk serial number sudah sesuai?');

        if($cek){
            var no_surat_jalan = document.getElementById('input_no_surat_jalan').value;
            document.getElementById('no_surat_jalan').value = no_surat_jalan;
            document.getElementById('form_surat_jalan_proses').submit();
            document.getElementById('loading').style.display = 'block';
            document.getElementById('loading2').style.display = 'block';
        }
    }

    function hapus_sn_klik(gudang_id, produk_id, jenis_barang_id, serial_number, jumlah, obj)
    {
        var cek = confirm('apakah anda yakin?');

        if(cek){
            //hapus row table
            var row = obj.closest("tr");
            document.getElementById("table_surat_jalan").deleteRow(row.rowIndex);

            //hapus array sejumlah jumlah
            var total_pesanan_entri = document.getElementById('total_pesanan_entri').value;
            total_pesanan_entri = parseInt(total_pesanan_entri);

            var jumlah_hapus = jumlah;
            jumlah_hapus = parseInt(jumlah_hapus);
            for (var i = 0; i < total_pesanan_entri; i++) {
                if(jumlah_hapus <= 0){
                    break;
                }
                var cek_arr_sn_gudang_id        = document.getElementById('arr_serial_number_gudang_id'.concat(i)).value;
                var cek_arr_sn_produk_id        = document.getElementById('arr_serial_number_produk_id'.concat(i)).value;
                var cek_arr_sn_jenis_barang_id  = document.getElementById('arr_serial_number_jenis_barang_id'.concat(i)).value;
                var cek_arr_sn_serial_number    = document.getElementById('arr_serial_number_serial_number'.concat(i)).value;

                if(gudang_id == cek_arr_sn_gudang_id && produk_id == cek_arr_sn_produk_id && jenis_barang_id == cek_arr_sn_jenis_barang_id && serial_number == cek_arr_sn_serial_number){
                    jumlah_hapus--;
                    document.getElementById('arr_serial_number_gudang_id'.concat(i)).value          = "";
                    document.getElementById('arr_serial_number_produk_id'.concat(i)).value          = "";
                    document.getElementById('arr_serial_number_jenis_barang_id'.concat(i)).value    = "";
                    document.getElementById('arr_serial_number_serial_number'.concat(i)).value      = "";
                }
            }
            
            //tambah stok arr_po_detail
            var quantity = document.getElementById('arr_po_detail'.concat(produk_id).concat('-').concat(jenis_barang_id)).value;
            quantity = parseInt(quantity);
            jumlah      = parseInt(jumlah);
            var new_jumlah = quantity+jumlah;
            
            document.getElementById('arr_po_detail'.concat(produk_id).concat('-').concat(jenis_barang_id)).value = new_jumlah;
        }
    }

    function sn_50_klik()
    {
        var gudang      = document.getElementById('gudang').value;
        var produk      = document.getElementById('produk').value;

        if(gudang == "" || produk == ""){
            alert('masukkan gudang dan produk terlebih dahulu');
            document.getElementById('serial_number').value  = "";
            document.getElementById('jumlah_sn').value      = 1;
        }
        else{ 
            gudang              = gudang.split(',');
            var gudang_id       = gudang[0];
            var gudang_nama     = gudang[1];

            produk              = produk.split(',');
            var produk_id       = produk[0];
            var produk_nama     = produk[1];
            var jenis_barang_id = produk[2];

            for (var i = 1; i <= 50; i++) {
                var serial_number   = document.getElementById('sn'.concat(i)).value;
                var jumlah_sn       = 1;
                jumlah_sn = parseInt(jumlah_sn);
                document.getElementById('sn'.concat(i)).value = "";
                if(serial_number == ""){
                    continue;
                }

                //cek arr_po_detail
                var cek_quantity = document.getElementById('arr_po_detail'.concat(produk_id).concat('-').concat(jenis_barang_id)).value;
                cek_quantity = parseInt(cek_quantity);

                if(cek_quantity - jumlah_sn < 0){
                    if(cek_quantity <= 0){
                        alert('Jumlah serial number yang ditambahkan sudah sesuai dengan jumlah PO');
                        document.getElementById('serial_number').value  = "";
                        document.getElementById('jumlah_sn').value      = 1;
                        return;
                    }
                    else{
                        alert('jumlah serial number tidak mencukupi');
                        document.getElementById('serial_number').value  = "";
                        document.getElementById('jumlah_sn').value      = 1;
                        return;
                    }
                }
                else{
                    //kurangi quantity arr_po_detail
                    document.getElementById('arr_po_detail'.concat(produk_id).concat('-').concat(jenis_barang_id)).value = cek_quantity-jumlah_sn;

                    //tambah ke tabel
                    var table   = document.getElementById("table_surat_jalan");
                    var row     = table.insertRow(1);

                    var cell1 = row.insertCell(0);
                    var cell2 = row.insertCell(1);
                    var cell3 = row.insertCell(2);
                    var cell4 = row.insertCell(3);
                    var cell5 = row.insertCell(4);
                    var cell6 = row.insertCell(5);

                    var jenis = "";

                    if(jenis_barang_id == 1){
                        jenis = "PRODUK";
                    }
                    else{
                        jenis = "HADIAH";
                    }

                    cell1.innerHTML = gudang_nama;
                    cell2.innerHTML = produk_nama;
                    cell3.innerHTML = jenis;
                    cell4.innerHTML = serial_number;
                    cell5.innerHTML = jumlah_sn;
                    cell6.innerHTML = '<button class="btn btn-danger flat" onclick="hapus_sn_klik(\''+gudang_id+'\',\''+produk_id+'\',\''+jenis_barang_id+'\',\''+serial_number+'\',\''+jumlah_sn+'\',this)"><span class="fa fa-trash-o"><span> Hapus</button>';
                    
                    cell3.setAttribute("align", "center");
                    cell5.setAttribute("align", "center");
                    cell6.setAttribute("align", "center");

                    document.getElementById('serial_number').value  = "";
                    document.getElementById('jumlah_sn').value      = 1;

                    //tambahkan ke array
                    var total_pesanan_entri = document.getElementById('total_pesanan_entri').value;
                    total_pesanan_entri = parseInt(total_pesanan_entri);

                    for (var j = 0; j < total_pesanan_entri; j++) {
                        if(jumlah_sn <= 0){
                            break;
                        }

                        var cek_arr_sn = document.getElementById('arr_serial_number_serial_number'.concat(j)).value;
                        
                        if(cek_arr_sn == ""){
                            document.getElementById('arr_serial_number_gudang_id'.concat(j)).value          = gudang_id;
                            document.getElementById('arr_serial_number_produk_id'.concat(j)).value          = produk_id;
                            document.getElementById('arr_serial_number_jenis_barang_id'.concat(j)).value    = jenis_barang_id;
                            document.getElementById('arr_serial_number_serial_number'.concat(j)).value      = serial_number;
                            jumlah_sn--;
                        }
                    }
                } 
            }
        }
    }
</script>
@stop