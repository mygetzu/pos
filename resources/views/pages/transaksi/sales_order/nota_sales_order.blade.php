@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<?php
    function rupiah2($nominal)
    {
        $rupiah =  number_format($nominal,0, ",",".");
        $rupiah = $rupiah.',00';
        return $rupiah;
    }
?>
<link rel="stylesheet" type="text/css" href="{{ URL::to('/css/invoice.css') }}">
<style type="text/css">
    pre {
        font-family: sans-serif;
        background-color: transparent;
        border: none;
        overflow-x: hidden;
        white-space: pre-wrap;       /* css-3 */
        white-space: -moz-pre-wrap;  /* Mozilla, since 1999 */
        white-space: -pre-wrap;      /* Opera 4-6 */
        white-space: -o-pre-wrap;    /* Opera 7 */
        word-wrap: break-word; 
        padding: 0px;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <a href="{{ url('/nota_sales_order_print/'.$so_header->id) }}" target="_blank" class="btn bg-orange flat" style="margin-right: 5px;"><i class="fa fa-print"></i> Cetak</a>
                <a href="{{ url('/nota_sales_order_pdf/'.$so_header->id) }}" target="_blank" class="btn btn-info flat" style="margin-right: 5px;"><i class="fa fa-download"></i> Unduh PDF</a>
                <a href="{{ url('/tambah_surat_jalan_masuk') }}">
                    <button class='btn bg-navy flat'><span class='fa fa-file'></span> Surat Jalan Masuk</button>
                </a>
                <a href="{{ url('/sales_order') }}" class="pull-right" style="padding:0px">
                    <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                </a>
            </div>
            <div class="box-body">
                <div class="area">
                    <section id="header">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="title"><span style="border-bottom: 1px solid black;">{{ strtoupper($judul) }}</span></div>
                                <div class="company">
                                    {{ strtoupper($perusahaan->nama) }} <br>
                                    {{ $perusahaan->alamat }} {{ $kota_perusahaan }} <br>
                                    Telp. {{ $perusahaan->telp }} | {{ $perusahaan->telp2 }} <br>
                                    E. {{ $perusahaan->email }}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <table class="detail">
                                    <tr>
                                        <td class="title-detail">No. SO</td>
                                        <td class="titik">&nbsp;:&nbsp;</td>
                                        <td class="data-detail">{{ $so_header->no_sales_order }}</td>
                                    </tr>
                                    <tr>
                                        <td class="title-detail">Tanggal</td>
                                        <td class="titik">&nbsp;:&nbsp;</td>
                                        <td class="data-detail">
                                            <?php $tanggal = new DateTime($so_header->tanggal); 
                                            echo $tanggal->format('d/m/Y G:i:s');
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="title-detail">Pelanggan</td>
                                        <td class="titik">&nbsp;:&nbsp;</td>
                                        <td class="data-detail user">{{ $pelanggan['nama'] }}</td>
                                    </tr>
                                    <tr>
                                        <td class="title-detail" style="vertical-align: top;">Alamat </td>
                                        <td class="titik" style="vertical-align: top;">&nbsp;:&nbsp;</td>
                                        <td class="data-detail user">{{ $pelanggan['alamat'] }}<br>
                                        Telp. {{ $pelanggan['telp'] }}<br>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-4">
                                <div class="detail">
                                    <b>Barang dikirim ke: </b><br>
                                    <table width="100%">
                                        <tr>
                                            <td class="title-detail" style="vertical-align: top;">Alamat </td>
                                            <td class="titik" style="vertical-align: top;">&nbsp;:&nbsp;</td>
                                            <td class="data-detail user">{{ $so_alamat_pengiriman->alamat }} {{ ucwords(strtolower($so_alamat_pengiriman->kota->nama)) }}<br>
                                            T. {{ $so_alamat_pengiriman->telp }}<br>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section id="table">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th width="5%">No.</th>
                                    <th width="30%">Nama Produk</th>
                                    <th width="15%">Satuan</th>
                                    <th width="10%">Jumlah</th>
                                    <th width="20%">Harga</th>
                                    <th width="20%">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $jumlah_item=0; $total_akhir=0; ?>
                                @foreach($so_detail as $i => $val)
                                <tr>
                                    <td align="center">{{ $i+1 }}</td>
                                    <td>
                                        @if($val->jenis_barang_id == 1)
                                        {{ $val->produk->nama }}
                                        @elseif($val->jenis_barang_id == 2)
                                        {{ $val->hadiah->nama }}
                                        @elseif($val->jenis_barang_id == 3)
                                        {{ $val->paket->nama }}
                                        @endif
                                    </td>
                                    <td align="center">
                                        @if($val->jenis_barang_id == 1)
                                        {{ $val->produk->satuan }}
                                        @elseif($val->jenis_barang_id == 2)
                                        {{ $val->hadiah->satuan }}
                                        @elseif($val->jenis_barang_id == 3)
                                        PKT
                                        @endif
                                    </td>
                                    <td align="center">{{ $val->jumlah }}</td>
                                    <td align="right" style="padding-right: 40px;">{{ rupiah2($val->harga) }}</td>
                                    <td align="right" style="padding-right: 40px;">{{ rupiah2($val->jumlah*$val->harga) }}</td>
                                </tr>
                                <?php $jumlah_item=$jumlah_item+$val->jumlah; 
                                    $total_akhir = $total_akhir+ ($val->jumlah*$val->harga);
                                ?>
                                @endforeach
                            </tbody>
                        </table>
                    </section>
                    <section id="footer">
                        <div class="row">
                            <div class="col-md-5" style="margin-bottom: 30px;">
                                <table style="margin-top: 60px;">
                                    <tr style="vertical-align: text-top;">
                                        <td class="title-detail">Keterangan</td>
                                        <td class="titik">&nbsp;:&nbsp;</td>
                                        <td class="data-detail">
                                        {{ $so_header->catatan }}
                                        <br><br></td>
                                    <tr style="vertical-align: text-top;">
                                        <td class="title-detail">Terbilang</td>
                                        <td class="titik" style="vertical-align: top;">&nbsp;:&nbsp;</td>
                                        <td class="data-detail">{{ dibaca($so_header->total_tagihan) }} Rupiah</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-7">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <table style="margin-bottom: 50px;" width="62%">
                                            <tr>
                                                <td class="title-detail keterangan" width="40%" align="right">Jml Item</td>
                                                <td class="titik keterangan" width="7%" align="center">&nbsp;:&nbsp;</td>
                                                <td class="data-detail keterangan" width="30%">{{ $jumlah_item }}</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-md-6" style="padding-right: 25px;">
                                        <table>
                                            <tr>
                                                <td class="title-detail price" style="text-align: left;" width="160px;">Sub Total</td>
                                                <td class="titik price" style="text-align: center;" width="100px;">&nbsp;:&nbsp;</td>
                                                <td class="data-detail right price">{{ rupiah2($total_akhir) }}</td>
                                            </tr>
                                            @if($so_header->ppn > 0)
                                            <tr>
                                                <td class="title-detail price" style="text-align: left;" width="160px;">PPN {{ $so_header->ppn }}%</td>
                                                <td class="titik" style="text-align: center;" width="100px;">&nbsp;:&nbsp;</td>
                                                <td class="data-detail right price">{{ rupiah2($total_akhir*$so_header->ppn/100) }}</td>
                                            </tr>
                                            @endif
                                            @if(!empty($so_voucher))
                                            @foreach($so_voucher as $val)
                                            <tr>
                                                <td class="title-detail price" style="text-align: left;" width="160px;">Voucher</td>
                                                <td class="titik" style="text-align: center;" width="100px;">&nbsp;:&nbsp;</td>
                                                <td class="data-detail right price">{{ rupiah2($val->nominal) }}</td>
                                            </tr>
                                            @endforeach
                                            @endif
                                            <tr>
                                                <td class="title-detail price" style="text-align: left;" width="160px;">Total Akhir</td>
                                                <td class="titik price" style="text-align: center;" width="100px;">&nbsp;:&nbsp;</td>
                                                <td class="data-detail right price">{{ rupiah2($so_header->total_tagihan) }}</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-md-12" style="margin-top: 30px;">
                                        <div class="col-md-offset-4 col-md-4">
                                            <div style="margin-top: 20px;">
                                                <div style="text-align: center">
                                                    <strong>Penerima</strong>
                                                </div>
                                            </div>
                                            <div style="margin-top: 100px;">
                                                <div style="text-align: center">
                                                    <strong>(.........................)</strong>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div style="margin-top: 20px;">
                                                <div style="text-align: center">
                                                    <strong>Hormat Kami</strong>
                                                </div>
                                            </div>
                                            <div style="margin-top: 100px;">
                                                <div style="text-align: center">
                                                    <strong>(.........................)</strong>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php echo $print_time->format('d/m/Y G:i:s'); ?>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
@stop