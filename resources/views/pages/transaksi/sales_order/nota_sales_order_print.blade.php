<link rel="stylesheet" type="text/css" href="{{ URL::to('/css/nota.css') }}">
@include('includes.base_function')
<?php
    function rupiah2($nominal)
    {
        $rupiah =  number_format($nominal,0, ",",".");
        $rupiah = $rupiah.',00';
        return $rupiah;
    }
?>
<style type="text/css">
    [class*="col-"] {
        float: left;
    }
    pre {
        font-family: sans-serif;
        background-color: transparent;
        border: none;
        overflow-x: hidden;
        white-space: pre-wrap;       /* css-3 */
        white-space: -moz-pre-wrap;  /* Mozilla, since 1999 */
        white-space: -pre-wrap;      /* Opera 4-6 */
        white-space: -o-pre-wrap;    /* Opera 7 */
        word-wrap: break-word; 
        padding: 0px;
    }

    @font-face {
        font-family: mycalibri;
        src: url("{{ asset('fonts/Calibri.ttf') }}");
    }

    @font-face {
        font-family: mycalibri;
        src: url("{{ asset('fonts/Calibri-Bold.ttf') }}");
        font-weight: bold;
    }

    .area{
        font-family: mycalibri;
    }
</style>
<div class="area">
    <section id="header">
        <div>
            <div class="col-4">
                <div class="title"><span style="border-bottom: 1px solid black;">{{ strtoupper($judul) }}</span></div>
                <div class="company" style="margin-top: 20px;">
                    {{ strtoupper($perusahaan->nama) }} <br>
                    {{ $perusahaan->alamat }} {{ $kota_perusahaan }} <br>
                    Telp. {{ $perusahaan->telp }} | {{ $perusahaan->telp2 }} <br>
                    E. {{ $perusahaan->email }}
                </div>
            </div>
            <div class="col-4">
                <table class="detail" style="margin-bottom: 10px;">
                    <tr>
                        <td class="title-detail">No. SO</td>
                        <td class="titik">:</td>
                        <td class="data-detail">{{ $so_header->no_sales_order }}</td>
                    </tr>
                    <tr>
                        <td class="title-detail">Tanggal</td>
                        <td class="titik">:</td>
                        <td class="data-detail">
                            <?php $tanggal = new DateTime($so_header->tanggal); 
                            echo $tanggal->format('d/m/Y G:i:s');
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="title-detail">Pelanggan</td>
                        <td class="titik">:</td>
                        <td class="data-detail user">{{ $pelanggan['nama'] }}</td>
                    </tr>
                    <tr>
                        <td class="title-detail" style="vertical-align: top;">Alamat </td>
                        <td class="titik" style="vertical-align: top;">:</td>
                        <td class="data-detail user">{{ $pelanggan['alamat'] }}<br>
                        Telp. {{ $pelanggan['telp'] }}<br>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-4">
                <div class="detail">
                    <b>Barang dikirim ke: </b><br>
                    <table width="100%">
                    <tr>
                        <td class="title-detail" style="vertical-align: top;">Alamat </td>
                        <td class="titik" style="vertical-align: top;">:</td>
                        <td class="data-detail user">{{ $so_alamat_pengiriman->alamat }} {{ ucwords(strtolower($so_alamat_pengiriman->kota->nama)) }}<br>
                        T. {{ $so_alamat_pengiriman->telp }}<br>
                        </td>
                    </tr>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <section id="table">
        <table class="table">
            <thead>
                <tr>
                    <th width="5%">No.</th>
                    <th width="30%">Nama Produk</th>
                    <th width="15%">Satuan</th>
                    <th width="10%">Jumlah</th>
                    <th width="20%">Harga</th>
                    <th width="20%">Total</th>
                </tr>
            </thead>
            <tbody>
                <?php $jumlah_item=0; $total_akhir=0; ?>
                @foreach($so_detail as $i => $val)
                <tr>
                    <td style="text-align: center;">{{ $i+1 }}</td>
                    <td>
                        @if($val->jenis_barang_id == 1)
                        {{ $val->produk->nama }}
                        @elseif($val->jenis_barang_id == 2)
                        {{ $val->hadiah->nama }}
                        @elseif($val->jenis_barang_id == 3)
                        {{ $val->paket->nama }}
                        @endif
                    </td>
                    <td style="text-align: center;">
                        @if($val->jenis_barang_id == 1)
                        {{ $val->produk->satuan }}
                        @elseif($val->jenis_barang_id == 2)
                        {{ $val->hadiah->satuan }}
                        @elseif($val->jenis_barang_id == 3)
                        PKT
                        @endif
                    </td>
                    <td style="text-align: center;">{{ $val->jumlah }}</td>
                    <td style="padding-right: 40px;text-align: right;">{{ rupiah2($val->harga) }}</td>
                    <td style="padding-right: 40px;text-align: right;">{{ rupiah2($val->jumlah*$val->harga) }}</td>
                </tr>
                <?php $jumlah_item=$jumlah_item+$val->jumlah; 
                    $total_akhir = $total_akhir+ ($val->jumlah*$val->harga);
                ?>
                @endforeach
            </tbody>
        </table>
    </section>
    <section id="footer">
        <div class="col-5">
            <table style="margin-top: 60px;">
                <tr style="vertical-align: text-top;">
                    <td class="title-detail">Keterangan</td>
                    <td class="titik">:</td>
                    <td class="data-detail">
                    {{ $so_header->catatan }}
                    <br><br><br></td>
                </tr>
                <tr style="vertical-align: text-top;">
                    <td class="title-detail">Terbilang</td>
                    <td class="titik" style="vertical-align: top;">:</td>
                    <td class="data-detail">{{ dibaca($so_header->total_tagihan) }} Rupiah</td>
                </tr>
            </table>
            <p style="position: absolute;bottom: 0;"><?php echo $print_time->format('d/m/Y G:i:s'); ?></p>
        </div>
        <div class="col-7">
            <div class="col-6">
                <table width="70%">
                    <tr>
                        <td class="title-detail keterangan" width="45%" style="text-align: right;">Jml Item</td>
                        <td class="titik keterangan" width="8%" style="text-align: center;">:</td>
                        <td class="data-detail keterangan" width="30%">{{ $jumlah_item }}</td>
                    </tr>
                </table>
            </div>
            <div class="col-6">
                <table width="86%">
                    <tbody>
                        <tr>
                            <td class="title-detail price" style="text-align: left;" width="40%;">Sub Total</td>
                            <td class="titik price" style="text-align: center;" width="10%">:</td>
                            <td class="data-detail right price" width="50%">{{ rupiah2($total_akhir) }}</td>
                        </tr>
                        @if($so_header->ppn > 0)
                        <tr>
                            <td class="title-detail price" style="text-align: left;">PPN {{ $so_header->ppn }}%</td>
                            <td class="titik" style="text-align: center;">:</td>
                            <td class="data-detail right price">{{ rupiah2($total_akhir*$so_header->ppn/100) }}</td>
                        </tr>
                        @endif
                        @if(!empty($so_voucher))
                        @foreach($so_voucher as $val)
                        <tr>
                            <td class="title-detail price" style="text-align: left;">Voucher</td>
                            <td class="titik" style="text-align: center;">:</td>
                            <td class="data-detail right price">{{ rupiah2($val->nominal) }}</td>
                        </tr>
                        @endforeach
                        @endif
                        <tr>
                            <td class="title-detail price" style="text-align: left;">Total Akhir</td>
                            <td class="titik price" style="text-align: center;">:</td>
                            <td class="data-detail right price">{{ rupiah2($so_header->total_tagihan) }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <br>
            <div class="col-12">
                <div class="col-4" style="margin-left: 33%">
                    <div style="margin-top: 20px;">
                        <div style="text-align: center">
                            <strong>Penerima</strong>
                        </div>
                    </div>
                    <div style="margin-top: 100px;">
                        <div style="text-align: center">
                            <strong>(.........................)</strong>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div style="margin-top: 20px;">
                        <div style="text-align: center">
                            <strong>Hormat Kami</strong>
                        </div>
                    </div>
                    <div style="margin-top: 100px;">
                        <div style="text-align: center">
                            <strong>(.........................)</strong>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function(){
          window.onload = function() { window.print(); }
      });
</script>