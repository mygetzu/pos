<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<script type="text/javascript">
    $(function () {
        $("#data-table-custom").DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": false,
            "info": true,
            "autoWidth": false
        });
        $("#mydatatables").DataTable();
        $("#mydatatables2").DataTable();
    });

    function info_teknisi(id, nama, email, alamat, hp) {
        document.getElementById('detail_teknisi_id').innerHTML = id;
        document.getElementById('detail_teknisi_nama').innerHTML = nama;
        document.getElementById('detail_teknisi_email').innerHTML = email;
        document.getElementById('detail_teknisi_alamat').innerHTML = alamat;
        document.getElementById('detail_teknisi_hp').innerHTML = hp;
    }
</script>
@extends('layouts.master_admin')
@section('content')
    @include('includes.base_function')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary flat">
                <div class="box-header with-border">
                    <div>
                        <a href="{{ url('/tambah_servis') }}">
                            <button class='btn bg-purple flat'><span class='fa fa-plus'></span> Tambah Pengajuan Servis
                            </button>
                        </a>
                    </div>
                </div>
                <div class="box-body">
                    @if(Session::has('message'))
                        <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                            <i class="fa fa-check"></i>
                            {{ Session::get('message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    <table id="mydatatables" class="table table-striped table-bordered">
                        <thead>
                        <th style="text-align:center">NO</th>
                        <th style="text-align:center">NO NOTA</th>
                        <th style="text-align:center">NAMA PELANGGAN</th>
                        <th style="text-align:center">TANGGAL DITERIMA</th>
                        <th style="text-align:center">TANGGAL SELESAI</th>
                        <th style="text-align:center">BIAYA</th>
                        <th style="text-align:center;">STATUS</th>
                        <th></th>
                        <th></th>
                        <th></th>
                        </thead>
                        <tbody>
                        <?php $i = 1; $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');?>
                        @foreach($service_order as $key => $val)
                            <tr>
                                <td align="center">{{ $i++ }}</td>
                                <td>{{ $val->no_nota }}</td>
                                <td>{{ DB::table('tmst_pelanggan')->where('id', $val->pelanggan_id)->value('nama') }}</td>
                                <td>
                                    <?php
                                    date_default_timezone_set("Asia/Jakarta");
                                    $tanggal = new DateTime($val->tanggal_diterima);
                                    ?>
                                    <?php echo $tanggal->format('d') . " " . $bulan[(int)$tanggal->format('m')] . " " . $tanggal->format('Y'); ?>
                                </td>
                                <td>
                                    @if(!empty($val->tanggal_selesai))
                                        <?php $tanggal_selesai = new DateTime($val->tanggal_selesai); echo $tanggal_selesai->format('d') . " " . $bulan[(int)$tanggal_selesai->format('m')] . " " . $tanggal_selesai->format('Y'); ?>
                                    @endif
                                </td>
                                <td>Rp. {{ number_format($service_header[$key]['harga_total'], 0, ',', '.') }}</td>
                                <td align="center" style="">
                                    @if($val->status_service == 'Belum')
                                        <p class="btn btn-danger btn-flat btn-block"
                                           style="margin: 3px;">{{ $val->status_service }}</p>
                                    @elseif($val->status_service == 'Proses')
                                        @if($val->flag_acc == 0 && $val->is_check == 0 && $service_header[$key]['is_submit'] == 0)
                                            <p class="btn btn-success btn-flat btn-block" style="margin: 3px;">Proses
                                                Diperiksa</p>
                                        @elseif($val->flag_acc == 0 && $service_header[$key]['harga_total'] == 0)
                                            <p class="btn btn-info btn-flat btn-block blink_text" style="margin: 3px;">Pengisian
                                                Biaya</p>
                                        @elseif($val->flag_acc == 0 && ($val->is_check == 0 || $val->is_check == 1))
                                            <p class="btn btn-success btn-flat btn-block" style="margin: 3px;">Proses
                                                ACC</p>
                                        @elseif($val->flag_acc == 1 && $val->is_check == 1)
                                            <p class="btn btn-success btn-flat btn-block" style="margin: 3px;">Proses
                                                Diperbaiki</p>
                                        @endif
                                    @elseif($val->status_service == 'Selesai')
                                        @if($val->tanggal_diambil == NULL)
                                            <p class="btn btn-success btn-flat btn-block"
                                               style="margin: 3px;">Selesai & Belum Diambil</p>
                                        @else
                                            <p class="btn btn-success btn-flat btn-block"
                                               style="margin: 3px;">Selesai & Sudah Diambil</p>
                                        @endif
                                    @endif
                                </td>
                                <td align="center">
                                    <a href="{{ url('/nota_service_order/'.$val->id) }}" class="btn btn-info flat"
                                       data-toggle="tooltip" title="Nota Servis" style="font-size: 15pt"><i
                                                class="fa fa-print"></i></a>
                                </td>
                                <td align="center">
                                    <a class="btn btn-info flat" href="{{ url('/detail_servis/'.$val->id) }}"
                                       data-toggle="tooltip" title="Detail Servis" style="font-size: 15pt">
                                        <span class="fa fa-info-circle"></span>
                                    </a>
                                </td>
                                <td align="center">
                                    @if($val->status_service == 'Belum')
                                        <a href="#"
                                           data-toggle="tooltip"
                                           data-placement="top"
                                           data-title="Isi biaya perbaikan">
                                            <button class="btn bg-olive btn-flat"
                                                    style="font-size: 15pt">
                                                <span class="fa fa-cog fa-spin"></span>
                                            </button>
                                        </a>
                                    @elseif($service_header[$key]['harga_total'] !== 0)
                                        @if($service_header[$key]['is_lunas'] == 0)
                                            <a href="#"
                                               data-toggle="tooltip"
                                               data-placement="top"
                                               data-title="Proses perbaikan">
                                                <button class="btn bg-olive btn-flat"
                                                        style="font-size: 15pt">
                                                    <span class="fa fa-spin fa-cog"></span>
                                                </button>
                                            </a>
                                        @else
                                            <a href="#"
                                               data-toggle="tooltip"
                                               data-placement="top"
                                               data-title="{{ ($service_header[$key]['tanggal_diambil'] == NULL ? 'Sudah selesai & Belum diambil' : 'Sudah selesai & Sudah diambil') }}">
                                                <button class="btn bg-olive btn-flat"
                                                        style="font-size: 15pt">
                                                    <span class="fa fa-check"></span>
                                                </button>
                                            </a>
                                        @endif
                                    @else
                                        <a href="{{ url('/servis_harga/'.$val->id) }}"
                                           data-toggle="tooltip"
                                           data-placement="top"
                                           data-title="Isi biaya perbaikan">
                                            <button class="btn bg-olive btn-flat"
                                                    style="font-size: 15pt">
                                                <span class="fa fa-money"></span>
                                            </button>
                                        </a>
                                    @endif
                                </td>
                                <td align="center">
                                    @if($val->teknisi_id == NULL)
                                        <a style="font-size: 15pt" data-toggle="modal" data-target="#pilih_teknisi">
                                            <button class="btn btn-info btn-flat"
                                                    onclick="set_service_id('{{ $val->id }}')"
                                                    data-toggle="tooltip"
                                                    data-placement="top"
                                                    data-title="Pilih teknisi"
                                                    style="font-size: 15pt">
                                                <span class="fa fa-user-plus"></span>
                                            </button>
                                        </a>
                                    @else
                                        <a data-toggle="modal" data-target="#info_teknisi_modal">
                                            <button class="btn btn-success btn-flat"
                                                    onclick="return info_teknisi('{{ $val->teknisi_id }}', '{{ $teknisi_data[$key]['nama'] }}', '{{ $teknisi_data[$key]['email'] }}', '{{ $teknisi_data[$key]['alamat'] }}', '{{ $teknisi_data[$key]['hp'] }}')"
                                                    data-toggle="tooltip"
                                                    data-placement="top"
                                                    data-title="Lihat info teknisi"
                                                    style="font-size: 15pt">
                                                <span class="fa fa-user"></span>
                                            </button>
                                        </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="pilih_teknisi" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="riwayatheader">Pilih Teknisi</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <table class="table table-striped table-bordered" id="mydatatables2">
                            <thead>
                            <th style="text-align:center">NO</th>
                            <th style="text-align:center">NAMA</th>
                            <th style="text-align:center">ALAMAT</th>
                            <th style="text-align:center"></th>
                            </thead>
                            <tbody>
                            <?php $i = 1; ?>
                            @foreach($teknisi as $val)
                                <tr>
                                    <form action="{{ url('/servis/update_teknisi') }}" method="POST" autocomplete="off">
                                        {{ csrf_field() }}
                                        <input name="service_order_id" id="service_order_id" type="hidden" value=""/>
                                        <input name="teknisi_id" value="{{ $val->id }}" type="hidden"/>
                                        <td align="center">{{ $i++ }}</td>
                                        <td>{{ $val->name }}</td>
                                        <td>{{ $val->alamat }}</td>
                                        <td align="center">
                                            <button class="btn bg-maroon btn-flat"
                                                    onclick="document.getElementById('loading-pilih-teknisi').style.display = 'block'"
                                                    type="submit"><span class="fa fa-check"></span>
                                                Pilih
                                            </button>
                                        </td>
                                    </form>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div>
                <div class="overlay" id="loading-pilih-teknisi" style="display:none">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="info_teknisi_modal" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="riwayatheader">Info Teknisi</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-5">
                                <p>Nama Teknisi</p>
                                <p>Email Teknisi</p>
                                <p>Alamat Teknisi</p>
                                <p>No. HP</p>
                            </div>
                            <div class="col-md-7">
                                <p id="detail_teknisi_id"></p>
                                <p id="detail_teknisi_nama"></p>
                                <p id="detail_teknisi_email"></p>
                                <p id="detail_teknisi_alamat"></p>
                                <p id="detail_teknisi_hp"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script type="text/javascript">
    function set_service_id(service_id) {
        document.getElementById("service_order_id").value = service_id;
    }

    function submit_supplier(id) {
        document.getElementById("supplier_id").value = id;
        document.getElementById("nama_supplier").value = nama;
        document.getElementById("btn_pilih_produk").style.display = 'block';

        var url = "{{ url('/get_po_produk') }}";

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })

        $.ajax({
            type: "POST",
            url: url,
            data: {id: id},
            success: function (data) {
                $("#po_produk").html(data);
                document.getElementById("admin_cart").innerHTML = "";
                document.getElementById("checkout").style.display = 'none';
            },
            error: function (data) {
                alert('terjadi kesalahan pengambilan data');
            }
        });
    }

    function get_device(service, customer, verification_code, status) {
        swal({
            title: "Apakah pelanggan ingin mengambil perangkat ini?",
            text: "Setelah Anda menyetujui pesan ini, pelanggan Akan mendapatkan link konfirmasi via email yang harus diklik.",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            confirmButtonText: "Ya, pelanggan ingin mengambil!",
            cancelButtonText: "Tidak!",
            confirmButtonColor: "#ec6c62"
        }, function () {
            window.location.href = '<?php echo (string)url('/')?>/servis_ambil?service=' + service + '&customer=' + customer + '&verification_code=' + verification_code + '&status=get_device';
        });
    }
</script>