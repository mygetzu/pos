<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <a href="{{ url('/tambah_surat_jalan_keluar') }}">
                  <button class='btn bg-purple flat'><span class='fa fa-plus'></span> Tambah Surat Jalan Keluar</button>
                </a>
            </div>
            <div class="box-body">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <table id="mydatatables" class="table table-striped table-bordered" style="margin:0px">
                    <thead>
                        <th style="text-align:center">NO</th>
                        <th style="text-align:center">NO SURAT JALAN</th>
                        <th style="text-align:center">TANGGAL</th>
                        <th style="text-align:center">NO SALES ORDER</th>
                        <th style="text-align:center">PELANGGAN</th>
                        <th style="text-align:center"></th>
                        <th style="text-align:center"></th>
                    </thead>
                    <tbody>
                        @foreach($sj_keluar_header as $key => $value)
                        <tr>
                            <td align="center">{{ $key+1 }}</td>
                            <td>{{ $value->no_surat_jalan }}</td>
                            <td>{{ $value->tanggal }}</td>
                            <td>{{ $value->so_header->no_sales_order }}</td>
                            <td>{{ $value->pelanggan->nama }}</td>
                            <td align="center">
                                <a href="{{ url('/nota_surat_jalan_keluar/'.$value->id) }}" class="btn btn-info flat" data-toggle="tooltip" title="Surat Jalan Keluar" style="font-size: 15pt"><i class="fa fa-print"></i></a>
                            </td>
                            <td align="center">
                                @if($value->flag_nota_jual == 0)
                                <button class="btn btn-danger flat" data-toggle="tooltip" title="Hapus" style="font-size: 15pt" onclick="hapus_surat_jalan_keluar('{{ $value->id }}')"><i class="fa fa-trash"></i></button>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
              </table>
            </div>
            <div class="overlay" id="loading" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<form action="{{ url('/hapus_surat_jalan_keluar') }}" method="post" id="form_hapus_surat_jalan_keluar">
    {{ csrf_field() }}
    <input type="hidden" name="id" id="hapus_surat_jalan_keluar_id">
</form>

@stop
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script>
    $(function () {
        $("#mydatatables").DataTable();
    });

    function hapus_surat_jalan_keluar(id)
    {
        var cek = confirm('apakah anda yakin menghapus surat jalan keluar?');

        if(cek){
            document.getElementById('hapus_surat_jalan_keluar_id').value = id;
            document.getElementById('loading').style.display            = 'block';
            document.getElementById('form_hapus_surat_jalan_keluar').submit();
        }
    }
</script>