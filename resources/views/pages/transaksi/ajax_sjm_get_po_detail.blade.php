<?php
    function rupiah($nominal)
    {
        $rupiah =  number_format($nominal,0, ",",".");
        $rupiah = "Rp "  . $rupiah;
        return $rupiah;
    }
?>
<table class="table table-striped table-bordered" id="ajax_produk_tabless3">
    <thead>
        <tr>
            <th style="text-align:center">NO</th>
            <th style="text-align:center">NAMA PRODUK</th>
            <th style="text-align:center">HARGA SATUAN</th>
            <th style="text-align:center">JUMLAH</th>
            <th style="text-align:center">JUMLAH PESANAN TERKIRIM</th>
        </tr>
    </thead>
    <tbody>
        <?php $i=1; $total_pesanan = 0; ?>
        @foreach($po_detail as $val)
        <tr>
            <td align="center">{{ $i++ }}</td>
            <td>
                @if($val->jenis_barang_id == 1)
                {{ $val->produk->nama }}
                @elseif($val->jenis_barang_id == 2)
                {{ $val->hadiah->nama }}
                @endif
            </td>
            <td>{{ rupiah($val->harga) }}</td>
            <td align="center">{{ $val->jumlah }}<?php $total_pesanan=$total_pesanan+$val->jumlah; ?></td>
            <td align="center">{{ $val->jumlah_terkirim }}<?php $total_pesanan=$total_pesanan-$val->jumlah_terkirim; ?></td>
        </tr>
        @endforeach
    </tbody>
</table>
<input type="hidden" id="ajax_total_pesanan" value="{{ $total_pesanan }}">
<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
  $(function () {
    $("#ajax_produk_tabless3").DataTable();
  });
</script>