<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <div>
                  <a href="{{ url('/tambah_voucher') }}">
                  <button class='btn bg-purple flat'><span class='fa fa-plus'></span> Tambah Voucher</button>
                </a>
                </div>
            </div>
            <div class="box-body">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <table id="mydatatables" class="table table-striped table-bordered">
                    <thead>
                        <th style="text-align:center">NO</th>
                        <th style="text-align:center">KODE VOUCHER</th>
                        <th style="text-align:center">NOMINAL</th>
                        <th style="text-align:center">AWAL PERIODE</th>
                        <th style="text-align:center">AKHIR PERIODE</th>
                        <th></th>
                    </thead>
                    <tbody>
                        <?php $i=1; $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'); ?>
                        @foreach($voucher as $val)
                        <tr>
                            <td align="center">{{ $i++ }}</td>
                            <td>{{ $val->kode }}
                                <?php
                                    date_default_timezone_set("Asia/Jakarta");
                                    $date1 = new DateTime($val->awal_periode);
                                    $date2 = new DateTime($val->akhir_periode);
                                    $curr_date = new DateTime();
                                    $curr_dates = $curr_date->format('Y-m-d');
                                    $date1s = $date1->format('Y-m-d');
                                    $date2s = $date2->format('Y-m-d');
                                    if( $curr_dates >= $date1s && $curr_dates <= $date2s && $val->is_aktif == '1'){
                                        echo "<span class='label label-success flat'>Aktif</span>";
                                    }
                                    else
                                    {
                                        echo "<span class='label label-danger flat'>Tidak Aktif</span>";
                                    }
                                ?>
                            </td>
                            <td>{{ rupiah($val->nominal) }}</td>
                            <td align="center"><?php echo $date1->format('d')." ".$bulan[(int)$date1->format('m')]." ".$date1->format('Y'); ?></td>
                            <td align="center"><?php echo $date2->format('d')." ".$bulan[(int)$date2->format('m')]." ".$date2->format('Y'); ?></td>
                            <td align="center">
                                <a class="btn btn-info flat" href="{{ url('/detail_voucher/'.$val->id) }}" data-toggle="tooltip" title="detail" style="font-size: 15pt"><span class="fa fa-info-circle"></span></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script>
  $(function () {
    $("#mydatatables").DataTable();
  });
</script>