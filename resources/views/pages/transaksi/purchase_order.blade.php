<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <div>
                  <a href="{{ url('/tambah_purchase_order') }}">
                  <button class='btn bg-purple flat'><span class='fa fa-plus'></span> Tambah Purchase Order</button>
                </a>
                </div>
            </div>
            <div class="box-body">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <table id="mydatatables" class="table table-striped table-bordered" style="margin:0px">
                  <thead>
                      <th style="text-align:center">NO</th>
                      <th style="text-align:center">NO NOTA</th>
                      <th style="text-align:center">TANGGAL</th>
                      <th style="text-align:center">SUPPLIER</th>
                      <th style="text-align:center">TOTAL TAGIHAN</th>
                      <th style="text-align:center"></th>
                      <th style="text-align:center"></th>
                  </thead>
                  <tbody>
                      <?php $i=1; $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'); ?>
                      @foreach($po_header as $val)
                      <tr>
                          <td align="center">{{ $i++ }}</td>
                          <td align="center">{{ $val->no_purchase_order }}</td>
                          <td><?php
                                  date_default_timezone_set("Asia/Jakarta");
                                  $tanggal = new DateTime($val->tanggal);
                                  $tanggals = $tanggal->format('Y-m-d');
                              ?>
                              <?php echo $tanggal->format('d')." ".$bulan[(int)$tanggal->format('m')]." ".$tanggal->format('Y')." "; ?>
                          </td>
                          <td align="center">{{ $val->supplier->nama }}</td>
                          <td>{{ rupiah($val->total_tagihan) }}</td>
                          <td align="center">
                                <a href="{{ url('/nota_purchase_order/'.$val->id) }}" class="btn btn-info flat" data-toggle="tooltip" title="Purchase Order" style="font-size: 15pt"><i class="fa fa-print"></i></a>
                            </td>
                          <td align="center">
                              <a class="btn btn-info flat" href="{{ url('/detail_purchase_order/'.$val->id) }}" data-toggle="tooltip" title="detail" style="font-size: 15pt"><span class="fa fa-info-circle"></span></a>
                          </td>
                      </tr>
                      @endforeach
                  </tbody>
              </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="pengaturan_po" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content box">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="riwayatheader">Pengaturan PO</h4>
            </div>
            <form class="form-horizontal" action="{{ url('/pengaturan_po') }}" autocomplete="off" method="post">
            {{ csrf_field() }}
            <div class="modal-body">
                <div class="form-group">
                    <label class="control-label col-md-2">Alamat Pengiriman</label>
                    <div class="col-md-8">
                        <textarea class="form-control" name="alamat_pengiriman">@if(!empty($pengaturan_po)){{ $pengaturan_po->alamat_pengiriman }}@endif</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2">Syarat dan Ketentuan</label>
                    <div class="col-md-8">
                        <textarea class="form-control" name="syarat_ketentuan">@if(!empty($pengaturan_po)){{ $pengaturan_po->syarat_ketentuan }}@endif</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2">PPN</label>
                    <div class="col-md-2">
                        <div class="checkbox" >
                            <label >
                              <input name="checkbox_ppn" id="checkbox_ppn" type="checkbox" <?php if(!empty($pengaturan_po->ppn)){ echo "checked";  }?> value="checked"> Include PPN
                            </label>
                        </div>
                    </div>
                    <div class="col-md-2" id="display_ppn">
                        <input type="txt" name="ppn" class="form-control" onKeyPress="return numbersonly(this, event)" <?php if(!empty($pengaturan_po->ppn)){ ?> value='{{ $pengaturan_po->ppn }}' <?php }?>>
                    </div>
                    <div class="col-md-2" id="display_persen">
                    <label class="control-label">%</label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default flat pull-left"><span class="glyphicon glyphicon-ban-circle"></span> Batal</button>
                <button type="submit" class="btn btn-primary flat" onclick="document.getElementById('loading').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
            </div>
            </form>
            <div class="overlay" id="loading" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

@stop

<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script>
    $(function () {
        $("#mydatatables").DataTable();
    });
</script>
