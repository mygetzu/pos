<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <a href="{{ url('/tambah_pembayaran_pelanggan') }}">
                  <button class='btn bg-purple flat'><span class='fa fa-plus'></span> Tambah Pembayaran Pelanggan</button>
                </a>
            </div>
            <div class="box-body">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <table id="mydatatables" class="table table-striped table-bordered" style="margin:0px">
                    <thead>
                        <tr>
                            <th style="text-align:center">NO</th>
                            <th style="text-align:center">JENIS</th>
                            <th style="text-align:center">NO NOTA</th>
                            <th style="text-align:center">TANGGAL</th>
                            <th style="text-align:center">SUPPLIER</th>
                            <!-- <th style="text-align:center">TOTAL TAGIHAN</th> -->
                            <th style="text-align:center">TOTAL PEMBAYARAN</th>
                            <th style="text-align:center"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($pembayaran_pelanggan as $key => $value)
                        <tr>
                            <td align="center">{{ $key+1 }}</td>
                            <td>
                                @if($value->jenis == 'nota_jual')
                                Nota Jual
                                @elseif($value->jenis == 'retur_jual')
                                Retur Jual
                                @elseif($value->jenis == 'service_order')
                                Service Order
                                @elseif($value->jenis == 'transaksi_jasa')
                                Transaksi Jasa
                                @endif
                            </td>
                            <td>
                                @if($value->jenis == 'nota_jual')
                                {{ $value->nota_jual->no_nota }}
                                @elseif($value->jenis == 'retur_jual')
                                {{ $value->rj_header->no_nota }}
                                @elseif($value->jenis == 'service_order')
                                {{ $value->service_order->no_nota }}
                                @elseif($value->jenis == 'transaksi_jasa')
                                {{ $value->transaksi_jasa->no_nota }}
                                @endif
                            </td>
                            <td>{{ date('d-M-Y', strtotime($value->tanggal)) }}</td>
                            <td>{{ $value->pelanggan->nama }}</td>
                            <!-- <td>{{ rupiah($value->total_tagihan) }}</td> -->
                            <td>{{ rupiah($value->total_pembayaran) }}</td>
                            <td align="center">
                                @if(empty($value->no_jurnal))
                                <button class="btn btn-danger flat" data-toggle="tooltip" title="Hapus" style="font-size: 15pt" onclick="hapus_pembayaran_pelanggan('{{ $value->id }}')"><i class="fa fa-trash"></i></button>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
              </table>
            </div>
            <div class="overlay" id="loading" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<form action="{{ url('/hapus_bayar_penjualan') }}" method="post" id="form_hapus_bayar_penjualan">
    {{ csrf_field() }}
    <input type="hidden" name="id" id="hapus_bayar_penjualan_id">
</form>

@stop
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script>
    $(function () {
        $("#mydatatables").DataTable();
    });

    function hapus_pembayaran_pelanggan(id)
    {
        var cek = confirm('apakah anda yakin menghapus pembayaran?');

        if(cek){
            document.getElementById('hapus_bayar_penjualan_id').value   = id;
            document.getElementById('loading').style.display            = 'block';
            document.getElementById('form_hapus_bayar_penjualan').submit();
        }
    }
</script><link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">