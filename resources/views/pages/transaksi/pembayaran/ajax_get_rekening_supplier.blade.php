<table class="table table-striped table-bordered" id="ajax_produk_tabless">
    <thead>
        <tr>
            <th style="text-align:center">NO</th>
            <th style="text-align:center">NAMA BANK</th>
            <th style="text-align:center">NO REKENING</th>
            <th style="text-align:center">ATAS NAMA</th>
            <th style="text-align:center">MATA UANG</th>
            <th style="text-align:center"></th>
        </tr>
    </thead>
    <tbody>
        @foreach($rekening_supplier as $key => $value)
        <tr>
            <td align="center">{{ $key+1 }}</td>
            <td>{{ $value->nama_bank }}</td>
            <td>{{ $value->nomor_rekening }}</td>
            <td>{{ $value->atas_nama }}</td>
            <td>{{ $value->mata_uang }}</td>
            <td>
                <button class="btn bg-maroon flat" data-dismiss='modal' onclick="pilih_no_rekening('{{ $value->nomor_rekening }}', '{{ $value->nama_bank }}')" ><span class="fa fa-check"></span> Pilih</button>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>