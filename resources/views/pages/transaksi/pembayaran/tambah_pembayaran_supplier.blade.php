@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<input type="hidden" name="jumlah_pesanan" id="jumlah_pesanan" value="">
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <a href="{{ url('/pembayaran_supplier') }}" class="pull-right" style="padding:0px">
                    <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                </a>
            </div>
            <div class="box-body">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                    <i class="fa fa-check"></i>
                    {{ Session::get('message') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                <form class="form-horizontal" action="{{ url('/do_pembayaran_supplier') }}" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="nota_id" id="nota_id">
                    <input type="hidden" name="jenis_id" id="jenis_id">
                    <h3>Informasi Transaksi</h3>
                    <hr>
                    <div class="form-group">
                        <label class="control-label col-md-2">Nomor Nota</label>
                        <div class="col-md-4">
                            <input type="text" id="display_nota_beli" class="form-control" disabled="true" value="">
                        </div>
                        <div class="col-md-2">
                            <a data-toggle='modal' data-target='#pilih_nota_beli'>
                                <button class="btn bg-maroon flat">
                                    <span class="fa fa-check"></span> Pilih Nota Beli
                                </button>
                            </a>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2">Supplier</label>
                        <div class="col-md-8">
                            <input type="text" id="display_supplier" class="form-control" disabled="true">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2">Tagihan</label>
                        <div class="col-md-4">
                            <input type="text" id="display_total_harga" class="form-control" disabled="true">
                        </div>
                    </div>
                    <div class="form-group" id="div_ppn" style="display: none;">
                        <label class="control-label col-md-2">PPN <span id="display_ppn"></span></label>
                        <div class="col-md-4">
                            <input type="text" id="display_ppn_harga" class="form-control" disabled="true">
                        </div>
                    </div>
                    <div class="form-group" id="div_diskon" style="display: none;">
                        <label class="control-label col-md-2">Diskon</label>
                        <div class="col-md-4">
                            <input type="text" id="display_total_diskon" class="form-control" disabled="true">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2">Total Yang Harus Dibayar</label>
                        <div class="col-md-8">
                            <input type="text" id="display_total_bayar" class="form-control" disabled="true">
                        </div>
                    </div>
                    <br>
                    <h3>Informasi Pembayaran</h3>
                    <hr>
                    <div class="form-group">
                        <label class="control-label col-md-2">No. Invoice Supplier</label>
                        <div class="col-md-8">
                            <input name="no_inv_sup" type="text" class="form-control" placeholder="No Invoice Supplier" style="text-transform:uppercase"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2">Tanggal Invoice Supplier</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="tanggal_inv" data-date-format="dd-mm-yyyy" name="tanggal_inv" value="{{ $tanggal_inv }}" placeholder="Masukkan tanggal invoice suplier"/>
                            </div>
                            @if ($errors->has('tanggal_inv'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('tanggal_inv') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2">Tanggal Pembayaran</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="tanggal" data-date-format="dd-mm-yyyy" name="tanggal" value="{{ $tanggal }}" placeholder="Masukkan tanggal jatuh tempo">
                            </div>
                            @if ($errors->has('tanggal'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('tanggal') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2">Tunai</label>
                        <div class="col-md-3">
                            <input type="text" class="form-control" name="tunai" placeholder="masukkan tunai" id="tunai" data-thousands="." data-decimal="," style="text-align:right" value="{{ old('tunai') }}" />
                        </div>
                        <div class="col-md-2">
                            <label class="control-label">(IDR)</label>
                        </div>
                    </div>
                    <div id="form_pembayaran_non_tunai1"></div>
                    <input type="hidden" name="jumlah_pembayaran_non_tunai" id="jumlah_pembayaran_non_tunai" value="0">
                    <div class="form-group">
                        <div class="col-md-2 col-md-offset-2">
                            <button class="btn btn-primary flat" type="button" onclick="tambah_pembayaran_non_tunai()"><span class="fa fa-plus"></span> Tambah Pembayaran Non Tunai</button>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2">Keterangan</label>
                        <div class="col-md-8">
                            <textarea class="form-control" name="keterangan"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-2">
                            <button class="btn btn-primary flat" onclick="document.getElementById('loading').style.display = 'block'"><span class="fa fa-floppy-o"></span> Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="overlay" id="loading" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="indeks_pembayaran_target">

<div class="modal fade" id="pilih_nota_beli" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="riwayatheader">Pilih Nota Beli</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered" id="produk_table">
                    <thead>
                        <tr>
                            <th style="text-align: center;">NO</th>
                            <th style="text-align: center;">JENIS NOTA</th>
                            <th style="text-align: center;">NOMOR NOTA</th>
                            <th style="text-align: center;">NAMA SUPPLIER</th>
                            <th style="text-align: center;">TANGGAL</th>
                            <th style="text-align: center;"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php for($i=0; $i < count($nota_beli); $i++) { ?>
                        <tr>
                            <td align="center">{{ $i+1 }}</td>
                            <td>{{ $nota_beli[$i]['jenis'] }}</td>
                            <td>{{ $nota_beli[$i]['no_nota'] }}</td>
                            <td>{{ $nota_beli[$i]['supplier'] }}</td>
                            <td align="center">{{ $nota_beli[$i]['tanggal'] }}</td>
                            <td align="center">
                                <?php 
                                    $id             = $nota_beli[$i]['id']; 
                                    $jenis_id       = $nota_beli[$i]['jenis_id']; 
                                    $no_nota        = $nota_beli[$i]['no_nota'];
                                    $supplier       = $nota_beli[$i]['supplier']; 
                                    $supplier_id    = $nota_beli[$i]['supplier_id']; 
                                    $total_harga    = $nota_beli[$i]['total_harga'];
                                    $ppn            = $nota_beli[$i]['ppn'];  
                                    $total_diskon   = $nota_beli[$i]['total_diskon']; 
                                    $total_tagihan  = $nota_beli[$i]['total_tagihan'];
                                ?>
                                <button class="btn bg-maroon flat" data-dismiss='modal' onclick="pilih_nota_beli('{{ $id }}', '{{ $jenis_id }}', '{{ $no_nota }}', '{{ $supplier }}', '{{ $supplier_id }}', '{{ $total_harga }}', '{{ $ppn }}', '{{ $total_diskon }}', '{{ $total_tagihan }}')" ><span class="fa fa-check"></span> Pilih</button>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="pilih_no_rekening" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="riwayatheader">Pilih No Rekening Supplier</h4>
            </div>
            <div class="modal-body">
                <div id="content_rekening_supplier"></div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="tambah_pembayaran_non_tunai" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="riwayatheader">Pembayaran Non Tunai</h4>
            </div>
            <div class="modal-body form-horizontal">
                <div class="form-group">
                    <label class="control-label col-md-2">Metode</label>
                </div>
            </div>
        </div>
    </div>
</div>

<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ url('/js/jquery.maskMoney.js') }}" type="text/javascript"></script>
<script>
    $(function () {
        $("#produk_table").DataTable();

        $('#tanggal').datepicker({
            autoclose: true,
            todayHighlight: 1,
        });
        $('#tanggal_inv').datepicker({
            autoclose: true,
            todayHighlight: 1,
        });

        $('#metode_pembayaran').change(function() {
            var metode_pembayaran = document.getElementById('metode_pembayaran').value;
            m_bayar = metode_pembayaran.split("-");
            if(m_bayar[1] == 'tunai'){
                document.getElementById('div_no_rekening').style.display = 'none';
            }
            else{
                document.getElementById('div_no_rekening').style.display = 'block';
            }
        });

        $("#tunai").maskMoney({precision:0});
    });

    function pilih_nota_beli(id, jenis_id, no_nota, supplier, supplier_id, total_harga, ppn, total_diskon, total_tagihan)
    {
        document.getElementById('nota_id').value                = id;
        document.getElementById('jenis_id').value               = jenis_id;
        document.getElementById('display_nota_beli').value      = no_nota;
        document.getElementById('display_supplier').value       = supplier;
        document.getElementById('display_total_harga').value    = rupiah(total_harga);
        document.getElementById('div_ppn').style.display        = 'none';
        document.getElementById('div_diskon').style.display     = 'none';

        total_harga     = parseInt(total_harga);
        total_diskon    = parseInt(total_diskon);
        ppn             = parseInt(ppn);
        total_tagihan   = parseInt(total_tagihan);

        if(total_diskon > 0){
            document.getElementById('div_diskon').style.display     = 'block';
            document.getElementById('display_total_diskon').value   = rupiah(total_diskon);
        }

        if(ppn > 0){
            document.getElementById('div_ppn').style.display        = 'block';
            document.getElementById('display_ppn').innerHTML        = ppn+"%";
            document.getElementById('display_ppn_harga').value      = rupiah(total_harga*ppn/100);//nantinya pake total bayar
        }

        document.getElementById('display_total_bayar').value   = rupiah(total_tagihan);

        //get rekening supplier
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })

        var url = "{{ url('/get_rekening_supplier') }}";

        $.ajax({
            type: "POST",
            url: url,
            data: { supplier_id : supplier_id },
            success: function (data) {
                $('#content_rekening_supplier').html(data);
            },
            error: function (data) {
                alert('ooo');
            }
        });
    }

    function rupiah(nominal)
    {
        //rumus konversi rupiah
        nominal = "Rp "+nominal.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
        return nominal;
    }

    function pilih_no_rekening(no_rekening, bank)
    {
        indeks = document.getElementById('indeks_pembayaran_target').value

        document.getElementById('no_rekening'.concat(indeks)).value = no_rekening;
        document.getElementById('bank'.concat(indeks)).value        = bank;
    }

    function button_pilih_no_rekening(indeks)
    {
        document.getElementById('indeks_pembayaran_target').value = indeks;
    }

    function hapus_pembayaran(indeks)
    {
        var cek = confirm('Apakah anda yakin menghapus pembayaran?');

        if(cek){
            document.getElementById("form_pembayaran_non_tunai".concat(indeks)).innerHTML = "";
            var jumlah_pembayaran_non_tunai = document.getElementById('jumlah_pembayaran_non_tunai').value;
            jumlah_pembayaran_non_tunai = parseInt(jumlah_pembayaran_non_tunai);
            jumlah_pembayaran_non_tunai--;

            document.getElementById("jumlah_pembayaran_non_tunai").value = jumlah_pembayaran_non_tunai;
        }
    }

    function tambah_pembayaran_non_tunai()
    { 
        var jumlah_pembayaran_non_tunai = document.getElementById('jumlah_pembayaran_non_tunai').value;
        jumlah_pembayaran_non_tunai = parseInt(jumlah_pembayaran_non_tunai);
        jumlah_pembayaran_non_tunai++;

        document.getElementById("jumlah_pembayaran_non_tunai").value = jumlah_pembayaran_non_tunai;
        form_pembayaran_non_tunai = jumlah_pembayaran_non_tunai+1;

        $("#form_pembayaran_non_tunai".concat(jumlah_pembayaran_non_tunai)).html('<div class="form-group">\
                        <label class="control-label col-md-2">Metode Pembayaran</label>\
                        <div class="col-md-4">\
                            <select class="form-control" name="metode_pembayaran'+jumlah_pembayaran_non_tunai+'">\
                                @foreach($metode_pembayaran as $value)\
                                    <option value="{{ $value->id }}">{{ $value->nama }}</option>\
                                @endforeach\
                            </select>\
                        </div>\
                        <div class="col-md-4">\
                            <button class="btn btn-danger flat" onclick="hapus_pembayaran('+jumlah_pembayaran_non_tunai+')"><span class="fa fa-trash-o"></span> Hapus Pembayaran</button>\
                        </div>\
                    </div>\
                    <div class="form-group" id="div_no_rekening">\
                        <label class="control-label col-md-2">No Rekening</label>\
                        <div class="col-md-3">\
                            <input type="text" name="no_rekening'+jumlah_pembayaran_non_tunai+'" id="no_rekening'+jumlah_pembayaran_non_tunai+'" class="form-control" placeholder="Masukkan Nomor Rekening">\
                        </div>\
                        <div class="col-md-2">\
                            <input type="text" name="bank'+jumlah_pembayaran_non_tunai+'" id="bank'+jumlah_pembayaran_non_tunai+'" class="form-control" placeholder="Masukkan Bank">\
                        </div>\
                        <div class="col-md-2">\
                            <a data-toggle="modal" data-target="#pilih_no_rekening" onclick="button_pilih_no_rekening('+jumlah_pembayaran_non_tunai+')">\
                                <button class="btn bg-maroon flat">\
                                    <span class="fa fa-check"></span> Pilih Rekening Supplier\
                                </button>\
                            </a>\
                        </div>\
                    </div>\
                    <div class="form-group">\
                        <label class="control-label col-md-2">Nominal</label>\
                        <div class="col-md-3">\
                            <input type="text" class="form-control" name="non_tunai'+jumlah_pembayaran_non_tunai+'" placeholder="masukkan nominal non tunai" id="non_tunai'+jumlah_pembayaran_non_tunai+'" data-thousands="." data-decimal="," style="text-align:right"/>\
                        </div>\
                        <div class="col-md-2">\
                            <label class="control-label">(IDR)</label>\
                        </div>\
                    </div>\
                    <div id="form_pembayaran_non_tunai'+form_pembayaran_non_tunai+'"></div>');

        $("#non_tunai".concat(jumlah_pembayaran_non_tunai)).maskMoney({precision:0})
        
    }
</script>
@stop