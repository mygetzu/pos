<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <a href="{{ url('/tambah_nota_jual') }}">
                  <button class='btn bg-purple flat'><span class='fa fa-plus'></span> Tambah Nota Jual</button>
                </a>
            </div>
            <div class="box-body">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <table id="mydatatables" class="table table-striped table-bordered" style="margin:0px">
                    <thead>
                        <th style="text-align:center">NO</th>
                        <th style="text-align:center">NO NOTA</th>
                        <th style="text-align:center">TANGGAL</th>
                        <th style="text-align:center">PELANGGAN</th>
                        <th style="text-align:center">STATUS</th>
                        <th style="text-align:center"></th>
                        <th style="text-align:center"></th>
                    </thead>
                    <tbody>
                        @foreach($nota_jual as $key => $value)
                        <tr>
                            <td align="center">{{ $key+1 }}</td>
                            <td>{{ $value->no_nota }}</td>
                            <td>{{ $value->tanggal }}</td>
                            <td>{{ $value->pelanggan->nama }}</td>
                            <td align="center">
                                @if($value->is_lunas == 1)
                                    <span class="label label-success">LUNAS</span>
                                @else
                                    <span class="label label-danger">BELUM LUNAS</span><br>
                                    <p style="margin-top: 5px">Jatuh Tempo : {{ $value->jatuh_tempo }}</p>
                                @endif
                            </td>
                            <td align="center">
                                <a href="{{ url('/invoice_nota_jual/'.$value->id) }}" class="btn btn-info flat" data-toggle="tooltip" title="Nota Jual" style="font-size: 15pt"><i class="fa fa-print"></i></a>
                            </td>
                            <td align="center">
                                @if($value->is_lunas == 0 AND empty($value->no_jurnal))
                                <button class="btn btn-danger flat" data-toggle="tooltip" title="Hapus" style="font-size: 15pt" onclick="hapus_nota_jual('{{ $value->id }}')"><i class="fa fa-trash"></i></button>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
              </table>
            </div>
            <div class="overlay" id="loading" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<form action="{{ url('/hapus_nota_jual') }}" method="post" id="form_hapus_nota_jual">
    {{ csrf_field() }}
    <input type="hidden" name="id" id="hapus_nota_jual_id">
</form>

@stop
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script>
    $(function () {
        $("#mydatatables").DataTable();
    });

    function hapus_nota_jual(id)
    {
        var cek = confirm('apakah anda yakin menghapus nota beli?');

        if(cek){
            document.getElementById('hapus_nota_jual_id').value = id;
            document.getElementById('loading').style.display    = 'block';
            document.getElementById('form_hapus_nota_jual').submit();
        }
    }
</script><link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">