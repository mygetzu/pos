<?php
    function rupiah($nominal)
    {
        $rupiah =  number_format($nominal,0, ",",".");
        $rupiah = "Rp "  . $rupiah;
        return $rupiah;
    }
?>
<table class="table table-striped table-bordered" id="ajax_produk_tabless">
    <thead>
        <tr>
            <th style="text-align:center">NO</th>
            <th style="text-align:center">NAMA GUDANG</th>
            <th style="text-align:center">NAMA PRODUK</th>
            <th style="text-align:center">SERIAL NUMBER</th>
            <th style="text-align:center">STOK</th>
            <th style="text-align:center"></th>
        </tr>
    </thead>
    <tbody>
        @foreach($produk_serial_number as $key => $value)
        <tr>
            <td align="center">{{ $key+1 }}</td>
            <td>{{ $value->gudang->nama }}</td>
            <td>{{ $value->produk->nama }}</td>
            <td>
                {{ $value->serial_number }}
            </td>
            <td align="center">
                {{ $value->stok }}
            </td>
            <td align="center">
                <button class="btn btn-primary flat" onclick="pilih_serial_number('{{ $value->serial_number }}')" data-dismiss="modal"><span class="fa fa-check-circle"></span> Pilih</button>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@foreach($produk_serial_number as $key => $value)
<input type="hidden" id="list_serial_number{{ $key }}" value="{{ $value->serial_number }}">
<input type="hidden" id="stok_serial_number{{ $key }}" value="{{ $value->stok }}">
@endforeach
<input type="hidden" id="total_serial_number" value="{{ count($produk_serial_number) }}">
<link rel="stylesheet" href="{{ asset('/adminlte/plugins/select2/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/select2/select2.full.min.js') }}"></script>
<script>
  $(function () {
    $("#ajax_produk_tabless").DataTable();
  });
</script>