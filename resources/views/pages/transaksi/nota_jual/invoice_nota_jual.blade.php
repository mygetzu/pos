@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<?php
    function rupiah2($nominal)
    {
        $rupiah =  number_format($nominal,0, ",",".");
        $rupiah = $rupiah.',00';
        return $rupiah;
    }
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <a href="{{ url('/invoice_nota_jual_print/'.$nota_jual->id) }}" target="_blank" class="btn bg-orange flat"><i class="fa fa-print"></i> Cetak</a>
                <a href="{{ url('/invoice_nota_jual_pdf/'.$nota_jual->id) }}" target="_blank" class="btn btn-info flat" style="margin-right: 5px;"><i class="fa fa-download"></i> Unduh PDF</a>
                <a href="{{ url('/nota_jual') }}" class="pull-right" style="padding:0px">
                    <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                </a>
            </div>
            <div class="box-body">
                <div class="area">
                    <section id="header">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="title"><span style="border-bottom: 1px solid black;">{{ strtoupper($judul) }}</span></div>
                                <div class="company">
                                    {{ strtoupper($perusahaan->nama) }} <br>
                                    {{ $perusahaan->alamat }} {{ $kota_perusahaan }} <br>
                                    Telp. {{ $perusahaan->telp }} | {{ $perusahaan->telp2 }} <br>
                                    E. {{ $perusahaan->email }}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <table class="detail">
                                    <tr>
                                        <td class="title-detail" width="20%">Nota Jual</td>
                                        <td class="titik">&nbsp;:&nbsp;</td>
                                        <td class="data-detail">{{ $nota_jual->no_nota }}</td>
                                    </tr>
                                    <tr>
                                        <td class="title-detail">Tanggal</td>
                                        <td class="titik">&nbsp;:&nbsp;</td>
                                        <td class="data-detail">
                                            <?php 
                                            $tanggal_nb = new DateTime($nota_jual->tanggal);
                                            echo $tanggal_nb->format('d/m/Y');
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="title-detail">Pelanggan</td>
                                        <td class="titik">&nbsp;:&nbsp;</td>
                                        <td class="data-detail user">{{ $pelanggan['nama'] }}</td>
                                    </tr>
                                    <tr>
                                        <td class="title-detail" style="vertical-align: top;">Alamat </td>
                                        <td class="titik" style="vertical-align: top;">&nbsp;:&nbsp;</td>
                                        <td class="data-detail user">{{ $pelanggan['alamat'] }}<br>
                                        Telp. {{ $pelanggan['telp'] }}<br>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-4">
                                <table class="detail">
                                    <tr>
                                        <td class="title-detail">Tempo Pembayaran</td>
                                        <td class="titik">&nbsp;:&nbsp;</td>
                                        <td class="data-detail user">{{ date('d-M-Y', strtotime($nota_jual->jatuh_tempo)) }}</td>
                                    </tr>
                                    <tr>
                                        <td class="title-detail">Surat Jalan</td>
                                        <td class="titik">&nbsp;:&nbsp;</td>
                                        <td class="data-detail user">{{ $sj_keluar_header->no_surat_jalan }}</td>
                                    </tr>
                                    <tr>
                                        <td class="title-detail">Tanggal</td>
                                        <td class="titik">&nbsp;:&nbsp;</td>
                                        <td class="data-detail">
                                            <?php 
                                            $tanggal_sj = new DateTime($sj_keluar_header->tanggal);
                                            echo $tanggal_sj->format('d/m/Y');
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="title-detail">No SO</td>
                                        <td class="titik">&nbsp;:&nbsp;</td>
                                        <td class="data-detail user">{{ $so_header->no_sales_order }}</td>
                                    </tr>
                                    <tr>
                                        <td class="title-detail">Tanggal</td>
                                        <td class="titik">&nbsp;:&nbsp;</td>
                                        <td class="data-detail">
                                            <?php 
                                            $tanggal_so = new DateTime($so_header->tanggal);
                                            echo $tanggal_so->format('d/m/Y');
                                            ?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </section>
                    <section id="table">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th style="width: 5%">No.</th>
                                    <th style="width: 35%">Nama Produk</th>
                                    <th style="width: 10%">Satuan</th>
                                    <th style="width: 10%">Jumlah</th>
                                    <th style="width: 20%">Harga</th>
                                    <th style="width: 20%">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $jumlah_item=0; $total_akhir=0; ?>
                                @foreach($produk as $i => $val)
                                <tr>
                                    <td align="center">{{ $i+1 }}</td>
                                    <td>{{ $produk[$i]['nama'] }}</td>
                                    <td align="center">{{ $produk[$i]['satuan'] }}</td>
                                    <td align="center">{{ $produk[$i]['jumlah'] }}</td>
                                    <td align="right" style="padding-right: 40px;">{{ rupiah2($produk[$i]['harga']) }}</td>
                                    <td align="right" style="padding-right: 40px;">{{ rupiah2($produk[$i]['jumlah']*$produk[$i]['harga']) }}</td>
                                </tr>
                                <?php $jumlah_item=$jumlah_item+$produk[$i]['jumlah']; 
                                    $total_akhir = $total_akhir+ ($produk[$i]['jumlah']*$produk[$i]['harga']);
                                ?>
                                @endforeach
                            </tbody>
                        </table>
                    </section>
                    <section id="footer">
                        <div class="row">
                            <div class="col-md-5">
                                <table>
                                    <tr style="vertical-align: text-top;">
                                        <td class="title-detail">Keterangan</td>
                                        <td class="titik">&nbsp;:&nbsp;</td>
                                        <td class="data-detail">
                                        {{ $so_header->catatan }}
                                        <br><br></td>
                                    <tr style="vertical-align: text-top;">
                                        <td class="title-detail">Terbilang</td>
                                        <td class="titik" style="vertical-align: top;">&nbsp;:&nbsp;</td>
                                        <td class="data-detail">{{ dibaca($nota_jual->total_tagihan) }} Rupiah<br><br></td>
                                    </tr>
                                    <tr style="vertical-align: text-top;">
                                        <td class="title-detail">Syarat & Ketentuan</td>
                                        <td class="titik" style="vertical-align: top;">&nbsp;:&nbsp;</td>
                                        <td class="data-detail">{{ $so_header->syarat_ketentuan }}</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-7">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <table width="48%">
                                            <tr style="text-align: center;">
                                                <td class="title-detail keterangan" width="40%">Jml Item</td>
                                                <td class="titik keterangan" width="7%">&nbsp;:&nbsp;</td>
                                                <td class="data-detail keterangan" width="30%">{{ $jumlah_item }}</td>
                                            </tr>
                                        </table>
                                        <div class="col-md-12" style="margin-top: 30px;">
                                            <div class="col-md-6">
                                                <div style="margin-top: 20px;">
                                                    <div style="text-align: center">
                                                        <strong>Penerima</strong>
                                                    </div>
                                                </div>
                                                <div style="margin-top: 100px;">
                                                    <div style="text-align: center">
                                                        <strong>(.........................)</strong>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div style="margin-top: 20px;">
                                                    <div style="text-align: center">
                                                        <strong>Hormat Kami</strong>
                                                    </div>
                                                </div>
                                                <div style="margin-top: 100px;">
                                                    <div style="text-align: center">
                                                        <strong>(.........................)</strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6" style="padding-right: 25px;">
                                        <table>
                                            <tr>
                                                <td class="title-detail price" style="text-align: left;" width="160px;">Sub Total</td>
                                                <td class="titik price" style="text-align: center;" width="100px;">&nbsp;:&nbsp;</td>
                                                <td class="data-detail right price">{{ rupiah2($total_akhir) }}</td>
                                            </tr>
                                            @if($nota_jual->ppn > 0)
                                            <tr>
                                                <td class="title-detail price" style="text-align: left;" width="160px;">PPN {{ $nota_jual->ppn }}%</td>
                                                <td class="titik" style="text-align: center;" width="100px;">&nbsp;:&nbsp;</td>
                                                <td class="data-detail right price">{{ rupiah2($total_akhir*$nota_jual->ppn/100) }}</td>
                                            </tr>
                                            @endif
                                            @if(!empty($nota_jual->total_voucher))
                                            <tr>
                                                <td class="title-detail price" style="text-align: left;" width="160px;">Voucher</td>
                                                <td class="titik" style="text-align: center;" width="100px;">&nbsp;:&nbsp;</td>
                                                <td class="data-detail right price">{{ rupiah2($nota_jual->total_voucher) }}</td>
                                            </tr>
                                            @endif
                                            <tr>
                                                <td class="title-detail price" style="text-align: left;" width="160px;">DP PO</td>
                                                <td class="titik price" style="text-align: center;" width="100px;">&nbsp;:&nbsp;</td>
                                                <td class="data-detail right price">{{ rupiah2($nota_jual->total_tagihan) }}</td>
                                            </tr>
                                            <tr>
                                                <td class="title-detail price" style="text-align: left;" width="160px;">Tunai</td>
                                                <td class="titik price" style="text-align: center;" width="100px;">&nbsp;:&nbsp;</td>
                                                <td class="data-detail right price">{{ rupiah2($nota_jual->total_tagihan) }}</td>
                                            </tr>
                                            <tr>
                                                <td class="title-detail price" style="text-align: left;" width="160px;">Kredit</td>
                                                <td class="titik price" style="text-align: center;" width="100px;">&nbsp;:&nbsp;</td>
                                                <td class="data-detail right price">{{ rupiah2($nota_jual->total_tagihan) }}</td>
                                            </tr>
                                            <tr>
                                                <td class="title-detail price" style="text-align: left;" width="160px;">K. Debit</td>
                                                <td class="titik price" style="text-align: center;" width="100px;">&nbsp;:&nbsp;</td>
                                                <td class="data-detail right price">{{ rupiah2($nota_jual->total_tagihan) }}</td>
                                            </tr>
                                            <tr>
                                                <td class="title-detail price" style="text-align: left;" width="160px;">K. Kredit</td>
                                                <td class="titik price" style="text-align: center;" width="100px;">&nbsp;:&nbsp;</td>
                                                <td class="data-detail right price">{{ rupiah2($nota_jual->total_tagihan) }}</td>
                                            </tr>
                                            <tr>
                                                <td class="title-detail price" style="text-align: left;" width="160px;">Kembali</td>
                                                <td class="titik price" style="text-align: center;" width="100px;">&nbsp;:&nbsp;</td>
                                                <td class="data-detail right price">{{ rupiah2($nota_jual->total_tagihan) }}</td>
                                            </tr>
                                        </table>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <?php echo $print_time->format('d/m/Y G:i:s'); ?>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ URL::to('/css/invoice.css') }}">
@stop