<link rel="stylesheet" type="text/css" href="{{ URL::to('/css/nota.css') }}">
@include('includes.base_function')
<?php
    function rupiah2($nominal)
    {
        $rupiah =  number_format($nominal,0, ",",".");
        $rupiah = $rupiah.',00';
        return $rupiah;
    }
?>
<style type="text/css">
    [class*="col-"] {
        float: left;
    }
    pre {
        font-family: sans-serif;
        background-color: transparent;
        border: none;
        overflow-x: hidden;
        white-space: pre-wrap;       /* css-3 */
        white-space: -moz-pre-wrap;  /* Mozilla, since 1999 */
        white-space: -pre-wrap;      /* Opera 4-6 */
        white-space: -o-pre-wrap;    /* Opera 7 */
        word-wrap: break-word; 
        padding: 0px;
    }

    @font-face {
        font-family: mycalibri;
        src: url("{{ asset('fonts/Calibri.ttf') }}");
    }

    @font-face {
        font-family: mycalibri;
        src: url("{{ asset('fonts/Calibri-Bold.ttf') }}");
        font-weight: bold;
    }

    .area{
        font-family: mycalibri;
    }
</style>
<div class="area">
    <section id="header">
        <div>
            <div class="col-4">
                <div class="title"><span style="border-bottom: 1px solid black;">{{ strtoupper($judul) }}</span></div>
                <div class="company" style="margin-top: 20px;">
                    {{ strtoupper($perusahaan->nama) }} <br>
                    {{ $perusahaan->alamat }} {{ $kota_perusahaan }} <br>
                    Telp. {{ $perusahaan->telp }} | {{ $perusahaan->telp2 }} <br>
                    E. {{ $perusahaan->email }}
                </div>
            </div>
            <div class="col-4">
                <table class="detail">
                    <tr>
                        <td class="title-detail" width="20%">Nota Jual</td>
                        <td class="titik">&nbsp;:&nbsp;</td>
                        <td class="data-detail">{{ $nota_jual->no_nota }}</td>
                    </tr>
                    <tr>
                        <td class="title-detail">Tanggal</td>
                        <td class="titik">&nbsp;:&nbsp;</td>
                        <td class="data-detail">
                            <?php 
                            $tanggal_nb = new DateTime($nota_jual->tanggal);
                            echo $tanggal_nb->format('d/m/Y');
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="title-detail">Pelanggan</td>
                        <td class="titik">&nbsp;:&nbsp;</td>
                        <td class="data-detail user">{{ $pelanggan['nama'] }}</td>
                    </tr>
                    <tr>
                        <td class="title-detail" style="vertical-align: top;">Alamat </td>
                        <td class="titik" style="vertical-align: top;">&nbsp;:&nbsp;</td>
                        <td class="data-detail user">{{ $pelanggan['alamat'] }}<br>
                        Telp. {{ $pelanggan['telp'] }}<br>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-4">
                <table class="detail">
                    <tr>
                        <td class="title-detail">Surat Jalan</td>
                        <td class="titik">&nbsp;:&nbsp;</td>
                        <td class="data-detail user">{{ $sj_keluar_header->no_surat_jalan }}</td>
                    </tr>
                    <tr>
                        <td class="title-detail">Tanggal</td>
                        <td class="titik">&nbsp;:&nbsp;</td>
                        <td class="data-detail">
                            <?php 
                            $tanggal_sj = new DateTime($sj_keluar_header->tanggal);
                            echo $tanggal_sj->format('d/m/Y');
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="title-detail">No SO</td>
                        <td class="titik">&nbsp;:&nbsp;</td>
                        <td class="data-detail user">{{ $so_header->no_sales_order }}</td>
                    </tr>
                    <tr>
                        <td class="title-detail">Tanggal</td>
                        <td class="titik">&nbsp;:&nbsp;</td>
                        <td class="data-detail">
                            <?php 
                            $tanggal_so = new DateTime($so_header->tanggal);
                            echo $tanggal_so->format('d/m/Y');
                            ?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </section>
    <section id="table">
        <table class="table">
            <thead>
                <tr>
                    <th style="width: 5%">No.</th>
                    <th style="width: 35%">Nama Produk</th>
                    <th style="width: 10%">Satuan</th>
                    <th style="width: 10%">Jumlah</th>
                    <th style="width: 20%">Harga</th>
                    <th style="width: 20%">Total</th>
                </tr>
            </thead>
            <tbody>
                <?php $jumlah_item=0; $total_akhir=0; ?>
                @foreach($produk as $i => $val)
                <tr>
                    <td style="text-align: center;">{{ $i+1 }}</td>
                    <td>{{ $produk[$i]['nama'] }}</td>
                    <td style="text-align: center;">{{ $produk[$i]['satuan'] }}</td>
                    <td style="text-align: center;">{{ $produk[$i]['jumlah'] }}</td>
                    <td style="padding-right: 40px;text-align: right;">{{ rupiah2($produk[$i]['harga']) }}</td>
                    <td style="padding-right: 40px;text-align: right;">{{ rupiah2($produk[$i]['jumlah']*$produk[$i]['harga']) }}</td>
                </tr>
                <?php $jumlah_item=$jumlah_item+$produk[$i]['jumlah']; 
                    $total_akhir = $total_akhir+ ($produk[$i]['jumlah']*$produk[$i]['harga']);
                ?>
                @endforeach
            </tbody>
        </table>
    </section>
    <section id="footer">
        <div>
            <div class="col-5">
                <table>
                    <tr style="vertical-align: text-top;">
                        <td class="title-detail">Keterangan</td>
                        <td class="titik">&nbsp;:&nbsp;</td>
                        <td class="data-detail">
                        {{ $so_header->catatan }}
                        <br><br></td>
                    <tr style="vertical-align: text-top;">
                        <td class="title-detail">Terbilang</td>
                        <td class="titik" style="vertical-align: top;">&nbsp;:&nbsp;</td>
                        <td class="data-detail">{{ dibaca($nota_jual->total_tagihan) }} Rupiah<br><br></td>
                    </tr>
                    <tr style="vertical-align: text-top;">
                        <td class="title-detail">Syarat & Ketentuan</td>
                        <td class="titik" style="vertical-align: top;">&nbsp;:&nbsp;</td>
                        <td class="data-detail">{{ $so_header->syarat_ketentuan }}</td>
                    </tr>
                </table>
                <p style="position: absolute;bottom: 0;"><?php echo $print_time->format('d/m/Y G:i:s'); ?></p>
            </div>
            <div class="col-7">
                <div class="col-12">
                    <div class="col-6">
                        <table width="58%">
                            <tr style="text-align: center;">
                                <td class="title-detail keterangan" width="40%">Jml Item</td>
                                <td class="titik keterangan" width="7%">&nbsp;:&nbsp;</td>
                                <td class="data-detail keterangan" width="30%">{{ $jumlah_item }}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-6" style="padding-right: 40px;">
                        <table>
                            <tr>
                                <td class="title-detail price" style="text-align: left;" width="160px;">Sub Total</td>
                                <td class="titik price" style="text-align: center;" width="100px;">&nbsp;:&nbsp;</td>
                                <td class="data-detail right price">{{ rupiah2($total_akhir) }}</td>
                            </tr>
                            @if($nota_jual->ppn > 0)
                            <tr>
                                <td class="title-detail price" style="text-align: left;" width="160px;">PPN {{ $nota_jual->ppn }}%</td>
                                <td class="titik" style="text-align: center;" width="100px;">&nbsp;:&nbsp;</td>
                                <td class="data-detail right price">{{ rupiah2($total_akhir*$nota_jual->ppn/100) }}</td>
                            </tr>
                            @endif
                            @if(!empty($nota_jual->total_voucher))
                            <tr>
                                <td class="title-detail price" style="text-align: left;" width="160px;">Diskon</td>
                                <td class="titik" style="text-align: center;" width="100px;">&nbsp;:&nbsp;</td>
                                <td class="data-detail right price">{{ rupiah2($nota_jual->total_voucher) }}</td>
                            </tr>
                            @endif
                            <tr>
                                <td class="title-detail price" style="text-align: left;" width="160px;">Total Akhir</td>
                                <td class="titik price" style="text-align: center;" width="100px;">&nbsp;:&nbsp;</td>
                                <td class="data-detail right price">{{ rupiah2($nota_jual->total_tagihan) }}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-12">
                        <div class="col-4" style="margin-left: 33%">
                            <div style="margin-top: 20px;">
                                <div style="text-align: center">
                                    <strong>Penerima</strong>
                                </div>
                            </div>
                            <div style="margin-top: 100px;">
                                <div style="text-align: center">
                                    <strong>(.........................)</strong>
                                </div>
                            </div>
                        </div>
                        <div class="col-4">
                            <div style="margin-top: 20px;">
                                <div style="text-align: center">
                                    <strong>Hormat Kami</strong>
                                </div>
                            </div>
                            <div style="margin-top: 100px;">
                                <div style="text-align: center">
                                    <strong>(.........................)</strong>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function(){
          window.onload = function() { window.print(); }
      });
</script>