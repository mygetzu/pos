<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<input type="hidden" name="base_path" id="base_path" value="{{ asset('/') }}">
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <a data-toggle='modal' data-target='#pengaturan_po' class="pull-right">
                    <button class="btn bg-navy flat"><span class="fa fa-gear"></span> Pengaturan</button>
                </a>
                <a data-toggle='modal' data-target='#ubah_format_no_invoice_po' >
                <button class="btn btn-warning flat pull-right" type="button" style="margin-right: 20px;"><span class="fa fa-edit"></span> Ubah Format No. PO</button>
                </a>
            </div>
            <div class="box-body">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                    <i class="fa fa-check"></i>
                    {{ Session::get('message') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/po_proses') }}" id="form_po" autocomplete="off">
                    <input type="hidden" name="jumlah_produk" id="jumlah_produk" value="0">
                    <input type="hidden" name="supplier_id" id="supplier_id" class="form-control">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('no_nota') ? ' has-error' : '' }}">
                                <label class="col-sm-2 control-label">No. PO*</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" placeholder="Masukkan No. PO" name="no_nota" value="{{ $no_invoice }}">
                                    @if ($errors->has('no_nota'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('no_nota') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                                <label class="col-sm-2 control-label">Nama</label>
                                <div class="col-sm-6">
                                    <input type="text" name="nama_supplier" id="nama_supplier" class="form-control" disabled="true">
                                </div>
                                <div class="com-md-3">
                                    <a data-toggle="modal" data-target="#pilih_supplier">
                                        <button class="btn bg-maroon flat"><span class="fa fa-user"></span> Pilih Supplier</button>
                                    </a>
                                </div>
                            </div>
                           <div class="form-group">
                                <label class="col-sm-2 control-label">Diskon</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="diskon" placeholder="Masukkan Diskon" id="diskon" data-thousands="." data-decimal="," style="text-align:right;" value="{{ old('diskon') }}" />
                                </div>
                                <label class="control-label col-sm-1">(IDR)</label>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Catatan</label>
                                <div class="col-sm-10">
                                    <textarea name="catatan" class="form-control" placeholder="Masukkan Catatan">{{ old('catatan') }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                                <label class="control-label col-sm-2">Alamat Pengiriman</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" name="alamat_pengiriman" style="height: 120px;">@if(!empty($pengaturan_po)){{ $pengaturan_po->alamat_pengiriman }}@endif</textarea>
                                </div>
                            </div>
                            <div class="form-group" style="display: none">
                                <label class="col-sm-2 control-label">Syarat Ketentuan</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" name="syarat_ketentuan">@if(!empty($pengaturan_po)){{ $pengaturan_po->syarat_ketentuan }}@endif</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">PPn</label>
                                <div class="col-sm-2">
                                    <input type="text" name="ppn" class="form-control" value="<?php if(!empty($pengaturan_po->ppn)){ echo $pengaturan_po->ppn; } ?>">
                                </div>
                                <label class="control-label col-md-1">%</label>
                            </div>
                        </div>
                    </div>
                </form>
                <hr>
                <h4>Transaksi<a data-toggle="modal" data-target="#pilih_produk" >
                        <button class="btn bg-maroon pull-right flat" style="display:none" id="btn_pilih_produk"><span class="fa fa-archive"></span> Pilih Produk</button></a>
                </h4>
                <hr>
                <div id="admin_cart">
                    
                </div>
                <hr>
                <div class="row" id="checkout" style="display:none">
                <div class="col-md-6">
                <button class="btn btn-warning flat" id="update" onclick="update()"><span class="glyphicon glyphicon-refresh"></span> Update</button>
                </div>
                <div class="col-md-6">
                    <button class="btn btn-success flat pull-right" type="submit" id="checkout" onclick="btn_proses_po_klik()"><span class="fa fa-arrow-circle-right"></span> Proses</button>
                </div>
                </div>
            </div>
            <div class="overlay" id="loading" style="display:none">
              <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="pilih_supplier" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="riwayatheader">Pilih Supplier</h4>
      </div>
      <div class="modal-body">
          <div class="box-body">
            <table class="table table-striped table-bordered" id="mydatatables2">
                <thead>
                    <th style="text-align:center">NO</th>
                    <th style="text-align:center">NAMA</th>
                    <th style="text-align:center">ALAMAT</th>
                    <th style="text-align:center">NAMA SALES</th>
                    <th style="text-align:center"></th>
                </thead>
                <tbody>
                    <?php $i=1; ?>
                    @foreach($supplier as $val)
                    <tr>
                        <td align="center">{{ $i++ }}</td>
                        <td>{{ $val->nama }}</td>
                        <td>{{ $val->alamat }}, <b>{{ ucwords(strtolower($val->kota_nama)) }}</b></td>
                        <td>{{ $val->nama_sales }}</td>
                        <td align="center">
                            <button class="btn bg-maroon flat" type="button" onclick="pilih_supplier('{{ $val->id }}','{{ $val->nama }}')" data-dismiss="modal"><span class="fa fa-check"></span> Pilih</button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
          </div><!-- /.box-body -->
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="pilih_produk" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="riwayatheader">Pilih Produk</h4>
            </div>
            <div class="modal-body">
                <div class="box-body" id="po_produk">
                    
                </div>
            </div>
        </div>
  </div>
</div>

<div class="modal fade" id="ubah_format_no_invoice_po" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content box">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="riwayatheader">Ubah Format No. PO</h4>
            </div>
            <form method="post" action="{{ url('/ubah_format_no_invoice_po') }}" autocomplete="off" class="form-horizontal">
            {{ csrf_field() }}
            <div class="modal-body">
                <div class="form-group">
                    <label class="control-label col-md-2">Format</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="format" value="{{ $ori_format_invoice }}" style="text-transform:uppercase;">
                    </div>
                </div>
                <br>
                <p>Untuk menambahkan template tanggal: gunakan tag 'DD' (hari), 'MM' (bulan), 'YY' (tahun), 'YYYY' (tahun)</p>
                <p>Contoh : dd-mm-yyyy</p>
                <p>Untuk menambahkan template angka: gunakan tag #</p>
                <p>Contoh template angka 4 digit : ####</p>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default pull-left flat"><span class="glyphicon glyphicon-ban-circle"></span> batal</button>
                <button type="submit" class="btn btn-primary pull-right flat" onclick="document.getElementById('loading9').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
            </div><!-- /.box-footer -->
            </form>
            <div class="overlay" id="loading9" style="display:none">
              <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="pengaturan_po" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content box">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="riwayatheader">Pengaturan PO</h4>
            </div>
            <form class="form-horizontal" action="{{ url('/pengaturan_po') }}" autocomplete="off" method="post">
            {{ csrf_field() }}
            <div class="modal-body">
                <div class="form-group">
                    <label class="control-label col-md-2">Alamat Pengiriman</label>
                    <div class="col-md-8">
                        <textarea class="form-control" name="alamat_pengiriman">@if(!empty($pengaturan_po)){{ $pengaturan_po->alamat_pengiriman }}@endif</textarea>
                    </div>
                </div>
                <div class="form-group" style="display: none;">
                    <label class="control-label col-md-2">Syarat dan Ketentuan</label>
                    <div class="col-md-8">
                        <textarea class="form-control" name="syarat_ketentuan">@if(!empty($pengaturan_po)){{ $pengaturan_po->syarat_ketentuan }}@endif</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2">PPN</label>
                    <div class="col-md-2">
                        <div class="checkbox" >
                            <label >
                              <input name="checkbox_ppn" id="checkbox_ppn" type="checkbox" <?php if(!empty($pengaturan_po->ppn)){ echo "checked";  }?> value="checked"> Include PPN
                            </label>
                        </div>
                    </div>
                    <div class="col-md-2" id="display_ppn">
                        <input type="txt" name="ppn" class="form-control" onKeyPress="return numbersonly(this, event)" <?php if(!empty($pengaturan_po->ppn)){ ?> value='{{ $pengaturan_po->ppn }}' <?php }?>>
                    </div>
                    <div class="col-md-2" id="display_persen">
                    <label class="control-label">%</label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default flat pull-left"><span class="glyphicon glyphicon-ban-circle"></span> Batal</button>
                <button type="submit" class="btn btn-primary flat" onclick="document.getElementById('loading10').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
            </div>
            </form>
            <div class="overlay" id="loading10" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

@stop

<script src="{{ asset('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ url('js/jquery.maskMoney.js') }}" type="text/javascript"></script>
<script>
    $(function () {
        $("#mydatatables").DataTable();
        $("#mydatatables2").DataTable();

        $('#datepicker').datepicker({
          autoclose: true,
          todayHighlight: 1,
        });

        $("#diskon").maskMoney({precision:0});
    });

    function pilih_supplier(id, nama)
    { 
        document.getElementById("supplier_id").value = id;
        document.getElementById("nama_supplier").value = nama;  
        document.getElementById("btn_pilih_produk").style.display   = 'block';      

        var url = "{{ url('/get_po_produk') }}";

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })

        $.ajax({
            type: "POST",
            url: url,
            data: { id: id },
            success: function (data) {
                $("#po_produk").html(data);
                document.getElementById("admin_cart").innerHTML             = "";
                document.getElementById("checkout").style.display           = 'none';
            },
            error: function (data) {
                alert('terjadi kesalahan pengambilan data');
            }
        });
    }

    function pilih_produk(id, jenis)
    {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })

        var path =  document.getElementById('base_path').value;
        var url = "{{ url('/po_tambah_cart') }}";
        
        $.ajax({
            type: "POST",
            url: url,
            data: { id:id, jenis:jenis },
            beforeSend: function(){
                        document.getElementById('loading').style.display = "block";
                      },
            success: function (data) {
                $("#admin_cart").html(data);
                document.getElementById("checkout").style.display = 'block';
                document.getElementById('loading').style.display = 'none';
            },
            error: function (data) {
                alert('terjadi kesalahan');
            }
        });
    }

    function btn_proses_po_klik()
    {
        document.getElementById('loading').style.display='block'
        document.getElementById('form_po').submit();
    }

    function update()
    {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })

        var url = "{{ url('/po_update_cart') }}";
        
        $.ajax({
            type: "POST",
            url: url,
            data: $("#admin_ubah_cart").serialize(),
            beforeSend: function(){
                document.getElementById('loading').style.display = "block";
            },
            success: function (data) {
                $("#admin_cart").html(data);
                document.getElementById('loading').style.display = "none";
            },
            error: function (data) {
                alert('ooo');
            }
        });
    }

    function hapus(rowid)
    {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })
        var url = "{{ url('/po_hapus_cart') }}";
        
        $.ajax({
            type: "POST",
            url: url,
            data: { rowid:rowid },
            beforeSend: function(){
                        document.getElementById('loading').style.display = "block";
                      },
            success: function (data) {
                $("#admin_cart").html(data);
                document.getElementById('loading').style.display = "none";
            },
            error: function (data) {
                alert('ooo');
            }
        });
    }
</script>