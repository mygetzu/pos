<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
            </div>
            <div class="box-body">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                    <i class="fa fa-check"></i>
                    {{ Session::get('message') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                <table id="mydatatables" class="table table-striped table-bordered">
                    <thead>
                    <th style="text-align:center">NO</th>
                    <th style="text-align:center">NAMA</th>
                    <th style="text-align:center">ALAMAT</th>
                    <th style="text-align:center"></th>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        @foreach($teknisi as $val)
                        <tr>
                            <td align="center">{{ $i++ }}</td>
                            <td>{{ $val->name }}</td>
                            <td>{{ $val->alamat }}</td>
                            <td align="center">
                                <form action="{{ url('servis/update_teknisi') }}" method="POST" autocomplete="off">
                                    {{ csrf_field() }}
                                    <input name="service_order_id" type="hidden" value="{{ $general['service_id'] }}"/>
                                    <input name="teknisi_id" value="{{ $val->id }}" type="hidden"/>
                                    <button class="btn bg-maroon btn-flat" type="submit"><span class="fa fa-check"></span> Pilih</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@stop
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script>
$(function () {
    $("#mydatatables").DataTable();
    $("#mydatatables2").DataTable();
});
</script>