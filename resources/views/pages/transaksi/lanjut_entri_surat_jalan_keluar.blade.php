@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<input type="hidden" name="total_pesanan_entri" id="total_pesanan_entri" value="{{ $total_pesanan_entri }}">
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <a data-toggle='modal' data-target='#modal_so_detail' style="padding:0px">
                    <button class='btn btn-info flat'><span class='fa fa-info-circle'></span> Sales Order Detail</button>
                </a>
                <a href="{{ url('/sales_order') }}" class="pull-right" style="padding:0px">
                    <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                </a>
            </div>
            <div class="box-body">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <form class="form-horizontal" autocomplete="off" id="form_serial_number">
                    <input type="hidden" name="no_nota" id="no_nota" value="{{ $so_header->no_nota }}">
                    <input type="hidden" name="jumlah_produk" id="jumlah_produk" value="0">
                    <input type="hidden" name="pelanggan_id" id="pelanggan_id" class="form-control">
                    {{ csrf_field() }}
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Gudang</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="gudang" id="gudang">
                                    <option value="">-- Pilih Gudang --</option>
                                    @foreach($gudang as $val)
                                        <option value="{{ $val->id }},{{ $val->nama }}">{{ $val->nama }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Produk</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="produk" id="produk">
                                    <option value="">-- Pilih Produk --</option>
                                   
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">No Packing List</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Masukkan no packing list" name="input_no_surat_jalan" id="input_no_surat_jalan" value="{{ $no_surat_jalan }}">
                                @if ($errors->has('input_no_surat_jalan'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('input_no_surat_jalan') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                            <label class="col-sm-3 control-label">Total Pesanan</label>
                            <div class="col-sm-9">
                                <label class="control-label">{{ $total_pesanan_entri }}</label>
                            </div>
                        </div>
                    </div>
                </form>           
            </div>
            <div class="overlay" id="loading" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-body">
                <div class="form-horizontal">
                    <div class="col-md-6">
                        <div class="form-group" id="display_serial_number" style="display: none">
                            <label class="col-sm-3 control-label">Serial Number</label>
                            <div class="col-sm-9">
                                <input type="text" name="serial_number" class="form-control" onkeyup="if(event.keyCode == 13) pl_tambah_stok()" placeholder="Masukkan serial number, tekan enter" id="serial_number">
                            </div>
                        </div>
                        <div class="form-group" id="display_jumlah_sn" style="display: none">
                            <label class="col-sm-3 control-label">Jumlah SN</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" value="1" id="jumlah_sn" onKeyPress="return numbersonly(this, event)" >
                            </div>
                        </div>  
                        <div class="form-group" id="display_button_list" style="display: none">
                            <div class="col-sm-9 col-sm-offset-3">
                                <button type="button" class="btn btn-primary flat" onclick="pl_tambah_stok()"><span class="fa fa-file-text-o"></span> Masukkan List</button>
                            </div>
                        </div>    
                    </div>
                    <div class="col-md-6" id="display_button_pilih_serial_number" style="display: none">
                          <a data-toggle="modal" data-target="#modal_pilih_serial_number" class="btn btn-primary flat col-md-4"><span class="fa fa-plus-circle"></span> Pilih Serial Number</a>
                    </div>
                </div>
                <div class="col-md-12">
                    <button type="button" class="btn btn-success flat pull-right" onclick="btn_selesai()"><span class="fa fa-check"></span> Selesai</button>
                    <br>
                    <hr>
                </div>
                <div class="col-md-12">
                    <table class="table table-striped table-bordered" id="table_packing_list">
                        <thead>
                            <tr>
                                <th style="text-align:center">GUDANG</th>
                                <th style="text-align:center">NAMA PRODUK/HADIAH/PAKET</th>
                                <th style="text-align:center">JENIS</th>
                                <th style="text-align:center">SERIAL NUMBER</th>
                                <th style="text-align:center">JUMLAH</th>
                                <th style="text-align:center"></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="overlay" id="loading2" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>
@foreach($arr_so_detail as $key => $val)
    <input type="hidden" id="arr_so_detail{{ $val['produk_id'] }}-{{ $val['jenis_barang_id'] }}" value="{{ $val['quantity'] }}">
@endforeach

<form action="{{ url('/surat_jalan_keluar_proses') }}" method="post" id="form_packing_list_proses" >
    {{ csrf_field() }}
    <input type="hidden" name="no_surat_jalan_keluar" id="no_surat_jalan">
    <input type="hidden" name="no_nota_so" value="{{ $so_header->no_nota }}">
    <input type="hidden" name="jumlah_pesanan" value="{{ $total_pesanan_entri }}">
@foreach($arr_packing_list as $key => $val)
    <input type="hidden" name="arr_packing_list_id{{ $key }}" id="arr_packing_list_id{{ $key }}" value="{{ $val['id'] }}">
    <input type="hidden" name="arr_packing_list_gudang_id{{ $key }}" id="arr_packing_list_gudang_id{{ $key }}" value="{{ $val['gudang_id'] }}">
    <input type="hidden" name="arr_packing_list_produk_id{{ $key }}" id="arr_packing_list_produk_id{{ $key }}" value="{{ $val['produk_id'] }}">
    <input type="hidden" name="arr_packing_list_jenis_barang_id{{ $key }}" id="arr_packing_list_jenis_barang_id{{ $key }}" value="{{ $val['jenis_barang_id'] }}">
    <input type="hidden" name="arr_packing_list_serial_number{{ $key }}" id="arr_packing_list_serial_number{{ $key }}" value="{{ $val['serial_number'] }}">
@endforeach
</form>

<div class="modal fade" id="modal_so_detail" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content box">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">SO Detail</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-bordered" id="ajax_produk_tabless3">
                    <thead>
                        <tr>
                            <th style="text-align:center">NO</th>
                            <th style="text-align:center">NAMA</th>
                            <th style="text-align:center">JENIS</th>
                            <th style="text-align:center">JUMLAH</th>
                            <th style="text-align:center">HARGA</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($display_so_detail as $key => $value)
                        <tr>
                            <td align="center">{{ $key+1 }}</td>
                            <td>{{ $value['produk_nama'] }}</td>
                            <td>{{ $value['jenis'] }}</td>
                            <td align="center">{{ $value['quantity'] }}</td>
                            <td>{{ rupiah($value['harga']) }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="overlay" id="loading3" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_pilih_serial_number" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content box">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Produk Serial Number</h4>
            </div>
            <div class="modal-body">
                <div id="produk_serial_number">
                </div>
            </div>
            <div class="overlay" id="loading3" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
  $(function () {
    $("#ajax_produk_tabless3").DataTable();
  });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#gudang').change(function(){ 
            var gudang = document.getElementById('gudang').value;
            gudang = gudang.split(',');
            var gudang_id   = gudang[0];
            var gudang_nama = gudang[1];   

            var no_nota_so = document.getElementById('no_nota') .value;

            //get produk/paket/hadiah dengan gudang

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            var url = "{{ url('/sjk_get_produk_gudang') }}";

            $.ajax({
                type: "POST",
                url: url,
                data: { gudang_id:gudang_id, no_nota_so:no_nota_so },
                success: function (data) {
                    $("#produk").html(data);
                },
                error: function (data) {
                    alert('ooo');
                }
            });    
        });

        $('#produk').change(function(){
            //cek dia produk/hadiah/paket
            var produk          = document.getElementById('produk').value;

            var gudang      = document.getElementById('gudang').value;
            gudang          = gudang.split(',');
            var gudang_id   = gudang[0];
            var gudang_nama = gudang[1];

            if(produk == ""){
                document.getElementById('display_serial_number').style.display              = 'none';
                document.getElementById('display_button_pilih_serial_number').style.display = 'none';
                document.getElementById('display_jumlah_sn').style.display                  = 'none';
                document.getElementById('display_button_list').style.display                = 'none';
            }
            else{
                produk              = produk.split(',');
                var produk_id       = produk[0];
                var produk_nama     = produk[1];
                var produk_jenis    = produk[2]; 

                if(produk_jenis == 1){
                    document.getElementById('display_serial_number').style.display              = 'block';
                    document.getElementById('display_button_pilih_serial_number').style.display = 'block';
                    document.getElementById('display_jumlah_sn').style.display                  = 'block';
                    document.getElementById('display_button_list').style.display                = 'block';
                }
                else{
                    document.getElementById('display_serial_number').style.display              = 'none';
                    document.getElementById('display_button_pilih_serial_number').style.display = 'none';
                    document.getElementById('display_jumlah_sn').style.display                  = 'none';
                    document.getElementById('display_button_list').style.display                = 'block';
                }

                //ajax get produk serial number yang bisa ditambahkan
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                })

                var url = "{{ url('/get_so_produk_serial_number') }}";

                $.ajax({
                    type: "POST",
                    url: url,
                    data: { gudang_id:gudang_id, produk_id:produk_id },
                    success: function (data) {
                        $("#produk_serial_number").html(data);
                    },
                    error: function (data) {
                        alert('error');
                    }
                });
            }
        });
    });

    function pilih_serial_number(serial_number)
    {
        document.getElementById('serial_number').value = serial_number;
    }

    function pl_tambah_stok()
    { 
        var gudang      = document.getElementById('gudang').value;
        var produk      = document.getElementById('produk').value;

        if(gudang == "" || produk == ""){
            alert('masukkan gudang dan produk terlebih dahulu');
            document.getElementById('serial_number').value  = "";
            document.getElementById('jumlah_sn').value      = 1;
        }
        else{
            gudang              = gudang.split(',');
            var gudang_id       = gudang[0];
            var gudang_nama     = gudang[1];

            produk              = produk.split(',');
            var produk_id       = produk[0];
            var produk_nama     = produk[1];
            var jenis_barang_id = produk[2];

            var serial_number   = document.getElementById('serial_number').value;
            var jumlah_sn       = document.getElementById('jumlah_sn').value;

            if(serial_number == "" || jumlah_sn == ""){
                alert('masukkan serial number dan jumlah terlebih dahulu');
                document.getElementById('serial_number').value  = "";
                document.getElementById('jumlah_sn').value      = 1;
                return;
            }

            jumlah_sn = parseInt(jumlah_sn);

            //cek serial number terdaftar atau tidak
            var total_serial_number = document.getElementById('total_serial_number').value;
            total_serial_number = parseInt(total_serial_number);
            var stok_serial_number = 0;

            var cek_ada_serial_number = 0;
            for (var i = total_serial_number - 1; i >= 0; i--) {
                var list_serial_number = document.getElementById('list_serial_number'.concat(i)).value;
                if(list_serial_number == serial_number){
                    cek_ada_serial_number = 1;
                    stok_serial_number = document.getElementById('stok_serial_number'.concat(i)).value;
                    stok_serial_number = parseInt(stok_serial_number);
                    break;
                }
            }

            if(cek_ada_serial_number == 1){

                //cek stok mencukupi atau tidak
                var cek_stok = 0;
                var total_pesanan_entri = document.getElementById('total_pesanan_entri').value;
                total_pesanan_entri = parseInt(total_pesanan_entri);

                for (var i = 0; i < total_pesanan_entri; i++) {
                    var cek_gudang_id       = document.getElementById('arr_packing_list_gudang_id'.concat(i)).value;
                    var cek_produk_id       = document.getElementById('arr_packing_list_produk_id'.concat(i)).value;
                    var cek_jenis_barang_id = document.getElementById('arr_packing_list_jenis_barang_id'.concat(i)).value;
                    var cek_serial_number   = document.getElementById('arr_packing_list_serial_number'.concat(i)).value;

                    if(cek_gudang_id == gudang_id && cek_produk_id == produk_id && cek_jenis_barang_id == jenis_barang_id && cek_serial_number == serial_number){
                        cek_stok++;
                    }
                }

                cek_stok = cek_stok + jumlah_sn;

                if(stok_serial_number >= cek_stok){
                    //cek arr_so_detail
                    var cek_quantity = document.getElementById('arr_so_detail'.concat(produk_id).concat('-').concat(jenis_barang_id)).value;
                    cek_quantity = parseInt(cek_quantity);

                    if(cek_quantity - jumlah_sn < 0){
                        if(cek_quantity <= 0){
                            alert('serial number sudah di tambahkan');
                            document.getElementById('serial_number').value  = "";
                            document.getElementById('jumlah_sn').value      = 1;
                        }
                        else{
                            alert('jumlah melebihi pesanan');
                            document.getElementById('serial_number').value  = "";
                            document.getElementById('jumlah_sn').value      = 1;
                        }
                    }
                    else{
                        //kurangi quantity arr_so_detail
                        document.getElementById('arr_so_detail'.concat(produk_id).concat('-').concat(jenis_barang_id)).value = cek_quantity-jumlah_sn;

                        //tambah ke tabel
                        var table   = document.getElementById("table_packing_list");
                        var row     = table.insertRow(1);

                        var cell1 = row.insertCell(0);
                        var cell2 = row.insertCell(1);
                        var cell3 = row.insertCell(2);
                        var cell4 = row.insertCell(3);
                        var cell5 = row.insertCell(4);
                        var cell6 = row.insertCell(5);

                        var jenis = "";

                        if(jenis_barang_id == 1){
                            jenis = "PRODUK";
                        }
                        if(jenis_barang_id == 2){
                            jenis = "HADIAH";
                        }
                        if(jenis_barang_id == 3){
                            jenis = "PAKET";
                        }

                        cell1.innerHTML = gudang_nama;
                        cell2.innerHTML = produk_nama;
                        cell3.innerHTML = jenis;
                        cell4.innerHTML = serial_number;
                        cell5.innerHTML = jumlah_sn;
                        cell6.innerHTML = '<button class="btn btn-danger flat" onclick="hapus_sn_klik(\''+gudang_id+'\',\''+produk_id+'\',\''+jenis_barang_id+'\',\''+serial_number+'\',\''+jumlah_sn+'\',this)"><span class="fa fa-trash-o"><span> Hapus</button>';
                        
                        cell3.setAttribute("align", "center");
                        cell5.setAttribute("align", "center");
                        cell6.setAttribute("align", "center");

                        document.getElementById('serial_number').value  = "";
                        document.getElementById('jumlah_sn').value      = 1;

                        //tambahkan ke array
                        var total_pesanan_entri = document.getElementById('total_pesanan_entri').value;
                        total_pesanan_entri = parseInt(total_pesanan_entri);

                        for (var i = 0; i < total_pesanan_entri; i++) {
                            if(jumlah_sn <= 0){
                                break;
                            }

                            var cek_arr_pl = document.getElementById('arr_packing_list_serial_number'.concat(i)).value;
                            
                            if(cek_arr_pl == ""){
                                document.getElementById('arr_packing_list_gudang_id'.concat(i)).value          = gudang_id;
                                document.getElementById('arr_packing_list_produk_id'.concat(i)).value          = produk_id;
                                document.getElementById('arr_packing_list_jenis_barang_id'.concat(i)).value    = jenis_barang_id;
                                document.getElementById('arr_packing_list_serial_number'.concat(i)).value      = serial_number;
                                jumlah_sn--;
                            }
                        }
                    } 
                }
                else{
                    alert('stok tidak mencukupi');
                    document.getElementById('serial_number').value  = "";
                    document.getElementById('jumlah_sn').value      = 1;
                }
            }
            else{
                alert('serial number tidak terdaftar');
                document.getElementById('serial_number').value  = "";
                document.getElementById('jumlah_sn').value      = 1;
            }
        }
    }

    function hapus_sn_klik(gudang_id, produk_id, jenis_barang_id, serial_number, jumlah, obj)
    {
        var cek = confirm('apakah anda yakin?');

        if(cek){
            //hapus row table
            var row = obj.closest("tr");
            document.getElementById("table_packing_list").deleteRow(row.rowIndex);

            //hapus array sejumlah jumlah
            var total_pesanan_entri = document.getElementById('total_pesanan_entri').value;
            total_pesanan_entri = parseInt(total_pesanan_entri);

            var jumlah_hapus = jumlah;
            jumlah_hapus = parseInt(jumlah_hapus);
            for (var i = 0; i < total_pesanan_entri; i++) {
                if(jumlah_hapus <= 0){
                    break;
                }
                var cek_arr_sn_gudang_id        = document.getElementById('arr_packing_list_gudang_id'.concat(i)).value;
                var cek_arr_sn_produk_id        = document.getElementById('arr_packing_list_produk_id'.concat(i)).value;
                var cek_arr_sn_jenis_barang_id  = document.getElementById('arr_packing_list_jenis_barang_id'.concat(i)).value;
                var cek_arr_sn_serial_number    = document.getElementById('arr_packing_list_serial_number'.concat(i)).value;

                if(gudang_id == cek_arr_sn_gudang_id && produk_id == cek_arr_sn_produk_id && jenis_barang_id == cek_arr_sn_jenis_barang_id && serial_number == cek_arr_sn_serial_number){
                    jumlah_hapus--;
                    document.getElementById('arr_packing_list_gudang_id'.concat(i)).value          = "";
                    document.getElementById('arr_packing_list_produk_id'.concat(i)).value          = "";
                    document.getElementById('arr_packing_list_jenis_barang_id'.concat(i)).value    = "";
                    document.getElementById('arr_packing_list_serial_number'.concat(i)).value      = "";
                }
            }
            
            //tambah stok arr_po_detail
            var quantity = document.getElementById('arr_so_detail'.concat(produk_id).concat('-').concat(jenis_barang_id)).value;
            quantity = parseInt(quantity);
            jumlah      = parseInt(jumlah);
            var new_jumlah = quantity+jumlah;
            
            document.getElementById('arr_so_detail'.concat(produk_id).concat('-').concat(jenis_barang_id)).value = new_jumlah;
        }
    }

    function btn_selesai()
    {
        $cek = confirm('apakah produk serial number sudah sesuai?');

        if($cek){
            var no_surat_jalan = document.getElementById('input_no_surat_jalan').value;
            document.getElementById('no_surat_jalan').value = no_surat_jalan;
            document.getElementById('form_packing_list_proses').submit();
            document.getElementById('loading').style.display = 'block';
            document.getElementById('loading2').style.display = 'block';
        }
    }
</script>
@stop