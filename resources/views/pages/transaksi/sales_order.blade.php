<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <a href="{{ url('/tambah_sales_order') }}">
                    <button class='btn bg-purple flat'><span class='fa fa-plus'></span> Tambah Sales Order</button>
                </a>
            </div>
            <div class="box-body">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <table id="mydatatables" class="table table-striped table-bordered" style="margin:0px">
                    <thead>
                        <th style="text-align:center">NO</th>
                        <th style="text-align:center">NO NOTA</th>
                        <th style="text-align:center">TANGGAL</th>
                        <th style="text-align:center">NAMA PELANGGAN</th>
                        <th style="text-align:center">TOTAL TAGIHAN</th>
                        <th style="text-align:center"></th>
                        <th style="text-align:center"></th>
                    </thead>
                    <tbody>
                        <?php $i=1; $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'); ?>
                        @foreach($so_header as $val)
                        <tr>
                            <td align="center">{{ $i++ }}</td>
                            <td align="center">{{ $val->no_sales_order }}</td>
                            <td><?php
                                    date_default_timezone_set("Asia/Jakarta");
                                    $tanggal = new DateTime($val->tanggal);
                                    $jatuh_tempo = new DateTime($val->due_date);
                                    $tanggals = $tanggal->format('Y-m-d');
                                    $jatuh_tempos = $jatuh_tempo->format('Y-m-d');
                                ?>
                                <?php echo $tanggal->format('d')." ".$bulan[(int)$tanggal->format('m')]." ".$tanggal->format('Y')." ".$tanggal->format('G:i:s'); ?>
                            </td>
                            <td>{{ $val->pelanggan->nama }}</td>
                            <td>{{ rupiah($val->total_tagihan) }}</td>
                            <td align="center">
                                <a href="{{ url('/nota_sales_order/'.$val->id) }}" class="btn btn-info flat" data-toggle="tooltip" title="Sales Order" style="font-size: 15pt"><i class="fa fa-print"></i></a>
                            </td>
                            <td align="center">
                                <a class="btn btn-info flat" href="{{ url('/detail_sales_order/'.$val->id) }}" data-toggle="tooltip" title="detail" style="font-size: 15pt"><span class="fa fa-info-circle"></span></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script>
  $(function () {
    $("#mydatatables").DataTable();
  });
</script>
