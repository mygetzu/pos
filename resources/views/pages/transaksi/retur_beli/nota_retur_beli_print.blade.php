<link rel="stylesheet" type="text/css" href="{{ URL::to('/css/nota.css') }}">
@include('includes.base_function')
<?php
    function rupiah2($nominal)
    {
        $rupiah =  number_format($nominal,0, ",",".");
        $rupiah = $rupiah.',00';
        return $rupiah;
    }
?>
<style type="text/css">
    [class*="col-"] {
        float: left;
    }
    pre {
        font-family: sans-serif;
        background-color: transparent;
        border: none;
        overflow-x: hidden;
        white-space: pre-wrap;       /* css-3 */
        white-space: -moz-pre-wrap;  /* Mozilla, since 1999 */
        white-space: -pre-wrap;      /* Opera 4-6 */
        white-space: -o-pre-wrap;    /* Opera 7 */
        word-wrap: break-word; 
        padding: 0px;
    }

    @font-face {
        font-family: mycalibri;
        src: url("{{ asset('fonts/Calibri.ttf') }}");
    }

    @font-face {
        font-family: mycalibri;
        src: url("{{ asset('fonts/Calibri-Bold.ttf') }}");
        font-weight: bold;
    }

    .area{
        font-family: mycalibri;
    }
</style>
<div class="area">
    <section id="header">
        <div>
            <div class="col-5">
                <div class="title"><span style="border-bottom: 1px solid black;">{{ strtoupper($judul) }}</span></div>
                <div class="company">
                    {{ strtoupper($perusahaan->nama) }} <br>
                    {{ $perusahaan->alamat }} {{ $kota_perusahaan }} <br>
                    Telp. {{ $perusahaan->telp }} | {{ $perusahaan->telp2 }} <br>
                    E. {{ $perusahaan->email }}
                </div>
            </div>
            <div class="col-5" style="margin-left: 20px;">
                <table class="detail">
                    <tr>
                        <td class="title-detail">No. Retur</td>
                        <td class="titik">&nbsp;:&nbsp;</td>
                        <td class="data-detail">{{ $rb_header->no_nota }}</td>
                    </tr>
                    <tr>
                        <td class="title-detail">Tanggal</td>
                        <td class="titik">&nbsp;:&nbsp;</td>
                        <td class="data-detail">
                            <?php $tanggal = new DateTime($rb_header->tanggal); 
                            echo $tanggal->format('d/m/Y G:i:s');
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="title-detail">Supplier</td>
                        <td class="titik">&nbsp;:&nbsp;</td>
                        <td class="data-detail user">{{ $supplier['nama'] }}</td>
                    </tr>
                    <tr>
                        <td class="title-detail" style="vertical-align: top;">Alamat </td>
                        <td class="titik" style="vertical-align: top;">&nbsp;:&nbsp;</td>
                        <td class="data-detail user">{{ $supplier['alamat'] }}<br>
                        Telp. {{ $supplier['telp'] }}<br>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </section>
    <section id="table">
        <div>
            <table class="table">
                <thead>
                    <tr style="text-align: center;">
                        <th style="width: 5%">No.</th>
                        <th style="width: 29%">Nama Produk</th>
                        <th style="width: 12%">Serial Number</th>
                        <th style="width: 10%">Satuan</th>
                        <th style="width: 8%">Jumlah</th>
                        <th style="width: 18%">Harga</th>
                        <th style="width: 18%">Total</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $jumlah_item=0; $sub_total=0; ?>
                    @foreach($rb_detail as $i => $val)
                    <tr>
                        <td style="text-align: center;">{{ $i+1 }}</td>
                        <td>{{ $val['nama'] }}</td>
                        <td>{{ $val['serial_number'] }}</td>
                        <td style="text-align: center;">{{ $val['satuan'] }}</td>
                        <td style="text-align: center;">{{ $val['jumlah'] }}</td>
                        <td style="text-align: right;padding-right: 40px;">{{ rupiah($val['harga']) }}</td>
                        <td style="text-align: right;padding-right: 40px;">{{ rupiah($val['jumlah']*$val['harga']) }}</td>
                    </tr>
                    <?php $jumlah_item=$jumlah_item+$val['jumlah']; 
                        $sub_total = $sub_total+ ($val['jumlah']*$val['harga']);
                    ?>
                    @endforeach
                </tbody>
            </table>
        </div>
    </section>
    <section id="footer">
        <div>
            <div class="col-5" style="margin-bottom: 30px;">
                <table style="margin-top: 60px;">
                    <tr style="vertical-align: text-top;">
                        <td class="title-detail">Keterangan</td>
                        <td class="titik">&nbsp;:&nbsp;</td>
                        <td class="data-detail">
                        {{ $rb_header->catatan }}
                        <br><br></td>
                    <tr style="vertical-align: text-top;">
                        <td class="title-detail">Terbilang</td>
                        <td class="titik" style="vertical-align: top;">&nbsp;:&nbsp;</td>
                        <td class="data-detail">{{ dibaca($rb_header->total_tagihan) }} Rupiah</td>
                    </tr>
                </table>
                <p style="position: absolute;bottom: 0;"><?php echo $print_time->format('d/m/Y G:i:s'); ?></p>
            </div>
            <div class="col-7">
                <div class="col-12">
                    <div class="col-6">
                        <table width="75%">
                            <tr style="text-align: center;">
                                <td class="title-detail keterangan" width="55%">Jml Item</td>
                                <td class="titik keterangan" width="7%">&nbsp;:&nbsp;</td>
                                <td class="data-detail keterangan" width="25%">{{ $jumlah_item }}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-6">
                        <table  width="86%">
                            <tr>
                                <td class="title-detail price" style="text-align: left;" width="40%;">Sub Total</td>
                                <td class="titik price" style="text-align: center;" width="10%">&nbsp;:&nbsp;</td>
                                <td class="data-detail right price">{{ rupiah2($rb_header->total_retur) }}</td>
                            </tr>
                            @if(!empty($rb_header->potongan_retur))
                            <tr>
                                <td class="title-detail price" style="text-align: left;" >Diskon</td>
                                <td class="titik" style="text-align: center;">&nbsp;:&nbsp;</td>
                                <td class="data-detail right price">{{ rupiah2($rb_header->potongan_retur) }}</td>
                            </tr>
                            @endif
                            <tr>
                                <td class="title-detail price" style="text-align: left;" >Total Akhir</td>
                                <td class="titik price" style="text-align: center;">&nbsp;:&nbsp;</td>
                                <td class="data-detail right price">{{ rupiah2($rb_header->total_tagihan) }}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-12">
                        <div class="col-4" style="margin-left: 20%">
                            <div style="margin-top: 20px;">
                                <div style="text-align: center">
                                    <strong>Penerima</strong>
                                </div>
                            </div>
                            <div style="margin-top: 100px;">
                                <div style="text-align: center">
                                    <strong>(.........................)</strong>
                                </div>
                            </div>
                        </div>
                        <div class="col-4">
                            <div style="margin-top: 20px;">
                                <div style="text-align: center">
                                    <strong>Hormat Kami</strong>
                                </div>
                            </div>
                            <div style="margin-top: 100px;">
                                <div style="text-align: center">
                                    <strong>(.........................)</strong>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function(){
          window.onload = function() { window.print(); }
      });
</script>