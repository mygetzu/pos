@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<?php
    function rupiah2($nominal)
    {
        $rupiah =  number_format($nominal,0, ",",".");
        $rupiah = $rupiah.',00';
        return $rupiah;
    }
?>
<link rel="stylesheet" type="text/css" href="{{ URL::to('/css/invoice.css') }}">
<style type="text/css">
    pre {
        font-family: sans-serif;
        background-color: transparent;
        border: none;
        overflow-x: hidden;
        white-space: pre-wrap;       /* css-3 */
        white-space: -moz-pre-wrap;  /* Mozilla, since 1999 */
        white-space: -pre-wrap;      /* Opera 4-6 */
        white-space: -o-pre-wrap;    /* Opera 7 */
        word-wrap: break-word; 
        padding: 0px;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                
                <a href="{{ url('/nota_retur_beli_print/'.$rb_header->id) }}" target="_blank" class="btn bg-orange flat" style="margin-right: 5px;"><i class="fa fa-print"></i> Cetak</a>
                <a href="{{ url('/nota_retur_beli_pdf/'.$rb_header->id) }}" target="_blank" class="btn btn-info flat" style="margin-right: 5px;"><i class="fa fa-download"></i> Unduh PDF</a>
                <a href="{{ url('/retur_beli') }}" class="pull-right" style="padding:0px">
                    <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                </a>
            </div>
            <div class="box-body">
                <div class="area">
                    <section id="header">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="title"><span style="border-bottom: 1px solid black;">{{ strtoupper($judul) }}</span></div>
                                <div class="company">
                                    {{ strtoupper($perusahaan->nama) }} <br>
                                    {{ $perusahaan->alamat }} {{ $kota_perusahaan }} <br>
                                    Telp. {{ $perusahaan->telp }} | {{ $perusahaan->telp2 }} <br>
                                    E. {{ $perusahaan->email }}
                                </div>
                            </div>
                            <div class="col-md-5 col-md-offset-1">
                                <table class="detail">
                                    <tr>
                                        <td class="title-detail">No. Retur</td>
                                        <td class="titik">&nbsp;:&nbsp;</td>
                                        <td class="data-detail">{{ $rb_header->no_nota }}</td>
                                    </tr>
                                    <tr>
                                        <td class="title-detail">Tanggal</td>
                                        <td class="titik">&nbsp;:&nbsp;</td>
                                        <td class="data-detail">
                                            <?php $tanggal = new DateTime($rb_header->tanggal); 
                                            echo $tanggal->format('d/m/Y G:i:s');
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="title-detail">Supplier</td>
                                        <td class="titik">&nbsp;:&nbsp;</td>
                                        <td class="data-detail user">{{ $supplier['nama'] }}</td>
                                    </tr>
                                    <tr>
                                        <td class="title-detail" style="vertical-align: top;">Alamat </td>
                                        <td class="titik" style="vertical-align: top;">&nbsp;:&nbsp;</td>
                                        <td class="data-detail user">{{ $supplier['alamat'] }}<br>
                                        Telp. {{ $supplier['telp'] }}<br>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </section>
                    <section id="table">
                        <div>
                            <table class="table">
                                <thead>
                                    <tr style="text-align: center;">
                                        <th style="width: 5%">No.</th>
                                        <th style="width: 29%">Nama Produk</th>
                                        <th style="width: 12%">Serial Number</th>
                                        <th style="width: 10%">Satuan</th>
                                        <th style="width: 8%">Jumlah</th>
                                        <th style="width: 18%">Harga</th>
                                        <th style="width: 18%">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $jumlah_item=0; $sub_total=0; ?>
                                    @foreach($rb_detail as $i => $val)
                                    <tr>
                                        <td style="text-align: center;">{{ $i+1 }}</td>
                                        <td>{{ $val['nama'] }}</td>
                                        <td>{{ $val['serial_number'] }}</td>
                                        <td style="text-align: center;">{{ $val['satuan'] }}</td>
                                        <td style="text-align: center;">{{ $val['jumlah'] }}</td>
                                        <td style="text-align: right;padding-right: 40px;">{{ rupiah($val['harga']) }}</td>
                                        <td style="text-align: right;padding-right: 40px;">{{ rupiah($val['jumlah']*$val['harga']) }}</td>
                                    </tr>
                                    <?php $jumlah_item=$jumlah_item+$val['jumlah']; 
                                        $sub_total = $sub_total+ ($val['jumlah']*$val['harga']);
                                    ?>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </section>
                    <section id="footer">
                        <div class="row">
                            <div class="col-md-5" style="margin-bottom: 30px;">
                                <table style="margin-top: 60px;">
                                    <tr style="vertical-align: text-top;">
                                        <td class="title-detail">Keterangan</td>
                                        <td class="titik">&nbsp;:&nbsp;</td>
                                        <td class="data-detail">
                                        {{ $rb_header->catatan }}
                                        <br><br></td>
                                    <tr style="vertical-align: text-top;">
                                        <td class="title-detail">Terbilang</td>
                                        <td class="titik" style="vertical-align: top;">&nbsp;:&nbsp;</td>
                                        <td class="data-detail">{{ dibaca($rb_header->total_tagihan) }} Rupiah</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-7">
                                <div class="col-md-12">
                                    <div class="col-md-5">
                                        <table width="85%">
                                            <tr style="text-align: center;">
                                                <td class="title-detail keterangan" width="55%">Jml Item</td>
                                                <td class="titik keterangan" width="7%">&nbsp;:&nbsp;</td>
                                                <td class="data-detail keterangan" width="25%">{{ $jumlah_item }}</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-md-7" style="padding-right: 25px;">
                                        <table>
                                            <tr>
                                                <td class="title-detail price" style="text-align: left;" width="160px;">Sub Total</td>
                                                <td class="titik price" style="text-align: center;" width="100px;">&nbsp;:&nbsp;</td>
                                                <td class="data-detail right price">{{ rupiah2($rb_header->total_retur) }}</td>
                                            </tr>
                                            @if(!empty($rb_header->potongan_retur))
                                            <tr>
                                                <td class="title-detail price" style="text-align: left;" width="160px;">Diskon</td>
                                                <td class="titik" style="text-align: center;" width="100px;">&nbsp;:&nbsp;</td>
                                                <td class="data-detail right price">{{ rupiah2($rb_header->potongan_retur) }}</td>
                                            </tr>
                                            @endif
                                            <tr>
                                                <td class="title-detail price" style="text-align: left;" width="160px;">Total Akhir</td>
                                                <td class="titik price" style="text-align: center;" width="100px;">&nbsp;:&nbsp;</td>
                                                <td class="data-detail right price">{{ rupiah2($rb_header->total_tagihan) }}</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-offset-3 col-md-4">
                                            <div style="margin-top: 20px;">
                                                <div style="text-align: center">
                                                    <strong>Penerima</strong>
                                                </div>
                                            </div>
                                            <div style="margin-top: 100px;">
                                                <div style="text-align: center">
                                                    <strong>(.........................)</strong>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div style="margin-top: 20px;">
                                                <div style="text-align: center">
                                                    <strong>Hormat Kami</strong>
                                                </div>
                                            </div>
                                            <div style="margin-top: 100px;">
                                                <div style="text-align: center">
                                                    <strong>(.........................)</strong>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php echo $print_time->format('d/m/Y G:i:s'); ?>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
@stop