<table class="table table-borderless">
    <tr>
        <th width="20%">Serial Number</th>
        <th width="5%">:</th>
        <td>
            <input type="text" name="serial_number" id="serial_number" class="form-control" onkeyup="if(event.keyCode == 13) sj_tambah_stok()" placeholder="Masukkan serial number, tekan enter">
        </td>
        <th width="15%" style="text-align:right">Jumlah</th>
        <th width="3%">:</th>
        <td>
            <select class="form-control" id="jumlah" name="jumlah">
               
            </select>
        </td>
    </tr>
    <tr>
        <th width="20%">Keterangan</th>
        <th width="5%">:</th>
        <td colspan="4">
            <textarea type="text" name="keterangan" id="keterangan" class="form-control" placeholder="Masukkan keterangan">
            </textarea>
        </td>
    </tr>
</table>