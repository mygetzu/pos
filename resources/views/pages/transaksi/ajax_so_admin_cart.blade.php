<?php
    function rupiah($nominal)
    {
        $rupiah =  number_format($nominal,0, ",",".");
        $rupiah = "Rp "  . $rupiah;
        return $rupiah;
    }
?>
<form id="admin_ubah_cart" autocomplete="off">
    <input type="hidden" name="pelanggan_id" id="cart_pelanggan_id">
    {{ csrf_field() }}
    <table class="table table-striped">
        <thead>
            <tr>
                <th width="5%" style="text-align:center">NO</th>
                <th style="text-align:center">NAMA PRODUK</th>                    
                <th style="text-align:center">HARGA</th>
                <th width="10%" style="text-align:center">JUMLAH</th>
                <th style="text-align:center">TOTAL HARGA</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php $i = 1; $jumlah=0; $total_tagihan=0; $row=1; ?>
            @foreach($cart_content as $cart)
            <tr>
                <input type="hidden" name="rowid" value="{{ $cart->rowId }}">
                <input type="hidden" name="id{{ $row }}" value="{{ $cart->id }}">
                <td align="center">{{ $i++ }}</td>
                <td>
                    @if($cart->name === 'voucher')
                    <b>{{ $cart->name }}</b>
                    @else
                        {{ $cart->name }} 
                        @if(strpos($cart->id, 'DIS') === 0) 
                            <b>(*diskon)</b> 
                        @elseif(strpos($cart->id, 'PKT') === 0) 
                            <b>(*paket)</b> 
                        @elseif(strpos($cart->id, 'HDH') === 0) 
                            <b>(*hadiah)</b>
                        @endif
                    @endif
                </td>
                <td align="right" style="padding-right:20px">{{ rupiah($cart->price) }}</td>
                @if(substr($cart->id, 0, 3) == "HDH")
                <td align="center">{{ $cart->qty }}</td>
                @else
                <td align="center">
                    <input type="text" name="jumlah{{ $row }}" id="jumlah" class="form-control" value="{{ $cart->qty }}" style="text-align:right">
                </td>
                @endif
                <td align="right" style="padding-right:20px">{{ rupiah($cart->price*$cart->qty) }}</td>
                <td align="center">
                    <button type="button" class="btn btn-danger flat" onclick="hapus('{{ $cart->rowId }}')"><span class="glyphicon glyphicon-trash"></span> Hapus</button>
                </td>
            </tr>
            <?php 
                $jumlah += $cart->qty;
                $total_tagihan += ($cart->price*$cart->qty);
                $row++;
            ?>
            @endforeach
            <tr>
                <td></td>
                <td colspan="3" align="center">TOTAL</td>
                <td align="right"><p><b>{{ $jumlah }}</b></p></td>
                <td align="right"><p><b>{{ rupiah($total_tagihan) }}</b></p></td>
                <td></td>
            </tr>
        </tbody>
    </table>
    <input type="hidden" name="total" value="{{ $row }}">
    <input type="hidden" id="total_tagihan" value="{{ rupiah($total_tagihan) }}">
</form>