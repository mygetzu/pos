<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="nav-tabs-custom tab-primary flat">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">Purchase Order Detail</a></li>
              <li><a href="#tab_2" data-toggle="tab">Purchase Order Produk</a></li>
              <li class="pull-right">
                <a href="{{ url('/purchase_order') }}" class="pull-right" style="padding:0px">
                    <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                </a>
              </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <table class="table table-striped">
                    <tr>
                        <th width="20%">No Purchase Order</th>
                        <th width="5%">:</th>
                        <td><input type="text" class="form-control" value="{{ $po_header->no_purchase_order }}" disabled="true"></td>
                    </tr>
                    <tr>
                        <th>Tanggal</th>
                        <th>:</th>
                            <?php
                            $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
                            date_default_timezone_set("Asia/Jakarta");
                            $tanggal = new DateTime($po_header->tanggal);
                            $jatuh_tempo = new DateTime($po_header->due_date);
                            ?>
                        <td><input type="text" class="form-control" name="alamat" id="alamat" value="{{ $tanggal->format('d') }} {{ $bulan[(int)$tanggal->format('m')] }} {{ $tanggal->format('Y') }} {{ $tanggal->format('G:i:s') }}" disabled="true"></td>
                    </tr>
                    <tr>
                        <th>Nama Supplier</th>
                        <th>:</th>
                        <td><input type="text" class="form-control" value="{{ $po_header->supplier->nama }}" disabled="true"></td>
                    </tr>
                    <tr>
                        <th>Diskon</th>
                        <th>:</th>
                        <td><input type="text" class="form-control" value="@if(!empty($po_diskon)){{ rupiah($po_diskon->nominal) }}@endif" disabled="true"></td>
                    </tr>
                    <tr>
                        <th>Total Tagihan</th>
                        <th>:</th>
                        <td><input type="text" class="form-control" value="{{ rupiah($po_header->total_tagihan) }}" disabled="true"></td>
                    </tr>
                    <tr>
                        <th>Catatan</th>
                        <th>:</th>
                        <td><textarea type="text" class="form-control" disabled="true">{{ $po_header->catatan }}</textarea></td>
                    </tr>
                </table>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
                <table class="table table-striped table-bordered" id="mydatatables">
                    <thead>
                        <th style="text-align:center">NO</th>
                        <th style="text-align:center">NAMA PRODUK</th>
                        <th style="text-align:center">JUMLAH</th>
                        <th style="text-align:center">HARGA</th>
                    </thead>
                    <tbody>
                        <?php $i=1; ?>
                        @foreach($po_detail as $val)
                        <tr>
                            <td align="center">{{ $i++ }}</td>
                            <td>
                                @if($val->jenis_barang_id == 1)
                                {{ $val->produk->nama }}
                                @elseif($val->jenis_barang_id == 2)
                                {{ $val->hadiah->nama }}
                                @endif
                            </td>
                            <td align="center">{{ $val->jumlah }}</td>
                            <td>{{ rupiah($val->harga) }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
    </div>
</div>
@stop
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script>
  $(function () {
    $("#mydatatables").DataTable();
  });
</script>