<?php
    function rupiah($nominal)
    {
        $rupiah =  number_format($nominal,0, ",",".");
        $rupiah = "Rp "  . $rupiah;
        return $rupiah;
    }
?>
<!-- List stok serial number -->
@foreach($produk_serial_number as $val)
<input type="hidden" id="cek_stok{{ $val->serial_number }}" value="{{ $val->stok }}">
@endforeach
<table class="table table-striped table-bordered" id="ajax_produk_tabless">
    <thead>
        <tr>
            <th style="text-align:center">NAMA PRODUK</th>
            <th style="text-align:center">SERIAL NUMBER</th>
            <th style="text-align:center">STOK</th>
            <th style="text-align:center">JUMLAH</th>
        </tr>
    </thead>
    <tbody>
        @for($i=0; $i < $jumlah; $i++)
        <tr>
            <td>{{ $produk->nama }}</td>
            <td>
                <select class="form-control select2" onchange="serial_number_change('{{ $i }}')" data-placeholder="Pilih serial number" style="width: 100%;" id="serial_number{{ $i }}" name="serial_number{{ $i }}">
                    <option value=""></option>
                    @foreach($produk_serial_number as $val)
                    <option value="{{ $val->serial_number }}">{{ $val->serial_number }}</option>
                    @endforeach
                </select>
            </td>
            <td align="center">
                <p id="display_stok{{ $i }}"></p>
                <input type="hidden" name="stok{{ $i }}" id="stok{{ $i }}">
            </td>
            <td align="center">
                <input type="text" name="jumlah{{ $i }}" id="jumlah{{ $i }}" class="form-control" value="1" onKeyPress="return numbersonly(this, event)" >
            </td>
        </tr>
        @endfor
    </tbody>
</table>
<link rel="stylesheet" href="{{ asset('/adminlte/plugins/select2/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/select2/select2.full.min.js') }}"></script>
<script>
  $(function () {
    $("#ajax_produk_tabless").DataTable();
    $(".select2").select2();
  });
</script>