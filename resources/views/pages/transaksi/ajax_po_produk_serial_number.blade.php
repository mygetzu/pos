<?php
    function rupiah($nominal)
    {
        $rupiah =  number_format($nominal,0, ",",".");
        $rupiah = "Rp "  . $rupiah;
        return $rupiah;
    }
?>
<table class="table table-striped table-bordered" id="ajax_produk_tabless2">
    <thead>
        <tr>
            <th style="text-align:center">NO</th>
            <th style="text-align:center">GUDANG</th>
            <th style="text-align:center">PRODUK</th>
            <th style="text-align:center">SERIAL NUMBER</th>
            <th style="text-align:center">JUMLAH</th>
            <td></td>
        </tr>
    </thead>
    <tbody>
        <?php $i=1; ?>
        @foreach($po_detail as $val)
        <tr>
            <td align="center">{{ $i++ }}</td>
            <td>{{ $val->gudang_nama }}</td>
            <td>{{ $val->produk_nama }}</td>
            <td>{{ $val->serial_number }}</td>
            <td align="center">{{ $val->quantity }}</td>
            <td align="center">
                <button class="btn btn-danger flat" onclick="sj_hapus_stok('{{ $val->serial_number }}', '{{ $val->quantity }}')"><span class="fa fa-trash"></span> Hapus</button>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
  $(function () {
    $("#ajax_produk_tabless2").DataTable();
  });
</script>