<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="nav-tabs-custom tab-primary flat">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">Sales Order Detail</a></li>
              <li><a href="#tab_2" data-toggle="tab">Sales Order Produk</a></li>
              <li><a href="#tab_3" data-toggle="tab">Sales Order Voucher</a></li>
              <li class="pull-right">
                <a href="{{ url('/sales_order') }}" class="pull-right" style="padding:0px">
                    <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                </a>
              </li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <table class="table table-striped">
                    <tr>
                        <th width="20%">No Sales Order</th>
                        <th width="5%">:</th>
                        <td><input type="text" class="form-control" value="{{ $so_header->no_sales_order }}" disabled="true"></td>
                    </tr>
                    <tr>
                        <th>Nama Pelanggan</th>
                        <th>:</th>
                        <td><input type="text" class="form-control" name="nama" id="nama" value="{{ $so_header->pelanggan->nama }}" disabled="true"></td>
                    </tr>
                    <tr>
                        <th>Tanggal</th>
                        <th>:</th>
                            <?php
                            $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
                            date_default_timezone_set("Asia/Jakarta");
                            $tanggal = new DateTime($so_header->tanggal);
                            $jatuh_tempo = new DateTime($so_header->due_date);
                            ?>
                        <td><input type="text" class="form-control" name="alamat" id="alamat" value="{{ $tanggal->format('d') }} {{ $bulan[(int)$tanggal->format('m')] }} {{ $tanggal->format('Y') }} {{ $tanggal->format('G') }}:{{ $tanggal->format('i') }}:{{ $tanggal->format('s') }}" disabled="true"></td>
                    </tr>
                    <tr>
                    </tr>
                    <tr>
                        <th>Catatan</th>
                        <th>:</th>
                        <td><textarea class="form-control" disabled="true">{{ $so_header->catatan }}</textarea></td>
                    </tr>
                    <tr>
                        <th>Total Tagihan</th>
                        <th>:</th>
                        <td><input type="text" class="form-control" value="{{ rupiah($so_header->total_tagihan) }}" disabled="true"></td>
                    </tr>
                </table>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
                <table class="table table-striped table-bordered" id="mydatatables">
                    <thead>
                        <tr>
                            <th style="text-align: center;">No.</th>
                            <th style="text-align: center;">Nama Produk</th>
                            <th style="text-align: center;">Satuan</th>
                            <th style="text-align: center;">Harga</th>
                            <th style="text-align: center;">Jumlah</th>
                            <th style="text-align: center;">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $jumlah_item=0; $total_akhir=0; ?>
                        @foreach($so_detail as $i => $val)
                        <tr>
                            <td align="center">{{ $i+1 }}</td>
                            <td align="left">
                                @if($val->jenis_barang_id == 1)
                                {{ $val->produk->nama }}
                                @elseif($val->jenis_barang_id == 2)
                                {{ $val->hadiah->nama }}
                                @elseif($val->jenis_barang_id == 3)
                                {{ $val->paket->nama }}
                                @endif
                            </td>
                            <td align="center">
                                @if($val->jenis_barang_id == 1)
                                {{ $val->produk->satuan }}
                                @elseif($val->jenis_barang_id == 2)
                                {{ $val->hadiah->satuan }}
                                @elseif($val->jenis_barang_id == 3)
                                <p>PKT</p>
                                @endif
                            </td>
                            <td align="right">{{ rupiah($val->harga) }}</td>
                            <td align="center">{{ $val->jumlah }}</td>
                            <td align="right">{{ rupiah($val->jumlah*$val->harga) }}</td>
                        </tr>
                        <?php $jumlah_item=$jumlah_item+$val->jumlah; 
                            $total_akhir = $total_akhir+ ($val->jumlah*$val->harga);
                        ?>
                        @endforeach
                    </tbody>
                </table>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_3">
                <table class="table table-striped table-bordered" id="mydatatables2">
                    <thead>
                        <th style="text-align:center">NO</th>
                        <th style="text-align:center">KODE VOUCHER</th>
                        <th style="text-align:center">NOMINAL</th>
                    </thead>
                    <tbody>
                        <?php $i=1; ?>
                        @foreach($so_voucher as $val)
                        <tr>
                            <td align="center">{{ $i++ }}</td>
                            <td align="center">{{ $val->kode }}</td>
                            <td align="center">{{ rupiah($val->nominal) }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
        
    </div>
</div>
@stop
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script>
  $(function () {
    $("#mydatatables").DataTable();
    $("#mydatatables2").DataTable();
  });
</script>