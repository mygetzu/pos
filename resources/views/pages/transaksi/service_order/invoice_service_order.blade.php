@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<?php
    function rupiah2($nominal)
    {
        $rupiah =  number_format($nominal,0, ",",".");
        $rupiah = $rupiah.',00';
        return $rupiah;
    }
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <a href="{{ url('/nota_service_order_print/'.$service_order->id) }}" target="_blank" class="btn bg-orange flat"><i class="fa fa-print"></i> Cetak Nota Service Order</a>
                <a href="{{ url('/nota_service_order_pdf/'.$service_order->id) }}" target="_blank" class="btn btn-info flat" style="margin-right: 5px;"><i class="fa fa-download"></i> Unduh PDF Nota Service Order</a>
                @if($service_order->tanggal_diambil == NULL)
                @else
                    <a href="{{ url('/invoice_service_order_print/'.$service_order->id) }}" target="_blank" class="btn bg-orange flat"><i class="fa fa-print"></i> Cetak Invoice Service Order</a>
                    <a href="{{ url('/invoice_service_order_print/'.$service_order->id) }}" target="_blank" class="btn bg-orange flat"><i class="fa fa-print"></i> Unduh PDF Invoice Service Order</a>
                @endif
            </div>
            <div class="box-body">
                <div class="area">
                    <section id="header">
                        <div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="title">{{ $judul }}</div>
                                    <div class="company">
                                        {{ $perusahaan->nama }} <br>
                                        {{ $perusahaan->alamat }} <br>
                                        Telp. {{ $perusahaan->telp }} <br>
                                        Email. {{ $perusahaan->email }}
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <table class="detail" style="margin-top: 33px">
                                        <tr>
                                            <td class="title-detail">No. Transaksi</td>
                                            <td class="titik">&nbsp;:&nbsp;</td>
                                            <td class="data-detail">{{ $service_order->no_nota }}</td>
                                        </tr>
                                        <tr>
                                            <td class="title-detail">Tanggal</td>
                                            <td class="titik">&nbsp;:&nbsp;</td>
                                            <td class="data-detail">
                                                <?php $tanggal = new DateTime($service_order->tanggal_diterima); 
                                                echo $tanggal->format('d/m/Y H:i');
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="title-detail">Pelanggan</td>
                                            <td class="titik">&nbsp;:&nbsp;</td>
                                            <td class="data-detail user">{{ $service_order->pelanggan->nama }}</td>
                                        </tr>
                                        <tr>
                                            <td class="title-detail" style="vertical-align: top;">Alamat </td>
                                            <td class="titik" style="vertical-align: top;">&nbsp;:&nbsp;</td>
                                            <td class="data-detail user">{{ $service_order->pelanggan->alamat }}<br>
                                            Telp. {{ $service_order->pelanggan->telp }}<br>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-3">
                                    <table class="detail">
                                        <tbody style="vertical-align: text-top;">
                                            <tr>
                                                <td class="title-detail">Garansi</td>
                                                <td class="titik">&nbsp;:&nbsp;</td>
                                                <td class="data-detail">{{ $service_order->garansi_service }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section id="table">
                        <div>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Model</th>
                                        <th>Serial Number</th>
                                        <th>Keluhan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <td>{{ $service_order->model_produk }}</td>
                                    <td>{{ $service_order->serial_number }}</td>
                                    <td>{{ $service_order->keluhan_pelanggan }}</td>
                                </tbody>
                            </table>
                        </div>
                    </section>
                    <section id="footer">
                        <div class="row">
                            <div class="col-md-4">
                                <table class="keterangan">
                                    <tr>
                                        <td class="title-detail" style="vertical-align: top;">Keterangan</td>
                                        <td class="titik" style="vertical-align: top;">&nbsp;:&nbsp;</td>
                                        <td class="data-detail">
                                        {{ $service_order->catatan }}
                                        <br><br></td>
                                    </tr>
                                    <tr>
                                        <td class="title-detail" style="vertical-align: top;">Terbilang</td>
                                        <td class="titik" style="vertical-align: top;">&nbsp;:&nbsp;</td>
                                        <td class="data-detail">{{ dibaca($service_order->jasa_service) }} Rupiah</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-4" style="padding: 0;">
                                <!-- <table class="keterangan" style="margin-bottom: 50px;">
                                    <tr>
                                        <td class="title-detail">Jml Item</td>
                                        <td class="titik" style="vertical-align: top;">&nbsp;:&nbsp;</td>
                                        <td class="data-detail"></td>
                                    </tr>
                                </table> -->
                                <div class="col-md-6">
                                    <div style="margin-top: 70px;">
                                        <div style="text-align: center">
                                            <strong>Penerima</strong>
                                        </div>
                                    </div>
                                    <div style="margin-top: 100px;">
                                        <div style="text-align: center">
                                            <strong>(.........................)</strong>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div style="margin-top: 70px;">
                                        <div style="text-align: center">
                                            <strong>Hormat Kami</strong>
                                        </div>
                                    </div>
                                    <div style="margin-top: 100px;">
                                        <div style="text-align: center">
                                            <strong>(.........................)</strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if(!empty($service_order->jasa_service))
                            <div class="col-md-4 right" style="padding-right: 23px;">
                                <table>
                                    <tr>
                                        <td class="title-detail price" style="text-align: left;" width="160px;">Jasa Servis</td>
                                        <td class="titik price" style="text-align: center;" width="100px;">&nbsp;:&nbsp;</td>
                                        <td class="data-detail right price">{{ rupiah2($service_order->jasa_service) }}</td>
                                    </tr>
                                </table>
                            </div>
                            @endif
                        </div>
                        <?php echo $print_time->format('d/m/Y G:i:s'); ?>
                    </section>
            </div>
        </div>
    </div>
</div>
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ URL::to('/css/invoice.css') }}">
@stop