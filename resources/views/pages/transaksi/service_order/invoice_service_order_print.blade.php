<link rel="stylesheet" type="text/css" href="{{ URL::to('/css/nota.css') }}">
@include('includes.base_function')
<?php
    function rupiah2($nominal)
    {
        $rupiah =  number_format($nominal,0, ",",".");
        $rupiah = $rupiah.',00';
        return $rupiah;
    }
?>
<style type="text/css">
    [class*="col-"] {
        float: left;
    }
    pre {
        font-family: sans-serif;
        background-color: transparent;
        border: none;
        overflow-x: hidden;
        white-space: pre-wrap;       /* css-3 */
        white-space: -moz-pre-wrap;  /* Mozilla, since 1999 */
        white-space: -pre-wrap;      /* Opera 4-6 */
        white-space: -o-pre-wrap;    /* Opera 7 */
        word-wrap: break-word;
        padding: 0px;
    }

    @font-face {
        font-family: mycalibri;
        src: url("{{ asset('fonts/Calibri.ttf') }}");
    }

    @font-face {
        font-family: mycalibri;
        src: url("{{ asset('fonts/Calibri-Bold.ttf') }}");
        font-weight: bold;
    }

    .area{
        font-family: mycalibri;
    }
</style>
<div class="area">
    <section id="header">
        <div>
            <div class="">
                <div class="col-4">
                    <div class="title">{{ $judul }}</div>
                    <div class="company">
                        {{ $perusahaan->nama }} <br>
                        {{ $perusahaan->alamat }} <br>
                        Telp. {{ $perusahaan->telp }} <br>
                        Email. {{ $perusahaan->email }}
                    </div>
                </div>
                <div class="col-5">
                    <table class="detail" style="margin-top: 33px">
                        <tr>
                            <td class="title-detail">No. Transaksi</td>
                            <td class="titik">&nbsp;:&nbsp;</td>
                            <td class="data-detail">{{ $service_order->no_nota }}</td>
                        </tr>
                        <tr>
                            <td class="title-detail">Tanggal</td>
                            <td class="titik">&nbsp;:&nbsp;</td>
                            <td class="data-detail">
                                <?php $tanggal = new DateTime($service_order->tanggal_diterima);
                                echo $tanggal->format('d/m/Y');
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="title-detail">Pelanggan</td>
                            <td class="titik">&nbsp;:&nbsp;</td>
                            <td class="data-detail user">{{ $service_order->pelanggan->nama }}</td>
                        </tr>
                        <tr>
                            <td class="title-detail" style="vertical-align: top;">Alamat </td>
                            <td class="titik" style="vertical-align: top;">&nbsp;:&nbsp;</td>
                            <td class="data-detail user">{{ $service_order->pelanggan->alamat }}<br>
                            Telp. {{ $service_order->pelanggan->telp }}<br>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col-3">
                    <table class="detail">
                        <tbody style="vertical-align: text-top;">
                            <tr>
                                <td class="title-detail">Garansi</td>
                                <td class="titik">&nbsp;:&nbsp;</td>
                                <td class="data-detail">{{ $service_order->garansi_service }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <section id="table">
        <div>
            <table class="table">
                <thead>
                    <tr>
                        <th>Model</th>
                        <th>Serial Number</th>
                        <th>Keluhan</th>
                    </tr>
                </thead>
                <tbody>
                    <td>{{ $service_order->model_produk }}</td>
                    <td>{{ $service_order->serial_number }}</td>
                    <td>{{ $service_order->keluhan_pelanggan }}</td>
                </tbody>
            </table>
        </div>
    </section>
    <section id="footer">
        <div class="">
            <div class="col-4">
                <table class="keterangan">
                    <tr>
                        <td class="title-detail" style="vertical-align: top;">Keterangan</td>
                        <td class="titik" style="vertical-align: top;">&nbsp;:&nbsp;</td>
                        <td class="data-detail">
                        {{ $service_order->catatan }}
                        <br><br></td>
                    </tr>
                    <tr>
                        <td class="title-detail" style="vertical-align: top;">Terbilang</td>
                        <td class="titik" style="vertical-align: top;">&nbsp;:&nbsp;</td>
                        <td class="data-detail">{{ dibaca($service_order->total_tagihan) }} Rupiah</td>
                    </tr>
                </table>
            </div>
            <div class="col-4" style="padding: 0;">
                <!-- <table class="keterangan" style="margin-bottom: 50px;">
                    <tr>
                        <td class="title-detail">Jml Item</td>
                        <td class="titik" style="vertical-align: top;">&nbsp;:&nbsp;</td>
                        <td class="data-detail"></td>
                    </tr>
                </table> -->
                <div class="col-6">
                    <div style="margin-top: 70px;">
                        <div style="text-align: center">
                            <strong>Penerima</strong>
                        </div>
                    </div>
                    <div style="margin-top: 100px;">
                        <div style="text-align: center">
                            <strong>(.........................)</strong>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div style="margin-top: 70px;">
                        <div style="text-align: center">
                            <strong>Hormat Kami</strong>
                        </div>
                    </div>
                    <div style="margin-top: 100px;">
                        <div style="text-align: center">
                            <strong>(.........................)</strong>
                        </div>
                    </div>
                </div>
            </div>
            @if(!empty($service_order->jasa_service))
            <div class="col-4 right" style="padding-right: 23px;">
                <table>
                    <tr>
                        <td class="title-detail price" style="text-align: left;" width="160px;">Jasa Servis</td>
                        <td class="titik price" style="text-align: center;" width="100px;">&nbsp;:&nbsp;</td>
                        <td class="data-detail right price">{{ rupiah2($service_order->jasa_service) }}</td>
                    </tr>
                </table>
            </div>
            @endif
        </div>
        <?php echo $print_time->format('d/m/Y G:i:s'); ?>
    </section>
</div>
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function(){
//          window.onload = function() { window.print(); }
      });
</script>