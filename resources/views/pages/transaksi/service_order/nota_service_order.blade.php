@extends('layouts.master_admin')
@section('content')
    @include('includes.base_function')
    <?php
    function rupiah2($nominal)
    {
        $rupiah = number_format($nominal, 0, ",", ".");
        $rupiah = $rupiah . ',00';
        return $rupiah;
    }
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary flat">
                <div class="box-header with-border">
                    <a href="{{ url('/nota_service_order_print/'.$service_order->id) }}" target="_blank" class="btn bg-orange flat"><i class="fa fa-print"></i> Cetak Nota Service Order</a>
                    <a href="{{ url('/nota_service_order_pdf/'.$service_order->id) }}" target="_blank" class="btn btn-info flat" style="margin-right: 5px;"><i class="fa fa-download"></i> Unduh PDF Nota Service Order</a>
                    @if($service_order->tanggal_diambil == NULL)
                    @else
                        <a href="{{ url('/invoice_service_order_print/'.$service_order->id) }}" target="_blank" class="btn bg-orange flat"><i class="fa fa-print"></i> Cetak Invoice Service Order</a>
                        <a href="{{ url('/invoice_service_order_print/'.$service_order->id) }}" target="_blank" class="btn bg-orange flat"><i class="fa fa-print"></i> Unduh PDF Invoice Service Order</a>
                    @endif
                </div>
                <div class="box-body">
                    <div class="area">
                        <section id="header">
                            <div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="company">
                                            <span style="font-size: 20px;text-transform: uppercase;">{{ $perusahaan->nama }}</span><br/>
                                            {{ $perusahaan->alamat }} <br>
                                            Telp. {{ $perusahaan->telp }}
                                        </div>
                                    </div>
                                    <div class="col-md-12"
                                         style="text-transform: uppercase; text-align: center; margin-bottom: 25px;">
                                        <span class="title">{{ $judul }}</span>
                                        <span class="pull-right"
                                              style="font-size: 22px; font-weight: bold; text-transform: uppercase">{{ $service_order->no_nota }}</span>
                                    </div>
                                    <div class="col-md-6">
                                        <p style="font-size: 16px; font-weight: bold; text-transform: uppercase;">
                                            Informasi Pelanggan</p>
                                        <table class="table table-condensed">
                                            <tr>
                                                <td style="width: 15%">Tanggal Terima</td>
                                                <td style="width: 65%">{{ date('d-m-Y H:i', strtotime($service_order->tanggal_diterima  )) }}</td>
                                            </tr>
                                            <tr>
                                                <td>Nama</td>
                                                <td>{{ $service_order->pelanggan->nama }}</td>
                                            </tr>
                                            <tr>
                                                <td>Telepon</td>
                                                <td>{{ $service_order->pelanggan->telp2 }}</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-md-6">
                                        <p style="font-size: 16px; font-weight: bold; text-transform: uppercase;">
                                            Informasi Unit</p>
                                        <table class="table table-condensed">
                                            <tr>
                                                <td>Model</td>
                                                <td>{{ $service_order->model_produk }}</td>
                                                <td>Processor</td>
                                                <td>-</td>
                                            </tr>
                                            <tr>
                                                <td>No. Seri</td>
                                                <td colspan="3">{{ $service_order->serial_number }}</td>
                                            </tr>
                                            <tr>
                                                <td>Garansi</td>
                                                <td>{{ ($service_order->status_garansi_produk == 1 ? 'Ya' : 'Tidak') }}</td>
                                                <td>Pembelian</td>
                                                <td>-</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-md-12">
                                        <p style="font-size: 16px; font-weight: bold; text-transform: uppercase;">
                                            Kelengkapan Unit</p>
                                        <table class="table table-condensed">
                                            <tr>
                                                <td style="width: 10%;">Baterai</td>
                                                <td style="width: 40%;">{{ $service_order->kelengkapan_baterai }}</td>
                                                <td style="width: 10%;">ODD</td>
                                                <td style="width: 40%;">{{ $service_order->kelengkapan_odd }}</td>
                                            </tr>
                                            <tr>
                                                <td>Adapter</td>
                                                <td>{{ $service_order->kelengkapan_adapter }}</td>
                                                <td>SC + RB</td>
                                                <td>{{ $service_order->kelengkapan_scrb }}</td>
                                            </tr>
                                            <tr>
                                                <td>Memory</td>
                                                <td>{{ $service_order->kelengkapan_memory }}</td>
                                                <td>Defect</td>
                                                <td rowspan="2">{{ $service_order->deskripsi_produk }}</td>
                                            </tr>
                                            <tr>
                                                <td>Harddisk</td>
                                                <td>{{ $service_order->kelengkapan_harddisk }}</td>
                                            </tr>
                                            <tr>
                                                <td>Lainnya</td>
                                                <td>-</td>
                                                <td>Lainnya</td>
                                                <td>
                                                    @if($service_order->kelengkapan_wifi == 0)
                                                        <span style="text-decoration: line-through;">Wifi</span>
                                                    @else
                                                        <span>Wifi</span>
                                                    @endif -
                                                    @if($service_order->kelengkapan_modem == 0)
                                                        <span style="text-decoration: line-through;">Modem</span>
                                                    @else
                                                        <span>Modem</span>
                                                    @endif -
                                                    @if($service_order->kelengkapan_lan == 0)
                                                        <span style="text-decoration: line-through;">LAN</span>
                                                    @else
                                                        <span>LAN</span>
                                                    @endif -
                                                    @if($service_order->kelengkapan_bluetooth == 0)
                                                        <span style="text-decoration: line-through;">Bluetooth</span>
                                                    @else
                                                        <span>Bluetooth</span>
                                                    @endif
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-md-6">
                                        <p style="font-size: 16px; font-weight: bold; text-transform: uppercase;">
                                            Keluhan Pelanggan</p>
                                        <table class="table table-condensed">
                                            <tr>
                                                <td>{{ $service_order->keluhan_pelanggan }}</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-md-6">
                                        <p style="font-size: 16px; font-weight: bold; text-transform: uppercase;">
                                            Catatan Pelanggan</p>
                                        <table class="table table-condensed">
                                            <tr>
                                                <td>{{ ($service_order->catatan == NULL ? '-' : $service_order->catatan) }}</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-md-12">
                                        <p>Catatan :</p>
                                        <ol>
                                            <li>Kami tidak bertanggung jawab atas program - program yang terinstal pada
                                                notebook atau PC.
                                            </li>
                                            <li>Garansi servis berlaku 30 hari sejak barang diambil.</li>
                                            <li>Kami tidak bertanggung jawab apabila dalam 30 hari barang tidak diambil
                                                setelah servis.
                                            </li>
                                        </ol>
                                    </div>

                                </div>
                            </div>
                        </section>
                        <section id="footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-4" style="text-align: center;">
                                        <p style="font-weight: bold;">Diserahkan,</p>
                                        <br/>
                                        <p>
                                            <span>_____________</span>
                                            <br/>
                                            <span>Tgl, {{ ($service_order->tanggal_diterima == NULL ? '' : date('d-m-Y H:i', strtotime($service_order->tanggal_diterima))) }}</span>
                                        </p>
                                    </div>
                                    <div class="col-md-4" style="text-align: center;">
                                        <p style="font-weight: bold;">Diterima,</p>
                                        <br/>
                                        <p>
                                            <span>_____________</span>
                                            <br/>
                                            <span>Tgl, {{ ($service_order->tanggal_diperbaiki == NULL ? '' : date('d-m-Y H:i', strtotime($service_order->tanggal_diperbaiki))) }}</span>
                                        </p>
                                    </div>
                                    <div class="col-md-4" style="text-align: center;">
                                        <p style="font-weight: bold;">Diambil,</p>
                                        <br/>
                                        <p>
                                            <span>_____________</span>
                                            <br/>
                                            <span>Tgl, {{ ($service_order->tanggal_diambil == NULL ? '' : date('d-m-Y H:i', strtotime($service_order->tanggal_diambil))) }}</span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ URL::to('/css/invoice.css') }}">
@stop