<?php
    function rupiah($nominal)
    {
        $rupiah =  number_format($nominal,0, ",",".");
        $rupiah = "Rp "  . $rupiah;
        return $rupiah;
    }
?>
<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th width="5%" style="text-align:center">NO</th>
            <th style="text-align:center">NAMA PRODUK</th>                    
            <th style="text-align:center">SERIAL NUMBER</th>
            <th style="text-align:center">HARGA</th>
            <th style="text-align:center">JUMLAH</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php $i = 1; $jumlah=0; $total_bayar=0;?>
        @foreach($cart_content as $cart)
        <tr>
            <td align="center">{{ $i++ }}</td>
            <td>{{ $cart->name }}</td>
            <td>
                <?php
                    $produk = substr($cart->id, 3);
                    $serial_number = explode("---", $produk);
                ?> 
                {{ $serial_number[1] }}
            </td>
            <td align="right" style="padding-right:20px">{{ rupiah($cart->price) }}</td>
            <td align="center">{{ $cart->qty }}</td>
            <td align="center">
              <a class="btn btn-danger flat" onclick="hapus('{{ $cart->rowId }}')"><span class="glyphicon glyphicon-trash"></span> Hapus</a>
            </td>
        </tr>
        <?php 
            $jumlah += $cart->qty;
            $total_bayar += ($cart->price*$cart->qty);
        ?>
        @endforeach
        <tr>
            <td colspan="4" align="center"><b>Total</b></td>
            <td align="right" style="padding-right:20px"><p><b>{{ rupiah($total_bayar) }}</b></p></td>
            <td></td>
        </tr>
    </tbody>
</table>