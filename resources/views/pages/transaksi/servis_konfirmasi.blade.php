<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="author" content="Indonusa Solutama">
    <meta name="description"
          content="Aplikasi Point of Sale - Digunakan untuk memanajemen arus keluar masuk barang toko.">
    <title>POS - Konfirmasi Pelanggan</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ asset('/adminlte/bootstrap/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('/font-awesome-4.7.0/css/font-awesome.min.css') }}">
    <!-- Admin LTE -->
    <link rel="stylesheet" href="{{ asset('/adminlte/dist/css/skins/_all-skins.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/adminlte/dist/css/AdminLTE.min.css') }}">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="{{ asset('/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
    <!-- Sweetalert -->
    <link rel="stylesheet" href="{{ URL::asset('sweetalert-master/dist/sweetalert.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('sweetalert-master/themes/twitter/twitter.css') }}">
</head>
<body class="hold-transition skin-blue sidebar-mini" style="width: 75%; margin: 0 auto;">
<div class="wrapper">
    <header class="main-header">
        <!-- Logo -->
        <div class="logo" title="Kunjungi halaman utama" style="background-color: #3C8DBC;">
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Servis</b> - POS</span>
        </div>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="hidden">
                        <a href="#" data-toggle="control-sidebar">
                            Galerindo Teknologi - Perbaikan Perangkat
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <div class="content" style="background-color: #ECF0F5;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            @if($data['status'] == 'check')
                <h1>Informasi Perbaikan</h1>
            @elseif($data['status'] == 'confirm')
                <h1>Konfirmasi Persetujuan Perbaikan</h1>
            @elseif($data['status'] == 'info')
                <h1>Informasi Perbaikan</h1>
            @elseif($data['status'] == 'cancel')
                <h1>Pembatalan Perbaikan</h1>
            @elseif($data['status'] == 'get_device')
                <h1>Pengambilan Perangkat</h1>
            @elseif($data['status'] == '404')
                <h1>Kesalahan 404 - Halaman tidak ditemukan</h1>
            @endif
        </section>

        <!-- Main content -->
        <section class="content">
            @if($data['status'] == '404')
            <div class="row">
                <div class="col-md-12" style="text-align: center;">
                    <i class="fa fa-close fa-3x" style="color: #00C0EF;"></i>
                    <p style="font-size: 20px;">Data tidak ditemukan</p>
                </div>
            </div>
            @else
            <div class="row">
                @if($data['status'] == 'check')
                    <div class="callout callout-info">
                        <p style="font-weight: none;">Perangkat Anda sedang dilakukan pengecekan oleh teknisi.</p>
                    </div>
                @elseif($data['status'] == 'confirm')
                    <div class="callout callout-warning">
                        <p style="font-weight: none;">Anda belum memberikan jawaban (ACC) untuk perbaikan perangkat
                            Anda.</p>
                    </div>
                @elseif($data['status'] == 'info')
                    <div class="callout callout-success">
                        <p style="font-weight: none;">Perbaikan perangkat Anda sedang dikerjakan.</p>
                    </div>
                @elseif($data['status'] == 'cancel')
                    <div class="callout callout-danger">
                        <p style="font-weight: none;">Perbaikan perangkat Anda telah dibatalkan.</p>
                    </div>
                @elseif($data['status'] == 'get_device')
                    <div class="callout callout-success">
                        <p style="font-weight: none;">Perangkat Anda siap diambil.</p>
                    </div>
                @endif
                <div class="col-md-6">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Data Pelanggan</h3>
                        </div>
                        <div class="box-body">
                            <div class="col-md-4">
                                <p>Nama Pelanggan</p>
                                <p>Email Pelanggan</p>
                                <p>Alamat Pelanggan</p>
                                <p>No. Ponsel Pelanggan</p>
                                <p class="hidden-xs">&nbsp;</p>
                                <p class="hidden-xs">&nbsp;</p>
                                <p class="hidden-xs">&nbsp;</p>
                            </div>
                            <div class="col-md-8">
                                <p>{{ $data['pelanggan']->nama }}</p>
                                <p>{{ $data['pelanggan']->email }}</p>
                                <p>{{ $data['pelanggan']->alamat }}</p>
                                <p>{{ $data['pelanggan']->telp2 }}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Data Kerusakan</h3>
                        </div>
                        <div class="box-body">
                            <div class="col-md-4">
                                <p>No. Nota</p>
                                <p>Tanggal</p>
                                <p>Model Produk</p>
                                <p>No. Serial Produk</p>
                                <p>Status Garansi Produk</p>
                                <p>Keluhan Pelanggan</p>
                                <p>Deskripsi Produk</p>
                            </div>
                            <div class="col-md-8">
                                <p>{{ $data['service']->no_nota }}</p>
                                <p>{{ date('d-M-Y', strtotime($data['service']->tanggal_diterima)) }}</p>
                                <p>{{ $data['service']->model_produk }}</p>
                                <p>{{ $data['service']->serial_number }}</p>
                                <p>{{ ($data['service']->status_garansi_produk == 0 ? 'Tidak' : 'Ya')  }}</p>
                                <p>{{ $data['service']->keluhan_pelanggan }}</p>
                                <p>{{ $data['service']->deskripsi_produk }}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Data Perbaikan</h3>
                        </div>
                        <div class="box-body">
                            @if($data['status'] == 'check' || $data['status'] == 'cancel' || $data['count_invoice_header'] == 0)
                            @else
                                <table class="table table-stripped">
                                    <thead>
                                    <tr>
                                        <th style="width: 5%">No.</th>
                                        <th width="">Tindakan Perbaikan</th>
                                        <th style="width: 25%;">Biaya</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data['invoice_detail'] as $key => $value)
                                        <tr>
                                            <td>{{ ++$key }}</td>
                                            <td>{{ $value->tindakan }}</td>
                                            <td>Rp. {{ number_format($value->biaya, 0, ',' , '.') }}</td>
                                        </tr>
                                    @endforeach
                                    <tr style="font-size: 18px;">
                                        <td colspan="2" align="right">Total</td>
                                        <td>
                                            Rp. {{ number_format($data['invoice_header']->harga_total, 0, ',' , '.') }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            @endif
                            <div class="row" style="margin-top: 35px;">
                                @if($data['status'] == 'check')
                                    <div class="col-md-12" style="text-align: center;">
                                        <i class="fa fa-info-circle fa-3x" style="color: #00C0EF;"></i>
                                        <p style="font-size: 20px;">Perangkat Anda sedang dicek oleh teknisi</p>
                                        <div class="col-md-push-4 col-md-4">
                                            <button class="btn btn-danger btn-flat btn-block"
                                                    onclick="cancelConfirm('{{ $data['service']->no_nota }}', '{{ $data['service']->kode_verifikasi }}')">
                                                Batalkan perbaikan
                                            </button>
                                        </div>
                                    </div>
                                @elseif($data['status'] == 'confirm')
                                    <div class="col-md-12" style="text-align: center;">
                                        <i class="fa fa-warning fa-3x" style="color: #F39C12;"></i>
                                        <p style="font-size: 20px;">Apakah Anda menyetujui perbaikan perangkat Anda?</p>
                                        <div class="col-md-3 hidden-xs">
                                        </div>
                                        <div class="col-md-3 col-xs-6">
                                            <button class="btn bg-maroon btn-flat btn-block pull-right"
                                                    onclick="cancelConfirm('{{ $data['service']->no_nota }}', '{{ $data['service']->kode_verifikasi }}')">
                                                Tidak, Saya tidak setuju.
                                            </button>
                                        </div>
                                        <div class="col-md-3 col-xs-6">
                                            <button class="btn bg-olive btn-flat btn-block"
                                                    onclick="submitConfirm('{{ $data['service']->no_nota }}', '{{ $data['service']->kode_verifikasi }}')">
                                                Ya, Saya setuju.
                                            </button>
                                        </div>
                                        <div class="col-md-3 hidden-xs">
                                        </div>
                                    </div>
                                @elseif($data['status'] == 'info')
                                    <div class="col-md-12" style="text-align: center;">
                                        <i class="fa fa-gears fa-3x" style="color: #00C0EF;"></i>
                                        <p style="font-size: 20px;">Perangkat Anda sedang diperbaiki oleh teknisi</p>
                                    </div>
                                @elseif($data['status'] == 'cancel')
                                    <div class="col-md-12" style="text-align: center;">
                                        <i class="fa fa-close fa-3x" style="color: #00C0EF;"></i>
                                        <p style="font-size: 20px;">Anda telah membatalkan perbaikan</p>
                                    </div>
                                @elseif($data['status'] == 'get_device')
                                    <div class="col-md-12" style="text-align: center;">
                                        <i class="fa fa-smile-o fa-3x"></i>
                                        <p style="font-size: 20px;">Terima kasih telah kepercayaan Anda kepada kami</p>
                                        <p>Anda dapat mengambil perangkat Anda saat ini.</p>
                                        <div class="col-md-3 hidden-xs">
                                        </div>
                                        <div class="col-md-6 col-xs-6 hidden" >
                                            {{--<button class="btn bg-olive btn-flat btn-block"--}}
                                                    {{--onclick="getDevice('{{ $data['service']->no_nota }}', '{{ $data['service']->kode_verifikasi }}')">--}}
                                                {{--Ya, Saya ingin mengambil barang.--}}
                                            {{--</button>--}}
                                        </div>
                                        <div class="col-md-3 hidden-xs">
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </section>
    </div>
</div>
<!-- jQuery 2.2.3 -->
<script src="{{ asset('/adminlte/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset('/adminlte/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<!-- Slimscroll -->
<script src="{{ asset('/adminlte/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('/adminlte/plugins/fastclick/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('/adminlte/dist/js/app.min.js') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ asset('/adminlte/dist/js/pages/dashboard.js') }}"></script>
<!-- Sweetalert -->
<script src="{{ URL::asset('sweetalert-master/dist/sweetalert.min.js') }}"></script>
<script type="text/javascript">
    function cancelConfirm(no_nota, kode_verifikasi) {
        swal({
                title: "Anda yakin membatalkan perbaikan?",
                text: "Setelah Anda membatalkan perbaikan, seluruh proses akan kami hentikan dan Anda tidak dapat melanjutkan servis!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya, Batalkan perbaikan!",
                cancelButtonText: "Tidak!",
                closeOnConfirm: true,
                html: false
            }, function (isConfirm) {
                if (isConfirm) {
//                    window.location.href = 'http://www.youtube.com';
                    window.location.href = '{{ url('/')}}/service_confirm?action=cancel&nota=' + no_nota + '&verification_code=' + kode_verifikasi;
                }
            }
        )
    }

    function submitConfirm(no_nota, kode_verifikasi) {
        swal({
                title: "Anda yakin menyetujui perbaikan?",
                text: "Setelah Anda menyetujui perbaikan, kami akan mengirimkan Anda Invoice Servis dan perbaikan akan kami kerjakan!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya, Saya setuju perbaikan!",
                cancelButtonText: "Tidak!",
                closeOnConfirm: true,
                html: false
            }, function () {
//                window.location.href = 'http://www.google.com';
                    window.location.href = '{{ url('/')}}/service_confirm?action=verify&nota=' + no_nota + '&verification_code=' + kode_verifikasi;
            }
        )
    }
</script>
</body>
</html>