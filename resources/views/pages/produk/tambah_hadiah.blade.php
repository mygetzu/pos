@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <div class="pull-right">
                    <a href="{{ url('/hadiah') }}">
                        <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                    </a>
                </div>
            </div>
            <div class="box-body">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <div id="message"></div>
                <form class="form-horizontal" method="POST" action="{{ url('/do_tambah_hadiah') }}" autocomplete="off" enctype="multipart/form-data" id="form_tambah_hadiah">
                    <input type="hidden" name="desk" id="desk">
                    <table class="table table-striped">
                        {{ csrf_field() }}
                        <br>
                        <tr class="{{ $errors->has('nama') ? ' has-error' : '' }}">
                            <th width="20%"><label for="name" class="control-label">Nama*</label></th>
                            <th width="5%">:</th>
                            <td>
                            <div class="col-md-12">
                                <input type="text" class="form-control" name="nama" id="nama" value="{{ old('nama') }}" placeholder="Masukkan Nama Hadiah" style="text-transform:capitalize">
                                @if ($errors->has('nama'))
                                    <span class="help-block">
                                        <strong>Kolom Nama Wajib Diisi</strong>
                                    </span>
                                @endif
                            </div>
                            </td>
                        </tr>
                        <tr class="{{ $errors->has('satuan') ? ' has-error' : '' }}">
                            <th><label class="control-label">Satuan*</label></th>
                            <th>:</th>
                            <td>
                            <div class="col-md-12">
                                <input type="text" class="form-control" name="satuan" id="satuan" value="{{ old('satuan') }}" placeholder="Masukkan Satuan" style="text-transform:capitalize">
                                @if ($errors->has('satuan'))
                                    <span class="help-block">
                                        <strong>Kolom Satuan Wajib Diisi</strong>
                                    </span>
                                @endif
                            </div>
                            </td>
                        </tr>
                        <tr class="{{ $errors->has('berat') ? ' has-error' : '' }}">
                            <th><label class="control-label">Berat*</label></th>
                            <th>:</th>
                            <td>
                            <div class="col-md-3">
                                <input type="text" class="form-control" name="berat" id="berat" value="{{ old('berat') }}"onKeyPress="return numbersdecimal(this, event)" placeholder="Masukkan Nilai Berat" style="text-align:right">
                                @if ($errors->has('berat'))
                                    <span class="help-block">
                                        <strong>Kolom Berat Wajib Diisi</strong>
                                    </span>
                                @endif
                            </div>
                            <label class="control-label">Kg</label>
                            </td>
                        </tr>
                        <tr class="{{ $errors->has('gudang') ? ' has-error' : '' }}">
                            <th><label class="control-label">Gudang*</label></th>
                            <th>:</th>
                            <td>
                            <div class="col-md-12">
                                <select class="form-control" name="gudang">
                                    <option value="">-- Pilih Gudang --</option>
                                    @foreach($gudang as $val)
                                    <option value="{{ $val->id }}">{{ $val->nama }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('gudang'))
                                    <span class="help-block">
                                        <strong>Pilihan Gudang Wajib Diisi</strong>
                                    </span>
                                @endif
                            </div>
                            </td>
                        </tr>
                        <tr class="{{ $errors->has('deskripsi') ? ' has-error' : '' }}">
                            <th><label class="control-label">Deskripsi*</label></th>
                            <th>:</th>
                            <td>
                            <div class="col-md-12">
                                <textarea type="text" class="form-control" name="deskripsi" id="deskripsi" placeholder="Masukkan Deskripsi">{{ old('deskripsi') }}</textarea>
                                @if ($errors->has('deskripsi'))
                                    <span class="help-block">
                                        <strong>Kolom Deskripsi Wajib Diisi</strong>
                                    </span>
                                @endif
                            </div>
                            </td>
                        </tr>
                        <tr class="{{ $errors->has('insert_image') ? ' has-error' : '' }}">
                            <th><label class="control-label">Gambar</label></th>
                            <th>:</th>
                            <td>
                            <div class="col-md-12">
                                <input type="file" class="form-control" name="insert_image" id="insert_image">
                                @if ($errors->has('insert_image'))
                                    <span class="help-block">
                                        <strong>Gambar Wajib Disertakan</strong>
                                    </span>
                                @endif
                            </div>
                            </td>
                        </tr>
                        <tr>
                            <th><label class="control-label"></label></th>
                            <th></th>
                            <td>
                            <div class="col-md-12" id="button_submit">
                                <button type="submit" class="btn btn-primary flat" onclick="document.getElementById('loading').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
                                <button type="button" class="btn btn-primary flat" onclick="btn_simpan_duplikasi()" style="margin-left:10px"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan dan Duplikasi</button>
                            </div>
                            </td>
                        </tr>
                    </table>
                    </form>
                    <p>Keterangan (*) : Wajib Diisi</p>
            </div>
            <div class="overlay" id="loading" style="display:none">
              <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>
@stop
<script src="{{ asset('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<!-- TINYMCE -->
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
  tinymce.init({ selector:'textarea',
  height: 400,
  plugins: [
    'advlist autolink lists link image charmap print preview anchor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste code'
  ],
  toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image' });
</script>
<script type="text/javascript">
    function btn_simpan_duplikasi()
    {
        var deskripsi       = tinyMCE.activeEditor.getContent();
        document.getElementById("desk").value = deskripsi;

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })

        var url = "{{ url('/do_tambah_hadiah_by_ajax') }}";

        $.ajax({
            type: "POST",
            url: url,
            data: new FormData($("#form_tambah_hadiah")[0]),
            cache:false,
            contentType: false,
            processData: false,
            beforeSend: function(){
                document.getElementById('loading').style.display = "block";
            },
            success: function (data) {
                document.getElementById("message").innerHTML = data;
                document.getElementById('loading').style.display = "none";
            },
            error: function (data) {
                alert('Lengkapi dulu data yang wajib diisi');
                document.getElementById('loading').style.display = "none";
            }
        });
    }
</script>