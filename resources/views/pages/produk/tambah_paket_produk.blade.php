@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<input type="hidden" name="base_path" id="base_path" value="{{ asset('/') }}">
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <div class="pull-right">
                    <a href="{{ url('/paket_produk') }}">
                        <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                    </a>
                </div>
            </div>
            <div class="box-body">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <form class="form-horizontal" method="POST" action="{{ url('/do_tambah_paket_produk') }}" autocomplete="off" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="jumlah_produk" id="jumlah_produk" value="0">
                    <input type="hidden" name="mypaketproduk" id="mypaketproduk" value="">
                    <div class="box-body">
                        <div class="form-group{{ $errors->has('kode_bar') ? ' has-error' : '' }}">
                            <label class="control-label col-sm-2">Kode Paket</label>
                            <div class="col-sm-6">
                                <input id="name" type="text" class="form-control" name="kode" id="kode" value="{{ old('kode') }}" placeholder="Masukkan Kode Paket" style="text-transform:capitalize">
                                @if ($errors->has('kode'))
                                    <span class="help-block">
                                        <strong>Kolom Kode Paket Wajib Diisi</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                            <label class="control-label col-sm-2">Nama Paket*</label>
                            <div class="col-sm-6">
                                <input id="name" type="text" class="form-control" name="nama" id="nama" value="{{ old('nama') }}" placeholder="Masukkan Nama Paket" style="text-transform:capitalize">
                                @if ($errors->has('nama'))
                                    <span class="help-block">
                                        <strong>Kolom Nama Wajib Diisi</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Harga Rekomendasi</label>
                            <div class="col-sm-6">
                                <input id="harga_rekomendasi" type="text" class="form-control" disabled="true">
                                <input id="harga_rekomendasi2" type="hidden" class="form-control">
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('harga_total') ? ' has-error' : '' }}">
                            <label class="control-label col-sm-2">Harga*</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="harga_total" name="harga_total" placeholder="harga" id="harga" data-thousands="." data-decimal="," style="text-align:right" value="{{ old('harga_total') }}"/>
                                <script type="text/javascript">$("#harga_total").maskMoney({precision:0});</script>
                                @if ($errors->has('harga_total'))
                                    <span class="help-block">
                                        <strong>Kolom Harga Total Wajib Diisi</strong>
                                    </span>
                                @endif
                            </div>
                            <label class="control-label">(IDR)</label>
                        </div>
                        <div id="produk_list" class="row">
                            <div class="col-xs-6 col-md-3">
                                <a data-toggle="modal" data-target="#add_produk" class="thumbnail flat" style="height:200px;text-align:center;background:white;cursor: pointer;border-color:maroon">
                                  <span class="glyphicon glyphicon-plus" style="margin-top:80px;color:maroon;"></span>
                                </a>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                            <label class="control-label col-sm-2">Gambar Paket</label>
                            <div class="col-sm-6">
                                <input type="file" class="form-control" name="image" id="image">
                                @if ($errors->has('image'))
                                    <span class="help-block">
                                        <strong>Gambar Wajib Disertakan</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                            <div class="col-sm-2 col-sm-offset-2">
                                <button class="btn btn-primary flat" onclick="document.getElementById('loading').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
                            </div>
                        </div>
                    </div>
                </form>
                <p>Keterangan (*) : Wajib Diisi</p>
            </div>
            <div class="overlay" id="loading" style="display:none">
              <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="add_produk" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="riwayatheader">Tambah Produk</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" id="form_produk" method="post" action="">
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Pilih Kategori</label>
            <div class="col-md-10">
                <select class="form-control" name="kategori_id" id="kategori_id">
                    <option value="">-- Semua Kategori --</option>
                    @foreach($kategori_produk as $p)
                    <option value="{{ $p->id }}">{{ $p->nama }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <hr>
        <div id="content_produk">
        <table class="table table-striped table-bordered" id="mydatatables">
            <thead>
                <th style="text-align:center">NO</th>
                <th style="text-align:center">JENIS</th>
                <th style="text-align:center">NAMA</th>
                <th style="text-align:center">HARGA RETAIL</th>
                <th style="text-align:center"></th>
            </thead>
            <tbody>
                <?php $i=1; ?>
                @foreach($produk as $val)
                <tr>
                    <td align="center">{{ $i++ }}</td>
                    <td>@if(substr($val->kode,0,3) == 'PKT')<p>PAKET</p>@else<p>PRODUK</p>@endif</td>
                    <td>{{ $val->nama }}</td>
                    <td>{{ rupiah($val->harga_retail) }}</td>
                    <td align="center">
                        <?php 
                        if (!empty($val->produk_galeri_first->file_gambar)) {
                            $file_gambar = $val->produk_galeri_first->file_gambar;
                        } else {
                            $file_gambar = $gambar_produk_default;
                        }
                        ?>
                        <button class="btn bg-maroon flat" type="button" onclick="pilih_produk('{{ $val->id }}', '{{ $val->harga_retail }}', '{{ $val->nama }}', '{{ $file_gambar }}')" data-dismiss="modal"><span class="fa fa-check"></span> Pilih</button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        </div>
        </form>
    </div>
  </div>
</div>
</div>

@stop

<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<script src="{{ asset('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ url('js/jquery.maskMoney.js') }}" type="text/javascript"></script>
<script>
  $(function () {
    $("#mydatatables").DataTable();
  });
</script>
<script type="text/javascript">
    function rupiah(nominal)
    {
        //rumus konversi rupiah
        nominal = "Rp "+nominal.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
        return nominal;
    }

    function pilih_produk(id, harga_retail, nama, file_gambar)
    {
        document.getElementById('loading').style.display = "none";
        var n = document.getElementById("jumlah_produk").value;
        var m = document.getElementById("mypaketproduk").value;
        var o = document.getElementById("harga_rekomendasi2").value;
        var div = document.createElement('div');
        n = parseInt(n);
        n++;
        harga_retail = parseInt(harga_retail);
        document.getElementById("jumlah_produk").value = n;
        if(m == "")
            document.getElementById("mypaketproduk").value = id;
        else
            document.getElementById("mypaketproduk").value = m+"-"+id;
        if(o == ""){

            document.getElementById("harga_rekomendasi").value = rupiah(harga_retail);
            document.getElementById("harga_rekomendasi2").value = harga_retail;
        }
        else{
            o = parseInt(o);
            document.getElementById("harga_rekomendasi").value = rupiah(o+harga_retail);
            document.getElementById("harga_rekomendasi2").value = o+harga_retail;
        }
        div.innerHTML = '<div class="col-xs-6 col-md-3">\
                <a class="thumbnail flat" style="height:200px;text-align:center;background:white;color:maroon">\
                <img src="" id="img_'+n+'" style="height:130px;width:auto;padding: 15px 15px 0px 15px;" class="img-responsive">'
                + nama +
                '</a></div>';
         document.getElementById('produk_list').appendChild(div);
         aa = 'img_';
         var path =  document.getElementById('base_path').value;

         document.getElementById(aa.concat(n)).src = path+"img/produk/"+ file_gambar;
    }

    $(document).ready(function(){
          $('#kategori_id').change(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            var url = "{{ url('/get_produk') }}";
            
              $.ajax({
                type: "POST",
                url: url,
                data: $("#form_produk").serialize(),
                success: function (data) {

                    $("#content_produk").html(data);
                },
                error: function (data) {
                    alert('ooo');
                }
            });
          });

          $('#target_customer').change(function(){
            if(document.getElementById("target_customer").value == 1){
              document.getElementById("label1_pilih_customer").style.display = 'none';
              document.getElementById("label2_pilih_customer").style.display = 'none';
              document.getElementById("button_pilih_customer").style.display = 'none';
            }
            else{
              document.getElementById("label1_pilih_customer").style.display = 'block';
              document.getElementById("label2_pilih_customer").style.display = 'block';
              document.getElementById("button_pilih_customer").style.display = 'block';
            }
          });
      });
</script>