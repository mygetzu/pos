@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<input type="hidden" name="ori_nama" id="ori_nama" value="{{ $paket->nama }}">
<input type="hidden" name="ori_kode" id="ori_kode" value="{{ $paket->kode }}">
<div class="row">
    <div class="col-md-12">
        <div class="nav-tabs-custom box tab-primary flat" style="border-top: 0">
            <ul class="nav nav-tabs">
              <li><a href="#tab_1" data-toggle="tab">Detail Paket</a></li>
              <li><a href="#tab_2" data-toggle="tab">Data Produk</a></li>
              <li class="pull-right">
                <a href="{{ url('/paket_produk') }}" style="padding:0px">
                    <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                </a>
              </li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane" id="tab_1">
                <div id="ubah" style="display:block">
                    <div class="">
                      <button class="btn btn-warning flat" onclick="ubah('{{ $paket->harga_total }}')"><span class='fa fa-edit'></span> Ubah</button>
                    </div>
                </div>
                <div id="batal" style="display:none">
                    <div class="">
                      <button class="btn btn-default flat" onclick="batal('{{ rupiah($paket->harga_total) }}')"><span class='fa fa-ban'></span> Batal</button>
                    </div>
                </div>
                <br>
                @if(Session::has('message1'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message1') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                @if ($errors->has('kode_paket'))
                <div class="alert alert-danger flat">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    {{ $errors->first('kode_paket') }}
                </div>
                @endif
                @if ($errors->has('kode_produk'))
                <div class="alert alert-danger flat">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    {{ $errors->first('kode_produk') }}
                </div>
                @endif
                <form class="form-horizontal" action="{{ url('/ubah_paket_produk') }}" method="post" autocomplete="off">
                    <input type="hidden" name="id" value="{{ $paket->id }}">
                    {{ csrf_field() }}
                    <div class="box-body">
                        <div class="form-group{{ $errors->has('kode') ? ' has-error' : '' }}">
                            <label class="control-label col-sm-2">Kode<span id="ast0"></span></label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="kode" id="kode" value="{{ $paket->kode }}" disabled="true" placeholder="Masukkan Kode" style="text-transform:capitalize">
                                @if ($errors->has('kode'))
                                    <span class="help-block">
                                        <strong>Kolom Kode Wajib Diisi</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                            <label class="control-label col-sm-2">Nama<span id="ast1"></span></label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="nama" id="nama" value="{{ $paket->nama }}" disabled="true" placeholder="Masukkan Nama" style="text-transform:capitalize">
                                @if ($errors->has('nama'))
                                    <span class="help-block">
                                        <strong>Kolom Nama Wajib Diisi</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('harga_total') ? ' has-error' : '' }}">
                            <label class="control-label col-sm-2">Harga Total<span id="ast2"></span></label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="harga_total" name="harga_total" placeholder="harga" id="harga" data-thousands="." data-decimal="," style="text-align:right;" value="{{ rupiah($paket->harga_total) }}" disabled="true" />
                                <script type="text/javascript">$("#harga_total").maskMoney({precision:0});</script>
                                @if ($errors->has('harga_total'))
                                    <span class="help-block">
                                        <strong>Kolom Harga Total Wajib Diisi</strong>
                                    </span>
                                @endif
                            </div>
                            <label class="control-label">(IDR)</label>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-10 col-sm-offset-2">
                                <button type="submit" class="btn btn-primary flat" id="button_submit" style="display:none" onclick="document.getElementById('loading').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
                            </div>
                        </div>
                    </div>
                </form>
                <label id="astket" class="control-label"></label>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
                <div>
                    <a data-toggle="modal" data-target="#tambah_produk">
                        <button class='btn bg-purple flat'><span class='fa fa-plus'></span> Tambah Produk</button>
                    </a>
                </div>
                <br>
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <table class="table table-striped table-bordered" id="mydatatables">
                    <thead>
                        <th style="text-align:center">NO</th>
                        <th style="text-align:center">NAMA PRODUK</th>
                        <th style="text-align:center">JUMLAH</th>
                        <th style="text-align:center">GAMBAR PRODUK</th>
                        <th style="text-align:center"></th>
                    </thead>
                    <tbody>
                        <?php $i=1; ?>
                        @foreach($paket->paket_produk as $val)
                        <tr>
                            <td align="center">{{ $i++ }}</td>
                            <td>{{ $val->produk->nama }}</td>
                            <td align="center">{{ $val->qty_produk }}</td>
                            <td align="center">
                                @if(!empty($val->produk->produk_galeri_first->file_gambar))
                                <img src="{{ URL::asset('img/produk/'.$val->produk->produk_galeri_first->file_gambar) }}" style="height:100px;width:auto;padding: 15px 15px 0px 15px" class="img-responsive" alt="">
                                @else
                                <img src="{{ URL::asset('img/'.$gambar_default->value) }}" style="height:100px;width:auto;padding: 15px 15px 0px 15px" class="img-responsive" alt="">
                                @endif
                            </td>
                            <td align="center">
                                <form method="post" action="{{ url('/hapus_paket_produk') }}" id="hapus_form">
                                {{ csrf_field() }}
                                    <input name="produk_id" type="hidden" value="{{ $val->produk_id }}">
                                    <input name="paket_id" type="hidden" value="{{ $paket->id }}">
                                      <button class='btn btn-danger flat' onclick="btn_hapus()" type="button" data-toggle="tooltip" title="hapus" style="font-size: 15pt"><span class='fa fa-trash-o'></span></button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
            <div class="overlay" id="loading" style="display:none">
              <i class="fa fa-refresh fa-spin"></i>
            </div>
          </div>
    </div>
</div>

<div class="modal fade" id="tambah_produk" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content box">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="riwayatheader">Tambah Produk</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Pilih Kategori</label>
            <div class="col-md-10">
                <select class="form-control" name="kode_kategori" id="kode_kategori">
                    <option value="">-- Pilih Kategori --</option>
                    @foreach($kategori_produk as $p)
                    <option value="{{ $p->id }}">{{ $p->nama }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <hr>
        <div id="content_produk">
        <table class="table table-striped table-bordered" id="mydatatables2">
            <thead>
                <th style="text-align:center">NO</th>
                <th style="text-align:center">KODE</th>
                <th style="text-align:center">JENIS</th>
                <th style="text-align:center">NAMA</th>
                <th style="text-align:center">HARGA RETAIL</th>
                <th style="text-align:center"></th>
            </thead>
            <tbody>
                <?php $i=1; ?>
                @foreach($produk as $val)
                <tr>
                    <td align="center">{{ $i++ }}</td>
                    <td>{{ $val->kode }}</td>
                    <td>@if(substr($val->kode,0,3) == 'PKT')<p>PAKET</p>@else<p>PRODUK</p>@endif</td>
                    <td>{{ $val->nama }}</td>
                    <td>{{ rupiah($val->harga_retail) }}</td>
                    <td align="center">
                    <a href="{{ url('/tambah_paket_produk2/'.$paket->kode.'/'.$val->kode) }}">
                        <button class="btn bg-maroon flat" onclick="document.getElementById('loading2').style.display='block'"><span class="fa fa-check"></span> Pilih</button>
                    </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        </div>
    </div>

        <div class="overlay" id="loading2" style="display:none">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
  </div>
</div>
</div>

@stop

<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<script src="{{ asset('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ url('js/jquery.maskMoney.js') }}" type="text/javascript"></script>
<script>
  $(function () {
    $("#mydatatables").DataTable();
    $("#mydatatables2").DataTable();
  });

  function ubah(harga)
    {
        document.getElementById("kode").disabled           = false;
        document.getElementById("nama").disabled           = false;
        document.getElementById("harga_total").disabled    = false;
        document.getElementById("harga_total").value       = harga;

        document.getElementById("ubah").style.display = 'none';
        document.getElementById("batal").style.display = 'block';
        document.getElementById("button_submit").style.display = 'block';

        document.getElementById("astket").innerHTML = "Keterangan (*) : Wajib Diisi";
        document.getElementById("ast0").innerHTML = "*";
        document.getElementById("ast1").innerHTML = "*";
        document.getElementById("ast2").innerHTML = "*";
    }
    function batal(harga)
    {
        document.getElementById("kode").disabled           = true;
        document.getElementById("nama").disabled           = true;
        document.getElementById("harga_total").disabled    = true;
        document.getElementById("harga_total").value       = harga;

        document.getElementById("ubah").style.display = 'block';
        document.getElementById("batal").style.display = 'none';
        document.getElementById("button_submit").style.display = 'none';

        document.getElementById("kode").value           = document.getElementById("ori_kode").value;
        document.getElementById("nama").value           = document.getElementById("ori_nama").value;

        document.getElementById("astket").innerHTML = "";
        document.getElementById("ast0").innerHTML = "";
        document.getElementById("ast1").innerHTML = "";
        document.getElementById("ast2").innerHTML = "";
    }
    function btn_hapus()
    {
        var cek = confirm('Apakah anda yakin menghapus produk?');
        
        if(cek)
        {
            document.getElementById('loading').style.display='block';
            document.getElementById("hapus_form").submit();
        }
    }

    $(document).ready(function(){
          $('#kode_kategori').change(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            var url = "{{ url('/get_produk') }}";

              $.ajax({
                type: "POST",
                url: url,
                data: $("#form_produk").serialize(),
                beforeSend: function(){
                        document.getElementById('loading2').style.display = "block";
                    },
                success: function (data) {
                    $("#content_produk").html(data);
                    document.getElementById('loading2').style.display = "none";
                },
                error: function (data) {
                    alert('ooo');
                }
            });
          });

          $('#target_customer').change(function(){
            if(document.getElementById("target_customer").value == 1){
              document.getElementById("label1_pilih_customer").style.display = 'none';
              document.getElementById("label2_pilih_customer").style.display = 'none';
              document.getElementById("button_pilih_customer").style.display = 'none';
            }
            else{
              document.getElementById("label1_pilih_customer").style.display = 'block';
              document.getElementById("label2_pilih_customer").style.display = 'block';
              document.getElementById("button_pilih_customer").style.display = 'block';
            }
          });

          // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // save the latest tab; use cookies if you like 'em better:
                localStorage.setItem('lastTab_detail_paket', $(this).attr('href'));
            });

            // go to the latest tab, if it exists:
            var lastTab_detail_paket = localStorage.getItem('lastTab_detail_paket');
            if (lastTab_detail_paket) {
                $('[href="' + lastTab_detail_paket + '"]').tab('show');
            }
            else
            {
                lastTab_detail_paket = "#tab_1";
                $('[href="' + lastTab_detail_paket + '"]').tab('show');
            }
      });
</script>