@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <div class="pull-right">
                    <a href="{{ url('/kategori_produk') }}">
                        <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                    </a>
                </div>
            </div>
            <div class="box-body">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <form class="form-horizontal" method="POST" action="{{ url('/do_tambah_kategori_produk') }}" autocomplete="off" enctype="multipart/form-data">    
                    <input type="hidden" name="jumlah_kolom_spesifikasi" id="jumlah_kolom_spesifikasi" value="1">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                        <label for="name" class="control-label col-md-2">Nama*</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="nama" id="nama" value="{{ old('nama') }}" placeholder="Masukkan Nama Kategori" style="text-transform:capitalize">
                            @if ($errors->has('nama'))
                                <span class="help-block">
                                    <strong>Kolom Nama Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('deskripsi') ? ' has-error' : '' }}">
                        <label class="control-label col-md-2">Deskripsi</label>
                        <div class="col-md-8">
                            <textarea type="text" class="form-control" name="deskripsi" id="deskripsi" placeholder="Masukkan Deskripsi">{{ old('deskripsi') }}</textarea>
                            @if ($errors->has('deskripsi'))
                                <span class="help-block">
                                    <strong>Kolom Deskripsi Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2">Icon</label>
                        <div class="col-md-8">
                            <input type="file" name="file_gambar" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2">Kolom Spesifikasi</label>
                        <div class="col-md-8">
                            <div id="put_kolom_spesifikasi"></div>
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="text" name="kolom_spesifikasi" id="kolom_spesifikasi" class="form-control" style="text-transform:capitalize" placeholder="Masukkan kolom spesifikasi, ex: memori">
                                </div>
                                <div class="col-md-2">
                                    <button class="btn btn-primary flat" type="button" onclick="tambah_kolom_spesifikasi()" data-toggle="tooltip" title="tambah" style="font-size: 15pt"><i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-2" id="button_submit">
                            <button type="submit" class="btn btn-primary flat" onclick="document.getElementById('loading').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
                        </div>
                    </div>
                </form>
                <p>Keterangan (*) : Wajib Diisi</p>
            </div>
            <div class="overlay" id="loading" style="display:none">
              <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>
@stop
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script type="text/javascript">
    function tambah_kolom_spesifikasi()
    {
        var jumlah = document.getElementById('jumlah_kolom_spesifikasi').value;
        jumlah = parseInt(jumlah);

        var kolom_spesifikasi = document.getElementById('kolom_spesifikasi').value;
        document.getElementById('kolom_spesifikasi').value          = "";

        $('#put_kolom_spesifikasi').append('<div class="row" style="padding-bottom:15px;" id="div_spesifikasi'+jumlah+'">\
                    <div class="col-md-6">\
                        <input type="text" name="kolom_spesifikasi'+jumlah+'" value="'+kolom_spesifikasi+'"  style="text-transform:capitalize" class="form-control">\
                    </div>\
                    <div class="col-md-2">\
                        <button class="btn btn-danger flat" type="button" onclick="hapus_kolom_spesifikasi('+jumlah+')" \data-toggle="tooltip" title="hapus" style="font-size: 15pt"><i class="fa fa-trash-o"></i></button>\
                    </div>\
                </div>');

        jumlah++;
        document.getElementById('jumlah_kolom_spesifikasi').value = jumlah;
    }

    function hapus_kolom_spesifikasi(id)
    {
        document.getElementById('div_spesifikasi'.concat(id)).innerHTML = "";
        document.getElementById('div_spesifikasi'.concat(id)).style.padding = '0px';
    }
</script>
