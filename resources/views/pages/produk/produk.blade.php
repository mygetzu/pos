<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<script src="{{ asset('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ url('assets/js/jquery.maskMoney.js') }}" type="text/javascript"></script>
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<input type="hidden" name="base_path" id="base_path" value="{{ asset('/') }}">
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <a href="{{ url('/tambah_produk') }}">
                  <button class='btn bg-purple flat'><span class='fa fa-plus'></span> Tambah Produk</button>
                </a>
            </div>
            <div class="box-body">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <table id="mydatatables" class="table table-striped table-bordered">
                    <thead>
                        <th style="text-align:center">NO</th>
                        <th style="text-align:center">NAMA</th>
                        <th style="text-align:center">KATEGORI</th>
                        <th style="text-align:center">STOK</th>
                        <th style="text-align:center">STOK DIPESAN</th>
                        <th style="text-align:center">STATUS PROMO</th>
                        <th style="text-align:center"></th>
                        <th></th>
                    </thead>
                    <tbody>
                        <?php $i=1; ?>
                        @foreach($produk as $val)
                        <tr>
                            <td align="center">{{ $i++ }}</td>
                            <td>{{ $val->nama }}</td>
                            <td>{{ $val->kategori_produk->nama }}</td>
                            <td align="center">{{ $val->stok }}</td>
                            <td align="center">{{ $val->stok_dipesan }}</td>
                            <td align="center">
                              @if(!empty($val->promo_diskon)) diskon {{ $val->promo_diskon->diskon }}% <br> @endif
                              @if(!empty($val->promo_cashback)) cashback {{ rupiah($val->promo_cashback->cashback) }} <br> @endif
                              @if(!empty($val->promo_hadiah)) berhadiah {{ $val->promo_hadiah->hadiah->nama }} <br> @endif
                            </td>
                            <td align="center">
                                <label class="switch">
                                    <input type="checkbox" onchange='window.location.href="{{ url('/ubah_status_produk/'.$val->id) }}"' <?php if($val->is_aktif == 1) echo "checked"; else echo ""; ?>>
                                    <div class="slider" data-toggle="tooltip" title="status aktif"></div>
                                </label>
                            </td>
                            <td align="center">
                                <a class="btn btn-info flat" href="{{ url('/detail_produk/'.$val->id) }}" data-toggle="tooltip" title="detail" style="font-size: 15pt"><i class="fa fa-info-circle"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop
<link href="{{ URL::to('/switch/dist/css/bootstrap3/bootstrap-switch.css') }}" rel="stylesheet">

<script>
  $(function () {
    $("#mydatatables").DataTable();
  });
</script>
