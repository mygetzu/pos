<table class="table table-bordered">
    <thead>
        <tr>
            <th style="text-align: center;">SPESIFIKASI</th>
            <th style="text-align: center;">NILAI</th>
            <th style="text-align: center;">SATUAN</th>
        </tr>
    </thead>
    <tbody>
        @foreach($parameter_spesifikasi as $key => $value)
        <tr>
            <td>{{ $value->nama }}</td>
            <td>
                <input type="hidden" name="parameter_spesifikasi_id{{ $key }}" class="form-control" value="{{ $value->id }}">
                <input type="text" name="nilai{{ $key }}" class="form-control" value="" style="text-transform:capitalize">
            </td>
            <td>
                <input type="text" name="satuan{{ $key }}" class="form-control" value="" style="text-transform:capitalize">
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<input type="hidden" name="jumlah_spesifikasi" value="{{ $key+1 }}">