<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <a href="{{ url('/tambah_hadiah') }}">
                  <button class='btn bg-purple flat'><span class='fa fa-plus'></span> Tambah Hadiah</button>
                </a>
            </div>
            <div class="box-body">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                @if ($errors->has('nama'))
                <div class="alert alert-danger flat">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    Kolom Nama Wajib Diisi
                </div>
                @endif
                @if ($errors->has('deskripsi'))
                <div class="alert alert-danger flat">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    Kolom Deskripsi Wajib Diisi
                </div>
                @endif
                @if ($errors->has('satuan'))
                <div class="alert alert-danger flat">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    Kolom Satuan Wajib Diisi
                </div>
                @endif
                @if ($errors->has('berat'))
                <div class="alert alert-danger flat">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    Kolom Berat Wajib Diisi
                </div>
                @endif
                @if ($errors->has('stok'))
                <div class="alert alert-danger flat">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    Kolom Stok Wajib Diisi
                </div>
                @endif
                <table id="mydatatables" class="table table-striped table-bordered">
                  <thead>
                      <th style="text-align:center">NO</th>
                      <th style="text-align:center">NAMA</th>
                      <th style="text-align:center">SATUAN</th>
                      <th style="text-align:center">BERAT</th>
                      <th style="text-align:center">STOK</th>
                      <th style="text-align:center">STOK DIPESAN</th>
                      <th style="text-align:center"></th>
                  </thead>
                  <tbody>
                      <?php $i=1; ?>
                      @foreach($hadiah as $val)
                      <tr>
                          <td align="center">{{ $i++ }}</td>
                          <td>{{ $val->nama }}</td>
                          <td>{{ $val->satuan }}</td>
                          <td align="center">{{ $val->berat }}Kg</td>
                          <td align="center">{{ $val->stok }}</td>
                          <td align="center">{{ $val->stok_dipesan }}</td>
                          <td align="center">
                            <a href="{{ url('/detail_hadiah/'.$val->id) }}" class="btn btn-info flat" data-toggle="tooltip" title="detail" style="font-size: 15pt"><span class="fa fa-info-circle"></span></a>
                          </td>
                      </tr>
                      @endforeach
                  </tbody>
              </table>
            </div>
        </div>
    </div>
</div>
@stop
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script>
  $(function () {
    $("#mydatatables").DataTable();
  });
</script>