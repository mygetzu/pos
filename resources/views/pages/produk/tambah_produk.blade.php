<script type="text/javascript">
    // function tambah_supplier()
    // {
    //     $.ajaxSetup({
    //         headers: {
    //             'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
    //         }
    //     })

    //     var url = "{{ url('/get_supplier') }}";

    //     $.ajax({
    //         type: "POST",
    //         url: url,
    //         success: function (data) {
    //             var row_supplier = document.getElementById("row_supplier").value;
    //             row_supplier = parseInt(row_supplier);
    //             var table = document.getElementById("myTable");
    //             var sp_row = table.insertRow(row_supplier+2);
    //             var sp_cell1 = sp_row.insertCell(0);
    //             var sp_cell2 = sp_row.insertCell(1);
    //             var sp_cell3 = sp_row.insertCell(2);
    //             new_row_supplier = row_supplier+1;
    //             sp_cell1.innerHTML = "";
    //             sp_cell2.innerHTML = "<label class='control-label'>:</label>";
    //             sp_cell3.innerHTML = "<div class='col-md-6'><select class='form-control' name='supplier"+new_row_supplier+"'>"
    //             +data+"</select></div><button id='sp_hapus"+new_row_supplier+"' class='btn btn-danger flat' type='button' onclick='hapus_supplier("+new_row_supplier+")''><span class='glyphicon glyphicon-trash'></span> hapus</button>";

    //             if(new_row_supplier > 2){//tombol hapus diatasnya disembunyikan
    //                 var sp = new_row_supplier-1;
    //                 document.getElementById("sp_hapus"+sp).style.display = 'none';
    //             }
    //             document.getElementById("row_supplier").value = new_row_supplier;
    //         },
    //         error: function (data) {
                
    //         }
    //     });
    // }

    function hapus_supplier(id)
    {
        if(id > 1)
        {
            var sp = id-1;
            if(sp > 1)
                document.getElementById("sp_hapus"+sp).style.display = 'block';
            document.getElementById("myTable").deleteRow(id+1);
            document.getElementById("row_supplier").value = id-1;
        }
    }

    function btn_simpan_duplikasi()
    {
        var deskripsi       = tinyMCE.activeEditor.getContent();
        document.getElementById("desk").value = deskripsi;

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })

        var url = "{{ url('/do_tambah_produk_by_ajax') }}";

        $.ajax({
            type: "POST",
            url: url,
            data: new FormData($("#form_tambah_produk")[0]),
            cache:false,
            contentType: false,
            processData: false,
            beforeSend: function(){
                document.getElementById('loading').style.display = "block";
            },
            success: function (data) {
                document.getElementById("message").innerHTML = data;
                document.getElementById('loading').style.display = "none";
            },
            error: function (data) {
                alert('Lengkapi dulu data yang wajib diisi');
                document.getElementById('loading').style.display = "none";
            }
        });

    }
</script>
<script src="{{ asset('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ url('/js/jquery.maskMoney.js') }}" type="text/javascript"></script>
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <div class="pull-right">
                    <a href="{{ url('/produk') }}">
                        <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                    </a>
                </div>
            </div>
            <div class="box-body">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                    <i class="fa fa-check"></i>
                    {{ Session::get('message') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                <div id="message"></div>
                <form class="form-horizontal" method="POST" action="{{ url('/do_tambah_produk') }}" autocomplete="off" enctype="multipart/form-data" id="form_tambah_produk">
                    <input type="hidden" name="row_supplier" id="row_supplier" value="1">
                    <input type="hidden" name="desk" id="desk">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('kode') ? ' has-error' : '' }}">
                        <label for="name" class="control-label col-md-2">Kode Produk</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="kode" id="kode" value="{{ old('kode') }}" placeholder="Masukkan Kode Produk">
                            @if ($errors->has('kode'))
                                <span class="help-block">
                                    <strong>Kolom Kode Produk Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                        <label for="name" class="control-label col-md-2">Nama*</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="nama" id="nama" value="{{ old('nama') }}" placeholder="Masukkan Nama Produk" style="text-transform:capitalize">
                            @if ($errors->has('nama'))
                                <span class="help-block">
                                    <strong>Kolom Nama Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('kategori') ? ' has-error' : '' }}">
                        <label class="control-label col-md-2">Kategori Produk*</label>
                        <div class="col-md-8">
                            <select class="form-control" name="kategori" id="kategori">
                                <option value="">-- Pilih Kategori Produk --</option>
                                @foreach($kategori_produk as $val)
                                <option value="{{ $val->id }}">{{ $val->nama }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('kategori'))
                                <span class="help-block">
                                    <strong>Pilihan Kategori Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group" class="{{ $errors->has('supplier1') ? ' has-error' : '' }}">
                        <label class="control-label col-md-2">Supplier*</label>
                        <div class="col-md-8">
                            <select class="form-control" name="supplier1" id="supplier1">
                                <option value="">-- Pilih Supplier --</option>
                                @foreach($supplier as $val2)
                                <option value="{{ $val2->id }}">{{ $val2->nama }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('supplier1'))
                                <span class="help-block">
                                    <strong>Pilihan Supplier Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('harga') ? ' has-error' : '' }}">
                        <label class="control-label col-md-2">Harga*</label>
                        <div class="col-md-3">
                            <input type="text" class="form-control" name="harga" placeholder="harga" id="harga" data-thousands="." data-decimal="," style="text-align:right" value="{{ old('harga') }}" />
                            <script type="text/javascript">$("#harga").maskMoney({precision:0});</script>
                            @if ($errors->has('harga'))
                                <span class="help-block">
                                    <strong>Kolom Harga Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                        <label class="control-label col-md-1">(IDR)</label>
                    </div>
                    <div class="form-group{{ $errors->has('satuan') ? ' has-error' : '' }}">
                        <label class="control-label col-md-2">Satuan*</label>
                        <div class="col-md-3">
                            <input type="text" value="{{ old('satuan') }}" class="form-control" name="satuan" id="satuan" placeholder="Masukkan Satuan" style="text-transform:capitalize">
                            @if ($errors->has('satuan'))
                                <span class="help-block">
                                    <strong>Kolom Satuan Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('berat') ? ' has-error' : '' }}">
                        <label class="control-label col-md-2">Berat*</label>
                        <div class="col-md-3">
                            <input type="text" value="{{ old('berat') }}" class="form-control" name="berat" id="berat" onKeyPress="return numbersdecimal(this, event)" placeholder="Masukkan Berat" style="text-align:right">
                            @if ($errors->has('berat'))
                                <span class="help-block">
                                    <strong>Kolom Berat Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                        <label class="control-label col-md-1">Kg</label>
                    </div>
                    <div class="form-group{{ $errors->has('deskripsi') ? ' has-error' : '' }}">
                        <label class="control-label col-md-2">Deskripsi*</label>
                        <div class="col-md-8">
                            <textarea class="form-control" name="deskripsi" id="deskripsi" placeholder="Masukkan Deskripsi">{{ old('deskripsi') }}</textarea>
                            @if ($errors->has('deskripsi'))
                                <span class="help-block">
                                    <strong>Kolom Deskripsi Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('deskripsi') ? ' has-error' : '' }}">
                        <label class="control-label col-md-2">Spesifikasi</label>
                        <div class="col-md-8" id="div_spesifikasi">
                            <input type="hidden" name="jumlah_spesifikasi" value="0">
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('produk_galeri') ? ' has-error' : '' }}">
                        <label class="control-label col-md-2">Gambar</label>
                        <div class="col-md-8">
                            <input type="file" class="form-control" name="produk_galeri" id="produk_galeri">
                            @if ($errors->has('produk_galeri'))
                                <span class="help-block">
                                    <strong>Gambar Wajib Disertakan</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-2" id="button_submit">
                            <button type="submit" class="btn btn-primary flat" onclick="document.getElementById('loading').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
                            <button type="button" class="btn btn-primary flat" onclick="btn_simpan_duplikasi()" style="margin-left:10px"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan dan Duplikasi</button>
                        </div>
                    </div>
                </form>
                <p>Keterangan (*) : Wajib Diisi</p>
            </div>
            <div class="overlay" id="loading" style="display:none">
              <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>
@stop
<!-- TINYMCE -->
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
  tinymce.init({ selector:'textarea',
  height: 400,
  plugins: [
    'advlist autolink lists link image charmap print preview anchor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste code'
  ],
  toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image' });
</script>
<script type="text/javascript">
    $( document ).ready(function() {
        $("#kategori").change(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });

            var kategori_id = document.getElementById('kategori').value;
            
            var url = "{{ url('/get_kolom_spesifikasi') }}";
            
            $.ajax({
                type: "POST",
                url: url,
                data: { kategori_id:kategori_id },
                success: function (data) {
                    $("#div_spesifikasi").html(data);
                },
                error: function (data) {
                    
                }
            });
        });
    });
</script>