<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<style>
.datepicker{z-index:1151 !important;}
</style>
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<input type="hidden" name="ori_nama" id="ori_nama" value="{{ $produk->nama }}">
<input type="hidden" name="ori_satuan" id="ori_satuan" value="{{ $produk->satuan }}">
<input type="hidden" name="ori_berat" id="ori_berat" value="{{ $produk->berat }}">
<input type="hidden" name="ori_deskripsi" id="ori_deskripsi" value="{{ $produk->deskripsi }}">
<input type="hidden" name="ori_kode" id="ori_kode" value="{{ $produk->kode }}">
<input type="hidden" name="ori_kategori_id" id="ori_kategori_id" value="{{ $produk->kategori_id }}">
<form action="{{ url('/hapus_serial_number') }}" method="post" id="form_hapus_serial_number">
    {{ csrf_field() }}
    <input type="hidden" name="hsn_produk_id" id="hsn_produk_id">
    <input type="hidden" name="hsn_serial_number" id="hsn_serial_number">
    <input type="hidden" name="hsn_gudang_id" id="hsn_gudang_id">
</form>
<form action="{{ url('/hapus_promo_diskon') }}" method="post" id="form_hapus_promo_diskon">
    {{ csrf_field() }}
    <input type="hidden" name="hpd_id" id="hpd_id">
     <input type="hidden" name="produk_id" value="{{ $produk->id }}">
</form>
<form action="{{ url('/hapus_promo_cashback') }}" method="post" id="form_hapus_promo_cashback">
    {{ csrf_field() }}
    <input type="hidden" name="hpc_id" id="hpc_id">
     <input type="hidden" name="produk_id" value="{{ $produk->id }}">
</form>
<form action="{{ url('/hapus_produk_supplier') }}" method="post" id="form_hapus_produk_supplier">
    {{ csrf_field() }}
    <input type="hidden" name="hs_produk_id" id="hs_produk_id">
    <input type="hidden" name="hs_supplier_id" id="hs_supplier_id">
</form>
<form action="{{ url('/hapus_promo_hadiah') }}" method="post" id="form_hapus_promo_hadiah">
    {{ csrf_field() }}
    <input type="hidden" name="hph_id" id="hph_id">
    <input type="hidden" name="produk_id" value="{{ $produk->id }}">
</form>
<form action="{{ url('/hapus_produk_galeri') }}" method="post" id="form_hapus_produk_galeri">
    {{ csrf_field() }}
    <input type="hidden" name="hg_id" id="hg_id">
    <input type="hidden" name="produk_id" value="{{ $produk->id }}">
</form>
</form>
<div class="row">
    <div class="col-md-12">
        <div class="nav-tabs-custom box tab-primary flat" style="border-top: 0">
            <ul class="nav nav-tabs">
                <li><a href="#tab_1" data-toggle="tab">Detail Produk</a></li>
                <li><a href="#tab_2" data-toggle="tab">Serial Number</a></li>
                <li><a href="#tab_3" data-toggle="tab">Promo Diskon</a></li>
                <li><a href="#tab_4" data-toggle="tab">Promo Cashback</a></li>
                <li><a href="#tab_5" data-toggle="tab">Promo Hadiah</a></li>
                <li><a href="#tab_6" data-toggle="tab">Produk Supplier</a></li>
                <li><a href="#tab_7" data-toggle="tab">Produk Galeri</a></li>
                <li class="pull-right">
                    <a href="{{ url('/produk') }}" style="padding:0px">
                        <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane" id="tab_1">
            <div class="row">
            <div class="col-md-1">
            <div id="ubah" style="display:block">
                <div class="">
                  <button class="btn btn-warning flat" onclick="ubah('{{ $produk->harga_retail }}', '{{ $jumlah_spesifikasi }}')"><span class='fa fa-edit'></span> Ubah</button>
                </div>
            </div>
            <div id="batal" style="display:none">
                <div class="">
                  <button class="btn btn-default flat" onclick="batal('{{ rupiah($produk->harga_retail) }}', '{{ substr($produk->kategori_id, 3) }}','{{ $jumlah_spesifikasi }}')"><span class='fa fa-ban'></span> Batal</button>
                </div>
            </div>
            </div>
            <div class="col-md-4">
                <a data-toggle="modal" data-target="#jadikan_hadiah">
                <button class="btn bg-navy flat"><span class='fa fa-gift'></span> Jadikan Sebagai Hadiah</button>
                </a>
            </div>
            </div>
            <br>
            @if(Session::has('message1'))
            <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
              <i class="fa fa-check"></i>
              {{ Session::get('message1') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            @endif
            <form action="{{ url('/ubah_produk') }}" method="post" autocomplete="off" class="form-horizontal">
                <input type="hidden" name="id" value="{{ $produk->id }}">
                <input type="hidden" name="jumlah_spesifikasi" value="{{ $jumlah_spesifikasi }}">
                {{ csrf_field() }}
                <table class="table table-striped">
                    <tr class="{{ $errors->has('nama') ? ' has-error' : '' }}">
                        <th><label class="control-label">Kode Produk</label><label id="ast6" class="control-label"></label></th>
                        <th>:</th>
                        <td>
                            <input type="text" class="form-control" name="kode" id="kode" value="{{ $produk->kode }}" disabled="true" placeholder="Masukkan Kode Produk" style="text-transform:capitalize">
                            @if ($errors->has('kode'))
                                <span class="help-block">
                                    <strong>Kolom Kode Produk Wajib Diisi</strong>
                                </span>
                            @endif
                        </td>
                    </tr>
                    <tr class="{{ $errors->has('nama') ? ' has-error' : '' }}">
                        <th><label class="control-label">Nama</label><label id="ast1" class="control-label"></label></th>
                        <th>:</th>
                        <td>
                            <input type="text" class="form-control" name="nama" id="nama" value="{{ $produk->nama }}" disabled="true" placeholder="Masukkan Nama Produk" style="text-transform:capitalize">
                            @if ($errors->has('nama'))
                                <span class="help-block">
                                    <strong>Kolom Nama Wajib Diisi</strong>
                                </span>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>Kategori</th>
                        <th>:</th>
                        <td>
                        <div id="select_kategori" style="display:none">
                            <select class="form-control" name="kategori_produk_id" id="kategori_produk_id">
                                @foreach($kategori_produk as $val)
                                    <option value="{{ $val->id }}">{{ $val->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div id="display_kategori" style="display:block">
                            <input type="text" class="form-control" value="{{ $produk->kategori_produk_nama }}" disabled="true">
                        </div>

                        </td>
                    </tr>
                    <tr class="{{ $errors->has('harga') ? ' has-error' : '' }}">
                        <th><label class="control-label">Harga</label><label id="ast2" class="control-label"></label></th>
                        <th>:</th>
                        <td>
                        <div class="col-md-3">
                            <input type="text" class="form-control" name="harga" placeholder="harga" id="harga" data-thousands="." data-decimal="," style="text-align:right;margin-left:-15" value="{{ rupiah($produk->harga_retail) }}" disabled="true" />
                            <script type="text/javascript">$("#harga").maskMoney({precision:0});</script>
                            @if ($errors->has('harga'))
                                <span class="help-block">
                                    <strong>Kolom Harga Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                        <label class="control-label">(IDR)</label>
                        </td>
                    </tr>
                    <tr class="{{ $errors->has('satuan') ? ' has-error' : '' }}">
                        <th><label class="control-label">Satuan</label><label id="ast3" class="control-label"></label></th>
                        <th>:</th>
                        <td>
                            <input type="text" class="form-control" name="satuan" id="satuan" value="{{ $produk->satuan }}" disabled="true" style="text-transform:capitalize">
                            @if ($errors->has('satuan'))
                                <span class="help-block">
                                    <strong>Kolom Satuan Wajib Diisi</strong>
                                </span>
                            @endif
                        </td>
                    </tr>
                    <tr class="{{ $errors->has('berat') ? ' has-error' : '' }}">
                        <th><label class="control-label">Berat</label><label id="ast4" class="control-label"></label></th>
                        <th>:</th>
                        <td>
                            <div class="col-md-3" style="padding-left:0px">
                                <input type="text" class="form-control" name="berat" id="berat" value="{{ $produk->berat }}" onKeyPress="return numbersdecimal(this, event)" disabled="true">
                                @if ($errors->has('berat'))
                                    <span class="help-block">
                                        <strong>Kolom Berat Wajib Diisi</strong>
                                    </span>
                                @endif
                            </div>
                            <label class="control-label">Kg</label>
                        </td>
                    </tr>
                    <tr class="{{ $errors->has('deskripsi') ? ' has-error' : '' }}">
                        <th><label class="control-label">Deskripsi</label><label id="ast5" class="control-label"></label></th>
                        <th>:</th>
                        <td>
                            <div id="display_deskripsi" style="display:block">
                            <textarea type="text" class="form-control" name="deskripsi2" id="deskripsi2" disabled="true" style="height:120px">{{ $produk->deskripsi }}</textarea> 
                            </div>
                            <div id="display_deskripsi2" style="display:none">
                            <textarea type="text" class="form-control" name="deskripsi" id="deskripsi" style="height:120px">{{ $produk->deskripsi }}</textarea> 
                            </div>
                            @if ($errors->has('deskripsi'))
                                <span class="help-block">
                                    <strong>Kolom Deskripsi Wajib Diisi</strong>
                                </span>
                            @endif
                        </td>
                    </tr>
                    <tr class="{{ $errors->has('deskripsi') ? ' has-error' : '' }}">
                        <th><label class="control-label">Spesifikasi</label><label id="ast5" class="control-label"></label></th>
                        <th>:</th>
                        <td>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th style="text-align: center;">SPESIFIKASI</th>
                                        <th style="text-align: center;">NILAI</th>
                                        <th style="text-align: center;">SATUAN</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($spesifikasi as $key => $value)
                                    <tr>
                                        <td>{{ $value->nama }}</td>
                                        <td>
                                            <input type="hidden" name="spesifikasi_param_id{{ $key }}" class="form-control" value="{{ $value->id }}">
                                            <input type="text" name="spesifikasi_nilai{{ $key }}" id="spesifikasi_nilai{{ $key }}" class="form-control" value="@if($value->nilai_spesifikasi != '[]'){{ $value->nilai_spesifikasi[0]['nilai'] }} @endif" style="text-transform:capitalize" disabled="true">
                                        </td>
                                        <td>
                                            <input type="text" name="spesifikasi_satuan{{ $key }}" id="spesifikasi_satuan{{ $key }}" class="form-control" value="@if($value->nilai_spesifikasi != '[]') {{ $value->nilai_spesifikasi[0]['satuan'] }} @endif" style="text-transform:capitalize" disabled="true">
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <th>Stok</th>
                        <th>:</th>
                        <td><input type="text" class="form-control" name="stok" id="stok" value="{{ $produk->stok }}" disabled="true" onKeyPress="return numbersonly(this, event)"></td>
                    </tr>
                    <tr>
                        <th>Stok dipesan</th>
                        <th>:</th>
                        <td><input type="text" class="form-control" value="{{ $produk->stok_dipesan }}" disabled="true"></td>
                    </tr>
                    <tr>
                        <th><label class="control-label"></label></th>
                        <th></th>
                        <td>
                            <button type="submit" class="btn btn-primary flat" id="button_submit" style="display:none" onclick="document.getElementById('loading').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
                        </td>
                    </tr>
                </table>
            </form>
                <label id="astket" class="control-label"></label>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
                    <div>
                    <a data-toggle="modal" data-target="#tambah_serial_number">
                        <button class='btn bg-purple flat'><span class='fa fa-plus'></span> Tambah Serial Number</button>
                    </a>
                </div>
                <br>
                @if(Session::has('message2'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message2') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                @if(Session::has('message_error2'))
                <div class="alert alert-danger alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message_error2') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                @if ($errors->has('gudang_id'))
                <div class="alert alert-danger flat">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    Pilihan Gudang Wajib Diisi
                </div>
                @endif
                @if ($errors->has('serial_number'))
                <div class="alert alert-danger flat">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    Kolom Serial Number Wajib Diisi
                </div>
                @endif
                @if ($errors->has('deskripsi'))
                <div class="alert alert-danger flat">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    Kolom Deskripsi Wajib Diisi
                </div>
                @endif
                <table id="mydatatables" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th style="text-align:center">NO</th>
                            <th style="text-align:center">GUDANG</th>
                            <th style="text-align:center">SERIAL NUMBER</th>
                            <th style="text-align:center">KETERANGAN</th>
                            <th style="text-align:center">STOK</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i=1;?>
                        @foreach($produk_serial_number as $val)
                        <tr>
                            <td align="center">{{ $i++ }}</td>
                            <td align="center">{{ $val->nama }}</td>
                            <td align="center">{{ $val->serial_number }}</td>
                            <td align="center">{{ $val->keterangan }}</td>
                            <td align="center">{{ $val->stok }}</td>
                            <td align="center">
                                @if($val->stok == 0)
                                <button class="btn btn-danger flat" onclick="btn_hapus_serial_number('{{ $val->produk_id }}', '{{ $val->serial_number }}', '{{ $val->gudang_id }}')" data-toggle="tooltip" title="hapus" style="font-size: 15pt"><span class="fa fa-trash"></span></button>
                                @else
                                <button class="btn btn-danger flat" disabled="true" data-toggle="tooltip" title="hapus" style="font-size: 15pt"><span class="fa fa-trash"></span></button>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_3">
                <div>
                    @if(empty($cek_diskon))
                    <a data-toggle="modal" data-target="#tambah_promo_diskon">
                        <button class='btn bg-purple flat' onclick="tambah_promo_diskon('{{ $produk->id }}', '{{ $produk->nama }}')"><span class='fa fa-plus'></span> Tambah Promo Diskon</button>
                    </a>
                    @else
                    <button class='btn bg-purple flat' disabled="true"><span class='fa fa-plus'></span> Tambah Promo Diskon</button>
                    @endif
                </div>
                <br>
                @if(Session::has('message3'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message3') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                @if ($errors->has('gudang_id'))
                <div class="alert alert-danger flat">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    Jumlah beli wajib diisi
                </div>
                @endif
                <table id="mydatatables2" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th style="text-align:center">NO</th>
                            <th style="text-align:center">STATUS</th>
                            <th style="text-align:center">JUMLAH BELI</th>
                            <th style="text-align:center">DISKON</th>
                            <th style="text-align:center">AWAL PERIODE</th>
                            <th style="text-align:center">AKHIR PERIODE</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i=1; $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');?>
                        @foreach($promo_diskon as $val)
                        <tr>
                            <td align="center">{{ $i++ }}</td>
                            <td align="center">
                            <?php
                                date_default_timezone_set("Asia/Jakarta");
                                $date1 = new DateTime($val->awal_periode);
                                $date2 = new DateTime($val->akhir_periode);
                                $curr_date = new DateTime();
                                $curr_dates = $curr_date->format('d-m-Y');
                                $date1s = $date1->format('d-m-Y');
                                $date2s = $date2->format('d-m-Y');
                                if( $curr_dates >= $date1s && $curr_dates <= $date2s){
                                    echo "<span class='label label-success flat'>Aktif</span>";
                                }else{
                                     echo "<span class='label label-danger flat'>Tidak Aktif</span>";
                                }
                            ?>
                            </td>
                            <td align="center">{{ $val->qty_beli }}</td>
                            <td>{{ $val->diskon }}%</td>
                            <td align="center"><?php echo $date1->format('d')." ".$bulan[(int)$date1->format('m')]." ".$date1->format('Y'); ?></td>
                            <td align="center"><?php echo $date2->format('d')." ".$bulan[(int)$date2->format('m')]." ".$date2->format('Y'); ?></td>
                            <td align="center">
                                <a data-toggle="modal" data-target="#ubah_promo_diskon">
                                    <button class="btn btn-warning flat" onclick="ubah_promo_diskon('{{ $val->id }}','{{ $val->qty_beli }}','{{ $val->diskon }}','{{ $date1s }}', '{{ $date2s }}')" data-toggle="tooltip" title="ubah" style="font-size: 15pt"><span class="fa fa-edit"></span></button>
                                </a>
                            </td>
                            <td align="center">
                                <button class="btn btn-danger flat" onclick="hapus_promo_diskon('{{ $val->id }}')" data-toggle="tooltip" title="hapus" style="font-size: 15pt"><span class="fa fa-trash"></span></button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_4">
                <div>
                    @if(empty($cek_cashback))
                    <a data-toggle="modal" data-target="#tambah_promo_cashback">
                        <button class='btn bg-purple flat'><span class='fa fa-plus'></span> Tambah Promo Cashback</button>
                    </a>
                    @else
                    <button class='btn bg-purple flat' disabled="true"><span class='fa fa-plus'></span> Tambah Promo Cashback</button>
                    @endif
                </div>
                <br>
                @if(Session::has('message4'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message4') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <table id="mydatatables3" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th style="text-align:center">NO</th>
                            <th style="text-align:center">STATUS</th>
                            <th style="text-align:center">JUMLAH BELI</th>
                            <th style="text-align:center">CASHBACK</th>
                            <th style="text-align:center">AWAL PERIODE</th>
                            <th style="text-align:center">AKHIR PERIODE</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i=1; $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');?>
                        @foreach($promo_cashback as $val)
                        <tr>
                            <td align="center">{{ $i++ }}</td>
                            <td align="center">
                            <?php
                                date_default_timezone_set("Asia/Jakarta");
                                $date1 = new DateTime($val->awal_periode);
                                $date2 = new DateTime($val->akhir_periode);
                                $curr_date = new DateTime();
                                $curr_dates = $curr_date->format('d-m-Y');
                                $date1s = $date1->format('d-m-Y');
                                $date2s = $date2->format('d-m-Y');
                                if( $curr_dates >= $date1s && $curr_dates <= $date2s){
                                    echo "<span class='label label-success flat'>Aktif</span>";
                                }else{
                                     echo "<span class='label label-danger flat'>Tidak Aktif</span>";
                                }
                            ?>
                            </td>
                            <td align="center">{{ $val->qty_beli }}</td>
                            <td>{{ rupiah($val->cashback) }}</td>
                            <td align="center"><?php echo $date1->format('d')." ".$bulan[(int)$date1->format('m')]." ".$date1->format('Y'); ?></td>
                            <td align="center"><?php echo $date2->format('d')." ".$bulan[(int)$date2->format('m')]." ".$date2->format('Y'); ?></td>
                            <td align="center">
                                <a data-toggle="modal" data-target="#ubah_promo_cashback">
                                    <button class="btn btn-warning flat" onclick="ubah_promo_cashback('{{ $val->id }}','{{ $val->qty_beli }}','{{ $val->cashback }}','{{ $date1s }}', '{{ $date2s }}')" data-toggle="tooltip" title="ubah" style="font-size: 15pt"><span class="fa fa-edit"></span></button>
                                </a>
                            </td>
                            <td align="center">
                                <button class="btn btn-danger flat" onclick="hapus_promo_cashback('{{ $val->id }}')" data-toggle="tooltip" title="hapus" style="font-size: 15pt"><span class="fa fa-trash"></span></button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_5">
                <div>
                    <a data-toggle="modal" data-target="#tambah_promo_hadiah">
                        <button class='btn bg-purple flat'><span class='fa fa-plus'></span> Tambah Promo Hadiah</button>
                    </a>
                </div>
                <br>
                @if(Session::has('message5'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message5') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                @if(Session::has('message_hadiah'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message_hadiah') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                @if ($errors->has('qty_beli'))
                <div class="alert alert-danger flat">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    Kolom Minimal Pembelian Promo Hadiah Wajib Diisi
                </div>
                @endif
                @if ($errors->has('hadiah'))
                <div class="alert alert-danger flat">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    Pilihan Hadiah Wajib Diisi
                </div>
                @endif
                @if ($errors->has('qty_hadiah'))
                <div class="alert alert-danger flat">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    Kolom Jumlah Hadiah Wajib Diisi
                </div>
                @endif
                @if ($errors->has('awal_periode'))
                <div class="alert alert-danger flat">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    Kolom Awal Peride Wajib Diisi dan Masukkan Tanggal yang Benar
                </div>
                @endif
                @if ($errors->has('awal_periode'))
                <div class="alert alert-danger flat">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    Kolom Akhir Peride Wajib Diisi dan Masukkan Tanggal yang Benar
                </div>
                @endif
                <table id="mydatatables4" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th style="text-align:center">NO</th>
                            <th style="text-align:center">STATUS</th>
                            <th style="text-align:center">JUMLAH BELI</th>
                            <th style="text-align:center">NAMA HADIAH</th>
                            <th style="text-align:center">GAMBAR HADIAH</th>
                            <th style="text-align:center">AWAL PERIODE</th>
                            <th style="text-align:center">AKHIR PERIODE</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i=1; $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');?>
                        @foreach($promo_hadiah as $val)
                        <tr>
                            <td align="center">{{ $i++ }}</td>
                            <td align="center">
                                <?php
                                date_default_timezone_set("Asia/Jakarta");
                                $date1 = new DateTime($val->awal_periode);
                                $date2 = new DateTime($val->akhir_periode);
                                $curr_date = new DateTime();
                                $curr_dates = $curr_date->format('d-m-Y');
                                $date1s = $date1->format('d-m-Y');
                                $date2s = $date2->format('d-m-Y');
                                if( $curr_dates >= $date1s && $curr_dates <= $date2s){
                                    echo "<span class='label label-success flat'>Aktif</span>";
                                }
                                else{
                                    echo "<span class='label label-danger flat'>Tidak Aktif</span>";
                                }
                            ?>
                            </td>
                            <td align="center">{{ $val->qty_beli }}</td>
                            <td align="center">{{ $val->nama }}</td>
                            <td align="center">
                                <img src="{{ URL::asset('img/produk/'.$val->file_gambar) }}" style="height:130px;width:auto;padding: 15px 15px 0px 15px" class="img-responsive" alt="">
                            </td>
                            <td align="center"><?php echo $date1->format('d')." ".$bulan[(int)$date1->format('m')]." ".$date1->format('Y'); ?></td>
                            <td align="center"><?php echo $date2->format('d')." ".$bulan[(int)$date2->format('m')]." ".$date2->format('Y'); ?></td>
                            <td>
                                <a data-toggle="modal" data-target="#ubah_promo_hadiah">
                                    <button class="btn btn-warning flat" onclick="ubah_promo_hadiah('{{ $val->id }}','{{ $val->hadiah_id }}','{{ $val->qty_beli }}','{{ $val->qty_hadiah }}','{{ $date1s }}', '{{ $date2s }}')" data-toggle="tooltip" title="ubah" style="font-size: 15pt"><span class="fa fa-edit"></span></button>
                                </a>
                            </td>
                            <td align="center">
                                <button class="btn btn-danger flat" onclick="hapus_promo_hadiah('{{ $val->id }}')" data-toggle="tooltip" title="hapus" style="font-size: 15pt"><span class="fa fa-trash"></span></button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_6">
                <div>
                    <a data-toggle="modal" data-target="#tambah_produk_supplier">
                        <button class='btn bg-purple flat'><span class='fa fa-plus'></span> Tambah Produk Supplier</button>
                    </a>
                </div>
                <br>
                @if(Session::has('message6'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message6') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <table id="mydatatables5" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th style="text-align:center">NO</th>
                            <th style="text-align:center">NAMA</th>
                            <th style="text-align:center">ALAMAT</th>
                            <th style="text-align:center">KOTA</th>
                            <th style="text-align:center">NAMA SALES</th>
                            <th style="text-align:center">DESKRIPSI</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i=1; ?>
                        @foreach($produk_supplier as $val)
                        <tr>
                            <td align="center">{{ $i++ }}</td>
                            <td>{{ $val->nama }}</td>
                            <td>{{ $val->alamat }}</td>
                            <td>{{ ucwords(strtolower($val->kota_nama)) }}</td>
                            <td>{{ $val->nama_sales }}</td>
                            <td>{{ $val->deskripsi }}</td>
                            <td align="center">
                                <button class="btn btn-danger flat" onclick="hapus_supplier('{{ $val->produk_id }}','{{ $val->supplier_id }}')" data-toggle="tooltip" title="hapus" style="font-size: 15pt"><span class="fa fa-trash"></span></button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_7">
                @if(Session::has('message7'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message7') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                @foreach($produk_galeri as $val2)
                <div class="col-md-3" align="center">
                    <div class="panel panel-primary flat" align="center">
                    <button type="button" class="btn btn-danger flat pull-right" onclick="hapus_produk_galeri('{{ $val2->id }}')">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <img src="{{ URL::asset('img/produk/'.$val2->file_gambar) }}" style="height:120px;object-fit: contain;width:auto;padding: 5px 5px 0px 5px" class="img-responsive" alt="">
                    <a data-toggle="modal" data-target="#ubah_produk_galeri">
                        <button class='btn btn-warning flat' onclick="ubah_produk_galeri('{{ $val2->id }}')" style="margin-bottom:10px"><span class='fa fa-edit'></span> Ubah Gambar</button></a>
                    </div>
                </div>
                @endforeach
                <br>
                <table class="table table-striped">
                    <form method="post" action="{{ url('/tambah_produk_galeri/'.$produk->id) }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <tr>
                        <th width="20%"><label class="control-label">Tambah Produk Galeri</label></th>
                        <th width="5%">:</th>
                        <td><input type="file" class="form-control" name="image" id="image"></td>
                    </tr>
                    <tr>
                        <th width="20%"></th>
                        <th width="5%"></th>
                        <td><button class="btn btn-primary flat" type="submit" onclick="document.getElementById('loading').style.display='block'"><span class="fa fa-save"></span> Simpan</button></td>
                    </tr>
                    </form>
                </table>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
            <div class="overlay" id="loading" style="display:none">
              <i class="fa fa-refresh fa-spin"></i>
            </div>
          </div>
    </div>
</div>


<div class="modal fade" id="tambah_serial_number" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content box">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="riwayatheader">Tambah Serial Number</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="post" action="{{ url('/tambah_serial_number') }}" autocomplete="off">
        <input type="hidden" name="produk_id" value="{{ $produk->id }}">
            {{ csrf_field() }}
          <div class="box-body">
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Nama Produk</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="nama_produk" value="{{ $produk->nama }}" disabled="true">
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Pilih Gudang*</label>
              <div class="col-sm-10">
                <select class="form-control" name="gudang_id">
                    <option value="">-- Pilih gudang --</option>
                    @foreach($gudang as $val_gudang)
                    <option value="{{ $val_gudang->id }}">{{ $val_gudang->nama }}</option>
                    @endforeach
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Serial Number*</label>
              <div class="col-sm-4">
                <input type="text" class="form-control" name="serial_number1" placeholder="Masukkan Serial Number">
              </div>
              <div class="col-sm-4">
                <input type="text" class="form-control" name="serial_number2" placeholder="Masukkan Serial Number">
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label"></label>
              <div class="col-sm-4">
                <input type="text" class="form-control" name="serial_number3" placeholder="Masukkan Serial Number">
              </div>
              <div class="col-sm-4">
                <input type="text" class="form-control" name="serial_number4" placeholder="Masukkan Serial Number">
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label"></label>
              <div class="col-sm-4">
                <input type="text" class="form-control" name="serial_number5" placeholder="Masukkan Serial Number">
              </div>
              <div class="col-sm-4">
                <input type="text" class="form-control" name="serial_number6" placeholder="Masukkan Serial Number">
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label"></label>
              <div class="col-sm-4">
                <input type="text" class="form-control" name="serial_number7" placeholder="Masukkan Serial Number">
              </div>
              <div class="col-sm-4">
                <input type="text" class="form-control" name="serial_number8" placeholder="Masukkan Serial Number">
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label"></label>
              <div class="col-sm-4">
                <input type="text" class="form-control" name="serial_number9" placeholder="Masukkan Serial Number">
              </div>
              <div class="col-sm-4">
                <input type="text" class="form-control" name="serial_number10" placeholder="Masukkan Serial Number">
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Keterangan</label>
              <div class="col-sm-10">
                <textarea type="text" class="form-control" name="keterangan" placeholder="Masukkan Keterangan"></textarea>
              </div>
            </div>
          </div><!-- /.box-body -->
          <p>Keterangan (*) : Wajib Diisi</p>
          <div class="box-footer">
            <button data-dismiss="modal" class="btn btn-default flat"><span class="glyphicon glyphicon-ban-circle"></span> Batal</button>
            <button type="submit" class="btn btn-primary pull-right flat" onclick="document.getElementById('loading2').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
          </div><!-- /.box-footer -->
        </form>
      </div>
      <div class="overlay" id="loading2" style="display:none">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
    </div>
  </div>
</div>

<div class="modal fade" id="tambah_promo_diskon" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content box">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="riwayatheader">Tambah Promo Diskon</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="post" action="{{ url('/tambah_promo_diskon') }}" autocomplete="off">
        <input type="hidden" name="produk_id" id="diskon_produk_id">
        {{ csrf_field() }}
          <div class="box-body">
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Nama</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="nama" id="diskon_nama" disabled="true">
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Minimal Pembelian Promo*</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="qty_beli" placeholder="Masukkan Nilai Jumlah Minimal Pembelian Promo" onKeyPress="return numbersonly(this, event)">
              </div>
            </div>
            <div class="form-group" style="display:block" id="diskon">
              <label for="inputPassword3" class="col-sm-2 control-label">Diskon*</label>
              <div class="col-sm-3">
                <input type="text" class="form-control" name="diskon" placeholder="Masukkan Diskon" onKeyPress="return numbersonly(this, event)" style="text-align:right" maxlength="3">
              </div>
              <label class="control-label">%</label>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Awal Periode*</label>
              <div class="col-sm-6">
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" id="diskon_datepicker1" data-date-format="dd-mm-yyyy" name="awal_periode" placeholder="Masukkan awal periode">
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Akhir Periode*</label>
              <div class="col-sm-6">
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" id="diskon_datepicker2" data-date-format="dd-mm-yyyy" name="akhir_periode" placeholder="Masukkan akhir periode">
                </div>
              </div>
            </div>
          </div><!-- /.box-body -->
          <p>Keterangan (*) : Wajib Diisi</p>
          <div class="box-footer">
            <button data-dismiss="modal" class="btn btn-default flat"><span class="glyphicon glyphicon-ban-circle"></span> Batal</button>
            <button type="submit" class="btn btn-primary pull-right flat" onclick="document.getElementById('loading3').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
          </div><!-- /.box-footer -->
        </form>
      </div>
      <div class="overlay" id="loading3" style="display:none">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
    </div>
  </div>
</div>

<div class="modal fade" id="ubah_promo_diskon" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content box">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="riwayatheader">Ubah Promo Diskon</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="post" action="{{ url('/ubah_promo_diskon') }}" autocomplete="off">
        <input type="hidden" name="promo_diskon_id" id="upd_promo_diskon_id">
        <input type="hidden" name="produk_id" value="{{ $produk->id }}">
            {{ csrf_field() }}
          <div class="box-body">
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Nama Produk</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" disabled="true" value="{{ $produk->nama }}">
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Minimal Pembelian Promo*</label>
              <div class="col-sm-4">
                <input type="text" class="form-control" name="qty_beli" id="upd_qty_beli" placeholder="Masukkan Nilai Jumlah Minimal Pembelian" onKeyPress="return numbersonly(this, event)">
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">Diskon*</label>
              <div class="col-sm-3">
                <input type="text" class="form-control" name="diskon" id="upd_diskon" placeholder="Masukkan Diskon" onKeyPress="return numbersonly(this, event)" style="text-align:right">
              </div>
              <label class="control-label">%</label>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Awal Periode*</label>
              <div class="col-sm-6">
                <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" id="diskon_datepicker3" data-date-format="dd-mm-yyyy" name="awal_periode" placeholder="Masukkan awal periode">
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Akhir Periode*</label>
              <div class="col-sm-6">
                <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" id="diskon_datepicker4" data-date-format="dd-mm-yyyy" name="akhir_periode" placeholder="Masukkan akhir periode">
                </div>
              </div>
            </div>
          </div><!-- /.box-body -->
          <p>Keterangan (*) : Wajib Diisi</p>
          <div class="box-footer">
            <button data-dismiss="modal" class="btn btn-default flat"><span class="glyphicon glyphicon-ban-circle"></span> Batal</button>
            <button type="submit" class="btn btn-primary pull-right flat" onclick="document.getElementById('loading4').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
          </div><!-- /.box-footer -->
        </form>
      </div>
      <div class="overlay" id="loading4" style="display:none">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
    </div>
  </div>
</div>

<div class="modal fade" id="tambah_promo_cashback" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content box">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="riwayatheader">Tambah Promo Cashback</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="post" action="{{ url('/tambah_promo_cashback') }}" autocomplete="off">
        <input type="hidden" name="produk_id" value="{{ $produk->id }}">
        {{ csrf_field() }}
          <div class="box-body">
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Nama</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="nama" disabled="true" value="{{ $produk->nama }}">
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Minimal Pembelian Promo*</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="qty_beli" placeholder="Masukkan Nilai Jumlah Minimal Pembelian Promo" onKeyPress="return numbersonly(this, event)">
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">Cashback*</label>
              <div class="col-sm-3">
                 <input type="text" class="form-control" name="cashback" placeholder="Masukkan cashback" id="cashback" data-thousands="." data-decimal="," style="text-align:right"/>
                <script type="text/javascript">$("#cashback").maskMoney({precision:0});</script>
              </div>
              <label class="control-label">(IDR)</label>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Awal Periode*</label>
              <div class="col-sm-6">
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" id="cashback_datepicker1" data-date-format="dd-mm-yyyy" name="awal_periode" placeholder="Masukkan awal periode">
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Akhir Periode*</label>
              <div class="col-sm-6">
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" id="cashback_datepicker2" data-date-format="dd-mm-yyyy" name="akhir_periode" placeholder="Masukkan akhir periode">
                </div>
              </div>
            </div>
          </div><!-- /.box-body -->
          <p>Keterangan (*) : Wajib Diisi</p>
          <div class="box-footer">
            <button data-dismiss="modal" class="btn btn-default flat"><span class="glyphicon glyphicon-ban-circle"></span> Batal</button>
            <button type="submit" class="btn btn-primary pull-right flat" onclick="document.getElementById('loading5').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
          </div><!-- /.box-footer -->
        </form>
      </div>
      <div class="overlay" id="loading5" style="display:none">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
    </div>
  </div>
</div>

<div class="modal fade" id="ubah_promo_cashback" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content box">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="riwayatheader">Ubah Promo Cashback</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="post" action="{{ url('/ubah_promo_cashback') }}" autocomplete="off">
        <input type="hidden" name="promo_cashback_id" id="upc_promo_cashback_id">
        <input type="hidden" name="produk_id" value="{{ $produk->id }}">
            {{ csrf_field() }}
          <div class="box-body">
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Nama Produk</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" disabled="true" value="{{ $produk->nama }}">
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Minimal Pembelian Promo*</label>
              <div class="col-sm-4">
                <input type="text" class="form-control" name="qty_beli" id="upc_qty_beli" placeholder="Masukkan Nilai Jumlah Minimal Pembelian" onKeyPress="return numbersonly(this, event)">
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">cashback*</label>
              <div class="col-sm-3">
                 <input type="text" class="form-control" name="cashback" placeholder="Masukkan cashback" id="upc_cashback" data-thousands="." data-decimal="," style="text-align:right"/>
                <script type="text/javascript">$("#upc_cashback").maskMoney({precision:0});</script>
              </div>
              <label class="control-label">(IDR)</label>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Awal Periode*</label>
              <div class="col-sm-6">
                <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" id="cashback_datepicker3" data-date-format="dd-mm-yyyy" name="awal_periode" placeholder="Masukkan awal periode">
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Akhir Periode*</label>
              <div class="col-sm-6">
                <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" id="cashback_datepicker4" data-date-format="dd-mm-yyyy" name="akhir_periode" placeholder="Masukkan akhir periode">
                </div>
              </div>
            </div>
          </div><!-- /.box-body -->
          <p>Keterangan (*) : Wajib Diisi</p>
          <div class="box-footer">
            <button data-dismiss="modal" class="btn btn-default flat"><span class="glyphicon glyphicon-ban-circle"></span> Batal</button>
            <button type="submit" class="btn btn-primary pull-right flat" onclick="document.getElementById('loading6').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
          </div><!-- /.box-footer -->
        </form>
      </div>
      <div class="overlay" id="loading6" style="display:none">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
    </div>
  </div>
</div>

<div class="modal fade" id="tambah_promo_hadiah" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content box">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="riwayatheader">Tambah Promo Hadiah</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="post" action="{{ url('/tambah_promo_hadiah') }}" autocomplete="off">
        <input type="hidden" name="produk_id" value="{{ $produk->id }}">
        {{ csrf_field() }}
          <div class="box-body">
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Nama</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="nama" value="{{ $produk->nama }}" disabled="true">
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Jumlah Pembelian Promo Hadiah*</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="qty_beli" placeholder="Masukkan Jumlah Pembelian Promo Hadiah" onKeyPress="return numbersonly(this, event)">
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">Hadiah*</label>
              <div class="col-sm-10">
                <select class="form-control" name="hadiah_id">
                  @foreach($hadiah as $valh)
                    <option value="{{ $valh->id }}">{{ $valh->nama }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Jumlah Hadiah*</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="qty_hadiah" placeholder="Masukkan Jumlah Hadiah" onKeyPress="return numbersonly(this, event)">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Awal Periode*</label>
              <div class="col-sm-6">
                <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" id="hadiah_datepicker1" data-date-format="dd-mm-yyyy" name="awal_periode" placeholder="Masukkan awal periode">
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Akhir Periode*</label>
              <div class="col-sm-6">
                <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" id="hadiah_datepicker2" data-date-format="dd-mm-yyyy" name="akhir_periode" placeholder="Masukkan akhir periode">
                </div>
              </div>
            </div>
          </div><!-- /.box-body -->
          <p>Keterangan (*) : Wajib Diisi</p>
          <div class="box-footer">
            <button data-dismiss="modal" class="btn btn-default flat"><span class="glyphicon glyphicon-ban-circle"></span> Batal</button>
            <button type="submit" class="btn btn-primary pull-right flat" onclick="document.getElementById('loading7').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
          </div><!-- /.box-footer -->
        </form>
      </div>
      <div class="overlay" id="loading7" style="display:none">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
    </div>
  </div>
</div>

<div class="modal fade" id="ubah_promo_hadiah" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content box">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="riwayatheader">Ubah Promo Hadiah</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="post" action="{{ url('/ubah_promo_hadiah') }}" autocomplete="off">
        <input type="hidden" name="id" id="uph_id">
        <input type="hidden" name="produk_id" value="{{ $produk->id }}">
            {{ csrf_field() }}
          <div class="box-body">
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Nama Produk</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" value="{{ $produk->nama }}" disabled="true">
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Pilih Hadiah*</label>
              <div class="col-sm-10">
                <select class="form-control" name="hadiah_id" id="uph_hadiah_id">
                    @foreach($hadiah as $val)
                    <option value="{{ $val->id }}">{{ $val->nama }}</option>
                    @endforeach
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Jumlah Minimal Pembelian Promo*</label>
              <div class="col-sm-4">
                <input type="text" class="form-control" name="qty_beli" id="uph_qty_beli" placeholder="Masukkan Nilai Jumlah Minimal Pembelian" onKeyPress="return numbersonly(this, event)">
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">Jumlah Hadiah yang Diberikan*</label>
              <div class="col-sm-4">
                <input type="text" class="form-control col-md-4" name="qty_hadiah" id="uph_qty_hadiah" placeholder="Masukkan Jumlah Diskon" onKeyPress="return numbersonly(this, event)">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Awal Periode*</label>
              <div class="col-sm-6">
                <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" id="hadiah_datepicker3" data-date-format="dd-mm-yyyy" name="awal_periode" placeholder="Masukkan awal periode">
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Akhir Periode*</label>
              <div class="col-sm-6">
                <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" id="hadiah_datepicker4" data-date-format="dd-mm-yyyy" name="akhir_periode" placeholder="Masukkan akhir periode">
                </div>
              </div>
            </div>
          </div><!-- /.box-body -->
          <p>Keterangan (*) : Wajib Diisi</p>
          <div class="box-footer">
            <button data-dismiss="modal" class="btn btn-default flat"><span class="glyphicon glyphicon-ban-circle"></span> Batal</button>
            <button type="submit" class="btn btn-primary pull-right flat" onclick="document.getElementById('loading8').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
          </div><!-- /.box-footer -->
        </form>
      </div>
      <div class="overlay" id="loading8" style="display:none">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
    </div>
  </div>
</div>

<div class="modal fade" id="tambah_produk_supplier" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content box">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="riwayatheader">Tambah Produk Supplier</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="post" action="{{ url('/tambah_produk_supplier') }}" autocomplete="off">
        <input type="hidden" name="produk_id" value="{{ $produk->id }}">
        {{ csrf_field() }}
          <div class="box-body">
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Supplier</label>
              <div class="col-sm-10">
                <select class="form-control" name="supplier_id">
                    @foreach($supplier as $val)
                    <option value="{{ $val->id }}">{{ $val->nama }}</option>
                    @endforeach
                </select>
              </div>
            </div>
          </div><!-- /.box-body -->
          <div class="box-footer">
            <button data-dismiss="modal" class="btn btn-default flat"><span class="glyphicon glyphicon-ban-circle"></span> batal</button>
            <button type="submit" class="btn btn-primary pull-right flat" onclick="document.getElementById('loading9').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
          </div><!-- /.box-footer -->
        </form>
      </div>
      <div class="overlay" id="loading9" style="display:none">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
    </div>
  </div>
</div>

<div class="modal fade" id="ubah_produk_galeri" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content box">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="riwayatheader">Ubah Produk Galeri</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="post" action="{{ url('/ubah_produk_galeri') }}" enctype="multipart/form-data">
        <input type="hidden" name="id" id="upg_id">
        <input type="hidden" name="produk_id" value="{{ $produk->id }}">
        {{ csrf_field() }}
          <div class="box-body">
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">Gambar</label>
              <div class="col-sm-10">
                <input type="file" class="form-control" name="edit_image" id="edit_image">
              </div>
            </div>
          </div><!-- /.box-body -->
          <div class="box-footer">
            <button data-dismiss="modal" class="btn btn-default flat"><span class="glyphicon glyphicon-ban-circle"></span> Batal</button>
            <button type="submit" class="btn btn-primary pull-right flat" onclick="document.getElementById('loading10').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
          </div><!-- /.box-footer -->
        </form>
      </div>
      <div class="overlay" id="loading10" style="display:none">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
    </div>
  </div>
</div>

@endsection


<script src="{{ asset('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ url('js/jquery.maskMoney.js') }}" type="text/javascript"></script>
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
  tinymce.init({ selector:'#deskripsi',
  height: 400,
  plugins: [
    'advlist autolink lists link image charmap print preview anchor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste code'
  ],
  toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image' });

  tinymce.init({ selector:'#deskripsi2',
  height: 400,
  plugins: [
    'advlist autolink lists link image charmap print preview anchor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste code'
  ],
  toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
  readonly : 1 });

  function ubah_promo_hadiah(id,hadiah_id,qty_beli,qty_hadiah,awl,akr)
    {
        document.getElementById('uph_id').value             = id;
        var element = document.getElementById('uph_hadiah_id');
        element.value = hadiah_id;
        document.getElementById('uph_qty_beli').value       = qty_beli;
        document.getElementById('uph_qty_hadiah').value     = qty_hadiah;
        document.getElementById('hadiah_datepicker3').value   = awl;
        document.getElementById('hadiah_datepicker4').value  = akr;
    }

    function ubah_promo_diskon(promo_diskon_id,qty_beli,diskon,awl,akr)
    {
        document.getElementById('upd_promo_diskon_id').value    = promo_diskon_id;
        document.getElementById('upd_qty_beli').value           = qty_beli;
        document.getElementById('upd_diskon').value             = diskon;
        document.getElementById('diskon_datepicker3').value     = awl;
        document.getElementById('diskon_datepicker4').value     = akr;
    }

    function ubah_promo_cashback(promo_cashback_id,qty_beli,cashback,awl,akr)
    {
        document.getElementById('upc_promo_cashback_id').value    = promo_cashback_id;
        document.getElementById('upc_qty_beli').value           = qty_beli;
        document.getElementById('upc_cashback').value             = cashback;
        document.getElementById('cashback_datepicker3').value     = awl;
        document.getElementById('cashback_datepicker4').value     = akr;
    }

    function ubah(harga, jumlah_spesifikasi)
    {
        document.getElementById("nama").disabled = false;
        document.getElementById("satuan").disabled = false;
        document.getElementById("deskripsi").disabled = false;
        document.getElementById("berat").disabled = false;
        document.getElementById("harga").disabled = false;
        document.getElementById("harga").value = harga;
        //document.getElementById("stok").disabled = false;
        document.getElementById("kode").disabled = false;
        
        document.getElementById("ubah").style.display = 'none';
        document.getElementById("batal").style.display = 'block';
        document.getElementById("button_submit").style.display = 'block';

        document.getElementById("astket").innerHTML = "Keterangan (*) : Wajib Diisi";
        document.getElementById("ast1").innerHTML = "*";
        document.getElementById("ast2").innerHTML = "*";
        document.getElementById("ast3").innerHTML = "*";
        document.getElementById("ast4").innerHTML = "*";
        document.getElementById("ast5").innerHTML = "*";
        document.getElementById("ast6").innerHTML = "*";

        document.getElementById("display_deskripsi").style.display = 'none';
        document.getElementById("display_deskripsi2").style.display = 'block';
        document.getElementById("select_kategori").style.display = 'block';
        document.getElementById("display_kategori").style.display = 'none';

        var kategori_id = document.getElementById("ori_kategori_id").value;
        kategori_id = parseInt(kategori_id);
        document.getElementById("kategori_produk_id").value = kategori_id;

        for (var i = 0; i < jumlah_spesifikasi; i++) {
            document.getElementById('spesifikasi_nilai'.concat(i)).disabled = false;
            document.getElementById('spesifikasi_satuan'.concat(i)).disabled = false;
        }

    }
    function batal(harga, jumlah_spesifikasi)
    {
        document.getElementById("nama").disabled = true;
        document.getElementById("satuan").disabled = true;
        document.getElementById("deskripsi").disabled = true;
        document.getElementById("berat").disabled = true;
        document.getElementById("harga").disabled = true;
        document.getElementById("harga").value = harga;
        document.getElementById("kode").disabled = true;
        //document.getElementById("stok").disabled = true;

        
        document.getElementById("ubah").style.display = 'block';
        document.getElementById("batal").style.display = 'none';
        document.getElementById("button_submit").style.display = 'none';

        document.getElementById("nama").value       = document.getElementById("ori_nama").value;
        document.getElementById("satuan").value     = document.getElementById("ori_satuan").value;
        document.getElementById("deskripsi").value  = document.getElementById("ori_deskripsi").value;
        document.getElementById("berat").value      = document.getElementById("ori_berat").value;
        document.getElementById("kode").value       = document.getElementById("ori_kode").value;

        document.getElementById("astket").innerHTML = "";
        document.getElementById("ast1").innerHTML = "";
        document.getElementById("ast2").innerHTML = "";
        document.getElementById("ast3").innerHTML = "";
        document.getElementById("ast4").innerHTML = "";
        document.getElementById("ast5").innerHTML = "";
        document.getElementById("ast6").innerHTML = "";

        document.getElementById("display_deskripsi").style.display = 'block';
        document.getElementById("display_deskripsi2").style.display = 'none';
        document.getElementById("select_kategori").style.display = 'none';
        document.getElementById("display_kategori").style.display = 'block';

        for (var i = 0; i < jumlah_spesifikasi; i++) {
            document.getElementById('spesifikasi_nilai'.concat(i)).disabled = false;
            document.getElementById('spesifikasi_satuan'.concat(i)).disabled = false;
        }
    }
    function hadiah_button(produk_id,nama)
    {
        document.getElementById("hadiah_produk_id").value   = produk_id;
        document.getElementById("hadiah_nama").value          = nama;
    }

    function ubah_produk_galeri(id)
    {
        document.getElementById("upg_id").value          = id;
    }

    function tambah_sn()
    {
        alert('nambah')
    }

    function btn_hapus_serial_number(produk_id,serial_number,gudang_id)
    {
        var cek = confirm("Apakah anda yakin akan menghapus serial number?");

        if(cek)
        {
            document.getElementById("loading").style.display    = 'block';
            document.getElementById("hsn_produk_id").value    = produk_id;
            document.getElementById("hsn_serial_number").value  = serial_number;
            document.getElementById("hsn_gudang_id").value    = gudang_id;
            document.getElementById("form_hapus_serial_number").submit();
        }
    }

    function hapus_promo_diskon(id)
    {
        var cek = confirm("Apakah anda yakin akan menghapus promo diskon?");

        if(cek)
        {
            document.getElementById("loading").style.display    = 'block';
            document.getElementById("hpd_id").value             = id;
            document.getElementById("form_hapus_promo_diskon").submit();
        }
    }

    function hapus_promo_cashback(id)
    {
        var cek = confirm("Apakah anda yakin akan menghapus promo cashback?");

        if(cek)
        {
            document.getElementById("loading").style.display    = 'block';
            document.getElementById("hpc_id").value             = id;
            document.getElementById("form_hapus_promo_cashback").submit();
        }
    }

    function tambah_promo_diskon(produk_id,nama)
    {
        document.getElementById("diskon_produk_id").value   = produk_id;
        document.getElementById("diskon_nama").value          = nama;
    }

    function hapus_supplier(produk_id, supplier_id)
    {
        var cek = confirm("Apakah anda yakin akan menghapus supplier?");

        if(cek)
        {
            document.getElementById("loading").style.display    = 'block';
            document.getElementById("hs_produk_id").value     = produk_id;
            document.getElementById("hs_supplier_id").value   = supplier_id;
            document.getElementById("form_hapus_produk_supplier").submit();
        }
    }

    function hapus_promo_hadiah(id)
    {
        var cek = confirm("Apakah anda yakin akan menghapus produk hadiah?");

        if(cek)
        {
            document.getElementById("loading").style.display    = 'block';
            document.getElementById("hph_id").value             = id;
            document.getElementById("form_hapus_promo_hadiah").submit();
        }
    }

    function hapus_produk_galeri(id)
    {
        var cek = confirm("Apakah anda yakin akan menghapus galeri?");

        if(cek)
        {
            document.getElementById("loading").style.display    = 'block';
            document.getElementById("hg_id").value      = id;
            document.getElementById("form_hapus_produk_galeri").submit();
        }
    }

  $(function () {
    $("#mydatatables").DataTable();
    $("#mydatatables2").DataTable();
    $("#mydatatables3").DataTable();
    $("#mydatatables4").DataTable();
    $("#mydatatables5").DataTable();
    $("#mydatatables6").DataTable();

    $('#diskon_datepicker1').datepicker({
      autoclose: true,
      todayHighlight: 1,
    });

    $('#diskon_datepicker2').datepicker({
      autoclose: true,
      todayHighlight: 1,
    });

    $('#diskon_datepicker3').datepicker({
      autoclose: true,
      todayHighlight: 1,
    });

    $('#cashback_datepicker4').datepicker({
      autoclose: true,
      todayHighlight: 1,
    });

    $('#cashback_datepicker1').datepicker({
      autoclose: true,
      todayHighlight: 1,
    });

    $('#cashback_datepicker2').datepicker({
      autoclose: true,
      todayHighlight: 1,
    });

    $('#cashback_datepicker3').datepicker({
      autoclose: true,
      todayHighlight: 1,
    });

    $('#cashback_datepicker4').datepicker({
      autoclose: true,
      todayHighlight: 1,
    });

    $('#cashback_datepicker4').datepicker({
      autoclose: true,
      todayHighlight: 1,
    });

    $('#hadiah_datepicker1').datepicker({
      autoclose: true,
      todayHighlight: 1,
    });

    $('#hadiah_datepicker2').datepicker({
      autoclose: true,
      todayHighlight: 1,
    });

    $('#hadiah_datepicker3').datepicker({
      autoclose: true,
      todayHighlight: 1,
    });

    $('#hadiah_datepicker4').datepicker({
      autoclose: true,
      todayHighlight: 1,
    });
  });

  $(document).ready(function(){

           // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // save the latest tab; use cookies if you like 'em better:
                localStorage.setItem('lastTab_detail_produk', $(this).attr('href'));
            });

            // go to the latest tab, if it exists:
            var lastTab_detail_produk = localStorage.getItem('lastTab_detail_produk');
            if (lastTab_detail_produk) {
                $('[href="' + lastTab_detail_produk + '"]').tab('show');
            }
            else
            {
                lastTab_detail_produk = "#tab_1";
                $('[href="' + lastTab_detail_produk + '"]').tab('show');
            }
      });
</script>