<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <a href="{{ url('/tambah_supplier') }}">
                  <button class='btn bg-purple flat'><span class='fa fa-plus'></span> Tambah Supplier</button>
                </a>
            </div>
            <div class="box-body">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <table id="mydatatables" class="table table-striped table-bordered">
                    <thead>
                        <th style="text-align:center">NO</th>
                        <th style="text-align:center">NAMA</th>
                        <th style="text-align:center">ALAMAT</th>
                        <th style="text-align:center">NAMA SALES</th>
                        <th style="text-align:center"></th>
                        <th></th>
                    </thead>
                    <tbody>
                        <?php $i=1; ?>
                        @foreach($supplier as $val)
                        <tr>
                            <td align="center">{{ $i++ }}</td>
                            <td>{{ $val->nama }}</td>
                            <td>{{ $val->alamat }}<br><b>{{ ucwords(strtolower($val->kota_nama)) }}</b></td>
                            <td>{{ $val->nama_sales }}</td>
                            <td align="center">
                                <label class="switch">
                                    <input type="checkbox" onchange='window.location.href="{{ url('/ubah_status_supplier/'.$val->id) }}"' <?php if($val->is_aktif == 1) echo "checked"; else echo ""; ?>>
                                    <div class="slider" data-toggle="tooltip" title="status aktif"></div>
                                </label>
                            </td>
                            <td align="center">
                              <a class="btn btn-info flat" href="{{ url('/detail_supplier/'.$val->id) }}" data-toggle="tooltip" title="detail" style="font-size: 15pt"><i class="fa fa-info-circle"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop
<style type="text/css">
    .tooltip-inner {
        white-space:nowrap;
        max-width:none;
    }
</style>
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script>
  $(function () {
    $("#mydatatables").DataTable();
  });
</script>