<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
              <a href="{{ url('/tambah_kategori_produk') }}">
                <button class='btn bg-purple flat'><span class='fa fa-plus'></span> Tambah Kategori Produk</button>
              </a>
            </div>
            <div class="box-body">
            @if(Session::has('message'))
            <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
              <i class="fa fa-check"></i>
              {{ Session::get('message') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            @endif
            @if ($errors->has('nama'))
            <div class="alert alert-danger flat">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                Kolom Nama Wajib Diisi
            </div>
            @endif
            @if ($errors->has('deskripsi'))
            <div class="alert alert-danger flat">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                Kolom Deskripsi Wajib Diisi
            </div>
            @endif
            <table class="table table-striped table-bordered" id="mydatatables">
                <thead>
                  <tr>
                      <th style="text-align:center">NO</th>
                      <th style="text-align:center">NAMA</th>
                      <th style="text-align:center">DESKRIPSI</th>
                      <th style="text-align:center">KOLOM SPESIFIKASI</th>
                      <th style="text-align:center">ICON</th>
                      <th style="text-align:center"></th>
                      <th></th>
                    </tr>
                </thead>
                <tbody>
                  <?php $i = 1; ?>
                  @foreach($kategori_produk as $val)
                  <tr>
                    <td align="center">{{ $i++ }}</td>
                    <td>{{ $val->nama }}</td>
                    <td>{{ $val->deskripsi }}</td>
                    <td>
                        @foreach($val->parameter_spesifikasi as $key => $val2)
                            @if($key==0)
                                {{ $val2->nama }}
                            @else
                                , {{ $val2->nama }}
                            @endif
                        @endforeach
                    </td>
                    <td align="center">
                        <img src="{{ URL::asset('img/kategori/'.$val->file_gambar) }}" height="70px"><br><br>
                        <a data-toggle="modal" data-target="#ubah_icon">
                        <button class="btn btn-warning flat" onclick="ubah_icon('{{ $val->id }}')" data-toggle="tooltip" title="ubah"><i class="fa fa-edit"></i> Ubah Icon</button>
                        </a>
                    </td>
                    <td align="center"> <?php $spek = json_encode($val->parameter_spesifikasi); ?>
                        <label class="switch">
                            <input type="checkbox" onchange='window.location.href="{{ url('/ubah_status_kategori_produk/'.$val->id) }}"' <?php if($val->is_aktif == 1) echo "checked"; else echo ""; ?>>
                            <div class="slider" data-toggle="tooltip" title="status aktif"></div>
                        </label>
                    </td>
                    <td align="center">
                      <a data-toggle="modal" data-target="#ubah_kategori">
                          <button class="btn btn-warning flat" onclick="ubah_button('{{ $val->id }}', '{{ $val->nama }}', '{{ $val->deskripsi }}', '{{ $spek }}')" data-toggle="tooltip" title="ubah" style="font-size: 15pt"><i class="fa fa-edit"></i></button>
                        </a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ubah_kategori" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content box">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="riwayatheader">Ubah Kategori Produk</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="post" action="{{ url('/ubah_kategori_produk') }}">
        <input type="hidden" name="id" id="id">
        <input type="hidden" name="jumlah_kolom_spesifikasi" id="jumlah_kolom_spesifikasi" value="1">
        {{ csrf_field() }}
          <div class="box-body">
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Nama*</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama Kategori" style="text-transform:capitalize">
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Deskripsi*</label>
              <div class="col-sm-10">
                <textarea type="text" class="form-control" name="deskripsi" id="deskripsi" placeholder="Masukkan Deskripsi"></textarea>
              </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Kolom Spesifikasi</label>
                <div class="col-sm-10">
                    <div id="put_kolom_spesifikasi"></div>
                    <div class="row">
                        <div class="col-sm-6">
                            <input type="text" name="kolom_spesifikasi" id="kolom_spesifikasi" class="form-control" style="text-transform:capitalize" placeholder="Masukkan kolom spesifikasi, ex: memori">
                        </div>
                        <div class="col-sm-2">
                            <button class="btn btn-primary flat" type="button" onclick="tambah_kolom_spesifikasi()" data-toggle="tooltip" title="tambah" style="font-size: 15pt"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>
                </div>
            </div>
          </div><!-- /.box-body -->
          <p>Keterangan (*) : Wajib Diisi</p>
          <div class="box-footer">
            <button data-dismiss="modal" class="btn btn-default flat"><span class="glyphicon glyphicon-ban-circle"></span> Batal</button>
            <button type="submit" class="btn btn-primary pull-right flat" onclick="document.getElementById('loading').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
          </div><!-- /.box-footer -->
        </form>
      </div>
      <div class="overlay" id="loading" style="display:none">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
    </div>
  </div>
</div>

<div class="modal fade" id="ubah_icon" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content box">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="riwayatheader">Ubah Icon</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="post" action="{{ url('/ubah_icon_kategori_produk') }}" enctype="multipart/form-data">
            <input type="hidden" name="id" id="ubah_icon_id">
            {{ csrf_field() }}
          <div class="box-body">
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">File Gambar</label>
              <div class="col-sm-10">
                <input type="file" class="form-control" name="file_gambar">
              </div>
            </div>
          </div><!-- /.box-body -->
          <div class="box-footer">
            <button data-dismiss="modal" class="btn btn-default flat"><span class="glyphicon glyphicon-ban-circle"></span> Batal</button>
            <button type="submit" class="btn btn-primary pull-right flat" onclick="document.getElementById('loading2').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
          </div><!-- /.box-footer -->
        </form>
      </div>
      <div class="overlay" id="loading2" style="display:none">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
    </div>
  </div>
</div>
@stop
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script>
  $(function () {
    $("#mydatatables").DataTable();
  });
</script>

<script type="text/javascript">
    function ubah_button(id,nama,deskripsi, spek)
    {
        myspek = JSON.parse(spek);

        document.getElementById("id").value       = id;
        document.getElementById("nama").value       = nama;
        document.getElementById("deskripsi").value  = deskripsi;

        for (var i = 0; i < myspek.length; i++) {

            $('#put_kolom_spesifikasi').append('<div class="row" style="padding-bottom:15px;" id="old_div_spesifikasi'+myspek[i]['id']+'">\
                        <div class="col-md-6">\
                            <input type="text" name="old_kolom_spesifikasi'+myspek[i]['id']+'" value="'+myspek[i]['nama']+'"  style="text-transform:capitalize" class="form-control" >\
                        </div>\
                        <div class="col-md-2">\
                            <button class="btn btn-danger flat" type="button" onclick="old_hapus_kolom_spesifikasi('+myspek[i]['id']+')" data-toggle="tooltip" title="hapus" style="font-size: 15pt"><i class="fa fa-trash-o"></i></button>\
                        </div>\
                    </div>');
        }

        document.getElementById('jumlah_kolom_spesifikasi').value = myspek.length + 1;
    }

    function tambah_kolom_spesifikasi()
    {
        var jumlah = document.getElementById('jumlah_kolom_spesifikasi').value;
        jumlah = parseInt(jumlah);

        var kolom_spesifikasi = document.getElementById('kolom_spesifikasi').value;
        document.getElementById('kolom_spesifikasi').value          = "";

        $('#put_kolom_spesifikasi').append('<div class="row" style="padding-bottom:15px;" id="new_div_spesifikasi'+jumlah+'">\
                    <div class="col-md-6">\
                        <input type="text" name="new_kolom_spesifikasi'+jumlah+'" value="'+kolom_spesifikasi+'"  style="text-transform:capitalize" class="form-control" d>\
                    </div>\
                    <div class="col-md-2">\
                        <button class="btn btn-danger flat" type="button" onclick="new_hapus_kolom_spesifikasi('+jumlah+')" data-toggle="tooltip" title="hapus" style="font-size: 15pt"><i class="fa fa-trash-o"></i></button>\
                    </div>\
                </div>');

        jumlah++;
        document.getElementById('jumlah_kolom_spesifikasi').value = jumlah;
    }

    function old_hapus_kolom_spesifikasi(id)
    {
        document.getElementById('old_div_spesifikasi'.concat(id)).innerHTML = "";
        document.getElementById('old_div_spesifikasi'.concat(id)).style.padding = '0px';
    }

    function new_hapus_kolom_spesifikasi(id)
    {
        document.getElementById('new_div_spesifikasi'.concat(id)).innerHTML = "";
        document.getElementById('new_div_spesifikasi'.concat(id)).style.padding = '0px';
    }

    function ubah_icon(id)
    {
        document.getElementById('ubah_icon_id').value = id;
    }
</script>