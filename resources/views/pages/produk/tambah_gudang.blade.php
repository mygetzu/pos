@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<style>
    #peta {
      height: 300px;
      width: 600px;
    }
</style>
<input type="hidden" name="base_path" id="base_path" value="{{ asset('/') }}">
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <div class="pull-right">
                    <a href="{{ url('/gudang') }}">
                        <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                    </a>
                </div>
            </div>
            <div class="box-body">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <form class="form-horizontal" method="POST" action="{{ url('/do_tambah_gudang') }}" autocomplete="off">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                        <label class="control-label col-md-2">Nama*</label>
                        <div class="col-md-8">
                            <input type="text" value="{{ old('nama') }}" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama Gudang" style="text-transform:capitalize">
                            @if ($errors->has('nama'))
                                <span class="help-block">
                                    <strong>Kolom Nama Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('alamat') ? ' has-error' : '' }}">
                        <label class="control-label col-md-2">Alamat*</label>
                        <div class="col-md-8">
                            <textarea type="text" class="form-control" name="alamat" id="alamat" placeholder="Masukkan Alamat" style="text-transform:capitalize">{{ old('alamat') }}</textarea>
                            @if ($errors->has('alamat'))
                                <span class="help-block">
                                    <strong>Kolom Alamat Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('kota') ? ' has-error' : '' }}">
                        <label class="control-label col-md-2">Kabupaten/Kota*</label>
                        <div class="col-md-4">
                            <select class="form-control" id="provinsi" name="provinsi">
                                <option value="">-- Pilih Provinsi --</option>
                                @foreach($provinsi as $val)
                                <option value="{{ $val->kode }}">{{ $val->nama }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('provinsi'))
                                <span class="help-block">
                                    <strong>Pilihan Provinsi Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                        <div class="col-md-4">
                            <select class="form-control" id="kota" name="kota">
                                <option value="">-- Pilih Kabupaten/Kota --</option>
                            </select>
                            @if ($errors->has('kota'))
                                <span class="help-block">
                                    <strong>Pilihan Kabupaten/Kota Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('telp') ? ' has-error' : '' }}">
                        <label class="control-label col-md-2">Telepon*</label>
                        <div class="col-md-4">
                            <div class="input-group" style="margin-bottom:0px">
                                <div class="input-group-addon">
                                    <i class="fa fa-phone"></i>
                                </div>
                                <input type="text" value="{{ old('telp') }}" class="form-control" data-mask placeholder="contoh: (031)-9999999" name="telp" id="telp">
                            </div>
                            @if ($errors->has('telp'))
                                <span class="help-block">
                                    <strong>Kolom Telepon Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('supervisor') ? ' has-error' : '' }}">
                        <label class="control-label col-md-2">Supervisor*</label>
                        <div class="col-md-8">
                            <input type="text" value="{{ old('supervisor') }}" class="form-control" name="supervisor" id="supervisor" placeholder="Masukkan Nama Supervisor" style="text-transform:capitalize">
                            @if ($errors->has('supervisor'))
                                <span class="help-block">
                                    <strong>Kolom Supervisor Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('hp_supervisor') ? ' has-error' : '' }}">
                        <label class="control-label col-md-2">HP Supervisor*</label>
                        <div class="col-md-4">
                            <div class="input-group" style="margin-bottom:0px">
                                <div class="input-group-addon">
                                    <i class="fa fa-mobile" style="padding:2px"></i>
                                </div>
                                <input type="text" value="{{ old('hp_supervisor') }}" class="form-control" data-inputmask='"mask": "(+62)999-9999-9999"' data-mask2 placeholder="contoh: (+62)857-9999-9999" name="hp_supervisor" id="hp_supervisor">
                            </div>
                            @if ($errors->has('hp_supervisor'))
                                <span class="help-block">
                                    <strong>Kolom HP Supervisor Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('latitude') ? ' has-error' : '' }}">
                        <label class="control-label col-md-2">Latitude</label>
                        <div class="col-md-8">
                            <input type="hidden" value="{{ old('latitude') }}" class="form-control" name="latitude" id="latitude" placeholder="Masukkan Koordinat Latitude">
                            <input type="text" value="{{ old('latitude') }}" class="form-control" name="latitude2" id="latitude2" placeholder="Masukkan Koordinat Latitude" disabled="true">
                            @if ($errors->has('latitude'))
                                <span class="help-block">
                                    <strong>Kolom Latitude Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('longitude') ? ' has-error' : '' }}">
                        <label class="control-label col-md-2">Longitude</label>
                        <div class="col-md-8">
                            <input type="hidden" value="{{ old('longitude') }}" class="form-control" name="longitude" id="longitude" placeholder="Masukkan Koordinat Longitude">
                            <input type="text" value="{{ old('longitude') }}" class="form-control" name="longitude2" id="longitude2" placeholder="Masukkan Koordinat Longitude" disabled="true">
                            @if ($errors->has('longitude'))
                                <span class="help-block">
                                    <strong>Kolom Longitude Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2">Lokasi</label>
                        <div class="col-md-8">
                            <div id="peta"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-2" id="button_submit">
                            <button type="submit" class="btn btn-primary flat" onclick="document.getElementById('loading').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="overlay" id="loading" style="display:none">
              <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>
@stop
<!-- jQuery 2.2.0 -->
<script src="{{ asset('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('/adminlte/plugins/select2/select2.full.min.js') }}"></script>
<!-- InputMask -->
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();
    
    $("[data-mask]").inputmask({ mask: "(999[9])-9999999", greedy: false ,
        definitions: {
                '*': {
                validator: "[0-9-]",
                cardinality: 1,
                casing: "lower"
            }
        }
    });
    
    $("[data-mask2]").inputmask();
  });

  $(document).ready(function(){
        $('#provinsi').change(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            var kode_provinsi = document.getElementById('provinsi').value;
            if(kode_provinsi == "")
                kode_provinsi = "KOSONG"
            var path =  document.getElementById('base_path').value;
            var url = path+"get_kota/"+kode_provinsi;

            $.ajax({
                type: "POST",
                url: url,
                data: $("#form_provinsi").serialize(),
                success: function (data) {
                    $("#kota").html(data);
                },
                error: function (data) {
                    alert('ooo');
                }
            });
        });
    });
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBxGbZh_n2hLPuHS1owKGKhSDThUbC4544"></script>
<script>
    var map;
    function initialize() 
    {
        var la = document.getElementById("latitude").value;
        var lo = document.getElementById("longitude").value

        if(la == "") la = '-7.2754665';
        if(lo == "") lo = '112.6416432';

        var myLatlng = new google.maps.LatLng(la,lo);
            var myOptions = {
            zoom: 16,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        map = new google.maps.Map(document.getElementById("peta"), myOptions); 
        var marker = new google.maps.Marker({
            draggable: true,
            position: myLatlng, 
            map: map,
            title: "Pilih Lokasi Anda"
        });

        google.maps.event.addListener(marker, 'drag', function (event) {
            document.getElementById("latitude").value = event.latLng.lat();
            document.getElementById("longitude").value = event.latLng.lng();
            document.getElementById("latitude2").value = event.latLng.lat();
            document.getElementById("longitude2").value = event.latLng.lng();
        });

        google.maps.event.addListener(marker, 'click', function (event) {
            document.getElementById("latitude").value = event.latLng.lat();
            document.getElementById("longitude").value = event.latLng.lng();
            document.getElementById("latitude2").value = event.latLng.lat();
            document.getElementById("longitude2").value = event.latLng.lng();
        });

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                
                myLatlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                var myOptions = {
                    zoom: 16,
                    center: myLatlng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                }

                map = new google.maps.Map(document.getElementById("peta"), myOptions); 
                var marker = new google.maps.Marker({
                    draggable: true,
                    position: myLatlng, 
                    map: map,
                    title: "Pilih Lokasi Anda"
                });

                google.maps.event.addListener(marker, 'drag', function (event) {
                    document.getElementById("latitude").value = event.latLng.lat();
                    document.getElementById("longitude").value = event.latLng.lng();
                    document.getElementById("latitude2").value = event.latLng.lat();
                    document.getElementById("longitude2").value = event.latLng.lng();
                });

                google.maps.event.addListener(marker, 'click', function (event) {
                    document.getElementById("latitude").value = event.latLng.lat();
                    document.getElementById("longitude").value = event.latLng.lng();
                    document.getElementById("latitude2").value = event.latLng.lat();
                    document.getElementById("longitude2").value = event.latLng.lng();
                });
            });
        }

        
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>