<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<input type="hidden" name="base_path" id="base_path" value="{{ asset('/') }}">
<input type="hidden" name="ori_nama" id="ori_nama" value="{{ $supplier->nama }}">
<input type="hidden" name="ori_alamat" id="ori_alamat" value="{{ $supplier->alamat }}">
<input type="hidden" name="ori_telp1" id="ori_telp1" value="{{ $supplier->telp1 }}">
<input type="hidden" name="ori_telp2" id="ori_telp2" value="{{ $supplier->telp2 }}">
<input type="hidden" name="ori_nama_sales" id="ori_nama_sales" value="{{ $supplier->nama_sales }}">
<input type="hidden" name="ori_hp_sales" id="ori_hp_sales" value="{{ $supplier->hp_sales }}">
<input type="hidden" name="ori_deskripsi" id="ori_deskripsi" value="{{ $supplier->deskripsi }}">
<input type="hidden" name="ori_umur_hutang" id="ori_umur_hutang" value="{{ $supplier->umur_hutang }}">
<form action="{{ url('/hapus_rekening_supplier') }}" method="post" id="form_hapus_rekening">
    {{ csrf_field() }}
    <input type="hidden" name="supplier_id_hapus" id="supplier_id_hapus">
    <input type="hidden" name="rekening_id_supplier" id="rekening_id_supplier">
</form>
<div class="row">
    <div class="col-md-12">
        <div class="nav-tabs-custom box tab-primary flat" style="border-top: 0">
            <ul class="nav nav-tabs">
              <li><a href="#tab_1" data-toggle="tab">Detail Supplier</a></li>
              <li><a href="#tab_2" data-toggle="tab">Rekening Supplier</a></li>
              <li><a href="#tab_3" data-toggle="tab">Data Produk Supplier</a></li>
              <li><a href="#tab_4" data-toggle="tab">Data Hadiah Supplier</a></li>
              <li class="pull-right">
                <a href="{{ url('/supplier') }}" style="padding:0px">
                      <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                  </a>
              </li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane" id="tab_1">
                <div id="ubah" style="display:block">
                  <div class="">
                    <button class="btn btn-warning flat" onclick="ubah('{{ $my_kota->provinsi_id }}','{{ $my_kota->kota_id }}')"><span class='fa fa-edit'></span> Ubah</button>
                  </div>
                </div>
                <div id="batal" style="display:none">
                  <div class="">
                    <button class="btn btn-default flat" onclick="batal()"><span class='fa fa-ban'></span> Batal</button>
                  </div>
                </div>
                <br>
                @if(Session::has('message1'))
              <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                <i class="fa fa-check"></i>
                {{ Session::get('message1') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              @endif
              <form class="form-horizontal" action="{{ url('/ubah_supplier') }}" method="post" autocomplete="off">
                <input type="hidden" name="id" value="{{ $supplier->id }}">
                {{ csrf_field() }}
                <table class="table table-striped">
                    <tr class="{{ $errors->has('nama') ? ' has-error' : '' }}">
                        <th width="20%"><label class="control-label">Nama</label><label id="ast1" class="control-label"></label></th>
                        <th width="5%">:</th>
                        <td>
                            <div class="col-md-12">
                                <input type="text" class="form-control" name="nama" id="nama" value="{{ $supplier->nama }}" disabled="true" placeholder="Masukkan Nama" style="text-transform:capitalize">
                                @if ($errors->has('nama'))
                                    <span class="help-block">
                                        <strong>Kolom Nama Wajib Diisi</strong>
                                    </span>
                                @endif
                            </div>
                        </td>
                    </tr>
                    <tr class="{{ $errors->has('alamat') ? ' has-error' : '' }}">
                        <th><label class="control-label">Alamat</label><label id="ast2" class="control-label"></label></th>
                        <th>:</th>
                        <td>
                            <div class="col-md-12">
                                <textarea type="text" class="form-control" name="alamat" id="alamat" disabled="true" placeholder="Masukkan Alamat" style="text-transform:capitalize">{{ $supplier->alamat }}</textarea>
                                @if ($errors->has('alamat'))
                                    <span class="help-block">
                                        <strong>Kolom Alamat Wajib Diisi</strong>
                                    </span>
                                @endif
                            </div>
                        </td>
                    </tr>
                    <tr class="{{ $errors->has('kota') ? ' has-error' : '' }}">
                        <th><label class="control-label">Kabupaten/Kota</label><label id="ast3" class="control-label"></th>
                        <th>:</th>
                        <td>
                        <div class="col-md-12" id="kota_saya">
                            <input type="text" class="form-control" value="{{ ucwords(strtolower($my_kota->nama)) }}" disabled="true" placeholder="Masukkan Kota">
                            @if ($errors->has('provinsi') AND $errors->has('kota'))
                                    <span class="help-block">
                                        <strong>Pilihan Provinsi Serta Kabupaten/Kota Wajib Diisi</strong>
                                    </span>
                            @elseif ($errors->has('provinsi'))
                                <span class="help-block">
                                    <strong>Pilihan Provinsi Wajib Diisi</strong>
                                </span>
                            @elseif ($errors->has('kota'))
                                <span class="help-block">
                                    <strong>Pilihan Kabupaten/Kota Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                        <div class="col-md-5" id="edit_kota_saya" style="display:none">
                            <select class="form-control" id="provinsi" name="provinsi">
                                <option value="">-- Pilih Provinsi --</option>
                                @foreach($provinsi as $val)
                                <option value="{{ $val->kode }}">{{ $val->nama }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('provinsi'))
                                <span class="help-block">
                                    <strong>Pilihan Provinsi Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                        <div class="col-md-5" id="edit_kota_saya2" style="display:none">
                            <select class="form-control" id="kota" name="kota">
                                
                            </select>
                            @if ($errors->has('kota'))
                                <span class="help-block">
                                    <strong>Pilihan Kabupaten/Kota Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                        </td>
                    </tr>
                    <tr>
                    <th><label class="control-label">Telepon 1</label></th>
                    <th>:</th>
                    <td>
                        <div class="col-md-4">
                            <div class="input-group" style="margin-bottom:0px">
                                <div class="input-group-addon">
                                    <i class="fa fa-phone"></i>
                                </div>
                                <input style="display:none" type="text" class="form-control" data-mask placeholder="contoh: (031)-9999999" name="telp1" id="telp1" value="{{ $supplier->telp1 }}">

                                <input style="display:block" type="text" class="form-control" placeholder="contoh: (031)-9999999" id="display_telp1" disabled="true" value="<?php echo str_replace('_', '', $supplier->telp1); ?>">
                            </div>
                        </div>
                    </td>
                    </tr>
                    <tr>
                        <th><label class="control-label">Telepon 2</label></th>
                            <th>:</th>
                            <td>
                            <div class="col-md-4">
                                <div class="input-group" style="margin-bottom:0px">
                                    <div class="input-group-addon">
                                        <i class="fa fa-phone"></i>
                                    </div>
                                    <input style="display:none" type="text" class="form-control" data-mask placeholder="contoh: (031)-9999999" name="telp2" id="telp2" value="{{ $supplier->telp2 }}">

                                    <input style="display:block" type="text" class="form-control" placeholder="contoh: (031)-9999999" id="display_telp2" disabled="true" value="<?php echo str_replace('_', '', $supplier->telp2); ?>">
                                </div>
                            </div>
                            </td>
                        </tr>
                    <tr>
                        <th>Nama Sales</th>
                        <th>:</th>
                        <td>
                            <div class="col-md-12">
                                <input type="text" class="form-control" name="nama_sales" id="nama_sales" value="{{ $supplier->nama_sales }}" disabled="true" placeholder="Masukkan Nama Sales" style="text-transform:capitalize">
                            </div>
                        </td>
                    </tr>
                    <tr>
                            <th><label class="control-label">HP Sales</label></th>
                            <th>:</th>
                            <td>
                            <div class="col-md-4">
                                <div class="input-group" style="margin-bottom:0px">
                                    <div class="input-group-addon">
                                        <i class="fa fa-mobile" style="padding:2px"></i>
                                    </div>
                                    <input style="display:none" type="text" class="form-control" data-inputmask='"mask": "(+62)999-9999-9999"' data-mask2 placeholder="contoh: (+62)857-9999-9999" name="hp_sales" id="hp_sales" value="{{ $supplier->hp_sales }}">

                                    <input style="display:block" type="text" class="form-control" placeholder="contoh: (+62)857-9999-9999" id="display_hp_sales" disabled="true" value="<?php echo str_replace('_', '', $supplier->hp_sales); ?>">
                                </div>
                            </div>
                            </td>
                        </tr>
                    <tr class="{{ $errors->has('deskripsi') ? ' has-error' : '' }}">
                        <th><label class="control-label">Deskripsi</label><label id="ast4" class="control-label"></label></th>
                        <th>:</th>
                        <td>
                            <div class="col-md-12">
                                <textarea type="text" class="form-control" name="deskripsi" id="deskripsi" disabled="true" style="height:120px" placeholder="Masukkan Deskripsi">{{ $supplier->deskripsi }}</textarea>
                                @if ($errors->has('deskripsi'))
                                    <span class="help-block">
                                        <strong>Kolom Deskripsi Wajib Diisi</strong>
                                    </span>
                                @endif
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>Umur Hutang</th>
                        <th>:</th>
                        <td>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="umur_hutang" id="umur_hutang" value="{{ $supplier->umur_hutang }}" disabled="true" placeholder="Masukkan Nilai Umur Hutang">
                            </div>
                            <label class="control-label">Hari</label>
                        </td>
                    </tr>
                    <tr>
                        <th><label class="control-label"></label></th>
                        <th></th>
                        <td>
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary flat" id="button_submit" style="display:none" onclick="document.getElementById('loading').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
                            </div>
                        </td>
                    </tr>
                </table>
                </form>
                <label id="astket" class="control-label"></label>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
                <div>
                    <a data-toggle="modal" data-target="#insert_rekening_supplier">
                        <button class='btn bg-purple flat'><span class='fa fa-plus'></span> Tambah Rekening Supplier</button>
                    </a>
                </div>
                <br>
                @if(Session::has('message2'))
              <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                <i class="fa fa-check"></i>
                {{ Session::get('message2') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              @endif
              @if($errors->has('nama_bank'))
                <div class="alert alert-danger flat">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <label>Kolom Nama Bank Wajib Diisi</label>
                </div>
              @endif
              @if($errors->has('nomor_rekening'))
                <div class="alert alert-danger flat">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <label>Kolom Nomor Rekening Wajib Diisi</label>
                </div>
              @endif
              @if($errors->has('atas_nama'))
                <div class="alert alert-danger flat">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <label>Kolom Atas Nama Wajib Diisi</label>
                </div>
              @endif
              @if($errors->has('mata_uang'))
                <div class="alert alert-danger flat">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <label>Pilihan Mata Uang Wajib Diisi</label>
                </div>
              @endif
                @if(Session::has('message_rekening'))
                  <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                    <i class="fa fa-check"></i>
                    {{ Session::get('message_rekening') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  @endif
                <table id="mydatatables" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th style="text-align:center">NO</th>
                        <th style="text-align:center">NAMA BANK</th>
                        <th style="text-align:center">NOMOR REKEING</th>
                        <th style="text-align:center">ATAS NAMA</th>
                        <th style="text-align:center">MATA UANG</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i=1; ?>
                    @foreach($rekening as $val)
                    <tr>
                        <td align="center">{{ $i++ }}</td>
                        <td>{{ $val->nama_bank }}</td>
                        <td>{{ $val->nomor_rekening }}</td>
                        <td>{{ $val->atas_nama }}</td>
                        <td align="center">@if($val->mata_uang === 'rupiah'){{ 'Rupiah (IDR)' }}@else{{ 'Dollar (USD)' }}@endif</td>
                        <td align="center">
                            <a data-toggle="modal" data-target="#edit_rekening_supplier">
                              <button class='btn btn-warning flat' onclick="edit_button('{{ $val->supplier_id }}', '{{ $val->nama_bank }}', '{{ $val->nomor_rekening }}', '{{ $val->atas_nama }}', '{{ $val->mata_uang }}', '{{ $val->id  }}')" data-toggle="tooltip" title="ubah" style="font-size: 15pt"><span class='fa fa-edit'></span></button>
                            </a>
                        </td>
                        <td align="center">
                            <button class='btn btn-danger flat' onclick="hapus_button('{{ $val->id }}', '{{ $supplier->id }}')" data-toggle="tooltip" title="hapus" style="font-size: 15pt"><span class='fa fa-trash'></span></button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                </table>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_3">
                <table id="mydatatables3" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th style="text-align:center">NO</th>
                        <th style="text-align:center">NAMA</th>
                        <th style="text-align:center">HARGA RETAIL</th>
                        <th style="text-align:center">HARGA PEMBELIAN TERAKHIR</th>
                        <th style="text-align:center">TANGGAL HARGA TERAKHIR</th>
                        <th style="text-align:center">STOK</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i=1; ?>
                    @foreach($produk_supplier as $val)
                    <tr>
                        <td align="center">{{ $i++ }}</td>
                        <td>{{ $val->nama }}</td>
                        <td>{{ rupiah($val->harga_retail) }}</td>
                        <td>{{ rupiah($val->harga_terakhir) }}</td>
                        <td>@if($val->tanggal_terakhir === "0000-00-00 00:00:00"){{ "-" }}@else{{ $val->tanggal_terakhir }}@endif</td>
                        <td>{{ $val->stok }}</td>
                    </tr>
                    @endforeach
                </tbody>
                </table>
                </div>
                <div class="tab-pane" id="tab_4">
                <table id="mydatatables4" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th style="text-align:center">NO</th>
                        <th style="text-align:center">NAMA</th>
                        <th style="text-align:center">HARGA RETAIL</th>
                        <th style="text-align:center">HARGA PEMBELIAN TERAKHIR</th>
                        <th style="text-align:center">TANGGAL HARGA TERAKHIR</th>
                        <th style="text-align:center">STOK</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i=1; ?>
                    @foreach($hadiah_supplier as $val)
                    <tr>
                        <td align="center">{{ $i++ }}</td>
                        <td>{{ $val->nama }}</td>
                        <td>0</td>
                        <td>{{ rupiah($val->harga_terakhir) }}</td>
                        <td>@if($val->tanggal_terakhir === "0000-00-00 00:00:00"){{ "-" }}@else{{ $val->tanggal_terakhir }}@endif</td>
                        <td>{{ $val->stok }}</td>
                    </tr>
                    @endforeach
                </tbody>
                </table>
                </div>
            <!-- /.tab-content -->
          </div>

            <div class="overlay" id="loading" style="display:none">
              <i class="fa fa-refresh fa-spin"></i>
            </div>
    </div>
</div>

<div class="modal fade" id="insert_rekening_supplier" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content box">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="riwayatheader">Tambah Rekening Supplier</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="post" action="{{ url('/tambah_rekening_supplier') }}" autocomplete="off">
        <input type="hidden" name="supplier_id" value="{{ $supplier->id }}">
        {{ csrf_field() }}
          <div class="box-body">
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Nama Bank*</label>
              <div class="col-sm-10">
                <input type="text" name="nama_bank" class="form-control" placeholder="Masukkan nama Bank">
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Nomor Rekening*</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="nomor_rekening" maxlength="16" onKeyPress="return numbersonly(this, event)" id="nomor_rekening" placeholder="Masukkan Nomor Rekening">
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Atas Nama*</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="atas_nama" id="atas_nama" placeholder="Masukkan Nama Pemilik Rekening" style="text-transform:capitalize">
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Mata Uang*</label>
              <div class="col-sm-10">
                <select name="mata_uang" class="form-control">
                    <option value="rupiah">Rupiah (IDR)</option>
                    <option value="dollar">Dollar (USD)</option>
                </select>
              </div>
            </div>
          </div><!-- /.box-body -->
          <p>Keterangan (*) : Wajib Diisi</p>
          <div class="box-footer">
            <button data-dismiss="modal" class="btn btn-default flat"><span class="glyphicon glyphicon-ban-circle"></span> Batal</button>
            <button type="submit" class="btn btn-primary pull-right flat" onclick="document.getElementById('loading2').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
          </div><!-- /.box-footer -->
        </form>
      </div>
       <div class="overlay" id="loading2" style="display:none">
              <i class="fa fa-refresh fa-spin"></i>
            </div>
    </div>
  </div>
</div>

<div class="modal fade" id="edit_rekening_supplier" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content box">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="riwayatheader">Ubah Rekening Supplier</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="post" action="{{ url('/ubah_rekening_supplier') }}">
        <input type="hidden" name="supplier_id" id="edit_kode">
        <input type="hidden" name="rekening_id" id="rekening_id">
        {{ csrf_field() }}
          <div class="box-body">
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Nama Bank*</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="nama_bank" id="edit_nama_bank" placeholder="Masukkan Nama Bank" style="text-transform:capitalize">
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Nomor Rekening*</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="nomor_rekening" id="edit_nomor_rekening" maxlength="16" onKeyPress="return numbersonly(this, event)" id="nomor_rekening" placeholder="Masukkan Nomor Rekening" style="text-transform:capitalize">
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Atas Nama*</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="atas_nama" id="edit_atas_nama" placeholder="Masukkan Nama Pemilik Rekening" style="text-transform:capitalize">
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Mata Uang*</label>
              <div class="col-sm-10">
                <select name="mata_uang" id="edit_mata_uang" class="form-control">
                    <option value="rupiah">Rupiah (IDR)</option>
                    <option value="dollar">Dollar (USD)</option>
                </select>
              </div>
            </div>
          </div><!-- /.box-body -->
          <p>Keterangan (*) : Wajib Diisi</p>
          <div class="box-footer">
            <button data-dismiss="modal" class="btn btn-default flat"><span class="glyphicon glyphicon-ban-circle"></span> Batal</button>
            <button type="submit" class="btn btn-primary pull-right flat" onclick="document.getElementById('loading3').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
          </div><!-- /.box-footer -->
        </form>
      </div>
      <div class="overlay" id="loading3" style="display:none">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
    </div>
  </div>
</div>

@stop

<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script>
  $(function () {
    $("#mydatatables").DataTable();
    $("#mydatatables2").DataTable();
    $("#mydatatables3").DataTable();
    $("#mydatatables4").DataTable();
  });
</script>
<!-- jQuery 2.2.0 -->
<script src="{{ asset('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('/adminlte/plugins/select2/select2.full.min.js') }}"></script>
<!-- InputMask -->
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();
    
    $("[data-mask]").inputmask({ mask: "(999[9])-9999999", greedy: false ,
        definitions: {
                '*': {
                validator: "[0-9-]",
                cardinality: 1,
                casing: "lower"
            }
        }
    });
    
    $("[data-mask2]").inputmask();
  });
</script>
<script type="text/javascript">
    function edit_button(kode,nama_bank,nomor_rekening,atas_nama,mata_uang, rekening_id)
    {
        document.getElementById("edit_kode").value = kode;
        document.getElementById('edit_nama_bank').value = nama_bank;
        document.getElementById('edit_nomor_rekening').value = nomor_rekening;
        document.getElementById('edit_atas_nama').value = atas_nama;
        if(mata_uang == 'rupiah') {
            document.getElementById('edit_mata_uang').selectedIndex = 0;
        }
        else{
            document.getElementById('edit_mata_uang').selectedIndex = 1;
        }
        document.getElementById('rekening_id').value = rekening_id;
    }
    function ubah(provinsi_id,kota_id)
    {
        document.getElementById("nama").disabled           = false;
        document.getElementById("alamat").disabled         = false;
        document.getElementById("nama_sales").disabled     = false;
        document.getElementById("deskripsi").disabled      = false;
        document.getElementById("umur_hutang").disabled    = false;

        document.getElementById("telp1").style.display           = 'block';
        document.getElementById("display_telp1").style.display   = 'none';
        document.getElementById("telp2").style.display           = 'block';
        document.getElementById("display_telp2").style.display   = 'none';
        document.getElementById("hp_sales").style.display        = 'block';
        document.getElementById("display_hp_sales").style.display= 'none';

        document.getElementById("ubah").style.display = 'none';
        document.getElementById("batal").style.display = 'block';
        document.getElementById("button_submit").style.display = 'block';

        document.getElementById("kota_saya").style.display = 'none';
        document.getElementById("edit_kota_saya").style.display = 'block';
        document.getElementById("edit_kota_saya2").style.display = 'block';

        document.getElementById("provinsi").selectedIndex = provinsi_id;
        $("#kota").html("");

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })

        var kode_provinsi = document.getElementById('provinsi').value;
        var path =  document.getElementById('base_path').value;
        var url = path+"get_kota2/"+kode_provinsi;

        $.ajax({
            type: "POST",
            url: url,
            dataType: 'json',
            success: function (data) {
                var my_obj = Object.keys(data).length;
                var my_index = 0;

                for (var i = 0; i < my_obj; i++) {
                    var z = document.createElement("option");
                    $.each(data[i], function(key,value){
                        if(key=='id'){
                         z.setAttribute("value", value);
                        }
                        else if(key == 'nama')
                        {
                            var t = document.createTextNode(value);
                            z.appendChild(t);
                        }
                        if(key == 'id' && value == kota_id)
                        { 
                            my_index = i;
                        }
                    });
                    document.getElementById("kota").appendChild(z);
                }
                document.getElementById("kota").selectedIndex = my_index;
            },
            error: function (data) {
                alert('ooo');
            }
        });

        document.getElementById("astket").innerHTML = "Keterangan (*) : Wajib Diisi";
        document.getElementById("ast1").innerHTML = "*";
        document.getElementById("ast2").innerHTML = "*";
        document.getElementById("ast3").innerHTML = "*";
        document.getElementById("ast4").innerHTML = "*";
    }
    function batal()
    {
        document.getElementById("nama").disabled           = true;
        document.getElementById("alamat").disabled         = true;
        document.getElementById("nama_sales").disabled     = true;
        document.getElementById("deskripsi").disabled      = true;
        document.getElementById("umur_hutang").disabled    = true;

        document.getElementById("telp1").style.display           = 'none';
        document.getElementById("display_telp1").style.display   = 'block';
        document.getElementById("telp2").style.display           = 'none';
        document.getElementById("display_telp2").style.display   = 'block';
        document.getElementById("hp_sales").style.display        = 'none';
        document.getElementById("display_hp_sales").style.display= 'block';

        document.getElementById("ubah").style.display = 'block';
        document.getElementById("batal").style.display = 'none';
        document.getElementById("button_submit").style.display = 'none';

        document.getElementById("kota_saya").style.display = 'block';
        document.getElementById("edit_kota_saya").style.display = 'none';
        document.getElementById("edit_kota_saya2").style.display = 'none';

        document.getElementById("nama").value = document.getElementById("ori_nama").value;
        document.getElementById("alamat").value = document.getElementById("ori_alamat").value;
        document.getElementById("telp1").value = document.getElementById("ori_telp1").value;
        document.getElementById("telp2").value = document.getElementById("ori_telp2").value;
        document.getElementById("nama_sales").value = document.getElementById("ori_nama_sales").value;
        document.getElementById("hp_sales").value = document.getElementById("ori_hp_sales").value;
        document.getElementById("deskripsi").value = document.getElementById("ori_deskripsi").value;
        document.getElementById("umur_hutang").value = document.getElementById("ori_umur_hutang").value;

        document.getElementById("astket").innerHTML = "";
        document.getElementById("ast1").innerHTML = "";
        document.getElementById("ast2").innerHTML = "";
        document.getElementById("ast3").innerHTML = "";
        document.getElementById("ast4").innerHTML = "";
    }

    function hapus_button(id,supplier_id)
    {
        var cek = confirm("Apakah anda yakin akan menghapus rekening supplier?");

        if(cek)
        {
            document.getElementById("loading").style.display = 'block';
            document.getElementById("rekening_id_supplier").value = id;
            document.getElementById("supplier_id_hapus").value = supplier_id;
            document.getElementById("form_hapus_rekening").submit();
        }
    }

    $(document).ready(function(){
          $('#provinsi').change(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            var kode_provinsi = document.getElementById('provinsi').value;
            if(kode_provinsi == "")
                kode_provinsi = "KOSONG"
            var path =  document.getElementById('base_path').value;
            var url = path+"get_kota/"+kode_provinsi;
            
            $.ajax({
                type: "POST",
                url: url,
                data: $("#form_provinsi").serialize(),
                success: function (data) {
                    $("#kota").html(data);
                },
                error: function (data) {
                    alert('ooo');
                }
            });
          });

          // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // save the latest tab; use cookies if you like 'em better:
                localStorage.setItem('lastTab_detail_supplier', $(this).attr('href'));
            });

            // go to the latest tab, if it exists:
            var lastTab_detail_supplier = localStorage.getItem('lastTab_detail_supplier');
            if (lastTab_detail_supplier) {
                $('[href="' + lastTab_detail_supplier + '"]').tab('show');
            }
            else
            {
                lastTab_detail_supplier = "#tab_1";
                $('[href="' + lastTab_detail_supplier + '"]').tab('show');
            }
      });
</script>