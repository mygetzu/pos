<?php
    function rupiah($nominal)
    {
        $rupiah =  number_format($nominal,0, ",",".");
        $rupiah = "Rp "  . $rupiah;
        return $rupiah;
    }
?>

<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<table class="table table-striped table-bordered" id="ajax_produk_tabless">
    <thead>
        <th style="text-align:center">NO</th>
        <th style="text-align:center">JENIS</th>
        <th style="text-align:center">NAMA</th>
        <th style="text-align:center">HARGA RETAIL</th>
        <th style="text-align:center"></th>
    </thead>
    <tbody>
        <?php $i=1; ?>
        @foreach($produk as $val)
        <tr>
            <td align="center">{{ $i++ }}</td>
            <td>@if(substr($val->kode,0,3) == 'PKT')<p>PAKET</p>@else<p>PRODUK</p>@endif</td>
            <td>{{ $val->nama }}</td>
            <td>{{ rupiah($val->harga_retail) }}</td>
            <td align="center">
                <?php 
                    if(!empty($val->produk_galeri_first->file_gambar))
                        $file_gambar = $val->produk_galeri_first->file_gambar;
                    else
                        $file_gambar = $gambar_produk_default->value; 
                ?>
                <button class="btn bg-maroon flat" type="button" onclick="pilih_produk('{{ $val->id }}', '{{ $val->harga_retail }}', '{{ $val->nama }}', '{{ $file_gambar }}')" data-dismiss="modal"><span class="fa fa-check"></span> Pilih</button>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
  $(function () {
    $("#ajax_produk_tabless").DataTable();
  });
</script>