@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<input type="hidden" name="base_path" id="base_path" value="{{ asset('/') }}">
<input type="hidden" name="" id="old_kode_provinsi" value="{{ old('provinsi') }}">
<input type="hidden" name="" id="old_kota" value="{{ old('kota') }}">
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <h4>Tambah Supplier
                <div class="pull-right">
                    <a href="{{ url('/supplier') }}">
                        <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                    </a>
                </div>
                </h4>
            </div>
            <div class="box-body">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <form class="form-horizontal" method="POST" action="{{ url('/do_tambah_supplier') }}" autocomplete="off">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                        <label class="control-label col-md-2">Nama*</label>
                        <div class="col-md-8">
                            <input type="text" value="{{ old('nama') }}" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama" style="text-transform:capitalize">
                            @if ($errors->has('nama'))
                                <span class="help-block">
                                    <strong>Kolom Nama Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('alamat') ? ' has-error' : '' }}">
                        <label class="control-label col-md-2">Alamat*</label>
                        <div class="col-md-8">
                            <textarea type="text" class="form-control" name="alamat" id="alamat" placeholder="Masukkan Alamat" style="text-transform:capitalize">{{ old('alamat') }}</textarea>
                            @if ($errors->has('alamat'))
                                <span class="help-block">
                                    <strong>Kolom Alamat Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('kota') ? ' has-error' : '' }}">
                        <label class="control-label col-md-2">Kabupaten/Kota*</label>
                        <div class="col-md-4">
                            <select class="form-control" id="provinsi" name="provinsi">
                                <option value="">-- Pilih Provinsi --</option>
                                @foreach($provinsi as $val)
                                <option value="{{ $val->kode }}">{{ $val->nama }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('provinsi'))
                                <span class="help-block">
                                    <strong>Pilihan Provinsi Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                        <div class="col-md-4">
                            <select class="form-control" id="kota" name="kota">
                                <option value="">-- Pilih Kabupaten/Kota --</option>
                            </select>
                            @if ($errors->has('kota'))
                                <span class="help-block">
                                    <strong>Pilihan Kabupaten/Kota Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2">Telepon 1</label>
                        <div class="col-md-4">
                            <div class="input-group" style="margin-bottom:0px">
                                <div class="input-group-addon">
                                    <i class="fa fa-phone"></i>
                                </div>
                                <input type="text" value="{{ old('telp1') }}" class="form-control" data-mask placeholder="contoh: (031)-9999999" name="telp1" id="telp1">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2">Telepon 2</label>
                        <div class="col-md-4">
                            <div class="input-group" style="margin-bottom:0px">
                                <div class="input-group-addon">
                                    <i class="fa fa-phone"></i>
                                </div>
                                <input type="text" value="{{ old('telp2') }}" class="form-control" data-mask placeholder="contoh: (031)-9999999" name="telp2" id="telp2">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2">Nama Sales</label>
                        <div class="col-md-8">
                            <input type="text" value="{{ old('nama_sales') }}" class="form-control" name="nama_sales" id="nama_sales" placeholder="Masukkan Nama Sales" style="text-transform:capitalize">
                            @if ($errors->has('nama_sales'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('nama_sales') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2">HP Sales</label>
                        <div class="col-md-4">
                            <div class="input-group" style="margin-bottom:0px">
                                <div class="input-group-addon">
                                    <i class="fa fa-mobile" style="padding:2px"></i>
                                </div>
                                <input type="text" value="{{ old('hp_sales') }}" class="form-control" data-inputmask='"mask": "(+62)999-9999-9999"' data-mask2 placeholder="contoh: (+62)857-9999-9999" name="hp_sales" id="hp_sales">
                            </div>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('deskripsi') ? ' has-error' : '' }}">
                        <label class="control-label col-md-2">Deskripsi</label>
                        <div class="col-md-8">
                            <textarea type="text" class="form-control" name="deskripsi" id="deskripsi" placeholder="Masukkan Deskripsi">{{ old('deskripsi') }}</textarea>
                            @if ($errors->has('deskripsi'))
                                <span class="help-block">
                                    <strong>Kolom Deskripsi Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2">Umur Hutang</label>
                        <div class="col-md-2">
                            <input type="text" value="{{ old('umur_hutang') }}" class="form-control" name="umur_hutang" id="umur_hutang" onKeyPress="return numbersonly(this, event)" placeholder="Umur Hutang" maxlength="4" style="text-align:right">
                        </div>
                        <label class="control-label col-md-1">Hari</label>
                    </div>
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-2" id="button_submit">
                            <button type="submit" class="btn btn-primary flat" onclick="document.getElementById('loading').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
                        </div>
                    </div>
                </form>
                <p>Keterangan (*) : Wajib Diisi</p>
            </div>
            <div class="overlay" id="loading" style="display:none">
              <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>
@stop
<!-- jQuery 2.2.0 -->
<script src="{{ asset('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('/adminlte/plugins/select2/select2.full.min.js') }}"></script>
<!-- InputMask -->
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();
    
    $("[data-mask]").inputmask({ mask: "(999[9])-9999999", greedy: false ,
        definitions: {
                '*': {
                validator: "[0-9-]",
                cardinality: 1,
                casing: "lower"
            }
        }
    });

    $("[data-mask2]").inputmask();
  });

  $(document).ready(function(){
        var kode_prov = document.getElementById('old_kode_provinsi').value;
           var old_kota = document.getElementById('old_kota').value;

           if(kode_prov != "")
           {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                })

                var path =  document.getElementById('base_path').value;
                var url = path+"get_id_provinsi/"+kode_prov;

                $.ajax({
                    type: "POST",
                    url: url,
                    success: function (data) {
                        document.getElementById("provinsi").selectedIndex = data;
                    },
                    error: function (data) {
                        alert('ooo');
                    }
                });


                $("#kota").html("");

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                })

                var kode_provinsi = document.getElementById('old_kode_provinsi').value;
                var path =  document.getElementById('base_path').value;
                var url = path+"get_kota2/"+kode_provinsi;

                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: 'json',
                    success: function (data) {
                        var my_obj = Object.keys(data).length;
                        var my_index = 0;
                        if(old_kota == "")
                        {
                            var z = document.createElement("option");
                            z.setAttribute("value", '');
                            var t = document.createTextNode("-- Pilih Kabupaten/Kota --");
                            z.appendChild(t);
                            document.getElementById("kota").appendChild(z);
                        }
                        for (var i = 0; i < my_obj; i++) {
                            var z = document.createElement("option");
                            $.each(data[i], function(key,value){
                                if(key=='id'){
                                 z.setAttribute("value", value);
                                }
                                else if(key == 'nama')
                                {
                                    var t = document.createTextNode(value);
                                    z.appendChild(t);
                                }
                                if(key == 'id' && value == old_kota)
                                { 
                                    my_index = i;
                                }
                            });
                            document.getElementById("kota").appendChild(z);
                        }
                        document.getElementById("kota").selectedIndex = my_index;
                    },
                    error: function (data) {
                        alert('ooo');
                    }
                });
           }

          $('#provinsi').change(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            var kode_provinsi = document.getElementById('provinsi').value;
            if(kode_provinsi == "")
                kode_provinsi = "KOSONG"
            var path =  document.getElementById('base_path').value;
            var url = path+"get_kota/"+kode_provinsi;
            
            $.ajax({
                type: "POST",
                url: url,
                data: $("#form_provinsi").serialize(),
                success: function (data) {
                    $("#kota").html(data);
                },
                error: function (data) {
                    alert('ooo');
                }
            });
          });
      });
</script>
