<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBxGbZh_n2hLPuHS1owKGKhSDThUbC4544"></script>
<script>
    var map;
    function initialize() 
    {
        var la = document.getElementById("ori_latitude").value;
        var lo = document.getElementById("ori_longitude").value

        if(la == "") la = '-7.2754665';
        if(lo == "") lo = '112.6416432';

        var myLatlng = new google.maps.LatLng(la,lo);
        var myOptions = {
            zoom: 16,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        map = new google.maps.Map(document.getElementById("peta"), myOptions); 
        var marker = new google.maps.Marker({
            draggable: false,
            position: myLatlng, 
            map: map,
            title: "Pilih Lokasi Anda"
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>
<style>
    #peta {
      height: 300px;
      width: 600px;
    }
</style>
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<input type="hidden" name="base_path" id="base_path" value="{{ asset('/') }}">
<input type="hidden" name="ori_nama" id="ori_nama" value="{{ $gudang->nama }}">
<input type="hidden" name="ori_alamat" id="ori_alamat" value="{{ $gudang->alamat }}">
<input type="hidden" name="ori_telp" id="ori_telp" value="{{ $gudang->telp }}">
<input type="hidden" name="ori_supervisor" id="ori_supervisor" value="{{ $gudang->supervisor }}">
<input type="hidden" name="ori_hp_supervisor" id="ori_hp_supervisor" value="{{ $gudang->hp_supervisor }}">
<input type="hidden" name="ori_longitude" id="ori_longitude" value="{{ $gudang->longitude }}">
<input type="hidden" name="ori_latitude" id="ori_latitude" value="{{ $gudang->latitude }}">
<div class="row">
    <div class="col-md-12">
        <div class="nav-tabs-custom box tab-primary flat" style="border-top: 0">
            <ul class="nav nav-tabs">
              <li><a href="#tab_1" data-toggle="tab">Detail Gudang</a></li>
              <li><a href="#tab_2" data-toggle="tab">Data Produk</a></li>
              <li><a href="#tab_3" data-toggle="tab">Data Hadiah</a></li>
              <li class="pull-right">
                <a href="{{ url('/gudang') }}" style="padding:0px">
                    <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                </a>
              </li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane" id="tab_1">
                <div id="ubah" style="display:block">
                    <div class="">
                      <button class="btn btn-warning flat" onclick="ubah('{{ $my_kota->provinsi_id }}','{{ $my_kota->kota_id }}')"><span class='fa fa-edit'></span> Ubah</button>
                    </div>
                </div>
                <div id="batal" style="display:none">
                    <div class="">
                      <button class="btn btn-default flat" onclick="batal()"><span class='fa fa-ban'></span> Batal</button>
                    </div>
                </div>
                <br>
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <form action="{{ url('/ubah_gudang') }}" method="post" autocomplete="off">
                <input type="hidden" name="id" value="{{ $gudang->id }}">
                {{ csrf_field() }}
                <table class="table table-striped">
                    <tr class="{{ $errors->has('nama') ? ' has-error' : '' }}">
                        <th><label class="control-label">Nama</label><label id="ast1" class="control-label"></label></th>
                        <th>:</th>
                        <td>
                            <input type="text" class="form-control" name="nama" id="nama" value="{{ $gudang->nama }}" disabled="true" placeholder="Masukkan Nama Lokasi" style="text-transform:capitalize">
                            @if ($errors->has('nama'))
                                <span class="help-block">
                                    <strong>Kolom Nama Wajib Diisi</strong>
                                </span>
                            @endif
                        </td>
                    </tr>
                    <tr class="{{ $errors->has('alamat') ? ' has-error' : '' }}">
                        <th><label class="control-label">Alamat</label><label id="ast2" class="control-label"></label></th>
                        <th>:</th>
                        <td>
                            <textarea type="text" class="form-control" name="alamat" id="alamat" disabled="true" placeholder="Masukkan Alamat" style="text-transform:capitalize">{{ $gudang->alamat }}</textarea>
                            @if ($errors->has('alamat'))
                                <span class="help-block">
                                    <strong>Kolom Alamat Wajib Diisi</strong>
                                </span>
                            @endif
                        </td>
                    </tr>
                    <tr class="{{ $errors->has('kota') ? ' has-error' : '' }}">
                        <th><label class="control-label">Kabupaten/Kota</label><label id="ast6" class="control-label"></th>
                        <th>:</th>
                        <td>
                        <div class="col-md-12" id="kota_saya" style="margin-left:-15">
                            <input type="text" class="form-control" value="{{ ucwords(strtolower($my_kota->nama)) }}" disabled="true" placeholder="Masukkan Kota">
                            @if ($errors->has('provinsi') AND $errors->has('kota'))
                                    <span class="help-block">
                                        <strong>Pilihan Provinsi Serta Kabupaten/Kota Wajib Diisi</strong>
                                    </span>
                            @elseif ($errors->has('provinsi'))
                                <span class="help-block">
                                    <strong>Pilihan Provinsi Wajib Diisi</strong>
                                </span>
                            @elseif ($errors->has('kota'))
                                <span class="help-block">
                                    <strong>Pilihan Kabupaten/Kota Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                        <div class="col-md-5" id="edit_kota_saya" style="display:none;margin-left:-15">
                            <select class="form-control" id="provinsi" name="provinsi">
                                <option value="">-- Pilih Provinsi --</option>
                                @foreach($provinsi as $val)
                                <option value="{{ $val->kode }}">{{ $val->nama }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('provinsi'))
                                <span class="help-block">
                                    <strong>Pilihan Provinsi Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                        <div class="col-md-5" id="edit_kota_saya2" style="display:none">
                            <select class="form-control" id="kota" name="kota">
                                
                            </select>
                            @if ($errors->has('kota'))
                                <span class="help-block">
                                    <strong>Pilihan Kabupaten/Kota Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                        </td>
                    </tr>
                    <tr class="{{ $errors->has('telp') ? ' has-error' : '' }}">
                        <th><label class="control-label">Telepon</label><label id="ast3" class="control-label"></label></th>
                        <th>:</th>
                        <td>
                            <div class="input-group col-md-4" style="margin-bottom:0px">
                                    <div class="input-group-addon">
                                        <i class="fa fa-phone"></i>
                                    </div>
                                    <input style="display:none" type="text" class="form-control" data-mask placeholder="contoh: (031)-9999999" name="telp" id="telp" value="{{ $gudang->telp }}">

                                    <input style="display:block" type="text" class="form-control" placeholder="contoh: (031)-9999999" id="display_telp" disabled="true" value="<?php echo str_replace('_', '', $gudang->telp); ?>">
                                </div>
                            @if ($errors->has('telp'))
                                <span class="help-block">
                                    <strong>Kolom Telepon Wajib Diisi</strong>
                                </span>
                            @endif
                        </td>
                    </tr>
                    <tr class="{{ $errors->has('supervisor') ? ' has-error' : '' }}">
                        <th><label class="control-label">Supervisor</label><label id="ast4" class="control-label"></label></th>
                        <th>:</th>
                        <td>
                            <input type="text" class="form-control" name="supervisor" id="supervisor" value="{{ $gudang->supervisor }}" disabled="true" placeholder="Masukkan Nama Supervisor" style="text-transform:capitalize">
                            @if ($errors->has('supervisor'))
                                <span class="help-block">
                                    <strong>Kolom Supervisor Wajib Diisi</strong>
                                </span>
                            @endif
                        </td>
                    </tr>
                    <tr class="{{ $errors->has('hp_supervisor') ? ' has-error' : '' }}">
                        <th><label class="control-label">HP Supervisor</label><label id="ast5" class="control-label"></label></th>
                        <th>:</th>
                        <td>
                            <div class="input-group col-md-4" style="margin-bottom:0px">
                                    <div class="input-group-addon">
                                        <i class="fa fa-mobile" style="padding:2px"></i>
                                    </div>
                                    <input style="display:none" type="text" class="form-control" data-inputmask='"mask": "(+62)999-9999-9999"' data-mask2 placeholder="contoh: (+62)857-9999-9999" name="hp_supervisor" id="hp_supervisor" value="{{ $gudang->hp_supervisor }}">

                                    <input style="display:block" type="text" class="form-control" placeholder="contoh: (+62)857-9999-9999" id="display_hp_supervisor" disabled="true" value="<?php echo str_replace('_', '', $gudang->hp_supervisor); ?>">
                                </div>
                            @if ($errors->has('hp_supervisor'))
                                <span class="help-block">
                                    <strong>Kolom HP Supervisor Wajib Diisi</strong>
                                </span>
                            @endif
                        </td>
                    </tr>
                    <tr class="{{ $errors->has('latitude') ? ' has-error' : '' }}">
                        <th><label class="control-label">Latitude</label></th>
                        <th>:</th>
                        <td>
                            <input type="hidden" class="form-control" name="latitude" id="latitude" value="{{ $gudang->latitude }}" disabled="true" placeholder="Masukkan Koordinat Latitude">
                            <input type="text" class="form-control" name="latitude2" id="latitude2" value="{{ $gudang->latitude }}" disabled="true" placeholder="Masukkan Koordinat Latitude">
                            @if ($errors->has('latitude'))
                                <span class="help-block">
                                    <strong>Kolom Latitude Wajib Diisi</strong>
                                </span>
                            @endif
                        </td>
                    </tr>
                    <tr class="{{ $errors->has('longitude') ? ' has-error' : '' }}">
                        <th><label class="control-label">Longitude</label></th>
                        <th>:</th>
                        <td>
                            <input type="hidden" class="form-control" name="longitude" id="longitude" value="{{ $gudang->longitude }}" disabled="true" placeholder="Masukkan Koordinat Longitude">
                            <input type="text" class="form-control" name="longitude2" id="longitude2" value="{{ $gudang->longitude }}" disabled="true" placeholder="Masukkan Koordinat Longitude">
                            @if ($errors->has('longitude'))
                                <span class="help-block">
                                    <strong>Kolom Longitude Wajib Diisi</strong>
                                </span>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th><label class="control-label">Lokasi</label></th>
                        <th>:</th>
                        <td>
                            <div id="peta"></div>
                        </td>
                    </tr>
                    <tr>
                        <th><label class="control-label"></label></th>
                        <th></th>
                        <td>
                            <button type="submit" class="btn btn-primary flat" id="button_submit" style="display:none" onclick="document.getElementById('loading').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
                        </td>
                    </tr>
                </table>
                </form>
                <label id="astket" class="control-label"></label>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
                <table class="table table-striped table-bordered" id="mydatatables" >
                    <thead>
                        <tr>
                            <th style="text-align:center">NO</th>
                            <th style="text-align:center">KODE PRODUK</th>
                            <th style="text-align:center">NAMA PRODUK</th>
                            <th style="text-align:center">KATEGORI</th>
                            <th style="text-align:center">HARGA</th>
                            <th style="text-align:center">STOK</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i=1; ?>
                        @foreach($produk_gudang as $val)
                        <tr>
                            <td align="center">{{ $i++ }}</td>
                            <td>{{ $val->kode }}</td>
                            <td>{{ $val->nama }}</td>
                            <td>{{ $val->kategori_produk_nama }}</td>
                            <td>{{ rupiah($val->harga_retail) }}</td>
                            <td>{{ $val->stok }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_3">
                <table class="table table-striped table-bordered" id="mydatatables2" >
                    <thead>
                        <tr>
                            <th style="text-align:center">NO</th>
                            <th style="text-align:center">NAMA HADIAH</th>
                            <th style="text-align:center">STOK</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i=1; ?>
                        @foreach($hadiah_gudang as $val)
                        <tr>
                            <td align="center">{{ $i++ }}</td>
                            <td>{{ $val->nama }}</td>
                            <td>{{ $val->stok }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
              </div>
              <!-- /.tab-pane -->
            </div>
            <div class="overlay" id="loading" style="display:none">
              <i class="fa fa-refresh fa-spin"></i>
            </div>
            <!-- /.tab-content -->
          </div>
    </div>
</div>
@stop
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script>
  $(function () {
    $("#mydatatables").DataTable();
    $("#mydatatables2").DataTable();
  });
</script>
<!-- jQuery 2.2.0 -->
<script src="{{ asset('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('/adminlte/plugins/select2/select2.full.min.js') }}"></script>
<!-- InputMask -->
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();
    
    $("[data-mask]").inputmask({ mask: "(999[9])-9999999", greedy: false ,
        definitions: {
                '*': {
                validator: "[0-9-]",
                cardinality: 1,
                casing: "lower"
            }
        }
    });
    
    $("[data-mask2]").inputmask();
  });
    function ubah(provinsi_id,kota_id)
    {
        document.getElementById("nama").disabled           = false;
        document.getElementById("alamat").disabled         = false;
        document.getElementById("supervisor").disabled     = false;
        document.getElementById("longitude").disabled      = false;
        document.getElementById("latitude").disabled       = false;

        document.getElementById("telp").style.display           = 'block';
        document.getElementById("display_telp").style.display   = 'none';
        document.getElementById("hp_supervisor").style.display             = 'block';
        document.getElementById("display_hp_supervisor").style.display     = 'none';

        document.getElementById("ubah").style.display = 'none';
        document.getElementById("batal").style.display = 'block';
        document.getElementById("button_submit").style.display = 'block';

        document.getElementById("kota_saya").style.display = 'none';
        document.getElementById("edit_kota_saya").style.display = 'block';
        document.getElementById("edit_kota_saya2").style.display = 'block';

        document.getElementById("astket").innerHTML = "Keterangan (*) : Wajib Diisi";
        document.getElementById("ast1").innerHTML = "*";
        document.getElementById("ast2").innerHTML = "*";
        document.getElementById("ast3").innerHTML = "*";
        document.getElementById("ast4").innerHTML = "*";
        document.getElementById("ast5").innerHTML = "*";
        document.getElementById("ast6").innerHTML = "*";

        document.getElementById("provinsi").selectedIndex = provinsi_id;
        $("#kota").html("");

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })

        var provinsi_kode = document.getElementById('provinsi').value;
        var path =  document.getElementById('base_path').value;
        var url = path+"get_kota2/"+provinsi_kode;

        $.ajax({
            type: "POST",
            url: url,
            dataType: 'json',
            success: function (data) {
                var my_obj = Object.keys(data).length;
                var my_index = 0;

                for (var i = 0; i < my_obj; i++) {
                    var z = document.createElement("option");
                    $.each(data[i], function(key,value){
                        if(key=='id'){
                         z.setAttribute("value", value);
                        }
                        else if(key == 'nama')
                        {
                            var t = document.createTextNode(value);
                            z.appendChild(t);
                        }
                        if(key == 'id' && value == kota_id)
                        { 
                            my_index = i;
                        }
                    });
                    document.getElementById("kota").appendChild(z);
                }
                document.getElementById("kota").selectedIndex = my_index;
            },
            error: function (data) {
                alert('ooo');
            }
        });


        var la = document.getElementById("ori_latitude").value;
        var lo = document.getElementById("ori_longitude").value

        if(la == "") la = '-7.2754665';
        if(lo == "") lo = '112.6416432';

        var myLatlng = new google.maps.LatLng(la,lo);
        var myOptions = {
            zoom: 16,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        map = new google.maps.Map(document.getElementById("peta"), myOptions); 
        var marker = new google.maps.Marker({
            draggable: true,
            position: myLatlng, 
            map: map,
            title: "Pilih Lokasi Anda"
        });

        google.maps.event.addListener(marker, 'drag', function (event) {
            document.getElementById("latitude").value = event.latLng.lat();
            document.getElementById("longitude").value = event.latLng.lng();
            document.getElementById("latitude2").value = event.latLng.lat();
            document.getElementById("longitude2").value = event.latLng.lng();
        });

        google.maps.event.addListener(marker, 'click', function (event) {
            document.getElementById("latitude").value = event.latLng.lat();
            document.getElementById("longitude").value = event.latLng.lng();
            document.getElementById("latitude2").value = event.latLng.lat();
            document.getElementById("longitude2").value = event.latLng.lng();
        });
    }
    function batal()
    {
        document.getElementById("nama").disabled           = true;
        document.getElementById("alamat").disabled         = true;
        document.getElementById("supervisor").disabled     = true;
        document.getElementById("longitude").disabled      = true;
        document.getElementById("latitude").disabled       = true;

        document.getElementById("telp").style.display           = 'none';
        document.getElementById("display_telp").style.display   = 'block';
        document.getElementById("hp_supervisor").style.display             = 'none';
        document.getElementById("display_hp_supervisor").style.display     = 'block';

        document.getElementById("ubah").style.display = 'block';
        document.getElementById("batal").style.display = 'none';
        document.getElementById("button_submit").style.display = 'none';

        document.getElementById("kota_saya").style.display = 'block';
        document.getElementById("edit_kota_saya").style.display = 'none';
        document.getElementById("edit_kota_saya2").style.display = 'none';

        document.getElementById("nama").value           = document.getElementById("ori_nama").value;
        document.getElementById("alamat").value         = document.getElementById("ori_alamat").value;
        document.getElementById("telp").value           = document.getElementById("ori_telp").value;
        document.getElementById("supervisor").value     = document.getElementById("ori_supervisor").value;
        document.getElementById("hp_supervisor").value  = document.getElementById("ori_hp_supervisor").value;
        document.getElementById("longitude").value      = document.getElementById("ori_longitude").value;
        document.getElementById("latitude").value       = document.getElementById("ori_latitude").value;
        document.getElementById("longitude2").value      = document.getElementById("ori_longitude").value;
        document.getElementById("latitude2").value       = document.getElementById("ori_latitude").value;

        document.getElementById("astket").innerHTML = "";
        document.getElementById("ast1").innerHTML = "";
        document.getElementById("ast2").innerHTML = "";
        document.getElementById("ast3").innerHTML = "";
        document.getElementById("ast4").innerHTML = "";
        document.getElementById("ast5").innerHTML = "";
        document.getElementById("ast6").innerHTML = "";

        var la = document.getElementById("ori_latitude").value;
        var lo = document.getElementById("ori_longitude").value

        if(la == "") la = '-7.2754665';
        if(lo == "") lo = '112.6416432';

        var myLatlng = new google.maps.LatLng(la,lo);
        var myOptions = {
            zoom: 16,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        map = new google.maps.Map(document.getElementById("peta"), myOptions); 
        var marker = new google.maps.Marker({
            draggable: false,
            position: myLatlng, 
            map: map,
            title: "Pilih Lokasi Anda"
        });

        google.maps.event.addListener(marker, 'drag', function (event) {
            document.getElementById("latitude").value = event.latLng.lat();
            document.getElementById("longitude").value = event.latLng.lng();
            document.getElementById("latitude2").value = event.latLng.lat();
            document.getElementById("longitude2").value = event.latLng.lng();
        });

        google.maps.event.addListener(marker, 'click', function (event) {
            document.getElementById("latitude").value = event.latLng.lat();
            document.getElementById("longitude").value = event.latLng.lng();
            document.getElementById("latitude2").value = event.latLng.lat();
            document.getElementById("longitude2").value = event.latLng.lng();
        });
    }
    $(document).ready(function(){
            $('#provinsi').change(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            var provinsi_kode = document.getElementById('provinsi').value;
            if(provinsi_kode == "")
                provinsi_kode = "KOSONG"
            var path =  document.getElementById('base_path').value;
            var url = path+"get_kota/"+provinsi_kode;
            
            $.ajax({
                type: "POST",
                url: url,
                data: $("#form_provinsi").serialize(),
                success: function (data) {
                    $("#kota").html(data);
                },
                error: function (data) {
                    alert('ooo');
                }
            });
          });

           // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // save the latest tab; use cookies if you like 'em better:
                localStorage.setItem('lastTab_detail_gudang', $(this).attr('href'));
            });

            // go to the latest tab, if it exists:
            var lastTab_detail_gudang = localStorage.getItem('lastTab_detail_gudang');
            if (lastTab_detail_gudang) {
                $('[href="' + lastTab_detail_gudang + '"]').tab('show');
            }
            else
            {
                lastTab_detail_gudang = "#tab_1";
                $('[href="' + lastTab_detail_gudang + '"]').tab('show');
            }
      });
</script>