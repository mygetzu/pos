<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<input type="hidden" name="base_path" id="base_path" value="{{ asset('/') }}">
<input type="hidden" name="ori_nama" id="ori_nama" value="{{ $hadiah->nama }}">
<input type="hidden" name="ori_satuan" id="ori_satuan" value="{{ $hadiah->satuan }}">
<input type="hidden" name="ori_berat" id="ori_berat" value="{{ $hadiah->berat }}">
<div class="row">
    <div class="col-md-12">
        <div class="nav-tabs-custom box tab-primary flat" style="border-top: 0">
            <ul class="nav nav-tabs">
              <li><a href="#tab_1" data-toggle="tab">Detail Hadiah</a></li>
              <li><a href="#tab_2" data-toggle="tab">Hadiah Supplier</a></li>
              <li><a href="#tab_3" data-toggle="tab">Gudang</a></li>
              <li><a href="#tab_4" data-toggle="tab">Galeri</a></li>
              <li class="pull-right">
                <a href="{{ url('/hadiah') }}" style="padding:0px">
                      <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                  </a>
              </li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane" id="tab_1">
                <div id="ubah" style="display:block">
                  <div class="">
                    <button class="btn btn-warning flat" onclick="ubah()"><span class='fa fa-edit'></span> Ubah</button>
                  </div>
                </div>
                <div id="batal" style="display:none">
                  <div class="">
                    <button class="btn btn-default flat" onclick="batal()"><span class='fa fa-ban'></span> Batal</button>
                  </div>
                </div>
                <br>
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                <i class="fa fa-check"></i>
                {{ Session::get('message') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              @endif
              <form class="form-horizontal" action="{{ url('/ubah_hadiah') }}" method="post" autocomplete="off">
                <input type="hidden" name="id" value="{{ $hadiah->id }}">
                {{ csrf_field() }}
                <table class="table table-striped">
                    <tr class="{{ $errors->has('nama') ? ' has-error' : '' }}">
                        <th><label class="control-label">Nama</label><label id="ast1" class="control-label"></label></th>
                        <th>:</th>
                        <td>
                            <div class="col-md-12">
                                <input type="text" class="form-control" name="nama" id="nama" value="{{ $hadiah->nama }}" disabled="true" placeholder="Masukkan Nama" style="text-transform:capitalize">
                                @if ($errors->has('nama'))
                                    <span class="help-block">
                                        <strong>Kolom Nama Wajib Diisi</strong>
                                    </span>
                                @endif
                            </div>
                        </td>
                    </tr>
                    <tr class="{{ $errors->has('satuan') ? ' has-error' : '' }}">
                        <th><label class="control-label">Satuan</label><label id="ast2" class="control-label"></label></th>
                        <th>:</th>
                        <td>
                            <div class="col-md-12">
                                <input type="text" class="form-control" name="satuan" id="satuan" value="{{ $hadiah->satuan }}" disabled="true" placeholder="Masukkan Satuan" style="text-transform:capitalize">
                                @if ($errors->has('satuan'))
                                    <span class="help-block">
                                        <strong>Kolom Satuan Wajib Diisi</strong>
                                    </span>
                                @endif
                            </div>
                        </td>
                    </tr>
                    <tr class="{{ $errors->has('berat') ? ' has-error' : '' }}">
                        <th><label class="control-label">Berat</label><label id="ast2" class="control-label"></label></th>
                        <th>:</th>
                        <td>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="berat" id="berat" value="{{ $hadiah->berat }}" disabled="true" placeholder="Masukkan Berat" style="text-transform:capitalize">
                                @if ($errors->has('berat'))
                                    <span class="help-block">
                                        <strong>Kolom Berat Wajib Diisi</strong>
                                    </span>
                                @endif
                            </div>
                        </td>
                    </tr>
                    <tr class="{{ $errors->has('deskripsi') ? ' has-error' : '' }}">
                        <th><label class="control-label">Deskripsi</label><label id="ast4" class="control-label"></label></th>
                        <th>:</th>
                        <td>
                            <div class="col-md-12">
                                <div id="display_deskripsi2">
                                    <textarea type="text" class="form-control" disabled="true" id="deskripsi2" style="height:120px" placeholder="Masukkan Deskripsi">{{ $hadiah->deskripsi }}</textarea>
                                <br>
                                </div>
                                <div id="display_deskripsi" style="display:none">
                                    <textarea type="text" class="form-control" name="deskripsi" id="deskripsi" style="height:120px" placeholder="Masukkan Deskripsi">{{ $hadiah->deskripsi }}</textarea>
                                </div>
                                @if ($errors->has('deskripsi'))
                                    <span class="help-block">
                                        <strong>Kolom Deskripsi Wajib Diisi</strong>
                                    </span>
                                @endif
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>Stok</th>
                        <th>:</th>
                        <td>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="umur_hutang" id="umur_hutang" value="{{ $hadiah->stok }}" disabled="true">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>Stok dipesan</th>
                        <th>:</th>
                        <td>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="umur_hutang" id="umur_hutang" value="{{ $hadiah->stok_dipesan }}" disabled="true">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th><label class="control-label"></label></th>
                        <th></th>
                        <td>
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary flat" id="button_submit" style="display:none" onclick="document.getElementById('loading').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
                            </div>
                        </td>
                    </tr>
                </table>
                </form>
                <label id="astket" class="control-label"></label>
              </div>
              <div class="tab-pane" id="tab_2">
                <div>
                    <a data-toggle="modal" data-target="#tambah_hadiah_supplier">
                        <button class='btn bg-purple flat'><span class='fa fa-plus'></span> Tambah Hadiah Supplier</button>
                    </a>
                </div>
                <br>
                @if(Session::has('message_hadiah_supplier'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message_hadiah_supplier') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <table id="mydatatables4" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th style="text-align:center">NO</th>
                            <th style="text-align:center">NAMA</th>
                            <th style="text-align:center">ALAMAT</th>
                            <th style="text-align:center">KOTA</th>
                            <th style="text-align:center">NAMA SALES</th>
                            <th style="text-align:center">DESKRIPSI</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i=1; ?>
                        @foreach($hadiah_supplier as $val)
                        <tr>
                            <td align="center">{{ $i++ }}</td>
                            <td>{{ $val->nama }}</td>
                            <td>{{ $val->alamat }}</td>
                            <td>{{ ucwords(strtolower($val->kota_nama)) }}</td>
                            <td>{{ $val->nama_sales }}</td>
                            <td>{{ $val->deskripsi }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
              </div>
              <div class="tab-pane" id="tab_3">
              <table id="mydatatables2" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th style="text-align:center">NO</th>
                            <th style="text-align:center">NAMA</th>
                            <th style="text-align:center">ALAMAT</th>
                            <th style="text-align:center">KOTA</th>
                            <th style="text-align:center">SUPERVISOR</th>
                            <th style="text-align:center">STOK</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i=1; ?>
                        @foreach($hadiah_gudang as $val)
                        <tr>
                            <td align="center">{{ $i++ }}</td>
                            <td>{{ $val->nama }}</td>
                            <td>{{ $val->alamat }}</td>
                            <td>{{ ucwords(strtolower($val->kota_nama)) }}</td>
                            <td>{{ $val->supervisor }}</td>
                            <td align="center">{{ $val->stok }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
              </div>
              <div class="tab-pane" id="tab_4">
                @if(Session::has('message4'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                <i class="fa fa-check"></i>
                {{ Session::get('message4') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              @endif
                <table class="table table-striped">
                <tr>
                        <th width="10%">Gambar</th>
                        <th width="5%">:</th>
                        <td width="20%">
                            <div align="center">
                            <img src="{{ URL::asset('img/produk/'.$hadiah->file_gambar) }}" style="height:130px;width:auto;padding: 15px 15px 0px 15px" class="img-responsive" alt="">
                            <a data-toggle="modal" data-target="#edit_gambar">
                              <button class='btn btn-warning flat' onclick="edit_gambar('{{ $hadiah->id }}')"><span class='fa fa-edit'></span> Ubah Gambar</button></a>
                            </div>
                        </td>
                        <td></td>
                    </tr>
                    </table>
              </div>
            <!-- /.tab-content -->
          </div>

            <div class="overlay" id="loading" style="display:none">
              <i class="fa fa-refresh fa-spin"></i>
            </div>
    </div>
</div>

<div class="modal fade" id="edit_gambar" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content box">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="riwayatheader">Ubah Gambar</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="post" action="{{ url('/ubah_gambar_hadiah/'.$hadiah->id) }}" enctype="multipart/form-data">
        <input type="hidden" name="hadiah_id" id="edit_hadiah_id">
        {{ csrf_field() }}
          <div class="box-body">
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">Gambar</label>
              <div class="col-sm-10">
                <input type="file" class="form-control" name="edit_image" id="edit_image">
              </div>
            </div>
          </div><!-- /.box-body -->
          <div class="box-footer">
            <button data-dismiss="modal" class="btn btn-default flat"><span class="glyphicon glyphicon-ban-circle"></span> Batal</button>
            <button type="submit" class="btn btn-primary pull-right flat" onclick="document.getElementById('loading2').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
          </div><!-- /.box-footer -->
        </form>
      </div>
      <div class="overlay" id="loading2" style="display:none">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
    </div>
  </div>
</div>

<div class="modal fade" id="tambah_hadiah_supplier" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content box">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="riwayatheader">Tambah Hadiah Supplier</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="post" action="{{ url('/tambah_hadiah_supplier') }}" autocomplete="off">
        <input type="hidden" name="hadiah_id" value="{{ $hadiah->id }}">
        {{ csrf_field() }}
          <div class="box-body">
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Supplier</label>
              <div class="col-sm-10">
                <select class="form-control" name="supplier_id">
                    @foreach($supplier as $val)
                    <option value="{{ $val->id }}">{{ $val->nama }}</option>
                    @endforeach
                </select>
              </div>
            </div>
          </div><!-- /.box-body -->
          <div class="box-footer">
            <button data-dismiss="modal" class="btn btn-default flat"><span class="glyphicon glyphicon-ban-circle"></span> batal</button>
            <button type="submit" class="btn btn-primary pull-right flat" onclick="document.getElementById('loading3').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
          </div><!-- /.box-footer -->
        </form>
      </div>
      <div class="overlay" id="loading3" style="display:none">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
    </div>
  </div>
</div>

@stop

<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script>
  $(function () {
    $("#mydatatables").DataTable();
    $("#mydatatables2").DataTable();
    $("#mydatatables3").DataTable();
    $("#mydatatables4").DataTable();
  });

  function ubah()
    {
        document.getElementById("nama").disabled           = false;
        document.getElementById("satuan").disabled         = false;
        document.getElementById("berat").disabled           = false;

        document.getElementById("display_deskripsi2").style.display      = 'none';
        document.getElementById("display_deskripsi").style.display      = 'block';

        document.getElementById("ubah").style.display = 'none';
        document.getElementById("batal").style.display = 'block';
        document.getElementById("button_submit").style.display = 'block';

        document.getElementById("astket").innerHTML = "Keterangan (*) : Wajib Diisi";
        document.getElementById("ast1").innerHTML = "*";
        document.getElementById("ast2").innerHTML = "*";
        document.getElementById("ast3").innerHTML = "*";
        document.getElementById("ast4").innerHTML = "*";
    }
    function batal()
    {
        document.getElementById("nama").disabled           = true;
        document.getElementById("satuan").disabled         = true;
        document.getElementById("berat").disabled           = true;

        document.getElementById("display_deskripsi2").style.display      = 'block';
        document.getElementById("display_deskripsi").style.display      = 'none';

        document.getElementById("ubah").style.display = 'block';
        document.getElementById("batal").style.display = 'none';
        document.getElementById("button_submit").style.display = 'none';

        document.getElementById("nama").value = document.getElementById("ori_nama").value;
        document.getElementById("satuan").value = document.getElementById("ori_satuan").value;
        document.getElementById("berat").value = document.getElementById("ori_berat").value;

        document.getElementById("astket").innerHTML = "";
        document.getElementById("ast1").innerHTML = "";
        document.getElementById("ast2").innerHTML = "";
        document.getElementById("ast3").innerHTML = "";
        document.getElementById("ast4").innerHTML = "";
    }
    function edit_gambar(id)
    {
        document.getElementById("edit_hadiah_id").value          = id;
    }

    $(document).ready(function(){

          // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // save the latest tab; use cookies if you like 'em better:
                localStorage.setItem('lastTab_detail_hadiah', $(this).attr('href'));
            });

            // go to the latest tab, if it exists:
            var lastTab_detail_hadiah = localStorage.getItem('lastTab_detail_hadiah');
            if (lastTab_detail_hadiah) {
                $('[href="' + lastTab_detail_hadiah + '"]').tab('show');
            }
            else
            {
                lastTab_detail_hadiah = "#tab_1";
                $('[href="' + lastTab_detail_hadiah + '"]').tab('show');
            }
      });
</script>
<!-- jQuery 2.2.0 -->
<script src="{{ asset('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('/adminlte/plugins/select2/select2.full.min.js') }}"></script>
<!-- InputMask -->
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
<!-- TINYMCE -->
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
  tinymce.init({ selector:'#deskripsi',
  height: 400,
  plugins: [
    'advlist autolink lists link image charmap print preview anchor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste code'
  ],
  toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image' });

  tinymce.init({ selector:'#deskripsi2',
  height: 400,
  plugins: [
    'advlist autolink lists link image charmap print preview anchor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste code'
  ],
  toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
  readonly : 1 });
</script>