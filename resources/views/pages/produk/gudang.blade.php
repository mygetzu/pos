<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <a href="{{ url('/tambah_gudang') }}">
                  <button class='btn bg-purple flat'><span class='fa fa-plus'></span> Tambah Gudang</button>
                </a>
            </div>
            <div class="box-body">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <table id="mydatatables" class="table table-striped table-bordered">
                  <thead>
                    <tr>
                        <th style="text-align:center">NO</th>
                        <th style="text-align:center">NAMA</th>
                        <th style="text-align:center">ALAMAT</th>
                        <th style="text-align:center">KOTA</th>
                        <th style="text-align:center"></th>
                        <th></th>
                        <td></td>
                      </tr>
                  </thead>
                  <tbody>
                    <?php $i = 1; ?>
                    @foreach($gudang as $val)
                    <tr>
                      <td align="center">{{ $i++ }}</td>
                      <td>{{ $val->nama }}</td>
                      <td>{{ $val->alamat }}</td>
                      <td>{{ ucwords(strtolower($val->kota_nama)) }}</td>
                      <td align="center">
                        <label class="switch">
                            <input type="checkbox" onchange='window.location.href="{{ url('/ubah_status_gudang/'.$val->id) }}"' <?php if($val->is_aktif == 1) echo "checked"; else echo ""; ?>>
                            <div class="slider" data-toggle="tooltip" title="status aktif"></div>
                        </label>
                      </td>
                      <td align="center">
                        @if($val->terpakai === '0')
                        <form action="{{ url('/hapus_gudang') }}" method="post" id="form_hapus{{ $val->id }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="gudang_id" value="{{ $val->id }}">
                        <button class="btn btn-danger flat" type="button" onclick="btn_hapus('{{ $val->id }}')" data-toggle="tooltip" title="hapus" style="font-size: 15pt"><span class="fa fa-trash"></span></button>
                        </form>
                        @else
                        <button class="btn btn-danger flat" disabled="true" data-toggle="tooltip" title="detail" style="font-size: 15pt"><span class="fa fa-trash"></span></button>
                        @endif
                      </td>
                      <td align="center">
                        <a href="{{ url('/detail_gudang/'.$val->id) }}">
                          <button class='btn btn-info flat' data-toggle="tooltip" title="detail" style="font-size: 15pt"><span class='fa fa-info-circle'></span></button>
                        </a>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
              </table>
            </div>
            <div class="overlay" id="loading" style="display:none">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
        </div>
    </div>
</div>
<div class="modal fade" id="ubah_status" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content box">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="riwayatheader">Ubah Status</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="post" action="{{ url('/ubah_status_lokasi') }}">
        <input type="hidden" id="us_gudang_id" name="us_gudang_id">
        {{ csrf_field() }}
          <div class="form-group">
              <label class="col-sm-3 control-label">Nama Gudang</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="us_gudang_nama" disabled="true">
              </div>
          </div><!-- /.box-body -->
          <div class="form-group">
              <label class="col-sm-3 control-label">Pilih Status</label>
              <div class="col-sm-9">
                <select class="form-control" name="us_status_lokasi" id="us_status_lokasi">
                    <option value="1">Aktif</option>
                    <option value="0">Tidak Aktif</option>
                </select>
              </div>
          </div><!-- /.box-body -->
          <div class="box-footer">
            <button data-dismiss="modal" class="btn btn-default flat"><span class="glyphicon glyphicon-ban-circle"></span> Batal</button>
            <button type="submit" class="btn btn-primary pull-right flat" onclick="document.getElementById('loading3').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
          </div><!-- /.box-footer -->
        </form>
      </div>
      <div class="overlay" id="loading3" style="display:none">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
    </div>
  </div>
</div>

@stop
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script>
  $(function () {
    $("#mydatatables").DataTable();
  });
</script>

<script type="text/javascript">
    function edit_button(kode,nama,alamat,telp,supervisor,hp_supervisor,longitude,latitude)
    {
        document.getElementById("edit_kode").value          = kode;
        document.getElementById("edit_nama").value          = nama;
        document.getElementById("edit_alamat").value        = alamat;
        document.getElementById("edit_telp").value          = telp;
        document.getElementById("edit_supervisor").value    = supervisor;
        document.getElementById("edit_hp_supervisor").value = hp_supervisor;
        document.getElementById("edit_longitude").value     = longitude;
        document.getElementById("edit_latitude").value      = latitude;
    }
    function ubah_status(kode,nama,status)
    {
      document.getElementById("us_gudang_id").value   = kode;
      document.getElementById("us_gudang_nama").value   = nama;
      if(status == '1'){
        document.getElementById("us_status_lokasi").selectedIndex = '0';
      }else{
        document.getElementById("us_status_lokasi").selectedIndex = '1';
      }
    }

    function btn_hapus(kode)
    {
      var cek = confirm("apakah Anda Yakin?");

      if(cek)
      {
        document.getElementById('loading').style.display = 'block';
        document.getElementById('form_hapus'.concat(kode)).submit();
      }
    }
</script>