<style type="text/css">
    .table.table-borderless td, .table.table-borderless th {
        border: 0 !important;
    }
</style>
@extends('layouts.master_teknisi')
@section('content')
    @include('includes.base_function')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary flat">
                <div class="box-body">
                    <div class="col-md-12">
                        <h3>Data Perbaikan Produk</h3>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-xs-6">
                            <div class="small-box bg-aqua flat">
                                <div class="inner">
                                    <p style="font-size: 16px">Produk yang akan diservis</p>
                                    <h3>{{ $data['akan_servis'] }}</h3>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-sign-in" style="padding-top:15px"></i>
                                </div>
                                <a href="{{ url('/teknisi/produk_belum_servis') }}" class="small-box-footer">Info lebih
                                    lanjut
                                    <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xs-6">
                            <div class="small-box bg-green flat">
                                <div class="inner">
                                    <p style="font-size: 16px">Produk yang sedang diservis</p>
                                    <h3>{{ $data['sedang_servis'] }}</h3>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-cogs" style="padding-top:15px"></i>
                                </div>
                                <a href="{{ url('/teknisi/produk_sedang_servis') }}" class="small-box-footer">Info lebih
                                    lanjut
                                    <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xs-6">
                            <div class="small-box bg-yellow flat">
                                <div class="inner">
                                    <p style="font-size: 16px">Produk yang telah diservis</p>
                                    <h3>{{ $data['telah_servis'] }}</h3>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-sign-out" style="padding-top:15px"></i>
                                </div>
                                <a href="{{ url('/teknisi/produk_selesai_servis') }}" class="small-box-footer">Info
                                    lebih lanjut
                                    <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xs-6">
                            <div class="small-box bg-red flat">
                                <div class="inner">
                                    <p style="font-size: 16px">Laporan servis</p>
                                    <h3><br/></h3>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-file-text-o" style="padding-top:15px"></i>
                                </div>
                                <a href="{{ url('/teknisi/laporan_servis') }}" class="small-box-footer">Info lebih
                                    lanjut
                                    <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin: 15px">
                        <h3>Data Perbaikan Sedang Diproses</h3>
                        <table id="mydatatables" class="table table-striped table-bordered">
                            <thead>
                            <th>No.</th>
                            <th>No. Nota</th>
                            <th>Nama Pelanggan</th>
                            <th>Model Perangkat</th>
                            <th>Keluhan Pelanggan</th>
                            <th>Tanggal Diterima</th>
                            <th>Status</th>
                            </thead>
                            <tbody>
                            <?php $a = 0; ?>
                            @foreach ($data['servis'] as $i => $r_servis)
                                <tr>
                                    <td>{{ ++$a }}</td>
                                    <td>{{ $data[$i]['no_nota'] }}</td>
                                    <td>{{ $data[$i]['pelanggan'] }}</td>
                                    <td>{{ $data[$i]['model_produk'] }}</td>
                                    <td>{{ implode(' ', array_slice(explode(' ', $data[$i]['keluhan_pelanggan']), 0, 8)) }}</td>
                                    <td>{{ date('d-M-Y', strtotime($data[$i]['tgl_diterima'])) }}</td>
                                    <td>
                                        @if($data[$i]['status'] == 'Belum Ada Teknisi' || $data[$i]['status'] == 'Perbaikan Dibatalkan')
                                            <button class="btn btn-flat btn-block btn-danger">
                                                {{ $data[$i]['status']  }}
                                            </button>
                                        @elseif($data[$i]['status'] == 'Belum Diperiksa' || $data[$i]['status'] == 'Sedang Diperiksa' || $data[$i]['status'] == 'Belum Disetujui')
                                            <button class="btn btn-flat btn-block btn-warning">
                                                {{ $data[$i]['status']  }}
                                            </button>
                                        @elseif($data[$i]['status'] == 'Belum Diberi Biaya' || $data[$i]['status'] == 'Telah Disetujui')
                                            <button class="btn btn-flat btn-block btn-info">
                                                {{ $data[$i]['status']  }}
                                            </button>
                                        @elseif($data[$i]['status'] == 'Perbaikan Selesai')
                                            <button class="btn btn-flat btn-block btn-success">
                                                {{ $data[$i]['status']  }}
                                            </button>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script>
    $(function () {
        $("#mydatatables").DataTable({
            'ordering' : false
        });
    });
</script>