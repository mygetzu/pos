@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-body">
                <br>
                <div class="col-md-12" id="content">
                    <table id="mydatatables" class="table table-striped table-bordered">
                        <thead>
                            <th style="text-align:center">NO</th>
                            <th style="text-align:center">KODE</th>
                            <th style="text-align:center">NAMA</th>
                            <th style="text-align:center">DEBIT/KREDIT</th>
                            <th></th>
                        </thead>
                        <tbody>
                            <?php $i=1; ?>
                            @foreach($group_account as $val)
                            <tr>
                                <td align="center">{{ $i++ }}</td>
                                <td>{{ $val->kode }}</td>
                                <td>{{ $val->nama }}</td>
                                <td align="center">@if($val->debit_or_kredit === "D")
                                    Debit
                                    @else
                                    Kredit
                                    @endif</td>
                                <td></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="overlay" id="loading" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>
@stop

<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script>
  $(function () {
    $("#mydatatables").DataTable();
  });
</script>