@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<style type="text/css">
    #uploadFile{
        width:610px;
        height: 200px;
        border:4px dotted #444;
        }
    .camera{
        display:block;
        cursor:pointer;
        width:610px;
        height: 180px;
    }
    .camera:before{
      content:"\2b";
      font-family:"Glyphicons Halflings";
      line-height:2;
      font-size: 25pt;
      margin-top:50px;
      margin-left: 280px;
      display:inline-block;
      text-align: center;
      color: #00a65a !important;
    }
    #uploadFile input[type="file"]{
        display:block;
        width:610px;
        height: 180px;
        margin-top: -10px;
        display:none;
    }

    .submitI{
        padding:5px 10px 5px 10px;
        background:#0000ff;
        color:white;
        width:120px;
        margin:auto;
        cursor:pointer;
    }
    #website{
        padding:5px 10px 5px 10px;
        background:#ff0000;
        color:white;
        width:120px;
        margin:auto;
        text-decoration:none;
        cursor:pointer;
    }
    .imageReader{
        height:190px;
        width:600px;
        cursor: move;
        background-size: cover;
        background-repeat: no-repeat;
        background-position: 50% 50%;
    }

    .badge
    {
        position: absolute;
        z-index:99;
        color:black;
        font-size: 20pt;
        background-color: #00a65a;
        color: white;
    }

    #sortable { 
    list-style: none; 
    text-align: left; 
}

#sortable li span {
    background-repeat: no-repeat;
    background-position: center;
    display: inline-block;
    float: left;
    cursor: move;
}
#sortable li img {
    /*height: 165px;
    border: 5px solid #cccccc;
    display: inline-block;
    float: left;*/
}
.nomor {
    height: 215px;

    text-align: center;
}
.table.table-borderless td, .table.table-borderless th {
        border: 0 !important;
    }
</style>
<form action="{{ url('/hapus_data_produk_baru') }}" method="post" id="form_hapus_data_produk_baru">
    {{ csrf_field() }}
    <input type="hidden" name="produk_id_baru" id="produk_id_baru">
</form>
<form action="{{ url('/hapus_data_produk_akan_datang') }}" method="post" id="form_hapus_data_produk_akan_datang">
    {{ csrf_field() }}
    <input type="hidden" name="produk_id_akan_datang" id="produk_id_akan_datang">
</form>
<div class="row">
    <div class="col-md-12">
        <div class="nav-tabs-custom box tab-primary flat" style="border-top: 0">
            <ul class="nav nav-tabs" id="myTab">
              <li><a href="#tab_1" data-toggle="tab">Slider Promo</a></li>
              <li><a href="#tab_2" data-toggle="tab">Produk Pilihan</a></li>
              <li><a href="#tab_3" data-toggle="tab">Data Produk Baru</a></li>
              <li><a href="#tab_4" data-toggle="tab">Data Produk Akan Datang</a></li>
              <li><a href="#tab_5" data-toggle="tab">Gambar Produk Default</a></li>
              <li><a href="#tab_6" data-toggle="tab">Cara Belanja</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane" id="tab_1">
                    @if(Session::has('message'))
                        <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                            <i class="fa fa-check"></i>
                            {{ Session::get('message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-1">
                                <table width="100%" class="table table-borderless">
                                    <tbody>
                                        <?php $ve=count($halaman_promo); ?>
                                        @for($i=1; $i<=$ve; $i++)
                                        <tr class="nomor">
                                            <td align="right"><label class="label label-success" style="font-size: 20pt;">{{ $i }}</label></td>
                                        </tr>
                                        @endfor
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-8">
                                <ul id="sortable" class="row">
                                <?php $ve=1; ?>
                                @foreach($halaman_promo as $val)
                                    <li id="{{ $ve }}" class="col-md-12" class="sortable"><button type="button" class="btn btn-danger" onclick="hapus_halaman_promo('{{ $val->id }}')" style="z-index: 1; position: absolute; left: 592px">
                                                &times;
                                                </button>
                                        <form action="{{ url('/ubah_banner_promo/'.$val->id) }}" id="ubah_banner_promo{{ $val->id }}" method="post" enctype="multipart/form-data" >
                                            {{ csrf_field() }}
                                            <div id="uploadFile">
                                                
                                                <span>
                                                <label for="upload{{ $val->id }}" ><img id="imageReader{{ $val->id }}" src="{{ URL::asset('img/banner/'.$val->file_gambar) }}" class="imageReader"></label>
                                                <input type="file" name="banner" required id="upload{{ $val->id }}" onchange="this.form.submit()">
                                                </span>
                                            </div>  
                                            <div id="type{{ $val->id }}"></div>
                                        </form>
                                    </li>
                                    <?php $ve++ ?>
                                @endforeach
                                </ul>
                                <div class="row">
                                    <div class="col-md-4 static" style="margin-left: 40px">
                                        <form action="{{ url('/tambah_banner_promo') }}" method="post" enctype="multipart/form-data" >
                                            {{ csrf_field() }}
                                            <div id="uploadFile">
                                                <label for="upload" title="Unggah Slider Promo" class="camera">
                                                </label>
                                                <label for="upload" ><img  id="imageReader"></label>
                                                <input type="file" name="banner"  required id="upload">
                                            </div>  
                                            <div id="type"></div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab_2">
                    <a data-toggle="modal" data-target="#tambah_produk_tampil_di_beranda">
                        <button class='btn bg-purple flat'><span class='fa fa-plus-circle'></span> Tambah Produk Pilihan</button>
                    </a>
                    <br><br>
                    @if(Session::has('message5'))
                        <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                          <i class="fa fa-check"></i>
                          {{ Session::get('message5') }}
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                    @endif
                    <table id="mydatatables" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th style="text-align:center">NO</th>
                                <th style="text-align:center">NAMA</th>
                                <th style="text-align:center">TANGGAL INPUT</th>
                                <th style="text-align:center"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=1; ?>
                            @foreach($produk_tampil_di_beranda as $val)
                            <tr>
                                <td align="center">{{ $i++ }}</td>
                                <td>{{ $val->nama }}</td>
                                <td>{{ $val->tanggal_input }}</td>
                                <td align="center">
                                    <button class="btn btn-danger flat" onclick="hapus_produk_tampil_di_beranda('{{ $val->produk_id }}')" data-toggle="tooltip" title="hapus" style="font-size: 15pt"><span class="fa fa-trash"></span></button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="tab_3">
                    <a data-toggle="modal" data-target="#tambah_data_produk_baru">
                        <button class='btn bg-purple flat'><span class='fa fa-plus-circle'></span> Tambah Data Produk Baru</button>
                    </a>
                    <br><br>
                    @if(Session::has('message2'))
                        <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                          <i class="fa fa-check"></i>
                          {{ Session::get('message2') }}
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                    @endif
                    <table id="mydatatables2" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th style="text-align:center">NO</th>
                                <th style="text-align:center">NAMA</th>
                                <th style="text-align:center">TANGGAL INPUT</th>
                                <th style="text-align:center"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=1; ?>
                            @foreach($produk_baru as $val)
                            <tr>
                                <td align="center">{{ $i++ }}</td>
                                <td>{{ $val->nama }}</td>
                                <td>{{ $val->tanggal_input }}</td>
                                <td align="center">
                                    <button class="btn btn-danger flat" onclick="hapus_data_produk_baru('{{ $val->produk_id }}')" data-toggle="tooltip" title="hapus" style="font-size: 15pt"><span class="fa fa-trash"></span></button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="tab_4">
                    <a data-toggle="modal" data-target="#tambah_data_produk_akan_datang">
                        <button class='btn bg-purple flat'><span class='fa fa-plus-circle'></span> Tambah Data Produk Akan Datang</button>
                    </a>
                    <br><br>
                    @if(Session::has('message3'))
                        <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                          <i class="fa fa-check"></i>
                          {{ Session::get('message3') }}
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                    @endif
                    <table id="mydatatables3" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th style="text-align:center">NO</th>
                                <th style="text-align:center">NAMA</th>
                                <th style="text-align:center">TANGGAL INPUT</th>
                                <th style="text-align:center"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=1; ?>
                            @foreach($produk_akan_datang as $val)
                            <tr>
                                <td align="center">{{ $i++ }}</td>
                                <td>{{ $val->nama }}</td>
                                <td>{{ $val->tanggal_input }}</td>
                                <td align="center">
                                    <button class="btn btn-danger flat" onclick="hapus_data_produk_akan_datang('{{ $val->produk_id }}')" data-toggle="tooltip" title="hapus" style="font-size: 15pt"><span class="fa fa-trash"></span></button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="tab_5">
                    @if(Session::has('message4'))
                        <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                          <i class="fa fa-check"></i>
                          {{ Session::get('message4') }}
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                    @endif
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Gambar Produk Kosong</label>
                            <div class="col-sm-10">
                                <img src="{{ URL::asset('img/'.$gambar_produk_default->value) }}" style="height:130px;width:auto;" class="img-responsive" alt="">
                            </div>
                        </div>
                        <form method="post" action="{{ url('/upload_gambar_produk_default') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-8">
                                    <input type="file" name="gambar" id="gambar" class="form-control">
                                </div>
                                <div class="col-md-4">
                                    <button class="btn btn-primary flat" onclick="document.getElementById('loading').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div> 
                <div class="tab-pane" id="tab_6">
                    @if(Session::has('message6'))
                        <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                          <i class="fa fa-check"></i>
                          {{ Session::get('message6') }}
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                    @endif
                    <form class="form-horizontal" method="post" action="{{ url('/tambah_cara_belanja') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="control-label col-md-2">Cara Belanja</label>
                            <div class="col-md-10">
                                <textarea class="form-control" name="cara_belanja" id="cara_belanja" placeholder="Masukkan Deskripsi">{{ $cara_belanja }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-2">
                                <button class="btn btn-primary flat"><span class="fa fa-floppy-o"></span> Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>               
            </div>
            <div class="overlay" id="loading" style="display:none">
              <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="tambah_data_produk_baru" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content box">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="riwayatheader">Tambah Data Produk Baru</h4>
            </div>
            <div class="modal-body">
                <div class="box-body">
                    <table id="mydatatables4" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th style="text-align:center">NO</th>
                                <th style="text-align:center">NAMA</th>
                                <th style="text-align:center"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=1; ?>
                            @foreach($produk as $val)
                            <tr>
                                <form class="form-horizontal" method="post" action="{{ url('/tambah_data_produk_baru') }}" autocomplete="off">
                                 {{ csrf_field() }}
                                <input type="hidden" name="produk_id" value="{{ $val->id }}">
                                    <td align="center">{{ $i++ }}</td>
                                    <td>{{ $val->nama }}</td>
                                    <td align="center">
                                        <button class="btn btn-primary flat" onclick="document.getElementById('loading2').style.display = 'block'"><span class="fa fa-check"></span> Pilih</button>
                                    </td>
                                </form>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="overlay" id="loading2" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="tambah_data_produk_akan_datang" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content box">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="riwayatheader">Tambah Data Produk Akan Datang</h4>
            </div>
            <div class="modal-body">
                <div class="box-body">
                    <table id="mydatatables5" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th style="text-align:center">NO</th>
                                <th style="text-align:center">NAMA</th>
                                <th style="text-align:center"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=1; ?>
                            @foreach($produk as $val)
                            <tr>
                                <form class="form-horizontal" method="post" action="{{ url('/tambah_data_produk_akan_datang') }}" autocomplete="off">
                                 {{ csrf_field() }}
                                <input type="hidden" name="produk_id" value="{{ $val->id }}">
                                    <td align="center">{{ $i++ }}</td>
                                    <td>{{ $val->nama }}</td>
                                    <td align="center">
                                        <button class="btn btn-primary flat" onclick="document.getElementById('loading3').style.display = 'block'"><span class="fa fa-check"></span> Pilih</button>
                                    </td>
                                </form>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="overlay" id="loading3" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="tambah_produk_tampil_di_beranda" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content box">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah Produk Pilihan</h4>
            </div>
            <div class="modal-body">
                <table id="mydatatables6" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th style="text-align:center">NO</th>
                            <th style="text-align:center">NAMA</th>
                            <th style="text-align:center"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i=1; ?>
                        @foreach($produk as $val)
                        <tr>
                            <form class="form-horizontal" method="post" action="{{ url('/tambah_produk_tampil_di_beranda') }}" autocomplete="off">
                             {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{ $val->id }}">
                                <td align="center">{{ $i++ }}</td>
                                <td>{{ $val->nama }}</td>
                                <td align="center">
                                    <button class="btn btn-primary flat" onclick="document.getElementById('loading4').style.display = 'block'"><span class="fa fa-check"></span> Pilih</button>
                                </td>
                            </form>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="overlay" id="loading4" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<form id="form_hapus_halaman_promo" action="{{ url('/hapus_halaman_promo') }}" method="post">
    {{ csrf_field() }}
    <input type="hidden" name="id" id="id_hapus_halaman_promo">
</form>

<form id="form_hapus_produk_tampil_di_beranda" action="{{ url('/hapus_produk_tampil_di_beranda') }}" method="post">
    {{ csrf_field() }}
    <input type="hidden" name="id" id="id_hapus_produk_tampil_di_beranda">
</form>
@endsection
<script src="{{ asset('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<link href="{{ URL::to('/switch/dist/css/bootstrap3/bootstrap-switch.css') }}" rel="stylesheet">
<script src="{{ URL::to('/switch/docs/js/highlight.js') }}"></script>
<script src="{{ URL::to('/switch/dist/js/bootstrap-switch.js') }}"></script>
<script src="{{ URL::to('/switch/docs/js/main.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function(){
        // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            // save the latest tab; use cookies if you like 'em better:
            localStorage.setItem('lastTab_pengaturan', $(this).attr('href'));
        });

        // go to the latest tab, if it exists:
        var lastTab_pengaturan = localStorage.getItem('lastTab_pengaturan');
        if (lastTab_pengaturan) {
            $('[href="' + lastTab_pengaturan + '"]').tab('show');
        }
        else
        {
            lastTab_pengaturan = "#tab_1";
            $('[href="' + lastTab_pengaturan + '"]').tab('show');
        }

        $("#mydatatables").DataTable();
        $("#mydatatables2").DataTable();
        $("#mydatatables3").DataTable();
        $("#mydatatables4").DataTable();
        $("#mydatatables5").DataTable();
        $("#mydatatables6").DataTable();

        $('[data-toggle="tooltip"]').tooltip(); 
    });

    function hapus_data_produk_baru(id)
    { 
        var cek = confirm('Apakah Anda Yakin?');

        if(cek)
        {
            document.getElementById('loading').style.display = 'block';
            document.getElementById('produk_id_baru').value = id;
            document.getElementById('form_hapus_data_produk_baru').submit();
        }
    }

    function hapus_data_produk_akan_datang(id)
    { 
        var cek = confirm('Apakah Anda Yakin?');

        if(cek)
        {
            document.getElementById('loading').style.display = 'block';
            document.getElementById('produk_id_akan_datang').value = id;
            document.getElementById('form_hapus_data_produk_akan_datang').submit();
        }
    }

    $(function() {
        $('#sortable').sortable({
            axis: 'x,y',
            opacity: 0.7,
            handle: 'span',
            update: function(event, ui) {
                var list_sortable = $(this).sortable('toArray').toString();
                var list = list_sortable.split(",");
                
                // for (var i = 0; i < list.length-1; i++) {
                //     var j = i+1;
                //     document.getElementById("order".concat(j)).innerHTML = list[i];
                //     document.getElementById("order".concat(j)).setAttribute("id", "order"+list[i]);
                    
                // }



                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                })
                // change order in the database using Ajax
                url = "{{ url('/set_order_halaman_promo') }}";
                
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: {list_order:list_sortable},
                        success: function(data) {
                    }
                });

            }
        }); // fin sortable
    });

    function hapus_halaman_promo(id)
    {
        var cek = confirm('Apakah anda yakin untuk menghapus slider promo?');

        if(cek)
        {
            document.getElementById('id_hapus_halaman_promo').value = id;
            document.getElementById('form_hapus_halaman_promo').submit();
            document.getElementById('loading').style.display = 'block';
        }
    }

    $(document).ready(function(){
        $("#upload").change(function(){
        
            var file        = this.files[0];
            var imageFile   = file.type;

            var match=["image/jpeg","image/png","image/jpg"];  

       
            if(!((imageFile==match[0]) || (imageFile==match[1]) || (imageFile==match[2]))){
                alert("Gambar yang diijinkan hanya Jpeg/ Jpg /Png /Gif");
                return false;
            }else{
                this.form.submit();
                //alert("required");
                // var reader=new FileReader();
                // reader.onload=imageIsLoaded('wkwk');
                // reader.readAsDataURL(this.files[0]);
                // document.getElementById("tambah_banner_promo").submit();
                // this.form.submit();
            }
        });


        function imageIsLoaded(e){
            // alert(par);
            $(".imageReader").attr('src', e.target.result);
            $(".camera").hide();
            $("#imgRead").show();
        }

        function ubah_banner_promo(id){
            alert('wkwk')
            // this.form.submit();
        }

        
    }); 

    function hapus_produk_tampil_di_beranda(id)
    {
        var cek = confirm('Apakah anda yakin untuk menghapus produk?');

        if(cek)
        {
            document.getElementById('id_hapus_produk_tampil_di_beranda').value = id;
            document.getElementById('form_hapus_produk_tampil_di_beranda').submit();
            document.getElementById('loading').style.display = 'block';
        }
    }
</script>
<!-- TINYMCE -->
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
  tinymce.init({ selector:'textarea',
  height: 400,
  plugins: [
    'advlist autolink lists link image charmap print preview anchor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste code'
  ],
  toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image' });
</script>