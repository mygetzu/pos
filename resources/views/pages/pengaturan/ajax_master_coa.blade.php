<table id="ajax_produk_tabless" class="table table-striped table-bordered">
    <thead>
        <th style="text-align:center">NO</th>
        <th style="text-align:center">KODE</th>
        <th style="text-align:center">NAMA</th>
        <th style="text-align:center">LEVEL</th>
        <th style="text-align:center">PARENT</th>
        <th></th>
        <th></th>
    </thead>
    <tbody>
        <?php $i=1; ?>
        @foreach($master_coa as $val)
        <tr>
            <td align="center">{{ $i++ }}</td>
            <td>{{ $val->kode }}</td>
            <td><?php for($j=0; $j <= $val->level; $j++) echo "&nbsp&nbsp"; ?>{{ $val->nama }}</td>
            <td align="center">{{ $val->level }}</td>
            <td>{{ $val->parent }}</td>
            <td align="center">
              <a class="btn btn-warning flat" data-toggle="modal" data-target="#ubah_master_coa" onclick="ubah_btn('{{ $val->kode }}', '{{ $val->nama }}', '{{ $val->level }}', '{{ $val->parent }}')"><i class="fa fa-edit"></i> Ubah</a>
            </td>
            <td align="center">
              @if((int)$val->child === 0)
              <a class="btn btn-danger flat" onclick="btn_hapus('{{ $val->kode }}')"><i class="fa fa-trash"></i> Hapus</a>
              @endif
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
  $(function () {
    $("#ajax_produk_tabless").DataTable();
  });
</script>