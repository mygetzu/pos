<td colspan="6">
    <form method="post" action="{{ url('/do_tambah_root_coa') }}" class="form-horizontal">
        <input type="hidden" name="level" value="{{ $level }}">
        <input type="hidden" name="parent" value="{{ $parent }}">
        {{ csrf_field() }}
        <div class="form-group">
            <div class="col-sm-2">
                <input type="text" name="kode" class="form-control" value="{{ $kode }}">
            </div>
            <div class="col-sm-3">
                <input type="text" name="nama" class="form-control">
            </div>
            <div class="col-sm-3">
                <select class="form-control" name="debit_or_kredit">
                    <option value="">-- Pilih Debit/Kredit --</option>
                    <option value="D">Debit</option>
                    <option value="K">Kredit</option>
                </select>
            </div>
            <div class="col-sm-2">
                <button type="submit" class="btn btn-primary flat" onclick="document.getElementById('loading').style.display = 'block'"><i class="fa fa-floppy-o"></i> Simpan</button>
            </div>
            <div class="col-sm-2">
                <!-- <button type="button" class="btn btn-default flat" onclick="batal_tambah_root()"><i class="fa fa-ban"></i> Batal</button> -->
            </div>
        </div>
    </form>
</td>