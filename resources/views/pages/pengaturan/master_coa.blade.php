@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-body">
                <a data-toggle="modal" data-target="#tambah_akun" onclick="tambah_akun('-')">
                    <button class='btn bg-purple flat'><span class='fa fa-plus'></span> Tambah Root</button>
                </a>
                <br>
                <br>
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                @if ($errors->has('nama'))
                <div class="alert alert-danger alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-ban"></i>
                  {{ $errors->first('nama') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                @if(Session::has('message_error'))
                <div class="alert alert-danger alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-ban"></i>
                  {{ Session::get('message_error') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <div class="col-md-12" id="content">
                    <table id="table_coa" class="table table-striped table-bordered">
                        <tbody>
                            <?php $i=0; ?>
                            @foreach($master_coa as $val)
                            <tr>
                                <td colspan="2"><?php for ($j = 0; $j < $val->level; $j++) {
                                        echo "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp";
                                    }
                                    ?>{{ $val->kode }} - {{ $val->nama }}</td>
                                <td align="center" width="10%">
                                    <a data-toggle="modal" data-target="#tambah_akun">
                                        <button class="btn btn-primary flat" onclick="tambah_akun('{{ $val->kode }}')" data-toggle="tooltip" title="tambah child" style="font-size: 15pt"><i class="fa fa-plus-circle"></i></button>
                                    </a>
                                </td>
                                <td align="center" width="10%">
                                    <a data-toggle="modal" data-target="#ubah_akun">
                                        <button class="btn btn-warning flat" onclick="ubah_akun('{{ $val->id }}', '{{ $val->kode }}', '{{ $val->parent }}', '{{ $val->level }}', '{{ $val->nama }}', '{{ $val->group_kode }}')" data-toggle="tooltip" title="ubah" style="font-size: 15pt"><i class="fa fa-edit"></i></button>
                                    </a>
                                </td>
                                <td align="center" width="10%">
                                    <?php $is_exist = DB::table('tmst_master_coa')->where('parent', (string)$val->kode)->where('level', (int)1)->count()?>
                                    @if((int)$val->level == 0)
                                    <button class="btn btn-danger flat" onclick="btn_hapus('{{ $val->kode }}')" data-toggle="tooltip" title="hapus" style="font-size: 15pt" disabled><i class="fa fa-trash"></i></button>
                                    @elseif((int)$val->level == 1 || $is_exist == 0)
                                    <button class="btn btn-danger flat" onclick="btn_hapus('{{ $val->kode }}')" data-toggle="tooltip" title="hapus {{ $is_exist }}" style="font-size: 15pt"><i class="fa fa-trash"></i></button>
                                    @endif
                                </td>
                            </tr>
                            <?php $i++; ?>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="overlay" id="loading" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>
<form action="{{ url('/hapus_master_coa') }}" id="form_hapus_master_coa" method="post">
  {{ csrf_field() }}
  <input type="hidden" name="kode" id="hapus_kode">
</form>

<form action="{{ url('/tambah_coa') }}" method="post" id="form_tambah_coa">
    {{ csrf_field() }}
    <input type="hidden" name="kode" id="form_kode">
    <input type="hidden" name="nama" id="form_nama">
    <input type="hidden" name="level" id="form_level">
    <input type="hidden" name="parent" id="form_parent">
</form>

<div class="modal fade" id="tambah_akun" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content box">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah Akun</h4>
            </div>
            <form class="form-horizontal" method="post" action="{{ url('/tambah_master_coa') }}" autocomplete="off">
            <div class="modal-body">
                <input type="hidden" name="parent" id="tambah_akun_parent">
                <input type="hidden" name="level" id="tambah_akun_level">
                {{ csrf_field() }}
                <div class="form-group" id="display_parent" style="display: none">
                    <label class="col-sm-2 control-label">Parent</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="tambah_akun_parent2" disabled="true">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Kode</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="tambah_akun_kode" name="kode">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama*</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="nama" placeholder="Masukkan nama" style="text-transform: capitalize;">
                    </div>
                </div>
                <div class="form-group" id="display_group" style="display: none">
                    <label class="col-sm-2 control-label">Group</label>
                    <div class="col-sm-10">
                        <select class="form-control" name="group_kode">
                            <option value="">-- Pilih Group --</option>
                            @foreach($group_account as $val)
                            <option value="{{ $val->kode }}">{{ $val->kode }} - {{ $val->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <p>Keterangan (*) : Wajib Diisi</p>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default flat pull-left"><span class="glyphicon glyphicon-ban-circle"></span> Batal</button>
                <button type="submit" class="btn btn-primary flat" onclick="document.getElementById('loading2').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
            </div>
            </form>
            <div class="overlay" id="loading2" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ubah_akun" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content box">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ubah Akun</h4>
            </div>
            <form class="form-horizontal" method="post" action="{{ url('/ubah_master_coa') }}" autocomplete="off">
            <div class="modal-body">
                <input type="hidden" name="id" id="ubah_akun_id">
                <input type="hidden" name="parent" id="ubah_akun_parent">
                <input type="hidden" name="level" id="ubah_akun_level">
                {{ csrf_field() }}
                <div class="form-group">
                    <label class="col-sm-2 control-label">Kode</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="kode" id="ubah_akun_kode">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama*</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="nama" id="ubah_akun_nama" placeholder="Masukkan nama" style="text-transform: capitalize;">
                    </div>
                </div>
                <div class="form-group" id="display_group2" style="display: none">
                    <label class="col-sm-2 control-label">Group</label>
                    <div class="col-sm-10">
                        <select class="form-control" name="group_kode" id="ubah_group_kode">
                            <option value="">-- Pilih Group --</option>
                            @foreach($group_account as $val)
                            <option value="{{ $val->kode }}">{{ $val->kode }} - {{ $val->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <p>Keterangan (*) : Wajib Diisi</p>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default flat pull-left"><span class="glyphicon glyphicon-ban-circle"></span> Batal</button>
                <button type="submit" class="btn btn-primary flat" onclick="document.getElementById('loading3').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
            </div>
            </form>
            <div class="overlay" id="loading3" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

@stop

<script src="{{ asset('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip(); 
    });

    function btn_hapus(kode)
    {
      var cek = confirm("Apakah Anda yakin akan menghapus akun?");
      if(cek)
      {
        document.getElementById("hapus_kode").value = kode;
        document.getElementById("loading").style.display = 'block';
        document.getElementById("form_hapus_master_coa").submit();
      }
    }

    function tambah_akun(kode_parent)
    {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })
        
        var url = "{{ url('/get_data_akun_baru') }}";
        
        $.ajax({
            type: "POST",
            url: url,
            dataType: 'JSON',
            data: { kode_parent:kode_parent },
            success: function (data) {

                if(data.kode_parent != '-')
                {
                    document.getElementById('display_parent').style.display = 'block';
                    document.getElementById('display_group').style.display  = 'none';
                }
                else
                {
                    document.getElementById('display_parent').style.display = 'none';
                    document.getElementById('display_group').style.display  = 'block';
                }

                document.getElementById('tambah_akun_kode').value       = data.kode;
                document.getElementById('tambah_akun_parent').value     = data.kode_parent;
                document.getElementById('tambah_akun_parent2').value    = data.nama_parent;
                document.getElementById('tambah_akun_level').value      = data.level;
            },
            error: function (data) {
                alert('ooo');
            }
        });
    }

    function ubah_akun(id, kode, parent, level, nama, group_kode)
    { 
        if(parent != '-')
        {
            document.getElementById('display_group2').style.display  = 'none';
        }
        else
        {
            document.getElementById('display_group2').style.display  = 'block';
            document.getElementById('ubah_group_kode').value = group_kode;
        }

        document.getElementById('ubah_akun_id').value               = id;
        document.getElementById('ubah_akun_kode').value             = kode;
        document.getElementById('ubah_akun_parent').value           = parent;
        document.getElementById('ubah_akun_level').value            = level;
        document.getElementById('ubah_akun_nama').value             = nama;
    }
</script>