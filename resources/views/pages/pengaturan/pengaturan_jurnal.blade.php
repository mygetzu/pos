<link rel="stylesheet" href="{{ asset('/adminlte/plugins/select2/select2.min.css') }}">
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="nav-tabs-custom box tab-primary flat" style="border-top: 0">
            <ul class="nav nav-tabs" id="myTab">
                <li><a href="#tab_1" data-toggle="tab">Nota Beli</a></li>
                <li><a href="#tab_2" data-toggle="tab">Nota Jual</a></li>
                <li><a href="#tab_3" data-toggle="tab">Retur Beli</a></li>
                <li><a href="#tab_4" data-toggle="tab">Retur Jual</a></li>
                <li><a href="#tab_5" data-toggle="tab">Jasa Servis</a></li>
                <li><a href="#tab_8" data-toggle="tab">Transaksi Jasa</a></li>
                <li><a href="#tab_6" data-toggle="tab">Pembayaran Supplier</a></li>
                <li><a href="#tab_7" data-toggle="tab">Pembayaran Pelanggan</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane" id="tab_1">
                    @if(Session::has('message1'))
                    <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                      <i class="fa fa-check"></i>
                      {{ Session::get('message1') }}
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    @endif
                    <table class="table table-striped table-bordered">
                        <thead>
                            <th style="text-align:center">NO</th>
                            <th style="text-align:center">NAMA TRANSAKSI</th>
                            <th style="text-align:center">NAMA AKUN</th>
                            <th style="text-align:center">DEBIT/KREDIT</th>
                            <th style="text-align:center"></th>
                            <th style="text-align:center"></th>
                        </thead>
                        <tbody>
                            <?php $i=1; ?>
                            @foreach($pengaturan_jurnal_nota_beli as $val)
                            <tr>
                                <form method="post" action="{{ url('/ubah_pengaturan_jurnal') }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{ $val->id }}">
                                <td align="center">{{ $i++ }}</td>
                                <td>
                                    <div id="display_nota_beli_nama_transaksi{{ $i }}">
                                    {{ $val->nama_transaksi->nama }}
                                    </div>
                                    <div id="display_ubah_nota_beli_nama_transaksi{{ $i }}" style="display:none">
                                        <select class="form-control select2" data-placeholder="Pilih transaksi" style="width:100%" name="transaksi_id">
                                            <option value=""></option>
                                            @foreach($nota_beli as $val2)
                                            <option value="{{ $val2->id }}" <?php if($val->nama_transaksi->id == $val2->id) echo 'selected';?>>{{ $val2->nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <div id="display_nota_beli_master_coa{{ $i }}">
                                    {{ $val->master_coa->kode }} - {{ $val->master_coa->nama }}
                                    </div>
                                    <div id="display_ubah_nota_beli_master_coa{{ $i }}" style="display:none">
                                        <select class="form-control select2" data-placeholder="Pilih akun" style="width:100%" name="akun_id">
                                            <option value=""></option>
                                            <?php for($j = 0; $j < count($kode_akun); $j++) { ?>
                                            <option value="{{ $kode_akun[$j]['id'] }}" <?php if($val->master_coa->id == $kode_akun[$j]['id']) echo 'selected';?>>{{ $kode_akun[$j]['kode'] }} - {{ $kode_akun[$j]['nama'] }}</option>
                                            <?php } ?>
                                        </select>
                                    </div>                                    
                                </td>
                                <td align="center">
                                    <div id="display_nota_beli_debit_or_kredit{{ $i }}">
                                    @if($val->debit_or_kredit === "D")
                                    Debit
                                    @else
                                    Kredit
                                    @endif
                                    </div>
                                    <div id="display_ubah_nota_beli_debit_or_kredit{{ $i }}" style="display:none">
                                        <select class="form-control select2" data-placeholder="Pilih transaksi" style="width:100%" name="debit_or_kredit">
                                            <option value=""></option>
                                            <option value="D" <?php if($val->debit_or_kredit == "D") echo 'selected';?>>Debit</option>
                                            <option value="K" <?php if($val->debit_or_kredit == "K") echo 'selected';?>>Kredit</option>
                                        </select>
                                    </div> 
                                </td>
                                <td align="center">
                                    <div id="nota_beli_btn_ubah{{ $i }}">
                                        <button class="btn btn-warning flat" type="button" onclick="ubah_nota_beli('{{ $i }}')" data-toggle="tooltip" title="ubah" style="font-size: 15pt"><span class="fa fa-edit"></span></button>
                                    </div>
                                    <div id="nota_beli_btn_simpan{{ $i }}" style="display:none">
                                        <button class="btn btn-primary flat" type="submit" onclick="document.getElementById('loading').style.display = 'block'" data-toggle="tooltip" title="simpan" style="font-size: 15pt"><span class="fa fa-floppy-o"></span></button>
                                    </div>
                                </td>
                                <td align="center">
                                    <div id="nota_beli_btn_hapus{{ $i }}">
                                        <button class="btn btn-danger flat" type="button" onclick="hapus_nota_beli('{{ $val->id }}')" data-toggle="tooltip" title="hapus" style="font-size: 15pt"><span class="fa fa-trash"></span></button>
                                    </div>
                                    <div id="nota_beli_btn_batal{{ $i }}" style="display:none">
                                        <button class="btn btn-default flat" type="button" onclick="batal_nota_beli('{{ $i }}')" data-toggle="tooltip" title="batal" style="font-size: 15pt"><span class="fa fa-ban"></span></button>
                                    </div>
                                </td>
                                </form>
                            </tr>
                            @endforeach
                            <tr>
                                <form method="post" action="{{ url('/tambah_pengaturan_jurnal') }}">
                                {{ csrf_field() }}
                                <td align="center"></td>
                                <td>
                                    <select class="form-control select2" data-placeholder="Pilih transaksi" style="width:100%" name="transaksi_id">
                                        <option value=""></option>
                                        @foreach($nota_beli as $val2)
                                        <option value="{{ $val2->id }}">{{ $val2->nama }}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control select2" data-placeholder="Pilih akun" style="width:100%" name="akun_id">
                                        <option value=""></option>
                                        <?php for($j = 0; $j < count($kode_akun); $j++) { ?>
                                        <option value="{{ $kode_akun[$j]['id'] }}">{{ $kode_akun[$j]['kode'] }} - {{ $kode_akun[$j]['nama'] }}</option>
                                        <?php } ?>
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control select2" data-placeholder="Pilih debit/kredit" style="width:100%" name="debit_or_kredit">
                                        <option value=""></option>
                                        <option value="D">Debit</option>
                                        <option value="K">Kredit</option>
                                    </select>
                                </td>
                                <td align="center">
                                    <button class="btn btn-primary flat" type="submit" onclick="document.getElementById('loading').style.display = 'block'" data-toggle="tooltip" title="simpan" style="font-size: 15pt"><span class="fa fa-floppy-o"></span></button>
                                </td>
                                <td></td>
                                </form>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="tab_2">
                    @if(Session::has('message2'))
                    <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                      <i class="fa fa-check"></i>
                      {{ Session::get('message2') }}
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    @endif
                    <table class="table table-striped table-bordered">
                        <thead>
                            <th style="text-align:center">NO</th>
                            <th style="text-align:center">NAMA TRANSAKSI</th>
                            <th style="text-align:center">NAMA AKUN</th>
                            <th style="text-align:center">DEBIT/KREDIT</th>
                            <th style="text-align:center"></th>
                            <th style="text-align:center"></th>
                        </thead>
                        <tbody>
                            <?php $i=1; ?>
                            @foreach($pengaturan_jurnal_nota_jual as $val)
                            <tr>
                                <form method="post" action="{{ url('/ubah_pengaturan_jurnal') }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{ $val->id }}">
                                <td align="center">{{ $i++ }}</td>
                                <td>
                                    <div id="display_nota_jual_nama_transaksi{{ $i }}">
                                    {{ $val->nama_transaksi->nama }}
                                    </div>
                                    <div id="display_ubah_nota_jual_nama_transaksi{{ $i }}" style="display:none">
                                        <select class="form-control select2" data-placeholder="Pilih transaksi" style="width:100%" name="transaksi_id">
                                            <option value=""></option>
                                            @foreach($nota_jual as $val2)
                                            <option value="{{ $val2->id }}" <?php if($val->nama_transaksi->id == $val2->id) echo 'selected';?>>{{ $val2->nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <div id="display_nota_jual_master_coa{{ $i }}">
                                    {{ $val->master_coa->kode }} - {{ $val->master_coa->nama }}
                                    </div>
                                    <div id="display_ubah_nota_jual_master_coa{{ $i }}" style="display:none">
                                        <select class="form-control select2" data-placeholder="Pilih akun" style="width:100%" name="akun_id">
                                            <option value=""></option>
                                            <?php for($j = 0; $j < count($kode_akun); $j++) { ?>
                                            <option value="{{ $kode_akun[$j]['id'] }}" <?php if($val->master_coa->id == $kode_akun[$j]['id']) echo 'selected';?>>{{ $kode_akun[$j]['kode'] }} - {{ $kode_akun[$j]['nama'] }}</option>
                                            <?php } ?>
                                        </select>
                                    </div>                                    
                                </td>
                                <td align="center">
                                    <div id="display_nota_jual_debit_or_kredit{{ $i }}">
                                    @if($val->debit_or_kredit === "D")
                                    Debit
                                    @else
                                    Kredit
                                    @endif
                                    </div>
                                    <div id="display_ubah_nota_jual_debit_or_kredit{{ $i }}" style="display:none">
                                        <select class="form-control select2" data-placeholder="Pilih transaksi" style="width:100%" name="debit_or_kredit">
                                            <option value=""></option>
                                            <option value="D" <?php if($val->debit_or_kredit == "D") echo 'selected';?>>Debit</option>
                                            <option value="K" <?php if($val->debit_or_kredit == "K") echo 'selected';?>>Kredit</option>
                                        </select>
                                    </div> 
                                </td>
                                <td align="center">
                                    <div id="nota_jual_btn_ubah{{ $i }}">
                                        <button class="btn btn-warning flat" type="button" onclick="ubah_nota_jual('{{ $i }}')" data-toggle="tooltip" title="ubah" style="font-size: 15pt"><span class="fa fa-edit"></span></button>
                                    </div>
                                    <div id="nota_jual_btn_simpan{{ $i }}" style="display:none">
                                        <button class="btn btn-primary flat" type="submit" onclick="document.getElementById('loading').style.display = 'block'" data-toggle="tooltip" title="simpan" style="font-size: 15pt"><span class="fa fa-floppy-o"></span></button>
                                    </div>
                                </td>
                                <td align="center">
                                    <div id="nota_jual_btn_hapus{{ $i }}">
                                        <button class="btn btn-danger flat" type="button" onclick="hapus_nota_jual('{{ $val->id }}')" data-toggle="tooltip" title="hapus" style="font-size: 15pt"><span class="fa fa-trash"></span></button>
                                    </div>
                                    <div id="nota_jual_btn_batal{{ $i }}" style="display:none">
                                        <button class="btn btn-default flat" type="button" onclick="batal_nota_jual('{{ $i }}')" data-toggle="tooltip" title="batal" style="font-size: 15pt"><span class="fa fa-ban"></span></button>
                                    </div>
                                </td>
                                </form>
                            </tr>
                            @endforeach
                            <tr>
                                <form method="post" action="{{ url('/tambah_pengaturan_jurnal') }}">
                                {{ csrf_field() }}
                                <td align="center"></td>
                                <td>
                                    <select class="form-control select2" data-placeholder="Pilih transaksi" style="width:100%" name="transaksi_id">
                                        <option value=""></option>
                                        @foreach($nota_jual as $val2)
                                        <option value="{{ $val2->id }}">{{ $val2->nama }}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control select2" data-placeholder="Pilih akun" style="width:100%" name="akun_id">
                                        <option value=""></option>
                                        <?php for($j = 0; $j < count($kode_akun); $j++) { ?>
                                        <option value="{{ $kode_akun[$j]['id'] }}">{{ $kode_akun[$j]['kode'] }} - {{ $kode_akun[$j]['nama'] }}</option>
                                        <?php } ?>
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control select2" data-placeholder="Pilih debit/kredit" style="width:100%" name="debit_or_kredit">
                                        <option value=""></option>
                                        <option value="D">Debit</option>
                                        <option value="K">Kredit</option>
                                    </select>
                                </td>
                                <td align="center">
                                    <button class="btn btn-primary flat" type="submit" onclick="document.getElementById('loading').style.display = 'block'" data-toggle="tooltip" title="simpan" style="font-size: 15pt"><span class="fa fa-floppy-o"></span></button>
                                </td>
                                <td></td>
                                </form>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="tab_3">
                    @if(Session::has('message3'))
                    <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                      <i class="fa fa-check"></i>
                      {{ Session::get('message3') }}
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    @endif
                    <table class="table table-striped table-bordered">
                        <thead>
                            <th style="text-align:center">NO</th>
                            <th style="text-align:center">NAMA TRANSAKSI</th>
                            <th style="text-align:center">NAMA AKUN</th>
                            <th style="text-align:center">DEBIT/KREDIT</th>
                            <th style="text-align:center"></th>
                            <th style="text-align:center"></th>
                        </thead>
                        <tbody>
                            <?php $i=1; ?>
                            @foreach($pengaturan_jurnal_retur_beli as $val)
                            <tr>
                                <form method="post" action="{{ url('/ubah_pengaturan_jurnal') }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{ $val->id }}">
                                <td align="center">{{ $i++ }}</td>
                                <td>
                                    <div id="display_retur_beli_nama_transaksi{{ $i }}">
                                    {{ $val->nama_transaksi->nama }}
                                    </div>
                                    <div id="display_ubah_retur_beli_nama_transaksi{{ $i }}" style="display:none">
                                        <select class="form-control select2" data-placeholder="Pilih transaksi" style="width:100%" name="transaksi_id">
                                            <option value=""></option>
                                            @foreach($retur_beli as $val2)
                                            <option value="{{ $val2->id }}" <?php if($val->nama_transaksi->id == $val2->id) echo 'selected';?>>{{ $val2->nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <div id="display_retur_beli_master_coa{{ $i }}">
                                    {{ $val->master_coa->kode }} - {{ $val->master_coa->nama }}
                                    </div>
                                    <div id="display_ubah_retur_beli_master_coa{{ $i }}" style="display:none">
                                        <select class="form-control select2" data-placeholder="Pilih akun" style="width:100%" name="akun_id">
                                            <option value=""></option>
                                            <?php for($j = 0; $j < count($kode_akun); $j++) { ?>
                                            <option value="{{ $kode_akun[$j]['id'] }}" <?php if($val->master_coa->id == $kode_akun[$j]['id']) echo 'selected';?>>{{ $kode_akun[$j]['kode'] }} - {{ $kode_akun[$j]['nama'] }}</option>
                                            <?php } ?>
                                        </select>
                                    </div>                                    
                                </td>
                                <td align="center">
                                    <div id="display_retur_beli_debit_or_kredit{{ $i }}">
                                    @if($val->debit_or_kredit === "D")
                                    Debit
                                    @else
                                    Kredit
                                    @endif
                                    </div>
                                    <div id="display_ubah_retur_beli_debit_or_kredit{{ $i }}" style="display:none">
                                        <select class="form-control select2" data-placeholder="Pilih transaksi" style="width:100%" name="debit_or_kredit">
                                            <option value=""></option>
                                            <option value="D" <?php if($val->debit_or_kredit == "D") echo 'selected';?>>Debit</option>
                                            <option value="K" <?php if($val->debit_or_kredit == "K") echo 'selected';?>>Kredit</option>
                                        </select>
                                    </div> 
                                </td>
                                <td align="center">
                                    <div id="retur_beli_btn_ubah{{ $i }}">
                                        <button class="btn btn-warning flat" type="button" onclick="ubah_retur_beli('{{ $i }}')" data-toggle="tooltip" title="ubah" style="font-size: 15pt"><span class="fa fa-edit"></span></button>
                                    </div>
                                    <div id="retur_beli_btn_simpan{{ $i }}" style="display:none">
                                        <button class="btn btn-primary flat" type="submit" onclick="document.getElementById('loading').style.display = 'block'" data-toggle="tooltip" title="simpan" style="font-size: 15pt"><span class="fa fa-floppy-o"></span></button>
                                    </div>
                                </td>
                                <td align="center">
                                    <div id="retur_beli_btn_hapus{{ $i }}">
                                        <button class="btn btn-danger flat" type="button" onclick="hapus_retur_beli('{{ $val->id }}')" data-toggle="tooltip" title="hapus" style="font-size: 15pt"><span class="fa fa-trash"></span></button>
                                    </div>
                                    <div id="retur_beli_btn_batal{{ $i }}" style="display:none">
                                        <button class="btn btn-default flat" type="button" onclick="batal_retur_beli('{{ $i }}')" data-toggle="tooltip" title="batal" style="font-size: 15pt"><span class="fa fa-ban"></span></button>
                                    </div>
                                </td>
                                </form>
                            </tr>
                            @endforeach
                            <tr>
                                <form method="post" action="{{ url('/tambah_pengaturan_jurnal') }}">
                                {{ csrf_field() }}
                                <td align="center"></td>
                                <td>
                                    <select class="form-control select2" data-placeholder="Pilih transaksi" style="width:100%" name="transaksi_id">
                                        <option value=""></option>
                                        @foreach($retur_beli as $val2)
                                        <option value="{{ $val2->id }}">{{ $val2->nama }}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control select2" data-placeholder="Pilih akun" style="width:100%" name="akun_id">
                                        <option value=""></option>
                                        <?php for($j = 0; $j < count($kode_akun); $j++) { ?>
                                        <option value="{{ $kode_akun[$j]['id'] }}">{{ $kode_akun[$j]['kode'] }} - {{ $kode_akun[$j]['nama'] }}</option>
                                        <?php } ?>
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control select2" data-placeholder="Pilih debit/kredit" style="width:100%" name="debit_or_kredit">
                                        <option value=""></option>
                                        <option value="D">Debit</option>
                                        <option value="K">Kredit</option>
                                    </select>
                                </td>
                                <td align="center">
                                    <button class="btn btn-primary flat" type="submit" onclick="document.getElementById('loading').style.display = 'block'" data-toggle="tooltip" title="simpan" style="font-size: 15pt"><span class="fa fa-floppy-o"></span></button>
                                </td>
                                <td></td>
                                </form>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="tab_4">
                    @if(Session::has('message4'))
                    <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                      <i class="fa fa-check"></i>
                      {{ Session::get('message4') }}
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    @endif
                    <table class="table table-striped table-bordered">
                        <thead>
                            <th style="text-align:center">NO</th>
                            <th style="text-align:center">NAMA TRANSAKSI</th>
                            <th style="text-align:center">NAMA AKUN</th>
                            <th style="text-align:center">DEBIT/KREDIT</th>
                            <th style="text-align:center"></th>
                            <th style="text-align:center"></th>
                        </thead>
                        <tbody>
                            <?php $i=1; ?>
                            @foreach($pengaturan_jurnal_retur_jual as $val)
                            <tr>
                                <form method="post" action="{{ url('/ubah_pengaturan_jurnal') }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{ $val->id }}">
                                <td align="center">{{ $i++ }}</td>
                                <td>
                                    <div id="display_retur_jual_nama_transaksi{{ $i }}">
                                    {{ $val->nama_transaksi->nama }}
                                    </div>
                                    <div id="display_ubah_retur_jual_nama_transaksi{{ $i }}" style="display:none">
                                        <select class="form-control select2" data-placeholder="Pilih transaksi" style="width:100%" name="transaksi_id">
                                            <option value=""></option>
                                            @foreach($retur_jual as $val2)
                                            <option value="{{ $val2->id }}" <?php if($val->nama_transaksi->id == $val2->id) echo 'selected';?>>{{ $val2->nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <div id="display_retur_jual_master_coa{{ $i }}">
                                    {{ $val->master_coa->kode }} - {{ $val->master_coa->nama }}
                                    </div>
                                    <div id="display_ubah_retur_jual_master_coa{{ $i }}" style="display:none">
                                        <select class="form-control select2" data-placeholder="Pilih akun" style="width:100%" name="akun_id">
                                            <option value=""></option>
                                            <?php for($j = 0; $j < count($kode_akun); $j++) { ?>
                                            <option value="{{ $kode_akun[$j]['id'] }}" <?php if($val->master_coa->id == $kode_akun[$j]['id']) echo 'selected';?>>{{ $kode_akun[$j]['kode'] }} - {{ $kode_akun[$j]['nama'] }}</option>
                                            <?php } ?>
                                        </select>
                                    </div>                                    
                                </td>
                                <td align="center">
                                    <div id="display_retur_jual_debit_or_kredit{{ $i }}">
                                    @if($val->debit_or_kredit === "D")
                                    Debit
                                    @else
                                    Kredit
                                    @endif
                                    </div>
                                    <div id="display_ubah_retur_jual_debit_or_kredit{{ $i }}" style="display:none">
                                        <select class="form-control select2" data-placeholder="Pilih transaksi" style="width:100%" name="debit_or_kredit">
                                            <option value=""></option>
                                            <option value="D" <?php if($val->debit_or_kredit == "D") echo 'selected';?>>Debit</option>
                                            <option value="K" <?php if($val->debit_or_kredit == "K") echo 'selected';?>>Kredit</option>
                                        </select>
                                    </div> 
                                </td>
                                <td align="center">
                                    <div id="retur_jual_btn_ubah{{ $i }}">
                                        <button class="btn btn-warning flat" type="button" onclick="ubah_retur_jual('{{ $i }}')" data-toggle="tooltip" title="ubah" style="font-size: 15pt"><span class="fa fa-edit"></span></button>
                                    </div>
                                    <div id="retur_jual_btn_simpan{{ $i }}" style="display:none">
                                        <button class="btn btn-primary flat" type="submit" onclick="document.getElementById('loading').style.display = 'block'" data-toggle="tooltip" title="simpan" style="font-size: 15pt"><span class="fa fa-floppy-o"></span></button>
                                    </div>
                                </td>
                                <td align="center">
                                    <div id="retur_jual_btn_hapus{{ $i }}">
                                        <button class="btn btn-danger flat" type="button" onclick="hapus_retur_jual('{{ $val->id }}')" data-toggle="tooltip" title="hapus" style="font-size: 15pt"><span class="fa fa-trash"></span></button>
                                    </div>
                                    <div id="retur_jual_btn_batal{{ $i }}" style="display:none">
                                        <button class="btn btn-default flat" type="button" onclick="batal_retur_jual('{{ $i }}')" data-toggle="tooltip" title="batal" style="font-size: 15pt"><span class="fa fa-ban"></span></button>
                                    </div>
                                </td>
                                </form>
                            </tr>
                            @endforeach
                            <tr>
                                <form method="post" action="{{ url('/tambah_pengaturan_jurnal') }}">
                                {{ csrf_field() }}
                                <td align="center"></td>
                                <td>
                                    <select class="form-control select2" data-placeholder="Pilih transaksi" style="width:100%" name="transaksi_id">
                                        <option value=""></option>
                                        @foreach($retur_jual as $val2)
                                        <option value="{{ $val2->id }}">{{ $val2->nama }}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control select2" data-placeholder="Pilih akun" style="width:100%" name="akun_id">
                                        <option value=""></option>
                                        <?php for($j = 0; $j < count($kode_akun); $j++) { ?>
                                        <option value="{{ $kode_akun[$j]['id'] }}">{{ $kode_akun[$j]['kode'] }} - {{ $kode_akun[$j]['nama'] }}</option>
                                        <?php } ?>
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control select2" data-placeholder="Pilih debit/kredit" style="width:100%" name="debit_or_kredit">
                                        <option value=""></option>
                                        <option value="D">Debit</option>
                                        <option value="K">Kredit</option>
                                    </select>
                                </td>
                                <td align="center">
                                    <button class="btn btn-primary flat" type="submit" onclick="document.getElementById('loading').style.display = 'block'" data-toggle="tooltip" title="simpan" style="font-size: 15pt"><span class="fa fa-floppy-o"></span></button>
                                </td>
                                <td></td>
                                </form>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="tab_5">
                    @if(Session::has('message5'))
                    <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                      <i class="fa fa-check"></i>
                      {{ Session::get('message5') }}
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    @endif
                    <table class="table table-striped table-bordered">
                        <thead>
                            <th style="text-align:center">NO</th>
                            <th style="text-align:center">NAMA TRANSAKSI</th>
                            <th style="text-align:center">NAMA AKUN</th>
                            <th style="text-align:center">DEBIT/KREDIT</th>
                            <th style="text-align:center"></th>
                            <th style="text-align:center"></th>
                        </thead>
                        <tbody>
                            <?php $i=1; ?>
                            @foreach($pengaturan_jurnal_jasa_servis as $val)
                            <tr>
                                <form method="post" action="{{ url('/ubah_pengaturan_jurnal') }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{ $val->id }}">
                                <td align="center">{{ $i++ }}</td>
                                <td>
                                    <div id="display_jasa_servis_nama_transaksi{{ $i }}">
                                    {{ $val->nama_transaksi->nama }}
                                    </div>
                                    <div id="display_ubah_jasa_servis_nama_transaksi{{ $i }}" style="display:none">
                                        <select class="form-control select2" data-placeholder="Pilih transaksi" style="width:100%" name="transaksi_id">
                                            <option value=""></option>
                                            @foreach($jasa_servis as $val2)
                                            <option value="{{ $val2->id }}" <?php if($val->nama_transaksi->id == $val2->id) echo 'selected';?>>{{ $val2->nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <div id="display_jasa_servis_master_coa{{ $i }}">
                                    {{ $val->master_coa->kode }} - {{ $val->master_coa->nama }}
                                    </div>
                                    <div id="display_ubah_jasa_servis_master_coa{{ $i }}" style="display:none">
                                        <select class="form-control select2" data-placeholder="Pilih akun" style="width:100%" name="akun_id">
                                            <option value=""></option>
                                            <?php for($j = 0; $j < count($kode_akun); $j++) { ?>
                                            <option value="{{ $kode_akun[$j]['id'] }}" <?php if($val->master_coa->id == $kode_akun[$j]['id']) echo 'selected';?>>{{ $kode_akun[$j]['kode'] }} - {{ $kode_akun[$j]['nama'] }}</option>
                                            <?php } ?>
                                        </select>
                                    </div>                                    
                                </td>
                                <td align="center">
                                    <div id="display_jasa_servis_debit_or_kredit{{ $i }}">
                                    @if($val->debit_or_kredit === "D")
                                    Debit
                                    @else
                                    Kredit
                                    @endif
                                    </div>
                                    <div id="display_ubah_jasa_servis_debit_or_kredit{{ $i }}" style="display:none">
                                        <select class="form-control select2" data-placeholder="Pilih transaksi" style="width:100%" name="debit_or_kredit">
                                            <option value=""></option>
                                            <option value="D" <?php if($val->debit_or_kredit == "D") echo 'selected';?>>Debit</option>
                                            <option value="K" <?php if($val->debit_or_kredit == "K") echo 'selected';?>>Kredit</option>
                                        </select>
                                    </div> 
                                </td>
                                <td align="center">
                                    <div id="jasa_servis_btn_ubah{{ $i }}">
                                        <button class="btn btn-warning flat" type="button" onclick="ubah_jasa_servis('{{ $i }}')" data-toggle="tooltip" title="ubah" style="font-size: 15pt"><span class="fa fa-edit"></span></button>
                                    </div>
                                    <div id="jasa_servis_btn_simpan{{ $i }}" style="display:none">
                                        <button class="btn btn-primary flat" type="submit" onclick="document.getElementById('loading').style.display = 'block'" data-toggle="tooltip" title="simpan" style="font-size: 15pt"><span class="fa fa-floppy-o"></span></button>
                                    </div>
                                </td>
                                <td align="center">
                                    <div id="jasa_servis_btn_hapus{{ $i }}">
                                        <button class="btn btn-danger flat" type="button" onclick="hapus_jasa_servis('{{ $val->id }}')" data-toggle="tooltip" title="hapus" style="font-size: 15pt"><span class="fa fa-trash"></span></button>
                                    </div>
                                    <div id="jasa_servis_btn_batal{{ $i }}" style="display:none">
                                        <button class="btn btn-default flat" type="button" onclick="batal_jasa_servis('{{ $i }}')" data-toggle="tooltip" title="batal" style="font-size: 15pt"><span class="fa fa-ban"></span></button>
                                    </div>
                                </td>
                                </form>
                            </tr>
                            @endforeach
                            <tr>
                                <form method="post" action="{{ url('/tambah_pengaturan_jurnal') }}">
                                {{ csrf_field() }}
                                <td align="center"></td>
                                <td>
                                    <select class="form-control select2" data-placeholder="Pilih transaksi" style="width:100%" name="transaksi_id">
                                        <option value=""></option>
                                        @foreach($jasa_servis as $val2)
                                        <option value="{{ $val2->id }}">{{ $val2->nama }}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control select2" data-placeholder="Pilih akun" style="width:100%" name="akun_id">
                                        <option value=""></option>
                                        <?php for($j = 0; $j < count($kode_akun); $j++) { ?>
                                        <option value="{{ $kode_akun[$j]['id'] }}">{{ $kode_akun[$j]['kode'] }} - {{ $kode_akun[$j]['nama'] }}</option>
                                        <?php } ?>
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control select2" data-placeholder="Pilih debit/kredit" style="width:100%" name="debit_or_kredit">
                                        <option value=""></option>
                                        <option value="D">Debit</option>
                                        <option value="K">Kredit</option>
                                    </select>
                                </td>
                                <td align="center">
                                    <button class="btn btn-primary flat" type="submit" onclick="document.getElementById('loading').style.display = 'block'" data-toggle="tooltip" title="simpan" style="font-size: 15pt"><span class="fa fa-floppy-o"></span></button>
                                </td>
                                <td></td>
                                </form>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="tab_8">
                    @if(Session::has('message8'))
                    <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                      <i class="fa fa-check"></i>
                      {{ Session::get('message8') }}
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    @endif
                    <table class="table table-striped table-bordered">
                        <thead>
                            <th style="text-align:center">NO</th>
                            <th style="text-align:center">NAMA TRANSAKSI</th>
                            <th style="text-align:center">NAMA AKUN</th>
                            <th style="text-align:center">DEBIT/KREDIT</th>
                            <th style="text-align:center"></th>
                            <th style="text-align:center"></th>
                        </thead>
                        <tbody>
                            <?php $i=1; ?>
                            @foreach($pengaturan_jurnal_transaksi_jasa as $val)
                            <tr>
                                <form method="post" action="{{ url('/ubah_pengaturan_jurnal') }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{ $val->id }}">
                                <td align="center">{{ $i++ }}</td>
                                <td>
                                    <div id="display_transaksi_jasa_nama_transaksi{{ $i }}">
                                    {{ $val->nama_transaksi->nama }}
                                    </div>
                                    <div id="display_ubah_transaksi_jasa_nama_transaksi{{ $i }}" style="display:none">
                                        <select class="form-control select2" data-placeholder="Pilih transaksi" style="width:100%" name="transaksi_id">
                                            <option value=""></option>
                                            @foreach($transaksi_jasa as $val2)
                                            <option value="{{ $val2->id }}" <?php if($val->nama_transaksi->id == $val2->id) echo 'selected';?>>{{ $val2->nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <div id="display_transaksi_jasa_master_coa{{ $i }}">
                                    {{ $val->master_coa->kode }} - {{ $val->master_coa->nama }}
                                    </div>
                                    <div id="display_ubah_transaksi_jasa_master_coa{{ $i }}" style="display:none">
                                        <select class="form-control select2" data-placeholder="Pilih akun" style="width:100%" name="akun_id">
                                            <option value=""></option>
                                            <?php for($j = 0; $j < count($kode_akun); $j++) { ?>
                                            <option value="{{ $kode_akun[$j]['id'] }}" <?php if($val->master_coa->id == $kode_akun[$j]['id']) echo 'selected';?>>{{ $kode_akun[$j]['kode'] }} - {{ $kode_akun[$j]['nama'] }}</option>
                                            <?php } ?>
                                        </select>
                                    </div>                                    
                                </td>
                                <td align="center">
                                    <div id="display_transaksi_jasa_debit_or_kredit{{ $i }}">
                                    @if($val->debit_or_kredit === "D")
                                    Debit
                                    @else
                                    Kredit
                                    @endif
                                    </div>
                                    <div id="display_ubah_transaksi_jasa_debit_or_kredit{{ $i }}" style="display:none">
                                        <select class="form-control select2" data-placeholder="Pilih transaksi" style="width:100%" name="debit_or_kredit">
                                            <option value=""></option>
                                            <option value="D" <?php if($val->debit_or_kredit == "D") echo 'selected';?>>Debit</option>
                                            <option value="K" <?php if($val->debit_or_kredit == "K") echo 'selected';?>>Kredit</option>
                                        </select>
                                    </div> 
                                </td>
                                <td align="center">
                                    <div id="transaksi_jasa_btn_ubah{{ $i }}">
                                        <button class="btn btn-warning flat" type="button" onclick="ubah_transaksi_jasa('{{ $i }}')" data-toggle="tooltip" title="ubah" style="font-size: 15pt"><span class="fa fa-edit"></span></button>
                                    </div>
                                    <div id="transaksi_jasa_btn_simpan{{ $i }}" style="display:none">
                                        <button class="btn btn-primary flat" type="submit" onclick="document.getElementById('loading').style.display = 'block'" data-toggle="tooltip" title="simpan" style="font-size: 15pt"><span class="fa fa-floppy-o"></span></button>
                                    </div>
                                </td>
                                <td align="center">
                                    <div id="transaksi_jasa_btn_hapus{{ $i }}">
                                        <button class="btn btn-danger flat" type="button" onclick="hapus_transaksi_jasa('{{ $val->id }}')" data-toggle="tooltip" title="hapus" style="font-size: 15pt"><span class="fa fa-trash"></span></button>
                                    </div>
                                    <div id="transaksi_jasa_btn_batal{{ $i }}" style="display:none">
                                        <button class="btn btn-default flat" type="button" onclick="batal_nota_beli('{{ $i }}')" data-toggle="tooltip" title="batal" style="font-size: 15pt"><span class="fa fa-ban"></span></button>
                                    </div>
                                </td>
                                </form>
                            </tr>
                            @endforeach
                            <tr>
                                <form method="post" action="{{ url('/tambah_pengaturan_jurnal') }}">
                                {{ csrf_field() }}
                                <td align="center"></td>
                                <td>
                                    <select class="form-control select2" data-placeholder="Pilih transaksi" style="width:100%" name="transaksi_id">
                                        <option value=""></option>
                                        @foreach($transaksi_jasa as $val2)
                                        <option value="{{ $val2->id }}">{{ $val2->nama }}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control select2" data-placeholder="Pilih akun" style="width:100%" name="akun_id">
                                        <option value=""></option>
                                        <?php for($j = 0; $j < count($kode_akun); $j++) { ?>
                                        <option value="{{ $kode_akun[$j]['id'] }}">{{ $kode_akun[$j]['kode'] }} - {{ $kode_akun[$j]['nama'] }}</option>
                                        <?php } ?>
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control select2" data-placeholder="Pilih debit/kredit" style="width:100%" name="debit_or_kredit">
                                        <option value=""></option>
                                        <option value="D">Debit</option>
                                        <option value="K">Kredit</option>
                                    </select>
                                </td>
                                <td align="center">
                                    <button class="btn btn-primary flat" type="submit" onclick="document.getElementById('loading').style.display = 'block'" data-toggle="tooltip" title="simpan" style="font-size: 15pt"><span class="fa fa-floppy-o"></span></button>
                                </td>
                                <td></td>
                                </form>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="tab_6">
                    @if(Session::has('message6'))
                    <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                      <i class="fa fa-check"></i>
                      {{ Session::get('message6') }}
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    @endif
                    <table class="table table-striped table-bordered">
                        <thead>
                            <th style="text-align:center">NO</th>
                            <th style="text-align:center">NAMA TRANSAKSI</th>
                            <th style="text-align:center">NAMA AKUN</th>
                            <th style="text-align:center">DEBIT/KREDIT</th>
                            <th style="text-align:center"></th>
                            <th style="text-align:center"></th>
                        </thead>
                        <tbody>
                            <?php $i=1; ?>
                            @foreach($pengaturan_jurnal_pembayaran_supplier as $val)
                            <tr>
                                <form method="post" action="{{ url('/ubah_pengaturan_jurnal') }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{ $val->id }}">
                                <td align="center">{{ $i++ }}</td>
                                <td>
                                    <div id="display_pembayaran_supplier_nama_transaksi{{ $i }}">
                                    {{ $val->nama_transaksi->nama }}
                                    </div>
                                    <div id="display_ubah_pembayaran_supplier_nama_transaksi{{ $i }}" style="display:none">
                                        <select class="form-control select2" data-placeholder="Pilih transaksi" style="width:100%" name="transaksi_id">
                                            <option value=""></option>
                                            @foreach($pembayaran_supplier as $val2)
                                            <option value="{{ $val2->id }}" <?php if($val->nama_transaksi->id == $val2->id) echo 'selected';?>>{{ $val2->nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <div id="display_pembayaran_supplier_master_coa{{ $i }}">
                                    {{ $val->master_coa->kode }} - {{ $val->master_coa->nama }}
                                    </div>
                                    <div id="display_ubah_pembayaran_supplier_master_coa{{ $i }}" style="display:none">
                                        <select class="form-control select2" data-placeholder="Pilih akun" style="width:100%" name="akun_id">
                                            <option value=""></option>
                                            <?php for($j = 0; $j < count($kode_akun); $j++) { ?>
                                            <option value="{{ $kode_akun[$j]['id'] }}" <?php if($val->master_coa->id == $kode_akun[$j]['id']) echo 'selected';?>>{{ $kode_akun[$j]['kode'] }} - {{ $kode_akun[$j]['nama'] }}</option>
                                            <?php } ?>
                                        </select>
                                    </div>                                    
                                </td>
                                <td align="center">
                                    <div id="display_pembayaran_supplier_debit_or_kredit{{ $i }}">
                                    @if($val->debit_or_kredit === "D")
                                    Debit
                                    @else
                                    Kredit
                                    @endif
                                    </div>
                                    <div id="display_ubah_pembayaran_supplier_debit_or_kredit{{ $i }}" style="display:none">
                                        <select class="form-control select2" data-placeholder="Pilih transaksi" style="width:100%" name="debit_or_kredit">
                                            <option value=""></option>
                                            <option value="D" <?php if($val->debit_or_kredit == "D") echo 'selected';?>>Debit</option>
                                            <option value="K" <?php if($val->debit_or_kredit == "K") echo 'selected';?>>Kredit</option>
                                        </select>
                                    </div> 
                                </td>
                                <td align="center">
                                    <div id="pembayaran_supplier_btn_ubah{{ $i }}">
                                        <button class="btn btn-warning flat" type="button" onclick="ubah_pembayaran_supplier('{{ $i }}')" data-toggle="tooltip" title="ubah" style="font-size: 15pt"><span class="fa fa-edit"></span></button>
                                    </div>
                                    <div id="pembayaran_supplier_btn_simpan{{ $i }}" style="display:none">
                                        <button class="btn btn-primary flat" type="submit" onclick="document.getElementById('loading').style.display = 'block'" data-toggle="tooltip" title="simpan" style="font-size: 15pt"><span class="fa fa-floppy-o"></span></button>
                                    </div>
                                </td>
                                <td align="center">
                                    <div id="pembayaran_supplier_btn_hapus{{ $i }}">
                                        <button class="btn btn-danger flat" type="button" onclick="hapus_pembayaran_supplier('{{ $val->id }}')" data-toggle="tooltip" title="hapus" style="font-size: 15pt"><span class="fa fa-trash"></span></button>
                                    </div>
                                    <div id="pembayaran_supplier_btn_batal{{ $i }}" style="display:none">
                                        <button class="btn btn-default flat" type="button" onclick="batal_nota_beli('{{ $i }}')" data-toggle="tooltip" title="batal" style="font-size: 15pt"><span class="fa fa-ban"></span></button>
                                    </div>
                                </td>
                                </form>
                            </tr>
                            @endforeach
                            <tr>
                                <form method="post" action="{{ url('/tambah_pengaturan_jurnal') }}">
                                {{ csrf_field() }}
                                <td align="center"></td>
                                <td>
                                    <select class="form-control select2" data-placeholder="Pilih transaksi" style="width:100%" name="transaksi_id">
                                        <option value=""></option>
                                        @foreach($pembayaran_supplier as $val2)
                                        <option value="{{ $val2->id }}">{{ $val2->nama }}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control select2" data-placeholder="Pilih akun" style="width:100%" name="akun_id">
                                        <option value=""></option>
                                        <?php for($j = 0; $j < count($kode_akun); $j++) { ?>
                                        <option value="{{ $kode_akun[$j]['id'] }}">{{ $kode_akun[$j]['kode'] }} - {{ $kode_akun[$j]['nama'] }}</option>
                                        <?php } ?>
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control select2" data-placeholder="Pilih debit/kredit" style="width:100%" name="debit_or_kredit">
                                        <option value=""></option>
                                        <option value="D">Debit</option>
                                        <option value="K">Kredit</option>
                                    </select>
                                </td>
                                <td align="center">
                                    <button class="btn btn-primary flat" type="submit" onclick="document.getElementById('loading').style.display = 'block'" data-toggle="tooltip" title="simpan" style="font-size: 15pt"><span class="fa fa-floppy-o"></span></button>
                                </td>
                                <td></td>
                                </form>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="tab_7">
                    @if(Session::has('message7'))
                    <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                      <i class="fa fa-check"></i>
                      {{ Session::get('message7') }}
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    @endif
                    <table class="table table-striped table-bordered">
                        <thead>
                            <th style="text-align:center">NO</th>
                            <th style="text-align:center">NAMA TRANSAKSI</th>
                            <th style="text-align:center">NAMA AKUN</th>
                            <th style="text-align:center">DEBIT/KREDIT</th>
                            <th style="text-align:center"></th>
                            <th style="text-align:center"></th>
                        </thead>
                        <tbody>
                            <?php $i=1; ?>
                            @foreach($pengaturan_jurnal_pembayaran_pelanggan as $val)
                            <tr>
                                <form method="post" action="{{ url('/ubah_pengaturan_jurnal') }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{ $val->id }}">
                                <td align="center">{{ $i++ }}</td>
                                <td>
                                    <div id="display_pembayaran_pelanggan_nama_transaksi{{ $i }}">
                                    {{ $val->nama_transaksi->nama }}
                                    </div>
                                    <div id="display_ubah_pembayaran_pelanggan_nama_transaksi{{ $i }}" style="display:none">
                                        <select class="form-control select2" data-placeholder="Pilih transaksi" style="width:100%" name="transaksi_id">
                                            <option value=""></option>
                                            @foreach($pembayaran_pelanggan as $val2)
                                            <option value="{{ $val2->id }}" <?php if($val->nama_transaksi->id == $val2->id) echo 'selected';?>>{{ $val2->nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <div id="display_pembayaran_pelanggan_master_coa{{ $i }}">
                                    {{ $val->master_coa->kode }} - {{ $val->master_coa->nama }}
                                    </div>
                                    <div id="display_ubah_pembayaran_pelanggan_master_coa{{ $i }}" style="display:none">
                                        <select class="form-control select2" data-placeholder="Pilih akun" style="width:100%" name="akun_id">
                                            <option value=""></option>
                                            <?php for($j = 0; $j < count($kode_akun); $j++) { ?>
                                            <option value="{{ $kode_akun[$j]['id'] }}" <?php if($val->master_coa->id == $kode_akun[$j]['id']) echo 'selected';?>>{{ $kode_akun[$j]['kode'] }} - {{ $kode_akun[$j]['nama'] }}</option>
                                            <?php } ?>
                                        </select>
                                    </div>                                    
                                </td>
                                <td align="center">
                                    <div id="display_pembayaran_pelanggan_debit_or_kredit{{ $i }}">
                                    @if($val->debit_or_kredit === "D")
                                    Debit
                                    @else
                                    Kredit
                                    @endif
                                    </div>
                                    <div id="display_ubah_pembayaran_pelanggan_debit_or_kredit{{ $i }}" style="display:none">
                                        <select class="form-control select2" data-placeholder="Pilih transaksi" style="width:100%" name="debit_or_kredit">
                                            <option value=""></option>
                                            <option value="D" <?php if($val->debit_or_kredit == "D") echo 'selected';?>>Debit</option>
                                            <option value="K" <?php if($val->debit_or_kredit == "K") echo 'selected';?>>Kredit</option>
                                        </select>
                                    </div> 
                                </td>
                                <td align="center">
                                    <div id="pembayaran_pelanggan_btn_ubah{{ $i }}">
                                        <button class="btn btn-warning flat" type="button" onclick="ubah_pembayaran_pelanggan('{{ $i }}')" data-toggle="tooltip" title="ubah" style="font-size: 15pt"><span class="fa fa-edit"></span></button>
                                    </div>
                                    <div id="pembayaran_pelanggan_btn_simpan{{ $i }}" style="display:none">
                                        <button class="btn btn-primary flat" type="submit" onclick="document.getElementById('loading').style.display = 'block'" data-toggle="tooltip" title="simpan" style="font-size: 15pt"><span class="fa fa-floppy-o"></span></button>
                                    </div>
                                </td>
                                <td align="center">
                                    <div id="pembayaran_pelanggan_btn_hapus{{ $i }}">
                                        <button class="btn btn-danger flat" type="button" onclick="hapus_pembayaran_pelanggan('{{ $val->id }}')" data-toggle="tooltip" title="hapus" style="font-size: 15pt"><span class="fa fa-trash"></span></button>
                                    </div>
                                    <div id="pembayaran_pelanggan_btn_batal{{ $i }}" style="display:none">
                                        <button class="btn btn-default flat" type="button" onclick="batal_nota_beli('{{ $i }}')" data-toggle="tooltip" title="batal" style="font-size: 15pt"><span class="fa fa-ban"></span></button>
                                    </div>
                                </td>
                                </form>
                            </tr>
                            @endforeach
                            <tr>
                                <form method="post" action="{{ url('/tambah_pengaturan_jurnal') }}">
                                {{ csrf_field() }}
                                <td align="center"></td>
                                <td>
                                    <select class="form-control select2" data-placeholder="Pilih transaksi" style="width:100%" name="transaksi_id">
                                        <option value=""></option>
                                        @foreach($pembayaran_pelanggan as $val2)
                                        <option value="{{ $val2->id }}">{{ $val2->nama }}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control select2" data-placeholder="Pilih akun" style="width:100%" name="akun_id">
                                        <option value=""></option>
                                        <?php for($j = 0; $j < count($kode_akun); $j++) { ?>
                                        <option value="{{ $kode_akun[$j]['id'] }}">{{ $kode_akun[$j]['kode'] }} - {{ $kode_akun[$j]['nama'] }}</option>
                                        <?php } ?>
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control select2" data-placeholder="Pilih debit/kredit" style="width:100%" name="debit_or_kredit">
                                        <option value=""></option>
                                        <option value="D">Debit</option>
                                        <option value="K">Kredit</option>
                                    </select>
                                </td>
                                <td align="center">
                                    <button class="btn btn-primary flat" type="submit" onclick="document.getElementById('loading').style.display = 'block'" data-toggle="tooltip" title="simpan" style="font-size: 15pt"><span class="fa fa-floppy-o"></span></button>
                                </td>
                                <td></td>
                                </form>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="overlay" id="loading" style="display:none">
              <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>
<form action="{{ url('/hapus_pengaturan_jurnal') }}" method="post" id="form_hapus_pengaturan_jurnal">
    {{ csrf_field() }}
    <input type="hidden" name="id" id="id_hapus_pengaturan_jurnal">
</form>
@stop

<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/select2/select2.full.min.js') }}"></script>
<script>
    $(function () {
        $("#mydatatables").DataTable();
        $("#mydatatables2").DataTable();

        // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            // save the latest tab; use cookies if you like 'em better:
            localStorage.setItem('lastTab_pengaturan_jurnal', $(this).attr('href'));
        });

        // go to the latest tab, if it exists:
        var lastTab_pengaturan_jurnal = localStorage.getItem('lastTab_pengaturan_jurnal');
        if (lastTab_pengaturan_jurnal) {
            $('[href="' + lastTab_pengaturan_jurnal + '"]').tab('show');
        }
        else
        {
            lastTab_pengaturan_jurnal = "#tab_1";
            $('[href="' + lastTab_pengaturan_jurnal + '"]').tab('show');
        }

        $(".select2").select2();
    });

    function ubah_nota_beli(i)
    {
        document.getElementById('display_nota_beli_nama_transaksi'.concat(i)).style.display         = 'none';
        document.getElementById('display_ubah_nota_beli_nama_transaksi'.concat(i)).style.display    = 'block';
        document.getElementById('display_nota_beli_master_coa'.concat(i)).style.display             = 'none';
        document.getElementById('display_ubah_nota_beli_master_coa'.concat(i)).style.display        = 'block';
        document.getElementById('display_nota_beli_debit_or_kredit'.concat(i)).style.display        = 'none';
        document.getElementById('display_ubah_nota_beli_debit_or_kredit'.concat(i)).style.display   = 'block';
        document.getElementById('nota_beli_btn_ubah'.concat(i)).style.display                       = 'none';
        document.getElementById('nota_beli_btn_hapus'.concat(i)).style.display                      = 'none';
        document.getElementById('nota_beli_btn_simpan'.concat(i)).style.display                     = 'block';
        document.getElementById('nota_beli_btn_batal'.concat(i)).style.display                      = 'block';
    }

    function batal_nota_beli(i)
    {
        document.getElementById('display_nota_beli_nama_transaksi'.concat(i)).style.display         = 'block';
        document.getElementById('display_ubah_nota_beli_nama_transaksi'.concat(i)).style.display    = 'none';
        document.getElementById('display_nota_beli_master_coa'.concat(i)).style.display             = 'block';
        document.getElementById('display_ubah_nota_beli_master_coa'.concat(i)).style.display        = 'none';
        document.getElementById('display_nota_beli_debit_or_kredit'.concat(i)).style.display        = 'block';
        document.getElementById('display_ubah_nota_beli_debit_or_kredit'.concat(i)).style.display   = 'none';
        document.getElementById('nota_beli_btn_ubah'.concat(i)).style.display                       = 'block';
        document.getElementById('nota_beli_btn_hapus'.concat(i)).style.display                      = 'block';
        document.getElementById('nota_beli_btn_simpan'.concat(i)).style.display                     = 'none';
        document.getElementById('nota_beli_btn_batal'.concat(i)).style.display                      = 'none';
    }

    function hapus_nota_beli(id)
    {
        var cek = confirm("Apakah anda yakin untuk menghapus data?");

        if(cek)
        {
            document.getElementById('id_hapus_pengaturan_jurnal').value = id;
            document.getElementById('loading').style.display            = 'block';
            document.getElementById('form_hapus_pengaturan_jurnal').submit();
        }
    }

    function ubah_nota_jual(i)
    {
        document.getElementById('display_nota_jual_nama_transaksi'.concat(i)).style.display         = 'none';
        document.getElementById('display_ubah_nota_jual_nama_transaksi'.concat(i)).style.display    = 'block';
        document.getElementById('display_nota_jual_master_coa'.concat(i)).style.display             = 'none';
        document.getElementById('display_ubah_nota_jual_master_coa'.concat(i)).style.display        = 'block';
        document.getElementById('display_nota_jual_debit_or_kredit'.concat(i)).style.display        = 'none';
        document.getElementById('display_ubah_nota_jual_debit_or_kredit'.concat(i)).style.display   = 'block';
        document.getElementById('nota_jual_btn_ubah'.concat(i)).style.display                       = 'none';
        document.getElementById('nota_jual_btn_hapus'.concat(i)).style.display                      = 'none';
        document.getElementById('nota_jual_btn_simpan'.concat(i)).style.display                     = 'block';
        document.getElementById('nota_jual_btn_batal'.concat(i)).style.display                      = 'block';
    }

    function batal_nota_jual(i)
    {
        document.getElementById('display_nota_jual_nama_transaksi'.concat(i)).style.display         = 'block';
        document.getElementById('display_ubah_nota_jual_nama_transaksi'.concat(i)).style.display    = 'none';
        document.getElementById('display_nota_jual_master_coa'.concat(i)).style.display             = 'block';
        document.getElementById('display_ubah_nota_jual_master_coa'.concat(i)).style.display        = 'none';
        document.getElementById('display_nota_jual_debit_or_kredit'.concat(i)).style.display        = 'block';
        document.getElementById('display_ubah_nota_jual_debit_or_kredit'.concat(i)).style.display   = 'none';
        document.getElementById('nota_jual_btn_ubah'.concat(i)).style.display                       = 'block';
        document.getElementById('nota_jual_btn_hapus'.concat(i)).style.display                      = 'block';
        document.getElementById('nota_jual_btn_simpan'.concat(i)).style.display                     = 'none';
        document.getElementById('nota_jual_btn_batal'.concat(i)).style.display                      = 'none';
    }

    function hapus_nota_jual(id)
    {
        var cek = confirm("Apakah anda yakin untuk menghapus data?");

        if(cek)
        {
            document.getElementById('id_hapus_pengaturan_jurnal').value = id;
            document.getElementById('loading').style.display            = 'block';
            document.getElementById('form_hapus_pengaturan_jurnal').submit();
        }
    }

    function ubah_retur_beli(i)
    {
        document.getElementById('display_retur_beli_nama_transaksi'.concat(i)).style.display         = 'none';
        document.getElementById('display_ubah_retur_beli_nama_transaksi'.concat(i)).style.display    = 'block';
        document.getElementById('display_retur_beli_master_coa'.concat(i)).style.display             = 'none';
        document.getElementById('display_ubah_retur_beli_master_coa'.concat(i)).style.display        = 'block';
        document.getElementById('display_retur_beli_debit_or_kredit'.concat(i)).style.display        = 'none';
        document.getElementById('display_ubah_retur_beli_debit_or_kredit'.concat(i)).style.display   = 'block';
        document.getElementById('retur_beli_btn_ubah'.concat(i)).style.display                       = 'none';
        document.getElementById('retur_beli_btn_hapus'.concat(i)).style.display                      = 'none';
        document.getElementById('retur_beli_btn_simpan'.concat(i)).style.display                     = 'block';
        document.getElementById('retur_beli_btn_batal'.concat(i)).style.display                      = 'block';
    }

    function batal_retur_beli(i)
    {
        document.getElementById('display_retur_beli_nama_transaksi'.concat(i)).style.display         = 'block';
        document.getElementById('display_ubah_retur_beli_nama_transaksi'.concat(i)).style.display    = 'none';
        document.getElementById('display_retur_beli_master_coa'.concat(i)).style.display             = 'block';
        document.getElementById('display_ubah_retur_beli_master_coa'.concat(i)).style.display        = 'none';
        document.getElementById('display_retur_beli_debit_or_kredit'.concat(i)).style.display        = 'block';
        document.getElementById('display_ubah_retur_beli_debit_or_kredit'.concat(i)).style.display   = 'none';
        document.getElementById('retur_beli_btn_ubah'.concat(i)).style.display                       = 'block';
        document.getElementById('retur_beli_btn_hapus'.concat(i)).style.display                      = 'block';
        document.getElementById('retur_beli_btn_simpan'.concat(i)).style.display                     = 'none';
        document.getElementById('retur_beli_btn_batal'.concat(i)).style.display                      = 'none';
    }

    function hapus_retur_beli(id)
    {
        var cek = confirm("Apakah anda yakin untuk menghapus data?");

        if(cek)
        {
            document.getElementById('id_hapus_pengaturan_jurnal').value = id;
            document.getElementById('loading').style.display            = 'block';
            document.getElementById('form_hapus_pengaturan_jurnal').submit();
        }
    }

    function ubah_retur_jual(i)
    {
        document.getElementById('display_retur_jual_nama_transaksi'.concat(i)).style.display         = 'none';
        document.getElementById('display_ubah_retur_jual_nama_transaksi'.concat(i)).style.display    = 'block';
        document.getElementById('display_retur_jual_master_coa'.concat(i)).style.display             = 'none';
        document.getElementById('display_ubah_retur_jual_master_coa'.concat(i)).style.display        = 'block';
        document.getElementById('display_retur_jual_debit_or_kredit'.concat(i)).style.display        = 'none';
        document.getElementById('display_ubah_retur_jual_debit_or_kredit'.concat(i)).style.display   = 'block';
        document.getElementById('retur_jual_btn_ubah'.concat(i)).style.display                       = 'none';
        document.getElementById('retur_jual_btn_hapus'.concat(i)).style.display                      = 'none';
        document.getElementById('retur_jual_btn_simpan'.concat(i)).style.display                     = 'block';
        document.getElementById('retur_jual_btn_batal'.concat(i)).style.display                      = 'block';
    }

    function batal_retur_jual(i)
    {
        document.getElementById('display_retur_jual_nama_transaksi'.concat(i)).style.display         = 'block';
        document.getElementById('display_ubah_retur_jual_nama_transaksi'.concat(i)).style.display    = 'none';
        document.getElementById('display_retur_jual_master_coa'.concat(i)).style.display             = 'block';
        document.getElementById('display_ubah_retur_jual_master_coa'.concat(i)).style.display        = 'none';
        document.getElementById('display_retur_jual_debit_or_kredit'.concat(i)).style.display        = 'block';
        document.getElementById('display_ubah_retur_jual_debit_or_kredit'.concat(i)).style.display   = 'none';
        document.getElementById('retur_jual_btn_ubah'.concat(i)).style.display                       = 'block';
        document.getElementById('retur_jual_btn_hapus'.concat(i)).style.display                      = 'block';
        document.getElementById('retur_jual_btn_simpan'.concat(i)).style.display                     = 'none';
        document.getElementById('retur_jual_btn_batal'.concat(i)).style.display                      = 'none';
    }

    function hapus_retur_jual(id)
    {
        var cek = confirm("Apakah anda yakin untuk menghapus data?");

        if(cek)
        {
            document.getElementById('id_hapus_pengaturan_jurnal').value = id;
            document.getElementById('loading').style.display            = 'block';
            document.getElementById('form_hapus_pengaturan_jurnal').submit();
        }
    }

    function ubah_jasa_servis(i)
    {
        document.getElementById('display_jasa_servis_nama_transaksi'.concat(i)).style.display         = 'none';
        document.getElementById('display_ubah_jasa_servis_nama_transaksi'.concat(i)).style.display    = 'block';
        document.getElementById('display_jasa_servis_master_coa'.concat(i)).style.display             = 'none';
        document.getElementById('display_ubah_jasa_servis_master_coa'.concat(i)).style.display        = 'block';
        document.getElementById('display_jasa_servis_debit_or_kredit'.concat(i)).style.display        = 'none';
        document.getElementById('display_ubah_jasa_servis_debit_or_kredit'.concat(i)).style.display   = 'block';
        document.getElementById('jasa_servis_btn_ubah'.concat(i)).style.display                       = 'none';
        document.getElementById('jasa_servis_btn_hapus'.concat(i)).style.display                      = 'none';
        document.getElementById('jasa_servis_btn_simpan'.concat(i)).style.display                     = 'block';
        document.getElementById('jasa_servis_btn_batal'.concat(i)).style.display                      = 'block';
    }

    function batal_jasa_servis(i)
    {
        document.getElementById('display_jasa_servis_nama_transaksi'.concat(i)).style.display         = 'block';
        document.getElementById('display_ubah_jasa_servis_nama_transaksi'.concat(i)).style.display    = 'none';
        document.getElementById('display_jasa_servis_master_coa'.concat(i)).style.display             = 'block';
        document.getElementById('display_ubah_jasa_servis_master_coa'.concat(i)).style.display        = 'none';
        document.getElementById('display_jasa_servis_debit_or_kredit'.concat(i)).style.display        = 'block';
        document.getElementById('display_ubah_jasa_servis_debit_or_kredit'.concat(i)).style.display   = 'none';
        document.getElementById('jasa_servis_btn_ubah'.concat(i)).style.display                       = 'block';
        document.getElementById('jasa_servis_btn_hapus'.concat(i)).style.display                      = 'block';
        document.getElementById('jasa_servis_btn_simpan'.concat(i)).style.display                     = 'none';
        document.getElementById('jasa_servis_btn_batal'.concat(i)).style.display                      = 'none';
    }

    function hapus_jasa_servis(id)
    {
        var cek = confirm("Apakah anda yakin untuk menghapus data?");

        if(cek)
        {
            document.getElementById('id_hapus_pengaturan_jurnal').value = id;
            document.getElementById('loading').style.display            = 'block';
            document.getElementById('form_hapus_pengaturan_jurnal').submit();
        }
    }

    function ubah_pembayaran_supplier(i)
    {
        document.getElementById('display_pembayaran_supplier_nama_transaksi'.concat(i)).style.display         = 'none';
        document.getElementById('display_ubah_pembayaran_supplier_nama_transaksi'.concat(i)).style.display    = 'block';
        document.getElementById('display_pembayaran_supplier_master_coa'.concat(i)).style.display             = 'none';
        document.getElementById('display_ubah_pembayaran_supplier_master_coa'.concat(i)).style.display        = 'block';
        document.getElementById('display_pembayaran_supplier_debit_or_kredit'.concat(i)).style.display        = 'none';
        document.getElementById('display_ubah_pembayaran_supplier_debit_or_kredit'.concat(i)).style.display   = 'block';
        document.getElementById('pembayaran_supplier_btn_ubah'.concat(i)).style.display                       = 'none';
        document.getElementById('pembayaran_supplier_btn_hapus'.concat(i)).style.display                      = 'none';
        document.getElementById('pembayaran_supplier_btn_simpan'.concat(i)).style.display                     = 'block';
        document.getElementById('pembayaran_supplier_btn_batal'.concat(i)).style.display                      = 'block';
    }

    function batal_pembayaran_supplier(i)
    {
        document.getElementById('display_pembayaran_supplier_nama_transaksi'.concat(i)).style.display         = 'block';
        document.getElementById('display_ubah_pembayaran_supplier_nama_transaksi'.concat(i)).style.display    = 'none';
        document.getElementById('display_pembayaran_supplier_master_coa'.concat(i)).style.display             = 'block';
        document.getElementById('display_ubah_pembayaran_supplier_master_coa'.concat(i)).style.display        = 'none';
        document.getElementById('display_pembayaran_supplier_debit_or_kredit'.concat(i)).style.display        = 'block';
        document.getElementById('display_ubah_pembayaran_supplier_debit_or_kredit'.concat(i)).style.display   = 'none';
        document.getElementById('pembayaran_supplier_btn_ubah'.concat(i)).style.display                       = 'block';
        document.getElementById('pembayaran_supplier_btn_hapus'.concat(i)).style.display                      = 'block';
        document.getElementById('pembayaran_supplier_btn_simpan'.concat(i)).style.display                     = 'none';
        document.getElementById('pembayaran_supplier_btn_batal'.concat(i)).style.display                      = 'none';
    }

    function hapus_pembayaran_supplier(id)
    {
        var cek = confirm("Apakah anda yakin untuk menghapus data?");

        if(cek)
        {
            document.getElementById('id_hapus_pengaturan_jurnal').value = id;
            document.getElementById('loading').style.display            = 'block';
            document.getElementById('form_hapus_pengaturan_jurnal').submit();
        }
    }

    function ubah_pembayaran_pelanggan(i)
    {
        document.getElementById('display_pembayaran_pelanggan_nama_transaksi'.concat(i)).style.display         = 'none';
        document.getElementById('display_ubah_pembayaran_pelanggan_nama_transaksi'.concat(i)).style.display    = 'block';
        document.getElementById('display_pembayaran_pelanggan_master_coa'.concat(i)).style.display             = 'none';
        document.getElementById('display_ubah_pembayaran_pelanggan_master_coa'.concat(i)).style.display        = 'block';
        document.getElementById('display_pembayaran_pelanggan_debit_or_kredit'.concat(i)).style.display        = 'none';
        document.getElementById('display_ubah_pembayaran_pelanggan_debit_or_kredit'.concat(i)).style.display   = 'block';
        document.getElementById('pembayaran_pelanggan_btn_ubah'.concat(i)).style.display                       = 'none';
        document.getElementById('pembayaran_pelanggan_btn_hapus'.concat(i)).style.display                      = 'none';
        document.getElementById('pembayaran_pelanggan_btn_simpan'.concat(i)).style.display                     = 'block';
        document.getElementById('pembayaran_pelanggan_btn_batal'.concat(i)).style.display                      = 'block';
    }

    function batal_pembayaran_pelanggan(i)
    {
        document.getElementById('display_pembayaran_pelanggan_nama_transaksi'.concat(i)).style.display         = 'block';
        document.getElementById('display_ubah_pembayaran_pelanggan_nama_transaksi'.concat(i)).style.display    = 'none';
        document.getElementById('display_pembayaran_pelanggan_master_coa'.concat(i)).style.display             = 'block';
        document.getElementById('display_ubah_pembayaran_pelanggan_master_coa'.concat(i)).style.display        = 'none';
        document.getElementById('display_pembayaran_pelanggan_debit_or_kredit'.concat(i)).style.display        = 'block';
        document.getElementById('display_ubah_pembayaran_pelanggan_debit_or_kredit'.concat(i)).style.display   = 'none';
        document.getElementById('pembayaran_pelanggan_btn_ubah'.concat(i)).style.display                       = 'block';
        document.getElementById('pembayaran_pelanggan_btn_hapus'.concat(i)).style.display                      = 'block';
        document.getElementById('pembayaran_pelanggan_btn_simpan'.concat(i)).style.display                     = 'none';
        document.getElementById('pembayaran_pelanggan_btn_batal'.concat(i)).style.display                      = 'none';
    }

    function hapus_pembayaran_pelanggan(id)
    {
        var cek = confirm("Apakah anda yakin untuk menghapus data?");

        if(cek)
        {
            document.getElementById('id_hapus_pengaturan_jurnal').value = id;
            document.getElementById('loading').style.display            = 'block';
            document.getElementById('form_hapus_pengaturan_jurnal').submit();
        }
    }

    function ubah_transaksi_jasa(i)
    {
        document.getElementById('display_transaksi_jasa_nama_transaksi'.concat(i)).style.display         = 'none';
        document.getElementById('display_ubah_transaksi_jasa_nama_transaksi'.concat(i)).style.display    = 'block';
        document.getElementById('display_transaksi_jasa_master_coa'.concat(i)).style.display             = 'none';
        document.getElementById('display_ubah_transaksi_jasa_master_coa'.concat(i)).style.display        = 'block';
        document.getElementById('display_transaksi_jasa_debit_or_kredit'.concat(i)).style.display        = 'none';
        document.getElementById('display_ubah_transaksi_jasa_debit_or_kredit'.concat(i)).style.display   = 'block';
        document.getElementById('transaksi_jasa_btn_ubah'.concat(i)).style.display                       = 'none';
        document.getElementById('transaksi_jasa_btn_hapus'.concat(i)).style.display                      = 'none';
        document.getElementById('transaksi_jasa_btn_simpan'.concat(i)).style.display                     = 'block';
        document.getElementById('transaksi_jasa_btn_batal'.concat(i)).style.display                      = 'block';
    }

    function batal_transaksi_jasa(i)
    {
        document.getElementById('display_transaksi_jasa_nama_transaksi'.concat(i)).style.display         = 'block';
        document.getElementById('display_ubah_transaksi_jasa_nama_transaksi'.concat(i)).style.display    = 'none';
        document.getElementById('display_transaksi_jasa_master_coa'.concat(i)).style.display             = 'block';
        document.getElementById('display_ubah_transaksi_jasa_master_coa'.concat(i)).style.display        = 'none';
        document.getElementById('display_transaksi_jasa_debit_or_kredit'.concat(i)).style.display        = 'block';
        document.getElementById('display_ubah_transaksi_jasa_debit_or_kredit'.concat(i)).style.display   = 'none';
        document.getElementById('transaksi_jasa_btn_ubah'.concat(i)).style.display                       = 'block';
        document.getElementById('transaksi_jasa_btn_hapus'.concat(i)).style.display                      = 'block';
        document.getElementById('transaksi_jasa_btn_simpan'.concat(i)).style.display                     = 'none';
        document.getElementById('transaksi_jasa_btn_batal'.concat(i)).style.display                      = 'none';
    }

    function hapus_transaksi_jasa(id)
    {
        var cek = confirm("Apakah anda yakin untuk menghapus data?");

        if(cek)
        {
            document.getElementById('id_hapus_pengaturan_jurnal').value = id;
            document.getElementById('loading').style.display            = 'block';
            document.getElementById('form_hapus_pengaturan_jurnal').submit();
        }
    }
</script>