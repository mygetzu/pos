<link rel="stylesheet" href="{{ asset('/adminlte/plugins/select2/select2.min.css') }}">
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<?php $i=1; ?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <div class="pull-right">
                    <a href="{{ url('/master_coa') }}">
                        <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                    </a>
                </div>
            </div>
            <div class="box-body">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <form action="{{ url('/do_tambah_master_coa') }}" method="post" autocomplete="off">
                {{ csrf_field() }}
                <table class="table table-striped">
                    <br>
                    <tr class="{{ $errors->has('level') ? ' has-error' : '' }}">
                        <th><label class="control-label">Level</label></th>
                        <th>:</th>
                        <td>
                        <div class="col-md-12">
                            <select class="form-control" name="level" id="level">
                                <?php for ($i=$max_level; $i >= 0 ; $i--) { ?>
                                    <option value="{{ $i }}">{{ $i }}</option>
                                <?php } ?>
                            </select>
                            @if ($errors->has('level'))
                            <span class="help-block">
                                <strong>{{ $errors->first('level') }}</strong>
                            </span>
                        @endif
                        </div>
                        </td>
                    </tr>
                    <tr class="{{ $errors->has('parent') ? ' has-error' : '' }}">
                        <th><label class="control-label">Parent</label></th>
                        <th>:</th>
                        <td>
                        <div class="col-md-12">
                            <select class="form-control select2" id="parent" " style="width:100%" name="parent">
                                <option value="-">-- Pilih Parent --</option>
                                @foreach($parents as $val)
                                <option value="{{ $val->kode }}">{{ $val->nama }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('parent'))
                            <span class="help-block">
                                <strong>{{ $errors->first('parent') }}</strong>
                            </span>
                        @endif
                        </div>
                        </td>
                    </tr>
                    <tr class="{{ $errors->has('kode') ? ' has-error' : '' }}">
                        <th width="20%"><label class="control-label">Kode</label></th>
                        <th width="5%">:</th>
                        <td>
                            <div class="col-md-12">
                                <input type="hidden" name="kode" id="kode" class="form-control" value="@if($kode_awal != '0'){{ $kode_awal }}@endif">
                                <input type="text" id="kode2" class="form-control" disabled="true" value="@if($kode_awal != '0'){{ $kode_awal }}@endif">
                                @if ($errors->has('kode'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('kode') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </td>
                    </tr>
                    <tr class="{{ $errors->has('nama') ? ' has-error' : '' }}">
                        <th><label class="control-label">Nama</label></th>
                        <th>:</th>
                        <td>
                        <div class="col-md-12">
                            <input type="text" name="nama" id="nama" class="form-control">
                            @if ($errors->has('nama'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('nama') }}</strong>
                                </span>
                            @endif
                        </div>
                        </td>
                    </tr>
                    <tr class="{{ $errors->has('debit_or_kredit') ? ' has-error' : '' }}">
                        <th><label class="control-label">Debit/Kredit</label></th>
                        <th>:</th>
                        <td>
                        <div class="col-md-12">
                            <select class="form-control" name="debit_or_kredit">
                                <option value="">-- Pilih Debit/Kredit --</option>
                                <option value="D">Debit</option>
                                <option value="K">Kredit</option>
                            </select>
                            @if ($errors->has('debit_or_kredit'))
                                <span class="help-block">
                                    <strong>Pilihan debit/kredit wajib diisi</strong>
                                </span>
                            @endif
                        </div>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <th></th>
                        <td>
                            <div class="col-md-12">
                                <button class="btn btn-primary flat" type="submit" onclick="document.getElementById('loading').style.display = 'block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
                            </div>
                        </div>
                        </td>
                    </tr>
                </table>
                </form>
                <p>Keterangan (*) : Wajib Diisi</p>
                <br>
            </div>
            <div class="overlay" id="loading" style="display:none">
              <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>
@stop

<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/select2/select2.full.min.js') }}"></script>
<script type="text/javascript">
    $(function () {
        $(".select2").select2();
    });
    $(document).ready(function(){
        $('#level').change(function(){ 
            var level = document.getElementById("level").value;

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            var url = "{{ url('/get_parent') }}";

              $.ajax({
                type: "POST",
                url: url,
                data: { level:level },
                success: function (data) {
                    $("#parent").html(data);
                    if(level != '0')
                    {
                        document.getElementById('kode').value = "";
                        document.getElementById('kode2').value = "";
                    }
                    
                },
                error: function (data) {
                    alert('terjadi kesalahan');
                }
            });

            if(level == '0')
            { 
                var url = "{{ url('/get_count_child') }}";

                //cek berapa child yang sudah ada
                $.ajax({
                    type: "POST",
                    url: url,
                    data: { parent:'-' },
                    success: function (data) {
                        data = parseInt(data);
                        data++;

                        if(data < 10);
                        {
                            data = "00"+data;
                        }
                        
                        if(data < 100 && data > 10)
                        {
                            data = "0"+data;
                        }

                        document.getElementById('kode').value = data;
                        document.getElementById('kode2').value = data;
                    },
                    error: function (data) {
                        alert('terjadi kesalahan');
                    }
                });
            }
        });
        $('#parent').change(function(){
            var parent = document.getElementById("parent").value;

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            var url = "{{ url('/get_count_child') }}";

            //cek berapa child yang sudah ada
            $.ajax({
                type: "POST",
                url: url,
                data: { parent:parent },
                success: function (data) {
                    data = parseInt(data);
                    data++;
                    if(data < 10);
                    {
                        data = "00"+data;
                    }

                    if(data < 100 && data > 10)
                        {
                            data = "0"+data;
                        }

                    if(parent == '-')
                    {
                        document.getElementById('kode').value = "";
                        document.getElementById('kode2').value = "";
                    }
                    else
                    {
                        document.getElementById('kode').value = parent+"."+data;
                        document.getElementById('kode2').value = parent+"."+data;
                    }
                },
                error: function (data) {
                    alert('terjadi kesalahan');
                }
            });
        });
    });
</script>