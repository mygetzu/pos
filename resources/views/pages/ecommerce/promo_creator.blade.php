<link rel="stylesheet" href="{{ asset('/adminlte/plugins/select2/select2.min.css') }}">
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<style type="text/css">
    .select2-dropdown .select2-search__field:focus, .select2-search--inline .select2-search__field:focus {
        outline: none;
        border: 0px;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <div class="nav-tabs-custom box tab-primary" style="border-top: 0">
            <ul class="nav nav-tabs" id="myTab">
                <li><a href="#tab_1" data-toggle="tab">Slider Promo</a></li>
                <li><a href="#tab_2" data-toggle="tab">Mini Banner</a></li>
                <li><a href="#tab_3" data-toggle="tab">Produk Pilihan</a></li>
                <li><a href="#tab_4" data-toggle="tab">Produk Baru</a></li>
                <li><a href="#tab_5" data-toggle="tab">Produk Akan Datang</a></li>
                <li><a href="#tab_6" data-toggle="tab">Paket Pilihan</a></li>
                <!-- <li><a href="#tab_7" data-toggle="tab">Banner Tambahan</a></li> -->
            </ul>
            <div class="tab-content">
                <div class="tab-pane" id="tab_1">
                    <a data-toggle="modal" data-target="#tambah_slider_promo">
                        <button class='btn bg-purple flat'><span class='fa fa-plus-circle'></span> Tambah Slider Promo</button>
                    </a>
                    <br><br>
                    @if(Session::has('message1'))
                    <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                        <i class="fa fa-check"></i>
                        {{ Session::get('message1') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                    <input type="hidden" id="jumlah_slider_promo" value="{{ count($slider_promo) }}">
                    <table class="table table-striped table-bordered" id="tabel_slider">
                        <thead>
                            <tr>
                                <th style="text-align: center">NO</th>
                                <th style="text-align: center">NAMA</th>
                                <th style="text-align: center">BANNER</th>
                                <th style="text-align: center" width="35%">LIST PRODUK</th>
                                <th style="text-align: center"></th>
                                <th style="text-align: center"></th>
                            </tr>
                        </thead>
                        <tbody class="sorted_table">
                            @for($i = 0; $i < count($slider_promo); $i++)
                            <tr>
                                <td align="center" style="cursor: move;">{{ $slider_promo[$i]['item_order'] }}</td>
                                <td style="cursor: move;">
                                    {{ $slider_promo[$i]['nama'] }}
                                    <input type="text" name="nama" id="slider_promo_nama{{ $i }}" class="form-control" value="{{ $slider_promo[$i]['nama'] }}" style="display: none;">
                                </td>
                                <td>
                                    <?php $file_gambar = $slider_promo[$i]['file_gambar']; ?>
                                    <img id="{{ $slider_promo[$i]['id'] }}" src="{{ URL::asset('img/banner/'.$file_gambar) }}" width="300px">
                                    <input type="file" name="file_gambar"  id="slider_promo_file_gambar{{ $i }}"class="form-control" style="display: none;">
                                </td>
                                <td>
                                    <ul>
                                        @for($j = 0; $j < count($slider_promo[$i]['list_produk']); $j++)
                                            <li>{{ $slider_promo[$i]['list_produk'][$j]['nama'] }}</li>
                                        @endfor
                                    </ul>
                                </td>
                                <td>
                                    <button class="btn btn-warning flat"><span class="fa fa-edit"></span> Ubah</button>
                                </td>
                                <td>
                                    <?php $slider_promo_id = $slider_promo[$i]['id']; ?>
                                    <button class="btn btn-danger flat" onclick="hapus_slider_promo('{{ $slider_promo_id }}')"><span class="fa fa-trash"></span> Hapus</button>
                                </td>
                            </tr>
                            @endfor
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="tab_2">
                    @if(Session::has('message2'))
                    <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                        <i class="fa fa-check"></i>
                        {{ Session::get('message2') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                    <input type="hidden" id="jumlah_slider_promo" value="{{ count($slider_promo) }}">
                    <table class="table table-striped table-bordered" id="tabel_slider">
                        <thead>
                            <tr>
                                <th style="text-align: center">NO</th>
                                <th style="text-align: center">NAMA</th>
                                <th style="text-align: center">BANNER</th>
                                <th style="text-align: center"></th>
                                <th style="text-align: center"></th>
                            </tr>
                        </thead>
                        <tbody class="sorted_table">
                            <tr>
                                <form method="post" action="{{ url('/simpan_mini_banner') }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <td align="center">1<input type="hidden" class="form-control" name="id" value="@if(!empty($mini_banner[0]['id'])){{ $mini_banner[0]['id'] }}@endif"></td>
                                <td>
                                    <p id="mini_banner_display_nama1">@if(!empty($mini_banner[0]['nama'])){{ $mini_banner[0]['nama'] }}@endif</p>
                                    <input type="text" class="form-control" name="nama" id="mini_banner_nama1" value="@if(!empty($mini_banner[0]['nama'])){{ $mini_banner[0]['nama'] }}@endif" style="display: none; text-transform: capitalize;">
                                </td>
                                <td align="center">
                                    <?php if(!empty($mini_banner[0]['file_gambar'])){ 
                                        $file_gambar = $mini_banner[0]['file_gambar']; 
                                        } else { $file_gambar = ""; } ?>
                                    <img src="{{ URL::asset('img/banner/'.$file_gambar) }}" height="100px"><br><br>
                                    <input type="file" class="form-control" name="file_gambar" id="mini_banner_file_gambar1" value="" style="display: none">
                                </td>
                                <td align="center">
                                    <button type="button" class="btn btn-warning flat" onclick="ubah_mini_banner(1)" id="button_ubah_mini_banner1"><span class="fa fa-edit"></span> Ubah</button>
                                    <button type="submit" class="btn btn-primary flat" id="button_simpan_mini_banner1" style="display: none;" onclick="document.getElementById('loading').style.display = 'block'"><span class="fa fa-edit"></span> Simpan</button>
                                </td>
                                </form>
                                <td align="center">
                                    <button type="submit" class="btn btn-default flat" onclick="batal_mini_banner(1)" id="button_batal_mini_banner1" style="display: none;"><span class="fa fa-ban"></span> Batal</button>
                                    <form method="post" action="{{ url('/hapus_mini_banner') }}" id="form_hapus_mini_banner1">
                                    {{ csrf_field() }}
                                    <input type="hidden" class="form-control" name="id" value="@if(!empty($mini_banner[0]['id'])) {{ $mini_banner[0]['id'] }} @endif">
                                    <button type="button" class="btn btn-danger flat" onclick="hapus_mini_banner(1)" id="button_hapus_mini_banner1"><span class="fa fa-trash"></span> Hapus</button>
                                    </form>
                                </td>
                            </tr>
                            <tr>
                                <form method="post" action="{{ url('/simpan_mini_banner') }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <td align="center">2<input type="hidden" class="form-control" name="id" value="@if(!empty($mini_banner[1]['id'])){{ $mini_banner[1]['id'] }}@endif"></td>
                                <td>
                                    <p id="mini_banner_display_nama2">@if(!empty($mini_banner[1]['nama'])){{ $mini_banner[1]['nama'] }}@endif</p>
                                    <input type="text" class="form-control" name="nama" id="mini_banner_nama2" value="@if(!empty($mini_banner[1]['nama'])){{ $mini_banner[1]['nama'] }}@endif" style="display: none; text-transform: capitalize;">
                                </td>
                                <td align="center">
                                    <?php if(!empty($mini_banner[1]['file_gambar'])){ 
                                        $file_gambar = $mini_banner[1]['file_gambar']; 
                                        } else { $file_gambar = ""; } ?>
                                    <img src="{{ URL::asset('img/banner/'.$file_gambar) }}" height="100px"><br><br>
                                    <input type="file" class="form-control" name="file_gambar" id="mini_banner_file_gambar2" value="" style="display: none">
                                </td>
                                <td align="center">
                                    <button type="button" class="btn btn-warning flat" onclick="ubah_mini_banner(2)" id="button_ubah_mini_banner2"><span class="fa fa-edit"></span> Ubah</button>
                                    <button type="submit" class="btn btn-primary flat" id="button_simpan_mini_banner2" style="display: none;" onclick="document.getElementById('loading').style.display = 'block'"><span class="fa fa-edit"></span> Simpan</button>
                                </td>
                                </form>
                                <td align="center">
                                    <button type="submit" class="btn btn-default flat" onclick="batal_mini_banner(2)" id="button_batal_mini_banner2" style="display: none;"><span class="fa fa-ban"></span> Batal</button>
                                    <form method="post" action="{{ url('/hapus_mini_banner') }}" id="form_hapus_mini_banner2">
                                    {{ csrf_field() }}
                                    <input type="hidden" class="form-control" name="id" value="@if(!empty($mini_banner[1]['id'])) {{ $mini_banner[1]['id'] }} @endif">
                                    <button type="button" class="btn btn-danger flat" onclick="hapus_mini_banner(2)" id="button_hapus_mini_banner2"><span class="fa fa-trash"></span> Hapus</button>
                                    </form>
                                </td>
                            </tr>
                            <tr>
                                <form method="post" action="{{ url('/simpan_mini_banner') }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <td align="center">3<input type="hidden" class="form-control" name="id" value="@if(!empty($mini_banner[2]['id'])){{ $mini_banner[2]['id'] }}@endif"></td>
                                <td>
                                    <p id="mini_banner_display_nama3">@if(!empty($mini_banner[2]['nama'])){{ $mini_banner[2]['nama'] }}@endif</p>
                                    <input type="text" class="form-control" name="nama" id="mini_banner_nama3" value="@if(!empty($mini_banner[2]['nama'])){{ $mini_banner[2]['nama'] }}@endif" style="display: none; text-transform: capitalize;">
                                </td>
                                <td align="center">
                                    <?php if(!empty($mini_banner[2]['file_gambar'])){ 
                                        $file_gambar = $mini_banner[2]['file_gambar']; 
                                        } else { $file_gambar = ""; } ?>
                                    <img src="{{ URL::asset('img/banner/'.$file_gambar) }}" height="100px"><br><br>
                                    <input type="file" class="form-control" name="file_gambar" id="mini_banner_file_gambar3" value="" style="display: none">
                                </td>
                                <td align="center">
                                    <button type="button" class="btn btn-warning flat" onclick="ubah_mini_banner(3)" id="button_ubah_mini_banner3"><span class="fa fa-edit"></span> Ubah</button>
                                    <button type="submit" class="btn btn-primary flat" id="button_simpan_mini_banner3" style="display: none;" onclick="document.getElementById('loading').style.display = 'block'"><span class="fa fa-edit"></span> Simpan</button>
                                </td>
                                </form>
                                <td align="center">
                                    <button type="submit" class="btn btn-default flat" onclick="batal_mini_banner(3)" id="button_batal_mini_banner3" style="display: none;"><span class="fa fa-ban"></span> Batal</button>
                                    <form method="post" action="{{ url('/hapus_mini_banner') }}" id="form_hapus_mini_banner3">
                                    {{ csrf_field() }}
                                    <input type="hidden" class="form-control" name="id" value="@if(!empty($mini_banner[2]['id'])) {{ $mini_banner[2]['id'] }} @endif">
                                    <button type="button" class="btn btn-danger flat" onclick="hapus_mini_banner(3)" id="button_hapus_mini_banner3"><span class="fa fa-trash"></span> Hapus</button>
                                    </form>
                                </td>
                            </tr>
                            <tr>
                                <form method="post" action="{{ url('/simpan_mini_banner') }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <td align="center">4<input type="hidden" class="form-control" name="id" value="@if(!empty($mini_banner[3]['id'])){{ $mini_banner[3]['id'] }}@endif"></td>
                                <td>
                                    <p id="mini_banner_display_nama4">@if(!empty($mini_banner[3]['nama'])){{ $mini_banner[3]['nama'] }}@endif</p>
                                    <input type="text" class="form-control" name="nama" id="mini_banner_nama4" value="@if(!empty($mini_banner[3]['nama'])){{ $mini_banner[3]['nama'] }}@endif" style="display: none; text-transform: capitalize;">
                                </td>
                                <td align="center">
                                    <?php if(!empty($mini_banner[3]['file_gambar'])){ 
                                        $file_gambar = $mini_banner[3]['file_gambar']; 
                                        } else { $file_gambar = ""; } ?>
                                    <img src="{{ URL::asset('img/banner/'.$file_gambar) }}" height="100px"><br><br>
                                    <input type="file" class="form-control" name="file_gambar" id="mini_banner_file_gambar4" value="" style="display: none">
                                </td>
                                <td align="center">
                                    <button type="button" class="btn btn-warning flat" onclick="ubah_mini_banner(4)" id="button_ubah_mini_banner4"><span class="fa fa-edit"></span> Ubah</button>
                                    <button type="submit" class="btn btn-primary flat" id="button_simpan_mini_banner4" style="display: none;" onclick="document.getElementById('loading').style.display = 'block'"><span class="fa fa-edit"></span> Simpan</button>
                                </td>
                                </form>
                                <td align="center">
                                    <button type="submit" class="btn btn-default flat" onclick="batal_mini_banner(4)" id="button_batal_mini_banner4" style="display: none;"><span class="fa fa-ban"></span> Batal</button>
                                    <form method="post" action="{{ url('/hapus_mini_banner') }}" id="form_hapus_mini_banner4">
                                    {{ csrf_field() }}
                                    <input type="hidden" class="form-control" name="id" value="@if(!empty($mini_banner[3]['id'])) {{ $mini_banner[3]['id'] }} @endif">
                                    <button type="button" class="btn btn-danger flat" onclick="hapus_mini_banner(4)" id="button_hapus_mini_banner4"><span class="fa fa-trash"></span> Hapus</button>
                                    </form>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="tab_3">
                    <a data-toggle="modal" data-target="#tambah_produk_pilihan">
                        <button class='btn bg-purple flat'><span class='fa fa-plus-circle'></span> Tambah Produk Pilihan</button>
                    </a>
                    <br><br>
                    @if(Session::has('message3'))
                        <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                          <i class="fa fa-check"></i>
                          {{ Session::get('message3') }}
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                    @endif
                    <input type="hidden" id="jumlah_slider_promo" value="{{ count($produk_pilihan) }}">
                    <table class="table table-striped table-bordered" id="mydatatables">
                        <thead>
                            <tr>
                                <th style="text-align: center">NO</th>
                                <th style="text-align: center">NAMA</th>
                                <th style="text-align: center">BANNER</th>
                                <th style="text-align: center" width="40%">LIST PRODUK</th>
                                <th style="text-align: center"></th>
                                <th style="text-align: center"></th>
                            </tr>
                        </thead>
                        <tbody class="sorted_table">
                            @for($i = 0; $i < count($produk_pilihan); $i++)
                            <tr>
                                <td align="center">{{ $produk_pilihan[$i]['item_order'] }}</td>
                                <td style="cursor: move;">{{ $produk_pilihan[$i]['nama'] }}</td>
                                <td>
                                    <?php $file_gambar = $produk_pilihan[$i]['file_gambar']; ?>
                                    <img id="{{ $produk_pilihan[$i]['id'] }}" src="{{ URL::asset('img/banner/'.$file_gambar) }}" width="150px">
                                </td>
                                <td>
                                    <ul>
                                        @for($j = 0; $j < count($produk_pilihan[$i]['list_produk']); $j++)
                                            <li>{{ $produk_pilihan[$i]['list_produk'][$j]['nama'] }}</li>
                                        @endfor
                                    </ul>
                                </td>
                                <td align="center">
                                    <?php $produk_pilihan_id = $produk_pilihan[$i]['id']; ?>
                                    <button class="btn btn-warning flat"><span class="fa fa-edit"></span> Ubah</button>
                                </td>
                                <td align="center">
                                    <?php $produk_pilihan_id = $produk_pilihan[$i]['id']; ?>
                                    <button class="btn btn-danger flat" onclick="hapus_produk_pilihan('{{ $produk_pilihan_id }}')"><span class="fa fa-trash"></span> Hapus</button>
                                </td>
                            </tr>
                            @endfor
                        </tbody>
                    </table>
                </div> 
                <div class="tab-pane" id="tab_4">
                    <a data-toggle="modal" data-target="#tambah_data_produk_baru">
                        <button class='btn bg-purple flat'><span class='fa fa-plus-circle'></span> Tambah Data Produk Baru</button>
                    </a>
                    <br><br>
                    @if(Session::has('message4'))
                        <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                          <i class="fa fa-check"></i>
                          {{ Session::get('message4') }}
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                    @endif
                    <table id="mydatatables2" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th style="text-align:center">NO</th>
                                <th style="text-align:center">NAMA</th>
                                <th style="text-align:center">TANGGAL INPUT</th>
                                <th style="text-align:center"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=1; ?>
                            @foreach($produk_baru as $val)
                            <tr>
                                <td align="center">{{ $i++ }}</td>
                                <td>{{ $val->nama }}</td>
                                <td>{{ $val->tanggal_input }}</td>
                                <td align="center">
                                    <button class="btn btn-danger flat" onclick="hapus_data_produk_baru('{{ $val->produk_id }}')" data-toggle="tooltip" title="hapus" style="font-size: 15pt"><span class="fa fa-trash"></span></button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="tab_5">
                    <a data-toggle="modal" data-target="#tambah_data_produk_akan_datang">
                        <button class='btn bg-purple flat'><span class='fa fa-plus-circle'></span> Tambah Data Produk Akan Datang</button>
                    </a>
                    <br><br>
                    @if(Session::has('message5'))
                        <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                          <i class="fa fa-check"></i>
                          {{ Session::get('message5') }}
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                    @endif
                    <table id="mydatatables3" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th style="text-align:center">NO</th>
                                <th style="text-align:center">NAMA</th>
                                <th style="text-align:center">TANGGAL INPUT</th>
                                <th style="text-align:center"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=1; ?>
                            @foreach($produk_akan_datang as $val)
                            <tr>
                                <td align="center">{{ $i++ }}</td>
                                <td>{{ $val->nama }}</td>
                                <td>{{ $val->tanggal_input }}</td>
                                <td align="center">
                                    <button class="btn btn-danger flat" onclick="hapus_data_produk_akan_datang('{{ $val->produk_id }}')" data-toggle="tooltip" title="hapus" style="font-size: 15pt"><span class="fa fa-trash"></span></button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div> 
                <div class="tab-pane" id="tab_6">
                    <a data-toggle="modal" data-target="#tambah_paket_pilihan">
                        <button class='btn bg-purple flat'><span class='fa fa-plus-circle'></span> Tambah Paket Pilihan</button>
                    </a>
                    <br><br>
                    @if(Session::has('message6'))
                        <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                          <i class="fa fa-check"></i>
                          {{ Session::get('message6') }}
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                    @endif
                    <input type="hidden" id="jumlah_slider_promo" value="{{ count($paket_pilihan) }}">
                    <table class="table table-striped table-bordered" id="mydatatables5">
                        <thead>
                            <tr>
                                <th style="text-align: center">NO</th>
                                <th style="text-align: center">NAMA</th>
                                <th style="text-align: center">BANNER</th>
                                <th style="text-align: center" width="40%">LIST PAKET</th>
                                <th style="text-align: center"></th>
                                <th style="text-align: center"></th>
                            </tr>
                        </thead>
                        <tbody class="sorted_table">
                            @for($i = 0; $i < count($paket_pilihan); $i++)
                            <tr>
                                <td align="center">{{ $paket_pilihan[$i]['item_order'] }}</td>
                                <td style="cursor: move;">{{ $paket_pilihan[$i]['nama'] }}</td>
                                <td>
                                    <?php $file_gambar = $paket_pilihan[$i]['file_gambar']; ?>
                                    <img id="{{ $paket_pilihan[$i]['id'] }}" src="{{ URL::asset('img/banner/'.$file_gambar) }}" width="150px">
                                </td>
                                <td>
                                    <ul>
                                        @for($j = 0; $j < count($paket_pilihan[$i]['list_paket']); $j++)
                                            <li>{{ $paket_pilihan[$i]['list_paket'][$j]['nama'] }}</li>
                                        @endfor
                                    </ul>
                                </td>
                                <td align="center">
                                    <?php $paket_pilihan_id = $paket_pilihan[$i]['id']; ?>
                                    <button class="btn btn-warning flat"><span class="fa fa-edit"></span> Ubah</button>
                                </td>
                                <td align="center">
                                    <?php $paket_pilihan_id = $paket_pilihan[$i]['id']; ?>
                                    <button class="btn btn-danger flat" onclick="hapus_paket_pilihan('{{ $paket_pilihan_id }}')"><span class="fa fa-trash"></span> Hapus</button>
                                </td>
                            </tr>
                            @endfor
                        </tbody>
                    </table>
                </div>
                <!-- <div class="tab-pane" id="tab_7">
                    <table class="table">
                        <thead>
                            <tr>
                                <th style="text-align: center;">NAMA</th>
                                <th style="text-align: center;">GAMBAR</th>
                                <th style="text-align: center;"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <form method="post" action="{{ url('/banner_tambahan/1') }}" enctype="multipart/form-data">
                                <td>
                                    Banner Beranda (1200 pixel x 100 pixel)
                                    <p style="font-size: 10pt;">diletakkan pada hamana beranda, ketika diklik akan menampilkan semua produk</p>
                                </td>
                                <td>
                                    <img src="">
                                    <input type="file" name="file_gambar" id="file_gambar1" style="display: none;" class="form-control">
                                </td>
                                <td>
                                    <button class="btn btn-warning flat"><span class="fa fa-edit"></span> Ubah</button>
                                    <button class="btn btn-primary flat"><span class="fa fa-edit"></span> Simpan</button>
                                </td>
                                </form>
                            </tr>
                        </tbody>
                    </table>
                </div>    -->         
            </div>
            <div class="overlay" id="loading" style="display:none">
              <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<form method="post" action="{{ url('/hapus_slider_promo') }}" id="form_hapus_slider_promo">
    {{ csrf_field() }}
    <input type="hidden" name="id" id="hapus_slider_promo_id">
</form>

<div class="modal fade" id="tambah_slider_promo" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content box">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah Slider Promo</h4>
            </div>
            <form class="form-horizontal" method="post" action="{{ url('/tambah_slider_promo') }}" autocomplete="off"  enctype="multipart/form-data">
            <div class="modal-body">
                {{ csrf_field() }}
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="nama" style="text-transform: capitalize;">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Banner (1200 pixel x 400 pixel)</label>
                    <div class="col-sm-10">
                        <input type="file" class="form-control" name="file_gambar">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">List Produk</label>
                    <div class="col-sm-10">
                        <select class="form-control select2" multiple="multiple" name="list_produk[]" style="width: 100%">
                            @foreach($all_produk as $value)
                                @if($value->produk != '[]')
                                <optgroup label="{{ $value->nama }}">
                                @endif
                                @foreach($value->produk as $value2)
                                    <option value="{{ $value2->id }}">{{ $value2->nama }}</option>
                                @endforeach
                                @if($value->produk != '[]')
                                </optgroup>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <p>Keterangan (*) : Wajib Diisi</p>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default flat pull-left"><span class="glyphicon glyphicon-ban-circle"></span> Batal</button>
                <button type="submit" class="btn btn-primary flat" onclick="document.getElementById('loading2').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
            </div>
            </form>
            <div class="overlay" id="loading2" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_tambah_list_produk" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content box">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="riwayatheader">Tambah List Produk</h4>
            </div>
            <div class="modal-body form-horizontal">
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Kode*</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="kode" placeholder="Masukkan Kode Bank">
                    </div>
                </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Nama*</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="nama" placeholder="Masukkan Nama Bank">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Nama Singkat*</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="nama_singkat" placeholder="Masukkan Nama Singkat Bank">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Keterangan</label>
                        <div class="col-sm-10">
                            <textarea type="text" class="form-control" name="keterangan" placeholder="Masukkan Keterangan"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Gambar</label>
                        <div class="col-sm-10">
                            <input type="file" name="file_gambar" class="form-control">
                        </div>
                    </div>
                <p>Keterangan (*) : Wajib Diisi</p>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default pull-left flat"><span class="glyphicon glyphicon-ban-circle"></span> Batal</button>
                <button type="submit" class="btn btn-primary pull-right flat" onclick="document.getElementById('loading3').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
            </div>
            </form>
            <div class="overlay" id="loading3" style="display:none">
                  <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="tambah_data_produk_baru" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content box">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="riwayatheader">Tambah Data Produk Baru</h4>
            </div>
            <div class="modal-body">
                <div class="box-body">
                    <table id="mydatatables4" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th style="text-align:center">NO</th>
                                <th style="text-align:center">NAMA</th>
                                <th style="text-align:center"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=1; ?>
                            @foreach($produk as $val)
                            <tr>
                                <form class="form-horizontal" method="post" action="{{ url('/tambah_data_produk_baru') }}" autocomplete="off">
                                 {{ csrf_field() }}
                                <input type="hidden" name="produk_id" value="{{ $val->id }}">
                                    <td align="center">{{ $i++ }}</td>
                                    <td>{{ $val->nama }}</td>
                                    <td align="center">
                                        <button class="btn btn-primary flat" onclick="document.getElementById('loading4').style.display = 'block'"><span class="fa fa-check"></span> Pilih</button>
                                    </td>
                                </form>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="overlay" id="loading4" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="tambah_data_produk_akan_datang" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content box">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="riwayatheader">Tambah Data Produk Akan Datang</h4>
            </div>
            <div class="modal-body">
                <div class="box-body">
                    <table id="mydatatables5" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th style="text-align:center">NO</th>
                                <th style="text-align:center">NAMA</th>
                                <th style="text-align:center"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=1; ?>
                            @foreach($produk as $val)
                            <tr>
                                <form class="form-horizontal" method="post" action="{{ url('/tambah_data_produk_akan_datang') }}" autocomplete="off">
                                 {{ csrf_field() }}
                                <input type="hidden" name="produk_id" value="{{ $val->id }}">
                                    <td align="center">{{ $i++ }}</td>
                                    <td>{{ $val->nama }}</td>
                                    <td align="center">
                                        <button class="btn btn-primary flat" onclick="document.getElementById('loading5').style.display = 'block'"><span class="fa fa-check"></span> Pilih</button>
                                    </td>
                                </form>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="overlay" id="loading5" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="tambah_produk_pilihan" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content box">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah Produk Pilihan</h4>
            </div>
            <form class="form-horizontal" method="post" action="{{ url('/tambah_produk_pilihan') }}" autocomplete="off"  enctype="multipart/form-data">
            <div class="modal-body">
                {{ csrf_field() }}
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="nama" style="text-transform: capitalize;">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Banner (persegi)</label>
                    <div class="col-sm-10">
                        <input type="file" class="form-control" name="file_gambar">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">List Produk</label>
                    <div class="col-sm-10">
                        <select class="form-control select2" multiple="multiple" name="list_produk[]" style="width: 100%">
                            @foreach($all_produk as $value)
                                @if($value->produk != '[]')
                                <optgroup label="{{ $value->nama }}">
                                @endif
                                @foreach($value->produk as $value2)
                                    <option value="{{ $value2->id }}">{{ $value2->nama }}</option>
                                @endforeach
                                @if($value->produk != '[]')
                                </optgroup>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <p>Keterangan (*) : Wajib Diisi</p>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default flat pull-left"><span class="glyphicon glyphicon-ban-circle"></span> Batal</button>
                <button type="submit" class="btn btn-primary flat" onclick="document.getElementById('loading6').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
            </div>
            </form>
            <div class="overlay" id="loading6" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="tambah_paket_pilihan" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content box">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah paket Pilihan</h4>
            </div>
            <form class="form-horizontal" method="post" action="{{ url('/tambah_paket_pilihan') }}" autocomplete="off"  enctype="multipart/form-data">
            <div class="modal-body">
                {{ csrf_field() }}
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="nama" style="text-transform: capitalize;">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Banner (persegi)</label>
                    <div class="col-sm-10">
                        <input type="file" class="form-control" name="file_gambar">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">List paket</label>
                    <div class="col-sm-10">
                        <select class="form-control select2" multiple="multiple" name="list_paket[]" style="width: 100%">
                            @foreach($all_paket as $value)
                                <option value="{{ $value->id }}">{{ $value->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <p>Keterangan (*) : Wajib Diisi</p>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default flat pull-left"><span class="glyphicon glyphicon-ban-circle"></span> Batal</button>
                <button type="submit" class="btn btn-primary flat" onclick="document.getElementById('loading7').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
            </div>
            </form>
            <div class="overlay" id="loading7" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<form action="{{ url('/hapus_data_produk_baru') }}" method="post" id="form_hapus_data_produk_baru">
    {{ csrf_field() }}
    <input type="hidden" name="produk_id_baru" id="produk_id_baru">
</form>
<form action="{{ url('/hapus_data_produk_akan_datang') }}" method="post" id="form_hapus_data_produk_akan_datang">
    {{ csrf_field() }}
    <input type="hidden" name="produk_id_akan_datang" id="produk_id_akan_datang">
</form>
<form id="form_hapus_produk_tampil_di_beranda" action="{{ url('/hapus_produk_tampil_di_beranda') }}" method="post">
    {{ csrf_field() }}
    <input type="hidden" name="id" id="id_hapus_produk_tampil_di_beranda">
</form>
<form id="form_hapus_produk_pilihan" action="{{ url('/hapus_produk_pilihan') }}" method="post">
    {{ csrf_field() }}
    <input type="hidden" name="id" id="hapus_produk_pilihan_id">
</form>

@endsection

<script src="{{ asset('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/select2/select2.full.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            // save the latest tab; use cookies if you like 'em better:
            localStorage.setItem('lastTab_promo_creator', $(this).attr('href'));
        });

        // go to the latest tab, if it exists:
        var lastTab_promo_creator = localStorage.getItem('lastTab_promo_creator');
        if (lastTab_promo_creator) {
            $('[href="' + lastTab_promo_creator + '"]').tab('show');
        }
        else
        {
            lastTab_promo_creator = "#tab_1";
            $('[href="' + lastTab_promo_creator + '"]').tab('show');
        }

        $("#mydatatables").DataTable();
        $("#mydatatables2").DataTable();
        $("#mydatatables3").DataTable();
        $("#mydatatables4").DataTable();
        $("#mydatatables5").DataTable();
        $('[data-toggle="tooltip"]').tooltip(); 
        $(".select2").select2();

        // Sortable rows BELUM SELESAI
        // $('.sorted_table').sortable({
        //     axis: 'y',
        //     opacity: 0.7,
        //     update: function(event, ui) {
        //         var counter = document.getElementById('jumlah_slider_promo').value;
        //         counter     = parseInt(counter);

        //         var list_order = "";
        //         for (var i = 0; i < counter; i++) {
        //             var list = document.getElementById('tabel_slider').rows[i+1].cells[0].innerHTML;

        //             if(i == 0){
        //                 list_order = list;
        //             }
        //             else{
        //                 list_order = list_order + ','+ list;
        //             }

        //             document.getElementById('tabel_slider').rows[i+1].cells[0].innerHTML = i+1;
        //         }

        //         $.ajaxSetup({
        //             headers: {
        //                 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        //             }
        //         })
        //         // change order in the database using Ajax
        //         url = "{{ url('/set_order_slider_promo') }}";
                
        //         $.ajax({
        //             url: url,
        //             type: 'POST',
        //             data: {list_order:list_sortable},
        //                 success: function(data) {
        //             }
        //         });

        //     }
        // });
    });

    function hapus_slider_promo(id)
    {
        var cek = confirm('apakah anda yakin menghapus slider promo');

        if(cek){
            document.getElementById('hapus_slider_promo_id').value = id;
            document.getElementById('loading').style.display = 'block';
            document.getElementById('form_hapus_slider_promo').submit();
        }
    }

    function hapus_data_produk_baru(id)
    { 
        var cek = confirm('Apakah Anda Yakin?');

        if(cek)
        {
            document.getElementById('loading').style.display = 'block';
            document.getElementById('produk_id_baru').value = id;
            document.getElementById('form_hapus_data_produk_baru').submit();
        }
    }

    function hapus_data_produk_akan_datang(id)
    { 
        var cek = confirm('Apakah Anda Yakin?');

        if(cek)
        {
            document.getElementById('loading').style.display = 'block';
            document.getElementById('produk_id_akan_datang').value = id;
            document.getElementById('form_hapus_data_produk_akan_datang').submit();
        }
    }

    function hapus_produk_tampil_di_beranda(id)
    {
        var cek = confirm('Apakah anda yakin untuk menghapus produk?');

        if(cek)
        {
            document.getElementById('id_hapus_produk_tampil_di_beranda').value = id;
            document.getElementById('form_hapus_produk_tampil_di_beranda').submit();
            document.getElementById('loading').style.display = 'block';
        }
    }

    function ubah_mini_banner(id)
    {
        document.getElementById('mini_banner_nama'.concat(id)).style.display            = 'block';
        document.getElementById('mini_banner_display_nama'.concat(id)).style.display    = 'none';
        document.getElementById('mini_banner_file_gambar'.concat(id)).style.display     = 'block';
        document.getElementById('button_ubah_mini_banner'.concat(id)).style.display     = 'none';
        document.getElementById('button_simpan_mini_banner'.concat(id)).style.display   = 'block';
        document.getElementById('button_hapus_mini_banner'.concat(id)).style.display    = 'none';
        document.getElementById('button_batal_mini_banner'.concat(id)).style.display    = 'block';
    }

    function batal_mini_banner(id)
    {
        document.getElementById('mini_banner_nama'.concat(id)).style.display            = 'none';
        document.getElementById('mini_banner_display_nama'.concat(id)).style.display    = 'block';
        document.getElementById('mini_banner_file_gambar'.concat(id)).style.display     = 'none';
        document.getElementById('button_ubah_mini_banner'.concat(id)).style.display     = 'block';
        document.getElementById('button_simpan_mini_banner'.concat(id)).style.display   = 'none';
        document.getElementById('button_hapus_mini_banner'.concat(id)).style.display    = 'block';
        document.getElementById('button_batal_mini_banner'.concat(id)).style.display    = 'none';
    }

    function hapus_mini_banner(id)
    {
        var cek = confirm('Apakah anda yakin?');

        if(cek)
        {
            document.getElementById('loading').style.display = 'block';
            document.getElementById('form_hapus_mini_banner'.concat(id)).submit();
        }
    }

    function hapus_produk_pilihan(id)
    {
        var cek = confirm('Apakah anda yakin?');

        if(cek)
        {
            document.getElementById('loading').style.display = 'block';
            document.getElementById('hapus_produk_pilihan_id').value = id;
            document.getElementById('form_hapus_produk_pilihan').submit();
        }
    }
</script>