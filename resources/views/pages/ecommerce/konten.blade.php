<link rel="stylesheet" href="{{ asset('/adminlte/plugins/select2/select2.min.css') }}">
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="nav-tabs-custom box tab-primary" style="border-top: 0">
            <ul class="nav nav-tabs" id="myTab">
                <li><a href="#tab_1" data-toggle="tab">Tentang Toko</a></li>
                <li><a href="#tab_2" data-toggle="tab">Tata Cara</a></li>
                <li><a href="#tab_3" data-toggle="tab">Bantuan</a></li>
                <li><a href="#tab_4" data-toggle="tab">Gambar Produk Default</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane" id="tab_1">
                    @if(Session::has('message1'))
                        <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                          <i class="fa fa-check"></i>
                          {{ Session::get('message1') }}
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                    @endif
                    <form action="{{ url('/tentang_toko') }}" method="post" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="control-label col-md-2">Nama Toko</label>
                            <div class="col-md-8">
                                <input type="text" name="nama_toko" class="form-control" value="{{ $nama_toko }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Jam Buka</label>
                            <div class="col-md-8">
                                <input type="text" name="jam_buka" class="form-control" value="{{ $jam_buka }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Tentang Kami</label>
                            <div class="col-md-8">
                                <textarea type="text" name="tentang_kami">{{ $tentang_kami }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Service Center</label>
                            <div class="col-md-8">
                                <textarea type="text" name="service_center">{{ $service_center }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <button class="btn btn-primary flat" onclick="document.getElementById('loading').style.display='block'"><span class="fa fa-floppy-o"></span> Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="tab-pane" id="tab_2">
                    @if(Session::has('message2'))
                        <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                          <i class="fa fa-check"></i>
                          {{ Session::get('message2') }}
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                    @endif
                    <form action="{{ url('/tata_cara') }}" method="post" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="control-label col-md-2">Cara Belanja</label>
                            <div class="col-md-8">
                                <textarea type="text" name="cara_belanja">{{ $cara_belanja }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Cara Pembayaran</label>
                            <div class="col-md-8">
                                <textarea type="text" name="cara_pembayaran">{{ $cara_pembayaran }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Retur Barang</label>
                            <div class="col-md-8">
                                <textarea type="text" name="retur_barang">{{ $retur_barang }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Sistem Pengiriman</label>
                            <div class="col-md-8">
                                <textarea type="text" name="sistem_pengiriman">{{ $sistem_pengiriman }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <button class="btn btn-primary flat" onclick="document.getElementById('loading').style.display='block'"><span class="fa fa-floppy-o"></span> Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="tab-pane" id="tab_3">
                    @if(Session::has('message3'))
                        <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                          <i class="fa fa-check"></i>
                          {{ Session::get('message3') }}
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                    @endif
                    <form action="{{ url('/form_bantuan') }}" method="post" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="control-label col-md-2">Ketentuan Pengguna</label>
                            <div class="col-md-8">
                                <textarea type="text" name="ketentuan_pengguna">{{ $ketentuan_pengguna }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">FAQ (Frequently Asked Questions)</label>
                            <div class="col-md-8">
                                <textarea type="text" name="faq">{{ $faq }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Promo dan Diskon</label>
                            <div class="col-md-8">
                                <textarea type="text" name="bantuan">{{ $bantuan }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <button class="btn btn-primary flat" onclick="document.getElementById('loading').style.display='block'"><span class="fa fa-floppy-o"></span> Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>   
                <div class="tab-pane" id="tab_4">
                    @if(Session::has('message4'))
                        <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                          <i class="fa fa-check"></i>
                          {{ Session::get('message4') }}
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                    @endif
                    <div class="modal-body">
                        <div class="row">
                            <form method="post" action="{{ url('/upload_gambar_produk_default') }}" enctype="multipart/form-data" class="form-horizontal">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-md-2 control-label">Gambar Produk Default</label>
                                    <div class="col-md-8">
                                        @if(empty($gambar_produk_default))
                                        belum ditentukan
                                        @else
                                        <img src="{{ URL::asset('img/produk/'.$gambar_produk_default) }}" style="height:130px;width:auto;" class="img-responsive" alt="">
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-2">
                                        <input type="file" name="gambar" id="gambar" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-2">
                                        <button class="btn btn-primary flat" onclick="document.getElementById('loading').style.display='block'"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>          
            </div>
            <div class="overlay" id="loading" style="display:none">
              <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

@endsection

<script src="{{ asset('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            // save the latest tab; use cookies if you like 'em better:
            localStorage.setItem('lastTab_konten', $(this).attr('href'));
        });

        // go to the latest tab, if it exists:
        var lastTab_konten = localStorage.getItem('lastTab_konten');
        if (lastTab_konten) {
            $('[href="' + lastTab_konten + '"]').tab('show');
        }
        else
        {
            lastTab_konten = "#tab_1";
            $('[href="' + lastTab_konten + '"]').tab('show');
        }
    });
</script>
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
  tinymce.init({ selector:'textarea',
  height: 200,
  plugins: [
    'advlist autolink lists link image charmap print preview anchor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste code'
  ],
  toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image' });
</script>