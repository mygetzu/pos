<style type="text/css">
    .table.table-borderless td, .table.table-borderless th {
        border: 0 !important;
    }
</style>
<script type="text/javascript">
    $(function () {
        $('#mydatatables').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": false,
            "info": true,
            "autoWidth": false
        });
        $("#biaya").maskMoney({precision:0});
    });

    function deleteAction(serviceId, invoiceHeaderId, invoiceDetailId) {
        swal({
            title: "Anda yakin menghapus tindakan ini?",
            text: "Tindakan akan dihapus dan tidak dapat dikembalikan lagi.",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            confirmButtonText: "Ya, hapus!",
            cancelButtonText: "Tidak!",
            confirmButtonColor: "#ec6c62"
        }, function () {
            var service_Id = serviceId;
            var invoiceHeader_Id = invoiceHeaderId;
            var invoiceDetail_Id = invoiceDetailId;
            window.location.href = '<?php echo (string)url('/')?>/teknisi/service_delete?delete=selected&service_id=' + service_Id + '&invoice_header_id=' + invoiceHeader_Id + '&invoice_detail_id=' + invoiceDetail_Id;
        });
    }

    function cancelForm(serviceId, invoiceHeaderId) {
        swal({
                title: 'Batalkan perubahan',
                text: 'Anda yakin ingin membatalkan pendataan kerusakan?',
                type: 'warning',
                showCancelButton: true,
                closeOnConfirm: false,
                confirmButtonText: 'Ya, batalkan pendataan!',
                cancelButtonText: 'Tidak!',
                confirmButtonColor: '#ec6c62'
            },
            function () {
                window.location.href = '<?php echo (string)url('/')?>/teknisi/service_cancel?delete=all&service_id=' + serviceId + 'invoice_header=' + invoiceHeaderId;
            }
        );
    }

    function verifySubmit(serviceId) {
        swal({
                title: 'Apakah Anda yakin menutup perbaikan perangkat ini?',
                text: 'Setelah Anda menutup perbaikan Anda tidak dapat lagi memperbaiki perangkat ini. Sistem akan menandai bahwa Anda telah selesai mengerjakan perbaikan ini.',
                type: 'warning',
                showCancelButton: true,
                closeOnConfirm: false,
                confirmButtonText: 'Ya, Tutup perbaikan!',
                cancelButtonText: 'Tidak!',
                confirmButtonColor: '#ec6c62'
            },
            function () {
                window.location.href = '<?php echo (string)url('/')?>/teknisi/service_checked?submit_repair=1&service_id=' + serviceId;
            }
        );
    }

</script>
@extends('layouts.master_teknisi')
@section('content')
    @include('includes.base_function')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary flat">
                <div class="box-body">
                    <div class="col-md-12">
                        <h3>Data Perbaikan Produk</h3>
                        @if($data['invoice_header'] == NULL)
                            <div class="callout callout-info">
                                <p style="font-size: 16px;">Teknisi belum melakukan pemeriksaan.</p>
                            </div>
                        @else
                            @foreach($data['invoice_header'] as $key => $value)
                                @if($value->harga_total == 0)
                                    <div class="callout callout-info">
                                        <p style="font-size: 16px;">Admin belum menentukan biaya.</p>
                                    </div>
                                @else
                                    @foreach($data['servis'] as $r_servis)
                                        @if($r_servis->flag_acc == 0)
                                            <div class="callout callout-info">
                                                <p style="font-size: 16px;">Admin sudah menentukan biaya & Pelanggan belum menyetujui perbaikan.</p>
                                            </div>
                                        @else
                                            <div class="callout callout-info">
                                                <p style="font-size: 16px;">Admin sudah menentukan biaya & Pelanggan sudah menyetujui perbaikan.</p>
                                            </div>
                                        @endif
                                    @endforeach
                                @endif
                            @endforeach
                        @endif
                    </div>
                    <div class="row" style="margin: 15px">
                        <form method="POST" action="{{ url('/teknisi/service_submit') }}" name="service" id="form-servis"
                              onsubmit="return validateForm()">
                            <div class="col-md-2">
                                <p>Nama Pelanggan</p>
                                <p>Model Produk</p>
                                <p>Serial number</p>
                                <p>Status Garansi</p>
                                <p>Keluhan Pelanggan</p>
                                <p>Deskripsi Produk</p>
                            </div>
                            <div class="col-md-4">
                                @foreach($data['servis'] as $r_servis)
                                    <p>{{ DB::table('tmst_pelanggan')->where('id', $r_servis->pelanggan_id)->value('nama') }}</p>
                                    <p>{{ $r_servis->model_produk }}</p>
                                    <p>{{ $r_servis->serial_number }}</p>
                                    <p>{{ ($r_servis->status_garansi_produk == 1 ? 'Ya': 'Tidak') }}</p>
                                    <p>{{ $r_servis->keluhan_pelanggan }}</p>
                                    <p>{{ $r_servis->deskripsi_produk }}</p>
                                @endforeach
                            </div>
                            <div class="col-md-6">
                                <p>Catatan</p>
                                @if($data['link'] == 'service_detail' || $data['link'] == 'service_action')
                                    <p>{{ $data['catatan'] }}</p>
                                @else
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            {{--<label for="inputEmail3" class="col-sm-2 control-label">Catatan</label>--}}
                                            <div class="col-sm-8">
                                        <textarea name="catatan" type="text" class="form-control" id="note"
                                                  placeholder="Catatan Perbaikan"
                                                  rows="5">{{ $data['catatan'] }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="row" style="margin-top: 25px">
                                <div class="col-md-10">
                                    <table id="" class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th width="15px">No.</th>
                                            <th>Tindakan</th>
                                            <th>Biaya</th>
                                            <th style="width: 10%;"
                                                class="{{ ($data['link'] == 'service_detail'?'hidden':'') }}">
                                                Aksi
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($data['servis'] as $a => $val)
                                            @if($data['invoice_header'] == NULL)
                                                <tr>
                                                    <td colspan="3">Tidak ada tindakan perbaikan</td>
                                                </tr>
                                            @else
                                                @foreach($data['invoice_detail'] as $key => $value)
                                                    <tr>
                                                        <td>{{ ++$key }}</td>
                                                        <td>{{ $value->tindakan }}</td>
                                                        <td>Rp. {{ number_format($value->biaya, 0, ',', '.') }}</td>
                                                        <td class="{{ ($data['link'] == 'service_detail'?'hidden':'') }}"
                                                            style="max-width: 10%">
                                                            @if($data['link'] == 'service_detail')
                                                            @elseif($data['link'] == 'service_action')
                                                                <input name="servis_id" type="hidden" value="{{ $data['service_id'] }}"/>
                                                                <label class="switch">
                                                                    <input type="checkbox"
                                                                           onchange="window.location.href ='{{ url('/') }}/teknisi/service_checked?service_id={{ $data['service_id'] }}&invoice_header_id={{ $data['invoice_header_id'] }}&invoice_detail_id={{ $value->id }}'"
                                                                            {{ ($value->is_repair == 1?'checked':'') }}/>
                                                                    <div class="slider" data-toggle="tooltip"
                                                                         data-title="{{ ($value->is_repair == 1?'Jadikan belum dikerjakan':'Jadikan sudah dikerjakan') }}"></div>
                                                                </label>
                                                            @elseif($data['link'] == 'service_proses')
                                                                <button id="delete-action-old"
                                                                        data-toggle="tooltip"
                                                                        data-placement="top"
                                                                        data-title="Hapus tindakan ini"
                                                                        class="btn btn-flat btn-danger"
                                                                        type="button"
                                                                        style="font-size: 15pt;"
                                                                        onclick="return deleteAction({{ $data['service_id'] }},{{ $data['invoice_header_id'] }},{{ $value->id }})">
                                                                    <i class="fa fa-trash-o"></i>
                                                                </button>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                @if($data['link'] == 'service_detail' || $data['link'] == 'service_action')
                                    <div class="col-md-12">
                                        @if($data['link'] == 'service_action')
                                            <div class="col-md-push-2 col-md-2">
                                                <button class="btn btn-info btn-flat btn-block"
                                                        onclick="return {{ ($data['count_service_act'] == $data['count_service_done'] ? 'verifySubmit(\''. $data['service_id']. '\')': 'swal(\'Oops. Sepertinya semua tindakan perbaikan belum dilakukan.\', \'Mohon cek kembali tindakan perbaikan Anda. Pastikan semua telah ditandai.\', \'error\')') }}">
                                                    <span class="fa fa-gear"></span> Submit Perbaikan
                                                </button>
                                            </div>
                                        @endif
                                    </div>
                                @endif
                            </div>
                            @if($data['link'] == 'service_detail' || $data['link'] == 'service_action')
                            @else
                                <div class="form-horizontal">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                    @foreach($data['servis'] as $r_servis)
                                        <input type="hidden" name="servis_id" value="{{ $r_servis->id }}"/>
                                        <input type="hidden" name="pelanggan_id" value="{{ $r_servis->pelanggan_id }}"/>
                                        <input type="hidden" name="teknisi_id" value="{{ $r_servis->teknisi_id }}"/>
                                    @endforeach
                                    <div class="row" style="margin-top: 25px">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-2 control-label">Tindakan</label>
                                            <div class="col-sm-8">
                                                <input name="tindakan" type="text" class="form-control" id="tindakan"
                                                       placeholder="Tindakan" autofocus/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-2">
                                                <button type="submit" name="action" value="add"
                                                        class="btn btn-info btn-flat btn-block pull-left">Tambah Data
                                                </button>
                                            </div>
                                            <div class="col-sm-2">
                                                @if($data['invoice_header_id'] !== NULL)
                                                    <button type="submit" name="action" value="submit"
                                                            class="btn btn-success btn-flat btn-block pull-left">Submit
                                                        Data
                                                    </button>
                                                @endif
                                            </div>
                                            <div class="col-sm-offset-3 col-sm-1">
                                                @if($data['invoice_header_id'] !== NULL)
                                                    <button type="button"
                                                            onclick="cancelForm({{ $data['service_id'] }}, $data['invoice_header_id'])"
                                                            class="btn btn-default btn-flat btn-block pull-right">Batal
                                                    </button>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
    <script type="text/javascript">
        document.querySelector('#form-biaya').addEventListener('submit', function (e) {
                var form = this;
                e.preventDefault();
                swal({
                        title: "Anda yakin dengan biaya servis?",
                        text: "Setelah Anda men-submit, pelanggan akan menerima email notifikasi tagihan!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ya, saya yakin!",
                        cancelButtonText: "Batal",
                        closeOnConfirm: true,
                        html: false
                    },
                    function () {
                        form.submit();
                    }
                );
            }
        );

        document.querySelector('#form-delete').addEventListener('submit', function (e) {
                var form = this;
                e.preventDefault();
                swal({
                        title: "Are you sure?",
                        text: "You will not be able to recover this imaginary file!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Hapus!",
                        cancelButtonText: "Batal",
                        closeOnConfirm: true,
                        html: false
                    },
                    function () {
                        form.submit();
                    }
                );
            }
        );

        function validateForm() {
            var button = document.forms["service"]["action"].value;
            var tindakan = document.forms["service"]["tindakan"].value;
            if (button == "add" && tindakan == "") {
                swal("Oops...", "Kolom tindakan wajib diisi!", "error");
                return false;
            }
        }


        function deleteCheck() {
            swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Hapus!",
                    cancelButtonText: "Batal",
                    closeOnConfirm: true,
                    html: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        swal({
                            title: 'Shortlisted!',
                            text: 'Candidates are successfully shortlisted!',
                            type: 'success'
                        }, function () {
                            form.submit();
                        });

                    } else {
                        swal("Cancelled", "Your imaginary file is safe :)", "error");
                    }
                }
            );
        }


    </script>
@stop
