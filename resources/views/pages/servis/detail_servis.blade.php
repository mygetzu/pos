<style type="text/css">
    .table.table-borderless td, .table.table-borderless th {
        border: 0 !important;
    }
</style>
@extends('layouts.master_teknisi')
@section('content')
    @include('includes.base_function')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary flat">
                <div class="box-body">
                    <div class="col-md-12">
                        <h3>Data Perbaikan Produk</h3>
                    </div>
                    <div class="row" style="margin: 15px">
                        <div class="col-md-2">
                            <p>Nama Pelanggan</p>
                            <p>Model Produk</p>
                            <p>Serial number</p>
                            <p>Status Garansi</p>
                            <p>Keluhan Pelanggan</p>
                            <p>Deskripsi Produk</p>
                        </div>
                        <div class="col-md-10">
                            @foreach($data['servis'] as $r_servis)
                                <p>{{ DB::table('tmst_pelanggan')->where('id', $r_servis->pelanggan_id)->value('nama') }}</p>
                                <p>{{ $r_servis->model_produk }}</p>
                                <p>{{ $r_servis->serial_number }}</p>
                                <p>{{ ($r_servis->status_garansi_produk == 1 ? 'Ya': 'Tidak') }}</p>
                                <p>{{ $r_servis->keluhan_pelanggan }}</p>
                                <p>{{ $r_servis->deskripsi_produk }}</p>
                            @endforeach
                        </div>
                        <div class="row" style="margin-top: 25px">
                            <table id="" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Tindakan</th>
                                    <th>Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data['servis'] as $a => $val)
                                    @if($data['invoice_header'] == NULL)
                                        <tr>
                                            <td colspan="3">Tidak ada tindakan perbaikan</td>
                                        </tr>
                                    @else
                                        @foreach($data['invoice_detail'] as $key => $value)
                                            <form action="{{ url('/teknisi/service_delete') }}"
                                                  method="POST"
                                                  id="form-delete-2"
                                                  onsubmit=" return confirm('Anda yakin menghapus tindakan ini?')">
                                                <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                                                <input name="service_id" type="hidden"
                                                       value="{{ $data['service_id'] }}"/>
                                                <input name="invoice_header_id" type="hidden"
                                                       value="{{ $data['invoice_header_id'] }}"/>
                                                <input name="invoice_detail_id" type="hidden" value="{{ $value->id }}"/>
                                                <tr>
                                                    <td>{{ ++$key }}</td>
                                                    <td>{{ $value->tindakan }}</td>
                                                    <td style="max-width: 10%">
                                                        <button id="form-delete"
                                                                class="btn btn-flat btn-danger"
                                                                type="submit">
                                                            <i class="fa fa-trash-o"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                            </form>
                                        @endforeach
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <button class="btn btn-info btn-flat" onclick="return swal('skjfbhsjdkfsh', 'sdfgbsdfksdjf', 'error')">bksbvkjsdhfskdjf</button>
                        <form class="form-horizontal" method="POST" action="{{ url('/teknisi/service_submit') }}"
                              name="service"
                              onsubmit="return validateForm()">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                            @foreach($data['servis'] as $r_servis)
                                <input type="hidden" name="servis_id" value="{{ $r_servis->id }}"/>
                                <input type="hidden" name="pelanggan_id" value="{{ $r_servis->pelanggan_id }}"/>
                                <input type="hidden" name="teknisi_id" value="{{ $r_servis->teknisi_id }}"/>
                            @endforeach
                            <div class="row" style="margin-top: 25px">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Tindakan</label>
                                    <div class="col-sm-8">
                                        <input name="tindakan" type="text" class="form-control" id="tindakan"
                                               placeholder="Tindakan" autofocus/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Catatan</label>
                                    <div class="col-sm-8">
                                        <textarea name="catatan" type="text" class="form-control" id="note"
                                                  placeholder="Catatan Perbaikan"
                                                  rows="3">{{ $data['catatan'] }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-2">
                                        <button type="submit" name="action" value="add"
                                                class="btn btn-info btn-flat btn-block pull-left">Tambah Data
                                        </button>
                                    </div>
                                    <div class="col-sm-2">
                                        @if($data['invoice_header_id'] !== NULL)
                                            <button type="submit" name="action" value="submit"
                                                    class="btn btn-success btn-flat btn-block pull-left">Submit Data
                                            </button>
                                        @endif
                                    </div>
                                    <div class="col-sm-offset-3 col-sm-1">
                                        <button type="button"
                                                onclick="swal({
                                                        title: 'Batalkan perubahan',
                                                        text: 'Anda yakin ingin membatalkan pendataan kerusakan?',
                                                        type: 'warning',
                                                        showCancelButton: true,
                                                        closeOnConfirm: false,
                                                        confirmButtonText: 'Ya, batalkan pendataan!',
                                                        cancelButtonText: 'Tidak!',
                                                        confirmButtonColor: '#ec6c62'
                                                        },
                                                        function () {
                                                        location.href = '{{ url('teknisi/produk_sedang_servis') }}';
                                                        });"
                                                class="btn btn-default btn-flat btn-block pull-right">
                                            Batal
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script>
    $('button.delete-action').click(function () {
        var photoId = $(this).attr("data-detail-id");
        deletePhoto(photoId);
    });

    function deletePhoto() {
        swal({
            title: "Are you sure?",
            text: "Are you sure that you want to delete this photo?",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            confirmButtonText: "Yes, delete it!",
            confirmButtonColor: "#ec6c62"
        }, function () {
            $.ajax({
                url: "/api/photos/" + photoId,
            })
                .done(function (data) {
                    swal("Deleted!", "Your file was successfully deleted!", "success");
                })
                .error(function (data) {
                    swal("Oops", "We couldn't connect to the server!", "error");
                });
        });
    }

    document.querySelector('#form-delete').addEventListener('submit', function (e) {
        var form = this;
        e.preventDefault();
        swal({
                title: "Are you sure?",
                text: "You will not be able to recover this imaginary file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Hapus!",
                cancelButtonText: "Batal",
                closeOnConfirm: true,
                html: false
            },
            function () {
                form.submit();
            }
        );
    }

    function validateForm() {
        var button = document.forms["service"]["action"].value;
        var tindakan = document.forms["service"]["tindakan"].value;
        if (button == "add" && tindakan == "") {
            swal("Oops...", "Kolom tindakan wajib diisi!", "error");
            return false;
        }
    }

    function deleteCheck() {
        swal({
                title: "Are you sure?",
                text: "You will not be able to recover this imaginary file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Hapus!",
                cancelButtonText: "Batal",
                closeOnConfirm: true,
                html: false
            },
            function (isConfirm) {
                if (isConfirm) {
                    swal({
                        title: 'Shortlisted!',
                        text: 'Candidates are successfully shortlisted!',
                        type: 'success'
                    }, function () {
                        form.submit();
                    });

                } else {
                    swal("Cancelled", "Your imaginary file is safe :)", "error");
                }
            }
        );
    }

    $(function () {
        $('#mydatatables').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": false,
            "info": true,
            "autoWidth": false
        });
    });
</script>