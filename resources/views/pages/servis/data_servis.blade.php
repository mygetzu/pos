<style type="text/css">
    .table.table-borderless td, .table.table-borderless th {
        border: 0 !important;
    }
</style>
@extends('layouts.master_teknisi')
@section('content')
    @include('includes.base_function')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary flat">
                <div class="box-body">
                    <div class="col-md-12">
                        <?php
                        if ($data[0]['status_service'] == 'Belum') {
                            echo '<h3>Data Perbaikan Belum Diperbaiki</h3>';
                        } elseif ($data[0]['status_service'] == 'Proses') {
                            echo '<h3>Data Perbaikan Sedang Diperbaiki</h3>';
                        } else {
                            echo '<h3>Data Perbaikan Selesai Diperbaiki</h3>';
                        }
                        ?>
                    </div>
                    <div class="row" style="margin: 15px">
                        <table id="mydatatables" class="table table-striped table-bordered">
                            <thead>
                            <th>No.</th>
                            <th>No. Invoice</th>
                            <th>Nama Pelanggan</th>
                            <th>Model Perangkat</th>
                            <th>Keluhan Pelanggan</th>
                            <th>Status</th>
                            @if($data[0]['status_service'] == 'Proses' || $data[0]['status_service'] == 'Selesai')
                                <th>Aksi</th>
                            @endif
                            </thead>
                            <tbody>
                            <?php $a = 0;?>
                            @if($data['servis'] == FALSE)
                            @else
                                @for($i = 0; $i < $data['count']; $i++)
                                    <tr>
                                        <td>{{ ++$a }}</td>
                                        <td>{{ $data[$i]['no_nota'] }}</td>
                                        <td>{{ $data[$i]['pelanggan'] }}</td>
                                        <td>{{ $data[$i]['model_produk'] }}</td>
                                        <td>{{ $data[$i]['keluhan_pelanggan'] }}</td>
                                        <td>
                                            @if($data[$i]['status_service'] == 'Belum')
                                                <span class="btn btn-flat btn-block btn-danger">Belum Ada Teknisi</span>
                                            @elseif($data[$i]['status_service'] == 'Proses')
                                                @if($data[$i]['invoice_header_count'] == 0)
                                                    <span class="btn btn-flat btn-block btn-warning">Belum Diperiksa</span>
                                                @else
                                                    @if($data[$i]['is_submit'] == 0)
                                                        <span class="btn btn-flat btn-block btn-warning">Sedang Diperiksa</span>
                                                    @else
                                                        @if($data[$i]['harga_total'] == 0)
                                                            <span class="btn btn-flat btn-block btn-info">Belum Diberi Biaya</span>
                                                        @else
                                                            @if($data[$i]['flag_acc'] == 0)
                                                                <span class="btn btn-flat btn-block btn-warning">Belum Disetujui</span>
                                                            @else
                                                                <span class="btn btn-flat btn-block btn-info">Telah Disetujui</span>
                                                            @endif
                                                        @endif
                                                    @endif
                                                @endif
                                            @elseif($data[$i]['status_service'] == 'Selesai')
                                                <span class="btn btn-flat btn-block btn-success">Perbaikan Selesai</span>
                                            @elseif($data[$i]['status_service'] == 'Selesai')
                                                <span class="btn btn-flat btn-block bg-navy">Perbaikan Dibatalkan</span>
                                            @endif
                                        </td>
                                        <td>
                                        @if($data[$i]['status_service'] == 'Proses')
                                            @if($data[$i]['invoice_header_count'] == 0)
                                                <!-- Do Insert -->
                                                    <a href="{{ url('/teknisi/service_proses/'.$data[$i]['service_id']) }}">
                                                        <button class="btn btn-success btn-flat"
                                                                data-toggle="tooltip"
                                                                data-placement="top"
                                                                data-title="Isi data kerusakan perangkat"
                                                                style="font-size: 15pt;">
                                                            <i class="fa fa-file-text-o"></i>
                                                        </button>
                                                    </a>
                                            @else
                                                @if($data[$i]['is_submit'] == 0)
                                                    <!-- Do Insert -->
                                                        <a href="{{ url('/teknisi/service_proses/'.$data[$i]['service_id']) }}">
                                                            <button class="btn btn-success btn-flat"
                                                                    data-toggle="tooltip"
                                                                    data-placement="top"
                                                                    data-title="Isi data kerusakan perangkat"
                                                                    style="font-size: 15pt;">
                                                                <i class="fa fa-file-text-o"></i>
                                                            </button>
                                                        </a>
                                                @else
                                                    @if($data[$i]['flag_acc'] == 0)
                                                        <!-- Do Detail -->
                                                            <a href="{{ url('/teknisi/service_detail/'.$data[$i]['id']) }}">
                                                                <button class="btn btn-info btn-flat"
                                                                        data-toggle="tooltip"
                                                                        data-placement="top"
                                                                        data-title="Lihat data kerusakan perangkat"
                                                                        style="font-size: 15pt;">
                                                                    <i class="fa fa-eye"></i>
                                                                </button>
                                                            </a>
                                                    @else
                                                        <!-- Do Update -->
                                                            <a href="{{ url('/teknisi/service_action/'.$data[$i]['id']) }}">
                                                                <button class="btn bg-olive btn-flat"
                                                                        data-toggle="tooltip"
                                                                        data-placement="top"
                                                                        data-title="Perbaiki kerusakan perangkat"
                                                                        style="font-size: 15pt;">
                                                                    <i class="fa fa-check-square-o"></i>
                                                                </button>
                                                            </a>
                                                    @endif
                                                @endif
                                            @endif
                                        @elseif($data[$i]['status_service'] == 'Selesai')
                                            <!-- Do Update -->
                                                <a href="{{ url('/teknisi/service_detail/'.$data[$i]['id']) }}">
                                                    <button class="btn btn-info btn-flat"
                                                            data-toggle="tooltip"
                                                            data-placement="top"
                                                            data-title="Lihat data kerusakan perangkat"
                                                            style="font-size: 15pt;">
                                                        <i class="fa fa-eye"></i>
                                                    </button>
                                                </a>
                                        @endif
                                        </td>
                                    </tr>
                                @endfor
                            @endif
                            </tbody>
                        </table>
                        <div class="col-md-12">
                            <div class="col-md-2">Keterangan Status</div>
                            <div class="col-md-10">
                                <ol>
                                    <li>Belum Ada Teknisi : <b>Belum ada teknisi yang dipilih</b> untuk melakukan perbaikan kepada perangkat.</li>
                                    <li>Belum Diperiksa : Teknisi <b>belum melakukan pemeriksaan</b> kepada perangkat.</li>
                                    <li>Belum Disetujui : Teknisi <b>telah melakukan pemeriksaan perangkat</b>, namun <b>pelanggan belum memberikan jawaban (ACC)</b> mengenai perbaikan perangkat.</li>
                                    <li>Telah Disetujui : Teknisi <b>telah melakukan pemeriksaan perangkat</b>, <b>pelanggan telah memberikan jawaban (ACC)</b> mengenai perbaikan perangkat.</li>
                                    <li>Perbaikan Selesai : Perbaikan telah <b>selesai</b> dilakukan.</li>
                                    <li>Perbaikan Batal : Perbaikan telah <b>dibatalkan</b> atas kehendak pelanggan.</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script>
    $(function () {
        $("#mydatatables").DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": false,
            "autoWidth": false
        });
    });
</script>