@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-body">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                    <i class="fa fa-check"></i>
                    {{ Session::get('message') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-md-2">Tanggal*</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-addon">
                                  <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="tanggal_awal" data-date-format="dd-mm-yyyy" name="tanggal_awal" value="{{ old('tanggal_awal') }}" placeholder="Masukkan tanggal awal">
                            </div>
                        </div>
                        <label class="control-label col-md-1">Sampai</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-addon">
                                  <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="tanggal_akhir" data-date-format="dd-mm-yyyy" name="tanggal_akhir" value="{{ old('tanggal_akhir') }}" placeholder="Masukkan tanggal akhir">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2">Status Pembayaran</label>
                        <div class="col-md-4">
                            <select class="form-control" id="status_pembayaran">
                                <option value="1">Lunas</option>
                                <option value="2">Belum Lunas</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-10">
                            <button class="btn btn-primary flat" onclick="btn_lanjut()"><span class="fa fa-arrow-right"></span> Lanjut</button>
                        </div>
                    </div>
                </form>
                <p>Keterangan (*) : Wajib Diisi</p>
                <br>
                <div id="result">

                </div>
            </div>
        </div>
        <div class="overlay" id="loading" style="display:none;">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
    </div>
</div>
@stop
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){

        $('#tanggal_awal').datepicker({
          autoclose: true,
          todayHighlight: 1,
        });

        $('#tanggal_akhir').datepicker({
          autoclose: true,
          todayHighlight: 1,
        });
    });

    function btn_lanjut()
    {
        var tanggal_awal        = document.getElementById("tanggal_awal").value;
        var tanggal_akhir       = document.getElementById("tanggal_akhir").value;
        var status_pembayaran   = document.getElementById("status_pembayaran").value;

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        var url = "{{ url('/buat_laporan_pembayaran_pelanggan') }}";
        
        $.ajax({
            type: "POST",
            url: url,
            data: { tanggal_awal:tanggal_awal, tanggal_akhir:tanggal_akhir, status_pembayaran:status_pembayaran },
            beforeSend: function(){
                    document.getElementById('loading').style.display = "block";
                  },
            success: function (data) {
                $("#result").html(data);
                document.getElementById('loading').style.display = "none";
            },
            error: function (data) {
                document.getElementById('loading').style.display = "none";
            }
        });
    }
</script>