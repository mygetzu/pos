<?php $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
                   
    function rupiah($nominal)
    {
        $rupiah =  number_format($nominal,0, ",",".");
        $rupiah = "Rp "  . $rupiah;
        return $rupiah;
    }
?>
<h2 align="center">Laporan Pembelian</h2>
<table class="table table-striped table-bordered" id="ajax_produk_tabless">
    <thead>
        <tr>
            <th style="text-align:center">Tanggal</th>
            <th style="text-align:center">Nota Beli</th>
            <th style="text-align:center">Supplier</th>
            <th style="text-align:center">Total Pembelian</th>
        </tr>
    </thead>
    <tbody>
        <?php $total_akhir = 0; ?>
        @for($i = 0; $i < $indeks; $i++)
        <tr>
            <td align="center">
                <?php
                    date_default_timezone_set("Asia/Jakarta");
                    $tanggal = new DateTime($laporan_pembelian[$i]['tanggal']);
                ?>
                <?php echo $tanggal->format('d')."-".$tanggal->format('m')."-".$tanggal->format('Y'); ?>
            </td>
            <td>{{ $laporan_pembelian[$i]['nota_beli'] }}</td>
            <td>{{ $laporan_pembelian[$i]['supplier'] }}</td>
            <td>{{ rupiah($laporan_pembelian[$i]['total_pembelian']) }}</td>
        </tr>
        <?php $total_akhir = $total_akhir + $laporan_pembelian[$i]['total_pembelian']; ?>
        @endfor
        <tr>
            <td colspan="3"></td>
            <td>{{ rupiah($total_akhir) }}</td>
        </tr>
    </tbody>
</table>
<script>
  $(function () {
    $("#ajax_produk_tabless").DataTable();
  });
</script>