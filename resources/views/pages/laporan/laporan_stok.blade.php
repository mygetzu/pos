@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-md-2">Gudang*</label>
                        <div class="col-md-6">
                            <select class="form-control" id="gudang_id">
                                <option value="">-- Pilih Gudang --</option>
                                @foreach($gudang as $val)
                                <option value="{{ $val->id }}">{{ $val->nama }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('nama'))
                                <span class="help-block">
                                    <strong>Kolom Nama Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2">Per Tanggal*</label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <div class="input-group-addon">
                                  <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="tanggal" data-date-format="dd-mm-yyyy" name="tanggal" value="{{ old('$tanggal') }}" placeholder="Masukkan tanggal">
                            </div>
                            @if ($errors->has('tanggal'))
                                <span class="help-block">
                                    <strong>Kolom Nama Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-2">
                            <button class="btn btn-primary flat" onclick="btn_lanjut()"><span class="fa fa-arrow-right"></span> Lanjut</button>
                        </div>
                    </div>
                </div>
                <p>Keterangan (*) : Wajib Diisi</p>
                <br>
                <div id="laporan_stok">

                </div>
            </div>
            <div class="overlay" id="loading" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>
@stop
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<script type="text/javascript">
    $(function () {
        $('#tanggal').datepicker({
          autoclose: true,
          todayHighlight: 1,
        });
    });

    function btn_lanjut()
    { 
        var gudang_id   = document.getElementById("gudang_id").value;
        var tanggal     = document.getElementById("tanggal").value;

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        var url = "{{ url('/do_tambah_laporan_stok') }}";
        
        $.ajax({
            type: "POST",
            url: url,
            data: { gudang_id:gudang_id, tanggal:tanggal },
            beforeSend: function(){
                    document.getElementById('loading').style.display = "block";
                  },
            success: function (data) {
                $("#laporan_stok").html(data);
                document.getElementById('loading').style.display = "none";
            },
            error: function (data) {
                alert('Masukkan data dengan benar');
                document.getElementById('loading').style.display = "none";
            }
        });
    }
</script>