<?php $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
                   
    function rupiah($nominal)
    {
        $rupiah =  number_format($nominal,0, ",",".");
        $rupiah = "Rp "  . $rupiah;
        return $rupiah;
    }
?>
<h2 align="center">Laporan Pembayaran Pelanggan</h2>
<table class="table table-striped table-bordered" id="ajax_produk_tabless">
    <thead>
        <tr>
            <th style="text-align:center">Tanggal Transaksi</th>
            <th style="text-align:center">Tanggal Pembayaran</th>
            <th style="text-align:center">Jenis</th>
            <th style="text-align:center">Pelanggan</th>
            <th style="text-align:center">Total Tagihan</th>
            <th style="text-align:center">Status Pembayaran</th>
            <th style="text-align:center">Total Pembayaran</th>
        </tr>
    </thead>
    <tbody>
        <?php $total_akhir = 0; ?>
        @for($i=0; $i<$indeks; $i++)
        <tr>
            <td align="center">
                <?php
                    if($pembayaran[$i]['tanggal_transaksi'] != "-"){
                        $tanggal_transaksi = new DateTime($pembayaran[$i]['tanggal_transaksi']);
                        echo $tanggal_transaksi->format('d')."-".$tanggal_transaksi->format('m')."-".$tanggal_transaksi->format('Y'); 
                    }
                    else{
                        echo "-";
                    }
                ?>
            </td>
            <td align="center">
                <?php
                    if($pembayaran[$i]['tanggal_pembayaran'] != "-"){
                        $tanggal_pembayaran = new DateTime($pembayaran[$i]['tanggal_pembayaran']);
                        echo $tanggal_pembayaran->format('d')."-".$tanggal_pembayaran->format('m')."-".$tanggal_pembayaran->format('Y'); 
                    }
                    else{
                        echo "-";
                    }
                ?>
            </td>
            <td>{{ $pembayaran[$i]['jenis'] }}</td>
            <td>{{ $pembayaran[$i]['pelanggan'] }}</td>
            <td align="right" style="padding-right: 30px;">{{ rupiah($pembayaran[$i]['total_tagihan']) }}</td>
            <td align="right" style="padding-right: 30px;">{{ ($pembayaran[$i]['total_pembayaran'] !== '-' ? 'Lunas' : 'Belum Lunas') }}</td>
            <td align="right" style="padding-right: 30px;">@if($pembayaran[$i]['total_pembayaran'] === '-') - @else{{ rupiah($pembayaran[$i]['total_pembayaran']) }}@endif</td>
        </tr>
        <?php if($pembayaran[$i]['total_pembayaran'] != '-') { $total_akhir = $total_akhir + $pembayaran[$i]['total_pembayaran']; } ?>
        @endfor
        <tr>
            <td colspan="6" align="center">Total</td>
            <td align="right" style="padding-right: 30px;">{{ rupiah($total_akhir) }}</td>
        </tr>
    </tbody>
</table>
<script>
  $(function () {
    $("#ajax_produk_tabless").DataTable();
  });
</script>