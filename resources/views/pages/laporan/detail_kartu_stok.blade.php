@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<?php
    $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
    date_default_timezone_set("Asia/Jakarta");
    $tanggal_awal   = new DateTime($kartu_stok->tanggal_awal);
                    $tanggal_akhir   = new DateTime($kartu_stok->tanggal_akhir);
                    $indeks = 0;
                    $saldo  = 0;
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                 <a href="{{ url('/kartu_stok_cetak/'.$kartu_stok->id) }}" target="_blank" class="btn bg-orange flat"><i class="fa fa-print"></i> Cetak</a>
                    <a href="{{ url('/kartu_stok_pdf/'.$kartu_stok->id) }}" target="_blank" class="btn btn-info flat" style="margin-left: 5px;"><i class="fa fa-download"></i> Unduh PDF</a>
            </div>
            <div class="box-body">
                    <h2 align="center">Kartu Stok</h2>
                    <h4 align="center">{{ $kartu_stok->gudang_nama }}</h4>
                    <h4 align="center">{{ $kartu_stok->produk_nama }}</h4>
                    <h4 align="center">{{ $tanggal_awal->format('d')." ".$bulan[(int)$tanggal_awal->format('m')]." ".$tanggal_awal->format('Y')." - ".$tanggal_akhir->format('d')." ".$bulan[(int)$tanggal_akhir->format('m')]." ".$tanggal_akhir->format('Y') }}
                    </h4>
                    <table class="table table-striped table-bordered" id="my_table">
                        <thead>
                            <tr>
                                <th style="text-align:center">Tanggal</th>
                                <th style="text-align:center">Serial Number</th>
                                <th style="text-align:center">Sumber Data</th>
                                <th style="text-align:center">Masuk</th>
                                <th style="text-align:center">Harga</th>
                                <th style="text-align:center">Nilai</th>
                                <th style="text-align:center">Keluar</th>
                                <th style="text-align:center">Harga</th>
                                <th style="text-align:center">Nilai</th>
                                <th style="text-align:center">Saldo</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($kartu_stok_detail as $val)
                            <tr>
                                <td align="center">
                                    <?php
                                        date_default_timezone_set("Asia/Jakarta");
                                        $tanggal = new DateTime($val->tanggal);
                                    ?>
                                    <?php echo $tanggal->format('d')."-".$tanggal->format('m')."-".$tanggal->format('Y'); ?>
                                </td>
                                <td>{{ $val->serial_number }}</td>
                                <td>{{ $val->sumber_data_nama }}</td>
                                <td align="center">
                                    @if($val->sumber_data_id === 1) {{ $val->jumlah }}  
                                    <?php $saldo=$saldo+$val->jumlah; ?>
                                    @endif
                                </td>
                                <td>
                                    @if($val->sumber_data_id === 1) {{ rupiah($val->harga) }}
                                    @endif
                                </td>
                                <td>
                                    @if($val->sumber_data_id === 1) {{ rupiah($val->jumlah*$val->harga) }}
                                    @endif
                                </td>
                                <td align="center">@if($val->sumber_data_id === 2) {{ $val->jumlah }} 
                                    <?php $saldo=$saldo-$val->jumlah; ?>
                                    @endif
                                </td>
                                <td>
                                    @if($val->sumber_data_id === 2) {{ rupiah($val->harga) }}
                                    @endif
                                </td>
                                <td>
                                    @if($val->sumber_data_id === 2) {{ rupiah($val->jumlah*$val->harga) }}
                                    @endif
                                </td>
                                <td align="center">{{ $saldo }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
</div>
@stop
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script>
  $(function () {
    $("#my_table").DataTable();
  });
</script>