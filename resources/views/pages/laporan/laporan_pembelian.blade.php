@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-body">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <table class="table table-striped">
                    <tr class="{{ $errors->has('alamat') ? ' has-error' : '' }}">
                        <th><label class="control-label">Tanggal*</label></th>
                        <th>:</th>
                        <td>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-addon">
                                  <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="tanggal_awal" data-date-format="dd-mm-yyyy" name="tanggal_awal" value="{{ old('tanggal_awal') }}" placeholder="Masukkan tanggal awal">
                            </div>
                        </div>
                        <label class="control-label col-md-1">Sampai</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-addon">
                                  <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="tanggal_akhir" data-date-format="dd-mm-yyyy" name="tanggal_akhir" value="{{ old('tanggal_akhir') }}" placeholder="Masukkan tanggal akhir">
                            </div>
                        </div>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <th></th>
                        <td>
                            <div class="col-md-12">
                                <button class="btn btn-primary flat" onclick="btn_lanjut()"><span class="fa fa-arrow-right"></span> Lanjut</button>
                            </div>
                        </div>
                        </td>
                    </tr>
                </table>
                <p>Keterangan (*) : Wajib Diisi</p>
                <br>
                <div id="result">

                </div>
            </div>
            <div class="overlay" id="loading" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>
@stop
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script type="text/javascript">
    function btn_lanjut()
    {
        var tanggal_awal   = document.getElementById("tanggal_awal").value;
        var tanggal_akhir       = document.getElementById("tanggal_akhir").value;

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        var url = "{{ url('/buat_laporan_pembelian') }}";
        
        $.ajax({
            type: "POST",
            url: url,
            data: { tanggal_awal:tanggal_awal, tanggal_akhir:tanggal_akhir },
            beforeSend: function(){
                    document.getElementById('loading').style.display = "block";
                  },
            success: function (data) {
                $("#result").html(data);
                document.getElementById('loading').style.display = "none";
            },
            error: function (data) {
                alert('Masukkan data dengan benar');
                document.getElementById('loading').style.display = "none";
            }
        });
    }

    $(document).ready(function(){

        $('#tanggal_awal').datepicker({
          autoclose: true,
          todayHighlight: 1,
        });

        $('#tanggal_akhir').datepicker({
          autoclose: true,
          todayHighlight: 1,
        });

          $('#gudang_id').change(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            id = document.getElementById("gudang_id").value;

            var url = "{{ url('/get_produk_gudang') }}";

              $.ajax({
                type: "POST",
                url: url,
                data : {id:id},
                success: function (data) {
                    $("#produk_id").html(data);
                },
                error: function (data) {
                    alert('ooo');
                }
            });
          });

          $('#target_customer').change(function(){
            if(document.getElementById("target_customer").value == 1){
              document.getElementById("label1_pilih_customer").style.display = 'none';
              document.getElementById("label2_pilih_customer").style.display = 'none';
              document.getElementById("button_pilih_customer").style.display = 'none';
            }
            else{
              document.getElementById("label1_pilih_customer").style.display = 'block';
              document.getElementById("label2_pilih_customer").style.display = 'block';
              document.getElementById("button_pilih_customer").style.display = 'block';
            }
          });
      });
</script>