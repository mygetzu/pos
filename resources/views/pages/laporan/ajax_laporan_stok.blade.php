@include('includes.base_function')
<?php
$bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
date_default_timezone_set("Asia/Jakarta");
$tanggal = new DateTime($laporan_stok_header->tanggal);
$indeks = 0;
$saldo = 0;
?>
<a href="{{ url('/laporan_stok_cetak/'.$laporan_stok_header->id) }}" target="_blank" class="btn bg-orange flat"><i class="fa fa-print"></i> Cetak</a>
<a href="{{ url('/laporan_stok_pdf/'.$laporan_stok_header->id) }}" target="_blank" class="btn btn-info flat" style="margin-left: 5px;"><i class="fa fa-download"></i> Unduh PDF</a>
<h2 align="center">Laporan Stok</h2>
<h4 align="center">{{ $laporan_stok_header->gudang->nama }}</h4>
<h4 align="center">{{ $tanggal->format('d')." ".$bulan[(int)$tanggal->format('m')]." ".$tanggal->format('Y') }}
</h4>
<h5 align="center">Total Stok : {{ $laporan_stok_header->total_stok }}</h5>
<table class="table table-striped table-bordered" id="ajax_produk_tabless">
    <thead>
        <tr>
            <th style="text-align:center">No.</th>
            <th style="text-align:center">Nama Produk</th>
            <th style="text-align:center">Serial Number</th>
            <th style="text-align:center">Jumlah</th>
        </tr>
    </thead>
    <tbody>
        <?php $index_produk = 0;?>
        @foreach($laporan_stok_detail as $key => $value)
        <tr>
            <td align="center">{{ $key+1 }}</td>
            <td>{{ $value->produk->nama }}</td>
            <td>
                <?php
                for ($index_serial = 0; $index_serial < $value->jumlah; $index_serial++) {
                    echo $laporan_stok_detail[$index_produk][$index_serial];
                    if(($index_serial+1) == $value->jumlah){
                        echo '.';
                        continue;
                    }
                    echo ', ';
                }
                $index_produk++;
                ?>
            </td>
            <td align="center">{{ $value->jumlah }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
<script>
    $(function () {
        $("#ajax_produk_tabless").DataTable();
    });
</script>