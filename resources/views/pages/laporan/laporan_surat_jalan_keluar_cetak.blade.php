<link rel="stylesheet" type="text/css" href="{{ URL::to('/css/nota.css') }}">
@include('includes.base_function')
<style type="text/css">
    [class*="col-"] {
        float: left;
    }

    table, td, th {
        border: 1px solid black;
        padding: 10px;
    }

    table {
        border-collapse: collapse;
        width: 100%;
    }

    th {
        height: 50px;
    }
</style>
<h2 align="center">Laporan Surat Jalan Keluar</h2>
@for($i = 0; $i < $indeks; $i++)
<table>
    <thead>
        <tr>
            <th style="text-align:center">TANGGAL</th>
            <th style="text-align:center">NO SURAT JALAN</th>
            <th style="text-align:center">PELANGGAN</th>
            <th style="text-align:center">PRODUK</th>
            <th style="text-align:center">JUMLAH</th>
            <th style="text-align:center">SERIAL NUMBER</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th rowspan="{{ $laporan_surat_jalan_keluar[$i]['jumlah_produk']}}">
            <?php $tanggal = new DateTime($laporan_surat_jalan_keluar[$i]['tanggal']); 
                echo $tanggal->format('d')."-".(int)$tanggal->format('m')."-".$tanggal->format('Y');
            ?>
            </td>
            <th rowspan="{{ $laporan_surat_jalan_keluar[$i]['jumlah_produk']}}">
            {{ $laporan_surat_jalan_keluar[$i]['no_surat_jalan'] }}
            </td>
            <th rowspan="{{ $laporan_surat_jalan_keluar[$i]['jumlah_produk']}}">
            {{ $laporan_surat_jalan_keluar[$i]['pelanggan'] }}
            </td>
            @foreach($laporan_surat_jalan_keluar[$i]['sj_keluar_detail'] as $key => $value)
        @if($key > 0)
        <tr>
        @endif
            <td>
                @if($value->jenis_barang_id == 1)
                    {{ $value->produk->nama }}
                @elseif($value->jenis_barang_id == 2)
                    {{ $value->hadiah->nama }}
                @elseif($value->jenis_barang_id == 3)
                    {{ $value->paket->nama }}
                @endif
            </td>
            <td align="center">{{ $value->jumlah_total }}</td>
            <td>
                <?php
                    $indeks2 = 1;
                    foreach($produk_sn[$laporan_surat_jalan_keluar[$i]['id']][$key] as $val2)
                    {
                        if($indeks2 == 1){
                            echo "$val2->serial_number";
                        }
                        elseif(!empty($val2->serial_number)){
                            echo ", $val2->serial_number";
                        }

                        $indeks2++;
                    }
                ?>
            </td>
        @if($key > 0)
        </tr>
        @else
        </tr>
        @endif
        @endforeach
    </tbody>
</table>
@endfor
<br><br><br>
<div class="col-12">
    <?php echo $print_time->format('d/m/Y G:i:s'); ?>
</div>
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function(){
          window.onload = function() { window.print(); }
      });
</script>