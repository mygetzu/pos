@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <!-- <div class="box-header with-border">
                <a href="{{ url('/laporan_stok') }}" class="pull-right">
                  <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                </a>
            </div> -->
            <div class="box-body">
                @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  {{ Session::get('message') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <table class="table table-striped">
                    <br>
                    <tr class="{{ $errors->has('nama') ? ' has-error' : '' }}">
                        <th width="20%"><label class="control-label">Gudang*</label></th>
                        <th width="5%">:</th>
                        <td>
                        <div class="col-md-12">
                            <select class="form-control" id="gudang_id">
                                <option value="">-- Pilih Gudang --</option>
                                @foreach($gudang as $val)
                                <option value="{{ $val->id }}">{{ $val->nama }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('nama'))
                                <span class="help-block">
                                    <strong>Kolom Nama Wajib Diisi</strong>
                                </span>
                            @endif
                        </div>
                        </td>
                    </tr>
                    <tr class="{{ $errors->has('alamat') ? ' has-error' : '' }}">
                        <th><label class="control-label">Per Tanggal*</label></th>
                        <th>:</th>
                        <td>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-addon">
                                  <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="tanggal" data-date-format="dd-mm-yyyy" name="tanggal" value="{{ old('$tanggal') }}" placeholder="Masukkan tanggal">
                            </div>
                        </div>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <th></th>
                        <td>
                            <div class="col-md-12">
                                <button class="btn btn-primary flat" onclick="btn_lanjut()"><span class="fa fa-arrow-right"></span> Lanjut</button>
                            </div>
                        </div>
                        </td>
                    </tr>
                </table>
                <p>Keterangan (*) : Wajib Diisi</p>
                <br>
                <div id="laporan_stok">

                </div>
            </div>
            <div class="overlay" id="loading" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>
@stop
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script type="text/javascript">
    $(function () {
        $('#tanggal').datepicker({
          autoclose: true,
          todayHighlight: 1,
        });
    });

    function btn_lanjut()
    { 
        var gudang_id   = document.getElementById("gudang_id").value;
        var tanggal     = document.getElementById("tanggal").value;

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        var url = "{{ url('/do_tambah_laporan_stok') }}";
        
        $.ajax({
            type: "POST",
            url: url,
            data: { gudang_id:gudang_id, tanggal:tanggal },
            beforeSend: function(){
                    document.getElementById('loading').style.display = "block";
                  },
            success: function (data) {
                $("#laporan_stok").html(data);
                document.getElementById('loading').style.display = "none";
            },
            error: function (data) {
                alert('Masukkan data dengan benar');
                document.getElementById('loading').style.display = "none";
            }
        });
    }
</script>