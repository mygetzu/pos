<?php $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
                   
    function rupiah($nominal)
    {
        $rupiah =  number_format($nominal,0, ",",".");
        $rupiah = "Rp "  . $rupiah;
        return $rupiah;
    }
?>
<h2 align="center">Laporan Servis</h2>
<table class="table table-striped table-bordered" id="ajax_produk_tabless">
    <thead>
        <tr>
            <th style="text-align:center">Tanggal</th>
            <th style="text-align:center">Nota Servis</th>
            <th style="text-align:center">Pelanggan</th>
            <th style="text-align:center">Nama Produk</th>
            <th style="text-align:center">Serial Number</th>
            <th style="text-align:center">Status Garansi</th>
            <th style="text-align:center">Status Servis</th>
            <th style="text-align:center">Jasa Servis</th>
        </tr>
    </thead>
    <tbody>
        <?php $total_akhir = 0; ?>
        @foreach($servis as $key => $value)
        <tr>
            <td align="center">
                <?php
                    date_default_timezone_set("Asia/Jakarta");
                    $tanggal = new DateTime($value->tanggal);
                ?>
                <?php echo $tanggal->format('d')."-".$tanggal->format('M')."-".$tanggal->format('Y'); ?>
            </td>
            <td>{{ $value->no_nota }}</td>
            <td>{{ $value->pelanggan->nama }}</td>
            <td>{{ $value->model }}</td>
            <td>{{ $value->serial_number }}</td>
            <td>{{ $value->garansi }}</td>
            <td>@if($value->flag === 1) Selesai @else Belum Selesai @endif</td>
            <td align="right" style="padding-right: 30px;">{{ rupiah($value->jasa_service) }}</td>
        </tr>
        <?php $total_akhir = $total_akhir + $value->jasa_service; ?>
        @endforeach
        <tr>
            <td colspan="7" align="center">Total</td>
            <td align="right" style="padding-right: 30px;">{{ rupiah($total_akhir) }}</td>
        </tr>
    </tbody>
</table>
<script>
  $(function () {
    $("#ajax_produk_tabless").DataTable();
  });
</script>