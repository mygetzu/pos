<?php
$bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');

function rupiah($nominal) {
    $rupiah = number_format($nominal, 0, ",", ".");
    $rupiah = "Rp " . $rupiah;
    return $rupiah;
}
?>
<h2 align="center">Laporan Penjualan</h2>
<table class="table table-striped table-bordered" id="ajax_produk_tabless">
    <thead>
        <tr>
            <th style="text-align:center">Tanggal</th>
            <th style="text-align:center">Nota Jual</th>
            <th style="text-align:center">Pelanggan</th>
            <th style="text-align:center">Nama Barang</th>
            <th style="text-align:center">Jumlah</th>
            <th style="text-align:center">Serial Number</th>
            <th style="text-align:center">Total Penjualan</th>
        </tr>
    </thead>
    <tbody>
        <?php $total_akhir = 0; ?>
        @for($i = 0; $i < $indeks; $i++)
        <tr>
            <td align="center">
                <?php
                date_default_timezone_set("Asia/Jakarta");
                $tanggal = new DateTime($laporan_penjualan[$i]['tanggal']);
                ?>
                <?php echo $tanggal->format('d') . "-" . $tanggal->format('M') . "-" . $tanggal->format('Y'); ?>
            </td>
            <td>{{ $laporan_penjualan[$i]['nota_jual'] }}</td>
            <td>{{ $laporan_penjualan[$i]['pelanggan'] }}</td>
            <td>
                <?php
                $nama_produk_lama = '';
                for ($produk_index = 0; $produk_index < $produk; $produk_index++) {
                    $nama_produk_baru = $laporan_penjualan[$i][$produk_index]['nama_produk'];
                    if($nama_produk_baru == $nama_produk_lama){
                        continue;
                    }
                    $nama_produk_lama = $nama_produk_baru;
                    echo $nama_produk_baru;
                }
                ?>
            </td>
            <td>{{ $laporan_penjualan[$i]['jumlah_produk'] }}</td>
            <td>
                <?php
                for ($produk_index = 0; $produk_index < $produk; $produk_index++) {
                    echo strtoupper(strtolower($laporan_penjualan[$i][$produk_index]['serial_produk']));
                    if($produk_index == ($produk-1)){
                        echo '.';
                    } else {
                        echo ', ';
                    }
                }
                ?>
            </td>
            <td>{{ rupiah($laporan_penjualan[$i]['total_penjualan']) }}</td>
        </tr>
        <?php $total_akhir = $total_akhir + $laporan_penjualan[$i]['total_penjualan']; ?>
        @endfor
        <tr>
            <td colspan="6"></td>
            <td>{{ rupiah($total_akhir) }}</td>
        </tr>
    </tbody>
</table>
<script>
    $(function () {
        $("#ajax_produk_tabless").DataTable();
    });
</script>