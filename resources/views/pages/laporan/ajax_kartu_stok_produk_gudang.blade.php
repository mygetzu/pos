<?php
    function rupiah($nominal)
    {
        $rupiah =  number_format($nominal,0, ",",".");
        $rupiah = "Rp "  . $rupiah;
        return $rupiah;
    }
?>
<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
<form class="form-horizontal">
    <div class="form-group">
        <label class="control-label col-md-2">Kategori</label>
        <div class="col-md-10">
            <select class="form-control" id="kategori_produk">
                <option value="">-- Semua Produk --</option>
                @foreach($kategori_produk as $val)
                <option value="{{ $val->id }}" <?php if($my_kategori_produk == $val->id) echo "selected"; ?>>{{ $val->nama }}</option>
                @endforeach
            </select>
        </div>
    </div>
</form>
<table class="table table-striped table-bordered" id="ajax_produk_tabless">
    <thead>
        <tr>
            <th style="text-align:center">NO</th>
            <th style="text-align:center">KODE</th>
            <th style="text-align:center">NAMA</th>
            <th style="text-align:center">HARGA RETAIL</th>
            <th style="text-align:center"></th>
        </tr>
    </thead>
    <tbody>
        @foreach($produk_gudang as $i => $val)
        <tr>
            <td align="center">{{ $i+1 }}</td>
            <td>{{ $val->produk->kode }}</td>
            <td>{{ $val->produk->nama }}</td>
            <td>{{ rupiah($val->produk->harga_retail) }}</td>
            <td>
                <button class="btn btn-success flat" data-dismiss="modal" onclick="pilih_produk_gudang('{{ $val->produk_id }}', '{{ $val->produk->nama }}')"><span class="fa fa-check"></span> Pilih</button>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
  $(function () {
    $("#ajax_produk_tabless").DataTable();

    $('#kategori_produk').change(function(){
            
            var kategori_produk_id = document.getElementById("kategori_produk").value;

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            gudang_id = document.getElementById("gudang_id").value;

            var url = "{{ url('/kartu_stok_produk_gudang') }}";

              $.ajax({
                type: "POST",
                url: url,
                data : {gudang_id:gudang_id, kategori_produk_id:kategori_produk_id},
                success: function (data) {
                    $("#kartu_stok_produk").html(data);
                    //document.getElementById("produk_id").selectedIndex = -1;
                },
                error: function (data) {
                    alert('ooo');
                }
            });
          });
  });
</script>