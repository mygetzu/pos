<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-md-2">Gudang*</label>
                        <div class="col-md-8">
                            <select class="form-control" id="gudang_id">
                                <option value="">-- Pilih Gudang --</option>
                                @foreach($gudang as $val)
                                <option value="{{ $val->id }}">{{ $val->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2">Produk*</label>
                        <div class="col-md-6">
                            <input type="hidden" name="produk_id" id="produk_id">
                            <input type="text" name="display_produk_nama" id="display_produk_nama" class="form-control" disabled="true">
                        </div>
                        <div class="col-md-2">
                            <a data-toggle="modal" data-target="#pilih_produk">
                                <button class="btn btn-info flat"><span class="fa fa-plus-circle"></span> Pilih Produk</button>
                            </a>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2">Tanggal*</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-addon">
                                  <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="tanggal_awal" data-date-format="dd-mm-yyyy" name="tanggal_awal" value="{{ old('tanggal_awal') }}" placeholder="Masukkan tanggal awal">
                            </div>
                        </div>
                        <label class="control-label col-md-1">Sampai*</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-addon">
                                  <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="tanggal_akhir" data-date-format="dd-mm-yyyy" name="tanggal_akhir" value="{{ old('tanggal_akhir') }}" placeholder="Masukkan tanggal akhir">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-4 col-md-offset-2">
                            <button class="btn btn-primary flat" onclick="btn_lanjut()"><span class="fa fa-arrow-right"></span> Lanjut</button>
                        </div>
                    </div>
                </div>
                <p>Keterangan (*) : Wajib Diisi</p>
                <br>
                <div id="kartu_stok">

                </div>
            </div>
            <div class="overlay" id="loading" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="pilih_produk" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content box">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Pilih Produk</h4>
            </div>
            <div class="modal-body">
                <div id="kartu_stok_produk">
                </div>
            </div>
            <div class="overlay" id="loading2" style="display:none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>

@stop
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script>
    $(function () {
        $('#tanggal_awal').datepicker({
            autoclose: true,
            todayHighlight: 1,
        });

        $('#tanggal_akhir').datepicker({
            autoclose: true,
            todayHighlight: 1,
        });
    });

    $(document).ready(function(){
        $('#gudang_id').change(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            gudang_id = document.getElementById("gudang_id").value;

            var url = "{{ url('/kartu_stok_produk_gudang') }}";

            $.ajax({
                type: "POST",
                url: url,
                data : {gudang_id:gudang_id},
                success: function (data) {
                    $("#kartu_stok_produk").html(data);
                    //document.getElementById("produk_id").selectedIndex = -1;
                },
                error: function (data) {
                    alert('ooo');
                }
            });
        });
    });

    function pilih_produk_gudang(id, nama)
    {
        document.getElementById("produk_id").value = id;
        document.getElementById("display_produk_nama").value = nama;
    }

    function btn_lanjut()
    {
        var gudang_id       = document.getElementById("gudang_id").value;
        var produk_id       = document.getElementById("produk_id").value;
        var tanggal_awal    = document.getElementById("tanggal_awal").value;
        var tanggal_akhir   = document.getElementById("tanggal_akhir").value;

        if(gudang_id == "" || produk_id == "" || tanggal_awal == "" || tanggal_akhir == ""){
            alert('form wajib dilengkapi');
            return;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        var url = "{{ url('/do_tambah_kartu_stok') }}";
        
        $.ajax({
            type: "POST",
            url: url,
            data: { gudang_id:gudang_id, produk_id:produk_id, tanggal_awal:tanggal_awal, tanggal_akhir:tanggal_akhir },
            beforeSend: function(){
                    document.getElementById('loading').style.display = "block";
                  },
            success: function (data) {
                $("#kartu_stok").html(data);
                document.getElementById('loading').style.display = "none";
            },
            error: function (data) {
                alert('Terjadi kesalahan');
                document.getElementById('loading').style.display = "none";
            }
        });
    }
</script>