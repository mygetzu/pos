@include('includes.base_function')
<?php $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
                    date_default_timezone_set("Asia/Jakarta");
                    $tanggal   = new DateTime($laporan_stok_header->tanggal);
                    $indeks = 0;
                    $saldo  = 0;
?>
<?php echo $print_time->format('d/m/Y G:i:s'); ?>
<h2 align="center">Laporan Stok</h2>
<h4 align="center">{{ $laporan_stok_header->gudang->nama }}</h4>
<h4 align="center">{{ $tanggal->format('d')." ".$bulan[(int)$tanggal->format('m')]." ".$tanggal->format('Y') }}
</h4>
<h5 align="center">Total Stok : {{ $laporan_stok_header->total_stok }}</h5>
<table class="table">
    <thead>
        <tr>
            <th style="text-align:center">NO</th>
            <th style="text-align:center">NAMA PRODUK</th>
            <th style="text-align:center">JUMLAH</th>
        </tr>
    </thead>
    <tbody>
        @foreach($laporan_stok_detail as $key => $value)
        <tr>
            <td style="text-align:center">{{ $key+1 }}</td>
            <td>{{ $value->produk->nama }}</td>
            <td style="text-align:center">{{ $value->jumlah }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
<link rel="stylesheet" type="text/css" href="{{ URL::to('/css/nota.css') }}">
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function(){
          window.onload = function() { window.print(); }
      });
</script>