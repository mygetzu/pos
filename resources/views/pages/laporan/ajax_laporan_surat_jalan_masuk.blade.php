<?php $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
                   
    function rupiah($nominal)
    {
        $rupiah =  number_format($nominal,0, ",",".");
        $rupiah = "Rp "  . $rupiah;
        return $rupiah;
    }
?>
<a href="{{ url('/laporan_surat_jalan_masuk_cetak/'.$input_tanggal_awal.'/'.$input_tanggal_akhir) }}" target="_blank" class="btn bg-orange flat"><i class="fa fa-print"></i> Cetak</a>
<a href="{{ url('/laporan_surat_jalan_masuk_pdf/'.$input_tanggal_awal.'/'.$input_tanggal_akhir) }}" target="_blank" class="btn btn-info flat" style="margin-left: 5px;"><i class="fa fa-download"></i> Unduh PDF</a>
<h2 align="center">Laporan Surat Jalan Masuk</h2>
@for($i = 0; $i < $indeks; $i++)
<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th style="text-align:center">TANGGAL</th>
            <th style="text-align:center">NO SURAT JALAN</th>
            <th style="text-align:center">SUPPLIER</th>
            <th style="text-align:center">PRODUK</th>
            <th style="text-align:center">JUMLAH</th>
            <th style="text-align:center">SERIAL NUMBER</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th rowspan="{{ $laporan_surat_jalan_masuk[$i]['jumlah_produk']}}">
            <?php $tanggal = new DateTime($laporan_surat_jalan_masuk[$i]['tanggal']); 
                echo $tanggal->format('d')."-".(int)$tanggal->format('m')."-".$tanggal->format('Y');
            ?>
            </td>
            <th rowspan="{{ $laporan_surat_jalan_masuk[$i]['jumlah_produk']}}">
            {{ $laporan_surat_jalan_masuk[$i]['no_surat_jalan'] }}
            </td>
            <th rowspan="{{ $laporan_surat_jalan_masuk[$i]['jumlah_produk']}}">
            {{ $laporan_surat_jalan_masuk[$i]['supplier'] }}
            </td>
            @foreach($laporan_surat_jalan_masuk[$i]['sj_masuk_detail'] as $key => $value)
        @if($key > 0)
        <tr>
        @endif
            <td>
                @if($value->jenis_barang_id == 1)
                    {{ $value->produk->nama }}
                @elseif($value->jenis_barang_id == 2)
                    {{ $value->hadiah->nama }}
                @endif
            </td>
            <td align="center">{{ $value->jumlah_total }}</td>
            <td>
                <?php
                    $indeks2 = 1;
                    foreach($produk_sn[$laporan_surat_jalan_masuk[$i]['id']][$key] as $val2)
                    {
                        if($indeks2 == 1){
                            echo $val2->serial_number;
                        }
                        elseif(!empty($val2->serial_number)){
                            echo ",". $val2->serial_number;
                        }

                        $indeks2++;
                    }
                ?>
            </td>
        @if($key > 0)
        </tr>
        @else
        </tr>
        @endif
        @endforeach
    </tbody>
</table>
@endfor
<script>
  $(function () {
    $("#ajax_produk_tabless").DataTable();
  });
</script>