@extends('layouts.master_admin')
@section('content')
@include('includes.base_function')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary flat">
            <div class="box-header with-border">
                <a href="{{ url('/laporan_stok_cetak/'.$laporan_stok->id) }}" target="_blank" class="btn bg-orange flat"><i class="fa fa-print"></i> Cetak</a>
                <a href="{{ url('/laporan_stok_pdf/'.$laporan_stok->id) }}" target="_blank" class="btn btn-info flat" style="margin-left: 5px;"><i class="fa fa-download"></i> Unduh PDF</a>
                <a href="{{ url('/laporan_stok') }}" class="pull-right">
                  <button class='btn btn-default flat'><span class='fa fa-arrow-left'></span> Kembali</button>
                </a>
            </div>
            <div class="box-body">
                    <?php $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
                                        date_default_timezone_set("Asia/Jakarta");
                                        $tanggal   = new DateTime($laporan_stok->tanggal);
                                        $indeks = 0;
                                        $saldo  = 0;
                    ?>
                    <h2 align="center">Laporan Stok</h2>
                    <h4 align="center">{{ $laporan_stok->nama }}</h4>
                    <h4 align="center">{{ $tanggal->format('d')." ".$bulan[(int)$tanggal->format('m')]." ".$tanggal->format('Y') }}
                    </h4>
                    </div>
                    @foreach($laporan_stok_detail as $val)
                    <hr>
                    <h3>{{ $val->nama }}</h3>
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th style="text-align:center">Tanggal</th>
                                <th style="text-align:center">Sumber Data</th>
                                <th style="text-align:center">Masuk</th>
                                <th style="text-align:center">Keluar</th>
                                <th style="text-align:center">Saldo</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($laporan_stok_detail_produk[$indeks] as $val2)
                            <tr>
                                <td align="center">
                                    <?php
                                        date_default_timezone_set("Asia/Jakarta");
                                        $tanggal = new DateTime($val2->tanggal);
                                    ?>
                                    <?php echo $tanggal->format('d')."-".$tanggal->format('m')."-".$tanggal->format('Y'); ?>
                                </td>
                                <td>
                                    {{ $val2->sumber_data_nama }}
                                </td>
                                <td align="center">
                                    @if($val2->sumber_data_id === 1) {{ $val2->jumlah }}  
                                    <?php $saldo=$saldo+$val2->jumlah; ?>
                                    @endif
                                </td>
                                <td align="center">@if($val2->sumber_data_id === 2) {{ $val2->jumlah }} 
                                    <?php $saldo=$saldo-$val2->jumlah; ?>
                                    @endif
                                </td>
                                <td align="center">{{ $saldo }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <?php $indeks++; ?>
                    <br>
                    @endforeach
            </div>
        </div>
    </div>
</div>
@stop
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>