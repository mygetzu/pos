<?php $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
                    date_default_timezone_set("Asia/Jakarta");
                    $tanggal_awal   = new DateTime($kartu_stok_header->tanggal_awal);
                    $tanggal_akhir   = new DateTime($kartu_stok_header->tanggal_akhir);
                    $indeks = 0;
                    $saldo  = 0;
    function rupiah($nominal)
    {
        $rupiah =  number_format($nominal,0, ",",".");
        $rupiah = "Rp "  . $rupiah;
        return $rupiah;
    }
?>
<a href="{{ url('/kartu_stok_cetak/'.$kartu_stok_header->id) }}" target="_blank" class="btn bg-orange flat"><i class="fa fa-print"></i> Cetak</a>
<a href="{{ url('/kartu_stok_pdf/'.$kartu_stok_header->id) }}" target="_blank" class="btn btn-info flat" style="margin-left: 5px;"><i class="fa fa-download"></i> Unduh PDF</a>
<h2 align="center">Kartu Stok</h2>
<h4 align="center">{{ $kartu_stok_header->gudang->nama }}</h4>
<h4 align="center">{{ $kartu_stok_header->produk->nama }}</h4>
<h4 align="center">{{ $tanggal_awal->format('d')." ".$bulan[(int)$tanggal_awal->format('m')]." ".$tanggal_awal->format('Y')." - ".$tanggal_akhir->format('d')." ".$bulan[(int)$tanggal_akhir->format('m')]." ".$tanggal_akhir->format('Y') }}
</h4>
</div>
<table class="table table-striped table-bordered" id="ajax_produk_tabless">
    <thead>
        <tr>
            <th style="text-align:center">Tanggal</th>
            <th style="text-align:center">Serial Number</th>
            <th style="text-align:center">Sumber Data</th>
            <th style="text-align:center">Supplier/Pelanggan</th>
            <th style="text-align:center">Masuk</th>
            <th style="text-align:center">Harga</th>
            <th style="text-align:center">Nilai</th>
            <th style="text-align:center">Keluar</th>
            <th style="text-align:center">Harga</th>
            <th style="text-align:center">Nilai</th>
            <th style="text-align:center">Saldo</th>
        </tr>
    </thead>
    <tbody>
        @foreach($kartu_stok_detail as $val)
        <tr>
            <td align="center">
                <?php
                    date_default_timezone_set("Asia/Jakarta");
                    $tanggal = new DateTime($val->tanggal);
                ?>
                <?php echo $tanggal->format('d')."-".$tanggal->format('m')."-".$tanggal->format('Y'); ?>
            </td>
            <td>{{ $val->serial_number }}</td>
            <td><?php echo ucwords(str_replace('_', ' ', $val->sumber_data->nama)).' '.$val->no_nota; ?></td>
            <td>{{ $val->supplier_or_pelanggan }}</td>
            <td align="center">
                @if($val->sumber_data_id === 1 || $val->sumber_data_id === 3) {{ $val->jumlah }}  
                <?php $saldo=$saldo+$val->jumlah; ?>
                @endif
            </td>
            <td>
                @if($val->sumber_data_id === 1 || $val->sumber_data_id === 3) {{ rupiah($val->harga) }}
                @endif
            </td>
            <td>
                @if($val->sumber_data_id === 1 || $val->sumber_data_id === 3) {{ rupiah($val->jumlah*$val->harga) }}
                @endif
            </td>
            <td align="center">@if($val->sumber_data_id === 2 || $val->sumber_data_id === 4) {{ $val->jumlah }} 
                <?php $saldo=$saldo-$val->jumlah; ?>
                @endif
            </td>
            <td>
                @if($val->sumber_data_id === 2 || $val->sumber_data_id === 4) {{ rupiah($val->harga) }}
                @endif
            </td>
            <td>
                @if($val->sumber_data_id === 2 || $val->sumber_data_id === 4) {{ rupiah($val->jumlah*$val->harga) }}
                @endif
            </td>
            <td align="center">{{ $saldo }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
<script>
  $(function () {
    $("#").DataTable();
  });
</script>