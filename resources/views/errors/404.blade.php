<!DOCTYPE html>
<html>
    <head>
        <title>Galerindo Teknologi</title>
        <link rel="shortcut icon" type="image/x-icon" href="{{ url('img/logo.png') }}" />
        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link href="{{ URL::to('eshopper/css/main.css') }}" rel="stylesheet">
        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #B0BEC5;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 72px;
                margin-bottom: 40px;
            }
        </style>
    </head>
    <body>
        <div class="container text-center">
        <div class="logo-404">
            
        </div>
        <div class="content-404">
            <!-- <h1><b>ERROR 404</b></h1>
            <h1><b>OPPS!</b>.................................</h1> -->
            <p style="font-size:17pt">Halaman yang anda cari masih dalam proses perbaikan</p>
            <h2><a href="{{ url('/') }}" style="text-decoration:none">Kembali ke halaman utama</a></h2>
        </div>
    </div>
    </body>
</html>
