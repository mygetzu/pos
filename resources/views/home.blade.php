<!DOCTYPE html>
<html>
    <head>
        <title>WebSocket PHP Open Group Chat App</title>
        <link type="text/css" rel="stylesheet" href="style.css" />
        <script src="websocket_client.js"></script>
        <style type="text/css">
            body {
    font:12px arial;
    color: #222;
    text-align:center;
    padding:35px; }

#controls, p, span {
    margin:0;
    padding:0; }

input { font:12px arial; }

a {
    color:#0000FF;
    text-decoration:none; }

a:hover { text-decoration:underline; }

#wrapper, #loginform {
    margin:0 auto;
    padding-bottom:25px;
    background:#66CCFF;
    width:504px;
    border:1px solid #ACD8F0; }

#chatbox {
    text-align:left;
    margin:0 auto;
    margin-bottom:25px;
    padding:10px;
    background:#fff;
    height:270px;
    width:430px;
    border:1px solid #ACD8F0;
    overflow:auto; }

#chatname {
    width:395px;
    border:1px solid #ACD8F0;
    margin-left: 25px;
    float:left;}

#msg {
    width:395px;
    border:1px solid #ACD8F0; }

#submit { width: 60px; }

        </style>
    </head>
    <body onload="javascript:WebSocketSupport()">
        <div id="ws_support"></div>

        <div id="wrapper">
            <div id="menu">
                <h3 class="welcome">Welcome to WebSocket PHP Open Group Chat App v1</h3>
            </div>

            <div id="chatbox"></div>

            <div id ="controls">
                <label for="name"><b>Name</b></label>
                <input name="chatname" type="text" id="chatname" size="67" placeholder="Type your name here"/>
                <input name="msg" type="text" id="msg" size="63" placeholder="Type your message here" />
                <input name="sendmsg" type="submit"  id="sendmsg" value="Send" onclick="doSend(document.getElementById('msg').value)" />
            </div>
        </div>
        <script type="text/javascript">
            var output;

var websocket;

function WebSocketSupport()
{
    if (browserSupportsWebSockets() === false) {
        document.getElementById("ws_support").innerHTML = "<h2>Sorry! Your web browser does not supports web sockets</h2>";

        var element = document.getElementById("wrapper");
        element.parentNode.removeChild(element);

        return;
    }

    output = document.getElementById("chatbox");

    websocket = new WebSocket('ws:localhost:999');

    websocket.onopen = function(e) {
        writeToScreen("You have have successfully connected to the server");
    };


    websocket.onmessage = function(e) {
        onMessage(e)
    };

    websocket.onerror = function(e) {
        onError(e)
    };
}

function onMessage(e) {
    writeToScreen('<span style="color: blue;"> ' + e.data + '</span>');
}

function onError(e) {
    writeToScreen('<span style="color: red;">ERROR:</span> ' + e.data);
}

function doSend(message) {
    var validationMsg = userInputSupplied();
    if (validationMsg !== '') {
        alert(validationMsg);
        return;
    }
    var chatname = document.getElementById('chatname').value;

    document.getElementById('msg').value = "";

    document.getElementById('msg').focus();

    var msg = '@<b>' + chatname + '</b>: ' + message;

    websocket.send(msg);

    writeToScreen(msg);
}

function writeToScreen(message) {
    var pre = document.createElement("p");
    pre.style.wordWrap = "break-word";
    pre.innerHTML = message;
    output.appendChild(pre);
}

function userInputSupplied() {
    var chatname = document.getElementById('chatname').value;
    var msg = document.getElementById('msg').value;
    if (chatname === '') {
        return 'Please enter your username';
    } else if (msg === '') {
        return 'Please the message to send';
    } else {
        return '';
    }
}

function browserSupportsWebSockets() {
    if ("WebSocket" in window)
    {
        return true;
    }
    else
    {
        return false;
    }
}

        </script>
    </body>
</html>
