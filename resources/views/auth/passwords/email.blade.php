@extends('layouts.master_home2')
@section('content')
@include('includes.base_function')
<div class="col-sm-12">
    <h2 class="konten-title">Email</h2>
    @if(session('status'))
        <div class="alert alert-success flat alert-dismissable" style="margin-left: 0px;">
            <i class="fa fa-check"></i>
            {{ session('status') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
        {{ csrf_field() }}

        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email" class="col-md-4 control-label">Email</label>

            <div class="col-md-6">
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Masukkan alamat email">
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-btn fa-envelope"></i> Send Password Reset Link
                </button>
            </div>
        </div>
    </form>
</div>
@endsection
<script type="text/javascript">
  $(document).ready(function(){

            $('#btn-pass').click(function(){
                var obj = document.getElementById('password');
                if(obj.type == "text")
                {
                    obj.type = "password"
                    $(this).find('span').removeClass('glyphicon glyphicon-eye-open');
                    $(this).find('span').addClass('glyphicon glyphicon-eye-close');
                }else{
                    obj.type = "text"
                    $(this).find('span').removeClass('glyphicon glyphicon-eye-close');
                    $(this).find('span').addClass('glyphicon glyphicon-eye-open');
                }
                   
            });
      });
    
</script>