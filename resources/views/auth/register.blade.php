@extends('layouts.master_home2')
@section('content')
@include('includes.base_function')
<div class="col-md-12">
    <input type="hidden" name="base_path" id="base_path" value="{{ asset('/') }}">
    <input type="hidden" name="" id="old_kode_provinsi" value="{{ old('provinsi') }}">
    <input type="hidden" name="" id="old_kota_id" value="{{ old('kota_id') }}">
    <h2 class="konten-title">Registrasi</h2>
    <form class="form-horizontal" role="form" method="POST" action="{{ url('/registrasi') }}">
        {{ csrf_field() }}
        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name" class="col-md-2 control-label">Nama*</label>
            <div class="col-md-10">
                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Masukkan Nama" style="text-transform:capitalize">
                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>Kolom Nama Wajib Diisi</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email" class="col-md-2 control-label">Email*</label>
            <div class="col-md-10">
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Masukkan Alamat E-Mail">
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="password" class="col-md-2 control-label">Password*</label>
            <div class="col-md-6">
                <div class="input-group" style="margin-bottom:0px">
                    <input id="password" type="password" class="form-control" name="password" placeholder="Masukkan Password" data-toggle="tooltip" data-placement="bottom" title="Password Minimal 6 Karakter">
                    <span class="input-group-btn">
                        <button class="btn btn-default" tabindex="-1" type="button" id="btn-pass"><span class="glyphicon glyphicon-eye-close" style="margin:3px"></span></button>
                    </span>
                </div>
                @if ($errors->has('password'))
                    <span class="help-block">
                        @if(strpos($errors->first('password'), 'required') !== false)
                        <strong>Kolom Password Wajib Diisi</strong>
                        @elseif(strpos($errors->first('password'), 'characters') !== false)
                        <strong>Pasword Minimal 6 Karakter</strong>
                        @else
                        <strong>Konfirmasi Password Tidak Sama</strong>
                        @endif
                    </span>
                @endif
            </div>
            <!-- <p id="hint_pass" style="padding:0px;color:maroon"><b>Password minmal 6 karakter</b></p> -->
        </div>
        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
            <label for="password-confirm" class="col-md-2 control-label">Konfirmasi Password*</label>
            <div class="col-md-6">
                <div class="input-group" style="margin-bottom:0px">
                <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" placeholder="Masukkan Konfirmasi Password" data-toggle="tooltip" data-placement="bottom" title="Password Minimal 6 Karakter">
                    <span class="input-group-btn">
                        <button class="btn btn-default" tabindex="-1" type="button" id="btn-pass2"><span class="glyphicon glyphicon-eye-close" style="margin:3px"></span></button>
                    </span>
                </div>
                @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                        <strong>Kolom Konfirmasi Password Wajib Diisi</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('alamat') ? ' has-error' : '' }}">
            <label for="alamat" class="col-md-2 control-label">Alamat*</label>
            <div class="col-md-10">
                <textarea id="alamat" type="text" class="form-control" name="alamat" placeholder="Masukkan Alamat" style="text-transform:capitalize" data-toggle="tooltip" data-placement="bottom" title="Alamat Minimal 4 Karakter">{{ old('alamat') }}</textarea>
                @if ($errors->has('alamat'))
                    <span class="help-block">
                        @if(strpos($errors->first('alamat'), 'required') !== false)
                        <strong>Kolom Alamat Wajib Diisi</strong>
                        @else
                        <strong>Alamat Minimal 4 Karakter</strong>
                        @endif
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('kota_id') ? ' has-error' : '' }}">
            <label for="kota" class="col-md-2 control-label">Kabupaten/Kota*</label>
            <div class="col-md-5">
                <select class="form-control" id="provinsi" name="provinsi">
                    <option value="">-- Pilih Provinsi --</option>
                    @foreach($provinsi as $val)
                    <option value="{{ $val->kode }}">{{ $val->nama }}</option>
                    @endforeach
                </select>
                @if ($errors->has('provinsi'))
                    <span class="help-block">
                        <strong>Pilihan Provinsi Wajib Diisi</strong>
                    </span>
                @endif
            </div>
            <div class="col-md-5">
                <select class="form-control" id="kota_id" name="kota_id">
                    <option value="">-- Pilih Kabupaten/Kota --</option>
                </select>
                @if ($errors->has('kota_id'))
                    <span class="help-block">
                        <strong>Pilihan Kabupaten/Kota Wajib Diisi</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('kode_pos') ? ' has-error' : '' }}">
            <label for="kode_pos" class="col-md-2 control-label">Kode Pos*</label>
            <div class="col-md-2">
                <input id="kode_pos" type="text" class="form-control" name="kode_pos" value="{{ old('kode_pos') }}" placeholder="Masukkan Kode Pos" onKeyPress="return numbersonly(this, event)" maxlength="5">
                @if ($errors->has('kode_pos'))
                    <span class="help-block">
                        <strong>Kolom Kode Pos Wajib Diisi</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('telp1') ? ' has-error' : '' }}">
            <label for="telp1" class="col-md-2 control-label">Nomor Telepon</label>
            <div class="col-md-4">
                <div class="input-group" style="margin-bottom:0px">
                    <div class="input-group-addon">
                        <i class="fa fa-phone"></i>
                    </div>
                    <input type="text" class="form-control" data-mask placeholder="contoh: (031)-9999999" name="telp" value="{{ old('telp') }}">
                </div>
            </div>
        </div>
        <div class="form-group{{ $errors->has('hp_sales') ? ' has-error' : '' }}">
            <label for="hp_sales" class="col-md-2 control-label">Nomor Handphone</label>
            <div class="col-md-4">
                <div class="input-group" style="margin-bottom:0px">
                    <div class="input-group-addon">
                        <i class="fa fa-mobile" style="width:12px"></i>
                    </div>
                    <input type="text" class="form-control" data-inputmask='"mask": "(+62)999-9999-9999"' data-mask2 placeholder="contoh: (+62)857-9999-9999" name="hp" value="{{ old('hp') }}">
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-10 col-md-offset-2">
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-btn fa-user"></i> Registrasi
                </button>
            </div>
        </div>
    </form>
    <p>Keterangan (*) : Wajib Diisi</p>
</div>

<script>
    $(document).ready(function(){
        var kode_prov = document.getElementById('old_kode_provinsi').value;
        var old_kota_id = document.getElementById('old_kota_id').value;

       if(kode_prov != "")
       {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            var path =  document.getElementById('base_path').value;
            var url = path+"get_id_provinsi/"+kode_prov;

            $.ajax({
                type: "POST",
                url: url,
                success: function (data) {
                    document.getElementById("provinsi").selectedIndex = data;
                },
                error: function (data) {
                    alert('error 1');
                }
            });


            $("#kota_id").html("");

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            var kode_provinsi = document.getElementById('old_kode_provinsi').value;
            var path =  document.getElementById('base_path').value;
            var url = path+"get_kota2/"+kode_provinsi;

            $.ajax({
                type: "POST",
                url: url,
                dataType: 'json',
                success: function (data) {
                    var my_obj = Object.keys(data).length;
                    var my_index = 0;
                    if(old_kota_id == "")
                    {
                        var z = document.createElement("option");
                        z.setAttribute("value", '');
                        var t = document.createTextNode("-- Pilih Kabupaten/Kota --");
                        z.appendChild(t);
                        document.getElementById("kota_id").appendChild(z);
                    }
                    for (var i = 0; i < my_obj; i++) {
                        var z = document.createElement("option");
                        $.each(data[i], function(key,value){
                            if(key=='id'){
                             z.setAttribute("value", value);
                            }
                            else if(key == 'nama')
                            {
                                var t = document.createTextNode(value);
                                z.appendChild(t);
                            }
                            if(key == 'id' && value == old_kota_id)
                            { 
                                my_index = i;
                            }
                        });
                        document.getElementById("kota_id").appendChild(z);
                    }
                    document.getElementById("kota_id").selectedIndex = my_index;
                },
                error: function (data) {
                    alert('error 2');
                }
            });
        }

        $('#provinsi').change(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            var kode_provinsi = document.getElementById('provinsi').value;
            if(kode_provinsi == "")
            {
                kode_provinsi = "ALL";
            }
            var path =  document.getElementById('base_path').value;
            var url = path+"get_kota/"+kode_provinsi;

            $.ajax({
                type: "POST",
                url: url,
                success: function (data) {
                    $("#kota_id").html(data);
                },
                error: function (data) {
                    alert('error 3');
                }
            });
          });

        $('#btn-pass').click(function(){
            var obj = document.getElementById('password');
            if(obj.type == "text")
            {
                obj.type = "password"
                $(this).find('span').removeClass('glyphicon glyphicon-eye-open');
                $(this).find('span').addClass('glyphicon glyphicon-eye-close');
            }else{
                obj.type = "text"
                $(this).find('span').removeClass('glyphicon glyphicon-eye-close');
                $(this).find('span').addClass('glyphicon glyphicon-eye-open');
            }
               
        });

        $('#btn-pass2').click(function(){
            var obj = document.getElementById('password_confirmation');
            if(obj.type == "text")
            {
                obj.type = "password"
                $(this).find('span').removeClass('glyphicon glyphicon-eye-open');
                $(this).find('span').addClass('glyphicon glyphicon-eye-close');
            }else{
                obj.type = "text"
                $(this).find('span').removeClass('glyphicon glyphicon-eye-close');
                $(this).find('span').addClass('glyphicon glyphicon-eye-open');
            }
        });

        $(function () {
          $('[data-toggle="tooltip"]').tooltip();
        })

        $("#password").on('input',function(e){
            $(this).tooltip('hide');
        });

        $("#password").click(function() {
            $(this).tooltip('hide');
        });

        $("#password_confirmation").on('input',function(e){
            $(this).tooltip('hide');
        });

        $("#password_confirmation").click(function() {
            $(this).tooltip('hide');
        });

        $("#alamat").on('input',function(e){
            $(this).tooltip('hide');
        });

        $("#alamat").click(function() {
            $(this).tooltip('hide');
        });
    });    
</script>
<script src="{{ asset('/adminlte/plugins/select2/select2.full.min.js') }}"></script>
<!-- InputMask -->
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();
    //Money Euro
    $("[data-mask]").inputmask({ mask: "(999[9])-9999999", greedy: false ,
        definitions: {
                '*': {
                validator: "[0-9-]",
                cardinality: 1,
                casing: "lower"
            }
        }
    });

    $("[data-mask2]").inputmask();
  });
</script>
@endsection