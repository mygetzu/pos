@extends('layouts.master_home2')
@section('content')
@include('includes.base_function')
<div class="col-md-12">
    <h2 class="konten-title">Login</h2>
    @if(Session::has('message'))
    <div class="alert alert-danger flat alert-dismissable" style="margin-left: 0px;">
        <i class="fa fa-ban"></i>
        {{ Session::get('message') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
        {{ csrf_field() }}
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email" class="col-md-4 control-label">Email</label>
            <div class="col-md-6">
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="password" class="col-md-4 control-label">Password</label>
            <div class="col-md-6">
                <div class="input-group" style="margin-bottom:0px">
                    <input id="password" type="password" class="form-control" name="password">
                    <span class="input-group-btn">
                        <button class="btn btn-default" tabindex="-1" type="button" id="btn-pass"><span class="glyphicon glyphicon-eye-close" style="margin:3px"></span></button>
                    </span>
                </div>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="remember"> Ingat Saya
                    </label>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-btn fa-sign-in"></i> Login
                </button>
                <a class="btn btn-link" href="{{ url('/password/reset') }}" style="padding-top:20px">Lupa Password</a>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
  $(document).ready(function(){

            $('#btn-pass').click(function(){
                var obj = document.getElementById('password');
                if(obj.type == "text")
                {
                    obj.type = "password"
                    $(this).find('span').removeClass('glyphicon glyphicon-eye-open');
                    $(this).find('span').addClass('glyphicon glyphicon-eye-close');
                }else{
                    obj.type = "text"
                    $(this).find('span').removeClass('glyphicon glyphicon-eye-close');
                    $(this).find('span').addClass('glyphicon glyphicon-eye-open');
                }
                   
            });
      });
    
</script>
@endsection