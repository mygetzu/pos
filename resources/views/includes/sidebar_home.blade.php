<?php
    $kategori_produk    = DB::table('tmst_kategori_produk')->where('is_aktif', 1)->orderBy('nama', 'asc')->get();
?>
<style type="text/css">
    /*--index_content--*/
    .main{
        background:#eee;
        padding: 1em 0 5em;
    }

    .sidebar_box{
        padding-right:0;
        border-right: 1px solid #B8B8B8;
    }
    .menu {
        width: auto;
        height: auto;
        padding: 0;
        margin:0;
        list-style: none;
        background: #efefef;
        font-family: "Open Sans", "Helvetica Neue", Verdana, Arial, sans-serif;
    }
    ul {
        padding: 0;
        list-style: none;
    }
    .menu > li > a {
        width: 100%;
        line-height: 3.5em;
        text-indent: 0.4em;
        display: block;
        position: relative;
        color: #263238;
        font-size:14px;
        text-decoration:none;
        border-bottom: 1px solid #ddd;
        font-weight:800;
    }
    .menu > li > a img{
        vertical-align: baseline;
        margin-right: 5px;
    }
    .menu ul li a {
        width: 100%;
        display: block;
        position: relative;
        font:800 15px/15px 'Lato', sans-serif;
        color:#263238;
        text-decoration:none;
        line-height: 3.5em;
        text-indent: 0.4em;
    }
    .menu ul li a:hover{
        background:#df1f26;
        color:#fff;
    }
    .menu > li > a:hover, .menu > li > a.active {
        background:#fff;
        border-right: 5px solid #df1f26;
        text-indent: 0.4em;
    }
    .menu ul.kid-menu li a{
        border-bottom: none;
}
</style>
<div class="sidebar">
    <a href="{{ url('/semua_kategori') }}" >
        <h3 class="menu_head" style="color:white"><span class="glyphicon glyphicon-list"></span> Kategori</h3>
    </a>
    <div class="menu_box">
        <ul class="menu" >
            @foreach($kategori_produk as $val)
            <li class="item1"><a href="{{ url('/kategori/'.$val->slug_nama) }}" style="padding-left:10px">{{ $val->nama }}</a></li>
            @endforeach
        </ul>
    </div>
</div>
<script src="{{ URL::to('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(){
    var url = window.location.href;
    $("a").each(function(){
        if(url == (this.href)){
            $(this).addClass("active");
        }
    });
});
</script>