<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel" align="center">
        <div class="pull-left image">
          <img src="{{ URL::to('img/icon.png') }}" style="padding-top:5px" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{ Auth::user()->name }}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <br>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">Navigasi Utama</li>
        <li class="<?php echo ( $active['parent'] == 1? 'active' : ''); ?> treeview">
          <a href="{{ url('/teknisi/beranda') }}">
            <i class="fa fa-home"></i><span>Beranda</span>
          </a>
        </li>
        <li class="<?php echo ($active['parent'] == 2 ? 'active' : ''); ?> treeview">
          <a href="{{ url('/teknisi/akun') }}">
            <i class="fa fa-user"></i><span>Data Akun</span>
          </a>
        </li>
        <li class="<?php echo ($active['parent'] == 3 ? 'active' : ''); ?> treeview">
          <a href="#">
            <i class="fa fa-stethoscope"></i><span>Perbaikan</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li class="<?php echo ($active['child'] == 1 ? 'active' : ''); ?>">
                <a href="{{ url('/teknisi/produk_belum_servis') }}"><i class="fa fa-circle-o"></i> Perbaikan belum dikerjakan</a>
            </li>
            <li class="<?php echo ($active['child'] == 2 ? 'active' : ''); ?>">
                <a href="{{ url('/teknisi/produk_sedang_servis') }}"><i class="fa fa-circle-o"></i> Perbaikan sedang dikerjakan</a>
            </li>
            <li class="<?php echo ($active['child'] == 3 ? 'active' : ''); ?>">
                <a href="{{ url('/teknisi/produk_selesai_servis') }}"><i class="fa fa-circle-o"></i> Perbaikan telah dikekrjakan</a>
            </li>
          </ul>
        </li>
        <li class="<?php echo ($active['parent'] == 4 ? 'active' : ''); ?> treeview">
          <a href="#">
            <i class="fa fa-file-text-o"></i><span>Laporan</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li class="<?php echo ($active['child'] == 1 ? 'active' : ''); ?>">
                <a href="{{ url('/teknisi/laporan_servis') }}"><i class="fa fa-circle-o"></i> Laporan Perbaikan</a>
            </li>
            <li class="<?php echo ($active['child'] == 2 ? 'active' : ''); ?>">
                <a href="{{ url('/teknisi/laporan_servis_bulan') }}"><i class="fa fa-circle-o"></i> Laporan Perbaikan per Bulan</a>
            </li>
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>