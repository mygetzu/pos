<?php
    $hak_akses  = DB::table('hak_akses')->where('id', Auth::user()->hak_akses_id)->first();
    $menu_akses = $hak_akses->menu_akses;

    for ($i=101; $i <= 106; $i++) { 
        $menu[$i] = 0;
    }

    $menu_akses = explode('-', $menu_akses);

    for ($i=0; $i < count($menu_akses); $i++) { 
        $menu[$menu_akses[$i]] = 1;
    }

    for ($i=1; $i <= 3; $i++) { 
        $parent_menu[$i] = 1;
    }

    if($menu[101] == 0){
        $parent_menu[1] = 0;
    }
    if($menu[102] == 0){
        $parent_menu[2] = 0;
    }
    if($menu[103] == 0 && $menu[104] == 0 && $menu[105] == 0){
        $parent_menu[3] = 0;
    }
?>

<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel" align="center">
        <div class="pull-left image">
          <img src="{{ URL::to('img/icon.png') }}" style="padding-top:5px" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{ Auth::user()->name }}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <br>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">Navigasi Utama</li>
        <li <?php echo ($parent_menu[1] == 0 ? "style='display: none;'" : ""); ?> class="<?php echo (substr($general['kode_menu'], 0, 1) == 1 ? 'active' : ''); ?> treeview">
          <a href="{{ url('beranda_teknisi') }}">
            <i class="fa fa-home"></i><span>Beranda</span>
          </a>
        </li>
        <li <?php echo ($parent_menu[2] == 0 ? "style='display: none;'" : ""); ?> class="<?php echo (substr($general['kode_menu'], 0, 1) == 2 ? 'active' : ''); ?> treeview">
          <a href="{{ url('akun_teknisi') }}">
            <i class="fa fa-user"></i><span>Data Akun</span>
          </a>
        </li>
        <li class="<?php echo (substr($general['kode_menu'], 0, 1) == 3 ? 'active' : ''); ?> treeview">
          <a href="#">
            <i class="fa fa-laptop"></i><span>Perbaikan</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li <?php echo ($general['kode_menu'] == 1 ? "class='active'" : ""); ?>><a href="{{ url('/') }}"><i class="fa fa-circle-o"></i> Promo Creator</a></li>
            <li <?php echo ($general['kode_menu'] == 92 ? "class='active'" : ""); ?>><a href="{{ url('/konten') }}"><i class="fa fa-circle-o"></i> Konten</a></li>
          </ul>
        </li>
        <li  <?php if($parent_menu[2] == 0) echo "style='display: none;'"; ?> class="<?php echo (substr($general['kode_menu'], 0, 1) == 4 ? 'active' : ''); ?> treeview">
          <a href="#">
            <i class="fa fa-gear"></i><span>Laporan</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
              <ul class="treeview-menu">
            <li id="menu4" <?php if($menu[4] == 0) echo "style='display: none;'"; ?> <?php if($general['kode_menu'] == 25) echo "class='active'"; ?>><a href="{{ url('/pengaturan_umum') }}"><i class="fa fa-circle-o"></i> Pengaturan Umum</a></li>
            <li id="menu5" style='display: none;' <?php if($general['kode_menu'] == 21) echo "class='active'"; ?>><a href="{{ url('/ecommerce') }}"><i class="fa fa-circle-o"></i> E-commerce</a></li>
            <li id="menu6" <?php if($menu[6] == 0) echo "style='display: none;'"; ?> <?php if($general['kode_menu'] == 23) echo "class='active'"; ?>><a href="{{ url('/group_account') }}"><i class="fa fa-circle-o"></i> Group Account</a></li>
            <li id="menu7" <?php if($menu[7] == 0) echo "style='display: none;'"; ?> <?php if($general['kode_menu'] == 22) echo "class='active'"; ?>><a href="{{ url('/master_coa') }}"><i class="fa fa-circle-o"></i> Master COA (Chart of Account)</a></li>
            <li id="menu8" <?php if($menu[8] == 0) echo "style='display: none;'"; ?> <?php if($general['kode_menu'] == 24) echo "class='active'"; ?>><a href="{{ url('/pengaturan_jurnal') }}"><i class="fa fa-circle-o"></i> Pengaturan Jurnal</a></li>
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>