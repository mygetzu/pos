<?php
    $kategori_produk = DB::table('tmst_kategori_produk')->where('is_aktif', 1)->get();
    $cart_content    = Cart::content(1);
    $jumlah=0; $total_bayar=0;
    if(!empty($cart_content)){
        foreach($cart_content as $cart){
            $jumlah += $cart->qty;
            $total_bayar += ($cart->price*$cart->qty);
        }
    }
?>
<style type="text/css">

nav ul.menu-right li {
    display: inline-block;
    margin: 0 0px;
    border-right: 1px solid #fff;
    float: right;
}

nav ul.menu-right li:first-child {
    border-right: 0;
}

nav ul.menu-left li {
    display: inline-block;
    margin: 0 0px;
    border-right: 1px solid #fff;
    float: left;
}

nav ul.menu-left li:last-child {
    border-right: 0;
}

nav a {
    color: #ffffff;
    text-decoration: none;
    display: block;
    font-size: 12px;
    text-transform: uppercase;
    padding: 0px 13px;
}

nav a:hover {
    color: #d8d8d8;
}

/* ## Necessary Demo Styles
--------------------------------------------- */
.responsive-nav-icon::before,
.responsive-nav-close::before {
    color: #20BDD4;
    content: "\f0c9";
    font-family: FontAwesome;
    font-size: 22px;
    position: relative;
}

.responsive-nav-close::before {
    color: #20BDD4;
    content: "\f00d";
    font-size: 18px;
}

.responsive-nav-icon {
    background: #fff;
    line-height: normal;
    padding: 5px 8px 4px;
    top: 10px; left: 20px;
}

.responsive-nav-icon:hover,
.responsive-nav-close:hover {
    opacity: .7;
}

.responsive-nav-close {
    top: 10px; right: 10px;
}

.responsive-nav-icon,
.responsive-nav-close {
    cursor: pointer;
    display: none;
}

#overlay {
    background: 0 0 rgba(0, 0, 0, 0.8);
    display: none;
    height: 100%;
    position: fixed;
    top: 0; left: 0;
    -moz-transition: all 0.2s linear 0s;
    -webkit-transition: all 0.2s linear 0s;
    -ms-transition: all 0.2s linear 0s;
    transition: all 0.2s linear 0s;
    width: 100%;
    z-index: 1001;
}

@media only screen and (max-width: 600px) {
    .responsive-nav-icon,
    .responsive-nav-close {
        display: block;
        position: absolute;
        z-index: 1;
    }

    nav {
        background: #F7F7F7;
        height: 100%;
        padding: 20px;
        position: fixed;
        top: 0; left: -400px;
        -moz-transition: all 0.2s linear 0s;
        -webkit-transition: all 0.2s linear 0s;
        -ms-transition: all 0.2s linear 0s;
        transition: all 0.2s linear 0s;
        width: 0;
        clear: both;
    }
    
    nav.slide-in {
        left: 0;
        width: 280px;
        z-index: 1002;
    }
    
    nav .menu-item {
        display: block;
    }

    nav ul.menu-left {
        display: none;
    }

    nav ul li {
        border-right: 0;
    }

    nav a {
        color: #282b2d;
        display: block;
        padding: 10px 64px 10px 0;
        font-size: 12pt;
    }

    nav button.dropbtn {
            color: #ffffff;
    text-decoration: none;
    display: block;
    font-size: 12px;
    text-transform: uppercase;
    padding: 0px 13px;
    }

    nav ul.menu-right li {
        float: left;
    }

    nav .dropbtn {
        display: none;
    }

    nav .dropdown-content {
        position: relative;
        box-shadow: 0px 0px 0px 0px white;
        display: block;
        
    }

    nav .dropbtn{
        color: #282b2d;
    }

    .header_top {
        padding: 0px;
    }

    .logo {
        margin-left: 60px;
    }

    .logo img {
        height: 25px;
    }

    .search {
        margin: 0px 0px 0px 20px;
    }

    .affix {
        position: static;
    }
}
</style>
</style>
<div class="top_bg" id="home">
    <div style="padding-left: 10px;padding-right: 10px;">
        <div class="header_top">
            <nav class="" >
                <ul class="menu-left">
                    @if(!empty($perusahaan->telp))
                        <li><a style="pointer-events: none;"><span class="fa fa-phone"></span>
                            {{ str_replace("_", "", $perusahaan->telp) }}
                        </a></li>
                    @endif
                    @if(!empty($perusahaan->jam_operasional))
                        <li><a style="pointer-events: none;"><span class="fa fa-clock-o"></span>
                        {{ $perusahaan->jam_operasional }}
                          </a></li>
                    @endif
                    @if(!empty($perusahaan->email))
                    <li>
                        <a href="mailto:{{ $perusahaan->email }}"><span class="fa fa-envelope"></span>
                            {{ $perusahaan->email }}
                        </a>
                    </li>
                    @endif
                </ul>
                <ul  class="menu-right">
                    @if(Auth::check())
                    <li class="menu-item">
                        <a onclick="myFunction()" class="dropbtn"><i class="fa fa-user"></i> {{ Auth::user()->name }} <span class="caret"></span></a>
                        <div id="akun_dropdown" class="dropdown-content">
                            @if(Auth::user()->hak_akses_id == 2)
                            <a href="{{ url('/akun_pelanggan') }}"><i class="fa fa-user-circle" ></i> Akun Saya</a>
                            <a href="{{ url('/password_pelanggan') }}"><i class="fa fa-lock" ></i> Ubah Password</a>
                            <a href="{{ url('/pesanan') }}"><i class="fa fa-shopping-bag" ></i> Pesanan</a>
                            @endif
                            <a href="{{ url('logout') }}"><i class="glyphicon glyphicon-log-out" ></i> Logout</a>
                        </div>
                    </li>
                    @if(Auth::user()->hak_akses_id == 1)
                    <li class="menu-item"><a href="{{ url('/beranda_admin') }}"><i class="fa fa-tachometer"></i> Admin Panel <span class="sr-only">(current)</span></a></li>
                    @elseif(Auth::user()->hak_akses_id == 3)
                    <li class="menu-item"><a href="{{ url('/teknisi/beranda') }}"><i class="fa fa-tachometer"></i> Teknisi Panel <span class="sr-only">(current)</span></a></li>
                    @endif
                    @else
                    <li class="menu-item"><a href="{{ url('login') }}"><i class="glyphicon glyphicon-log-in"></i> Login</a></li>
                    <li class="menu-item"><a href="{{ url('registrasi') }}"><i class="glyphicon glyphicon-user" ></i> Registrasi</a></li>
                    @endif
                </ul>
            </nav>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<div class="header_bg" style="margin-bottom: 20px; ">
    <div class="head-t" style="background-color: #B5E2EC;z-index:1000;width: 100%;top: 0px;" data-spy="affix" data-offset-top="50">
        <div class="container">
            <section class="row one white">
            <div class="logo">
                <a href="{{ url('/') }}"><img src="http://130.211.105.244/pos/public/img/logo.png" height="30px" alt=""/></a>
            </div>
            <div class="search">
                <form method="get" action="{{ url('/search') }}">
                    <input type="text" name="search" class="textbox" placeholder="Cari produk di sini" value="@if(!empty($search)){{ $search }}@endif">
                    <input type="submit" value="Subscribe" id="submit" name="submit">
                    <div id="response"> </div>
                </form>
            </div>
            <div class="header_right">
                <button class="dropdown-toggle" type="button" id="menu1" data-toggle="dropdown" style="background-color: transparent;border: 0px;padding: 12px 0px 0px 0px;font-size: 11pt; color: #4D4D4D">
                            <i class="glyphicon glyphicon-shopping-cart" style="font-size: 27pt;padding-left: 0px; color:#20BDD4"></i><b style="top: 2px">({{ $jumlah }}) Item</b>
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="menu1" style="width: 250px;padding: 10px; left:auto;right:0; margin-right:-10px;">
                            <li role="presentation">Total ({{ $jumlah }}) Item {{ rupiah($total_bayar) }}</li>
                            <li role="presentation" class="divider"></li>
                            <li role="presentation">
                                <div class="row">
                                    <div class="col-md-6">
                                        <a href="{{ url('/cart') }}" class="btn btn-info flat">Keranjang Belanja</a>
                                    </div>
                                    <div class="col-md-6">
                                        <a href="{{ url('/checkout/data_pengiriman') }}" class="btn btn-success flat pull-right">Checkout</a>
                                    </div>
                                </div>
                            </li>
                        </ul>          
            </div>
            <div class="clearfix"></div>    
        </div>
    </div>
    <div id="overlay"></div>
    <div class="header" style="padding-top: 5px; padding-bottom:5px; box-shadow: 0 4px 2px -2px gray; background-color: white;">
        <div class="container">
            <!--start-header-menu-->
            <ul class="megamenu skyblue">
                <li class="grid"><a class="color2" href="#">Kategori</a>
                    <div class="megapanel" style="z-index: 1000;">
                        <div class="">
                            <div class="h_nav">
                                <ul>
                                    <li><a href="{{ url('/semua_kategori') }}" style="padding-left:10px">Semua Kategori</a></li>
                                    @foreach($kategori_produk as $val)
                                    <li><a href="{{ url('/kategori/'.$val->slug_nama) }}" style="padding-left:10px">{{ $val->nama }}</a></li>
                                    @endforeach
                                </ul>   
                            </div>  
                        </div>
                    </div>
                </li>
                <li class="grid"><a class="color1" href="{{ url('/') }}">Beranda</a></li>
                <li class="grid"><a class="color1" href="{{ url('/promo') }}">Promo</a></li>
                <li class="grid"><a class="color1" href="{{ url('/list_paket_produk') }}">Paket Produk</a></li>
                <li class="grid"><a class="color1" href="{{ url('/cara_belanja') }}">Cara Belanja</a></li>
                <li class="grid"><a class="color1" href="{{ url('/lacak_pesanan') }}">Lacak Pesanan</a></li>
                <li class="grid"><a class="color1" href="{{ url('/service_center') }}">Service Center</a></li>
                <li class="grid"><a class="color1" href="{{ url('/bantuan') }}">Bantuan</a></li>
             </ul> 
        </div>
    </div>
</div>

<script type="text/javascript">
    /* When the user clicks on the button,
    toggle between hiding and showing the dropdown content */
    function myFunction() {
        document.getElementById("akun_dropdown").classList.toggle("show");
    }

    // Close the dropdown menu if the user clicks outside of it
    window.onclick = function(event) {
      if (!event.target.matches('.dropbtn')) {

        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
          var openDropdown = dropdowns[i];
          if (openDropdown.classList.contains('show')) {
            openDropdown.classList.remove('show');
          }
        }
      }
    }

    $(document).ready(function () {
        $('a').click(function(e) {
            localStorage.removeItem('ingat_menu_bar');
        });

        $('li > a').click(function(e) {

            $('li').removeClass();

            $(this).parent().addClass('active');
            localStorage.setItem('ingat_menu_bar', $(this).attr('href'));
        });

        var ingat_menu_bar = localStorage.getItem('ingat_menu_bar');
        
        if(ingat_menu_bar){
            $('[href="' + ingat_menu_bar + '"]').closest('li').addClass("active");
        }
    });
    $(function() {
    // Insert Responsive Sidebar Icon
    $('<div class="responsive-nav-icon" />').appendTo('.row.one');
    $('<div class="responsive-nav-close" />').appendTo('nav');

    // Navigation Slide In
    $('.responsive-nav-icon').click(function() {
        $('nav').addClass('slide-in');
        $('html').css("overflow", "hidden");
        $('#overlay').show();
        return false;
    });

    // Navigation Slide Out
    $('#overlay, .responsive-nav-close').click(function() {
        $('nav').removeClass('slide-in');
        $('html').css("overflow", "auto");
        $('#overlay').hide();
        return false;
    });
});
</script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>