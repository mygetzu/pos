<?php
    function rupiah($nominal)
    {
        $rupiah =  number_format($nominal,0, ",",".");
        $rupiah = "Rp "  . $rupiah;
        return $rupiah;
    }
    
    function decimal($nominal)
    {
        $rupiah =  number_format($nominal,0, ",",".");
        return $rupiah;
    }

    function colourCreator($colour, $per)
    { 
        $colour = substr( $colour, 1 ); // Removes first character of hex string (#)
        $rgb = ''; // Empty variable
        $per = $per/100*255; // Creates a percentage to work with. Change the middle figure to control colour temperature
        
        if  ($per < 0 ) // Check to see if the percentage is a negative number
        {
            // DARKER
            $per =  abs($per); // Turns Neg Number to Pos Number
            for ($x=0;$x<3;$x++)
            {
                $c = hexdec(substr($colour,(2*$x),2)) - $per;
                $c = ($c < 0) ? 0 : dechex($c);
                $rgb .= (strlen($c) < 2) ? '0'.$c : $c;
            }  
        } 
        else
        {
            // LIGHTER        
            for ($x=0;$x<3;$x++)
            {            
                $c = hexdec(substr($colour,(2*$x),2)) + $per;
                $c = ($c > 255) ? 'ff' : dechex($c);
                $rgb .= (strlen($c) < 2) ? '0'.$c : $c;
            }   
        }
        return '#'.$rgb;
    }

    function dibaca($x) {
    $angkaBaca = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");

    switch ($x) {
        case ($x < 12):
            echo " " . $angkaBaca[$x];
            break;
        case ($x < 20):
            echo $result = Dibaca($x - 10) . " Belas";
            break;
        case ($x < 100):
            echo Dibaca($x / 10);
            echo " Puluh ";
            echo Dibaca($x % 10);
            break;
        case ($x < 200):
            echo " Seratus ";
            echo Dibaca($x - 100);
            break;
        case ($x < 1000):
            echo Dibaca($x / 100);
            echo " Ratus";
            echo Dibaca($x % 100);
            break;
        case ($x < 2000):
            echo " Seribu ";
            echo Dibaca($x - 1000);
            break;
        case ($x < 1000000):
            echo Dibaca($x / 1000);
            echo " Ribu ";
            echo Dibaca($x % 1000);
            break;
        case ($x < 1000000000):
            echo Dibaca($x / 1000000);
            echo " Juta ";
            echo Dibaca($x % 1000000);
            break;
        case ($x < 1000000000000):
            echo Dibaca($x / 1000000000);
            $y = $x % 1000000000;
            echo " Milyar $x % 1000000000=$y";
            echo Dibaca($x % 1000000000);
            break;
    }
}
?>
<script type="text/javascript">
    function numbersonly(myfield, e, dec) 
    { 
        var key; var keychar; 
        if (window.event) key = window.event.keyCode; 
        else if (e) key = e.which; 
        else return true; keychar = String.fromCharCode(key);
        if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) ) return true;
        else if ((("0123456789").indexOf(keychar) > -1)) return true; 
        else if (dec && (keychar == ".")) { myfield.form.elements[dec].focus(); return false; } 
        else return false; 
    }
    function numbersdecimal(myfield, e, dec) 
    { 
        var key; var keychar; 
        if (window.event) key = window.event.keyCode; 
        else if (e) key = e.which; 
        else return true; keychar = String.fromCharCode(key);
        if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) ) return true;
        else if ((("0123456789.").indexOf(keychar) > -1)) return true; 
        else return false; 
    }
</script>
<style type="text/css">
    .table.table-borderless td, .table.table-borderless th {
        border: 0 !important;
    }
</style>
