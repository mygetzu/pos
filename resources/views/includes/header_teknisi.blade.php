<?php
    $notifikasi = DB::table('notifikasi')->where('user_id_target', Auth::user()->id)->orWhere('hak_akses_id_target', Auth::user()->hak_akses_id)->where('is_aktif', 1)->orderBy('id', 'desc')->get();
?>
<style type="text/css">
  .navbar {
      border-radius: 0;

  }

  .navbar-new {
        background-color: #20BDD4;
        z-index: 100;
        min-height:40px; height: 40px;
    }

  .navbar .navbar-brand,
  .navbar .navbar-brand:hover,
  .navbar .navbar-brand:focus {
        color: #fff;
        padding: 0px 12px;font-size: 16px;line-height: 40px;
  }

    .navbar-new .navbar-nav > li > a {
        background-color: transparent;
        color: #fff;
        padding-top: 0px; padding-bottom: 0px; line-height: 40px;
    }
    .navbar-new .navbar-nav > li > a:hover,
    .navbar-new .navbar-nav > li > a:focus {
        background-color: transparent;
        color: #B5E2EC;
        padding-top: 0px; padding-bottom: 0px; line-height: 40px;
    }

    .main-header .navbar-new > a {
        color: #4D4D4D;
    }
    .main-header .navbar-new > a:hover,
    .main-header .navbar-new > a:focus {
        background-color: transparent;
        color: #808080;
    }


  .navbar-new .navbar-text {
      color: #222;
  }

  .navbar-new .navbar-toggle .icon-bar {
      background-color: #333;
  }
  .navbar-default .navbar-nav > .active > .active, .navbar-default .navbar-nav > .active > a:hover, .navbar-default .navbar-nav > .active > a:focus {
      background-color:#FAB005;
      color: #fff;
  }
  .navbar-default .navbar-nav > .active > a {
      background-color:#ccc;
      color: #fff;
  }


</style>
<header class="main-header">
    <!-- Logo -->
<a href="{{ url('/beranda_admin') }}" class="logo" style="min-height:40px; height: 40px;background-color: #20BDD4;color: #fff;
        padding-top: 0px; padding-bottom: 0px; line-height: 40px;border-right: 0;">
  <!-- mini logo for sidebar mini 50x50 pixels -->
  <span class="logo-mini"><b>POS</b></span>
  <!-- logo for regular state and mobile devices -->
  <span class="logo-lg"><b>Admin</b> Panel</span>
</a>
<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar-new navbar-static-top">
  <!-- Sidebar toggle button-->
  <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button" id="aa" style="min-height:40px; height: 40px;background-color: #20BDD4;color: #fff;
        padding-top: 0px; padding-bottom: 0px; line-height: 40px;">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
  </a>
  <div class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
            <li class="dropdown notifications-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-bell-o"></i>
                    <span class="label label-warning" id="notif">{{ count($notifikasi) }}</span>
                </a>
                <ul class="dropdown-menu lg">
                    <li class="header">
                        @if(empty($notifikasi))
                            <label class="control-label" id="notif_header"> Tidak ada notifikasi</label>
                        @else
                            <label class="control-label" id="notif_header"> Anda memiliki {{ count($notifikasi) }} notifikasi </label><a href="" class="pull-right"><span class="fa fa-close"></span></a>
                        @endif

                        <!-- inner menu: contains the actual data -->
                        <ul class="menu" id="data_notif" >
                            <li>
                                @foreach($notifikasi as $val)
                                    <a href="{{ url('/'.$val->link) }}" onclick="notif_klik('{{ $val->id }}')">
                                        <?php echo $val->pesan; ?>
                                    </a> 
                                @endforeach
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          @if(Auth::check())
          <li><a href="{{ url('/') }}"><i class="glyphicon glyphicon-arrow-left"></i> Kembali ke Halaman Utama</a></li>
          <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i> {{ Auth::user()->name }} <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="{{ url('logout') }}"><i class="glyphicon glyphicon-log-out" ></i> Logout</a></li>
                </ul>
              </li>
          @else
          <li><a href="{{ url('register') }}"><i class="glyphicon glyphicon-user" ></i> Registrasi</a></li>
          <li><a href="{{ url('login') }}"><i class="glyphicon glyphicon-log-in"></i> Login</a></li>
          @endif
          </ul>
  </div>
</nav>
</header>
<script type="text/javascript">
    function notif_klik(id)
    {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })

        var url = "{{ url('/nonaktif_notifikasi') }}";
        
        $.ajax({
            type: "POST",
            url: url,
            data: {id:id},
            success: function (data) {
            },
            error: function (data) {
                swal('Oops, Gagal menghapus notifikasi.');
            }
        });
    }
</script>