<style type="text/css">
    /*#slider-banner.owl-theme .owl-dots{
        margin-top: -40px;
        text-align: center;
    }*/

    #slider-banner.owl-theme .owl-nav .owl-next {
        color: #fff;
        right: 5px;
        top: 150px;
        font-size: 50pt;
        position: absolute;
        z-index: 100;
        background-color: rgba(173, 173, 173, 0.5);
    }

    #slider-banner.owl-theme .owl-nav .owl-next:hover{
        background-color: rgba(173, 173, 173, 0.9);
    }

    #slider-banner.owl-theme .owl-nav .owl-prev {
        color: #fff;
        left: 5px;
        top: 150px;
        font-size: 50pt;
        position: absolute;
        z-index: 100;
        background-color: rgba(173, 173, 173, 0.5);
    }

    #slider-banner.owl-theme .owl-nav .owl-prev:hover{
        background-color: rgba(173, 173, 173, 0.9);
    }
</style>
<section>
    <div class="">
        <div class="columns">
            <div class="owl-carousel owl-theme" id="slider-banner">
                @foreach($slider_promo as $val)
                    <div class="item" data-dot="{{ $val->nama }}">
                        <a href="{{ url('/slider-promo/'.$val->url) }}"><img src="{{ url('/img/banner/'.$val->file_gambar) }}" alt="{{ $val->nama }}" id="mybanner"></a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
<style type="text/css">
    #slider-banner.owl-theme .owl-dots {
        display: -webkit-box; display: flex; justify-content: space-around;
    }
    #slider-banner.owl-theme .owl-dots .owl-dot {
        display: inline-block;
        -webkit-box-flex: 1;
        flex-grow: 1;
        text-align: center;
        padding: 10px 15px;
        position: relative;
        -webkit-transition: color .2s;
        transition: color .2s;
        color: #808080;
        text-decoration: none;
        background-color: #ffffff;
        border: 1px solid transparent;
    }

    #slider-banner.owl-theme .owl-dots .owl-dot:hover {
        color: #20BDD4;
        border: 1px solid #20BDD4;
    }

    #slider-banner.owl-theme .owl-dots .owl-dot.active {
        background-color: #20BDD4;
        color: #ffffff;
    }

    #mybanner{
        height: 400px;
    }

    @media (max-width:900px){
        #mybanner{
            height: 350px;
        }
    }

    @media (max-width:600px){
        #mybanner{
            height: 250px;
        }
    }

    @media (max-width:400px){
        #mybanner{
            height: 200px;
        }
    }

    @media (max-width:300px){
        #mybanner{
            height: 170px;
        }
    }
</style>

<!-- vendors -->
<script src="{{ URL::to('owl-carousel2/docs/assets/vendors/highlight.js') }}"></script>
<script src="{{ URL::to('owl-carousel2/docs/assets/js/app.js') }}"></script>