<?php
    $bank = DB::table('tref_bank')->where('is_aktif', 1)->get();
?>
<style type="text/css">
    .footer_home {
        background-color: #1e1e1e;
        border-top: 3px solid #20BDD4;
    }
    .footer_home > li > a {
        color: white;
    }
    .footer_home > li > a:hover,
    .footer_home > li > a:focus {
        background-color: transparent;
        color: #E6E6E6;
    }
</style>
<br>
<div class="footer" style="padding: 0px;">
    <div class="footer-top">
        <img class="img-responsive" alt="Indonusa Solutama" src="{{ asset('/img/footer_town.png') }}" style="width: 100%"/>
        <div class="row" style="background-color: #1e1e1e">
            <div class="col-md-12">
                <div class="col-md-3 footer-left">
                    <h3>Tentang Kami</h3>
                    <ul class="nav nav-pills nav-stacked">
                        <li><a href="{{ url('/tentang_kami') }}">Tentang @if(!empty($perusahaan->nama)){{ $perusahaan->nama }} @endif</a></li>
                        <li><a href="{{ url('/hubungi_kami') }}">Karir</a></li>
                        <li><a href="{{ url('/faqs') }}">FAQ’s</a></li>
                    </ul>
                </div>
                <div class="col-md-3 footer-left">
                    <h3>Produk</h3>
                    <ul class="nav nav-pills nav-stacked">
                        <li><a href="{{ url('/list_paket_produk') }}">Paket Produk</a></li>
                        <li><a href="{{ url('/semua_kategori') }}">Produk</a></li>
                    </ul>
                </div>
                <div class="col-md-3 footer-left">
                    <h3>Ketentuan</h3>
                    <ul class="nav nav-pills nav-stacked">
                        <li><a href="{{ url('/ketentuan_pengguna') }}">Ketentuan Pengguna</a></li>
                        <li><a href="{{ url('/ketentuan_retur_barang') }}">Ketentuan Retur Barang</a></li>
                        <li><a href="{{ url('/sistem_pembayaran') }}">Sistem Pembayaran</a></li>
                        <!-- <li><a href="{{ url('/sistem_pembayaran') }}">Cara Pembelian</a></li> -->
                    </ul>
                </div>
                <div class="col-md-3 footer-left  lost">
                    <h3>Download Segera</h3>
                    <ul class="nav nav-pills nav-stacked">
                        <li>
                            <img src="{{ asset('/img/playstore.png') }}" style="height: 40px;">
                        </li>
                        <li>
                            <img src="{{ asset('/img/appstore.png') }}" style="height: 40px;">
                        </li>
                    </ul>
                </div>
                <div class="row" style="color: #888; padding-bottom: 10px;">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            Metode Pembayaran : 
                            @foreach($bank as $val)
                            <img src="{{ asset('/img/ref/'.$val->file_gambar) }}" style="height: 40px; width: auto; padding-left: 10px;">
                            @endforeach
                        </div>
                        <!-- <div class="col-md-6">
                            Jasa Pengiriman : 
                            @foreach($bank as $val)
                            <img src="{{ asset('/img/ref/'.$val->file_gambar) }}" style="height: 40px; width: auto; padding-left: 10px;">
                            @endforeach
                        </div> -->
                    </div>
                    <br>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
 <!--/start-copyright-->
<div class="copy" style="background-color: #20BDD4; margin: 0px;">
    <div class="container">
        <p align="center" style="padding-top: 10px;">Copyright © 2016 @if(!empty($perusahaan->nama)){{ $perusahaan->nama }} @endif</p>
    </div>
</div>
