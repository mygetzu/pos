<?php
    $hak_akses  = DB::table('hak_akses')->where('id', Auth::user()->hak_akses_id)->first();
    $menu_akses = $hak_akses->menu_akses;

    for ($i=1; $i <= 36; $i++) { 
        $menu[$i] = 0;
    }

    $menu_akses = explode('-', $menu_akses);

    for ($i=0; $i < count($menu_akses); $i++) { 
        $menu[$menu_akses[$i]] = 1;
    }

    for ($i=1; $i <= 8; $i++) { 
        $parent_menu[$i] = 1;
    }

    if($menu[1] == 0 && $menu[2] == 0 && $menu[3] == 0 && $menu[4] == 0){
        $parent_menu[1] = 0;
    }
    if($menu[5] == 0 && $menu[6] == 0 && $menu[7] == 0 && $menu[8] == 0){
        $parent_menu[2] = 0;
    }
    if($menu[9] == 0 && $menu[10] == 0 && $menu[11] == 0 && $menu[12] == 0 && $menu[13] == 0){
        $parent_menu[3] = 0;
    }
    if($menu[14] == 0 && $menu[15] == 0 && $menu[16] == 0 && $menu[17] == 0 && $menu[18] == 0 && $menu[19] == 0){
        $parent_menu[4] = 0;
    }
    if($menu[20] == 0 && $menu[21] == 0 && $menu[22] == 0 && $menu[23] == 0 && $menu[24] == 0 && $menu[25] == 0 && $menu[26] == 0 && $menu[27] == 0 && $menu[28] == 0 && $menu[29] == 0){
        $parent_menu[5] = 0;
    }
    if($menu[30] == 0 && $menu[31] == 0 && $menu[32] == 0){
        $parent_menu[6] = 0;
    }
    if($menu[33] == 0 && $menu[34] == 0 && $menu[35] == 0){
        $parent_menu[7] = 0;
    }
    if($menu[36] == 0){
        $parent_menu[8] = 0;
    }
?>

<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel" align="center">
        <div class="pull-left image">
          <img src="{{ URL::to('img/icon.png') }}" style="padding-top:5px" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{ Auth::user()->name }}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <br>
      <!-- search form -->
      <!-- <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form> -->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">Navigasi Utama</li>
        <li <?php if($parent_menu[1] == 0) echo "style='display: none;'"; ?> class="<?php if(substr($general['kode_menu'], 0, 1) == 1) echo 'active'; ?> treeview">
          <a href="#">
            <i class="fa fa-home"></i><span>Beranda</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li id="menu1" <?php if($menu[1] == 0) echo "style='display: none;'"; ?> <?php if($general['kode_menu'] == 11) echo "class='active'"; ?>><a href="{{ url('/beranda_admin') }}"><i class="fa fa-circle-o"></i> Dashboard</a></li>
            <li id="menu2" <?php if($menu[2] == 0) echo "style='display: none;'"; ?> <?php if($general['kode_menu'] == 12) echo "class='active'"; ?>><a href="{{ url('/perusahaan') }}"><i class="fa fa-circle-o"></i> Perusahaan</a></li>
            <li id="menu3" <?php if($menu[3] == 0) echo "style='display: none;'"; ?> <?php if($general['kode_menu'] == 13) echo "class='active'"; ?>><a href="{{ url('/chat') }}"><i class="fa fa-circle-o"></i> Chat</a></li>
          </ul>
        </li>
        <li class="<?php if(substr($general['kode_menu'], 0, 1) == 9) echo 'active'; ?> treeview">
          <a href="#">
            <i class="fa fa-shopping-cart"></i><span>E-Commerce</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li <?php if($general['kode_menu'] == 91) echo "class='active'"; ?>><a href="{{ url('/promo_creator') }}"><i class="fa fa-circle-o"></i> Promo Creator</a></li>
            <li <?php if($general['kode_menu'] == 92) echo "class='active'"; ?>><a href="{{ url('/konten') }}"><i class="fa fa-circle-o"></i> Konten</a></li>
          </ul>
        </li>
        <li  <?php if($parent_menu[2] == 0) echo "style='display: none;'"; ?> class="<?php if(substr($general['kode_menu'], 0, 1) == 2) echo 'active'; ?> treeview">
          <a href="#">
            <i class="fa fa-gear"></i><span>Pengaturan</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li id="menu4" <?php if($menu[4] == 0) echo "style='display: none;'"; ?> <?php if($general['kode_menu'] == 25) echo "class='active'"; ?>><a href="{{ url('/pengaturan_umum') }}"><i class="fa fa-circle-o"></i> Pengaturan Umum</a></li>
            <li id="menu5" style='display: none;' <?php if($general['kode_menu'] == 21) echo "class='active'"; ?>><a href="{{ url('/ecommerce') }}"><i class="fa fa-circle-o"></i> E-commerce</a></li>
            <li id="menu6" <?php if($menu[6] == 0) echo "style='display: none;'"; ?> <?php if($general['kode_menu'] == 23) echo "class='active'"; ?>><a href="{{ url('/group_account') }}"><i class="fa fa-circle-o"></i> Group Account</a></li>
            <li id="menu7" <?php if($menu[7] == 0) echo "style='display: none;'"; ?> <?php if($general['kode_menu'] == 22) echo "class='active'"; ?>><a href="{{ url('/master_coa') }}"><i class="fa fa-circle-o"></i> Master COA (Chart of Account)</a></li>
            <li id="menu8" <?php if($menu[8] == 0) echo "style='display: none;'"; ?> <?php if($general['kode_menu'] == 24) echo "class='active'"; ?>><a href="{{ url('/pengaturan_jurnal') }}"><i class="fa fa-circle-o"></i> Pengaturan Jurnal</a></li>
          </ul>
        </li>
        <li  <?php if($parent_menu[3] == 0) echo "style='display: none;'"; ?> class="<?php if(substr($general['kode_menu'], 0, 1) == 3) echo 'active'; ?> treeview">
          <a href="#">
            <i class="fa fa-users"></i><span>Akun dan Pelanggan</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li id="menu9" <?php if($menu[9] == 0) echo "style='display: none;'"; ?> <?php if($general['kode_menu'] == 31) echo "class='active'"; ?>><a href="{{ url('/akun_saya') }}"><i class="fa fa-circle-o"></i> Akun Saya</a></li>
            <li id="menu10" <?php if($menu[10] == 0) echo "style='display: none;'"; ?> <?php if($general['kode_menu'] == 32) echo "class='active'"; ?>><a href="{{ url('/data_user') }}"><i class="fa fa-circle-o"></i> Data Akun</a></li>
            <li id="menu11" <?php if($menu[11] == 0) echo "style='display: none;'"; ?> <?php if($general['kode_menu'] == 33) echo "class='active'"; ?>><a href="{{ url('/hak_akses') }}"><i class="fa fa-circle-o"></i> Hak Akses</a></li>
            <li id="menu12" <?php if($menu[12] == 0) echo "style='display: none;'"; ?> <?php if($general['kode_menu'] == 34) echo "class='active'"; ?>><a href="{{ url('/kategori_pelanggan') }}"><i class="fa fa-circle-o"></i> Kategori Pelanggan</a></li>
            <li id="menu13" <?php if($menu[13] == 0) echo "style='display: none;'"; ?> <?php if($general['kode_menu'] == 35) echo "class='active'"; ?>><a href="{{ url('/pelanggan') }}"><i class="fa fa-circle-o"></i> Pelanggan</a></li>
          </ul>
        </li>
        <li  <?php if($parent_menu[4] == 0) echo "style='display: none;'"; ?> class="<?php if(substr($general['kode_menu'], 0, 1) == 4) echo 'active'; ?> treeview">
          <a href="#">
            <i class="fa fa-archive"></i><span>Master</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li id="menu14" <?php if($menu[14] == 0) echo "style='display: none;'"; ?> <?php if($general['kode_menu'] == 41) echo "class='active'"; ?>><a href="{{ url('/supplier') }}"><i class="fa fa-circle-o"></i> Supplier</a></li>
            <li id="menu15" <?php if($menu[15] == 0) echo "style='display: none;'"; ?> <?php if($general['kode_menu'] == 42) echo "class='active'"; ?>><a href="{{ url('/kategori_produk') }}"><i class="fa fa-circle-o"></i> Kategori Produk</a></li>
            <li id="menu16" <?php if($menu[16] == 0) echo "style='display: none;'"; ?> <?php if($general['kode_menu'] == 43) echo "class='active'"; ?>><a href="{{ url('/gudang') }}"><i class="fa fa-circle-o"></i> Gudang</a></li>
            <li id="menu17" <?php if($menu[17] == 0) echo "style='display: none;'"; ?> <?php if($general['kode_menu'] == 44) echo "class='active'"; ?>><a href="{{ url('/produk') }}"><i class="fa fa-circle-o"></i> Produk</a></li>
            <li id="menu18" <?php if($menu[18] == 0) echo "style='display: none;'"; ?> <?php if($general['kode_menu'] == 45) echo "class='active'"; ?>><a href="{{ url('/hadiah') }}"><i class="fa fa-circle-o"></i> Hadiah</a></li>
            <li id="menu19" <?php if($menu[19] == 0) echo "style='display: none;'"; ?> <?php if($general['kode_menu'] == 46) echo "class='active'"; ?>><a href="{{ url('/paket_produk') }}"><i class="fa fa-circle-o"></i> Paket Produk</a></li>
          </ul>
        </li>
        <li  <?php if($parent_menu[5] == 0) echo "style='display: none;'"; ?> class="<?php if(substr($general['kode_menu'], 0, 1) == 5) echo 'active'; ?> treeview">
          <a href="#">
            <i class="fa fa-money"></i><span>Transaksi</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li id="menu20" <?php if($menu[20] == 0) echo "style='display: none;'"; ?> <?php if($general['kode_menu'] == 51) echo "class='active'"; ?>><a href="{{ url('/purchase_order') }}"><i class="fa fa-circle-o"></i> Purchase Order</a></li>
            <li id="menu21" <?php if($menu[21] == 0) echo "style='display: none;'"; ?> <?php if($general['kode_menu'] == 57) echo "class='active'"; ?>><a href="{{ url('/surat_jalan_masuk') }}"><i class="fa fa-circle-o"></i> Surat Jalan Masuk</a></li>
            <li id="menu22" <?php if($menu[22] == 0) echo "style='display: none;'"; ?> <?php if($general['kode_menu'] == 58) echo "class='active'"; ?>><a href="{{ url('/nota_beli') }}"><i class="fa fa-circle-o"></i> Nota Beli</a></li>
            <li id="menu27" <?php if($menu[27] == 0) echo "style='display: none;'"; ?> <?php if($general['kode_menu'] == 54) echo "class='active'"; ?>><a href="{{ url('/retur_beli') }}"><i class="fa fa-circle-o"></i> Retur Beli</a></li>
            <li <?php if($general['kode_menu'] == 511) echo "class='active'"; ?>><a href="{{ url('/pembayaran_supplier') }}"><i class="fa fa-circle-o"></i> Pembayaran Supplier</a></li>
            <li id="menu23" <?php if($menu[23] == 0) echo "style='display: none;'"; ?> <?php if($general['kode_menu'] == 52) echo "class='active'"; ?>><a href="{{ url('/sales_order') }}"><i class="fa fa-circle-o"></i> Sales Order</a></li>
            <li id="menu24" <?php if($menu[24] == 0) echo "style='display: none;'"; ?> <?php if($general['kode_menu'] == 59) echo "class='active'"; ?>><a href="{{ url('/surat_jalan_keluar') }}"><i class="fa fa-circle-o"></i> Surat Jalan Keluar</a></li>
            <li id="menu25" <?php if($menu[25] == 0) echo "style='display: none;'"; ?> <?php if($general['kode_menu'] == 510) echo "class='active'"; ?>><a href="{{ url('/nota_jual') }}"><i class="fa fa-circle-o"></i> Nota Jual</a></li>
            <li id="menu26" <?php if($menu[26] == 0) echo "style='display: none;'"; ?> <?php if($general['kode_menu'] == 53) echo "class='active'"; ?>><a href="{{ url('/retur_jual') }}"><i class="fa fa-circle-o"></i> Retur Jual</a></li>
            <li id="menu28" <?php if($menu[28] == 0) echo "style='display: none;'"; ?> <?php if($general['kode_menu'] == 55) echo "class='active'"; ?>><a href="{{ url('/servis') }}"><i class="fa fa-circle-o"></i> Servis</a></li>
            <li <?php if($general['kode_menu'] == 512) echo "class='active'"; ?>><a href="{{ url('/pembayaran_pelanggan') }}"><i class="fa fa-circle-o"></i> Pembayaran Pelanggan</a></li>
            <li <?php if($general['kode_menu'] == 513) echo "class='active'"; ?>><a href="{{ url('/transaksi_jasa') }}"><i class="fa fa-circle-o"></i> Transaksi Jasa</a></li>
            <li id="menu29" <?php if($menu[29] == 0) echo "style='display: none;'"; ?> <?php if($general['kode_menu'] == 56) echo "class='active'"; ?>><a href="{{ url('/voucher') }}"><i class="fa fa-circle-o"></i> Voucher</a></li>
          </ul>
        </li>
        <li  <?php if($parent_menu[6] == 0) echo "style='display: none;'"; ?> class="<?php if(substr($general['kode_menu'], 0, 1) == 6) echo 'active'; ?> treeview">
          <a href="#">
            <i class="fa fa-cube"></i><span>Stok</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li id="menu30" <?php if($menu[30] == 0) echo "style='display: none;'"; ?> <?php if($general['kode_menu'] == 61) echo "class='active'"; ?>><a href="{{ url('/penyesuaian_stok') }}"><i class="fa fa-circle-o"></i> Penyesuaian Stok</a></li>
            <li id="menu31" <?php if($menu[31] == 0) echo "style='display: none;'"; ?> <?php if($general['kode_menu'] == 62) echo "class='active'"; ?>><a href="{{ url('/mutasi_stok') }}"><i class="fa fa-circle-o"></i> Mutasi Stok</a></li>
            <li id="menu32" <?php if($menu[32] == 0) echo "style='display: none;'"; ?> <?php if($general['kode_menu'] == 63) echo "class='active'"; ?>><a href="{{ url('/stok_opname') }}"><i class="fa fa-circle-o"></i> Stok Opname</a></li>
          </ul>
        </li>
        <li  <?php if($parent_menu[7] == 0) echo "style='display: none;'"; ?> class="<?php if(substr($general['kode_menu'], 0, 1) == 7) echo 'active'; ?> treeview">
          <a href="#">
            <i class="fa fa-line-chart"></i><span>Laporan</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li id="menu33" <?php if($menu[33] == 0) echo "style='display: none;'"; ?> <?php if($general['kode_menu'] == 71) echo "class='active'"; ?>><a href="{{ url('/laporan_stok') }}"><i class="fa fa-circle-o"></i> Laporan Stok</a></li>
            <li id="menu34" <?php if($menu[34] == 0) echo "style='display: none;'"; ?> <?php if($general['kode_menu'] == 72) echo "class='active'"; ?>><a href="{{ url('/kartu_stok') }}"><i class="fa fa-circle-o"></i> Kartu Stok</a></li>
            <li <?php if($general['kode_menu'] == 74) echo "class='active'"; ?>><a href="{{ url('/laporan_pembelian') }}"><i class="fa fa-circle-o"></i> Laporan Pembelian</a></li>
            <li id="menu35" <?php if($menu[35] == 0) echo "style='display: none;'"; ?> <?php if($general['kode_menu'] == 73) echo "class='active'"; ?>><a href="{{ url('/laporan_penjualan') }}"><i class="fa fa-circle-o"></i> Laporan Penjualan</a></li>
            <li <?php if($general['kode_menu'] == 75) echo "class='active'"; ?>><a href="{{ url('/laporan_surat_jalan_masuk') }}"><i class="fa fa-circle-o"></i> Laporan Surat Jalan Masuk</a></li>
            <li <?php if($general['kode_menu'] == 76) echo "class='active'"; ?>><a href="{{ url('/laporan_surat_jalan_keluar') }}"><i class="fa fa-circle-o"></i> Laporan Surat Jalan Keluar</a></li>
            <li <?php if($general['kode_menu'] == 77) echo "class='active'"; ?>><a href="{{ url('/laporan_servis') }}"><i class="fa fa-circle-o"></i> Laporan Servis</a></li>
            <li <?php if($general['kode_menu'] == 78) echo "class='active'"; ?>><a href="{{ url('/laporan_pembayaran_supplier') }}"><i class="fa fa-circle-o"></i> Laporan Pembayaran Supplier</a></li>
            <li <?php if($general['kode_menu'] == 79) echo "class='active'"; ?>><a href="{{ url('/laporan_pembayaran_pelanggan') }}"><i class="fa fa-circle-o"></i> Laporan Pembayaran<br><p style="margin-left: 22px;">Pelanggan</p></a></li>
          </ul>
        </li>
        <li  <?php if($parent_menu[8] == 0) echo "style='display: none;'"; ?> class="<?php if(substr($general['kode_menu'], 0, 1) == 8) echo 'active'; ?> treeview">
          <a href="#">
            <i class="fa fa-book"></i><span>Accounting</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li id="menu36" <?php if($menu[36] == 0) echo "style='display: none;'"; ?> <?php if($general['kode_menu'] == 81) echo "class='active'"; ?>><a href="{{ url('/jurnal_umum') }}"><i class="fa fa-circle-o"></i> Jurnal Umum</a></li>
            <li <?php if($general['kode_menu'] == 82) echo "class='active'"; ?>><a href="{{ url('/laporan_jurnal_umum') }}"><i class="fa fa-circle-o"></i> Laporan Jurnal Umum</a></li>
            <li <?php if($general['kode_menu'] == 83) echo "class='active'"; ?>><a href="{{ url('/laporan_buku_besar') }}"><i class="fa fa-circle-o"></i> Laporan Buku Besar</a></li>
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>