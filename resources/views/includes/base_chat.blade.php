<script type="text/javascript">
    var output;
    var websocket;

    function WebSocketSupport()
    {
        if (browserSupportsWebSockets() === false) {
            document.getElementById("ws_support").innerHTML = "<h2>Sorry! Your web browser does not supports web sockets</h2>";

            var element = document.getElementById("wrapper");
            element.parentNode.removeChild(element);

            return;
        }

        output      = document.getElementById("chatbox");
        var path    =  document.getElementById('base_path2').value;

        if(path == "::1")
        {
            address = 'ws:localhost:443';
        }
        else
        {
            address = 'ws:130.211.105.244:443';
        }
        websocket = new WebSocket(address);

        websocket.onopen = function(e) {
            writeToScreen("You have have successfully connected to the server");
            //get log chat
            $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            })
            var my_id = document.getElementById('my_id').value;
            var url = "{{ url('/get_log_chat') }}";
            
            $.ajax({
              type: "POST",
              url: url,
              data:{id:1},
              dataType: 'json',
              success: function (data) {
                if(data){
                    for (var i = 0; i < data.length; i++) {

                        if(my_id == data[i].user_id)
                        {
                            var msg = '<div class="direct-chat-msg right">\
                              <div class="direct-chat-info clearfix">\
                                <span class="direct-chat-name pull-right">'+ data[i].nama +'</span>\
                                <span class="direct-chat-timestamp pull-left">'+ data[i].tanggal +'</span>\
                              </div>\
                              <div class="direct-chat-text" style="margin-right:5px; margin-left:40px;">'
                                + data[i].pesan +
                              '</div>\
                            </div>';
                        }
                        else {
                        var msg = '<div class="direct-chat-msg">\
                              <div class="direct-chat-info clearfix">\
                                <span class="direct-chat-name pull-left">'+ data[i].nama +'</span>\
                                <span class="direct-chat-timestamp pull-right">'+ data[i].tanggal +'</span>\
                              </div>\
                              <div class="direct-chat-text" style="margin-left:5px; margin-right:40px;">'
                                + data[i].pesan +
                              '</div>\
                            </div>';
                        }

                      $('#log_chatbox').append(msg);
                    }
                    document.getElementById("boxbody").scrollTop = document.getElementById("boxbody").scrollHeight;
                }
              },
              error: function (data) {
                  //alert('gagal menampilkan log chat');
              }
            });
        };


        websocket.onmessage = function(e) {
            onMessage(e)
        };

        websocket.onerror = function(e) {
            onError(e)
        };

        //cek chat dibuka atau tidak
          cek_status_notif = document.getElementById('status_chat_box').value;

          if(cek_status_notif == '0')
          {
            
              $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              })

              var url = "{{ url('/get_notif') }}";
              var path_master =  document.getElementById('base_path_master').value;
              
              $.ajax({
                  type: "POST",
                  url: url,
                  dataType: 'json',
                  success: function (data) {
                      var jml_notif = 0;
                      if(data){
                        for (var i = data.length - 1; i >= 0; i--) {
                          jml_notif++;
                        }
                        if(jml_notif == 0)
                             document.getElementById('customer_notif').innerHTML = "";
                        else
                            document.getElementById('customer_notif').innerHTML = jml_notif;
                      }
                  },
                  error: function (data) {
                     console.log(data)
                  }
              });
          }
    }

    function onMessage(e) 
    {
        var obj   = JSON.parse(e.data);
        var my_id = document.getElementById('my_id').value;

        if(obj.id_to == my_id)
        {
          var currentdate = new Date(); 
          var datetime = currentdate.getDate() + "/"
                + (currentdate.getMonth()+1)  + "/" 
                + currentdate.getFullYear() + " "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds();

          //cek chat dibuka atau tidak
          var cek_status_notif = document.getElementById('status_chat_box').value;

          if(cek_status_notif == '0')
          { 
            //save notif
              var msg_notif = '{"id_from": "'+ my_id +'","tanggal": "'+ datetime +'","pesan": "'+ obj.msg +'"}';

              $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              })

              var url = "{{ url('/save_notif') }}";
              var path_master =  document.getElementById('base_path_master').value;
              
              $.ajax({
                  type: "POST",
                  url: url,
                  dataType: 'json',
                  data: { msg_notif: msg_notif, user_id_from : obj.id_from },
                  success: function (data) {
                      
                  },
                  error: function (data) {
                      console.log(data)
                  }
              });
          }

            //save message
          var msg_notif = '{"user_id": "'+ obj.id_from +'","tanggal": "'+ datetime +'","pesan": "'+ obj.msg +'"}';

          $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
          })

          var url = "{{ url('/save_log_chat') }}";

          $.ajax({
              type: "POST",
              url: url,
              data: { user_id_a: my_id, user_id_b : obj.id_from, pesan : msg_notif },
              success: function (data) {
              },
              error: function (data) {
                  alert('ooo');
              }
          });

          var msg = '<div class="direct-chat-msg">\
                  <div class="direct-chat-info clearfix">\
                    <span class="direct-chat-name pull-left">'+ obj.name_from +'</span>\
                    <span class="direct-chat-timestamp pull-right">'+ datetime +'</span>\
                  </div>\
                  <div class="direct-chat-text" style="margin-left:5px; margin-right:40px;">'
                    + obj.msg +
                  '</div>\
                </div>';



          writeToScreen(msg, obj.id_from);
          //scrol to bottom
            document.getElementById("boxbody").scrollTop = document.getElementById("boxbody").scrollHeight;
        }
    }

    function onError(e) {
        //writeToScreen('<span style="color: red;">ERROR:</span> ' + e.data);
        writeToScreen('<span style="color: red;">Fitur masih dalam perbaikan</span>');
    }

    function doSend(message) {
        var validationMsg = userInputSupplied();
        if (validationMsg !== '') {
            alert(validationMsg);
            return;
        }
        var chatname = document.getElementById('chatname').value;

        document.getElementById('msg').value = "";

        document.getElementById('msg').focus();
        var currentdate = new Date(); 
        var datetime = currentdate.getDate() + "/"
                + (currentdate.getMonth()+1)  + "/" 
                + currentdate.getFullYear() + " "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds();

        
        var my_id    = document.getElementById('my_id').value;
        var my_name    = document.getElementById('my_name').value;

        var msg = '{"id_from": "'+ my_id +'","id_to": "1","name_from": "'+ my_name +'","msg": "'+ message +'"}';

        websocket.send(msg);

        var msg2 = '<div class="direct-chat-msg right">\
                  <div class="direct-chat-info clearfix">\
                    <span class="direct-chat-name pull-right">'+ chatname +'</span>\
                    <span class="direct-chat-timestamp pull-left">'+ datetime +'</span>\
                  </div>\
                  <div class="direct-chat-text" style="margin-right:5px; margin-left:40px;">'
                    + message +
                  '</div>\
                </div>';

        writeToScreen(msg2);

        //scrol to bottom
        document.getElementById("boxbody").scrollTop = document.getElementById("boxbody").scrollHeight;
    }

    function writeToScreen(message) {
        var pre = document.createElement("p");
        pre.style.wordWrap = "break-word";
        pre.innerHTML = message;
        output.appendChild(pre);
    }

    function userInputSupplied() {
        var chatname = document.getElementById('chatname').value;
        var msg = document.getElementById('msg').value;
        if (chatname === '') {
            return 'Please enter your username';
        } else if (msg === '') {
            return 'Please the message to send';
        } else {
            return '';
        }
    }

    function browserSupportsWebSockets() {
        if ("WebSocket" in window)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
</script>
<style type="text/css">
body
{
    font-family: 'Open Sans', sans-serif;
    }
.popup-box {
   background-color: #ffffff;
    border: 1px solid #b0b0b0;
    bottom: 0;
    display: none;
    height: 415px;
    position: fixed;
    right: 70px;
    width: 300px;
    font-family: 'Open Sans', sans-serif;
    z-index: 200;
}
.round.hollow {
    margin: 40px 0 0;
}
.round.hollow a {
    border: 2px solid #ff6701;
    border-radius: 35px;
    color: red;
    color: #ff6701;
    font-size: 23px;
    padding: 10px 21px;
    text-decoration: none;
    font-family: 'Open Sans', sans-serif;
}
.round.hollow a:hover {
    border: 2px solid #000;
    border-radius: 35px;
    color: red;
    color: #000;
    font-size: 23px;
    padding: 10px 21px;
    text-decoration: none;
}
.popup-box-on {
    display: block !important;
}
.popup-box .popup-head {
    background-color: #fff;
    clear: both;
    color: #7b7b7b;
    display: inline-table;
    font-size: 21px;
    padding: 7px 10px;
    width: 100%;
     font-family: Oswald;
}
.bg_none i {
    border: 1px solid #ff6701;
    border-radius: 25px;
    color: #ff6701;
    font-size: 17px;
    height: 33px;
    line-height: 30px;
    width: 33px;
}
.bg_none:hover i {
    border: 1px solid #000;
    border-radius: 25px;
    color: #000;
    font-size: 17px;
    height: 33px;
    line-height: 30px;
    width: 33px;
}
.bg_none {
    background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
    border: medium none;
}
.popup-box .popup-head .popup-head-right {
    margin: 11px 7px 0;
}
.popup-box .popup-messages {
}
.popup-head-left img {
    border: 1px solid #7b7b7b;
    border-radius: 50%;
    width: 44px;
}
.popup-messages-footer > textarea {
    border-bottom: 1px solid #b2b2b2 !important;
    height: 34px !important;
    margin: 7px;
    padding: 5px !important;
     border: medium none;
    width: 95% !important;
}
.popup-messages-footer {
    background: #fff none repeat scroll 0 0;
    bottom: 0;
    position: absolute;
    width: 100%;
}
.popup-messages-footer .btn-footer {
    overflow: hidden;
    padding: 2px 5px 10px 6px;
    width: 100%;
}
.simple_round {
    background: #d1d1d1 none repeat scroll 0 0;
    border-radius: 50%;
    color: #4b4b4b !important;
    height: 21px;
    padding: 0 0 0 1px;
    width: 21px;
}





.popup-box .popup-messages {
    background: #3f9684 none repeat scroll 0 0;
    height: 275px;
    overflow: auto;
}
.direct-chat-messages {
    overflow: auto;
    padding: 10px;
    transform: translate(0px, 0px);
    
}
.popup-messages .chat-box-single-line {
    border-bottom: 1px solid #a4c6b5;
    height: 12px;
    margin: 7px 0 20px;
    position: relative;
    text-align: center;
}
.popup-messages abbr.timestamp {
    background: #3f9684 none repeat scroll 0 0;
    color: #fff;
    padding: 0 11px;
}

.popup-head-right .btn-group {
    display: inline-flex;
    margin: 0 8px 0 0;
    vertical-align: top !important;
}
.chat-header-button {
    background: transparent none repeat scroll 0 0;
    border: 1px solid #636364;
    border-radius: 50%;
    font-size: 14px;
    height: 30px;
    width: 30px;
}
.popup-head-right .btn-group .dropdown-menu {
    border: medium none;
    min-width: 122px;
    padding: 0;
}
.popup-head-right .btn-group .dropdown-menu li a {
    font-size: 12px;
    padding: 3px 10px;
    color: #303030;
}

.popup-messages abbr.timestamp {
    background: #3f9684  none repeat scroll 0 0;
    color: #fff;
    padding: 0 11px;
}
.popup-messages .chat-box-single-line {
    border-bottom: 1px solid #a4c6b5;
    height: 12px;
    margin: 7px 0 20px;
    position: relative;
    text-align: center;
}
.popup-messages .direct-chat-messages {
    height: auto;
}
.popup-messages .direct-chat-text {
    background: #dfece7 none repeat scroll 0 0;
    border: 1px solid #dfece7;
    border-radius: 2px;
    color: #1f2121;
}

.popup-messages .direct-chat-timestamp {
    color: #fff;
    opacity: 0.6;
}

.popup-messages .direct-chat-name {
    font-size: 15px;
    font-weight: 600;
    margin: 0 0 0 49px !important;
    color: #fff;
    opacity: 0.9;
}
.popup-messages .direct-chat-info {
    display: block;
    font-size: 12px;
    margin-bottom: 0;
}
.popup-messages  .big-round {
    margin: -9px 0 0 !important;
}
.popup-messages  .direct-chat-img {
    border: 1px solid #fff;
    background: #3f9684  none repeat scroll 0 0;
    border-radius: 50%;
    float: left;
    height: 40px;
    margin: -21px 0 0;
    width: 40px;
}
.direct-chat-reply-name {
    color: #fff;
    font-size: 15px;
    margin: 0 0 0 10px;
    opacity: 0.9;
}

.direct-chat-img-reply-small
{
    border: 1px solid #fff;
    border-radius: 50%;
    float: left;
    height: 20px;
    margin: 0 8px;
    width: 20px;
    background:#3f9684;
}

.popup-messages .direct-chat-msg {
    margin-bottom: 10px;
    position: relative;
}

.popup-messages .doted-border::after {
    background: transparent none repeat scroll 0 0 !important;
    border-right: 2px dotted #fff !important;
    bottom: 0;
    content: "";
    left: 17px;
    margin: 0;
    position: absolute;
    top: 0;
    width: 2px;
     display: inline;
      z-index: 200;
}

.popup-messages .direct-chat-msg::after {
    background: #fff none repeat scroll 0 0;
    border-right: medium none;
    bottom: 0;
    content: "";
    left: 17px;
    margin: 0;
    position: absolute;
    top: 0;
    width: 2px;
     display: inline;
      z-index: 200;
}
.direct-chat-text::after, .direct-chat-text::before {
   
    border-color: transparent #dfece7 transparent transparent;
    
}
.direct-chat-text::after, .direct-chat-text::before {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    border-color: transparent #d2d6de transparent transparent;
    border-image: none;
    border-style: solid;
    border-width: medium;
    content: " ";
    height: 0;
    pointer-events: none;
    position: absolute;
    right: 100%;
    top: 15px;
    width: 0;
}
.direct-chat-text::after {
    border-width: 5px;
    margin-top: -5px;
}
.popup-messages .direct-chat-text {
    background: #dfece7 none repeat scroll 0 0;
    border: 1px solid #dfece7;
    border-radius: 2px;
    color: #1f2121;
}
.direct-chat-text {
    background: #d2d6de none repeat scroll 0 0;
    border: 1px solid #d2d6de;
    border-radius: 5px;
    color: #444;
    margin: 5px 0 0 50px;
    padding: 5px 10px;
    position: relative;
}

</style>
<script type="text/javascript">
      $(function(){
        $("#addClass").click(function () {
          $('#qnimate').addClass('popup-box-on');
                  //hapus riwayat notif
                  $.ajaxSetup({
                      headers: {
                          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                      }
                  })
                  
                  //save message
                  var url = "{{ url('/delete_pelanggan_notif/1') }}";
                  $.ajax({
                      type: "POST",
                      url: url,
                      success: function (data) {
                        document.getElementById('status_chat_box').value = '1';
                        document.getElementById('customer_notif').innerHTML = "";
                      },
                      error: function (data) {
                          //alert('gagal hapus notif');
                      }
                  });
            });
          
        $("#removeClass").click(function () {
          $('#qnimate').removeClass('popup-box-on');
          document.getElementById('status_chat_box').value = '0';
            });
        
        $("#msg").keyup(function(event){
            if(event.keyCode == 13){
                $("#sendmsg").click();
            }
        });
  })
</script>
<link rel="stylesheet" href="{{ asset('/adminlte/dist/css/AdminLTE.min.css') }}">
@if(Auth::check())
@if(Auth::user()->hak_akses_id != 1)
<input type="hidden" name="base_path" id="base_path" value="<?php echo $_SERVER['REMOTE_ADDR']; ?>">
<input type="hidden" name="base_path_master" id="base_path_master" value="{{ asset('/') }}">
<input type="hidden" name="my_id" id="my_id" value="{{ Auth::user()->id }}">
<input type="hidden" name="my_name" id="my_name" value="{{ Auth::user()->name }}">
<input type="hidden" id="status_chat_box" value="0">
<div class="popup-box popup-box-on" style="height:50px">
    <div class="box box-primary direct-chat direct-chat-primary flat">
        <div class="box-header with-border" id="addClass" style="cursor: pointer; ">
            <h3 class="box-title">Chat</h3>
        </div>
    </div>
</div>
<div class="popup-box chat-popup" id="qnimate">
    <div class="box box-primary direct-chat direct-chat-primary flat" style="z-index: 200">
        <div class="box-header with-border" id="removeClass" style="cursor: pointer; ">
            <h3 class="box-title">Chat</h3>
        </div>
        <div class="box-body">
            <div class="direct-chat-messages" id="boxbody">
                <div id="log_chatbox"></div>
                <div id="chatbox"></div>
            </div>
        </div>
        <div class="box-footer">
            <div class="input-group">
                <input name="chatname" type="hidden" id="chatname" value="{{ Auth::user()->name }}" />
                <input type="text" name="msg" id="msg" placeholder="Tulis Pesan ..." class="form-control" >
                <span class="input-group-btn">
                    <button type="submit" name="sendmsg" id="sendmsg" class="btn btn-success btn-flat" onclick="doSend(document.getElementById('msg').value)">Kirim</button>
                </span>
            </div>
        </div>
    </div>
</div>
@endif
@endif