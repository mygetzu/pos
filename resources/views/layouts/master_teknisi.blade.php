<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php 
        $perusahaan = DB::table('tmst_perusahaan')->first(); 
        //sementara masih static
        $warna_1        = "#20BDD4";
        $warna_2        = "#808080";
        $warna_huruf    = "#4D4D4D";
        $kategori_produk = DB::table('tmst_kategori_produk')->where('is_aktif', 1)->get();
    ?> 
    <title>@if(!empty($perusahaan)){{ $perusahaan->nama }}@endif</title>
    <link rel="shortcut icon" type="image/x-icon" href="{{ URL::to('img/icon.png') }}" />
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="{{ asset('/adminlte/bootstrap/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('/adminlte/dist/css/AdminLTE.min.css') }}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ asset('/adminlte/dist/css/skins/_all-skins.min.css') }}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ asset('/adminlte/plugins/iCheck/flat/blue.css') }}">
    <!-- Morris chart -->
    <link rel="stylesheet" href="{{ asset('/adminlte/plugins/morris/morris.css') }}">
    <!-- jvectormap -->
    <link rel="stylesheet" href="{{ asset('/adminlte/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
    <!-- Date Picker -->
    <link rel="stylesheet" href="{{ asset('/adminlte/plugins/datepicker/datepicker3.css') }}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{ asset('/adminlte/plugins/daterangepicker/daterangepicker-bs3.css') }}">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="{{ asset('/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::to('assets/css/bootstrap-datetimepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/adminpanel.css') }}">
    <!-- Sweetalert -->
    <link rel="stylesheet" href="{{ URL::asset('sweetalert-master/dist/sweetalert.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('sweetalert-master/themes/twitter/twitter.css') }}">
    <script src="{{ URL::asset('sweetalert-master/dist/sweetalert.min.js') }}"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition sidebar-mini skin-black" onload="javascript:WebSocketSupport()">
<script type="text/javascript">
  var websocket;

  function WebSocketSupport()
  {
      if (browserSupportsWebSockets() === false) {
          document.getElementById("ws_support").innerHTML = "<h2>Sorry! Your web browser does not supports web sockets</h2>";

          var element = document.getElementById("wrapper");
          element.parentNode.removeChild(element);

          return;
      }

//      output = document.getElementById("chatbox");
      var path =  document.getElementById('base_path2').value;

      if(path == "::1")
      {
          address = 'ws:localhost:443';
      }
      else
      {
          address = 'ws:130.211.105.244:443';
      }

      websocket = new WebSocket(address);
      var my_id    = document.getElementById('my_id').value;

      websocket.onopen = function(e) { 
          writeToScreen("You have have successfully connected to the server", my_id);
          //get notif
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      })

      var url = "{{ url('/get_notif') }}";
      var path_master =  document.getElementById('base_path_master').value;

      $.ajax({
          type: "POST",
          url: url,
          dataType: 'json',
          success: function (data) {
            var jml_notif = 0;
            if(data){
              for (var i = data.length - 1; i >= 0; i--) {
                jml_notif++
                $('#data_notif_chat').append('<li><a href="'+path_master+'show_chat/'+data[i].id+'">\
                <b style="color:blue">('+data[i].tanggal+') </b><b>'+ data[i].nama +': \
                </b>'+ data[i].pesan +'</a></li>');
              }
              document.getElementById('notif_chat').innerHTML = jml_notif;
              if(jml_notif == 0)
                document.getElementById('notif_chat_header').innerHTML = "";
              else
                document.getElementById('notif_chat_header').innerHTML = "Anda memiliki "+jml_notif+" pesan baru";
            }
          },
          error: function (data) {
              alert('gagal menampilkan notifikasi ws');
          }
      });
      };


      websocket.onmessage = function(e) {
          onMessage(e)
      };

      websocket.onerror = function(e) {
          onError(e)
      };         
  }

  function onMessage(e) 
  {
        var obj   = JSON.parse(e.data);
        var my_id = document.getElementById('my_id').value;

        if(obj.id_to == my_id)
        {
          var currentdate = new Date(); 
          var datetime = currentdate.getDate() + "/"
                + (currentdate.getMonth()+1)  + "/" 
                + currentdate.getFullYear() + " "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds();

          //save notif
          var msg_notif = '{"id_from": "'+ my_id +'","tanggal": "'+ datetime +'","pesan": "'+ obj.msg +'"}';

          $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
          })
          
          var url = "{{ url('/save_notif') }}";
          var path_master =  document.getElementById('base_path_master').value;
          
          $.ajax({
              type: "POST",
              url: url,
              dataType: 'json',
              data: { msg_notif: msg_notif, user_id_from : obj.id_from },
              success: function (data) {
                  var jml_notif = 0;
                  if(data){
                    for (var i = data.length - 1; i >= 0; i--) {
                      jml_notif++
                      $('#data_notif_chat').append('<li><a href="'+path_master+'show_chat/'+data[i].id+'">\
                      <b style="color:blue">('+data[i].tanggal+') </b><b>'+ data[i].nama +': \
                      </b>'+ data[i].pesan +'</a></li>');
                    }
                    document.getElementById('notif_chat').innerHTML = jml_notif;
                    document.getElementById('notif_chat_header').innerHTML = "Anda memiliki "+jml_notif+" pesan baru";
                  }
              },
              error: function (data) {
                  alert('gagal menyimpan notifikasi');
              }
          });

          $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
          })
          //save message
          var msg_notif = '{"user_id": "'+ obj.id_from +'","tanggal": "'+ datetime +'","pesan": "'+ obj.msg +'"}';
          var url = "{{ url('/save_log_chat') }}";
          $.ajax({
              type: "POST",
              url: url,
              data: { user_id_a: my_id, user_id_b : obj.id_from, pesan : msg_notif },
              success: function (data) {
              },
              error: function (data) {
                  alert('gagal menyimpan pesan');
              }
          });


          var msg = '<div class="direct-chat-msg">\
                  <div class="direct-chat-info clearfix">\
                    <span class="direct-chat-name pull-left">'+ obj.name_from +'</span>\
                    <span class="direct-chat-timestamp pull-right">'+ datetime +'</span>\
                  </div>\
                  <div class="direct-chat-text" style="margin-left:5px; margin-right:40px;">'
                    + obj.msg +
                  '</div>\
                </div>';

          writeToScreen(msg, obj.id_from);
          
          document.getElementById("boxbody".concat(obj.id_from)).scrollTop = document.getElementById("boxbody".concat(obj.id_from)).scrollHeight;
        }
    }

    function onError(e) {
        var my_id    = document.getElementById('my_id').value;
        writeToScreen('<span style="color: red;">ERROR:</span> ' + e.data,my_id);
    }

    function doSend(id) 
    { 
        var message = document.getElementById('msg'.concat(id)).value;

        var validationMsg = userInputSupplied(id);
        if (validationMsg !== '') {
            alert(validationMsg);
            return;
        }
        
        var chatname = document.getElementById('chatname'.concat(id)).value;
        output = document.getElementById("chatbox".concat(id));
        document.getElementById('msg'.concat(id)).value = "";

        document.getElementById('msg'.concat(id)).focus();

        var currentdate = new Date(); 
        var datetime = currentdate.getDate() + "/"
                + (currentdate.getMonth()+1)  + "/" 
                + currentdate.getFullYear() + " "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds();

        var my_id    = document.getElementById('my_id').value;
        var my_name    = document.getElementById('my_name').value;

        var msg = '{"id_from": "'+ my_id +'","id_to": "'+ id +'","name_from": "'+ my_name +'","msg": "'+ message +'"}';
        
        // var msg = '<div class="direct-chat-msg">\
        //           <div class="direct-chat-info clearfix">\
        //             <span class="direct-chat-name pull-left">'+ chatname +'</span>\
        //             <span class="direct-chat-timestamp pull-right">'+ datetime +'</span>\
        //           </div>\
        //           <div class="direct-chat-text">'
        //             + message +
        //           '</div>\
        //         </div>';

        websocket.send(msg);


        var msg2 = '<div class="direct-chat-msg right">\
                  <div class="direct-chat-info clearfix">\
                    <span class="direct-chat-name pull-right">'+ chatname +'</span>\
                    <span class="direct-chat-timestamp pull-left">'+ datetime +'</span>\
                  </div>\
                  <div class="direct-chat-text" style="margin-right:5px; margin-left:40px;">'
                    + message +
                  '</div>\
                </div>';
        writeToScreen(msg2, id);
        
       document.getElementById("boxbody".concat(id)).scrollTop = document.getElementById("boxbody".concat(id)).scrollHeight;
    }

    function writeToScreen(message, id) { 
        var pre = document.createElement("p");
        pre.style.wordWrap = "break-word";
        pre.innerHTML = message;
        output = document.getElementById("chatbox".concat(id));
        output.appendChild(pre);
    }

    function userInputSupplied(id) {
        var chatname = document.getElementById('chatname'.concat(id)).value;
        var msg = document.getElementById('msg'.concat(id)).value;
        if (chatname === '') {
            return 'Please enter your username';
        } else if (msg === '') {
            return 'Tulis pesan terlebih dahulu';
        } else {
            return '';
        }
    }

    function browserSupportsWebSockets() {
        if ("WebSocket" in window)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
</script>
<div class="wrapper">

<input type="hidden" name="base_path2" id="base_path2" value="<?php echo $_SERVER['REMOTE_ADDR']; ?>">
<input type="hidden" name="base_path_master" id="base_path_master" value="{{ asset('/') }}">
<input type="hidden" name="my_id" id="my_id" value="{{ Auth::user()->id }}">
<input type="hidden" name="my_name" id="my_name" value="{{ Auth::user()->name }}">
@include('includes.header_teknisi')
@include('includes.sidebar_teknisi_temp')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

      <h1>{{ $general['title'] }}</h1>
                <ol class="breadcrumb">
                <li><i class="fa fa-dashboard"></i> {{ $general['menu1'] }}</li>
                @if(!empty($general['menu2']))
                <li class="active">{{ $general['menu2'] }}</li>
                @endif
                @if(!empty($general['menu3']))
                <li class="active">{{ $general['menu3'] }}</li>
                @endif
                </ol>
    </section>

    <!-- Main content -->
    <section class="content">
          @yield('content')
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @include('includes.footer_admin')
  
</div>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset('/adminlte/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- Morris.js charts -->
<script src="{{ asset('/adminlte/https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/morris/morris.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('/adminlte/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('/adminlte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('/adminlte/plugins/knob/jquery.knob.js') }}"></script>
<!-- daterangepicker -->
<script src="{{ asset('/adminlte/https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- datepicker -->
<script src="{{ asset('/adminlte/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<!-- Slimscroll -->
<script src="{{ asset('/adminlte/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('/adminlte/plugins/fastclick/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('/adminlte/dist/js/app.min.js') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ asset('/adminlte/dist/js/pages/dashboard.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('/adminlte/dist/js/demo.js') }}"></script>
<meta name="_token" content="{!! csrf_token() !!}" />
<script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<!-- InputMask -->
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>   

</body>
</html>
