<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="">
    <meta name="author" content="">
    <title>POS</title>
    <link rel="stylesheet" href="{{ URL::to('bootstrap/css/bootstrap.min.css') }}">
    <meta name="keywords" content="Nuevo Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
    Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
    <script type="applijegleryion/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <link href="{{ URL::to('nuevo/css/bootstrap.css') }}" rel='stylesheet' type='text/css' />
    <!-- Custom Theme files -->
    <link href="{{ URL::to('nuevo/css/style.css') }}" rel='stylesheet' type='text/css' />  
    <script src="{{ URL::to('nuevo/js/jquery-1.11.1.min.js') }}"></script>
    <!-- start menu -->
    <link href="{{ URL::to('nuevo/css/megamenu.css') }}" rel="stylesheet" type="text/css" media="all" />
    <script type="text/javascript" src="{{ URL::to('nuevo/js/megamenu.js') }}"></script>
    <script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
    <script src="{{ URL::to('nuevo/js/menu_jquery.js') }}"></script>
    <script src="{{ URL::to('nuevo/js/simpleCart.min.js') }}"> </script>
    <!--web-fonts-->
     <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,300italic,600,700' rel='stylesheet' type='text/css'>
     <link href='//fonts.googleapis.com/css?family=Roboto+Slab:300,400,700' rel='stylesheet' type='text/css'>
    <!--//web-fonts-->
     <script src="{{ URL::to('nuevo/js/scripts.js') }}" type="text/javascript"></script>
    <script src="{{ URL::to('nuevo/js/modernizr.custom.js') }}"></script>
    <script type="text/javascript" src="{{ URL::to('nuevo/js/move-top.js') }}"></script>
    <script type="text/javascript" src="{{ URL::to('nuevo/js/easing.js') }}"></script>
    <!--/script-->
    <script type="text/javascript">
                jQuery(document).ready(function($) {
                    $(".scroll").click(function(event){     
                        event.preventDefault();
                        $('html,body').animate({scrollTop:$(this.hash).offset().top},900);
                    });
                });
    </script>

    <!-- Owl Carousel Assets -->
    <link href="{{ URL::to('owl-carousel/owl-carousel/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{ URL::to('owl-carousel/owl-carousel/owl.theme.css') }}" rel="stylesheet">
    <!-- Prettify -->
    <link href="{{ URL::to('owl-carousel/assets/js/google-code-prettify/prettify.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ URL::to('nuevo/css/flexslider.css') }}" type="text/css" media="screen" />
    <?php 
        $perusahaan = DB::table('tmst_perusahaan')->first(); 
        //sementara masih static
        $warna_1        = "#20BDD4";
        $warna_2        = "#808080";
        $warna_huruf    = "#4D4D4D";
    ?> 
</head><!--/head-->
<body  onload="javascript:WebSocketSupport()" style="background-size: 200px;">
    @include('includes.header_home')
    <section>
        <div class="container">
            @if(!empty($kategori))<h4>Kategori <span class="fa fa-chevron-right"></span> {{ $kategori }} <span class="fa fa-chevron-right"></span> {{ $produk->nama }}</h4>
            @endif
        </div><br>
        <div class="container"">
            <div class="col-md-12" style="background-color: white; border: 1px solid #aaa; min-height: 500px;">
                @yield('content')
            </div>
        </div>
    </section>
    @include('includes.footer_home')
    @include('includes.base_chat')
    <a href="#home" id="toTop" class="scroll" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
    <input type="hidden" name="base_path2" id="base_path2" value="<?php echo $_SERVER['REMOTE_ADDR']; ?>">
    <meta name="_token" content="{!! csrf_token() !!}" />
    <script type="text/javascript">
        $(document).ready(function() {
            simpleCart.currency({
                code: "IDR" ,
                symbol: "Rp " ,
                delimiter: "." , 
                decimal: "," , 
                after: false ,
                accuracy: 0
            });

            $("#owl-thumb").owlCarousel({
                items : 4,
                lazyLoad : true,
                navigation : true,
                rewindNav : false,
                navigationText : ["<span class='fa fa-angle-left'></span>", "<span class='fa fa-angle-right'></span>"], 
            });
        });
    </script>
</body>
</html>