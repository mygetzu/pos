-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versi server:                 10.1.19-MariaDB - mariadb.org binary distribution
-- OS Server:                    Win32
-- HeidiSQL Versi:               9.4.0.5144
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Membuang struktur basisdata untuk cctv_indonusa
CREATE DATABASE IF NOT EXISTS `cctv_indonusa` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `cctv_indonusa`;

-- membuang struktur untuk table cctv_indonusa.camera
CREATE TABLE IF NOT EXISTS `camera` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `firmware` text COLLATE utf8_unicode_ci,
  `floor` tinyint(4) DEFAULT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `width_mini` varchar(50) COLLATE utf8_unicode_ci DEFAULT '1',
  `height_mini` varchar(50) COLLATE utf8_unicode_ci DEFAULT '1',
  `width` varchar(50) COLLATE utf8_unicode_ci DEFAULT '1',
  `height` varchar(50) COLLATE utf8_unicode_ci DEFAULT '1',
  `stream_protocol` text COLLATE utf8_unicode_ci,
  `stream_url` text COLLATE utf8_unicode_ci,
  `ddns_url` text COLLATE utf8_unicode_ci,
  `url_stream` text COLLATE utf8_unicode_ci,
  `is_active` tinyint(4) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel cctv_indonusa.camera: ~10 rows (lebih kurang)
DELETE FROM `camera`;
/*!40000 ALTER TABLE `camera` DISABLE KEYS */;
INSERT INTO `camera` (`id`, `name`, `type`, `firmware`, `floor`, `ip_address`, `location`, `width_mini`, `height_mini`, `width`, `height`, `stream_protocol`, `stream_url`, `ddns_url`, `url_stream`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'Kamera Pintu Masuk', 'ICA-M3380P', '7.G.1.21368', 1, '192.168.1.11', 'Pintu Depan', '645', '485', '2048', '1536', 'rtsp', '/onvif-media/media.amp?streamprofile=Profile1&audio=0', 'indonusacctv.ddns.net', 'http://192.168.1.11/mjpg/video.mjpg&resolution=640x480', 1, '2017-01-24 15:41:53', '2017-01-31 11:33:17'),
	(2, 'Kamera Prisad', 'ICA-8200', 'A1.0.0_0822', 1, '192.168.1.12', 'Ruangan Prisad', '640', '480', '1280', '1024', 'rtsp', '/live1.sdp', 'indonusacctv12.mysecuritycamera.com', 'rtsp://192.168.1.12/live1.sdp', 1, '2017-01-24 12:05:11', '2017-01-31 11:33:18'),
	(3, 'Kamera Ruang Depan', 'ICA-M4320P', '7.G.1.21368', 1, '192.168.1.13', 'Ruangan Dian', '645', '485', '1280', '1024', 'rtsp', '/onvif-media/media.amp?streamprofile=Profile1&audio=0', 'indonusacctv13.mysecuritycamera.com', 'http://192.168.1.13/mjpg/video.cgi&resolution=640x480', 1, '2017-02-03 14:50:50', '2017-02-03 14:50:50'),
	(4, 'Kamera Loker', 'ICA-8500', 'A1.0.0_0731', 1, '192.168.1.14', 'Ruang Loker', '640', '480', '1280', '1024', 'rtsp', '/live1.sdp', 'indonusacctv14.mysecuritycamera.com', 'rtsp://192.168.1.14/live1.sdp', 1, '2017-02-03 14:50:54', '2017-02-03 14:50:52'),
	(5, 'Kamera Gudang', 'ICA-5260V', 'VG1.0.16_PL', 1, '192.168.1.15', 'Ruangan Gudang Lantai 1', '640', '480', '1280', '1024', 'rtsp', ':554/', 'indonusacctv15.planetddns.com', 'rtsp://192.168.1.15:554/', 1, '2017-02-03 14:50:56', '2017-02-03 14:50:57'),
	(6, 'Kamera Ruang Luar Lantai 2', 'ICA-3250', 'PL_V113', 2, '192.168.1.21', 'Ruang Luar Lantai 2', '640', '480', '1280', '1024', 'rtsp', '/stander/livestream/0/0', 'indonusacctv21.planetddns.com', 'rtsp://192.168.1.21:554/stander/livestream/0/0', 1, '2017-02-03 14:50:59', '2017-02-03 14:50:59'),
	(7, 'Kamera Lantai 2', 'ICA-8350', '6.M.2.14910', 2, '192.168.1.22', 'Ruang Kerja Lantai 2', '773', '773', '1536', '1536', 'rtsp', '/media.amp?streamprofile=Profile1&audio=0', 'indonusacctv22.mysecuritycamera.com', 'http://192.168.1.22/mjpg/video.cgi&resolution=640x480', 1, '2017-02-03 14:51:03', '2017-02-03 14:51:03'),
	(8, 'Kamera Ruang Bu Vera', 'ICA-4200V', '6.J.0.19804', 3, '192.168.1.31', 'Ruang Kerja & Gudang Lantai 3', '873', '773', '1280', '1024', 'rtsp', '/media.amp?streamprofile=Profile1&audio=0', 'indonusacctv31.mysecuritycamera.com', 'http://192.168.1.31/mjpg/video.mjpg&resolution=640x360', 1, '2017-02-03 14:51:05', '2017-02-03 14:51:06'),
	(9, 'Kamera Ruang Meeting Lantai 3', 'ICA-4250', 'PL_V113', 3, '192.168.1.32', 'Ruang Meeting', '640', '480', '1280', '1024', 'rtsp', '/stander/livestream/0/0', 'indonusacctv32.planetddns.com', 'rtsp://192.168.1.32:554/stander/livestream/0/0', 1, '2017-02-03 14:51:08', '2017-02-03 14:51:09'),
	(10, 'Kamera Lorong Lantai 3', 'ICA-4250', 'PL_V113', 3, '192.168.1.33', 'Lorong Lantai 3', '640', '480', '1280', '1024', 'rtsp', ':554/stander/livestream/0/0', 'indonusacctv33.planetddns.com', 'rtsp://192.168.1.33:554/stander/livestream/0/0', 1, '2017-02-03 14:51:11', '2017-02-03 14:51:11');
/*!40000 ALTER TABLE `camera` ENABLE KEYS */;

-- membuang struktur untuk table cctv_indonusa.group
CREATE TABLE IF NOT EXISTS `group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_default` tinyint(4) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel cctv_indonusa.group: ~3 rows (lebih kurang)
DELETE FROM `group`;
/*!40000 ALTER TABLE `group` DISABLE KEYS */;
INSERT INTO `group` (`id`, `name`, `is_default`, `created_at`, `updated_at`) VALUES
	(1, 'Grup - Pintu Depan', 0, '2017-01-31 08:14:29', '2017-01-31 12:12:47'),
	(3, 'Grup - Area Kerja', 1, '2017-01-31 11:33:59', '2017-01-31 12:13:00'),
	(4, 'Grup - Gudang & Koridor', 0, '2017-01-31 11:34:57', '2017-01-31 11:34:57');
/*!40000 ALTER TABLE `group` ENABLE KEYS */;

-- membuang struktur untuk table cctv_indonusa.group_camera
CREATE TABLE IF NOT EXISTS `group_camera` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) DEFAULT NULL,
  `camera_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel cctv_indonusa.group_camera: ~11 rows (lebih kurang)
DELETE FROM `group_camera`;
/*!40000 ALTER TABLE `group_camera` DISABLE KEYS */;
INSERT INTO `group_camera` (`id`, `group_id`, `camera_id`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, '2017-01-31 08:14:29', '2017-01-31 11:23:00'),
	(2, 1, 3, '2017-01-31 08:14:29', '2017-01-31 11:23:00'),
	(3, 1, 4, '2017-01-31 08:14:29', '2017-01-31 11:23:00'),
	(4, 1, 5, '2017-01-31 08:14:29', '2017-01-31 11:23:00'),
	(5, 3, 3, '2017-01-31 11:33:59', '2017-01-31 11:33:59'),
	(6, 3, 2, '2017-01-31 11:33:59', '2017-01-31 11:33:59'),
	(7, 3, 7, '2017-01-31 11:33:59', '2017-01-31 11:33:59'),
	(8, 3, 6, '2017-01-31 11:33:59', '2017-01-31 11:33:59'),
	(9, 4, 5, '2017-01-31 11:34:57', '2017-01-31 11:34:57'),
	(10, 4, 10, '2017-01-31 11:34:57', '2017-01-31 11:34:57'),
	(11, 4, 8, '2017-01-31 11:34:57', '2017-01-31 11:34:57'),
	(12, 4, 9, '2017-01-31 11:34:57', '2017-01-31 11:34:57');
/*!40000 ALTER TABLE `group_camera` ENABLE KEYS */;

-- membuang struktur untuk table cctv_indonusa.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel cctv_indonusa.migrations: ~7 rows (lebih kurang)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2017_01_23_085609_create_cctv_table', 1),
	(4, '2017_01_23_090048_create_group_cctv_table', 1),
	(5, '2017_01_23_090307_create_record_cctv_table', 1),
	(6, '2017_01_23_143612_rename_cctv_table', 2),
	(7, '2017_01_23_143849_rename_group_cctv_table', 2),
	(8, '2017_01_23_144052_rename_record_cctv_table', 2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- membuang struktur untuk table cctv_indonusa.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel cctv_indonusa.password_resets: ~0 rows (lebih kurang)
DELETE FROM `password_resets`;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- membuang struktur untuk table cctv_indonusa.record_camera
CREATE TABLE IF NOT EXISTS `record_camera` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cctv_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel cctv_indonusa.record_camera: ~0 rows (lebih kurang)
DELETE FROM `record_camera`;
/*!40000 ALTER TABLE `record_camera` DISABLE KEYS */;
/*!40000 ALTER TABLE `record_camera` ENABLE KEYS */;

-- membuang struktur untuk table cctv_indonusa.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` text COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `level` tinyint(4) NOT NULL DEFAULT '2' COMMENT '1 = Admin | 2 = View',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel cctv_indonusa.users: ~2 rows (lebih kurang)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `level`, `created_at`, `updated_at`) VALUES
	(1, 'Admin Master', 'admin', '$2y$10$k/MzLsaxLtt8WMYgodSjJuek0sqHwZdcSoiLjSXaudTHOXAJArJgy', '1NHoDOvBUOBqJlPnnUe2dRLHJkLsfGbv9zsl97eN89tLYFCClM0sbgC2Re73', 1, '2017-02-03 13:21:52', '2017-02-04 09:14:38'),
	(2, 'User', 'user', '$2y$10$PC0sp4/rjj2m2YLtKdlokO3AnL/fFLu.TSwLW687fnfqlQpn0RmX.', 'W7XRV4wmy6ofK6X70Ip1UBGHYJvdbtvhSFpQQtsf0bDMvpsTNlzPV52MfMd2', 2, '2017-02-03 13:22:11', '2017-02-04 08:53:33');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
