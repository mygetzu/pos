<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MutasiStok extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tran_mutasi_stok_header', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('no_mutasi_stok', 255); 
            $table->integer('gudang_id_asal'); //foreign key tmst_gudang
            $table->integer('gudang_id_tujuan'); //foreign key tmst_gudang
            $table->dateTime('tanggal')->nullable();
            $table->string('approval_by_user_id', 255)->nullable(); //foreign key users
            $table->string('received_by_user_id', 255)->nullable(); //foreign key users
            $table->timestamps();
        });

        Schema::create('tran_mutasi_stok_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('mutasi_stok_header_id');
            $table->string('no_mutasi_stok', 255); 
            $table->integer('produk_id');
            $table->string('serial_number', 255);
            $table->integer('jumlah');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tran_mutasi_stok_header');
        Schema::drop('tran_mutasi_stok_detail');
    }
}
