<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTranServiceOrder2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        date_default_timezone_set('Asia/Jakarta');
        Schema::table('tran_service_order', function (Blueprint $table) {
            $table->string('no_nota', 100);
            $table->integer('pelanggan_id');
            $table->integer('teknisi_id')->nullable()->default(NULL)->comment('Kosongkan jika belum dikerjakan ke teknisi');
            $table->text('model_produk');
            $table->string('serial_number', 100)->comment('Dibandingkan dengan produk jual, jika ada maka tampilkan data pelanggan pembeli.', 255);
            $table->smallInteger('status_garansi_produk')->comment('0 = Tidak bergaransi || 1 = Masih bergaransi');
            $table->longText('keluhan_pelanggan');
            $table->longText('deskripsi_produk');
            $table->string('kode_verifikasi', 100)->comment('Kode verifikasi digunakan untuk pelanggan dalam melacak perbaikan', 255);
            $table->smallInteger('is_check')->default(0)->comment('Untuk meihat, apakah pelanggan telah melihat status perbaikan yang telah dikirim ke email atau belum?');
            $table->smallInteger('flag_case')->default(0)->comment('0 = Open Case || 1 = Close Case');
            $table->string('status_service', 50)->default('Belum')->comment('Belum || Proses || Selesai || Batal. Jika status = Belum || Batal, jangan cek ke tabel service_teknisi', 255);
            $table->date('garansi_service');
            $table->dateTime('tanggal_diterima')->default(date('Y-m-d H:i:s'));
            $table->dateTime('tanggal_selesai')->default(date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s').' +5 day')));
            $table->dateTime('tanggal_diambil')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tran_service_order', function (Blueprint $table) {
            $table->dropColumn('no_nota');
            $table->dropColumn('pelanggan_id');
            $table->dropColumn('teknisi_id');
            $table->dropColumn('model_produk');
            $table->dropColumn('serial_number');
            $table->dropColumn('status_garansi_produk');
            $table->dropColumn('keluhan_pelanggan');
            $table->dropColumn('deskripsi_produk');
            $table->dropColumn('kode_verifikasi');
            $table->dropColumn('is_check');
            $table->dropColumn('flag_case');
            $table->dropColumn('status_service');
            $table->dropColumn('garansi_service');
            $table->dropColumn('tanggal_diterima');
            $table->dropColumn('tanggal_selesai');
            $table->dropColumn('tanggal_diambil');
        });
    }
}
