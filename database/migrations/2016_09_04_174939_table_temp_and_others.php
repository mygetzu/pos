<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableTempAndOthers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notif_chat', function (Blueprint $table) {
            $table->integer('user_id'); //foreign key users
            $table->integer('user_id_from'); //foreign key users
            $table->text('pesan');
        });

        Schema::create('log_chat', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id_a'); //foreign key users
            $table->integer('user_id_b'); //foreign key users
            $table->longText('pesan');
            $table->timestamps();
        });

        Schema::create('temp_stok_opname', function (Blueprint $table) {
            $table->integer('id');
            $table->string('gudang_id', 255); //foreign key tmst_gudang
            $table->string('produk_id', 255); //foreign key tmst_produk
            $table->string('serial_number', 255);
            $table->integer('stok');
            $table->timestamps();
        });

        Schema::create('halaman_promo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('file_gambar', 255);
            $table->string('is_aktif', 255);
            $table->integer('item_order');
            $table->timestamps();
        });

        Schema::create('template_layout', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama', 255);
            $table->string('keterangan', 255);
        });

        Schema::create('template_background', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama', 255);
            $table->string('keterangan', 255);
            $table->string('file_gambar', 255);
        });

        Schema::create('template_tombol', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama', 255);
            $table->string('keterangan', 255);
            $table->string('jenis', 255); //flat, rounded, 3d
            $table->string('is_icon_button', 255); //tampilkan sembunyikan icon pada button
            $table->string('btn_primary', 255);
            $table->string('btn_default', 255);
            $table->string('btn_warning', 255);
            $table->string('btn_danger', 255);
            $table->string('btn_info', 255);
        });

        Schema::create('tema', function (Blueprint $table) {
            $table->string('warna_dasar', 255); //untuk header footer
            $table->string('warna_huruf', 255);
            $table->string('logo', 255);
            $table->string('icon', 255);
            $table->integer('layout_id'); //foreign key template_layout
            $table->integer('background_id'); //foreign key template_background
            $table->integer('tombol_id'); //foreign key template_tombol
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('notif_chat');
        Schema::drop('log_chat');
        Schema::drop('temp_stok_opname');
        Schema::drop('halaman_promo');
        Schema::drop('template_layout');
        Schema::drop('template_background');
        Schema::drop('template_tombol');
        Schema::drop('tema');
    }
}
