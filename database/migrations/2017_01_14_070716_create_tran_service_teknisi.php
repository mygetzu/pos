<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTranServiceTeknisi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tran_service_teknisi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('service_order_id');
            $table->varchar('status', 100);
            $table->text('kerusakan_produk');
            $table->smallInteger('flag_service');
            $table->dateTime('tanggal_diterima');
            $table->dateTime('tanggal_selesai');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tran_service_teknisi');
    }
}
