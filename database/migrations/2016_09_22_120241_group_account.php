<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GroupAccount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_account', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode', 255)->nullable();
            $table->string('nama', 255)->nullable();
            $table->string('debit_or_kredit', 255)->nullable();
            $table->string('group_kode', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('group_account');
    }
}
