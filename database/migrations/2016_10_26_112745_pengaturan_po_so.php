<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PengaturanPoSo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tref_pengaturan_po', function (Blueprint $table) {
            $table->increments('id');
            $table->string('alamat_pengiriman', 255)->nullable();
            $table->string('syarat_ketentuan', 255)->nullable();
            $table->string('ppn', 255)->nullable();
            $table->timestamps();
        });

        Schema::create('tref_pengaturan_so', function (Blueprint $table) {
            $table->increments('id');
            $table->string('syarat_ketentuan', 255)->nullable();
            $table->string('ppn', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tref_pengaturan_po');
        Schema::drop('tref_pengaturan_so');
    }
}
