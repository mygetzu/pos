<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFlagServis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tran_service_order', function($table) {
            $table->integer('flag')->nullable();
            $table->integer('is_lunas')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tran_service_order', function($table) {
            $table->dropColumn('flag');
            $table->dropColumn('is_lunas');
        });
    }
}
