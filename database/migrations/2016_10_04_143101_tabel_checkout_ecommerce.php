<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TabelCheckoutEcommerce extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_alamat_pengiriman', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('no_nota', 255)->nullable();
            $table->dateTime('tanggal')->nullable();
            $table->string('nama', 255)->nullable();
            $table->string('email', 255)->nullable();
            $table->string('alamat', 255)->nullable();
            $table->integer('kota_id')->nullable();
            $table->string('kode_pos', 255)->nullable();
            $table->string('hp', 255)->nullable();
            $table->integer('metode_pengiriman_id')->nullable();
            $table->timestamps();
        });

        Schema::create('temp_pembayaran', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('no_nota', 255)->nullable();
            $table->bigInteger('tanggal')->nullable();
            $table->string('metode_pembayaran_id', 255)->nullable();
            $table->string('bayar_id', 255)->nullable();
            $table->string('nomor_pembayaran', 255)->nullable();
            $table->integer('bank_id')->nullable();
            $table->timestamps();
        });

        Schema::create('tref_metode_pengiriman', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama', 255)->nullable();
            $table->bigInteger('tarif_dasar')->nullable();
            $table->string('keterangan', 255)->nullable();
            $table->string('is_aktif', 255)->nullable();
            $table->string('file_gambar', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('temp_alamat_pengiriman');
        Schema::drop('temp_pembayaran');
        Schema::drop('tref_metode_pengiriman');
    }
}
