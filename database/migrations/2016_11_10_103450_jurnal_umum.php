<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class JurnalUmum extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tran_jurnal_umum_header', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('tanggal')->nullable();
            $table->integer('is_editable')->nullable();
            $table->timestamps();
        });

        Schema::create('tran_jurnal_umum_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('jurnal_umum_header_id');
            $table->string('no_jurnal', 255)->nullable();
            $table->dateTime('tanggal')->nullable();
            $table->string('akun_kode', 255)->nullable();
            $table->string('akun_nama', 255)->nullable();
            $table->string('referensi', 255)->nullable();
            $table->bigInteger('debit')->nullable();
            $table->bigInteger('kredit')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tran_jurnal_umum_header');
        Schema::drop('tran_jurnal_umum_detail');
    }
}
