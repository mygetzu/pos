<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TambahBeberapaTabelKolom extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tmst_perusahaan', function($table) {
            $table->string('telp2', 255)->nullable();
        });

        Schema::create('tran_pelanggan_cart', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('pelanggan_id')->nullable();
            $table->bigInteger('produk_id')->nullable();
            $table->integer('jenis_barang_id')->nullable();
            $table->integer('jumlah')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tmst_perusahaan', function($table) {
            $table->dropColumn('telp2');
        });

        Schema::drop('tran_pelanggan_cart');
    }
}
