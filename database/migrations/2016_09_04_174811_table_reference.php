<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableReference extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tref_provinsi', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode', 255);
            $table->string('nama', 255);
            $table->timestamps();
        });

        Schema::create('tref_kota', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode', 255);
            $table->string('provinsi_kode', 255); //foreign key tref_provinsi (pakai kode)
            $table->string('nama', 255);
            $table->timestamps();
        });

        Schema::create('tref_jenis_barang', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama', 255);
            $table->string('keterangan', 255);
            $table->timestamps();
        });

        Schema::create('tref_sumber_data', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama', 255);
            $table->timestamps();
        });

        Schema::create('tref_bank', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode', 255);
            $table->string('nama', 255);
            $table->string('nama_singkat', 255);
            $table->string('keterangan', 255);
            $table->integer('is_aktif');
            $table->string('file_gambar', 255);
            $table->timestamps();
        });

        Schema::create('tref_metode_pembayaran', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama', 255);
            $table->string('keterangan', 255);
            $table->integer('is_aktif');
            $table->string('file_gambar', 255);
            $table->string('jenis', 255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tref_provinsi');
        Schema::drop('tref_kota');
        Schema::drop('tref_jenis_barang');
        Schema::drop('tref_sumber_data');
        Schema::drop('tref_bank');
        Schema::drop('tref_metode_pembayaran');
    }
}
