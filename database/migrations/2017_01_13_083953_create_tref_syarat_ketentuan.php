<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrefSyaratKetentuan extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        if (Schema::hasTable('tref_syarat_ketentuan')) {
            //
        } else {
            Schema::create('tref_syarat_ketentuan', function (Blueprint $table) {
                $table->increments('id');
                $table->string('perihal', 50);
                $table->text('konten');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('tref_syarat_ketentuan');
    }

}
