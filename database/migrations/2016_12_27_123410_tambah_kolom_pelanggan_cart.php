<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TambahKolomPelangganCart extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tran_pelanggan_cart', function($table) {
            $table->bigInteger('harga_retail');
            $table->bigInteger('harga_akhir');
            $table->string('jenis_promo', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tran_pelanggan_cart', function ($table) {
           $table->dropColumn('harga_retail');
           $table->dropColumn('harga_akhir');
           $table->dropColumn('jenis_promo');
        });
    }
}
