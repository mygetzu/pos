<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TabelSpesifikasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tran_parameter_spesifikasi', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('kategori_produk_id')->nullable();
            $table->string('nama', 255)->nullable();
            $table->timestamps();
        });

        Schema::create('tran_nilai_spesifikasi', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('produk_id')->nullable();
            $table->bigInteger('parameter_spesifikasi_id')->nullable();
            $table->string('nilai', 255)->nullable();
            $table->string('satuan', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tran_parameter_spesifikasi');
        Schema::drop('tran_nilai_spesifikasi');
    }
}
