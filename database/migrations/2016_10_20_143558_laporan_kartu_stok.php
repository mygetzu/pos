<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LaporanKartuStok extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tran_laporan_stok_header', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('gudang_id')->nullable(); //foreign key tmst_gudang
            $table->dateTime('tanggal')->nullable();
            $table->integer('total_stok')->nullable();
            $table->timestamps();
        });

        Schema::create('tran_laporan_stok_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('laporan_stok_header_id')->nullable();
            $table->integer('produk_id')->nullable();
            $table->integer('jumlah')->nullable();
            $table->timestamps();
        });

        Schema::create('tran_kartu_stok_header', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('gudang_id')->nullable(); //foreign key tmst_gudang
            $table->integer('produk_id')->nullable(); //foreign key tmst_produk
            $table->dateTime('tanggal_awal')->nullable();
            $table->dateTime('tanggal_akhir')->nullable();
            $table->integer('total_stok')->nullable();
            $table->timestamps();
        });

        Schema::create('tran_kartu_stok_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('kartu_stok_header_id'); //foreign key tran_kartu_stok_header
            $table->integer('sumber_data_id')->nullable();
            $table->string('supplier_or_pelanggan', 255)->nullable();
            $table->string('no_nota', 255)->nullable();
            $table->string('serial_number', 255)->nullable();
            $table->dateTime('tanggal')->nullable();
            $table->integer('jumlah')->nullable();
            $table->bigInteger('harga')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tran_laporan_stok_header');
        Schema::drop('tran_laporan_stok_detail');
        Schema::drop('tran_kartu_stok_header');
        Schema::drop('tran_kartu_stok_detail');
    }
}
