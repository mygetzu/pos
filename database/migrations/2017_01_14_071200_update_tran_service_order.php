<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTranServiceOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tran_service_order', function (Blueprint $table) {
            $table->dropColumn('no_nota');
            $table->dropColumn('pelanggan_id');
            $table->dropColumn('tanggal');
            $table->dropColumn('serial_number');
            $table->dropColumn('keluhan');
            $table->dropColumn('deskripsi_penyelesaian');
            $table->dropColumn('tanggal_selesai');
            $table->dropColumn('jasa_service');
            $table->dropColumn('catatan');
            $table->dropColumn('garansi');
            $table->dropColumn('model');
            $table->dropColumn('no_jurnal');
            $table->dropColumn('flag');
            $table->dropColumn('is_lunas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tran_service_order', function (Blueprint $table) {
            $table->string('no_nota', 255);
            $table->integer('pelanggan_id');
            $table->dateTime('tanggal');
            $table->string('serial_number', 255);
            $table->text('keluhan');
            $table->text('deskripsi_penyelesaian');
            $table->dateTime('tanggal_selesai');
            $table->bigInteger('jasa_service');
            $table->text('catatan');
            $table->string('garansi', 255);
            $table->string('model', 255);
            $table->string('no_jurnal', 255);
        });
    }
}
