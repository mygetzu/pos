<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PenyesuaianStok extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tran_penyesuaian_stok_header', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_penyesuaian_stok', 255)->nullable();
            $table->integer('gudang_id')->nullable();
            $table->dateTime('tanggal')->nullable();
            $table->text('keterangan')->nullable();
            $table->integer('approval_by_user_id')->nullable();
            $table->integer('received_by_user_id')->nullable();
            $table->timestamps();
        });

        Schema::create('tran_penyesuaian_stok_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('penyesuaian_stok_header_id')->nullable();
            $table->bigInteger('produk_id')->nullable();
            $table->string('serial_number', 255)->nullable();
            $table->integer('stok_awal')->nullable();
            $table->integer('stok_ubah')->nullable();
            $table->integer('stok_baru')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tran_penyesuaian_stok_header');
        Schema::drop('tran_penyesuaian_stok_detail');
    }
}
