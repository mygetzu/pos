<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Notifikasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifikasi', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode', 255)->nullable();
            $table->text('pesan')->nullable();
            $table->integer('user_id_target')->nullable();
            $table->integer('hak_akses_id_target')->nullable();
            $table->string('link', 255)->nullable();
            $table->integer('is_aktif')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('notifikasi');
    }
}
