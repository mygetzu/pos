<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableTransaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tran_produk_galeri', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('produk_id')->nullable(); //foreign key tmst_produk
            $table->string('file_gambar', 255)->nullable();
            $table->timestamps();
        });

        Schema::create('tran_promo_hadiah', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('produk_id')->nullable(); //foreign key tmst_produk
            $table->integer('hadiah_id')->nullable(); //foreign key tmst_hadiah
            $table->string('kode_promo', 255)->nullable();
            $table->integer('qty_beli')->nullable();
            $table->integer('qty_hadiah')->nullable();
            $table->dateTime('awal_periode')->nullable();
            $table->dateTime('akhir_periode')->nullable();
            $table->timestamps();
        });

        Schema::create('tran_promo_diskon', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('produk_id')->nullable(); //foreign key tmst_produk
            $table->string('kode_promo', 255)->nullable();
            $table->integer('qty_beli')->nullable();
            $table->integer('diskon')->nullable();
            $table->dateTime('awal_periode')->nullable();
            $table->dateTime('akhir_periode')->nullable();
            $table->timestamps();
        });

        Schema::create('tran_promo_cashback', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('produk_id')->nullable(); //foreign key tmst_produk
            $table->string('kode_promo', 255)->nullable();
            $table->integer('qty_beli')->nullable();
            $table->integer('cashback')->nullable();
            $table->dateTime('awal_periode')->nullable();
            $table->dateTime('akhir_periode')->nullable();
            $table->timestamps();
        });

        Schema::create('tran_produk_harga', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('produk_id')->nullable(); //foreign key tmst_produk
            $table->integer('kategori_pelanggan_id')->nullable(); //foreign key tmst_kategori_pelanggan
            $table->string('diskon', 255)->nullable();
            $table->dateTime('awal_periode')->nullable();
            $table->dateTime('akhir_periode')->nullable();
            $table->timestamps();
        });

        Schema::create('tran_produk_gudang', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('produk_id'); //foreign key tmst_produk
            $table->integer('gudang_id'); //foreign key tmst_gudang
            $table->integer('stok')->nullable();
            $table->timestamps();
        });

        Schema::create('tran_produk_supplier', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('produk_id'); //foreign key tmst_produk
            $table->integer('supplier_id'); //foreign key tmst_supplier
            $table->bigInteger('harga_terakhir')->nullable();
            $table->dateTime('tanggal_terakhir')->nullable();
            $table->timestamps();
        });

        Schema::create('tran_hadiah_gudang', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('hadiah_id'); //foreign key tmst_hadiah
            $table->integer('gudang_id'); //foreign key tmst_gudang
            $table->integer('stok')->nullable();
            $table->timestamps();
        });

        Schema::create('tran_hadiah_supplier', function (Blueprint $table) {
            $table->integer('hadiah_id'); //foreign key tmst_hadiah
            $table->integer('supplier_id'); //foreign key tmst_supplier
            $table->bigInteger('harga_terakhir')->nullable();
            $table->dateTime('tanggal_terakhir')->nullable();
            $table->timestamps();
        });

        Schema::create('tran_paket_produk', function (Blueprint $table) {
            $table->integer('paket_id'); //foreign key tmst_paket
            $table->integer('produk_id'); //foreign key tmst_produk
            $table->integer('qty_produk')->nullable();
            $table->timestamps();
        });

        Schema::create('tran_produk_serial_number', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('gudang_id'); //foreign key tmst_gudang
            $table->integer('produk_id'); //foreign key tmst_produk
            $table->string('serial_number', 255)->nullable();
            $table->integer('stok')->nullable();
            $table->string('keterangan', 255)->nullable();
            $table->timestamps();
        });

        Schema::create('tran_penyesuaian_stok', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('gudang_id'); //foreign key tmst_gudang
            $table->integer('produk_id'); //foreign key tmst_produk
            $table->string('serial_number', 255);
            $table->integer('stok_awal')->nullable();
            $table->integer('stok_ubah')->nullable();
            $table->integer('stok_baru')->nullable();
            $table->dateTime('tanggal')->nullable();
            $table->string('approval_by_user_id', 255)->nullable(); //foreign key users
            $table->string('received_by_user_id', 255)->nullable(); //foreign key users
            $table->timestamps();
        });

        Schema::create('tran_produk_baru', function (Blueprint $table) {
            $table->integer('produk_id'); //foreign key tmst_produk
            $table->dateTime('tanggal_input')->nullable();
            $table->timestamps();
        });

        Schema::create('tran_produk_akan_datang', function (Blueprint $table) {
            $table->integer('produk_id'); //foreign key tmst_produk
            $table->dateTime('tanggal_input')->nullable();
            $table->timestamps();
        });

        Schema::create('tran_rekening_supplier', function(Blueprint $table){
            $table->increments('id');
            $table->string('nama_bank', 255)->nullable();
            $table->string('nomor_rekening', 255)->nullable();
            $table->string('atas_nama', 255)->nullable();
            $table->string('mata_uang', 255)->nullable();
            $table->string('supplier_id', 255)->nullable(); //foreign key tmst_supplier
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tran_produk_galeri');
        Schema::drop('tran_promo_hadiah');
        Schema::drop('tran_promo_diskon');
        Schema::drop('tran_promo_cashback');
        Schema::drop('tran_produk_harga');
        Schema::drop('tran_produk_gudang');
        Schema::drop('tran_produk_supplier');
        Schema::drop('tran_hadiah_gudang');
        Schema::drop('tran_hadiah_supplier');
        Schema::drop('tran_paket_produk');
        Schema::drop('tran_produk_serial_number');
        Schema::drop('tran_penyesuaian_stok');
        Schema::drop('tran_produk_baru');
        Schema::drop('tran_produk_akan_datang');
        Schema::drop('tran_rekening_supplier');
    }
}
