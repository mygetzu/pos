<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TmstJurnalUmum extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tref_nama_transaksi', function (Blueprint $table) {
            $table->bigInteger('id');
            $table->string('nama', 255)->nullable();
            $table->string('transaksi', 255)->nullable();
            $table->timestamps();
        });

        Schema::create('tran_pengaturan_jurnal', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('transaksi_id')->nullable();
            $table->integer('akun_id')->nullable();
            $table->string('debit_or_kredit', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tref_nama_transaksi');
        Schema::drop('tran_pengaturan_jurnal');
    }
}
