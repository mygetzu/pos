<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TabelPembayaran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tran_bayar_pembelian_header', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('nota_id')->nullable();
            $table->string('jenis', 255)->nullable();
            $table->integer('supplier_id')->nullable();
            $table->dateTime('tanggal')->nullable();
            $table->bigInteger('total_tagihan')->nullable();
            $table->bigInteger('total_pembayaran')->nullable();
            $table->text('keterangan')->nullable();
            $table->string('no_jurnal', 255)->nullable();
            $table->timestamps();
        });

        Schema::create('tran_bayar_pembelian_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('bayar_pembelian_header_id')->nullable();
            $table->integer('metode_pembayaran_id')->nullable();
            $table->string('nomor_pembayaran', 255)->nullable();
            $table->bigInteger('nominal')->nullable();
            $table->string('bank', 255)->nullable();
            $table->timestamps();
        });

        Schema::create('tran_bayar_penjualan_header', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('nota_id')->nullable();
            $table->string('jenis', 255)->nullable();
            $table->integer('pelanggan_id')->nullable();
            $table->dateTime('tanggal')->nullable();
            $table->bigInteger('total_tagihan')->nullable();
            $table->bigInteger('total_pembayaran')->nullable();
            $table->text('keterangan')->nullable();
            $table->string('no_jurnal', 255)->nullable();
            $table->timestamps();
        });

        Schema::create('tran_bayar_penjualan_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('bayar_penjualan_header_id')->nullable();
            $table->integer('metode_pembayaran_id')->nullable();
            $table->string('nomor_pembayaran', 255)->nullable();
            $table->bigInteger('nominal')->nullable();
            $table->string('bank', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tran_bayar_pembelian_header');
        Schema::drop('tran_bayar_pembelian_detail');
        Schema::drop('tran_bayar_penjualan_header');
        Schema::drop('tran_bayar_penjualan_detail');
    }
}
