<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TransaksiJasa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tran_transaksi_jasa', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_nota', 255)->nullable();
            $table->integer('pelanggan_id')->nullable();
            $table->dateTime('tanggal')->nullable();
            $table->text('deskripsi')->nullable();
            $table->bigInteger('biaya')->nullable();
            $table->string('no_jurnal', 255)->nullable();
            $table->integer('flag')->nullable();
            $table->integer('is_lunas')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tran_transaksi_jasa');
    }
}
