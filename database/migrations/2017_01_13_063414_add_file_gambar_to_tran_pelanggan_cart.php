<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFileGambarToTranPelangganCart extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        if (Schema::hasColumn('tran_pelanggan_cart', 'file_gambar')) {
            //Kolom file_gambar telah ada
        } else {
            Schema::table('tran_pelanggan_cart', function (Blueprint $table) {
                $table->text('file_gambar');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('tran_pelanggan_cart', function (Blueprint $table) {
            $table->dropColumn('file_gambar');
        });
    }

}
