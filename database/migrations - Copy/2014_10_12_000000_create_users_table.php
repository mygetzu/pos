<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->string('email', 255)->unique();
            $table->string('password', 255);
            $table->string('alamat', 255);
            $table->string('kota_id', 255); //foreign key tref_kota
            $table->string('kode_pos', 255);
            $table->string('telp', 255);
            $table->string('hp', 255);
            $table->integer('hak_akses_id');  //foreign key hak_akses
            $table->dateTime('tanggal_register');
            $table->dateTime('tanggal_unregister');
            $table->integer('is_aktif');
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('hak_akses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->longText('deskripsi');
            $table->string('menu_akses');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
        Schema::drop('hak_akses');
    }
}
