<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PromoCreator extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tran_slider_promo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama')->nullable();
            $table->string('url')->nullable();
            $table->string('file_gambar')->nullable();
            $table->string('list_produk')->nullable();
            $table->integer('item_order')->nullable();
            $table->timestamps();
        });

        Schema::create('tran_mini_banner', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama')->nullable();
            $table->string('url')->nullable();
            $table->string('file_gambar')->nullable();
            $table->string('list_produk')->nullable();
            $table->integer('item_order')->nullable();
            $table->timestamps();
        });

        Schema::create('tran_produk_pilihan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama')->nullable();
            $table->string('url')->nullable();
            $table->string('file_gambar')->nullable();
            $table->string('list_produk')->nullable();
            $table->integer('item_order')->nullable();
            $table->timestamps();
        });

        Schema::create('tref_konten', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama')->nullable();
            $table->longText('keterangan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tran_slider_promo');
        Schema::drop('tran_mini_banner');
        Schema::drop('tran_produk_pilihan');
        Schema::drop('tref_konten');
    }
}
