<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TambahKolomJenisBarangId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tmst_produk', function($table) {
            $table->integer('jenis_barang_id')->nullable();
        });

        Schema::table('tmst_hadiah', function($table) {
            $table->integer('jenis_barang_id')->nullable();
        });

        Schema::table('tmst_paket', function($table) {
            $table->integer('jenis_barang_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tmst_produk', function($table) {
            $table->dropColumn('jenis_barang_id');
        });

        Schema::table('tmst_hadiah', function($table) {
            $table->dropColumn('jenis_barang_id');
        });

        Schema::table('tmst_paket', function($table) {
            $table->dropColumn('jenis_barang_id');
        });
    }
}
