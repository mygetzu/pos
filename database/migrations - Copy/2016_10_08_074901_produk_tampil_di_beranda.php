<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProdukTampilDiBeranda extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tran_produk_tampil_di_beranda', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('produk_id')->nullable();
            $table->dateTime('tanggal_input')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tran_produk_tampil_di_beranda');
    }
}
