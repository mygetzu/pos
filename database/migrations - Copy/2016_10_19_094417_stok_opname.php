<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StokOpname extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tran_stok_opname_header', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('no_stok_opname'); 
            $table->integer('gudang_id'); //foreign key tmst_gudang
            $table->dateTime('tanggal')->nullable();
            $table->integer('total_stok')->nullable();
            $table->string('approval_by_user_id')->nullable(); //foreign key users
            $table->string('received_by_user_id')->nullable(); //foreign key users
            $table->timestamps();
        });

        Schema::create('tran_stok_opname_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('stok_opname_header_id');
            $table->string('no_stok_opname'); 
            $table->integer('produk_id');
            $table->string('serial_number');
            $table->integer('jumlah');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tran_stok_opname_header');
        Schema::drop('tran_stok_opname_detail');
    }
}
