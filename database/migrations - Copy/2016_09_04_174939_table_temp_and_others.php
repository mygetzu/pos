<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableTempAndOthers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notif_chat', function (Blueprint $table) {
            $table->integer('user_id'); //foreign key users
            $table->integer('user_id_from'); //foreign key users
            $table->text('pesan');
        });

        Schema::create('log_chat', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id_a'); //foreign key users
            $table->integer('user_id_b'); //foreign key users
            $table->longText('pesan');
            $table->timestamps();
        });

        Schema::create('temp_stok_opname', function (Blueprint $table) {
            $table->integer('id');
            $table->string('gudang_id'); //foreign key tmst_gudang
            $table->string('produk_id'); //foreign key tmst_produk
            $table->string('serial_number');
            $table->integer('stok');
            $table->timestamps();
        });

        Schema::create('halaman_promo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('file_gambar');
            $table->string('is_aktif');
            $table->integer('item_order');
            $table->timestamps();
        });

        Schema::create('template_layout', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->string('keterangan');
        });

        Schema::create('template_background', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->string('keterangan');
            $table->string('file_gambar');
        });

        Schema::create('template_tombol', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->string('keterangan');
            $table->string('jenis'); //flat, rounded, 3d
            $table->string('is_icon_button'); //tampilkan sembunyikan icon pada button
            $table->string('btn_primary');
            $table->string('btn_default');
            $table->string('btn_warning');
            $table->string('btn_danger');
            $table->string('btn_info');
        });

        Schema::create('tema', function (Blueprint $table) {
            $table->string('warna_dasar'); //untuk header footer
            $table->string('warna_huruf');
            $table->string('logo');
            $table->string('icon');
            $table->integer('layout_id'); //foreign key template_layout
            $table->integer('background_id'); //foreign key template_background
            $table->integer('tombol_id'); //foreign key template_tombol
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('notif_chat');
        Schema::drop('log_chat');
        Schema::drop('temp_stok_opname');
        Schema::drop('halaman_promo');
        Schema::drop('template_layout');
        Schema::drop('template_background');
        Schema::drop('template_tombol');
        Schema::drop('tema');
    }
}
