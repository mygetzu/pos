<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPengambilanBarangToTransSjKeluarHeader extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        if (Schema::hasColumn('tran_sj_keluar_header', 'pengambilan_barang')) {
            //Kolom pengambilan_barang telah ada
        } else {
            Schema::table('tran_sj_keluar_header', function (Blueprint $table) {
                $table->string('pengambilan_barang', 50);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('tran_sj_keluar_header', function (Blueprint $table) {
            $table->dropColumn('pengambilan_barang');
        });
    }

}
