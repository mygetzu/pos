<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewTransaksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tran_po_header', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('no_purchase_order')->nullable();
            $table->integer('supplier_id')->nullable(); //foreign key tmst_supplier
            $table->dateTime('tanggal')->nullable(); //simpan tanggal dan jam
            $table->dateTime('due_date')->nullable(); //tanggal request pengiriman
            $table->string('catatan')->nullable();
            $table->bigInteger('total_tagihan')->nullable(); //jumlah yang harus dibayar
            $table->integer('flag_sj_masuk')->nullable(); //untuk mengetahui surat jalan sudah dibuat semua atau belum
            $table->text('syarat_ketentuan')->nullable();
            $table->integer('ppn')->nullable();
            $table->timestamps();
        });

        Schema::create('tran_po_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('po_header_id');
            $table->integer('produk_id')->nullable(); //foreign key tmst_produk
            $table->string('jenis_barang_id')->nullable(); //foreign key tref_jenis_barang
            $table->integer('jumlah')->nullable();
            $table->integer('jumlah_terkirim')->nullable();
            $table->text('deskripsi')->nullable(); //deskripsi produk
            $table->bigInteger('harga')->nullable();
            $table->bigInteger('harga_retail')->nullable();
            $table->timestamps();
        });

        Schema::create('tran_po_alamat_pengiriman', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('po_header_id');
            $table->string('nama')->nullable();
            $table->string('email')->nullable();
            $table->text('alamat')->nullable();
            $table->integer('kota_id')->nullable();
            $table->string('kode_pos')->nullable();
            $table->string('hp')->nullable();
            $table->integer('metode_pengiriman_id')->nullable();
            $table->timestamps();
        });

        Schema::create('tran_po_diskon', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('po_header_id');
            $table->bigInteger('nominal')->nullable();
            $table->integer('is_dipakai')->nullable();
            $table->timestamps();
        });

        Schema::create('tran_sj_masuk_header', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('po_header_id');
            $table->string('no_surat_jalan')->nullable();
            $table->integer('supplier_id')->nullable();
            $table->dateTime('tanggal')->nullable();
            $table->integer('flag_nota_beli')->nullable();
            $table->bigInteger('total_harga')->nullable();
            $table->integer('ppn')->nullable();
            $table->bigInteger('total_diskon')->nullable();
            $table->bigInteger('total_tagihan')->nullable();
            $table->timestamps();
        });

        Schema::create('tran_sj_masuk_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('sj_masuk_header_id');
            $table->integer('produk_id')->nullable();
            $table->integer('jenis_barang_id')->nullable();
            $table->integer('gudang_id')->nullable();
            $table->string('serial_number')->nullable();
            $table->integer('jumlah')->nullable();
            $table->bigInteger('harga')->nullable();
            $table->bigInteger('harga_retail')->nullable();
            $table->timestamps();
        });

        Schema::create('tran_nota_beli', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('sj_masuk_header_id');
            $table->string('no_nota')->nullable();
            $table->integer('supplier_id')->nullable();
            $table->dateTime('tanggal')->nullable();
            $table->dateTime('jatuh_tempo')->nullable();
            $table->string('no_jurnal')->nullable();
            $table->integer('is_lunas')->nullable();
            $table->integer('ppn')->nullable();
            $table->bigInteger('total_harga')->nullable();
            $table->bigInteger('total_diskon')->nullable();
            $table->bigInteger('total_tagihan')->nullable();
            $table->timestamps();
        });

        Schema::create('tran_rb_header', function (Blueprint $table) { //retur beli
            $table->bigIncrements('id');
            $table->bigInteger('nota_beli_id');
            $table->string('no_nota')->nullable();
            $table->integer('supplier_id')->nullable();
            $table->dateTime('tanggal')->nullable();
            $table->bigInteger('total_retur')->nullable();
            $table->bigInteger('potongan_retur')->nullable();
            $table->bigInteger('total_tagihan')->nullable();
            $table->text('catatan')->nullable();
            $table->string('no_jurnal')->nullable();
            $table->integer('is_lunas')->nullable();
            $table->timestamps();
        });

        Schema::create('tran_rb_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('rb_header_id');
            $table->integer('produk_id')->nullable();
            $table->integer('jenis_barang_id')->nullable();
            $table->integer('gudang_id')->nullable();
            $table->string('serial_number')->nullable();
            $table->integer('jumlah')->nullable();
            $table->text('deskripsi')->nullable();
            $table->bigInteger('harga')->nullable();
            $table->timestamps();
        });

        Schema::create('tran_so_header', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('no_sales_order')->nullable();
            $table->integer('pelanggan_id')->nullable(); //foreign key tmst_supplier
            $table->dateTime('tanggal')->nullable(); //simpan tanggal dan jam
            $table->dateTime('due_date')->nullable(); //tanggal request pengiriman
            $table->string('catatan')->nullable();
            $table->bigInteger('total_tagihan')->nullable(); //jumlah yang harus dibayar
            $table->integer('flag_sj_keluar')->nullable(); //untuk mengetahui surat jalan sudah dibuat semua atau belum
            $table->integer('ppn')->nullable();
            $table->timestamps();
        });

        Schema::create('tran_so_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('so_header_id');
            $table->integer('produk_id')->nullable(); //foreign key tmst_produk
            $table->string('jenis_barang_id')->nullable(); //foreign key tref_jenis_barang
            $table->integer('jumlah')->nullable();
            $table->integer('jumlah_terkirim')->nullable();
            $table->text('deskripsi')->nullable(); //deskripsi produk
            $table->bigInteger('harga')->nullable();
            $table->bigInteger('harga_retail')->nullable();
            $table->timestamps();
        });

        Schema::create('tran_so_alamat_pengiriman', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('so_header_id');
            $table->string('nama')->nullable();
            $table->string('email')->nullable();
            $table->text('alamat')->nullable();
            $table->integer('kota_id')->nullable();
            $table->string('kode_pos')->nullable();
            $table->string('hp')->nullable();
            $table->integer('metode_pengiriman_id')->nullable();
            $table->timestamps();
        });

        Schema::create('tran_so_voucher', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('so_header_id');
            $table->bigInteger('voucher_id');
            $table->bigInteger('nominal')->nullable();
            $table->integer('is_dipakai')->nullable();
            $table->timestamps();
        });

        Schema::create('tran_sj_keluar_header', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('so_header_id');
            $table->string('no_surat_jalan')->nullable();
            $table->integer('pelanggan_id')->nullable();
            $table->dateTime('tanggal')->nullable();
            $table->integer('flag_nota_jual')->nullable();
            $table->integer('ppn')->nullable();
            $table->bigInteger('total_harga')->nullable();
            $table->bigInteger('total_voucher')->nullable();
            $table->bigInteger('total_tagihan')->nullable();
            $table->timestamps();
        });

        Schema::create('tran_sj_keluar_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('sj_keluar_header_id');
            $table->integer('produk_id')->nullable();
            $table->integer('jenis_barang_id')->nullable();
            $table->integer('gudang_id')->nullable();
            $table->string('serial_number')->nullable();
            $table->integer('jumlah')->nullable();
            $table->bigInteger('harga')->nullable();
            $table->bigInteger('harga_retail')->nullable();
            $table->timestamps();
        });

        Schema::create('tran_nota_jual', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('sj_keluar_header_id');
            $table->string('no_nota')->nullable();
            $table->integer('pelanggan_id')->nullable();
            $table->dateTime('tanggal')->nullable();
            $table->dateTime('jatuh_tempo')->nullable();
            $table->string('no_jurnal')->nullable();
            $table->integer('is_lunas')->nullable();
            $table->integer('ppn')->nullable();
            $table->bigInteger('total_harga')->nullable();
            $table->bigInteger('total_voucher')->nullable();
            $table->bigInteger('total_tagihan')->nullable();
            $table->timestamps();
        });

        Schema::create('tran_rj_header', function (Blueprint $table) { //retur beli
            $table->bigIncrements('id');
            $table->bigInteger('nota_jual_id');
            $table->string('no_nota')->nullable();
            $table->integer('pelanggan_id')->nullable();
            $table->dateTime('tanggal')->nullable();
            $table->bigInteger('total_retur')->nullable();
            $table->bigInteger('potongan_retur')->nullable();
            $table->bigInteger('total_tagihan')->nullable();
            $table->text('catatan')->nullable();
            $table->string('no_jurnal')->nullable();
            $table->integer('is_lunas')->nullable();
            $table->timestamps();
        });

        Schema::create('tran_rj_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('rj_header_id');
            $table->integer('produk_id')->nullable();
            $table->integer('jenis_barang_id')->nullable();
            $table->integer('gudang_id')->nullable();
            $table->string('serial_number')->nullable();
            $table->integer('jumlah')->nullable();
            $table->text('deskripsi')->nullable();
            $table->bigInteger('harga')->nullable();
            $table->timestamps();
        });

        Schema::create('tran_service_order', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('no_nota')->nullable();
            $table->integer('pelanggan_id')->nullable();
            $table->dateTime('tanggal')->nullable();
            $table->string('serial_number')->nullable();
            $table->text('keluhan')->nullable();
            $table->text('deskripsi_penyelesaian')->nullable();
            $table->dateTime('tanggal_selesai')->nullable();
            $table->bigInteger('jasa_service')->nullable();
            $table->text('catatan')->nullable();
            $table->string('garansi')->nullable();
            $table->string('model')->nullable();
            $table->string('no_jurnal')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tran_po_header');
        Schema::drop('tran_po_detail');
        Schema::drop('tran_po_alamat_pengiriman');
        Schema::drop('tran_po_diskon');
        Schema::drop('tran_sj_masuk_header');
        Schema::drop('tran_sj_masuk_detail');
        Schema::drop('tran_nota_beli');
        Schema::drop('tran_rb_header');
        Schema::drop('tran_rb_detail');
        Schema::drop('tran_so_header');
        Schema::drop('tran_so_detail');
        Schema::drop('tran_so_alamat_pengiriman');
        Schema::drop('tran_so_voucher');
        Schema::drop('tran_sj_keluar_header');
        Schema::drop('tran_sj_keluar_detail');
        Schema::drop('tran_nota_jual');
        Schema::drop('tran_rj_header');
        Schema::drop('tran_rj_detail');
        Schema::drop('tran_service_order');
    }
}
