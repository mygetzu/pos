<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddJasaPengirimanToNotaJual extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        if (Schema::hasColumn('tran_nota_jual', 'kirim_via')) {
            //kolom kirim via telah ada
        } else {
            Schema::table('tran_nota_jual', function (Blueprint $table) {
                $table->string('kirim_via', 255);
            });
        }

        if (Schema::hasColumn('tran_nota_jual', 'no_resi')) {
            //kolom kirim via telah ada
        } else {
            Schema::table('tran_nota_jual', function (Blueprint $table) {
                $table->string('no_resi', 255);
            });
        }

        if (Schema::hasColumn('tran_nota_jual', 'foto_resi')) {
            //kolom kirim via telah ada
        } else {
            Schema::table('tran_nota_jual', function (Blueprint $table) {
                $table->text('foto_resi');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('tran_nota_jual', function (Blueprint $table) {
            $table->dropColumn('kirim_via');
            $table->dropColumn('no_resi');
            $table->dropColumn('foto_resi');
        });
    }

}
