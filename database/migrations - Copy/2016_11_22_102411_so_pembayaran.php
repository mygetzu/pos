<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SoPembayaran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tran_so_pembayaran', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('so_header_id')->nullable();
            $table->dateTime('tanggal')->nullable();
            $table->integer('metode_pembayaran_id')->nullable();
            $table->integer('bank_id')->nullable();
            $table->string('nomor_pembayaran')->nullable();
            $table->bigInteger('nominal')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tran_so_pembayaran');
    }
}
