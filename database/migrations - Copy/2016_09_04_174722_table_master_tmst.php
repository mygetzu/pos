<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableMasterTmst extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmst_perusahaan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama', 255);
            $table->string('alamat', 255)->nullable();
            $table->integer('kota_id')->nullable();  //foreign key tref_kota
            $table->string('telp', 255)->nullable();
            $table->string('kontak_person', 255)->nullable();
            $table->string('bidang_usaha', 255)->nullable();
            $table->string('jam_operasional', 255)->nullable();
            $table->string('email', 255)->nullable();
            $table->dateTime('expired')->nullable();
            $table->integer('pos_id')->nullable(); //mereferensi nilai pos_id dari pos_master
            $table->timestamps();
        });

        Schema::create('tmst_cabang_perusahaan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama', 255)->nullable();
            $table->string('alamat', 255)->nullable();
            $table->integer('kota_id')->nullable();  //foreign key tref_kota
            $table->string('telp', 255)->nullable();
            $table->string('kontak_person', 255)->nullable();
            $table->integer('perusahaan_id')->nullable();  //foreign key tmst_perusahaan
            $table->timestamps();
        });

        Schema::create('tmst_kategori_produk', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama', 255);
            $table->string('slug_nama', 255);
            $table->longText('deskripsi')->nullable();
            $table->text('kolom_spesifikasi')->nullable();
            $table->integer('is_aktif')->nullable();
            $table->timestamps();
        });

        Schema::create('tmst_produk', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode', 255)->nullable();
            $table->string('nama', 255);
            $table->string('slug_nama', 255);
            $table->string('satuan', 255)->nullable();
            $table->string('berat', 255)->nullable();
            $table->longText('spesifikasi')->nullable();
            $table->longText('deskripsi')->nullable();
            $table->bigInteger('harga_retail')->nullable();
            $table->bigInteger('hpp')->nullable();
            $table->integer('stok')->nullable();
            $table->integer('stok_dipesan')->nullable();
            $table->integer('is_aktif')->nullable();
            $table->integer('kategori_id')->nullable();  //foreign key tmst_kategori_produk
            $table->timestamps();
        });

        Schema::create('tmst_paket', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode', 255)->nullable();
            $table->string('nama', 255)->nullable();
            $table->string('slug_nama', 255)->nullable();
            $table->bigInteger('harga_total')->nullable();
            $table->longText('deskripsi')->nullable();
            $table->string('file_gambar', 255)->nullable();
            $table->integer('stok')->nullable();
            $table->integer('stok_dipesan')->nullable();
            $table->integer('is_aktif')->nullable();
            $table->timestamps();
        });

        Schema::create('tmst_hadiah', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode', 255)->nullable();
            $table->string('nama', 255)->nullable();
            $table->string('satuan', 255)->nullable();
            $table->string('berat', 255)->nullable();
            $table->string('deskripsi', 255)->nullable();
            $table->integer('stok')->nullable();
            $table->integer('stok_dipesan')->nullable();
            $table->string('file_gambar', 255)->nullable();
            $table->integer('is_aktif')->nullable();
            $table->timestamps();
        });

        Schema::create('tmst_kategori_pelanggan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama', 255)->nullable();
            $table->longText('deskripsi')->nullable();
            $table->integer('default_diskon')->nullable();
            $table->timestamps();
        });

        Schema::create('tmst_pelanggan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->nullable();
            $table->string('nama', 255)->nullable();
            $table->string('email', 255)->nullable();
            $table->string('alamat', 255)->nullable();
            $table->integer('kota_id')->nullable(); //foreign key tref_kota
            $table->string('telp1', 255)->nullable();
            $table->string('telp2', 255)->nullable();
            $table->string('kode_pos', 255)->nullable();
            $table->string('nama_sales', 255)->nullable();
            $table->string('hp_sales', 255)->nullable();
            $table->string('deskripsi', 255)->nullable();
            $table->integer('kategori_id')->nullable(); //foreign key tmst_kategori_pelanggan
            $table->timestamps();
        });

        Schema::create('tmst_gudang', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama', 255)->nullable();
            $table->string('alamat', 255)->nullable();
            $table->integer('kota_id')->nullable(); //foreign key tref_kota
            $table->string('telp', 255)->nullable();
            $table->string('supervisor', 255)->nullable();
            $table->string('hp_supervisor', 255)->nullable();
            $table->string('longitude', 255)->nullable();
            $table->string('latitude', 255)->nullable();
            $table->integer('is_aktif')->nullable();
            $table->string('format_no_mutasi', 255)->nullable();
            $table->timestamps();
        });

        Schema::create('tmst_supplier', function(Blueprint $table){
            $table->increments('id');
            $table->string('nama', 255)->nullable();
            $table->string('alamat', 255)->nullable();
            $table->integer('kota_id')->nullable(); //foreign key tref_kota
            $table->string('telp1', 255)->nullable();
            $table->string('telp2', 255)->nullable();
            $table->string('nama_sales', 255)->nullable();
            $table->string('hp_sales', 255)->nullable();
            $table->string('deskripsi', 255)->nullable();
            $table->string('umur_hutang', 255)->nullable();
            $table->integer('is_aktif')->nullable();
            $table->timestamps();
        });

        Schema::create('tmst_voucher', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode', 255)->nullable();
            $table->bigInteger('nominal')->nullable();
            $table->dateTime('awal_periode')->nullable();
            $table->dateTime('akhir_periode')->nullable();
            $table->dateTime('tanggal_dipakai')->nullable();
            $table->integer('is_sekali_pakai')->nullable();
            $table->integer('is_aktif')->nullable();
            $table->string('pelanggan_id_target', 255)->nullable(); //foreign key tmst_pelanggan
            $table->string('pelanggan_id_pakai', 255)->nullable(); //foreign key tmst_pelanggan
            $table->timestamps();
        });

        Schema::create('tmst_master_coa', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode', 255)->nullable();
            $table->string('nama', 255)->nullable();
            $table->integer('level')->nullable();
            $table->string('parent', 255)->nullable();
            $table->string('debit_or_kredit', 255)->nullable();
            $table->string('group_kode', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tmst_perusahaan');
        Schema::drop('tmst_cabang_perusahaan');
        Schema::drop('tmst_kategori_produk');
        Schema::drop('tmst_produk');
        Schema::drop('tmst_paket');
        Schema::drop('tmst_hadiah');
        Schema::drop('tmst_kategori_pelanggan');
        Schema::drop('tmst_pelanggan');
        Schema::drop('tmst_gudang');
        Schema::drop('tmst_supplier');
        Schema::drop('tmst_voucher');
        Schema::drop('tmst_master_coa');
    }
}
