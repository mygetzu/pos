<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableReference extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tref_provinsi', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode');
            $table->string('nama');
            $table->timestamps();
        });

        Schema::create('tref_kota', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode');
            $table->string('provinsi_kode'); //foreign key tref_provinsi (pakai kode)
            $table->string('nama');
            $table->timestamps();
        });

        Schema::create('tref_jenis_barang', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->string('keterangan');
            $table->timestamps();
        });

        Schema::create('tref_sumber_data', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->timestamps();
        });

        Schema::create('tref_bank', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode');
            $table->string('nama');
            $table->string('nama_singkat');
            $table->string('keterangan');
            $table->integer('is_aktif');
            $table->string('file_gambar');
            $table->timestamps();
        });

        Schema::create('tref_metode_pembayaran', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->string('keterangan');
            $table->integer('is_aktif');
            $table->string('file_gambar');
            $table->string('jenis');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tref_provinsi');
        Schema::drop('tref_kota');
        Schema::drop('tref_jenis_barang');
        Schema::drop('tref_sumber_data');
        Schema::drop('tref_bank');
        Schema::drop('tref_metode_pembayaran');
    }
}
