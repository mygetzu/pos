<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTranServiceInvoiceHeader extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tran_service_invoice_header', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('service_order_id');
            $table->integer('service_teknisi_id');
            $table->bigInteger('harga_total');
            $table->text('catatan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tran_service_invoice_header');
    }
}
