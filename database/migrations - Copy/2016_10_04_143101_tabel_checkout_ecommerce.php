<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TabelCheckoutEcommerce extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_alamat_pengiriman', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('no_nota')->nullable();
            $table->dateTime('tanggal')->nullable();
            $table->string('nama')->nullable();
            $table->string('email')->nullable();
            $table->string('alamat')->nullable();
            $table->integer('kota_id')->nullable();
            $table->string('kode_pos')->nullable();
            $table->string('hp')->nullable();
            $table->integer('metode_pengiriman_id')->nullable();
            $table->timestamps();
        });

        Schema::create('temp_pembayaran', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('no_nota')->nullable();
            $table->bigInteger('tanggal')->nullable();
            $table->string('metode_pembayaran_id')->nullable();
            $table->string('bayar_id')->nullable();
            $table->string('nomor_pembayaran')->nullable();
            $table->integer('bank_id')->nullable();
            $table->timestamps();
        });

        Schema::create('tref_metode_pengiriman', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama')->nullable();
            $table->bigInteger('tarif_dasar')->nullable();
            $table->string('keterangan')->nullable();
            $table->string('is_aktif')->nullable();
            $table->string('file_gambar')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('temp_alamat_pengiriman');
        Schema::drop('temp_pembayaran');
        Schema::drop('tref_metode_pengiriman');
    }
}
