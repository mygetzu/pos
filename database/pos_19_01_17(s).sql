-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 19, 2017 at 09:15 AM
-- Server version: 5.7.13-0ubuntu0.16.04.2
-- PHP Version: 7.0.8-0ubuntu0.16.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pos`
--

-- --------------------------------------------------------

--
-- Table structure for table `group_account`
--

CREATE TABLE `group_account` (
  `id` int(10) UNSIGNED NOT NULL,
  `kode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `debit_or_kredit` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hak_akses`
--

CREATE TABLE `hak_akses` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deskripsi` longtext COLLATE utf8_unicode_ci NOT NULL,
  `menu_akses` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `halaman_promo`
--

CREATE TABLE `halaman_promo` (
  `id` int(10) UNSIGNED NOT NULL,
  `file_gambar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_aktif` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `log_chat`
--

CREATE TABLE `log_chat` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id_a` int(11) NOT NULL,
  `user_id_b` int(11) NOT NULL,
  `pesan` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `route` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `notifikasi`
--

CREATE TABLE `notifikasi` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pesan` text COLLATE utf8_unicode_ci,
  `user_id_target` int(11) DEFAULT NULL,
  `hak_akses_id_target` int(11) DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_aktif` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `notif_chat`
--

CREATE TABLE `notif_chat` (
  `user_id` int(11) NOT NULL,
  `user_id_from` int(11) NOT NULL,
  `pesan` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `session_id` int(10) UNSIGNED NOT NULL,
  `expire_time` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_token_scopes`
--

CREATE TABLE `oauth_access_token_scopes` (
  `id` int(10) UNSIGNED NOT NULL,
  `access_token_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `scope_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `session_id` int(10) UNSIGNED NOT NULL,
  `redirect_uri` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expire_time` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_code_scopes`
--

CREATE TABLE `oauth_auth_code_scopes` (
  `id` int(10) UNSIGNED NOT NULL,
  `auth_code_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `scope_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `secret` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_client_endpoints`
--

CREATE TABLE `oauth_client_endpoints` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `redirect_uri` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_client_grants`
--

CREATE TABLE `oauth_client_grants` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `grant_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_client_scopes`
--

CREATE TABLE `oauth_client_scopes` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `scope_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_grants`
--

CREATE TABLE `oauth_grants` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_grant_scopes`
--

CREATE TABLE `oauth_grant_scopes` (
  `id` int(10) UNSIGNED NOT NULL,
  `grant_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `scope_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `access_token_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `expire_time` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_scopes`
--

CREATE TABLE `oauth_scopes` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_sessions`
--

CREATE TABLE `oauth_sessions` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `owner_type` enum('client','user') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'user',
  `owner_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_redirect_uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_session_scopes`
--

CREATE TABLE `oauth_session_scopes` (
  `id` int(10) UNSIGNED NOT NULL,
  `session_id` int(10) UNSIGNED NOT NULL,
  `scope_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `shoppingcart`
--

CREATE TABLE `shoppingcart` (
  `identifier` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `instance` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `static`
--

CREATE TABLE `static` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tema`
--

CREATE TABLE `tema` (
  `warna_dasar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `warna_huruf` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `layout_id` int(11) NOT NULL,
  `background_id` int(11) NOT NULL,
  `tombol_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `template_background`
--

CREATE TABLE `template_background` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_gambar` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `template_layout`
--

CREATE TABLE `template_layout` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `template_tombol`
--

CREATE TABLE `template_tombol` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jenis` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_icon_button` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `btn_primary` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `btn_default` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `btn_warning` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `btn_danger` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `btn_info` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `temp_alamat_pengiriman`
--

CREATE TABLE `temp_alamat_pengiriman` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `no_nota` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kota_id` int(11) DEFAULT NULL,
  `kode_pos` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metode_pengiriman_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `temp_pembayaran`
--

CREATE TABLE `temp_pembayaran` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `no_nota` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tanggal` bigint(20) DEFAULT NULL,
  `metode_pembayaran_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bayar_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomor_pembayaran` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `temp_so_voucher`
--

CREATE TABLE `temp_so_voucher` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `no_nota` int(11) DEFAULT NULL,
  `voucher_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `temp_stok_opname`
--

CREATE TABLE `temp_stok_opname` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `gudang_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `produk_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `stok` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmst_cabang_perusahaan`
--

CREATE TABLE `tmst_cabang_perusahaan` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alamat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kota_id` int(11) DEFAULT NULL,
  `telp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kontak_person` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `perusahaan_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmst_gudang`
--

CREATE TABLE `tmst_gudang` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alamat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kota_id` int(11) DEFAULT NULL,
  `telp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supervisor` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hp_supervisor` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_aktif` int(11) DEFAULT NULL,
  `format_no_mutasi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmst_hadiah`
--

CREATE TABLE `tmst_hadiah` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `satuan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `berat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deskripsi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stok` int(11) DEFAULT NULL,
  `stok_dipesan` int(11) DEFAULT NULL,
  `file_gambar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_aktif` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `jenis_barang_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmst_kategori_pelanggan`
--

CREATE TABLE `tmst_kategori_pelanggan` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deskripsi` longtext COLLATE utf8_unicode_ci,
  `default_diskon` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmst_kategori_produk`
--

CREATE TABLE `tmst_kategori_produk` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug_nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deskripsi` longtext COLLATE utf8_unicode_ci,
  `kolom_spesifikasi` text COLLATE utf8_unicode_ci,
  `is_aktif` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `file_gambar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmst_master_coa`
--

CREATE TABLE `tmst_master_coa` (
  `id` int(10) UNSIGNED NOT NULL,
  `kode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `parent` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `debit_or_kredit` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `group_kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmst_paket`
--

CREATE TABLE `tmst_paket` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug_nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `harga_total` bigint(20) DEFAULT NULL,
  `deskripsi` longtext COLLATE utf8_unicode_ci,
  `file_gambar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stok` int(11) NOT NULL,
  `stok_dipesan` int(11) NOT NULL,
  `is_aktif` int(11) DEFAULT NULL,
  `jenis_barang_id` int(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmst_pelanggan`
--

CREATE TABLE `tmst_pelanggan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alamat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kota_id` int(11) DEFAULT NULL,
  `telp1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telp2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kode_pos` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama_sales` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hp_sales` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deskripsi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kategori_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmst_perusahaan`
--

CREATE TABLE `tmst_perusahaan` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kota_id` int(11) DEFAULT NULL,
  `telp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kontak_person` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bidang_usaha` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jam_operasional` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expired` datetime DEFAULT NULL,
  `pos_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `telp2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmst_produk`
--

CREATE TABLE `tmst_produk` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug_nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `satuan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `berat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `spesifikasi` longtext COLLATE utf8_unicode_ci,
  `deskripsi` longtext COLLATE utf8_unicode_ci,
  `harga_retail` bigint(20) DEFAULT NULL,
  `hpp` bigint(20) NOT NULL,
  `stok` int(11) DEFAULT NULL,
  `stok_dipesan` int(11) DEFAULT NULL,
  `is_aktif` int(11) DEFAULT NULL,
  `kategori_id` int(11) DEFAULT NULL,
  `jenis_barang_id` int(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmst_supplier`
--

CREATE TABLE `tmst_supplier` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alamat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kota_id` int(11) DEFAULT NULL,
  `telp1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telp2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama_sales` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hp_sales` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deskripsi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `umur_hutang` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_aktif` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmst_voucher`
--

CREATE TABLE `tmst_voucher` (
  `id` int(10) UNSIGNED NOT NULL,
  `kode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nominal` bigint(20) DEFAULT NULL,
  `awal_periode` datetime DEFAULT NULL,
  `akhir_periode` datetime DEFAULT NULL,
  `tanggal_dipakai` datetime DEFAULT NULL,
  `is_sekali_pakai` int(11) DEFAULT NULL,
  `is_aktif` int(11) DEFAULT NULL,
  `pelanggan_id_target` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pelanggan_id_pakai` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_bayar_pembelian_detail`
--

CREATE TABLE `tran_bayar_pembelian_detail` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `bayar_pembelian_header_id` bigint(20) DEFAULT NULL,
  `metode_pembayaran_id` int(11) DEFAULT NULL,
  `nomor_pembayaran` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nominal` bigint(20) DEFAULT NULL,
  `bank` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_bayar_pembelian_header`
--

CREATE TABLE `tran_bayar_pembelian_header` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nota_id` bigint(20) DEFAULT NULL,
  `jenis` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `total_tagihan` bigint(20) DEFAULT NULL,
  `total_pembayaran` bigint(20) DEFAULT NULL,
  `keterangan` text COLLATE utf8_unicode_ci,
  `no_jurnal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_bayar_penjualan_detail`
--

CREATE TABLE `tran_bayar_penjualan_detail` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `bayar_penjualan_header_id` bigint(20) DEFAULT NULL,
  `metode_pembayaran_id` int(11) DEFAULT NULL,
  `nomor_pembayaran` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nominal` bigint(20) DEFAULT NULL,
  `bank` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_bayar_penjualan_header`
--

CREATE TABLE `tran_bayar_penjualan_header` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nota_id` bigint(20) DEFAULT NULL,
  `jenis` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pelanggan_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `total_tagihan` bigint(20) DEFAULT NULL,
  `total_pembayaran` bigint(20) DEFAULT NULL,
  `keterangan` text COLLATE utf8_unicode_ci,
  `no_jurnal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_detail_stok_opname`
--

CREATE TABLE `tran_detail_stok_opname` (
  `id` int(10) UNSIGNED NOT NULL,
  `stok_opname_id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stok` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_hadiah_gudang`
--

CREATE TABLE `tran_hadiah_gudang` (
  `hadiah_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `stok` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_hadiah_supplier`
--

CREATE TABLE `tran_hadiah_supplier` (
  `hadiah_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `harga_terakhir` bigint(20) DEFAULT NULL,
  `tanggal_terakhir` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_jurnal_umum`
--

CREATE TABLE `tran_jurnal_umum` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `no_jurnal` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tanggal` datetime NOT NULL,
  `akun_kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `akun_nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `referensi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `debit` bigint(20) NOT NULL,
  `kredit` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_jurnal_umum_detail`
--

CREATE TABLE `tran_jurnal_umum_detail` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `jurnal_umum_header_id` bigint(20) NOT NULL,
  `no_jurnal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `akun_kode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `akun_nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `referensi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `debit` bigint(20) DEFAULT NULL,
  `kredit` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_jurnal_umum_header`
--

CREATE TABLE `tran_jurnal_umum_header` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tanggal` datetime DEFAULT NULL,
  `is_editable` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_kartu_stok_detail`
--

CREATE TABLE `tran_kartu_stok_detail` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kartu_stok_header_id` int(11) NOT NULL,
  `sumber_data_id` int(11) DEFAULT NULL,
  `supplier_or_pelanggan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_nota` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `harga` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_kartu_stok_header`
--

CREATE TABLE `tran_kartu_stok_header` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `gudang_id` int(11) DEFAULT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `tanggal_awal` datetime DEFAULT NULL,
  `tanggal_akhir` datetime DEFAULT NULL,
  `total_stok` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_laporan_stok_detail`
--

CREATE TABLE `tran_laporan_stok_detail` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `laporan_stok_header_id` int(11) DEFAULT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_laporan_stok_header`
--

CREATE TABLE `tran_laporan_stok_header` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `gudang_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `total_stok` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_mini_banner`
--

CREATE TABLE `tran_mini_banner` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_gambar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `list_produk` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_mutasi_stok`
--

CREATE TABLE `tran_mutasi_stok` (
  `id` int(10) UNSIGNED NOT NULL,
  `gudang_id_asal` int(11) NOT NULL,
  `gudang_id_tujuan` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `approval_by_user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `received_by_user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_mutasi_stok_detail`
--

CREATE TABLE `tran_mutasi_stok_detail` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `mutasi_stok_header_id` int(11) NOT NULL,
  `no_mutasi_stok` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `produk_id` int(11) NOT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jumlah` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_mutasi_stok_header`
--

CREATE TABLE `tran_mutasi_stok_header` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `no_mutasi_stok` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gudang_id_asal` int(11) NOT NULL,
  `gudang_id_tujuan` int(11) NOT NULL,
  `tanggal` datetime DEFAULT NULL,
  `approval_by_user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `received_by_user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_nilai_spesifikasi`
--

CREATE TABLE `tran_nilai_spesifikasi` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `produk_id` bigint(20) DEFAULT NULL,
  `parameter_spesifikasi_id` bigint(20) DEFAULT NULL,
  `nilai` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `satuan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_nota_beli`
--

CREATE TABLE `tran_nota_beli` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sj_masuk_header_id` bigint(20) NOT NULL,
  `no_nota` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `jatuh_tempo` datetime DEFAULT NULL,
  `total_diskon` bigint(20) DEFAULT NULL,
  `no_jurnal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_lunas` int(11) DEFAULT NULL,
  `ppn` int(11) NOT NULL,
  `total_harga` bigint(20) NOT NULL,
  `total_tagihan` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_nota_jual`
--

CREATE TABLE `tran_nota_jual` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sj_keluar_header_id` bigint(20) NOT NULL,
  `no_nota` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pelanggan_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `jatuh_tempo` datetime DEFAULT NULL,
  `no_jurnal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_voucher` bigint(20) DEFAULT NULL,
  `is_lunas` int(11) DEFAULT NULL,
  `ppn` int(11) NOT NULL,
  `total_harga` bigint(20) NOT NULL,
  `total_tagihan` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `kirim_via` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `no_resi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `foto_resi` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_paket_gudang`
--

CREATE TABLE `tran_paket_gudang` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `paket_id` int(11) DEFAULT NULL,
  `gudang_id` int(11) DEFAULT NULL,
  `stok` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_paket_pilihan`
--

CREATE TABLE `tran_paket_pilihan` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_gambar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `list_paket` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_paket_produk`
--

CREATE TABLE `tran_paket_produk` (
  `paket_id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `qty_produk` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_parameter_spesifikasi`
--

CREATE TABLE `tran_parameter_spesifikasi` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kategori_produk_id` int(11) DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_pelanggan_cart`
--

CREATE TABLE `tran_pelanggan_cart` (
  `id` int(10) UNSIGNED NOT NULL,
  `pelanggan_id` bigint(20) DEFAULT NULL,
  `produk_id` bigint(20) DEFAULT NULL,
  `jenis_barang_id` int(11) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `harga_retail` bigint(20) NOT NULL,
  `harga_akhir` bigint(20) NOT NULL,
  `jenis_promo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_gambar` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_pengaturan_jurnal`
--

CREATE TABLE `tran_pengaturan_jurnal` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `transaksi_id` int(11) NOT NULL,
  `akun_id` int(11) NOT NULL,
  `debit_or_kredit` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_penyesuaian_stok`
--

CREATE TABLE `tran_penyesuaian_stok` (
  `id` int(10) UNSIGNED NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `stok_awal` int(11) DEFAULT NULL,
  `stok_ubah` int(11) DEFAULT NULL,
  `stok_baru` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `approval_by_user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `received_by_user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_penyesuaian_stok_detail`
--

CREATE TABLE `tran_penyesuaian_stok_detail` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `penyesuaian_stok_header_id` int(11) DEFAULT NULL,
  `produk_id` bigint(20) DEFAULT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stok_awal` int(11) DEFAULT NULL,
  `stok_ubah` int(11) DEFAULT NULL,
  `stok_baru` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_penyesuaian_stok_header`
--

CREATE TABLE `tran_penyesuaian_stok_header` (
  `id` int(10) UNSIGNED NOT NULL,
  `no_penyesuaian_stok` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gudang_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `keterangan` text COLLATE utf8_unicode_ci,
  `approval_by_user_id` int(11) DEFAULT NULL,
  `received_by_user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_po_alamat_pengiriman`
--

CREATE TABLE `tran_po_alamat_pengiriman` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `po_header_id` bigint(20) NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alamat` text COLLATE utf8_unicode_ci,
  `kota_id` int(11) DEFAULT NULL,
  `kode_pos` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metode_pengiriman_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_po_detail`
--

CREATE TABLE `tran_po_detail` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `po_header_id` bigint(20) NOT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `jenis_barang_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `jumlah_terkirim` int(11) DEFAULT NULL,
  `deskripsi` text COLLATE utf8_unicode_ci,
  `harga` bigint(20) DEFAULT NULL,
  `harga_retail` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_po_diskon`
--

CREATE TABLE `tran_po_diskon` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `po_header_id` bigint(20) NOT NULL,
  `nominal` bigint(20) DEFAULT NULL,
  `is_dipakai` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_po_header`
--

CREATE TABLE `tran_po_header` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `no_purchase_order` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `due_date` datetime DEFAULT NULL,
  `catatan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_tagihan` bigint(20) DEFAULT NULL,
  `flag_sj_masuk` int(11) DEFAULT NULL,
  `syarat_ketentuan` text COLLATE utf8_unicode_ci NOT NULL,
  `ppn` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_produk_akan_datang`
--

CREATE TABLE `tran_produk_akan_datang` (
  `produk_id` int(11) NOT NULL,
  `tanggal_input` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_produk_baru`
--

CREATE TABLE `tran_produk_baru` (
  `produk_id` int(11) NOT NULL,
  `tanggal_input` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_produk_galeri`
--

CREATE TABLE `tran_produk_galeri` (
  `id` int(10) UNSIGNED NOT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `file_gambar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_produk_gudang`
--

CREATE TABLE `tran_produk_gudang` (
  `id` bigint(20) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `stok` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_produk_harga`
--

CREATE TABLE `tran_produk_harga` (
  `id` int(10) UNSIGNED NOT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `kategori_pelanggan_id` int(11) DEFAULT NULL,
  `diskon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `awal_periode` datetime DEFAULT NULL,
  `akhir_periode` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_produk_pilihan`
--

CREATE TABLE `tran_produk_pilihan` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_gambar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `list_produk` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_produk_serial_number`
--

CREATE TABLE `tran_produk_serial_number` (
  `id` bigint(20) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stok` int(11) DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_produk_supplier`
--

CREATE TABLE `tran_produk_supplier` (
  `id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `harga_terakhir` bigint(20) DEFAULT NULL,
  `tanggal_terakhir` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_produk_tampil_di_beranda`
--

CREATE TABLE `tran_produk_tampil_di_beranda` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `tanggal_input` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_promo_cashback`
--

CREATE TABLE `tran_promo_cashback` (
  `id` int(10) UNSIGNED NOT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `kode_promo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qty_beli` int(11) DEFAULT NULL,
  `cashback` int(11) DEFAULT NULL,
  `awal_periode` datetime DEFAULT NULL,
  `akhir_periode` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_promo_diskon`
--

CREATE TABLE `tran_promo_diskon` (
  `id` int(10) UNSIGNED NOT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `kode_promo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qty_beli` int(11) DEFAULT NULL,
  `diskon` int(11) DEFAULT NULL,
  `awal_periode` datetime DEFAULT NULL,
  `akhir_periode` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_promo_hadiah`
--

CREATE TABLE `tran_promo_hadiah` (
  `id` int(10) UNSIGNED NOT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `hadiah_id` int(11) DEFAULT NULL,
  `kode_promo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qty_beli` int(11) DEFAULT NULL,
  `qty_hadiah` int(11) DEFAULT NULL,
  `awal_periode` datetime DEFAULT NULL,
  `akhir_periode` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_rb_detail`
--

CREATE TABLE `tran_rb_detail` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `rb_header_id` int(11) NOT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `jenis_barang_id` int(11) DEFAULT NULL,
  `gudang_id` int(11) DEFAULT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `deskripsi` text COLLATE utf8_unicode_ci,
  `harga` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_rb_header`
--

CREATE TABLE `tran_rb_header` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nota_beli_id` bigint(20) NOT NULL,
  `no_nota` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `potongan_retur` bigint(20) DEFAULT NULL,
  `total_retur` bigint(20) DEFAULT NULL,
  `total_tagihan` bigint(20) NOT NULL,
  `catatan` text COLLATE utf8_unicode_ci,
  `no_jurnal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_lunas` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_rekening_supplier`
--

CREATE TABLE `tran_rekening_supplier` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama_bank` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomor_rekening` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `atas_nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mata_uang` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supplier_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_rj_detail`
--

CREATE TABLE `tran_rj_detail` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `rj_header_id` int(11) NOT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `jenis_barang_id` int(11) DEFAULT NULL,
  `gudang_id` int(11) DEFAULT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `deskripsi` text COLLATE utf8_unicode_ci,
  `harga` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_rj_header`
--

CREATE TABLE `tran_rj_header` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nota_jual_id` bigint(20) NOT NULL,
  `no_nota` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pelanggan_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `potongan_retur` bigint(20) DEFAULT NULL,
  `total_retur` bigint(20) DEFAULT NULL,
  `catatan` text COLLATE utf8_unicode_ci,
  `total_tagihan` bigint(20) NOT NULL,
  `no_jurnal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_lunas` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_service_invoice_detail`
--

CREATE TABLE `tran_service_invoice_detail` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_invoice_header_id` int(11) NOT NULL,
  `tindakan` text COLLATE utf8_unicode_ci NOT NULL,
  `biaya` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_service_invoice_header`
--

CREATE TABLE `tran_service_invoice_header` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_order_id` int(11) NOT NULL,
  `service_teknisi_id` int(11) NOT NULL,
  `harga_total` bigint(20) NOT NULL,
  `catatan` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_service_order`
--

CREATE TABLE `tran_service_order` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `no_nota` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pelanggan_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keluhan` text COLLATE utf8_unicode_ci,
  `deskripsi_penyelesaian` text COLLATE utf8_unicode_ci,
  `tanggal_selesai` datetime DEFAULT NULL,
  `jasa_service` bigint(20) DEFAULT NULL,
  `catatan` text COLLATE utf8_unicode_ci,
  `garansi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_jurnal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `flag` int(11) DEFAULT NULL,
  `is_lunas` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_sj_keluar_detail`
--

CREATE TABLE `tran_sj_keluar_detail` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sj_keluar_header_id` bigint(20) NOT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `jenis_barang_id` int(11) DEFAULT NULL,
  `gudang_id` int(11) DEFAULT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `harga` bigint(20) DEFAULT NULL,
  `harga_retail` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_sj_keluar_header`
--

CREATE TABLE `tran_sj_keluar_header` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `so_header_id` bigint(20) NOT NULL,
  `no_surat_jalan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pelanggan_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `flag_nota_jual` int(11) NOT NULL,
  `ppn` int(11) NOT NULL,
  `total_harga` bigint(20) NOT NULL,
  `total_voucher` bigint(20) NOT NULL,
  `total_tagihan` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `pengambilan_barang` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_sj_masuk_detail`
--

CREATE TABLE `tran_sj_masuk_detail` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sj_masuk_header_id` bigint(20) NOT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `jenis_barang_id` int(11) DEFAULT NULL,
  `gudang_id` int(11) DEFAULT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `harga` bigint(20) DEFAULT NULL,
  `harga_retail` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_sj_masuk_header`
--

CREATE TABLE `tran_sj_masuk_header` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `po_header_id` bigint(20) NOT NULL,
  `no_surat_jalan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `flag_nota_beli` int(11) NOT NULL,
  `ppn` int(11) NOT NULL,
  `total_harga` bigint(20) NOT NULL,
  `total_diskon` bigint(20) NOT NULL,
  `total_tagihan` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_slider_promo`
--

CREATE TABLE `tran_slider_promo` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_gambar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `list_produk` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_so_alamat_pengiriman`
--

CREATE TABLE `tran_so_alamat_pengiriman` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `so_header_id` bigint(20) NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alamat` text COLLATE utf8_unicode_ci,
  `kota_id` int(11) DEFAULT NULL,
  `kode_pos` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metode_pengiriman_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_so_detail`
--

CREATE TABLE `tran_so_detail` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `so_header_id` bigint(20) NOT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `jenis_barang_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `jumlah_terkirim` int(11) DEFAULT NULL,
  `deskripsi` text COLLATE utf8_unicode_ci,
  `harga` bigint(20) DEFAULT NULL,
  `harga_retail` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_so_header`
--

CREATE TABLE `tran_so_header` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `no_sales_order` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pelanggan_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `due_date` datetime DEFAULT NULL,
  `catatan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_tagihan` bigint(20) DEFAULT NULL,
  `flag_sj_keluar` int(11) DEFAULT NULL,
  `ppn` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_so_pembayaran`
--

CREATE TABLE `tran_so_pembayaran` (
  `id` int(10) UNSIGNED NOT NULL,
  `so_header_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `metode_pembayaran_id` int(11) DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `nomor_pembayaran` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nominal` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_so_voucher`
--

CREATE TABLE `tran_so_voucher` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `so_header_id` bigint(20) NOT NULL,
  `voucher_id` bigint(20) NOT NULL,
  `nominal` bigint(20) DEFAULT NULL,
  `is_dipakai` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_stok_opname`
--

CREATE TABLE `tran_stok_opname` (
  `id` int(10) UNSIGNED NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `tanggal` datetime DEFAULT NULL,
  `total_stok` int(11) DEFAULT NULL,
  `approval_by_user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_stok_opname_detail`
--

CREATE TABLE `tran_stok_opname_detail` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `stok_opname_header_id` int(11) NOT NULL,
  `no_stok_opname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `produk_id` int(11) NOT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jumlah` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_stok_opname_header`
--

CREATE TABLE `tran_stok_opname_header` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `no_stok_opname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `tanggal` datetime DEFAULT NULL,
  `total_stok` int(11) DEFAULT NULL,
  `approval_by_user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `received_by_user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_transaksi_jasa`
--

CREATE TABLE `tran_transaksi_jasa` (
  `id` int(10) UNSIGNED NOT NULL,
  `no_nota` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pelanggan_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `deskripsi` text COLLATE utf8_unicode_ci,
  `biaya` bigint(20) DEFAULT NULL,
  `no_jurnal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flag` int(11) DEFAULT NULL,
  `is_lunas` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tref_bank`
--

CREATE TABLE `tref_bank` (
  `id` int(10) UNSIGNED NOT NULL,
  `kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nama_singkat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_aktif` int(11) NOT NULL,
  `file_gambar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tref_jenis_barang`
--

CREATE TABLE `tref_jenis_barang` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tref_konten`
--

CREATE TABLE `tref_konten` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keterangan` longtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tref_kota`
--

CREATE TABLE `tref_kota` (
  `id` int(10) UNSIGNED NOT NULL,
  `kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provinsi_kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tref_metode_pembayaran`
--

CREATE TABLE `tref_metode_pembayaran` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_aktif` int(11) NOT NULL,
  `file_gambar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jenis` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tref_metode_pengiriman`
--

CREATE TABLE `tref_metode_pengiriman` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tarif_dasar` bigint(20) DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_aktif` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_gambar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tref_nama_transaksi`
--

CREATE TABLE `tref_nama_transaksi` (
  `id` bigint(20) NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `transaksi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tref_nomor_invoice`
--

CREATE TABLE `tref_nomor_invoice` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `format` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tref_pengaturan_po`
--

CREATE TABLE `tref_pengaturan_po` (
  `id` int(10) UNSIGNED NOT NULL,
  `alamat_pengiriman` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `syarat_ketentuan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ppn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tref_pengaturan_so`
--

CREATE TABLE `tref_pengaturan_so` (
  `id` int(10) UNSIGNED NOT NULL,
  `syarat_ketentuan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ppn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tref_provinsi`
--

CREATE TABLE `tref_provinsi` (
  `id` int(10) UNSIGNED NOT NULL,
  `kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tref_sumber_data`
--

CREATE TABLE `tref_sumber_data` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tref_syarat_ketentuan`
--

CREATE TABLE `tref_syarat_ketentuan` (
  `id` int(10) UNSIGNED NOT NULL,
  `perihal` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `konten` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kota_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kode_pos` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hak_akses_id` int(11) NOT NULL,
  `tanggal_register` datetime NOT NULL,
  `tanggal_unregister` datetime NOT NULL,
  `is_aktif` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `group_account`
--
ALTER TABLE `group_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hak_akses`
--
ALTER TABLE `hak_akses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `halaman_promo`
--
ALTER TABLE `halaman_promo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_chat`
--
ALTER TABLE `log_chat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifikasi`
--
ALTER TABLE `notifikasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `oauth_access_tokens_id_session_id_unique` (`id`,`session_id`),
  ADD KEY `oauth_access_tokens_session_id_index` (`session_id`);

--
-- Indexes for table `oauth_access_token_scopes`
--
ALTER TABLE `oauth_access_token_scopes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_token_scopes_access_token_id_index` (`access_token_id`),
  ADD KEY `oauth_access_token_scopes_scope_id_index` (`scope_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_session_id_index` (`session_id`);

--
-- Indexes for table `oauth_auth_code_scopes`
--
ALTER TABLE `oauth_auth_code_scopes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_code_scopes_auth_code_id_index` (`auth_code_id`),
  ADD KEY `oauth_auth_code_scopes_scope_id_index` (`scope_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `oauth_clients_id_secret_unique` (`id`,`secret`);

--
-- Indexes for table `oauth_client_endpoints`
--
ALTER TABLE `oauth_client_endpoints`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `oauth_client_endpoints_client_id_redirect_uri_unique` (`client_id`,`redirect_uri`);

--
-- Indexes for table `oauth_client_grants`
--
ALTER TABLE `oauth_client_grants`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_client_grants_client_id_index` (`client_id`),
  ADD KEY `oauth_client_grants_grant_id_index` (`grant_id`);

--
-- Indexes for table `oauth_client_scopes`
--
ALTER TABLE `oauth_client_scopes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_client_scopes_client_id_index` (`client_id`),
  ADD KEY `oauth_client_scopes_scope_id_index` (`scope_id`);

--
-- Indexes for table `oauth_grants`
--
ALTER TABLE `oauth_grants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_grant_scopes`
--
ALTER TABLE `oauth_grant_scopes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_grant_scopes_grant_id_index` (`grant_id`),
  ADD KEY `oauth_grant_scopes_scope_id_index` (`scope_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`access_token_id`),
  ADD UNIQUE KEY `oauth_refresh_tokens_id_unique` (`id`);

--
-- Indexes for table `oauth_scopes`
--
ALTER TABLE `oauth_scopes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_sessions`
--
ALTER TABLE `oauth_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_sessions_client_id_owner_type_owner_id_index` (`client_id`,`owner_type`,`owner_id`);

--
-- Indexes for table `oauth_session_scopes`
--
ALTER TABLE `oauth_session_scopes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_session_scopes_session_id_index` (`session_id`),
  ADD KEY `oauth_session_scopes_scope_id_index` (`scope_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `shoppingcart`
--
ALTER TABLE `shoppingcart`
  ADD PRIMARY KEY (`identifier`,`instance`);

--
-- Indexes for table `static`
--
ALTER TABLE `static`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `template_background`
--
ALTER TABLE `template_background`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `template_layout`
--
ALTER TABLE `template_layout`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `template_tombol`
--
ALTER TABLE `template_tombol`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_alamat_pengiriman`
--
ALTER TABLE `temp_alamat_pengiriman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_pembayaran`
--
ALTER TABLE `temp_pembayaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_so_voucher`
--
ALTER TABLE `temp_so_voucher`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmst_cabang_perusahaan`
--
ALTER TABLE `tmst_cabang_perusahaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmst_gudang`
--
ALTER TABLE `tmst_gudang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmst_hadiah`
--
ALTER TABLE `tmst_hadiah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmst_kategori_pelanggan`
--
ALTER TABLE `tmst_kategori_pelanggan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmst_kategori_produk`
--
ALTER TABLE `tmst_kategori_produk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmst_master_coa`
--
ALTER TABLE `tmst_master_coa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmst_paket`
--
ALTER TABLE `tmst_paket`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmst_pelanggan`
--
ALTER TABLE `tmst_pelanggan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmst_perusahaan`
--
ALTER TABLE `tmst_perusahaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmst_produk`
--
ALTER TABLE `tmst_produk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmst_supplier`
--
ALTER TABLE `tmst_supplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmst_voucher`
--
ALTER TABLE `tmst_voucher`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_bayar_pembelian_detail`
--
ALTER TABLE `tran_bayar_pembelian_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_bayar_pembelian_header`
--
ALTER TABLE `tran_bayar_pembelian_header`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_bayar_penjualan_detail`
--
ALTER TABLE `tran_bayar_penjualan_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_bayar_penjualan_header`
--
ALTER TABLE `tran_bayar_penjualan_header`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_detail_stok_opname`
--
ALTER TABLE `tran_detail_stok_opname`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_hadiah_gudang`
--
ALTER TABLE `tran_hadiah_gudang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_jurnal_umum`
--
ALTER TABLE `tran_jurnal_umum`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_jurnal_umum_detail`
--
ALTER TABLE `tran_jurnal_umum_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_jurnal_umum_header`
--
ALTER TABLE `tran_jurnal_umum_header`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_kartu_stok_detail`
--
ALTER TABLE `tran_kartu_stok_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_kartu_stok_header`
--
ALTER TABLE `tran_kartu_stok_header`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_laporan_stok_detail`
--
ALTER TABLE `tran_laporan_stok_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_laporan_stok_header`
--
ALTER TABLE `tran_laporan_stok_header`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_mini_banner`
--
ALTER TABLE `tran_mini_banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_mutasi_stok`
--
ALTER TABLE `tran_mutasi_stok`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_mutasi_stok_detail`
--
ALTER TABLE `tran_mutasi_stok_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_mutasi_stok_header`
--
ALTER TABLE `tran_mutasi_stok_header`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_nilai_spesifikasi`
--
ALTER TABLE `tran_nilai_spesifikasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_nota_beli`
--
ALTER TABLE `tran_nota_beli`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_nota_jual`
--
ALTER TABLE `tran_nota_jual`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_paket_gudang`
--
ALTER TABLE `tran_paket_gudang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_paket_pilihan`
--
ALTER TABLE `tran_paket_pilihan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_parameter_spesifikasi`
--
ALTER TABLE `tran_parameter_spesifikasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_pelanggan_cart`
--
ALTER TABLE `tran_pelanggan_cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_pengaturan_jurnal`
--
ALTER TABLE `tran_pengaturan_jurnal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_penyesuaian_stok`
--
ALTER TABLE `tran_penyesuaian_stok`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_penyesuaian_stok_detail`
--
ALTER TABLE `tran_penyesuaian_stok_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_penyesuaian_stok_header`
--
ALTER TABLE `tran_penyesuaian_stok_header`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_po_alamat_pengiriman`
--
ALTER TABLE `tran_po_alamat_pengiriman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_po_detail`
--
ALTER TABLE `tran_po_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_po_diskon`
--
ALTER TABLE `tran_po_diskon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_po_header`
--
ALTER TABLE `tran_po_header`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_produk_galeri`
--
ALTER TABLE `tran_produk_galeri`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_produk_gudang`
--
ALTER TABLE `tran_produk_gudang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_produk_harga`
--
ALTER TABLE `tran_produk_harga`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_produk_pilihan`
--
ALTER TABLE `tran_produk_pilihan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_produk_serial_number`
--
ALTER TABLE `tran_produk_serial_number`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_produk_supplier`
--
ALTER TABLE `tran_produk_supplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_produk_tampil_di_beranda`
--
ALTER TABLE `tran_produk_tampil_di_beranda`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_promo_cashback`
--
ALTER TABLE `tran_promo_cashback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_promo_diskon`
--
ALTER TABLE `tran_promo_diskon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_promo_hadiah`
--
ALTER TABLE `tran_promo_hadiah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_rb_detail`
--
ALTER TABLE `tran_rb_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_rb_header`
--
ALTER TABLE `tran_rb_header`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_rekening_supplier`
--
ALTER TABLE `tran_rekening_supplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_rj_detail`
--
ALTER TABLE `tran_rj_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_rj_header`
--
ALTER TABLE `tran_rj_header`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_service_invoice_detail`
--
ALTER TABLE `tran_service_invoice_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_service_invoice_header`
--
ALTER TABLE `tran_service_invoice_header`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_service_order`
--
ALTER TABLE `tran_service_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_sj_keluar_detail`
--
ALTER TABLE `tran_sj_keluar_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_sj_keluar_header`
--
ALTER TABLE `tran_sj_keluar_header`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_sj_masuk_detail`
--
ALTER TABLE `tran_sj_masuk_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_sj_masuk_header`
--
ALTER TABLE `tran_sj_masuk_header`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_slider_promo`
--
ALTER TABLE `tran_slider_promo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_so_alamat_pengiriman`
--
ALTER TABLE `tran_so_alamat_pengiriman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_so_detail`
--
ALTER TABLE `tran_so_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_so_header`
--
ALTER TABLE `tran_so_header`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_so_pembayaran`
--
ALTER TABLE `tran_so_pembayaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_so_voucher`
--
ALTER TABLE `tran_so_voucher`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_stok_opname`
--
ALTER TABLE `tran_stok_opname`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_stok_opname_detail`
--
ALTER TABLE `tran_stok_opname_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_stok_opname_header`
--
ALTER TABLE `tran_stok_opname_header`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_transaksi_jasa`
--
ALTER TABLE `tran_transaksi_jasa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tref_bank`
--
ALTER TABLE `tref_bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tref_jenis_barang`
--
ALTER TABLE `tref_jenis_barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tref_konten`
--
ALTER TABLE `tref_konten`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tref_kota`
--
ALTER TABLE `tref_kota`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tref_metode_pembayaran`
--
ALTER TABLE `tref_metode_pembayaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tref_metode_pengiriman`
--
ALTER TABLE `tref_metode_pengiriman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tref_nomor_invoice`
--
ALTER TABLE `tref_nomor_invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tref_pengaturan_po`
--
ALTER TABLE `tref_pengaturan_po`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tref_pengaturan_so`
--
ALTER TABLE `tref_pengaturan_so`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tref_provinsi`
--
ALTER TABLE `tref_provinsi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tref_sumber_data`
--
ALTER TABLE `tref_sumber_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tref_syarat_ketentuan`
--
ALTER TABLE `tref_syarat_ketentuan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `group_account`
--
ALTER TABLE `group_account`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `hak_akses`
--
ALTER TABLE `hak_akses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `halaman_promo`
--
ALTER TABLE `halaman_promo`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `log_chat`
--
ALTER TABLE `log_chat`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `notifikasi`
--
ALTER TABLE `notifikasi`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `oauth_access_token_scopes`
--
ALTER TABLE `oauth_access_token_scopes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oauth_auth_code_scopes`
--
ALTER TABLE `oauth_auth_code_scopes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oauth_client_endpoints`
--
ALTER TABLE `oauth_client_endpoints`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oauth_client_grants`
--
ALTER TABLE `oauth_client_grants`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oauth_client_scopes`
--
ALTER TABLE `oauth_client_scopes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oauth_grant_scopes`
--
ALTER TABLE `oauth_grant_scopes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oauth_sessions`
--
ALTER TABLE `oauth_sessions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=199;
--
-- AUTO_INCREMENT for table `oauth_session_scopes`
--
ALTER TABLE `oauth_session_scopes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `static`
--
ALTER TABLE `static`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `template_background`
--
ALTER TABLE `template_background`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `template_layout`
--
ALTER TABLE `template_layout`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `template_tombol`
--
ALTER TABLE `template_tombol`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `temp_alamat_pengiriman`
--
ALTER TABLE `temp_alamat_pengiriman`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT for table `temp_pembayaran`
--
ALTER TABLE `temp_pembayaran`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT for table `temp_so_voucher`
--
ALTER TABLE `temp_so_voucher`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tmst_cabang_perusahaan`
--
ALTER TABLE `tmst_cabang_perusahaan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tmst_gudang`
--
ALTER TABLE `tmst_gudang`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tmst_hadiah`
--
ALTER TABLE `tmst_hadiah`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tmst_kategori_pelanggan`
--
ALTER TABLE `tmst_kategori_pelanggan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tmst_kategori_produk`
--
ALTER TABLE `tmst_kategori_produk`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `tmst_master_coa`
--
ALTER TABLE `tmst_master_coa`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `tmst_paket`
--
ALTER TABLE `tmst_paket`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tmst_pelanggan`
--
ALTER TABLE `tmst_pelanggan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `tmst_perusahaan`
--
ALTER TABLE `tmst_perusahaan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tmst_produk`
--
ALTER TABLE `tmst_produk`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tmst_supplier`
--
ALTER TABLE `tmst_supplier`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tmst_voucher`
--
ALTER TABLE `tmst_voucher`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tran_bayar_pembelian_detail`
--
ALTER TABLE `tran_bayar_pembelian_detail`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tran_bayar_pembelian_header`
--
ALTER TABLE `tran_bayar_pembelian_header`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tran_bayar_penjualan_detail`
--
ALTER TABLE `tran_bayar_penjualan_detail`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tran_bayar_penjualan_header`
--
ALTER TABLE `tran_bayar_penjualan_header`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tran_detail_stok_opname`
--
ALTER TABLE `tran_detail_stok_opname`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tran_hadiah_gudang`
--
ALTER TABLE `tran_hadiah_gudang`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tran_jurnal_umum`
--
ALTER TABLE `tran_jurnal_umum`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tran_jurnal_umum_detail`
--
ALTER TABLE `tran_jurnal_umum_detail`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tran_jurnal_umum_header`
--
ALTER TABLE `tran_jurnal_umum_header`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tran_kartu_stok_detail`
--
ALTER TABLE `tran_kartu_stok_detail`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=155;
--
-- AUTO_INCREMENT for table `tran_kartu_stok_header`
--
ALTER TABLE `tran_kartu_stok_header`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `tran_laporan_stok_detail`
--
ALTER TABLE `tran_laporan_stok_detail`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;
--
-- AUTO_INCREMENT for table `tran_laporan_stok_header`
--
ALTER TABLE `tran_laporan_stok_header`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `tran_mini_banner`
--
ALTER TABLE `tran_mini_banner`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tran_mutasi_stok`
--
ALTER TABLE `tran_mutasi_stok`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tran_mutasi_stok_detail`
--
ALTER TABLE `tran_mutasi_stok_detail`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tran_mutasi_stok_header`
--
ALTER TABLE `tran_mutasi_stok_header`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tran_nilai_spesifikasi`
--
ALTER TABLE `tran_nilai_spesifikasi`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tran_nota_beli`
--
ALTER TABLE `tran_nota_beli`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tran_nota_jual`
--
ALTER TABLE `tran_nota_jual`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tran_paket_gudang`
--
ALTER TABLE `tran_paket_gudang`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tran_paket_pilihan`
--
ALTER TABLE `tran_paket_pilihan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tran_parameter_spesifikasi`
--
ALTER TABLE `tran_parameter_spesifikasi`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tran_pelanggan_cart`
--
ALTER TABLE `tran_pelanggan_cart`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `tran_pengaturan_jurnal`
--
ALTER TABLE `tran_pengaturan_jurnal`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `tran_penyesuaian_stok`
--
ALTER TABLE `tran_penyesuaian_stok`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tran_penyesuaian_stok_detail`
--
ALTER TABLE `tran_penyesuaian_stok_detail`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tran_penyesuaian_stok_header`
--
ALTER TABLE `tran_penyesuaian_stok_header`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tran_po_alamat_pengiriman`
--
ALTER TABLE `tran_po_alamat_pengiriman`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tran_po_detail`
--
ALTER TABLE `tran_po_detail`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tran_po_diskon`
--
ALTER TABLE `tran_po_diskon`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tran_po_header`
--
ALTER TABLE `tran_po_header`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tran_produk_galeri`
--
ALTER TABLE `tran_produk_galeri`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tran_produk_gudang`
--
ALTER TABLE `tran_produk_gudang`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `tran_produk_harga`
--
ALTER TABLE `tran_produk_harga`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tran_produk_pilihan`
--
ALTER TABLE `tran_produk_pilihan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tran_produk_serial_number`
--
ALTER TABLE `tran_produk_serial_number`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=181;
--
-- AUTO_INCREMENT for table `tran_produk_supplier`
--
ALTER TABLE `tran_produk_supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tran_produk_tampil_di_beranda`
--
ALTER TABLE `tran_produk_tampil_di_beranda`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tran_promo_cashback`
--
ALTER TABLE `tran_promo_cashback`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tran_promo_diskon`
--
ALTER TABLE `tran_promo_diskon`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tran_promo_hadiah`
--
ALTER TABLE `tran_promo_hadiah`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tran_rb_detail`
--
ALTER TABLE `tran_rb_detail`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tran_rb_header`
--
ALTER TABLE `tran_rb_header`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tran_rekening_supplier`
--
ALTER TABLE `tran_rekening_supplier`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tran_rj_detail`
--
ALTER TABLE `tran_rj_detail`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tran_rj_header`
--
ALTER TABLE `tran_rj_header`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tran_service_invoice_detail`
--
ALTER TABLE `tran_service_invoice_detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tran_service_invoice_header`
--
ALTER TABLE `tran_service_invoice_header`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tran_service_order`
--
ALTER TABLE `tran_service_order`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tran_sj_keluar_detail`
--
ALTER TABLE `tran_sj_keluar_detail`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tran_sj_keluar_header`
--
ALTER TABLE `tran_sj_keluar_header`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tran_sj_masuk_detail`
--
ALTER TABLE `tran_sj_masuk_detail`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `tran_sj_masuk_header`
--
ALTER TABLE `tran_sj_masuk_header`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tran_slider_promo`
--
ALTER TABLE `tran_slider_promo`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tran_so_alamat_pengiriman`
--
ALTER TABLE `tran_so_alamat_pengiriman`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `tran_so_detail`
--
ALTER TABLE `tran_so_detail`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `tran_so_header`
--
ALTER TABLE `tran_so_header`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `tran_so_pembayaran`
--
ALTER TABLE `tran_so_pembayaran`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `tran_so_voucher`
--
ALTER TABLE `tran_so_voucher`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tran_stok_opname`
--
ALTER TABLE `tran_stok_opname`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tran_stok_opname_detail`
--
ALTER TABLE `tran_stok_opname_detail`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tran_stok_opname_header`
--
ALTER TABLE `tran_stok_opname_header`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tran_transaksi_jasa`
--
ALTER TABLE `tran_transaksi_jasa`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tref_bank`
--
ALTER TABLE `tref_bank`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tref_jenis_barang`
--
ALTER TABLE `tref_jenis_barang`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tref_konten`
--
ALTER TABLE `tref_konten`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tref_kota`
--
ALTER TABLE `tref_kota`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=515;
--
-- AUTO_INCREMENT for table `tref_metode_pembayaran`
--
ALTER TABLE `tref_metode_pembayaran`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tref_metode_pengiriman`
--
ALTER TABLE `tref_metode_pengiriman`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tref_nomor_invoice`
--
ALTER TABLE `tref_nomor_invoice`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `tref_pengaturan_po`
--
ALTER TABLE `tref_pengaturan_po`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tref_pengaturan_so`
--
ALTER TABLE `tref_pengaturan_so`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tref_provinsi`
--
ALTER TABLE `tref_provinsi`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `tref_sumber_data`
--
ALTER TABLE `tref_sumber_data`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tref_syarat_ketentuan`
--
ALTER TABLE `tref_syarat_ketentuan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD CONSTRAINT `oauth_access_tokens_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `oauth_sessions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `oauth_access_token_scopes`
--
ALTER TABLE `oauth_access_token_scopes`
  ADD CONSTRAINT `oauth_access_token_scopes_access_token_id_foreign` FOREIGN KEY (`access_token_id`) REFERENCES `oauth_access_tokens` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `oauth_access_token_scopes_scope_id_foreign` FOREIGN KEY (`scope_id`) REFERENCES `oauth_scopes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD CONSTRAINT `oauth_auth_codes_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `oauth_sessions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `oauth_auth_code_scopes`
--
ALTER TABLE `oauth_auth_code_scopes`
  ADD CONSTRAINT `oauth_auth_code_scopes_auth_code_id_foreign` FOREIGN KEY (`auth_code_id`) REFERENCES `oauth_auth_codes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `oauth_auth_code_scopes_scope_id_foreign` FOREIGN KEY (`scope_id`) REFERENCES `oauth_scopes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `oauth_client_endpoints`
--
ALTER TABLE `oauth_client_endpoints`
  ADD CONSTRAINT `oauth_client_endpoints_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `oauth_clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `oauth_client_grants`
--
ALTER TABLE `oauth_client_grants`
  ADD CONSTRAINT `oauth_client_grants_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `oauth_clients` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `oauth_client_grants_grant_id_foreign` FOREIGN KEY (`grant_id`) REFERENCES `oauth_grants` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `oauth_client_scopes`
--
ALTER TABLE `oauth_client_scopes`
  ADD CONSTRAINT `oauth_client_scopes_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `oauth_clients` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `oauth_client_scopes_scope_id_foreign` FOREIGN KEY (`scope_id`) REFERENCES `oauth_scopes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `oauth_grant_scopes`
--
ALTER TABLE `oauth_grant_scopes`
  ADD CONSTRAINT `oauth_grant_scopes_grant_id_foreign` FOREIGN KEY (`grant_id`) REFERENCES `oauth_grants` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `oauth_grant_scopes_scope_id_foreign` FOREIGN KEY (`scope_id`) REFERENCES `oauth_scopes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD CONSTRAINT `oauth_refresh_tokens_access_token_id_foreign` FOREIGN KEY (`access_token_id`) REFERENCES `oauth_access_tokens` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `oauth_sessions`
--
ALTER TABLE `oauth_sessions`
  ADD CONSTRAINT `oauth_sessions_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `oauth_clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `oauth_session_scopes`
--
ALTER TABLE `oauth_session_scopes`
  ADD CONSTRAINT `oauth_session_scopes_scope_id_foreign` FOREIGN KEY (`scope_id`) REFERENCES `oauth_scopes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `oauth_session_scopes_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `oauth_sessions` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
