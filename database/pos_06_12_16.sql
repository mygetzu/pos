-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 06, 2016 at 03:03 PM
-- Server version: 5.7.13-0ubuntu0.16.04.2
-- PHP Version: 7.0.8-0ubuntu0.16.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pos`
--

-- --------------------------------------------------------

--
-- Table structure for table `group_account`
--

CREATE TABLE `group_account` (
  `id` int(10) UNSIGNED NOT NULL,
  `kode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `debit_or_kredit` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `group_account`
--

INSERT INTO `group_account` (`id`, `kode`, `nama`, `debit_or_kredit`, `group_kode`) VALUES
(1, '001', 'Aktiva Tetap', 'D', ''),
(2, '002', 'Aktiva Lancar', 'D', ''),
(3, '003', 'Passiva', 'K', '');

-- --------------------------------------------------------

--
-- Table structure for table `hak_akses`
--

CREATE TABLE `hak_akses` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deskripsi` longtext COLLATE utf8_unicode_ci NOT NULL,
  `menu_akses` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hak_akses`
--

INSERT INTO `hak_akses` (`id`, `nama`, `deskripsi`, `menu_akses`, `created_at`, `updated_at`) VALUES
(1, 'administrator', 'administrator', '1-2-3-4-5-6-7-8-9-10-11-12-13-14-15-16-17-18-19-20-21-22-23-24-25-26-27-28-29-30-31-32-33-34-35-36', NULL, NULL),
(2, 'pelanggan', 'pelanggan', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `halaman_promo`
--

CREATE TABLE `halaman_promo` (
  `id` int(10) UNSIGNED NOT NULL,
  `file_gambar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_aktif` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `halaman_promo`
--

INSERT INTO `halaman_promo` (`id`, `file_gambar`, `is_aktif`, `item_order`, `created_at`, `updated_at`) VALUES
(1, 'I4H-banner.jpg', '1', 1, NULL, NULL),
(2, '01S-banner.jpg', '1', 2, NULL, NULL),
(3, 'ZP0-banner.jpg', '1', 3, NULL, NULL),
(4, 'SZC-banner.jpg', '1', 4, NULL, NULL),
(5, 'T8U-banner.png', '1', 5, NULL, NULL),
(6, 'SNS-banner.jpg', '1', 6, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `log_chat`
--

CREATE TABLE `log_chat` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id_a` int(11) NOT NULL,
  `user_id_b` int(11) NOT NULL,
  `pesan` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `log_chat`
--

INSERT INTO `log_chat` (`id`, `user_id_a`, `user_id_b`, `pesan`, `created_at`, `updated_at`) VALUES
(1, 4, 1, '[{"user_id":"1","tanggal":"28\\/10\\/2016 19:39:39","pesan":"cek"},{"user_id":"4","tanggal":"28\\/10\\/2016 19:39:48","pesan":"iya"},{"user_id":"1","tanggal":"28\\/10\\/2016 19:40:24","pesan":"cek"},{"user_id":"4","tanggal":"28\\/10\\/2016 19:40:44","pesan":"selamat malam"},{"user_id":"1","tanggal":"28\\/10\\/2016 19:40:48","pesan":"oke"},{"user_id":"1","tanggal":"28\\/10\\/2016 19:41:9","pesan":"baik"},{"user_id":"4","tanggal":"31\\/10\\/2016 17:29:42","pesan":"halo"},{"user_id":"1","tanggal":"31\\/10\\/2016 17:29:57","pesan":"iyo"},{"user_id":"1","tanggal":"31\\/10\\/2016 17:30:5","pesan":"iso jadine"},{"user_id":"1","tanggal":"31\\/10\\/2016 17:31:48","pesan":"selamat sore apa ada yang bisa saya bantu??"},{"user_id":"4","tanggal":"31\\/10\\/2016 17:33:14","pesan":"cuy"}]', NULL, NULL),
(2, 1, 8, '[{"user_id":"8","tanggal":"31\\/10\\/2016 17:33:23","pesan":"cek"},{"user_id":"8","tanggal":"8\\/11\\/2016 19:56:56","pesan":"cek"},{"user_id":"8","tanggal":"8\\/11\\/2016 19:56:56","pesan":"cek"},{"user_id":"8","tanggal":"8\\/11\\/2016 19:56:56","pesan":"cek"},{"user_id":"1","tanggal":"8\\/11\\/2016 19:57:9","pesan":"tes 123"}]', NULL, NULL),
(3, 1, 6, '[{"user_id":"6","tanggal":"8\\/11\\/2016 20:0:9","pesan":"apa Kabar?"},{"user_id":"6","tanggal":"8\\/11\\/2016 20:0:9","pesan":"apa Kabar?"},{"user_id":"6","tanggal":"8\\/11\\/2016 20:0:10","pesan":"apa Kabar?"},{"user_id":"1","tanggal":"8\\/11\\/2016 20:0:24","pesan":"yuppppp"}]', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `route` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `nama`, `route`, `created_at`, `updated_at`) VALUES
(1, 'dashboard', 'beranda_admin', NULL, NULL),
(2, 'perusahaan', 'perusahaan', NULL, NULL),
(3, 'chat', 'chat', NULL, NULL),
(4, 'data referensi', 'data_referensi', NULL, NULL),
(5, 'ecommerce', 'ecommerce', NULL, NULL),
(6, 'group account', 'group_account', NULL, NULL),
(7, 'master coa', 'master_coa', NULL, NULL),
(8, 'pengaturan jurnal', 'pengaturan_jurnal', NULL, NULL),
(9, 'akun saya', 'akun_saya', NULL, NULL),
(10, 'data akun', 'data_user', NULL, NULL),
(11, 'hak akses', 'hak_akses', NULL, NULL),
(12, 'kategori pelanggan', 'kategori_pelanggan', NULL, NULL),
(13, 'pelanggan', 'pelanggan', NULL, NULL),
(14, 'supplier', 'supplier', NULL, NULL),
(15, 'kategori produk', 'kategori_produk', NULL, NULL),
(16, 'gudang', 'gudang', NULL, NULL),
(17, 'produk', 'produk', NULL, NULL),
(18, 'hadiah', 'hadiah', NULL, NULL),
(19, 'paket produk', 'paket_produk', NULL, NULL),
(20, 'purchase order', 'purchase_order', NULL, NULL),
(21, 'surat jalan_masuk', 'surat_jalan_masuk', NULL, NULL),
(22, 'nota beli', 'nota_beli', NULL, NULL),
(23, 'sales order', 'sales_order', NULL, NULL),
(24, 'surat jalan_keluar', 'surat_jalan_keluar', NULL, NULL),
(25, 'nota jual', 'nota_jual', NULL, NULL),
(26, 'retur jual', 'retur_jual', NULL, NULL),
(27, 'retur beli', 'retur_beli', NULL, NULL),
(28, 'servis', 'servis', NULL, NULL),
(29, 'voucher', 'voucher', NULL, NULL),
(30, 'penyesuaian stok', 'penyesuaian_stok', NULL, NULL),
(31, 'mutasi stok', 'mutasi_stok', NULL, NULL),
(32, 'stok opname', 'stok_opname', NULL, NULL),
(33, 'laporan stok', 'laporan_stok', NULL, NULL),
(34, 'kartu stok', 'kartu_stok', NULL, NULL),
(35, 'laporan penjualan', 'laporan_penjualan', NULL, NULL),
(36, 'jurnal umum', 'jurnal_umum', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_09_04_174722_table_master_tmst', 1),
('2016_09_04_174811_table_reference', 1),
('2016_09_04_174852_table_transaction', 1),
('2016_09_04_174939_table_temp_and_others', 1),
('2016_09_22_120241_group_account', 2),
('2016_09_22_144937_tref_nomor_invoice', 2),
('2016_09_23_120450_tmst_jurnal_umum', 3),
('2016_09_28_065807_tabel_static', 4),
('2016_10_03_052820_tran_po_diskon', 5),
('2016_10_04_143101_tabel_checkout_ecommerce', 6),
('2016_10_07_062132_notifikasi', 7),
('2016_10_08_074901_produk_tampil_di_beranda', 8),
('2016_10_12_175938_temp_hadiah_voucher', 9),
('2016_10_13_195657_paket_gudang', 10),
('2016_10_17_084317_mutasi_stok', 11),
('2016_10_19_094417_stok_opname', 12),
('2016_10_20_143558_laporan_kartu_stok', 13),
('2016_10_26_112745_pengaturan_po_so', 14),
('2016_10_27_075459_menu', 14),
('2016_11_10_103450_jurnal_umum', 16),
('2016_11_03_085148_new_transaksi', 17),
('2016_11_22_102411_so_pembayaran', 19),
('2016_11_20_124115_promo_creator', 20),
('2016_12_02_102408_tabel_pembayaran', 21),
('2016_12_05_111040_tabel_spesifikasi', 22),
('2016_12_06_142150_tambah_file_gambar_kategori_produk', 23);

-- --------------------------------------------------------

--
-- Table structure for table `notifikasi`
--

CREATE TABLE `notifikasi` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pesan` text COLLATE utf8_unicode_ci,
  `user_id_target` int(11) DEFAULT NULL,
  `hak_akses_id_target` int(11) DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_aktif` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `notifikasi`
--

INSERT INTO `notifikasi` (`id`, `kode`, `pesan`, `user_id_target`, `hak_akses_id_target`, `link`, `is_aktif`, `created_at`, `updated_at`) VALUES
(1, 'registrasi', '<i class="fa fa-user"> Registrasi baru akun Muhammad Riduwan</i>', NULL, 1, 'data_user', 0, '2016-10-20 12:30:18', '2016-10-20 12:30:50'),
(2, 'sales_order', '<i class="fa fa-shopping-cart"> Order Baru oleh akun guest</i>', NULL, 1, 'sales_order', 0, '2016-10-20 20:26:55', '2016-10-31 13:04:30'),
(3, 'registrasi', '<i class="fa fa-user"> Registrasi baru akun Dody Soegiarto</i>', NULL, 1, 'data_user', 0, '2016-10-24 03:18:59', '2016-10-31 13:04:45'),
(4, 'registrasi', '<i class="fa fa-user"> Registrasi baru akun Red</i>', NULL, 1, 'data_user', 0, '2016-10-24 06:31:51', '2016-10-31 13:04:35'),
(5, 'sales_order', '<i class="fa fa-shopping-cart"> Order Baru oleh akun Datamart Computer</i>', NULL, 1, 'sales_order', 0, '2016-10-28 20:31:17', '2016-10-31 13:04:48'),
(6, 'sales_order', '<i class="fa fa-shopping-cart"> Order Baru oleh akun Muhammad Riduwan</i>', NULL, 1, 'sales_order', 0, '2016-10-28 20:33:39', '2016-10-31 13:04:43'),
(7, 'sales_order', '<i class="fa fa-shopping-cart"> Order Baru oleh akun Muhammad Riduwan</i>', NULL, 1, 'sales_order', 0, '2016-10-28 20:35:16', '2016-10-31 13:04:40'),
(8, 'sales_order', '<i class="fa fa-shopping-cart"> Order Baru oleh akun Datamart Computer</i>', NULL, 1, 'sales_order', 0, '2016-10-28 20:39:37', '2016-10-31 13:04:27'),
(9, 'sales_order', '<i class="fa fa-shopping-cart"> Order Baru oleh akun guest</i>', NULL, 1, 'sales_order', 0, '2016-11-01 09:17:05', '2016-11-01 02:18:42'),
(10, 'sales_order', '<i class="fa fa-shopping-cart"> Order Baru oleh akun Muhammad Riduwan</i>', NULL, 1, 'sales_order', 0, '2016-11-04 20:04:56', '2016-11-04 13:32:19'),
(11, 'sales_order', '<i class="fa fa-shopping-cart"> Order Baru oleh akun guest</i>', NULL, 1, 'sales_order', 0, '2016-11-07 16:27:22', '2016-11-07 09:32:46'),
(12, 'sales_order', '<i class="fa fa-shopping-cart"> Order Baru oleh akun Muhammad Riduwan</i>', NULL, 1, 'sales_order', 1, '2016-11-25 18:40:15', '2016-11-25 18:40:15');

-- --------------------------------------------------------

--
-- Table structure for table `notif_chat`
--

CREATE TABLE `notif_chat` (
  `user_id` int(11) NOT NULL,
  `user_id_from` int(11) NOT NULL,
  `pesan` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `notif_chat`
--

INSERT INTO `notif_chat` (`user_id`, `user_id_from`, `pesan`) VALUES
(4, 1, '{"id_from": "4","tanggal": "28/10/2016 19:39:39","pesan": "cek"}'),
(4, 1, '{"id_from": "4","tanggal": "28/10/2016 19:40:24","pesan": "cek"}'),
(4, 1, '{"id_from": "4","tanggal": "28/10/2016 19:40:48","pesan": "oke"}'),
(4, 1, '{"id_from": "4","tanggal": "28/10/2016 19:41:9","pesan": "baik"}'),
(4, 1, '{"id_from": "4","tanggal": "31/10/2016 17:29:57","pesan": "iyo"}'),
(4, 1, '{"id_from": "4","tanggal": "31/10/2016 17:30:5","pesan": "iso jadine"}'),
(4, 1, '{"id_from": "4","tanggal": "31/10/2016 17:31:48","pesan": "selamat sore apa ada yang bisa saya bantu??"}'),
(1, 6, '{"id_from": "1","tanggal": "8/11/2016 20:0:9","pesan": "apa Kabar?"}'),
(1, 6, '{"id_from": "1","tanggal": "8/11/2016 20:0:9","pesan": "apa Kabar?"}'),
(1, 6, '{"id_from": "1","tanggal": "8/11/2016 20:0:10","pesan": "apa Kabar?"}');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `static`
--

CREATE TABLE `static` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `static`
--

INSERT INTO `static` (`id`, `nama`, `value`, `created_at`, `updated_at`) VALUES
(1, 'gambar_produk_default', 'GPDQHAPO77Y.jpg', NULL, NULL),
(2, 'cara_belanja', '<h2>Belanja di galerindoteknologi.com kini semakin Lengkap, mudah, dan aman</h2>\r\n<p>Belanja gadget, notebook, kamera, elektronik, dan seluruh kebutuhan Anda, Anti Repot! Dari mulai sensasi belanja online, berkonsultasi langsung via telepon/email/chat, hingga kunjungan langsung ke store. Bahkan sekarang bisa belanja via Smartphone dan Gadget Anda dengan Bhinneka.Com Mobile Version, Bhinneka Apps bahkan bisa langsung hitung Tarif Kirim dengan Aplikasi Tarif Kirim.<br />Belanja Online Via Shopcart<br />Cara belanja paling MUDAH hanya dengan klik tombol \'BELI\'di halaman \'Product List\' atau di \'Detail Page\' produk yang Anda inginkan, maka produk tersebut akan masuk ke keranjang belanja Anda (Shop Cart). Selanjutnya ikuti langkah demi langkah PERINCIAN BELANJA ANDA : Data Pengiriman &rarr; Cara Pembayaran &rarr; Konfirmasi hingga selesai.</p>\r\n<h2>Cara Berbelanja</h2>\r\n<p>Saran kami, untuk memudahkan proses berbelanja Online di masa yang akan datang, daftarlah menjadi Member galerindoteknologi.com terlebih dahulu. Selain dapat menghemat waktu berbelanja, Anda juga dapat melihat histori transaksi belanja, memiliki kesempatan berbelanja dengan diskon khusus member berupa eCoupon, dan masih banyak lagi keuntungan lainnya.</p>\r\n<p>&nbsp;</p>', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tema`
--

CREATE TABLE `tema` (
  `warna_dasar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `warna_huruf` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `layout_id` int(11) NOT NULL,
  `background_id` int(11) NOT NULL,
  `tombol_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `template_background`
--

CREATE TABLE `template_background` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_gambar` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `template_layout`
--

CREATE TABLE `template_layout` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `template_tombol`
--

CREATE TABLE `template_tombol` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jenis` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_icon_button` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `btn_primary` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `btn_default` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `btn_warning` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `btn_danger` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `btn_info` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `temp_alamat_pengiriman`
--

CREATE TABLE `temp_alamat_pengiriman` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `no_nota` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kota_id` int(11) DEFAULT NULL,
  `kode_pos` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metode_pengiriman_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `temp_alamat_pengiriman`
--

INSERT INTO `temp_alamat_pengiriman` (`id`, `no_nota`, `tanggal`, `nama`, `email`, `alamat`, `kota_id`, `kode_pos`, `hp`, `metode_pengiriman_id`, `created_at`, `updated_at`) VALUES
(13, NULL, '2016-10-28 20:38:30', 'Datamart Computer', 'dodyformello@gmail.com', 'Berbek Industri II/18', 242, '61256', '(+62)812-3278-760', 1, '2016-10-28 20:38:30', '2016-10-28 20:38:30');

-- --------------------------------------------------------

--
-- Table structure for table `temp_pembayaran`
--

CREATE TABLE `temp_pembayaran` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `no_nota` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tanggal` bigint(20) DEFAULT NULL,
  `metode_pembayaran_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bayar_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomor_pembayaran` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `temp_pembayaran`
--

INSERT INTO `temp_pembayaran` (`id`, `no_nota`, `tanggal`, `metode_pembayaran_id`, `bayar_id`, `nomor_pembayaran`, `bank_id`, `created_at`, `updated_at`) VALUES
(13, NULL, 2016, '6', NULL, NULL, NULL, '2016-10-28 20:35:39', '2016-10-28 20:35:39'),
(14, NULL, 2016, '6', NULL, NULL, NULL, '2016-10-28 20:38:35', '2016-10-28 20:38:35'),
(19, NULL, 2016, '6', NULL, NULL, NULL, '2016-11-25 18:39:18', '2016-11-25 18:39:18');

-- --------------------------------------------------------

--
-- Table structure for table `temp_so_voucher`
--

CREATE TABLE `temp_so_voucher` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `no_nota` int(11) DEFAULT NULL,
  `voucher_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `temp_stok_opname`
--

CREATE TABLE `temp_stok_opname` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `gudang_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `produk_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `stok` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmst_cabang_perusahaan`
--

CREATE TABLE `tmst_cabang_perusahaan` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alamat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kota_id` int(11) DEFAULT NULL,
  `telp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kontak_person` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `perusahaan_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tmst_cabang_perusahaan`
--

INSERT INTO `tmst_cabang_perusahaan` (`id`, `nama`, `alamat`, `kota_id`, `telp`, `kontak_person`, `perusahaan_id`, `created_at`, `updated_at`) VALUES
(1, 'Indonusa Solutama', 'Ruko Palacio B-20 Nginden', 264, '03192121800', 'Novi', 1, NULL, '2016-10-18 02:23:16');

-- --------------------------------------------------------

--
-- Table structure for table `tmst_gudang`
--

CREATE TABLE `tmst_gudang` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alamat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kota_id` int(11) DEFAULT NULL,
  `telp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supervisor` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hp_supervisor` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_aktif` int(11) DEFAULT NULL,
  `format_no_mutasi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tmst_gudang`
--

INSERT INTO `tmst_gudang` (`id`, `nama`, `alamat`, `kota_id`, `telp`, `supervisor`, `hp_supervisor`, `longitude`, `latitude`, `is_aktif`, `format_no_mutasi`, `created_at`, `updated_at`) VALUES
(1, 'Gudang Toko', 'Hi Tech Mall Lt.Dasar Blok.D No.17-19\r\nJl. Kusuma Bangsa No.116-118', 264, '0315477085', 'Vera', '(+62)8123-0434343', '', '', 1, '', NULL, NULL),
(2, 'Gudang Kantor', 'Ruko Palacio | City Pride\r\nJl. Nginden Semolo No.42 Blok.B20', 264, '0315913493', 'Dian', '(+62)8123-456789', '', '', 1, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tmst_hadiah`
--

CREATE TABLE `tmst_hadiah` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `satuan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `berat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deskripsi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stok` int(11) DEFAULT NULL,
  `stok_dipesan` int(11) DEFAULT NULL,
  `file_gambar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_aktif` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tmst_hadiah`
--

INSERT INTO `tmst_hadiah` (`id`, `kode`, `nama`, `satuan`, `berat`, `deskripsi`, `stok`, `stok_dipesan`, `file_gambar`, `is_aktif`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Flashdisk 16GB', 'Biji', '0', '<p>Flashdisk 16GB recovery windows</p>', 0, 0, '', NULL, NULL, NULL),
(2, NULL, 'Flashdisk 8GB', 'Biji', '0', '<p>Flashdisk 8GB recovery windows</p>', 0, 0, '', NULL, NULL, NULL),
(3, NULL, 'Keyboard Protector 15" Clear', 'Biji', '0', '<p>Free Keyboard Protector u/ pembelian Notebook 15"</p>', 0, 0, '', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tmst_kategori_pelanggan`
--

CREATE TABLE `tmst_kategori_pelanggan` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deskripsi` longtext COLLATE utf8_unicode_ci,
  `default_diskon` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tmst_kategori_pelanggan`
--

INSERT INTO `tmst_kategori_pelanggan` (`id`, `nama`, `deskripsi`, `default_diskon`, `created_at`, `updated_at`) VALUES
(1, 'Diamond Customer', 'Omzet Penjualan di atas Rp. 200,000,000 per bulan', 5, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tmst_kategori_produk`
--

CREATE TABLE `tmst_kategori_produk` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug_nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deskripsi` longtext COLLATE utf8_unicode_ci,
  `kolom_spesifikasi` text COLLATE utf8_unicode_ci,
  `is_aktif` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `file_gambar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tmst_kategori_produk`
--

INSERT INTO `tmst_kategori_produk` (`id`, `nama`, `slug_nama`, `deskripsi`, `kolom_spesifikasi`, `is_aktif`, `created_at`, `updated_at`, `file_gambar`) VALUES
(1, 'Accesories Notebook', 'accesories-notebook', 'Accesories Notebook', '', 1, NULL, '2016-12-06 14:56:44', 'accesories-notebook.png'),
(2, 'All In One', 'all-in-one', 'All In One', '', 1, NULL, '2016-12-06 14:57:01', 'all-in-one.png'),
(3, 'LED Monitor', 'led-monitor', 'LED Monitor', '', 1, NULL, '2016-12-06 14:57:17', 'led-monitor.png'),
(4, 'Notebook', 'notebook', 'Notebook', '', 1, NULL, '2016-12-06 14:57:40', 'notebook.png'),
(5, 'PC Desktop', 'pc-desktop', 'PC Desktop', '', 1, NULL, '2016-12-06 14:58:47', 'pc-desktop.png'),
(6, 'Server', 'server', 'Server', '', 1, NULL, '2016-12-06 14:59:03', 'server.png'),
(7, 'Software', 'software', 'Software', '', 1, NULL, '2016-12-06 14:59:29', 'software.png'),
(8, 'Sparepart', 'sparepart', 'Sparepart', '', 1, NULL, '2016-12-06 15:00:02', 'sparepart.png'),
(9, 'Workstation', 'workstation', 'Workstation', '', 1, NULL, '2016-12-06 15:00:18', 'workstation.png');

-- --------------------------------------------------------

--
-- Table structure for table `tmst_master_coa`
--

CREATE TABLE `tmst_master_coa` (
  `id` int(10) UNSIGNED NOT NULL,
  `kode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `parent` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `debit_or_kredit` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `group_kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tmst_master_coa`
--

INSERT INTO `tmst_master_coa` (`id`, `kode`, `nama`, `level`, `parent`, `debit_or_kredit`, `group_kode`, `created_at`, `updated_at`) VALUES
(1, '001', 'Kas', 0, '-', '', '002', '2016-10-03 15:07:05', '2016-11-21 13:11:14'),
(2, '001.001', 'kas rupiah', 1, '001', '', '', '2016-10-06 02:13:45', '2016-10-06 02:13:45'),
(3, '001.002', 'Kas USD', 1, '001', '', '', '2016-10-06 02:14:21', '2016-10-06 02:14:21'),
(4, '002', 'Persediaan', 0, '-', '', '002', '2016-10-06 02:19:35', '2016-11-21 13:11:38'),
(5, '003', 'Piutang', 0, '-', '', '002', '2016-10-06 02:20:47', '2016-11-21 13:12:04'),
(6, '003.001', 'Piutang Usaha', 1, '003', '', '', '2016-10-06 02:26:14', '2016-11-21 14:06:45'),
(10, '004', 'Hutang', 0, '-', '', '003', '2016-11-21 13:12:29', '2016-11-21 13:12:29'),
(11, '004.001', 'Hutang Usaha', 1, '004', '', '', '2016-11-21 13:12:43', '2016-11-21 13:12:43');

-- --------------------------------------------------------

--
-- Table structure for table `tmst_paket`
--

CREATE TABLE `tmst_paket` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug_nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `harga_total` bigint(20) DEFAULT NULL,
  `deskripsi` longtext COLLATE utf8_unicode_ci,
  `file_gambar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stok` int(11) NOT NULL,
  `stok_dipesan` int(11) NOT NULL,
  `is_aktif` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tmst_paket`
--

INSERT INTO `tmst_paket` (`id`, `kode`, `nama`, `slug_nama`, `harga_total`, `deskripsi`, `file_gambar`, `stok`, `stok_dipesan`, `is_aktif`, `created_at`, `updated_at`) VALUES
(1, '', 'tes', 'tes', 7000000, NULL, '', 0, 0, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tmst_pelanggan`
--

CREATE TABLE `tmst_pelanggan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alamat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kota_id` int(11) DEFAULT NULL,
  `telp1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telp2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kode_pos` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama_sales` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hp_sales` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deskripsi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kategori_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tmst_pelanggan`
--

INSERT INTO `tmst_pelanggan` (`id`, `user_id`, `nama`, `email`, `alamat`, `kota_id`, `telp1`, `telp2`, `kode_pos`, `nama_sales`, `hp_sales`, `deskripsi`, `kategori_id`, `created_at`, `updated_at`) VALUES
(1, NULL, 'guest', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 4, 'Peter Parker', 'peter@gmail.com', 'jalan raya nginden semolo', 264, '031-89898989', NULL, '60111', NULL, '(+62)098-9089-0809', NULL, 0, NULL, NULL),
(3, 5, 'Barry Alan', 'barry@gmail.com', 'keputih', 264, '898-80980980', NULL, '60111', NULL, '(+62)808-0980-9808', NULL, 0, NULL, NULL),
(4, 6, 'Datamart Computer', 'dodyformello@gmail.com', 'Berbek Industri II/18', 242, '0318547652', NULL, '61256', NULL, '(+62)812-3278-760', NULL, 1, NULL, NULL),
(5, 7, 'Budi', 'budi@gmail.com', 'Jl. Undaan No.43', 264, NULL, NULL, NULL, NULL, '(+62)812-3043-4343', NULL, NULL, NULL, NULL),
(6, 2, 'Silvi Anita Putri', 'silvi.galerindo@gmail.com', 'Jl. Nginden Semolo No.42 Blok.B-20', 264, NULL, NULL, NULL, NULL, '(+62)8523-4628857', NULL, NULL, NULL, NULL),
(15, 8, 'Muhammad Riduwan', 'ridwan.yaa71@gmail.com', 'keputih Gg Makam Blok D-11, Sukolilo', 264, '', NULL, '60111', NULL, '(+62)857-3511-2973', NULL, 1, NULL, NULL),
(16, 10, 'Red', 'red_chm@yahoo.co.id', 'cek123', 12, '8089080980', NULL, '98089', NULL, '(+62)080-9890-8908', NULL, 0, NULL, NULL),
(17, 9, 'Dody Soegiarto', 'dody_formello@yahoo.com', 'Berbek Industri II/18', 264, '', NULL, '61256', NULL, '', NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tmst_perusahaan`
--

CREATE TABLE `tmst_perusahaan` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kota_id` int(11) DEFAULT NULL,
  `telp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kontak_person` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bidang_usaha` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jam_operasional` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expired` datetime DEFAULT NULL,
  `pos_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tmst_perusahaan`
--

INSERT INTO `tmst_perusahaan` (`id`, `nama`, `alamat`, `kota_id`, `telp`, `kontak_person`, `bidang_usaha`, `jam_operasional`, `email`, `expired`, `pos_id`, `created_at`, `updated_at`) VALUES
(1, 'Galerindo Teknologi', 'Hi Tech Mall Lt. Dasar Jl. Kusuma Bangsa D17-19\r\nJl. Kusuma Bangsa No.116-118 ', 264, '031-5348998', 'Vera Herawati', 'Dell Products & Parts', 'Mon - Fri [10:00 - 17:30] , Sat  [10:00 - 16:30]', 'admin@galerindoteknologi.com', NULL, 1, NULL, '2016-10-14 05:31:36');

-- --------------------------------------------------------

--
-- Table structure for table `tmst_produk`
--

CREATE TABLE `tmst_produk` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug_nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `satuan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `berat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `spesifikasi` longtext COLLATE utf8_unicode_ci,
  `deskripsi` longtext COLLATE utf8_unicode_ci,
  `harga_retail` bigint(20) DEFAULT NULL,
  `hpp` bigint(20) NOT NULL,
  `stok` int(11) DEFAULT NULL,
  `stok_dipesan` int(11) DEFAULT NULL,
  `is_aktif` int(11) DEFAULT NULL,
  `kategori_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tmst_produk`
--

INSERT INTO `tmst_produk` (`id`, `kode`, `nama`, `slug_nama`, `satuan`, `berat`, `spesifikasi`, `deskripsi`, `harga_retail`, `hpp`, `stok`, `stok_dipesan`, `is_aktif`, `kategori_id`, `created_at`, `updated_at`) VALUES
(1, 'INS3458I35005UMAUBT-B', 'DELL INSPIRON 3458 I3-5005 UMA UBT - BLACK', 'dell-inspiron-3458-i3-5005-uma-ubt-black', 'UNIT', '3', '', '<p>DELL INSPIRON 3458 I3-5005 UMA UBT - BLACK</p>', 5500000, 8872728, 8, 0, 1, 4, NULL, '2016-11-29 20:02:55'),
(2, 'INS3458I35005UMAUBT-R', 'DELL INSPIRON 3458 I3-5005 UMA UBT - RED', 'dell-inspiron-3458-i3-5005-uma-ubt-red', 'UNIT', '3', '', '<p>DELL INSPIRON 3458 I3-5005 UMA UBT -&nbsp;RED</p>', 5500000, 4436364, 10, 1, 1, 4, NULL, '2016-11-29 11:32:47'),
(3, 'INS3458I35005UMAW10-R', 'DELL INSPIRON 3458 I3-5005 UMA W10 - RED', 'dell-inspiron-3458-i3-5005-uma-w10-red', 'UNIT', '3', '', '<p>DELL INSPIRON 3458 I3-5005 UMA W10 -&nbsp;RED</p>', 5800000, 0, 0, 0, 1, 4, NULL, '2016-11-01 13:50:48'),
(4, 'INS3458I35005UMAW10-B', 'DELL INSPIRON 3458 I3-5005 UMA W10 - BLACK', 'dell-inspiron-3458-i3-5005-uma-w10-black', 'UNIT', '3', '', '<p>DELL INSPIRON 3458 I3-5005 UMA W10 - BLACK</p>', 5800000, 0, 0, 0, 1, 4, NULL, '2016-10-31 14:58:43'),
(5, 'INS3458I35005VGAUBT-B', 'DELL INSPIRON 3458 I3-5005 VGA UBT - BLACK', 'dell-inspiron-3458-i3-5005-vga-ubt-black', 'UNIT', '3', '', '<p>DELL INSPIRON 3458 I3-5005 VGA UBT&nbsp;- BLACK</p>', 6000000, 0, 0, 0, 1, 4, NULL, '2016-11-03 03:44:53'),
(6, 'INS3458I35005VGAUBT-R', 'DELL INSPIRON 3458 I3-5005 VGA UBT - RED', 'dell-inspiron-3458-i3-5005-vga-ubt-red', 'UNIT', '3', '', '<p>DELL INSPIRON 3458 I3-5005 VGA UBT&nbsp;- BLACK</p>', 6000000, 0, 0, 0, 1, 4, NULL, '2016-11-03 03:44:12'),
(7, 'XPS129250', 'Dell XPS 12 9250', 'dell-xps-12-9250', 'Pcs', '1', '', '<h3>Notebook 2-in-1 Berlayar 4K UHD Pertama di Dunia</h3>\r\n<p>Detail gambar super tajam dan kemudahan penggunaan adalah dua hal yang bisa Anda dapat dari Notebook Hybrid 2-in-1 <strong>Dell XPS 12 </strong>seri terbaru. Layar sentuh seluas 12.5 Inci dibuat dengan teknologi kenamaan UltraSharp&trade; 4K Ultra HD dari Dell yang dapat menyajikan resolusi gambar hingga 3840 x 2160 piksel. Sebanyak 8 juta piksel hadir memanjakan mata. Ketajaman layarnya setara dengan 4 kali layar Full HD. Tingkat kecerahan layar 400-nit membuatnya tetap nyaman dilihat di bawah pancaran sinar matahari. Layar Dell XPS 12 didukung teknologi IGZO IPS Panel sehingga tampilan warna dan gambar tetap konsisten saat dilihat dari 170 derajat. Kontras warna mencapai 1500:1, sehingga gambar gelap dan terang terlihat lebih hidup.</p>\r\n<p><img src="http://assets.bmdstatic.com/assets/Data/Inventory/Overview/image/DELL-XPS-12-2015.jpg" alt="" /><br /><em>Perbandingan ketajaman layar Full HD (kiri) dan UltraSharp&trade; 4K UHD (kanan)</em></p>\r\n<h3>Processor Khusus Notebook Hybrid</h3>\r\n<p>Dell XPS 12 ditenagai Processor Intel&reg; Core&trade; M generasi ke-6 yang ditanam dengan proses manufaktur 14nm. Processor ini dapat menghadirkan performa Mobile yang hebat, respon lebih cepat, dan efisiensi daya yang sangat luar biasa. Processor berbasis arsitektur Skylake ini dikemas dengan fitur keamanan internal yang berguna untuk menunjang produktivitas, kreativitas, serta fitur Multimedia untuk hiburan. Intel&reg; Core&trade; M hadir dalam varian M3, M5, dan M7. Ketiga seri Processor tersebut sudah mendukung fitur sistem operasi Windows&reg; 10. Intel&reg; Core&trade; M sudah didukung pengolah grafis internal Intel HD Graphics 515 (Gen 9) yang memiliki performa 40% lebih baik dibanding generasi sebelumnya. Intel&reg; Core&trade; M memiliki fitur keamanan dirancang untuk kalangan konsumen dan Enterprise.</p>\r\n<h3><img src="http://assets.bmdstatic.com/assets/Data/Inventory/Overview/image/DELL-XPS-12-2015-a.jpg" alt="" />Processor Dual-Core Super Irit Daya</h3>\r\n<p>Di dalam Dell XPS 12 terbaru terdapat Processor Dual-Core Intel&reg; Core&trade; M5-6Y57. Ini merupakan Processor super efisien daya yang berbasis arsitektur Skylake. Processor dirancang agar lebih kompatibel untuk Notebook 2-in-1 atau Notebook Hybrid tanpa kipas. Kecepatan Processor mencapai 1.1 - 2.8 GHz (2.4 saat 2 Core aktif). Konsumsi daya listriknya mencapai 4.5W dan bisa diturunkan hingga 3.5W (TDP-Down) atau dinaikan hingga 7W (TDP-Up).<br />&nbsp;</p>\r\n<h3>Processor yang Sepenuhnya Mobile</h3>\r\n<p>Dirancang untuk Notebook Hybrid atau 2-in-1 dengan konsumsi listrik super efisien.<br /><img src="http://assets.bmdstatic.com/assets/Data/Inventory/Overview/image/DELL-XPS-12-2015-b.jpg" alt="" /></p>\r\n<h3>Benchmark Intel&reg; Core&trade; M Skylake vs Broadwell</h3>\r\n<p>Kelebihan Skylake: Benchmark Geekbench Single Core 15% lebih baik.</p>\r\n<p><img src="http://assets.bmdstatic.com/assets/Data/Inventory/Overview/image/DELL-XPS-12-2015-c.jpg" alt="" /><br /><em>Intel&reg; Core&trade; M5-6Y57 (6th Gen) vs Intel&reg; Core&trade; M 5Y71 (5th Gen)</em><br />&nbsp;</p>\r\n<h3>2X Lebih Baik Dibanding Processor Dual-Core Standar</h3>\r\n<p>Intel&reg; Core&trade; M5-6Y57 terintegrasi dengan teknologi Hyper-Threading yang memungkinkan kedua buah Core dapat bekerja dua kali lebih banyak dibanding Dual-Core standar. Walau secara fisik hanya memiliki dua buah Core, Processor ini dapat menandingi performa Processor dengan empat buah Core. Teknologi fenomenal dari Intel&reg; yang sudah ada sejak tahun 2010 ini berikan solusi bagi Anda yang menginginkan Processor cepat namun dengan konsumsi daya yang hemat. Teknologi Hyper-Threading hadir di beberapa seri Intel&reg; Core&trade; dan Intel&reg; Core&trade; seri M.</p>\r\n<h3>Memperkenalkan Teknologi Hyper-Threading dari Intel&reg;</h3>\r\n<p><iframe src="http://www.youtube.com/embed/iNXM8OW5pi8?rel=0" width="560" height="315" frameborder="1" allowfullscreen="allowfullscreen" data-mce-fragment="1"></iframe><br />&nbsp;</p>\r\n<h3>Konektivitas Lengkap Berkecepatan Tinggi</h3>\r\n<p>Kinerja Processor hebat perlu dukungan perangkat keras yang mumpuni. Dell XPS 12 memiliki Storage berjenis SSD (Solid State Drive) 256 GB. Sementara kapasitas RAM 8 GB memastikan proses komputasi berjalan lancar dan responsif. Kombinasi SSD dan RAM besar membuat Anda dapat menggunakan aplikasi yang tergolong berat untuk kebutuhan olah File Multimedia. Untuk transfer data disediakan Port Thunderbolt&trade; 3 yang bisa digunakan untuk mengisi daya baterai, menghubungkan Notebook ke layar hingga resolusi 4K, dan transfer data hingga 40 Gbps atau 8 kali lebih cepat dibanding USB 3.0.</p>\r\n<p><img src="http://assets.bmdstatic.com/assets/Data/Inventory/Overview/image/DELL-XPS-12-2015-d.jpg" alt="" /></p>\r\n<h3>Notebook dan Tablet Jadi Satu</h3>\r\n<p>Ini adalah Tablet dalam bentuk Notebook, atau sebaliknya. Dell XPS 12 terbaru adalah Notebook 2-in-1 yang memiliki layar fleksibel. Layar bisa dilepas pasang dari Keyboard (Docking). Layar dan Keyboard dihubungkan dengan Port Magnetik sehingga mudah dilepas dengan satu tangan saja. Ukuran layar sangat ringan, Anda bahkan serasa menggunakan Tablet modern ketimbang sebuah Notebook yang berat. Gabungkan layar ke Keyboard dan gunakan dalam mode konvensional. Mode ini memudahkan Anda mengetik. Sementara itu mode Tablet menjadi solusi agar tetap bisa mengakses data penting selama bekerja di lapangan.</p>\r\n<h3><img src="http://assets.bmdstatic.com/assets/Data/Inventory/Overview/image/DELL-XPS-12-2015-e.jpg" alt="" /></h3>\r\n<h3>Ukuran Dell XPS 12</h3>\r\n<p>Notebook 2-in-1 tipis dan ringan dengan teknologi terbaik di kelasnya.<br /><img src="http://assets.bmdstatic.com/assets/Data/Inventory/Overview/image/DELL-XPS-12-2015-g.jpg" alt="" /></p>', 21500000, 12950000, 15, 0, 1, 4, NULL, '2016-11-30 07:51:29'),
(8, '', 'DELL All-in-One Inspiron 3459 (Core I5-6200U) - Black', 'dell-all-in-one-inspiron-3459-core-i5-6200u-black', 'Pcs', '1', '', '<h3>Hiburan dan Produktivitas untuk Keluarga di Rumah</h3>\r\n<p><strong>Dell Inspiron 24 seri 3000</strong> membawa semua keunggulan hiburan berkelas multimedia dan kinerja Multitasking handal yang dibutuhkan semua anggota keluarga Anda di rumah. Desktop AiO ini didukung layar lebar seluas 24 Inch dan resolusi Full HD. Kualitas layar yang luar biasa tajam dan cerah membuat Anda dan keluarga bisa menikmati tampilan video dan aplikasi yang lebih hidup. Dell Inspiron 24 3000 ditenagai Processor Intel&reg; Core&trade; i5 generasi ke-6 yang memiliki performa komputasi lebih canggih dan efisien dibanding generasi pendahulunya. Dengan memiliki PC AiO ini di rumah, Anda bisa menghemat lebih banyak tempat karena di dalam satu unit sudah ada layar, perangkat keras, Speaker, dan Webcam.</p>\r\n<p><img src="http://assets.bmdstatic.com/assets/Data/Inventory/Overview/image/DELL-Inspiron-3459.jpg" alt="" /><br /><em>Gambar sekedar ilustrasi. Aksesoris PC dijual terpisah</em></p>\r\n<h3>Pusat Hiburan di dalam Rumah</h3>\r\n<p>Hanya dengan membeli satu unit PC AiO Dell Inspiron 24 3000 Anda sudah bisa menghemat tempat dan uang karena tidak perlu membeli perangkat pendukung PC tambahan seperti layar monitor, Speaker, dan DVD RW. Dell Inspiron 24 3000 terintegrasi langsung dengan layar sebesar 23.8 Inch (24&rdquo;) dengan resolusi mencapai Full HD (1920 x 1080 piksel). Resolusinya dua kali lebih tajam dibanding layar HD standar atau WXGA, gambar apapun jadi terlihat lebih nyata di mata Anda. Layarnya sudah dibekali fungsi sensor sentuh sehingga memudahkan Anda mengeksplorasi aplikasi-aplikasi Multimedia menarik dari sistem operasi yang Anda gunakan.</p>\r\n<p><img src="http://assets.bmdstatic.com/assets/Data/Inventory/Overview/image/DELL-Inspiron-3459-a.jpg" alt="" /></p>\r\n<h3>Processor Dual-Core Hebat dan Efisien</h3>\r\n<p>Di dalam Dell Inspiron 24 3000 terdapat Processor Intel&reg; Core&trade; i5-6200U. Processor ini memiliki dua buah Core yang masing-masingnya memiliki kecepatan 2.3 GHz. Berkat teknologi Intel&reg; Turbo Boost kecepatan tiap Core bisa ditingkatkan hingga 2.8 GHz saat kinerja Processor sudah mencapai puncaknya. Intel&reg; Core&trade; i5-6200U membawa peningkatan kecepatan Clock Turbo dibanding Processor generasi sebelumnya (i5-4200U,22nm). Berkat teknologi fabrikasi 14nm terbaru, konsumsi daya listriknya jadi sangat efisien yakni hanya berkisar 15 Watt saja. Saat dibandingkan dengan i5-4200U, kinerja i5-6200U torehkan Score Benchmark yang lebih baik.</p>\r\n<p><img src="http://assets.bmdstatic.com/assets/Data/Inventory/Overview/image/DELL-Inspiron-3459-b.jpg" alt="" /><br /><em>Perbandingan spesifikasi Processor oleh <a href="http://www.cpu-world.com/">www.cpu-world.com</a><br />Perbandingan Score Benchmark oleh <a href="http://www.cpuboss.com/">www.cpuboss.com</a></em></p>\r\n<h3>Teknologi Fenomenal Intel&reg; Hyper-Threading</h3>\r\n<p>Intel&reg; Core&trade; i5-6200U didukung teknologi Hyper-Threading yang memungkinkan kedua buah Core-nya dapat bekerja dua kali lipat lebih banyak dibanding Processor Dual-Core standar. Dua buah Core fisik di dalam Processor mampu bekerja beriringan dengan dua buah Core virtual sehingga dapat menandingi performa Processor dengan empat Core. Teknologi fenomenal Intel&reg; yang sudah ada sejak 2010 ini sudah menjadi teknologi standar untuk beberapa seri Processor Intel&reg; Core&trade;. Intel&reg; Hyper-Threading juga membuat PC dapat menjalankan lebih banyak aplikasi dalam waktu bersamaan dengan lebih sedikit daya listrik.</p>\r\n<h3>Memperkenalkan Teknologi Hyper-Threading dari Intel&reg;</h3>\r\n<p><iframe src="http://www.youtube.com/embed/iNXM8OW5pi8?rel=0" width="560" height="315" frameborder="1" allowfullscreen="allowfullscreen" data-mce-fragment="1"></iframe><br />&nbsp;</p>\r\n<h3>Kapasitas Penyimpanan Besar dan Port Konektivitas Lengkap</h3>\r\n<p>Dell Inspiron 24 3000 menyediakan ruang penyimpanan yang besar untuk menyimpan semua koleksi File Multimedia keluarga Anda di rumah. Berbekal Storage HDD berkapasitas 1TB, Anda bisa menyimpan hingga ribuan foto, musik, video, atau digunakan untuk meng-Instal puluhan aplikasi. Desktop AiO ini juga dilengkapi dengan Slot DVD RW sehingga Anda bisa membuka ataupun mengakses data yang disimpan dalam keping DVD. Dell Inspiron 3000 pun menyediakan Port konektivitas yang lengkap seperti: 4-in-1 Media Card Reader, USB 3.0 dan 2.0, LAN, Microphone dan Audio Jack, serta Audio Line-Out. Sementara itu, konektivitas nirkabel yang tersedia adalah Wi-Fi 802.11ac dan Bluetooth.</p>\r\n<p><img src="http://assets.bmdstatic.com/assets/Data/Inventory/Overview/image/DELL-Inspiron-3459-c.jpg" alt="" /></p>', 12299000, 0, 0, 0, 1, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tmst_supplier`
--

CREATE TABLE `tmst_supplier` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alamat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kota_id` int(11) DEFAULT NULL,
  `telp1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telp2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama_sales` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hp_sales` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deskripsi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `umur_hutang` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_aktif` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tmst_supplier`
--

INSERT INTO `tmst_supplier` (`id`, `nama`, `alamat`, `kota_id`, `telp1`, `telp2`, `nama_sales`, `hp_sales`, `deskripsi`, `umur_hutang`, `is_aktif`, `created_at`, `updated_at`) VALUES
(1, 'PT. TIXPRO INFORMATIKA MEGAH', 'RUKO ORION DUSIT NO.19-20', 158, '021-30005040', '', 'Hadiyanto Tarsley', '(+62)8128-1726999', 'Dell Counsumer Notebook Distributor', '30', 1, NULL, NULL),
(2, 'PT. ADAKOM INTERNATIONAL TECHNOLOGY', 'Ruko Mangga Dua Square Blok H No 24 Jl.Gunung Sahari Raya No.1 Jakarta 14420', 159, '021-62317952', '021-62310458', 'Wahyoe Otnawamseor', '(+62)8211-4164770', 'Dell Consumer & Commercial Distributor', '30', 1, NULL, NULL),
(3, 'PT. SYNNEX METRODATA INDONESIA', 'Intiland Tower Lt.7 Suite 5A  Jl. Panglima Sudirman 101-103 Surabaya 60271', 264, '031-5474218_', '031-53471479', 'Emmy', '(+62)8214-3713233', 'Dell Consumer & Comercial Distributor', '30', 1, NULL, NULL),
(4, 'PT. SMARTINDO INTEGRASI SYSTEM', 'Jl. Mangga Dua Raya, Komplek Perkantoran Harco Mangga Dua Blok H No.37', 158, '0216125559__', '', 'Jimmy', '', 'Dell ', '', 1, NULL, NULL),
(5, 'Coba', 'Apa Tahunya', 231, '2323232323__', '23232323____', 'Bejo Suhendar', '(+62)812-3278-760_', 'percobaan', '20', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tmst_voucher`
--

CREATE TABLE `tmst_voucher` (
  `id` int(10) UNSIGNED NOT NULL,
  `kode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nominal` bigint(20) DEFAULT NULL,
  `awal_periode` datetime DEFAULT NULL,
  `akhir_periode` datetime DEFAULT NULL,
  `tanggal_dipakai` datetime DEFAULT NULL,
  `is_sekali_pakai` int(11) DEFAULT NULL,
  `is_aktif` int(11) DEFAULT NULL,
  `pelanggan_id_target` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pelanggan_id_pakai` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tmst_voucher`
--

INSERT INTO `tmst_voucher` (`id`, `kode`, `nominal`, `awal_periode`, `akhir_periode`, `tanggal_dipakai`, `is_sekali_pakai`, `is_aktif`, `pelanggan_id_target`, `pelanggan_id_pakai`, `created_at`, `updated_at`) VALUES
(1, '5SK5KM67E1TQ57VMTNBQJYSFMX9YRCXN', 50000, '2016-12-01 00:00:00', '2016-12-31 00:00:00', NULL, 1, 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tran_bayar_pembelian_detail`
--

CREATE TABLE `tran_bayar_pembelian_detail` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `bayar_pembelian_header_id` bigint(20) DEFAULT NULL,
  `metode_pembayaran_id` int(11) DEFAULT NULL,
  `nomor_pembayaran` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nominal` bigint(20) DEFAULT NULL,
  `bank` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_bayar_pembelian_header`
--

CREATE TABLE `tran_bayar_pembelian_header` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nota_id` bigint(20) DEFAULT NULL,
  `jenis` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `total_tagihan` bigint(20) DEFAULT NULL,
  `total_pembayaran` bigint(20) DEFAULT NULL,
  `keterangan` text COLLATE utf8_unicode_ci,
  `no_jurnal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_bayar_penjualan_detail`
--

CREATE TABLE `tran_bayar_penjualan_detail` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `bayar_penjualan_header_id` bigint(20) DEFAULT NULL,
  `metode_pembayaran_id` int(11) DEFAULT NULL,
  `nomor_pembayaran` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nominal` bigint(20) DEFAULT NULL,
  `bank` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_bayar_penjualan_header`
--

CREATE TABLE `tran_bayar_penjualan_header` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nota_id` bigint(20) DEFAULT NULL,
  `jenis` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pelanggan_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `total_tagihan` bigint(20) DEFAULT NULL,
  `total_pembayaran` bigint(20) DEFAULT NULL,
  `keterangan` text COLLATE utf8_unicode_ci,
  `no_jurnal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_detail_stok_opname`
--

CREATE TABLE `tran_detail_stok_opname` (
  `id` int(10) UNSIGNED NOT NULL,
  `stok_opname_id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stok` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_hadiah_gudang`
--

CREATE TABLE `tran_hadiah_gudang` (
  `hadiah_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `stok` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tran_hadiah_gudang`
--

INSERT INTO `tran_hadiah_gudang` (`hadiah_id`, `gudang_id`, `stok`, `created_at`, `updated_at`, `id`) VALUES
(1, 1, 0, NULL, NULL, 1),
(2, 1, 0, NULL, NULL, 2),
(3, 1, 0, NULL, NULL, 3);

-- --------------------------------------------------------

--
-- Table structure for table `tran_hadiah_supplier`
--

CREATE TABLE `tran_hadiah_supplier` (
  `hadiah_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `harga_terakhir` bigint(20) DEFAULT NULL,
  `tanggal_terakhir` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_jurnal_umum`
--

CREATE TABLE `tran_jurnal_umum` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `no_jurnal` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tanggal` datetime NOT NULL,
  `akun_kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `akun_nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `referensi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `debit` bigint(20) NOT NULL,
  `kredit` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_jurnal_umum_detail`
--

CREATE TABLE `tran_jurnal_umum_detail` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `jurnal_umum_header_id` bigint(20) NOT NULL,
  `no_jurnal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `akun_kode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `akun_nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `referensi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `debit` bigint(20) DEFAULT NULL,
  `kredit` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_jurnal_umum_header`
--

CREATE TABLE `tran_jurnal_umum_header` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tanggal` datetime DEFAULT NULL,
  `is_editable` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_kartu_stok_detail`
--

CREATE TABLE `tran_kartu_stok_detail` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kartu_stok_header_id` int(11) NOT NULL,
  `sumber_data_id` int(11) DEFAULT NULL,
  `supplier_or_pelanggan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_nota` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `harga` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tran_kartu_stok_detail`
--

INSERT INTO `tran_kartu_stok_detail` (`id`, `kartu_stok_header_id`, `sumber_data_id`, `supplier_or_pelanggan`, `no_nota`, `serial_number`, `tanggal`, `jumlah`, `harga`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SO-201610-00005', '1234567', '2016-10-19 15:12:59', 1, 5200000, '2016-10-28 14:00:59', '2016-10-28 14:00:59'),
(2, 1, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SO-201610-00005', '456789', '2016-10-19 15:12:59', 4, 5200000, '2016-10-28 14:00:59', '2016-10-28 14:00:59'),
(3, 1, 2, 'Pelanggan : Datamart Computer', 'PL-201610-000010', '456789', '2016-10-19 15:40:25', 1, 5225000, '2016-10-28 14:00:59', '2016-10-28 14:00:59'),
(4, 2, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SO-201610-00005', '1234567', '2016-10-19 15:12:59', 1, 5200000, '2016-10-28 14:01:05', '2016-10-28 14:01:05'),
(5, 2, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SO-201610-00005', '456789', '2016-10-19 15:12:59', 4, 5200000, '2016-10-28 14:01:05', '2016-10-28 14:01:05'),
(6, 2, 2, 'Pelanggan : Datamart Computer', 'PL-201610-000010', '456789', '2016-10-19 15:40:25', 1, 5225000, '2016-10-28 14:01:05', '2016-10-28 14:01:05'),
(7, 3, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SO-201610-00005', '1234567', '2016-10-19 15:12:59', 1, 5200000, '2016-10-28 14:01:15', '2016-10-28 14:01:15'),
(8, 3, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SO-201610-00005', '456789', '2016-10-19 15:12:59', 4, 5200000, '2016-10-28 14:01:15', '2016-10-28 14:01:15'),
(9, 3, 2, 'Pelanggan : Datamart Computer', 'PL-201610-000010', '456789', '2016-10-19 15:40:25', 1, 5225000, '2016-10-28 14:01:15', '2016-10-28 14:01:15'),
(10, 4, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SO-201610-00005', '1234567', '2016-10-19 15:12:59', 1, 5200000, '2016-10-28 14:04:23', '2016-10-28 14:04:23'),
(11, 4, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SO-201610-00005', '456789', '2016-10-19 15:12:59', 4, 5200000, '2016-10-28 14:04:23', '2016-10-28 14:04:23'),
(12, 4, 2, 'Pelanggan : Datamart Computer', 'PL-201610-000010', '456789', '2016-10-19 15:40:25', 1, 5225000, '2016-10-28 14:04:23', '2016-10-28 14:04:23'),
(13, 5, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SO-201610-00005', 'ABCD1234', '2016-10-19 15:12:59', 4, 5210000, '2016-10-28 14:05:16', '2016-10-28 14:05:16'),
(14, 5, 2, 'Pelanggan : Datamart Computer', 'PL-201610-000010', 'ABCD1234', '2016-10-19 15:40:25', 1, 5225000, '2016-10-28 14:05:16', '2016-10-28 14:05:16'),
(15, 6, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SO-201610-00005', '1234567', '2016-10-19 15:12:59', 1, 5200000, '2016-10-28 14:07:52', '2016-10-28 14:07:52'),
(16, 6, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SO-201610-00005', '456789', '2016-10-19 15:12:59', 4, 5200000, '2016-10-28 14:07:52', '2016-10-28 14:07:52'),
(17, 6, 2, 'Pelanggan : Datamart Computer', 'PL-201610-000010', '456789', '2016-10-19 15:40:25', 1, 5225000, '2016-10-28 14:07:52', '2016-10-28 14:07:52'),
(18, 8, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SO-201610-00005', '1234567', '2016-10-19 15:12:59', 1, 5200000, '2016-10-28 14:28:25', '2016-10-28 14:28:25'),
(19, 8, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SO-201610-00005', '456789', '2016-10-19 15:12:59', 4, 5200000, '2016-10-28 14:28:25', '2016-10-28 14:28:25'),
(20, 8, 2, 'Pelanggan : Datamart Computer', 'PL-201610-000010', '456789', '2016-10-19 15:40:25', 1, 5225000, '2016-10-28 14:28:25', '2016-10-28 14:28:25'),
(21, 9, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SO-201610-00005', '1234567', '2016-10-19 15:12:59', 1, 5200000, '2016-10-28 14:29:45', '2016-10-28 14:29:45'),
(22, 9, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SO-201610-00005', '456789', '2016-10-19 15:12:59', 4, 5200000, '2016-10-28 14:29:45', '2016-10-28 14:29:45'),
(23, 9, 2, 'Pelanggan : Datamart Computer', 'PL-201610-000010', '456789', '2016-10-19 15:40:25', 1, 5225000, '2016-10-28 14:29:45', '2016-10-28 14:29:45'),
(24, 10, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SO-201610-00005', '1234567', '2016-10-19 15:12:59', 1, 5200000, '2016-10-28 14:31:23', '2016-10-28 14:31:23'),
(25, 10, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SO-201610-00005', '456789', '2016-10-19 15:12:59', 4, 5200000, '2016-10-28 14:31:23', '2016-10-28 14:31:23'),
(26, 10, 2, 'Pelanggan : Datamart Computer', 'PL-201610-000010', '456789', '2016-10-19 15:40:25', 1, 5225000, '2016-10-28 14:31:23', '2016-10-28 14:31:23'),
(27, 11, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SO-201610-00005', '1234567', '2016-10-19 15:12:59', 1, 5200000, '2016-10-28 14:32:55', '2016-10-28 14:32:55'),
(28, 11, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SO-201610-00005', '456789', '2016-10-19 15:12:59', 4, 5200000, '2016-10-28 14:32:55', '2016-10-28 14:32:55'),
(29, 11, 2, 'Pelanggan : Datamart Computer', 'PL-201610-000010', '456789', '2016-10-19 15:40:25', 1, 5225000, '2016-10-28 14:32:55', '2016-10-28 14:32:55'),
(30, 12, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SO-201610-00005', '1234567', '2016-10-19 15:12:59', 1, 5200000, '2016-10-28 14:34:29', '2016-10-28 14:34:29'),
(31, 12, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SO-201610-00005', '456789', '2016-10-19 15:12:59', 4, 5200000, '2016-10-28 14:34:29', '2016-10-28 14:34:29'),
(32, 12, 2, 'Pelanggan : Datamart Computer', 'PL-201610-000010', '456789', '2016-10-19 15:40:25', 1, 5225000, '2016-10-28 14:34:29', '2016-10-28 14:34:29'),
(33, 13, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SO-201610-00005', '1234567', '2016-10-19 15:12:59', 1, 5200000, '2016-10-28 14:39:37', '2016-10-28 14:39:37'),
(34, 13, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SO-201610-00005', '456789', '2016-10-19 15:12:59', 4, 5200000, '2016-10-28 14:39:37', '2016-10-28 14:39:37'),
(35, 13, 2, 'Pelanggan : Datamart Computer', 'PL-201610-000010', '456789', '2016-10-19 15:40:25', 1, 5225000, '2016-10-28 14:39:37', '2016-10-28 14:39:37'),
(36, 14, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SO-201610-00005', '1234567', '2016-10-19 15:12:59', 1, 5200000, '2016-10-28 14:42:48', '2016-10-28 14:42:48'),
(37, 14, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SO-201610-00005', '456789', '2016-10-19 15:12:59', 4, 5200000, '2016-10-28 14:42:48', '2016-10-28 14:42:48'),
(38, 14, 2, 'Pelanggan : Datamart Computer', 'PL-201610-000010', '456789', '2016-10-19 15:40:25', 1, 5225000, '2016-10-28 14:42:48', '2016-10-28 14:42:48'),
(39, 15, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SO-201610-00005', '1234567', '2016-10-19 15:12:59', 1, 5200000, '2016-10-28 14:46:13', '2016-10-28 14:46:13'),
(40, 15, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SO-201610-00005', '456789', '2016-10-19 15:12:59', 4, 5200000, '2016-10-28 14:46:13', '2016-10-28 14:46:13'),
(41, 16, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SO-201610-00005', '1234567', '2016-10-19 15:12:59', 1, 5200000, '2016-10-28 14:50:39', '2016-10-28 14:50:39'),
(42, 16, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SO-201610-00005', '456789', '2016-10-19 15:12:59', 4, 5200000, '2016-10-28 14:50:39', '2016-10-28 14:50:39'),
(43, 16, 2, 'Pelanggan : Datamart Computer', 'PL-201610-000010', '456789', '2016-10-19 15:40:25', 1, 5225000, '2016-10-28 14:50:39', '2016-10-28 14:50:39'),
(44, 16, 2, 'Pelanggan : Datamart Computer', 'PL-201610-00017', '1234567', '2016-10-28 20:39:37', 1, 5225000, '2016-10-28 14:50:39', '2016-10-28 14:50:39'),
(45, 17, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SO-201610-00005', '1234567', '2016-10-19 15:12:59', 1, 5200000, '2016-10-28 14:59:08', '2016-10-28 14:59:08'),
(46, 17, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SO-201610-00005', '456789', '2016-10-19 15:12:59', 4, 5200000, '2016-10-28 14:59:08', '2016-10-28 14:59:08'),
(47, 17, 2, 'Pelanggan : Datamart Computer', 'PL-201610-000010', '456789', '2016-10-19 15:40:25', 1, 5225000, '2016-10-28 14:59:08', '2016-10-28 14:59:08'),
(48, 17, 2, 'Pelanggan : Datamart Computer', 'PL-201610-00017', '1234567', '2016-10-28 20:39:37', 1, 5225000, '2016-10-28 14:59:08', '2016-10-28 14:59:08'),
(49, 18, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SO-201610-00005', '1234567', '2016-10-19 15:12:59', 1, 5200000, '2016-10-28 15:12:47', '2016-10-28 15:12:47'),
(50, 18, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SO-201610-00005', '456789', '2016-10-19 15:12:59', 4, 5200000, '2016-10-28 15:12:47', '2016-10-28 15:12:47'),
(51, 19, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SO-201610-00005', '1234567', '2016-10-19 15:12:59', 1, 5200000, '2016-10-28 15:13:04', '2016-10-28 15:13:04'),
(52, 19, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SO-201610-00005', '456789', '2016-10-19 15:12:59', 4, 5200000, '2016-10-28 15:13:04', '2016-10-28 15:13:04'),
(53, 20, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SO-201610-00005', 'ABCDEF', '2016-10-19 15:12:59', 2, 5210000, '2016-10-28 15:13:14', '2016-10-28 15:13:14'),
(54, 20, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SO-201610-00005', 'DEFGHI', '2016-10-19 15:12:59', 4, 5210000, '2016-10-28 15:13:14', '2016-10-28 15:13:14'),
(55, 21, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SO-201610-00005', '1234567', '2016-10-19 15:12:59', 1, 5200000, '2016-10-28 15:13:46', '2016-10-28 15:13:46'),
(56, 21, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SO-201610-00005', '456789', '2016-10-19 15:12:59', 4, 5200000, '2016-10-28 15:13:46', '2016-10-28 15:13:46'),
(57, 22, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SO-201610-00005', '1234567', '2016-10-19 15:12:59', 1, 5200000, '2016-10-28 15:15:26', '2016-10-28 15:15:26'),
(58, 22, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SO-201610-00005', '456789', '2016-10-19 15:12:59', 4, 5200000, '2016-10-28 15:15:26', '2016-10-28 15:15:26'),
(59, 23, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SO-201610-00005', '1234567', '2016-10-19 15:12:59', 1, 5200000, '2016-10-28 15:19:12', '2016-10-28 15:19:12'),
(60, 23, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SO-201610-00005', '456789', '2016-10-19 15:12:59', 4, 5200000, '2016-10-28 15:19:12', '2016-10-28 15:19:12'),
(61, 23, 2, 'Pelanggan : Datamart Computer', 'PL-201610-000010', '456789', '2016-10-19 15:40:25', 1, 5225000, '2016-10-28 15:19:12', '2016-10-28 15:19:12'),
(62, 23, 2, 'Pelanggan : Datamart Computer', 'PL-201610-00017', '1234567', '2016-10-28 20:39:37', 1, 5225000, '2016-10-28 15:19:12', '2016-10-28 15:19:12'),
(63, 24, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLBK0001', '2016-11-21 00:00:00', 1, 4000000, '2016-11-29 04:43:05', '2016-11-29 04:43:05'),
(64, 24, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLBK0002', '2016-11-21 00:00:00', 1, 4000000, '2016-11-29 04:43:05', '2016-11-29 04:43:05'),
(65, 24, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLBK0003', '2016-11-21 00:00:00', 1, 4000000, '2016-11-29 04:43:05', '2016-11-29 04:43:05'),
(66, 24, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLBK0004', '2016-11-21 00:00:00', 1, 4000000, '2016-11-29 04:43:05', '2016-11-29 04:43:05'),
(67, 24, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLBK0005', '2016-11-21 00:00:00', 1, 4000000, '2016-11-29 04:43:05', '2016-11-29 04:43:05'),
(68, 24, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLBK0006', '2016-11-21 00:00:00', 1, 4000000, '2016-11-29 04:43:05', '2016-11-29 04:43:05'),
(69, 24, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLBK0007', '2016-11-21 00:00:00', 1, 4000000, '2016-11-29 04:43:05', '2016-11-29 04:43:05'),
(70, 24, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLBK0008', '2016-11-21 00:00:00', 1, 4000000, '2016-11-29 04:43:05', '2016-11-29 04:43:05'),
(71, 24, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLBK0009', '2016-11-21 00:00:00', 1, 4000000, '2016-11-29 04:43:05', '2016-11-29 04:43:05'),
(72, 24, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLBK0010', '2016-11-21 00:00:00', 1, 4000000, '2016-11-29 04:43:05', '2016-11-29 04:43:05'),
(73, 24, 2, 'Pelanggan : Muhammad Riduwan', 'PL-201611-00001', 'DLBK0001', '2016-11-21 00:00:00', 1, 5225000, '2016-11-29 04:43:05', '2016-11-29 04:43:05'),
(74, 24, 2, 'Pelanggan : Muhammad Riduwan', 'PL-201611-00001', 'DLBK0002', '2016-11-21 00:00:00', 1, 5225000, '2016-11-29 04:43:05', '2016-11-29 04:43:05'),
(75, 24, 2, 'Pelanggan : Muhammad Riduwan', 'PL-201611-00001', 'DLBK0003', '2016-11-21 00:00:00', 1, 5225000, '2016-11-29 04:43:05', '2016-11-29 04:43:05'),
(76, 24, 2, 'Pelanggan : Muhammad Riduwan', 'PL-201611-00001', 'DLBK0004', '2016-11-21 00:00:00', 1, 5225000, '2016-11-29 04:43:05', '2016-11-29 04:43:05'),
(77, 25, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00003', 'XX-TOKO-BLACK', '2016-11-29 00:00:00', 3, 4800000, '2016-11-29 04:43:31', '2016-11-29 04:43:31'),
(78, 25, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00004', 'XX-KANTOR-BLACK', '2016-11-29 00:00:00', 2, 4800000, '2016-11-29 04:43:31', '2016-11-29 04:43:31'),
(79, 25, 4, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', NULL, 'XX-TOKO-BLACK', '2016-11-29 11:32:47', 1, 4800000, '2016-11-29 04:43:31', '2016-11-29 04:43:31'),
(80, 26, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00003', 'XX-TOKO-RED', '2016-11-29 00:00:00', 1, 4800000, '2016-11-29 04:44:00', '2016-11-29 04:44:00'),
(81, 26, 4, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', NULL, 'XX-TOKO-RED', '2016-11-29 11:32:47', 1, 4800000, '2016-11-29 04:44:00', '2016-11-29 04:44:00'),
(82, 27, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLRD0001', '2016-11-21 00:00:00', 1, 4000000, '2016-11-29 04:44:05', '2016-11-29 04:44:05'),
(83, 27, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLRD0002', '2016-11-21 00:00:00', 1, 4000000, '2016-11-29 04:44:05', '2016-11-29 04:44:05'),
(84, 27, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLRD0003', '2016-11-21 00:00:00', 1, 4000000, '2016-11-29 04:44:05', '2016-11-29 04:44:05'),
(85, 27, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLRD0004', '2016-11-21 00:00:00', 1, 4000000, '2016-11-29 04:44:05', '2016-11-29 04:44:05'),
(86, 27, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLRD0005', '2016-11-21 00:00:00', 1, 4000000, '2016-11-29 04:44:05', '2016-11-29 04:44:05'),
(87, 27, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLRD0006', '2016-11-21 00:00:00', 1, 4000000, '2016-11-29 04:44:05', '2016-11-29 04:44:05'),
(88, 27, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLRD0007', '2016-11-21 00:00:00', 1, 4000000, '2016-11-29 04:44:05', '2016-11-29 04:44:05'),
(89, 27, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLRD0008', '2016-11-21 00:00:00', 1, 4000000, '2016-11-29 04:44:05', '2016-11-29 04:44:05'),
(90, 27, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLRD0009', '2016-11-21 00:00:00', 1, 4000000, '2016-11-29 04:44:05', '2016-11-29 04:44:05'),
(91, 27, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLRD0010', '2016-11-21 00:00:00', 1, 4000000, '2016-11-29 04:44:05', '2016-11-29 04:44:05'),
(92, 28, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00003', 'XX-TOKO-RED', '2016-11-29 00:00:00', 1, 4800000, '2016-11-29 05:42:33', '2016-11-29 05:42:33'),
(93, 28, 4, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', NULL, 'XX-TOKO-RED', '2016-11-29 11:32:47', 1, 4800000, '2016-11-29 05:42:33', '2016-11-29 05:42:33'),
(94, 31, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLBK0001', '2016-11-21 00:00:00', 1, 4000000, '2016-11-29 09:13:43', '2016-11-29 09:13:43'),
(95, 31, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLBK0002', '2016-11-21 00:00:00', 1, 4000000, '2016-11-29 09:13:43', '2016-11-29 09:13:43'),
(96, 31, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLBK0003', '2016-11-21 00:00:00', 1, 4000000, '2016-11-29 09:13:43', '2016-11-29 09:13:43'),
(97, 31, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLBK0004', '2016-11-21 00:00:00', 1, 4000000, '2016-11-29 09:13:43', '2016-11-29 09:13:43'),
(98, 31, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLBK0005', '2016-11-21 00:00:00', 1, 4000000, '2016-11-29 09:13:43', '2016-11-29 09:13:43'),
(99, 31, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLBK0006', '2016-11-21 00:00:00', 1, 4000000, '2016-11-29 09:13:43', '2016-11-29 09:13:43'),
(100, 31, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLBK0007', '2016-11-21 00:00:00', 1, 4000000, '2016-11-29 09:13:43', '2016-11-29 09:13:43'),
(101, 31, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLBK0008', '2016-11-21 00:00:00', 1, 4000000, '2016-11-29 09:13:43', '2016-11-29 09:13:43'),
(102, 31, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLBK0009', '2016-11-21 00:00:00', 1, 4000000, '2016-11-29 09:13:43', '2016-11-29 09:13:43'),
(103, 31, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLBK0010', '2016-11-21 00:00:00', 1, 4000000, '2016-11-29 09:13:43', '2016-11-29 09:13:43'),
(104, 31, 2, 'Pelanggan : Muhammad Riduwan', 'PL-201611-00001', 'DLBK0001', '2016-11-21 00:00:00', 1, 5225000, '2016-11-29 09:13:43', '2016-11-29 09:13:43'),
(105, 31, 2, 'Pelanggan : Muhammad Riduwan', 'PL-201611-00001', 'DLBK0002', '2016-11-21 00:00:00', 1, 5225000, '2016-11-29 09:13:43', '2016-11-29 09:13:43'),
(106, 31, 2, 'Pelanggan : Muhammad Riduwan', 'PL-201611-00001', 'DLBK0003', '2016-11-21 00:00:00', 1, 5225000, '2016-11-29 09:13:43', '2016-11-29 09:13:43'),
(107, 31, 2, 'Pelanggan : Muhammad Riduwan', 'PL-201611-00001', 'DLBK0004', '2016-11-21 00:00:00', 1, 5225000, '2016-11-29 09:13:43', '2016-11-29 09:13:43'),
(108, 32, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLBK0001', '2016-11-21 21:42:25', 1, 4000000, '2016-11-29 09:45:14', '2016-11-29 09:45:14'),
(109, 32, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLBK0002', '2016-11-21 21:42:25', 1, 4000000, '2016-11-29 09:45:14', '2016-11-29 09:45:14'),
(110, 32, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLBK0003', '2016-11-21 21:42:25', 1, 4000000, '2016-11-29 09:45:14', '2016-11-29 09:45:14'),
(111, 32, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLBK0004', '2016-11-21 21:42:25', 1, 4000000, '2016-11-29 09:45:14', '2016-11-29 09:45:14'),
(112, 32, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLBK0005', '2016-11-21 21:42:25', 1, 4000000, '2016-11-29 09:45:14', '2016-11-29 09:45:14'),
(113, 32, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLBK0006', '2016-11-21 21:42:25', 1, 4000000, '2016-11-29 09:45:14', '2016-11-29 09:45:14'),
(114, 32, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLBK0007', '2016-11-21 21:42:25', 1, 4000000, '2016-11-29 09:45:14', '2016-11-29 09:45:14'),
(115, 32, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLBK0008', '2016-11-21 21:42:25', 1, 4000000, '2016-11-29 09:45:14', '2016-11-29 09:45:14'),
(116, 32, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLBK0009', '2016-11-21 21:42:25', 1, 4000000, '2016-11-29 09:45:14', '2016-11-29 09:45:14'),
(117, 32, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLBK0010', '2016-11-21 21:42:25', 1, 4000000, '2016-11-29 09:45:14', '2016-11-29 09:45:14'),
(118, 32, 2, 'Pelanggan : Muhammad Riduwan', 'PL-201611-00001', 'DLBK0001', '2016-11-21 21:45:13', 1, 5225000, '2016-11-29 09:45:14', '2016-11-29 09:45:14'),
(119, 32, 2, 'Pelanggan : Muhammad Riduwan', 'PL-201611-00001', 'DLBK0002', '2016-11-21 21:45:13', 1, 5225000, '2016-11-29 09:45:14', '2016-11-29 09:45:14'),
(120, 32, 2, 'Pelanggan : Muhammad Riduwan', 'PL-201611-00001', 'DLBK0003', '2016-11-21 21:45:13', 1, 5225000, '2016-11-29 09:45:14', '2016-11-29 09:45:14'),
(121, 32, 2, 'Pelanggan : Muhammad Riduwan', 'PL-201611-00001', 'DLBK0004', '2016-11-21 21:45:13', 1, 5225000, '2016-11-29 09:45:14', '2016-11-29 09:45:14'),
(122, 33, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00003', 'XX-TOKO-BLACK', '2016-11-29 11:21:36', 3, 4800000, '2016-11-29 09:45:21', '2016-11-29 09:45:21'),
(123, 33, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00004', 'XX-KANTOR-BLACK', '2016-11-29 11:25:34', 2, 4800000, '2016-11-29 09:45:21', '2016-11-29 09:45:21'),
(124, 33, 4, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', NULL, 'XX-TOKO-BLACK', '2016-11-29 11:32:47', 1, 4800000, '2016-11-29 09:45:21', '2016-11-29 09:45:21'),
(125, 34, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00003', 'XX-TOKO-BLACK', '2016-11-29 11:21:36', 3, 4800000, '2016-11-29 09:45:25', '2016-11-29 09:45:25'),
(126, 34, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00004', 'XX-KANTOR-BLACK', '2016-11-29 11:25:34', 2, 4800000, '2016-11-29 09:45:25', '2016-11-29 09:45:25'),
(127, 34, 4, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', NULL, 'XX-TOKO-BLACK', '2016-11-29 11:32:47', 1, 4800000, '2016-11-29 09:45:25', '2016-11-29 09:45:25'),
(128, 35, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00003', 'XX-TOKO-BLACK', '2016-11-29 11:21:36', 3, 4800000, '2016-11-29 09:45:45', '2016-11-29 09:45:45'),
(129, 35, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00004', 'XX-KANTOR-BLACK', '2016-11-29 11:25:34', 2, 4800000, '2016-11-29 09:45:45', '2016-11-29 09:45:45'),
(130, 35, 4, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', NULL, 'XX-TOKO-BLACK', '2016-11-29 11:32:47', 1, 4800000, '2016-11-29 09:45:45', '2016-11-29 09:45:45'),
(131, 36, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLBK0001', '2016-11-21 21:42:25', 1, 4000000, '2016-11-29 09:45:54', '2016-11-29 09:45:54'),
(132, 36, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLBK0002', '2016-11-21 21:42:25', 1, 4000000, '2016-11-29 09:45:54', '2016-11-29 09:45:54'),
(133, 36, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLBK0003', '2016-11-21 21:42:25', 1, 4000000, '2016-11-29 09:45:54', '2016-11-29 09:45:54'),
(134, 36, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLBK0004', '2016-11-21 21:42:25', 1, 4000000, '2016-11-29 09:45:54', '2016-11-29 09:45:54'),
(135, 36, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLBK0005', '2016-11-21 21:42:25', 1, 4000000, '2016-11-29 09:45:54', '2016-11-29 09:45:54'),
(136, 36, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLBK0006', '2016-11-21 21:42:25', 1, 4000000, '2016-11-29 09:45:54', '2016-11-29 09:45:54'),
(137, 36, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLBK0007', '2016-11-21 21:42:25', 1, 4000000, '2016-11-29 09:45:54', '2016-11-29 09:45:54'),
(138, 36, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLBK0008', '2016-11-21 21:42:25', 1, 4000000, '2016-11-29 09:45:54', '2016-11-29 09:45:54'),
(139, 36, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLBK0009', '2016-11-21 21:42:25', 1, 4000000, '2016-11-29 09:45:54', '2016-11-29 09:45:54'),
(140, 36, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLBK0010', '2016-11-21 21:42:25', 1, 4000000, '2016-11-29 09:45:54', '2016-11-29 09:45:54'),
(141, 36, 2, 'Pelanggan : Muhammad Riduwan', 'PL-201611-00001', 'DLBK0001', '2016-11-21 21:45:13', 1, 5225000, '2016-11-29 09:45:54', '2016-11-29 09:45:54'),
(142, 36, 2, 'Pelanggan : Muhammad Riduwan', 'PL-201611-00001', 'DLBK0002', '2016-11-21 21:45:13', 1, 5225000, '2016-11-29 09:45:54', '2016-11-29 09:45:54'),
(143, 36, 2, 'Pelanggan : Muhammad Riduwan', 'PL-201611-00001', 'DLBK0003', '2016-11-21 21:45:13', 1, 5225000, '2016-11-29 09:45:54', '2016-11-29 09:45:54'),
(144, 36, 2, 'Pelanggan : Muhammad Riduwan', 'PL-201611-00001', 'DLBK0004', '2016-11-21 21:45:13', 1, 5225000, '2016-11-29 09:45:54', '2016-11-29 09:45:54'),
(145, 37, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLRD0001', '2016-11-21 21:42:25', 1, 4000000, '2016-11-29 09:52:52', '2016-11-29 09:52:52'),
(146, 37, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLRD0002', '2016-11-21 21:42:25', 1, 4000000, '2016-11-29 09:52:52', '2016-11-29 09:52:52'),
(147, 37, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLRD0003', '2016-11-21 21:42:25', 1, 4000000, '2016-11-29 09:52:52', '2016-11-29 09:52:52'),
(148, 37, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLRD0004', '2016-11-21 21:42:25', 1, 4000000, '2016-11-29 09:52:52', '2016-11-29 09:52:52'),
(149, 37, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLRD0005', '2016-11-21 21:42:25', 1, 4000000, '2016-11-29 09:52:52', '2016-11-29 09:52:52'),
(150, 37, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLRD0006', '2016-11-21 21:42:25', 1, 4000000, '2016-11-29 09:52:52', '2016-11-29 09:52:52'),
(151, 37, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLRD0007', '2016-11-21 21:42:25', 1, 4000000, '2016-11-29 09:52:52', '2016-11-29 09:52:52'),
(152, 37, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLRD0008', '2016-11-21 21:42:25', 1, 4000000, '2016-11-29 09:52:52', '2016-11-29 09:52:52'),
(153, 37, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLRD0009', '2016-11-21 21:42:25', 1, 4000000, '2016-11-29 09:52:52', '2016-11-29 09:52:52'),
(154, 37, 1, 'Supplier : PT. TIXPRO INFORMATIKA MEGAH', 'SJM-201611-00001', 'DLRD0010', '2016-11-21 21:42:25', 1, 4000000, '2016-11-29 09:52:52', '2016-11-29 09:52:52');

-- --------------------------------------------------------

--
-- Table structure for table `tran_kartu_stok_header`
--

CREATE TABLE `tran_kartu_stok_header` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `gudang_id` int(11) DEFAULT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `tanggal_awal` datetime DEFAULT NULL,
  `tanggal_akhir` datetime DEFAULT NULL,
  `total_stok` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tran_kartu_stok_header`
--

INSERT INTO `tran_kartu_stok_header` (`id`, `gudang_id`, `produk_id`, `tanggal_awal`, `tanggal_akhir`, `total_stok`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2016-10-01 00:00:00', '2016-10-28 00:00:00', NULL, '2016-10-28 14:00:59', '2016-10-28 14:00:59'),
(2, 1, 1, '2016-10-01 00:00:00', '2016-10-28 00:00:00', NULL, '2016-10-28 14:01:05', '2016-10-28 14:01:05'),
(3, 1, 1, '2016-10-01 00:00:00', '2016-10-28 00:00:00', NULL, '2016-10-28 14:01:15', '2016-10-28 14:01:15'),
(4, 1, 1, '2016-10-01 00:00:00', '2016-10-28 00:00:00', NULL, '2016-10-28 14:04:23', '2016-10-28 14:04:23'),
(5, 2, 2, '2016-10-01 00:00:00', '2016-10-28 00:00:00', NULL, '2016-10-28 14:05:16', '2016-10-28 14:05:16'),
(6, 1, 1, '2016-10-01 00:00:00', '2016-10-28 00:00:00', NULL, '2016-10-28 14:07:52', '2016-10-28 14:07:52'),
(7, 1, 1, '2016-10-01 00:00:00', '2016-10-28 00:00:00', NULL, '2016-10-28 14:12:13', '2016-10-28 14:12:13'),
(8, 1, 1, '2016-10-01 00:00:00', '2016-10-28 00:00:00', NULL, '2016-10-28 14:28:25', '2016-10-28 14:28:25'),
(9, 1, 1, '2016-10-01 00:00:00', '2016-10-28 00:00:00', NULL, '2016-10-28 14:29:45', '2016-10-28 14:29:45'),
(10, 1, 1, '2016-10-01 00:00:00', '2016-10-28 00:00:00', NULL, '2016-10-28 14:31:23', '2016-10-28 14:31:23'),
(11, 1, 1, '2016-10-01 00:00:00', '2016-10-28 00:00:00', NULL, '2016-10-28 14:32:55', '2016-10-28 14:32:55'),
(12, 1, 1, '2016-10-01 00:00:00', '2016-10-28 00:00:00', NULL, '2016-10-28 14:34:29', '2016-10-28 14:34:29'),
(13, 1, 1, '2016-10-01 00:00:00', '2016-10-28 00:00:00', NULL, '2016-10-28 14:39:37', '2016-10-28 14:39:37'),
(14, 1, 1, '2016-10-01 00:00:00', '2016-10-28 00:00:00', NULL, '2016-10-28 14:42:48', '2016-10-28 14:42:48'),
(15, 1, 1, '2016-10-01 00:00:00', '2016-10-28 00:00:00', NULL, '2016-10-28 14:46:13', '2016-10-28 14:46:13'),
(16, 1, 1, '2016-10-01 00:00:00', '2016-10-28 00:00:00', 3, '2016-10-28 14:50:39', '2016-10-28 14:50:39'),
(17, 1, 1, '2016-10-01 00:00:00', '2016-10-28 00:00:00', 3, '2016-10-28 14:59:08', '2016-10-28 14:59:08'),
(18, 1, 1, '2016-10-01 00:00:00', '2016-10-28 00:00:00', NULL, '2016-10-28 15:12:47', '2016-10-28 15:12:47'),
(19, 1, 1, '2016-10-01 00:00:00', '2016-10-28 00:00:00', NULL, '2016-10-28 15:13:04', '2016-10-28 15:13:04'),
(20, 1, 2, '2016-10-01 00:00:00', '2016-10-28 00:00:00', NULL, '2016-10-28 15:13:14', '2016-10-28 15:13:14'),
(21, 1, 1, '2016-10-01 00:00:00', '2016-10-28 00:00:00', NULL, '2016-10-28 15:13:46', '2016-10-28 15:13:46'),
(22, 1, 1, '2016-10-01 00:00:00', '2016-10-28 00:00:00', NULL, '2016-10-28 15:15:26', '2016-10-28 15:15:26'),
(23, 1, 1, '2016-10-01 00:00:00', '2016-10-28 00:00:00', 3, '2016-10-28 15:19:12', '2016-10-28 15:19:12'),
(24, 1, 1, '2016-10-01 00:00:00', '2016-11-29 00:00:00', 6, '2016-11-29 04:43:05', '2016-11-29 04:43:05'),
(25, 2, 1, '2016-10-01 00:00:00', '2016-11-29 00:00:00', 4, '2016-11-29 04:43:31', '2016-11-29 04:43:31'),
(26, 2, 2, '2016-10-01 00:00:00', '2016-11-29 00:00:00', 0, '2016-11-29 04:44:00', '2016-11-29 04:44:00'),
(27, 1, 2, '2016-10-01 00:00:00', '2016-11-29 00:00:00', 10, '2016-11-29 04:44:05', '2016-11-29 04:44:05'),
(28, 2, 2, '2016-10-01 00:00:00', '2016-11-29 00:00:00', 0, '2016-11-29 05:42:33', '2016-11-29 05:42:33'),
(29, 1, 1, '2016-11-29 00:00:00', '2016-11-29 00:00:00', 0, '2016-11-29 09:13:18', '2016-11-29 09:13:18'),
(30, 1, 1, '2016-11-28 00:00:00', '2016-11-29 00:00:00', 0, '2016-11-29 09:13:23', '2016-11-29 09:13:23'),
(31, 1, 1, '2016-11-01 00:00:00', '2016-11-30 00:00:00', 6, '2016-11-29 09:13:43', '2016-11-29 09:13:43'),
(32, 1, 1, '2016-11-01 00:00:00', '2016-11-30 00:00:00', 6, '2016-11-29 09:45:14', '2016-11-29 09:45:14'),
(33, 2, 1, '2016-11-01 00:00:00', '2016-11-30 00:00:00', 4, '2016-11-29 09:45:21', '2016-11-29 09:45:21'),
(34, 2, 1, '2016-11-01 00:00:00', '2016-11-30 00:00:00', 4, '2016-11-29 09:45:25', '2016-11-29 09:45:25'),
(35, 2, 1, '2016-11-01 00:00:00', '2016-11-29 00:00:00', 4, '2016-11-29 09:45:45', '2016-11-29 09:45:45'),
(36, 1, 1, '2016-11-01 00:00:00', '2016-11-29 00:00:00', 6, '2016-11-29 09:45:54', '2016-11-29 09:45:54'),
(37, 1, 2, '2016-11-01 00:00:00', '2016-11-29 00:00:00', 10, '2016-11-29 09:52:52', '2016-11-29 09:52:52');

-- --------------------------------------------------------

--
-- Table structure for table `tran_laporan_stok_detail`
--

CREATE TABLE `tran_laporan_stok_detail` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `laporan_stok_header_id` int(11) DEFAULT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tran_laporan_stok_detail`
--

INSERT INTO `tran_laporan_stok_detail` (`id`, `laporan_stok_header_id`, `produk_id`, `jumlah`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 3, '2016-10-28 14:00:34', '2016-10-28 14:00:34'),
(2, 1, 2, 4, '2016-10-28 14:00:34', '2016-10-28 14:00:34'),
(3, 2, 1, 3, '2016-11-03 03:37:39', '2016-11-03 03:37:39'),
(4, 2, 2, 5, '2016-11-03 03:37:39', '2016-11-03 03:37:39'),
(5, 2, 3, 1, '2016-11-03 03:37:39', '2016-11-03 03:37:39'),
(6, 3, 1, 1, '2016-11-03 03:37:51', '2016-11-03 03:37:51'),
(7, 3, 2, 5, '2016-11-03 03:37:51', '2016-11-03 03:37:51'),
(8, 3, 3, 5, '2016-11-03 03:37:51', '2016-11-03 03:37:51'),
(9, 3, 4, 10, '2016-11-03 03:37:51', '2016-11-03 03:37:51'),
(10, 3, 5, 10, '2016-11-03 03:37:51', '2016-11-03 03:37:51'),
(11, 3, 6, 10, '2016-11-03 03:37:51', '2016-11-03 03:37:51'),
(12, 4, 1, 6, '2016-11-29 03:59:13', '2016-11-29 03:59:13'),
(13, 4, 2, 10, '2016-11-29 03:59:13', '2016-11-29 03:59:13'),
(14, 6, 1, 6, '2016-11-29 04:19:42', '2016-11-29 04:19:42'),
(15, 6, 2, 10, '2016-11-29 04:19:42', '2016-11-29 04:19:42'),
(16, 8, 1, 6, '2016-11-29 04:22:15', '2016-11-29 04:22:15'),
(17, 8, 2, 10, '2016-11-29 04:22:15', '2016-11-29 04:22:15'),
(18, 11, 1, 6, '2016-11-29 04:25:57', '2016-11-29 04:25:57'),
(19, 11, 2, 10, '2016-11-29 04:25:57', '2016-11-29 04:25:57'),
(20, 12, 1, 6, '2016-11-29 04:28:56', '2016-11-29 04:28:56'),
(21, 12, 2, 10, '2016-11-29 04:28:56', '2016-11-29 04:28:56'),
(22, 13, 1, 5, '2016-11-29 04:29:14', '2016-11-29 04:29:14'),
(23, 13, 2, 1, '2016-11-29 04:29:14', '2016-11-29 04:29:14'),
(24, 14, 1, 4, '2016-11-29 04:33:05', '2016-11-29 04:33:05'),
(25, 14, 2, 0, '2016-11-29 04:33:05', '2016-11-29 04:33:05'),
(26, 15, 1, 6, '2016-11-29 04:33:18', '2016-11-29 04:33:18'),
(27, 15, 2, 10, '2016-11-29 04:33:18', '2016-11-29 04:33:18'),
(28, 16, 1, 6, '2016-11-29 04:36:28', '2016-11-29 04:36:28'),
(29, 16, 2, 10, '2016-11-29 04:36:28', '2016-11-29 04:36:28'),
(30, 17, 1, 4, '2016-11-29 04:36:36', '2016-11-29 04:36:36'),
(31, 17, 2, 0, '2016-11-29 04:36:36', '2016-11-29 04:36:36'),
(32, 18, 1, 6, '2016-11-29 04:36:41', '2016-11-29 04:36:42'),
(33, 18, 2, 10, '2016-11-29 04:36:42', '2016-11-29 04:36:42'),
(34, 19, 1, 6, '2016-11-29 09:12:51', '2016-11-29 09:12:51'),
(35, 19, 2, 10, '2016-11-29 09:12:51', '2016-11-29 09:12:51'),
(36, 20, 1, 6, '2016-11-29 09:23:59', '2016-11-29 09:23:59'),
(37, 20, 2, 10, '2016-11-29 09:23:59', '2016-11-29 09:23:59'),
(38, 21, 1, 6, '2016-11-29 09:36:37', '2016-11-29 09:36:37'),
(39, 21, 2, 10, '2016-11-29 09:36:37', '2016-11-29 09:36:37'),
(40, 22, 1, 4, '2016-11-29 09:36:40', '2016-11-29 09:36:40'),
(41, 22, 2, 0, '2016-11-29 09:36:40', '2016-11-29 09:36:40'),
(42, 23, 1, 4, '2016-11-29 09:45:31', '2016-11-29 09:45:31'),
(43, 23, 2, 0, '2016-11-29 09:45:31', '2016-11-29 09:45:31'),
(44, 24, 1, 6, '2016-11-29 09:45:34', '2016-11-29 09:45:34'),
(45, 24, 2, 10, '2016-11-29 09:45:34', '2016-11-29 09:45:34'),
(46, 25, 1, 6, '2016-11-29 09:45:38', '2016-11-29 09:45:38'),
(47, 25, 2, 10, '2016-11-29 09:45:38', '2016-11-29 09:45:38'),
(48, 26, 1, 6, '2016-11-29 12:08:08', '2016-11-29 12:08:08'),
(49, 26, 2, 10, '2016-11-29 12:08:08', '2016-11-29 12:08:08'),
(50, 27, 1, 4, '2016-11-29 12:08:14', '2016-11-29 12:08:14'),
(51, 27, 2, 0, '2016-11-29 12:08:14', '2016-11-29 12:08:14'),
(52, 28, 1, 4, '2016-11-29 12:57:30', '2016-11-29 12:57:30'),
(53, 28, 2, 0, '2016-11-29 12:57:30', '2016-11-29 12:57:30'),
(54, 29, 1, 6, '2016-11-29 12:57:37', '2016-11-29 12:57:37'),
(55, 29, 2, 10, '2016-11-29 12:57:37', '2016-11-29 12:57:37'),
(56, 30, 1, 1, '2016-11-29 13:01:52', '2016-11-29 13:01:52'),
(57, 30, 2, 0, '2016-11-29 13:01:52', '2016-11-29 13:01:52'),
(58, 31, 1, 2, '2016-11-29 13:03:10', '2016-11-29 13:03:10'),
(59, 31, 2, 0, '2016-11-29 13:03:10', '2016-11-29 13:03:10');

-- --------------------------------------------------------

--
-- Table structure for table `tran_laporan_stok_header`
--

CREATE TABLE `tran_laporan_stok_header` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `gudang_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `total_stok` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tran_laporan_stok_header`
--

INSERT INTO `tran_laporan_stok_header` (`id`, `gudang_id`, `tanggal`, `total_stok`, `created_at`, `updated_at`) VALUES
(1, 1, '2016-10-28 00:00:00', 7, '2016-10-28 14:00:34', '2016-10-28 14:00:34'),
(2, 1, '2016-11-03 00:00:00', 9, '2016-11-03 03:37:39', '2016-11-03 03:37:39'),
(3, 2, '2016-11-03 00:00:00', 41, '2016-11-03 03:37:51', '2016-11-03 03:37:51'),
(4, 1, '2016-11-29 00:00:00', 16, '2016-11-29 03:59:13', '2016-11-29 03:59:13'),
(5, 2, '2016-11-29 00:00:00', 0, '2016-11-29 04:00:52', '2016-11-29 04:00:52'),
(6, 1, '2016-11-29 00:00:00', 16, '2016-11-29 04:19:42', '2016-11-29 04:19:42'),
(7, 2, '2016-11-29 00:00:00', 0, '2016-11-29 04:19:48', '2016-11-29 04:19:48'),
(8, 1, '2016-11-29 00:00:00', 16, '2016-11-29 04:22:15', '2016-11-29 04:22:15'),
(9, 2, '2016-11-29 00:00:00', 0, '2016-11-29 04:22:24', '2016-11-29 04:22:24'),
(10, 2, '2016-11-30 00:00:00', 0, '2016-11-29 04:25:53', '2016-11-29 04:25:53'),
(11, 1, '2016-11-30 00:00:00', 16, '2016-11-29 04:25:57', '2016-11-29 04:25:57'),
(12, 1, '2016-11-29 00:00:00', 16, '2016-11-29 04:28:56', '2016-11-29 04:28:56'),
(13, 2, '2016-11-29 00:00:00', 6, '2016-11-29 04:29:14', '2016-11-29 04:29:14'),
(14, 2, '2016-11-29 00:00:00', 4, '2016-11-29 04:33:05', '2016-11-29 04:33:05'),
(15, 1, '2016-11-29 00:00:00', 16, '2016-11-29 04:33:18', '2016-11-29 04:33:18'),
(16, 1, '2016-11-29 00:00:00', 16, '2016-11-29 04:36:28', '2016-11-29 04:36:28'),
(17, 2, '2016-11-29 00:00:00', 4, '2016-11-29 04:36:36', '2016-11-29 04:36:36'),
(18, 1, '2016-11-29 00:00:00', 16, '2016-11-29 04:36:41', '2016-11-29 04:36:42'),
(19, 1, '2016-11-30 00:00:00', 16, '2016-11-29 09:12:51', '2016-11-29 09:12:51'),
(20, 1, '2016-11-30 00:00:00', 16, '2016-11-29 09:23:59', '2016-11-29 09:23:59'),
(21, 1, '2016-11-30 00:00:00', 16, '2016-11-29 09:36:37', '2016-11-29 09:36:37'),
(22, 2, '2016-11-30 00:00:00', 4, '2016-11-29 09:36:40', '2016-11-29 09:36:40'),
(23, 2, '2016-11-30 00:00:00', 4, '2016-11-29 09:45:31', '2016-11-29 09:45:31'),
(24, 1, '2016-11-30 00:00:00', 16, '2016-11-29 09:45:34', '2016-11-29 09:45:34'),
(25, 1, '2016-11-29 00:00:00', 16, '2016-11-29 09:45:38', '2016-11-29 09:45:38'),
(26, 1, '2016-11-29 00:00:00', 16, '2016-11-29 12:08:08', '2016-11-29 12:08:08'),
(27, 2, '2016-11-29 00:00:00', 4, '2016-11-29 12:08:14', '2016-11-29 12:08:14'),
(28, 2, '2016-11-29 00:00:00', 4, '2016-11-29 12:57:30', '2016-11-29 12:57:30'),
(29, 1, '2016-11-29 00:00:00', 16, '2016-11-29 12:57:37', '2016-11-29 12:57:37'),
(30, 2, '2016-11-29 00:00:00', 1, '2016-11-29 13:01:52', '2016-11-29 13:01:52'),
(31, 2, '2016-11-29 00:00:00', 2, '2016-11-29 13:03:10', '2016-11-29 13:03:10');

-- --------------------------------------------------------

--
-- Table structure for table `tran_mini_banner`
--

CREATE TABLE `tran_mini_banner` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_gambar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `list_produk` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tran_mini_banner`
--

INSERT INTO `tran_mini_banner` (`id`, `nama`, `url`, `file_gambar`, `list_produk`, `item_order`, `created_at`, `updated_at`) VALUES
(1, 'Promo 1', 'promo-1', 'mb-ffkf-promo-1.jpg', NULL, NULL, '2016-12-06 13:26:31', '2016-12-06 13:26:31'),
(2, 'Promo 2', 'promo-2', 'mb-c0nl-promo-2.jpg', NULL, NULL, '2016-12-06 13:27:16', '2016-12-06 13:27:16'),
(3, 'Promo 3', 'promo-3', 'mb-rf6k-promo-3.png', NULL, NULL, '2016-12-06 13:27:33', '2016-12-06 13:27:33'),
(4, 'Promo 4', 'promo-4-1', 'mb-1dk6-promo-4-1.png', NULL, NULL, '2016-12-06 13:27:47', '2016-12-06 13:28:48');

-- --------------------------------------------------------

--
-- Table structure for table `tran_mutasi_stok`
--

CREATE TABLE `tran_mutasi_stok` (
  `id` int(10) UNSIGNED NOT NULL,
  `gudang_id_asal` int(11) NOT NULL,
  `gudang_id_tujuan` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `approval_by_user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `received_by_user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_mutasi_stok_detail`
--

CREATE TABLE `tran_mutasi_stok_detail` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `mutasi_stok_header_id` int(11) NOT NULL,
  `no_mutasi_stok` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `produk_id` int(11) NOT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jumlah` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_mutasi_stok_header`
--

CREATE TABLE `tran_mutasi_stok_header` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `no_mutasi_stok` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gudang_id_asal` int(11) NOT NULL,
  `gudang_id_tujuan` int(11) NOT NULL,
  `tanggal` datetime DEFAULT NULL,
  `approval_by_user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `received_by_user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tran_mutasi_stok_header`
--

INSERT INTO `tran_mutasi_stok_header` (`id`, `no_mutasi_stok`, `gudang_id_asal`, `gudang_id_tujuan`, `tanggal`, `approval_by_user_id`, `received_by_user_id`, `created_at`, `updated_at`) VALUES
(1, 'MTS/21/10/YY/0001', 2, 1, '2016-10-21 00:00:00', NULL, NULL, '2016-10-21 07:12:04', '2016-10-21 07:12:04');

-- --------------------------------------------------------

--
-- Table structure for table `tran_nilai_spesifikasi`
--

CREATE TABLE `tran_nilai_spesifikasi` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `produk_id` bigint(20) DEFAULT NULL,
  `parameter_spesifikasi_id` bigint(20) DEFAULT NULL,
  `nilai` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `satuan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_nota_beli`
--

CREATE TABLE `tran_nota_beli` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sj_masuk_header_id` bigint(20) NOT NULL,
  `no_nota` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `jatuh_tempo` datetime DEFAULT NULL,
  `total_diskon` bigint(20) DEFAULT NULL,
  `no_jurnal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_lunas` int(11) DEFAULT NULL,
  `ppn` int(11) NOT NULL,
  `total_harga` bigint(20) NOT NULL,
  `total_tagihan` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tran_nota_beli`
--

INSERT INTO `tran_nota_beli` (`id`, `sj_masuk_header_id`, `no_nota`, `supplier_id`, `tanggal`, `jatuh_tempo`, `total_diskon`, `no_jurnal`, `is_lunas`, `ppn`, `total_harga`, `total_tagihan`, `created_at`, `updated_at`) VALUES
(2, 2, 'NB-201611-00001', 1, '2016-11-21 00:00:00', '2016-11-25 00:00:00', 2000000, NULL, 1, 0, 0, 0, '2016-11-21 14:42:45', '2016-11-21 14:43:41'),
(3, 3, 'NB-201611-00003', 1, '2016-11-29 00:00:00', '2016-12-29 00:00:00', 200000, NULL, 0, 0, 0, 0, '2016-11-29 04:27:30', '2016-11-29 04:27:30'),
(4, 4, 'NB-201611-00004', 1, '2016-11-29 00:00:00', '2016-12-29 00:00:00', 0, NULL, 0, 0, 0, 0, '2016-11-29 04:28:22', '2016-11-29 04:28:22'),
(5, 5, 'NB-201611-00005', 4, '2016-11-30 00:00:00', '2016-12-29 00:00:00', 0, NULL, 0, 0, 0, 0, '2016-11-30 07:51:29', '2016-11-30 07:51:29');

-- --------------------------------------------------------

--
-- Table structure for table `tran_nota_jual`
--

CREATE TABLE `tran_nota_jual` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sj_keluar_header_id` bigint(20) NOT NULL,
  `no_nota` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pelanggan_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `jatuh_tempo` datetime DEFAULT NULL,
  `no_jurnal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_voucher` bigint(20) DEFAULT NULL,
  `is_lunas` int(11) DEFAULT NULL,
  `ppn` int(11) NOT NULL,
  `total_harga` bigint(20) NOT NULL,
  `total_tagihan` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tran_nota_jual`
--

INSERT INTO `tran_nota_jual` (`id`, `sj_keluar_header_id`, `no_nota`, `pelanggan_id`, `tanggal`, `jatuh_tempo`, `no_jurnal`, `total_voucher`, `is_lunas`, `ppn`, `total_harga`, `total_tagihan`, `created_at`, `updated_at`) VALUES
(1, 1, 'NJ-201611-00001', 15, '2016-11-21 00:00:00', '2016-11-21 00:00:00', NULL, NULL, 1, 0, 0, 0, '2016-11-21 14:45:31', '2016-11-21 14:46:11'),
(2, 2, 'NJ-201611-00002', 4, '2016-11-29 00:00:00', '2016-11-30 00:00:00', NULL, NULL, 0, 0, 0, 0, '2016-11-29 13:02:20', '2016-11-29 13:02:20');

-- --------------------------------------------------------

--
-- Table structure for table `tran_paket_gudang`
--

CREATE TABLE `tran_paket_gudang` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `paket_id` int(11) DEFAULT NULL,
  `gudang_id` int(11) DEFAULT NULL,
  `stok` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_paket_produk`
--

CREATE TABLE `tran_paket_produk` (
  `paket_id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `qty_produk` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tran_paket_produk`
--

INSERT INTO `tran_paket_produk` (`paket_id`, `produk_id`, `qty_produk`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(1, 2, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tran_parameter_spesifikasi`
--

CREATE TABLE `tran_parameter_spesifikasi` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kategori_produk_id` int(11) DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_pengaturan_jurnal`
--

CREATE TABLE `tran_pengaturan_jurnal` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `transaksi_id` int(11) NOT NULL,
  `akun_id` int(11) NOT NULL,
  `debit_or_kredit` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tran_pengaturan_jurnal`
--

INSERT INTO `tran_pengaturan_jurnal` (`id`, `transaksi_id`, `akun_id`, `debit_or_kredit`, `created_at`, `updated_at`) VALUES
(8, 1, 4, 'D', '2016-11-18 11:54:02', '2016-11-21 13:13:11'),
(9, 1, 11, 'D', '2016-11-18 11:54:10', '2016-11-21 13:13:22'),
(10, 6, 4, 'K', '2016-11-18 11:54:21', '2016-11-21 13:15:31'),
(11, 8, 4, 'D', '2016-11-18 11:54:31', '2016-11-21 13:16:01'),
(12, 2, 11, 'K', '2016-11-21 13:13:31', '2016-11-21 13:13:31'),
(13, 3, 4, 'K', '2016-11-21 13:13:41', '2016-11-21 13:13:41'),
(14, 3, 6, 'D', '2016-11-21 13:14:07', '2016-11-21 13:14:07'),
(15, 5, 6, 'K', '2016-11-21 13:14:17', '2016-11-21 13:14:17'),
(16, 6, 11, 'D', '2016-11-21 13:15:24', '2016-11-21 13:15:24'),
(17, 8, 6, 'K', '2016-11-21 13:16:16', '2016-11-21 13:16:16'),
(18, 10, 6, 'D', '2016-11-21 13:16:26', '2016-11-21 13:16:26'),
(19, 11, 2, 'K', '2016-11-21 13:16:42', '2016-11-21 13:16:51'),
(20, 11, 11, 'K', '2016-11-21 13:17:24', '2016-11-21 13:17:24'),
(21, 12, 2, 'D', '2016-11-21 13:17:48', '2016-11-21 13:17:48'),
(22, 12, 6, 'K', '2016-11-21 13:18:32', '2016-11-21 13:18:32');

-- --------------------------------------------------------

--
-- Table structure for table `tran_penyesuaian_stok`
--

CREATE TABLE `tran_penyesuaian_stok` (
  `id` int(10) UNSIGNED NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `stok_awal` int(11) DEFAULT NULL,
  `stok_ubah` int(11) DEFAULT NULL,
  `stok_baru` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `approval_by_user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `received_by_user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tran_penyesuaian_stok`
--

INSERT INTO `tran_penyesuaian_stok` (`id`, `gudang_id`, `produk_id`, `serial_number`, `stok_awal`, `stok_ubah`, `stok_baru`, `tanggal`, `approval_by_user_id`, `received_by_user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 5, '6WKX582', 5, 1, 6, '2016-09-17 13:20:12', '1', NULL, NULL, NULL),
(2, 1, 5, '3RGX582', 5, 1, 6, '2016-09-17 13:20:12', '1', NULL, NULL, NULL),
(3, 1, 5, '30HX582', 5, 1, 6, '2016-09-17 13:20:12', '1', NULL, NULL, NULL),
(4, 1, 5, '65XX582', 5, 1, 6, '2016-09-17 13:20:12', '1', NULL, NULL, NULL),
(5, 1, 5, '3KNX582', 5, 1, 6, '2016-09-17 13:20:12', '1', NULL, NULL, NULL),
(6, 1, 5, '6WKX582', 30, 1, 31, '2016-09-17 13:21:32', '1', NULL, NULL, NULL),
(7, 1, 5, '3RGX582', 30, 1, 31, '2016-09-17 13:21:32', '1', NULL, NULL, NULL),
(8, 1, 5, '30HX582', 30, 1, 31, '2016-09-17 13:21:32', '1', NULL, NULL, NULL),
(9, 1, 5, '65XX582', 30, 1, 31, '2016-09-17 13:21:32', '1', NULL, NULL, NULL),
(10, 1, 5, '3KNX582', 30, 1, 31, '2016-09-17 13:21:32', '1', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tran_po_alamat_pengiriman`
--

CREATE TABLE `tran_po_alamat_pengiriman` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `po_header_id` bigint(20) NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alamat` text COLLATE utf8_unicode_ci,
  `kota_id` int(11) DEFAULT NULL,
  `kode_pos` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metode_pengiriman_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tran_po_alamat_pengiriman`
--

INSERT INTO `tran_po_alamat_pengiriman` (`id`, `po_header_id`, `nama`, `email`, `alamat`, `kota_id`, `kode_pos`, `hp`, `metode_pengiriman_id`, `created_at`, `updated_at`) VALUES
(1, 3, NULL, NULL, 'Galerindo Teknologi\r\nHi Tech Mall Lt. Dasar Blok.D No.17-19\r\nJl. Kusuma Bangsa No.116-118 Surabaya\r\nT. 031 5477085 | 031 5348998\r\nUp. Bpk. Gondo/ Ibu Vera', NULL, NULL, NULL, NULL, '2016-11-21 21:40:08', '2016-11-21 21:40:08'),
(2, 4, NULL, NULL, 'Galerindo Teknologi\r\nHi Tech Mall Lt. Dasar Blok.D No.17-19\r\nJl. Kusuma Bangsa No.116-118 Surabaya\r\nT. 031 5477085 | 031 5348998\r\nUp. Bpk. Gondo/ Ibu Vera', NULL, NULL, NULL, NULL, '2016-11-29 11:15:44', '2016-11-29 11:15:44'),
(3, 5, NULL, NULL, 'Galerindo Teknologi\r\nHi Tech Mall Lt. Dasar Blok.D No.17-19\r\nJl. Kusuma Bangsa No.116-118 Surabaya\r\nT. 031 5477085 | 031 5348998\r\nUp. Bpk. Gondo/ Ibu Vera', NULL, NULL, NULL, NULL, '2016-11-30 14:40:40', '2016-11-30 14:40:40');

-- --------------------------------------------------------

--
-- Table structure for table `tran_po_detail`
--

CREATE TABLE `tran_po_detail` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `po_header_id` bigint(20) NOT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `jenis_barang_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `jumlah_terkirim` int(11) DEFAULT NULL,
  `deskripsi` text COLLATE utf8_unicode_ci,
  `harga` bigint(20) DEFAULT NULL,
  `harga_retail` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tran_po_detail`
--

INSERT INTO `tran_po_detail` (`id`, `po_header_id`, `produk_id`, `jenis_barang_id`, `jumlah`, `jumlah_terkirim`, `deskripsi`, `harga`, `harga_retail`, `created_at`, `updated_at`) VALUES
(3, 3, 1, '1', 10, 10, 'core i3 generasi terbaru', 4000000, NULL, '2016-11-21 21:40:08', '2016-11-21 21:42:25'),
(4, 3, 2, '1', 10, 10, 'core i3 generasi terbaru', 4000000, NULL, '2016-11-21 21:40:08', '2016-11-21 21:42:25'),
(5, 4, 1, '1', 5, 5, 'ubuntu aja', 4800000, NULL, '2016-11-29 11:15:44', '2016-11-29 11:25:34'),
(6, 4, 2, '1', 1, 1, 'ubuntu aja', 4800000, NULL, '2016-11-29 11:15:44', '2016-11-29 11:21:36'),
(7, 5, 7, '1', 15, 15, 'i5-6200U/8GB/256GB SSD/13.3"/W10SL/Non DVDRW', 12950000, NULL, '2016-11-30 14:40:40', '2016-11-30 14:50:33');

-- --------------------------------------------------------

--
-- Table structure for table `tran_po_diskon`
--

CREATE TABLE `tran_po_diskon` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `po_header_id` bigint(20) NOT NULL,
  `nominal` bigint(20) DEFAULT NULL,
  `is_dipakai` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tran_po_diskon`
--

INSERT INTO `tran_po_diskon` (`id`, `po_header_id`, `nominal`, `is_dipakai`, `created_at`, `updated_at`) VALUES
(1, 3, 2000000, 1, '2016-11-21 21:40:08', '2016-11-21 14:42:45'),
(2, 4, 200000, 1, '2016-11-29 11:15:44', '2016-11-29 04:27:30');

-- --------------------------------------------------------

--
-- Table structure for table `tran_po_header`
--

CREATE TABLE `tran_po_header` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `no_purchase_order` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `due_date` datetime DEFAULT NULL,
  `catatan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_tagihan` bigint(20) DEFAULT NULL,
  `flag_sj_masuk` int(11) DEFAULT NULL,
  `syarat_ketentuan` text COLLATE utf8_unicode_ci NOT NULL,
  `ppn` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tran_po_header`
--

INSERT INTO `tran_po_header` (`id`, `no_purchase_order`, `supplier_id`, `tanggal`, `due_date`, `catatan`, `total_tagihan`, `flag_sj_masuk`, `syarat_ketentuan`, `ppn`, `created_at`, `updated_at`) VALUES
(3, 'PO-201611-00001', 1, '2016-11-21 21:40:08', NULL, '', 78000000, 1, '', 0, '2016-11-21 21:40:08', '2016-11-21 21:42:25'),
(4, 'PO-201611-00004', 1, '2016-11-29 11:15:44', NULL, 'Kirim Cepat', 28600000, 1, '', 10, '2016-11-29 11:15:44', '2016-11-29 11:25:34'),
(5, 'PO-201611-00005', 4, '2016-11-30 14:40:40', NULL, '', 194250000, 1, '', 0, '2016-11-30 14:40:40', '2016-11-30 14:50:33');

-- --------------------------------------------------------

--
-- Table structure for table `tran_produk_akan_datang`
--

CREATE TABLE `tran_produk_akan_datang` (
  `produk_id` int(11) NOT NULL,
  `tanggal_input` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tran_produk_akan_datang`
--

INSERT INTO `tran_produk_akan_datang` (`produk_id`, `tanggal_input`, `created_at`, `updated_at`) VALUES
(6, '2016-10-06 09:11:24', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tran_produk_baru`
--

CREATE TABLE `tran_produk_baru` (
  `produk_id` int(11) NOT NULL,
  `tanggal_input` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tran_produk_baru`
--

INSERT INTO `tran_produk_baru` (`produk_id`, `tanggal_input`, `created_at`, `updated_at`) VALUES
(1, '2016-09-30 09:14:13', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tran_produk_galeri`
--

CREATE TABLE `tran_produk_galeri` (
  `id` int(10) UNSIGNED NOT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `file_gambar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tran_produk_galeri`
--

INSERT INTO `tran_produk_galeri` (`id`, `produk_id`, `file_gambar`, `created_at`, `updated_at`) VALUES
(1, 1, 'PRD7NEYU8R9.jpg', NULL, NULL),
(2, 2, 'PRDVCSR6Q3M.jpg', NULL, NULL),
(3, 4, 'PRD8YSVANAL.jpg', NULL, NULL),
(4, 7, 'PRDWMCUYJ5D.jpg', NULL, NULL),
(5, 7, 'PRD1TVMNVVU.jpg', NULL, NULL),
(6, 7, 'PRDHTVE312M.jpg', NULL, NULL),
(7, 8, 'PRDCPLZ9I42.JPG', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tran_produk_gudang`
--

CREATE TABLE `tran_produk_gudang` (
  `id` bigint(20) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `stok` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tran_produk_gudang`
--

INSERT INTO `tran_produk_gudang` (`id`, `produk_id`, `gudang_id`, `stok`, `created_at`, `updated_at`) VALUES
(23, 1, 1, '6', '2016-11-21 21:42:25', '2016-11-21 21:45:13'),
(24, 2, 1, '10', '2016-11-21 21:42:25', '2016-11-21 21:42:25'),
(25, 1, 2, '2', '2016-11-29 11:21:36', '2016-11-29 20:02:55'),
(26, 2, 2, '0', '2016-11-29 11:21:36', '2016-11-29 11:32:47'),
(27, 7, 1, '15', '2016-11-30 14:50:33', '2016-11-30 14:50:33');

-- --------------------------------------------------------

--
-- Table structure for table `tran_produk_harga`
--

CREATE TABLE `tran_produk_harga` (
  `id` int(10) UNSIGNED NOT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `kategori_pelanggan_id` int(11) DEFAULT NULL,
  `diskon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `awal_periode` datetime DEFAULT NULL,
  `akhir_periode` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_produk_pilihan`
--

CREATE TABLE `tran_produk_pilihan` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_gambar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `list_produk` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_produk_serial_number`
--

CREATE TABLE `tran_produk_serial_number` (
  `id` bigint(20) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stok` int(11) DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tran_produk_serial_number`
--

INSERT INTO `tran_produk_serial_number` (`id`, `gudang_id`, `produk_id`, `serial_number`, `stok`, `keterangan`, `created_at`, `updated_at`) VALUES
(143, 1, 1, 'DLBK0001', 0, NULL, '2016-11-21 21:42:25', '2016-11-21 21:45:13'),
(144, 1, 1, 'DLBK0002', 0, NULL, '2016-11-21 21:42:25', '2016-11-21 21:45:13'),
(145, 1, 1, 'DLBK0003', 0, NULL, '2016-11-21 21:42:25', '2016-11-21 21:45:13'),
(146, 1, 1, 'DLBK0004', 0, NULL, '2016-11-21 21:42:25', '2016-11-21 21:45:13'),
(147, 1, 1, 'DLBK0005', 1, NULL, '2016-11-21 21:42:25', '2016-11-21 21:42:25'),
(148, 1, 1, 'DLBK0006', 1, NULL, '2016-11-21 21:42:25', '2016-11-21 21:42:25'),
(149, 1, 1, 'DLBK0007', 1, NULL, '2016-11-21 21:42:25', '2016-11-21 21:42:25'),
(150, 1, 1, 'DLBK0008', 1, NULL, '2016-11-21 21:42:25', '2016-11-21 21:42:25'),
(151, 1, 1, 'DLBK0009', 1, NULL, '2016-11-21 21:42:25', '2016-11-21 21:42:25'),
(152, 1, 1, 'DLBK0010', 1, NULL, '2016-11-21 21:42:25', '2016-11-21 21:42:25'),
(153, 1, 2, 'DLRD0001', 1, NULL, '2016-11-21 21:42:25', '2016-11-21 21:42:25'),
(154, 1, 2, 'DLRD0002', 1, NULL, '2016-11-21 21:42:25', '2016-11-21 21:42:25'),
(155, 1, 2, 'DLRD0003', 1, NULL, '2016-11-21 21:42:25', '2016-11-21 21:42:25'),
(156, 1, 2, 'DLRD0004', 1, NULL, '2016-11-21 21:42:25', '2016-11-21 21:42:25'),
(157, 1, 2, 'DLRD0005', 1, NULL, '2016-11-21 21:42:25', '2016-11-21 21:42:25'),
(158, 1, 2, 'DLRD0006', 1, NULL, '2016-11-21 21:42:25', '2016-11-21 21:42:25'),
(159, 1, 2, 'DLRD0007', 1, NULL, '2016-11-21 21:42:25', '2016-11-21 21:42:25'),
(160, 1, 2, 'DLRD0008', 1, NULL, '2016-11-21 21:42:25', '2016-11-21 21:42:25'),
(161, 1, 2, 'DLRD0009', 1, NULL, '2016-11-21 21:42:25', '2016-11-21 21:42:25'),
(162, 1, 2, 'DLRD0010', 1, NULL, '2016-11-21 21:42:25', '2016-11-21 21:42:25'),
(163, 2, 1, 'XX-TOKO-BLACK', 0, NULL, '2016-11-29 11:21:36', '2016-11-29 20:01:33'),
(164, 2, 2, 'XX-TOKO-RED', 0, NULL, '2016-11-29 11:21:36', '2016-11-29 11:32:47'),
(165, 2, 1, 'XX-KANTOR-BLACK', 2, NULL, '2016-11-29 11:25:34', '2016-11-29 20:02:55'),
(166, 1, 7, '2MLSY22', 1, NULL, '2016-11-30 14:50:33', '2016-11-30 14:50:33'),
(167, 1, 7, '52NSY22', 1, NULL, '2016-11-30 14:50:33', '2016-11-30 14:50:33'),
(168, 1, 7, '5W0TY22', 1, NULL, '2016-11-30 14:50:33', '2016-11-30 14:50:33'),
(169, 1, 7, 'C8RSY22', 1, NULL, '2016-11-30 14:50:33', '2016-11-30 14:50:33'),
(170, 1, 7, '65VCX02', 1, NULL, '2016-11-30 14:50:33', '2016-11-30 14:50:33'),
(171, 1, 7, '3ZXSY22', 1, NULL, '2016-11-30 14:50:33', '2016-11-30 14:50:33'),
(172, 1, 7, '7LLSY22', 1, NULL, '2016-11-30 14:50:33', '2016-11-30 14:50:33'),
(173, 1, 7, 'D1YSY22', 1, NULL, '2016-11-30 14:50:33', '2016-11-30 14:50:33'),
(174, 1, 7, 'B9RSY22', 1, NULL, '2016-11-30 14:50:33', '2016-11-30 14:50:33'),
(175, 1, 7, '8V0TY22', 1, NULL, '2016-11-30 14:50:33', '2016-11-30 14:50:33'),
(176, 1, 7, 'BYMSY22', 1, NULL, '2016-11-30 14:50:33', '2016-11-30 14:50:33'),
(177, 1, 7, 'JFLSY22', 1, NULL, '2016-11-30 14:50:33', '2016-11-30 14:50:33'),
(178, 1, 7, 'HW0TY22', 1, NULL, '2016-11-30 14:50:33', '2016-11-30 14:50:33'),
(179, 1, 7, 'J5NSY22', 1, NULL, '2016-11-30 14:50:33', '2016-11-30 14:50:33'),
(180, 1, 7, '9TMSY22', 1, NULL, '2016-11-30 14:50:33', '2016-11-30 14:50:33');

-- --------------------------------------------------------

--
-- Table structure for table `tran_produk_supplier`
--

CREATE TABLE `tran_produk_supplier` (
  `id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `harga_terakhir` bigint(20) DEFAULT NULL,
  `tanggal_terakhir` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tran_produk_supplier`
--

INSERT INTO `tran_produk_supplier` (`id`, `produk_id`, `supplier_id`, `harga_terakhir`, `tanggal_terakhir`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 4800000, '2016-11-29 11:15:44', NULL, NULL),
(2, 2, 1, 4800000, '2016-11-29 11:15:44', NULL, NULL),
(3, 3, 1, 5500000, '2016-10-31 21:06:37', NULL, NULL),
(4, 4, 1, 5300000, '2016-10-31 21:06:37', NULL, NULL),
(5, 5, 1, 5000000, '2016-11-03 10:40:16', NULL, NULL),
(6, 6, 1, 5000000, '2016-11-03 10:40:16', NULL, NULL),
(7, 7, 4, 12950000, '2016-11-30 14:40:40', NULL, NULL),
(8, 8, 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tran_produk_tampil_di_beranda`
--

CREATE TABLE `tran_produk_tampil_di_beranda` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `tanggal_input` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tran_produk_tampil_di_beranda`
--

INSERT INTO `tran_produk_tampil_di_beranda` (`id`, `produk_id`, `tanggal_input`, `created_at`, `updated_at`) VALUES
(1, 1, '2016-10-10 08:48:21', NULL, NULL),
(2, 2, '2016-10-10 08:48:26', NULL, NULL),
(3, 3, '2016-10-10 08:48:32', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tran_promo_cashback`
--

CREATE TABLE `tran_promo_cashback` (
  `id` int(10) UNSIGNED NOT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `kode_promo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qty_beli` int(11) DEFAULT NULL,
  `cashback` int(11) DEFAULT NULL,
  `awal_periode` datetime DEFAULT NULL,
  `akhir_periode` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_promo_diskon`
--

CREATE TABLE `tran_promo_diskon` (
  `id` int(10) UNSIGNED NOT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `kode_promo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qty_beli` int(11) DEFAULT NULL,
  `diskon` int(11) DEFAULT NULL,
  `awal_periode` datetime DEFAULT NULL,
  `akhir_periode` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tran_promo_diskon`
--

INSERT INTO `tran_promo_diskon` (`id`, `produk_id`, `kode_promo`, `qty_beli`, `diskon`, `awal_periode`, `akhir_periode`, `created_at`, `updated_at`) VALUES
(1, 8, NULL, 1, 10, '2016-12-01 00:00:00', '2016-12-30 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tran_promo_hadiah`
--

CREATE TABLE `tran_promo_hadiah` (
  `id` int(10) UNSIGNED NOT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `hadiah_id` int(11) DEFAULT NULL,
  `kode_promo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qty_beli` int(11) DEFAULT NULL,
  `qty_hadiah` int(11) DEFAULT NULL,
  `awal_periode` datetime DEFAULT NULL,
  `akhir_periode` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_rb_detail`
--

CREATE TABLE `tran_rb_detail` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `rb_header_id` int(11) NOT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `jenis_barang_id` int(11) DEFAULT NULL,
  `gudang_id` int(11) DEFAULT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `deskripsi` text COLLATE utf8_unicode_ci,
  `harga` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tran_rb_detail`
--

INSERT INTO `tran_rb_detail` (`id`, `rb_header_id`, `produk_id`, `jenis_barang_id`, `gudang_id`, `serial_number`, `jumlah`, `deskripsi`, `harga`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 1, 2, 'XX-TOKO-RED', 1, '<p>DELL INSPIRON 3458 I3-5005 UMA UBT -&nbsp;RED</p>', 4800000, '2016-11-29 11:32:47', '2016-11-29 11:32:47'),
(2, 1, 1, 1, 2, 'XX-TOKO-BLACK', 1, '<p>DELL INSPIRON 3458 I3-5005 UMA UBT - BLACK</p>', 4800000, '2016-11-29 11:32:47', '2016-11-29 11:32:47');

-- --------------------------------------------------------

--
-- Table structure for table `tran_rb_header`
--

CREATE TABLE `tran_rb_header` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nota_beli_id` bigint(20) NOT NULL,
  `no_nota` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `potongan_retur` bigint(20) DEFAULT NULL,
  `total_retur` bigint(20) DEFAULT NULL,
  `total_tagihan` bigint(20) NOT NULL,
  `catatan` text COLLATE utf8_unicode_ci,
  `no_jurnal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_lunas` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tran_rb_header`
--

INSERT INTO `tran_rb_header` (`id`, `nota_beli_id`, `no_nota`, `supplier_id`, `tanggal`, `potongan_retur`, `total_retur`, `total_tagihan`, `catatan`, `no_jurnal`, `is_lunas`, `created_at`, `updated_at`) VALUES
(1, 3, 'RB-201611-00001', 1, '2016-11-29 11:32:47', 65000, 9600000, 0, 'dot pixel', NULL, 0, '2016-11-29 11:32:47', '2016-11-29 11:32:47');

-- --------------------------------------------------------

--
-- Table structure for table `tran_rekening_supplier`
--

CREATE TABLE `tran_rekening_supplier` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama_bank` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomor_rekening` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `atas_nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mata_uang` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supplier_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tran_rekening_supplier`
--

INSERT INTO `tran_rekening_supplier` (`id`, `nama_bank`, `nomor_rekening`, `atas_nama`, `mata_uang`, `supplier_id`, `created_at`, `updated_at`) VALUES
(1, 'BCA', '928882003222', 'Sdsdsds', 'rupiah', '5', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tran_rj_detail`
--

CREATE TABLE `tran_rj_detail` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `rj_header_id` int(11) NOT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `jenis_barang_id` int(11) DEFAULT NULL,
  `gudang_id` int(11) DEFAULT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `deskripsi` text COLLATE utf8_unicode_ci,
  `harga` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tran_rj_detail`
--

INSERT INTO `tran_rj_detail` (`id`, `rj_header_id`, `produk_id`, `jenis_barang_id`, `gudang_id`, `serial_number`, `jumlah`, `deskripsi`, `harga`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 2, 'XX-KANTOR-BLACK', 1, '<p>DELL INSPIRON 3458 I3-5005 UMA UBT - BLACK</p>', 5225000, '2016-11-29 20:02:55', '2016-11-29 20:02:55');

-- --------------------------------------------------------

--
-- Table structure for table `tran_rj_header`
--

CREATE TABLE `tran_rj_header` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nota_jual_id` bigint(20) NOT NULL,
  `no_nota` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pelanggan_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `potongan_retur` bigint(20) DEFAULT NULL,
  `total_retur` bigint(20) DEFAULT NULL,
  `catatan` text COLLATE utf8_unicode_ci,
  `total_tagihan` bigint(20) NOT NULL,
  `no_jurnal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_lunas` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tran_rj_header`
--

INSERT INTO `tran_rj_header` (`id`, `nota_jual_id`, `no_nota`, `pelanggan_id`, `tanggal`, `potongan_retur`, `total_retur`, `catatan`, `total_tagihan`, `no_jurnal`, `is_lunas`, `created_at`, `updated_at`) VALUES
(1, 2, 'RJ-201611-00001', 4, '2016-11-29 20:02:55', 0, 5225000, '', 0, NULL, 0, '2016-11-29 20:02:55', '2016-11-29 20:02:55');

-- --------------------------------------------------------

--
-- Table structure for table `tran_service_order`
--

CREATE TABLE `tran_service_order` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `no_nota` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pelanggan_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keluhan` text COLLATE utf8_unicode_ci,
  `deskripsi_penyelesaian` text COLLATE utf8_unicode_ci,
  `tanggal_selesai` datetime DEFAULT NULL,
  `jasa_service` bigint(20) DEFAULT NULL,
  `catatan` text COLLATE utf8_unicode_ci,
  `garansi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_jurnal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tran_service_order`
--

INSERT INTO `tran_service_order` (`id`, `no_nota`, `pelanggan_id`, `tanggal`, `serial_number`, `keluhan`, `deskripsi_penyelesaian`, `tanggal_selesai`, `jasa_service`, `catatan`, `garansi`, `model`, `no_jurnal`, `created_at`, `updated_at`) VALUES
(1, 'SV-2016Y11-00001', 4, '2016-11-29 00:00:00', 'coba', 'dot pixel', NULL, NULL, NULL, '', '29 juli 2017', 'XX-2123', NULL, '2016-11-29 13:06:57', '2016-11-29 13:06:57');

-- --------------------------------------------------------

--
-- Table structure for table `tran_sj_keluar_detail`
--

CREATE TABLE `tran_sj_keluar_detail` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sj_keluar_header_id` bigint(20) NOT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `jenis_barang_id` int(11) DEFAULT NULL,
  `gudang_id` int(11) DEFAULT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `harga` bigint(20) DEFAULT NULL,
  `harga_retail` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tran_sj_keluar_detail`
--

INSERT INTO `tran_sj_keluar_detail` (`id`, `sj_keluar_header_id`, `produk_id`, `jenis_barang_id`, `gudang_id`, `serial_number`, `jumlah`, `harga`, `harga_retail`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, 'DLBK0001', 1, 5225000, 5500000, '2016-11-21 21:45:13', '2016-11-21 21:45:13'),
(2, 1, 1, 1, 1, 'DLBK0002', 1, 5225000, 5500000, '2016-11-21 21:45:13', '2016-11-21 21:45:13'),
(3, 1, 1, 1, 1, 'DLBK0003', 1, 5225000, 5500000, '2016-11-21 21:45:13', '2016-11-21 21:45:13'),
(4, 1, 1, 1, 1, 'DLBK0004', 1, 5225000, 5500000, '2016-11-21 21:45:13', '2016-11-21 21:45:13'),
(5, 2, 1, 1, 2, 'XX-TOKO-BLACK', 2, 5225000, 5500000, '2016-11-29 20:01:33', '2016-11-29 20:01:33'),
(6, 2, 1, 1, 2, 'XX-KANTOR-BLACK', 1, 5225000, 5500000, '2016-11-29 20:01:33', '2016-11-29 20:01:33');

-- --------------------------------------------------------

--
-- Table structure for table `tran_sj_keluar_header`
--

CREATE TABLE `tran_sj_keluar_header` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `so_header_id` bigint(20) NOT NULL,
  `no_surat_jalan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pelanggan_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `flag_nota_jual` int(11) NOT NULL,
  `ppn` int(11) NOT NULL,
  `total_harga` bigint(20) NOT NULL,
  `total_voucher` bigint(20) NOT NULL,
  `total_tagihan` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tran_sj_keluar_header`
--

INSERT INTO `tran_sj_keluar_header` (`id`, `so_header_id`, `no_surat_jalan`, `pelanggan_id`, `tanggal`, `flag_nota_jual`, `ppn`, `total_harga`, `total_voucher`, `total_tagihan`, `created_at`, `updated_at`) VALUES
(1, 1, 'PL-201611-00001', 15, '2016-11-21 21:45:13', 0, 0, 0, 0, 0, '2016-11-21 21:45:13', '2016-11-21 21:45:13'),
(2, 3, 'SJK-201611-00002', 4, '2016-11-29 20:01:33', 1, 0, 0, 0, 0, '2016-11-29 20:01:33', '2016-11-29 13:02:20');

-- --------------------------------------------------------

--
-- Table structure for table `tran_sj_masuk_detail`
--

CREATE TABLE `tran_sj_masuk_detail` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sj_masuk_header_id` bigint(20) NOT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `jenis_barang_id` int(11) DEFAULT NULL,
  `gudang_id` int(11) DEFAULT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `harga` bigint(20) DEFAULT NULL,
  `harga_retail` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tran_sj_masuk_detail`
--

INSERT INTO `tran_sj_masuk_detail` (`id`, `sj_masuk_header_id`, `produk_id`, `jenis_barang_id`, `gudang_id`, `serial_number`, `jumlah`, `harga`, `harga_retail`, `created_at`, `updated_at`) VALUES
(11, 2, 1, 1, 1, 'DLBK0001', 1, 4000000, NULL, '2016-11-21 21:42:25', '2016-11-21 21:42:25'),
(12, 2, 1, 1, 1, 'DLBK0002', 1, 4000000, NULL, '2016-11-21 21:42:25', '2016-11-21 21:42:25'),
(13, 2, 1, 1, 1, 'DLBK0003', 1, 4000000, NULL, '2016-11-21 21:42:25', '2016-11-21 21:42:25'),
(14, 2, 1, 1, 1, 'DLBK0004', 1, 4000000, NULL, '2016-11-21 21:42:25', '2016-11-21 21:42:25'),
(15, 2, 1, 1, 1, 'DLBK0005', 1, 4000000, NULL, '2016-11-21 21:42:25', '2016-11-21 21:42:25'),
(16, 2, 1, 1, 1, 'DLBK0006', 1, 4000000, NULL, '2016-11-21 21:42:25', '2016-11-21 21:42:25'),
(17, 2, 1, 1, 1, 'DLBK0007', 1, 4000000, NULL, '2016-11-21 21:42:25', '2016-11-21 21:42:25'),
(18, 2, 1, 1, 1, 'DLBK0008', 1, 4000000, NULL, '2016-11-21 21:42:25', '2016-11-21 21:42:25'),
(19, 2, 1, 1, 1, 'DLBK0009', 1, 4000000, NULL, '2016-11-21 21:42:25', '2016-11-21 21:42:25'),
(20, 2, 1, 1, 1, 'DLBK0010', 1, 4000000, NULL, '2016-11-21 21:42:25', '2016-11-21 21:42:25'),
(21, 2, 2, 1, 1, 'DLRD0001', 1, 4000000, NULL, '2016-11-21 21:42:25', '2016-11-21 21:42:25'),
(22, 2, 2, 1, 1, 'DLRD0002', 1, 4000000, NULL, '2016-11-21 21:42:25', '2016-11-21 21:42:25'),
(23, 2, 2, 1, 1, 'DLRD0003', 1, 4000000, NULL, '2016-11-21 21:42:25', '2016-11-21 21:42:25'),
(24, 2, 2, 1, 1, 'DLRD0004', 1, 4000000, NULL, '2016-11-21 21:42:25', '2016-11-21 21:42:25'),
(25, 2, 2, 1, 1, 'DLRD0005', 1, 4000000, NULL, '2016-11-21 21:42:25', '2016-11-21 21:42:25'),
(26, 2, 2, 1, 1, 'DLRD0006', 1, 4000000, NULL, '2016-11-21 21:42:25', '2016-11-21 21:42:25'),
(27, 2, 2, 1, 1, 'DLRD0007', 1, 4000000, NULL, '2016-11-21 21:42:25', '2016-11-21 21:42:25'),
(28, 2, 2, 1, 1, 'DLRD0008', 1, 4000000, NULL, '2016-11-21 21:42:25', '2016-11-21 21:42:25'),
(29, 2, 2, 1, 1, 'DLRD0009', 1, 4000000, NULL, '2016-11-21 21:42:25', '2016-11-21 21:42:25'),
(30, 2, 2, 1, 1, 'DLRD0010', 1, 4000000, NULL, '2016-11-21 21:42:25', '2016-11-21 21:42:25'),
(31, 3, 1, 1, 2, 'XX-TOKO-BLACK', 3, 4800000, NULL, '2016-11-29 11:21:36', '2016-11-29 11:21:36'),
(32, 3, 2, 1, 2, 'XX-TOKO-RED', 1, 4800000, NULL, '2016-11-29 11:21:36', '2016-11-29 11:21:36'),
(33, 4, 1, 1, 2, 'XX-KANTOR-BLACK', 2, 4800000, NULL, '2016-11-29 11:25:34', '2016-11-29 11:25:34'),
(34, 5, 7, 1, 1, '2MLSY22', 1, 12950000, NULL, '2016-11-30 14:50:33', '2016-11-30 14:50:33'),
(35, 5, 7, 1, 1, '52NSY22', 1, 12950000, NULL, '2016-11-30 14:50:33', '2016-11-30 14:50:33'),
(36, 5, 7, 1, 1, '5W0TY22', 1, 12950000, NULL, '2016-11-30 14:50:33', '2016-11-30 14:50:33'),
(37, 5, 7, 1, 1, 'C8RSY22', 1, 12950000, NULL, '2016-11-30 14:50:33', '2016-11-30 14:50:33'),
(38, 5, 7, 1, 1, '65VCX02', 1, 12950000, NULL, '2016-11-30 14:50:33', '2016-11-30 14:50:33'),
(39, 5, 7, 1, 1, '3ZXSY22', 1, 12950000, NULL, '2016-11-30 14:50:33', '2016-11-30 14:50:33'),
(40, 5, 7, 1, 1, '7LLSY22', 1, 12950000, NULL, '2016-11-30 14:50:33', '2016-11-30 14:50:33'),
(41, 5, 7, 1, 1, 'D1YSY22', 1, 12950000, NULL, '2016-11-30 14:50:33', '2016-11-30 14:50:33'),
(42, 5, 7, 1, 1, 'B9RSY22', 1, 12950000, NULL, '2016-11-30 14:50:33', '2016-11-30 14:50:33'),
(43, 5, 7, 1, 1, '8V0TY22', 1, 12950000, NULL, '2016-11-30 14:50:33', '2016-11-30 14:50:33'),
(44, 5, 7, 1, 1, 'BYMSY22', 1, 12950000, NULL, '2016-11-30 14:50:33', '2016-11-30 14:50:33'),
(45, 5, 7, 1, 1, 'JFLSY22', 1, 12950000, NULL, '2016-11-30 14:50:33', '2016-11-30 14:50:33'),
(46, 5, 7, 1, 1, 'HW0TY22', 1, 12950000, NULL, '2016-11-30 14:50:33', '2016-11-30 14:50:33'),
(47, 5, 7, 1, 1, 'J5NSY22', 1, 12950000, NULL, '2016-11-30 14:50:33', '2016-11-30 14:50:33'),
(48, 5, 7, 1, 1, '9TMSY22', 1, 12950000, NULL, '2016-11-30 14:50:33', '2016-11-30 14:50:33');

-- --------------------------------------------------------

--
-- Table structure for table `tran_sj_masuk_header`
--

CREATE TABLE `tran_sj_masuk_header` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `po_header_id` bigint(20) NOT NULL,
  `no_surat_jalan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `flag_nota_beli` int(11) NOT NULL,
  `ppn` int(11) NOT NULL,
  `total_harga` bigint(20) NOT NULL,
  `total_diskon` bigint(20) NOT NULL,
  `total_tagihan` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tran_sj_masuk_header`
--

INSERT INTO `tran_sj_masuk_header` (`id`, `po_header_id`, `no_surat_jalan`, `supplier_id`, `tanggal`, `flag_nota_beli`, `ppn`, `total_harga`, `total_diskon`, `total_tagihan`, `created_at`, `updated_at`) VALUES
(2, 3, 'SJM-201611-00001', 1, '2016-11-21 21:42:25', 0, 0, 0, 0, 0, '2016-11-21 21:42:25', '2016-11-21 21:42:25'),
(3, 4, 'SJM-201611-00003', 1, '2016-11-29 11:21:36', 0, 0, 0, 0, 0, '2016-11-29 11:21:36', '2016-11-29 11:21:36'),
(4, 4, 'SJM-201611-00004', 1, '2016-11-29 11:25:34', 0, 0, 0, 0, 0, '2016-11-29 11:25:34', '2016-11-29 11:25:34'),
(5, 5, 'SJM-201611-00005', 4, '2016-11-30 14:50:33', 1, 0, 0, 0, 0, '2016-11-30 14:50:33', '2016-11-30 07:51:29');

-- --------------------------------------------------------

--
-- Table structure for table `tran_slider_promo`
--

CREATE TABLE `tran_slider_promo` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_gambar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `list_produk` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tran_slider_promo`
--

INSERT INTO `tran_slider_promo` (`id`, `nama`, `url`, `file_gambar`, `list_produk`, `item_order`, `created_at`, `updated_at`) VALUES
(1, 'All In One', 'all-in-one', 'sp-mdgj-all-in-one.jpg', '', 1, '2016-12-03 07:15:56', '2016-12-03 07:15:56'),
(2, 'Promo Dell', 'promo-dell', 'sp-r86l-promo-dell.jpg', '1,2,4,3,5,6', 2, '2016-12-03 07:16:50', '2016-12-03 07:16:50');

-- --------------------------------------------------------

--
-- Table structure for table `tran_so_alamat_pengiriman`
--

CREATE TABLE `tran_so_alamat_pengiriman` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `so_header_id` bigint(20) NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alamat` text COLLATE utf8_unicode_ci,
  `kota_id` int(11) DEFAULT NULL,
  `kode_pos` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metode_pengiriman_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tran_so_alamat_pengiriman`
--

INSERT INTO `tran_so_alamat_pengiriman` (`id`, `so_header_id`, `nama`, `email`, `alamat`, `kota_id`, `kode_pos`, `hp`, `metode_pengiriman_id`, `created_at`, `updated_at`) VALUES
(1, 1, 'Muhammad Riduwan', 'ridwan.yaa71@gmail.com', 'keputih Gg Makam Blok D-11, Sukolilo', 264, '60111', '(+62)857-3511-2973', NULL, '2016-11-21 21:44:39', '2016-11-21 21:44:39'),
(2, 2, 'Muhammad Riduwan', 'ridwan.yaa71@gmail.com', 'keputih Gg Makam Blok D-11, Sukolilo', 264, '60111', '(+62)857-3511-2973', 1, '2016-11-25 18:40:15', '2016-11-25 18:40:15'),
(3, 3, 'Datamart Computer', 'dodyformello@gmail.com', 'Berbek Industri II/18', 242, '61256', '(+62)812-3278-760', NULL, '2016-11-29 20:00:20', '2016-11-29 20:00:20');

-- --------------------------------------------------------

--
-- Table structure for table `tran_so_detail`
--

CREATE TABLE `tran_so_detail` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `so_header_id` bigint(20) NOT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `jenis_barang_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `jumlah_terkirim` int(11) DEFAULT NULL,
  `deskripsi` text COLLATE utf8_unicode_ci,
  `harga` bigint(20) DEFAULT NULL,
  `harga_retail` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tran_so_detail`
--

INSERT INTO `tran_so_detail` (`id`, `so_header_id`, `produk_id`, `jenis_barang_id`, `jumlah`, `jumlah_terkirim`, `deskripsi`, `harga`, `harga_retail`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '1', 4, 4, '<p>DELL INSPIRON 3458 I3-5005 UMA UBT - BLACK</p>', 5225000, 5500000, '2016-11-21 21:44:39', '2016-11-21 21:45:13'),
(2, 2, 2, '1', 1, NULL, '<p>DELL INSPIRON 3458 I3-5005 UMA UBT -&nbsp;RED</p>', 5225000, NULL, '2016-11-25 18:40:15', '2016-11-25 18:40:15'),
(3, 3, 1, '1', 3, 3, '<p>DELL INSPIRON 3458 I3-5005 UMA UBT - BLACK</p>', 5225000, 5500000, '2016-11-29 20:00:20', '2016-11-29 20:01:33');

-- --------------------------------------------------------

--
-- Table structure for table `tran_so_header`
--

CREATE TABLE `tran_so_header` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `no_sales_order` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pelanggan_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `due_date` datetime DEFAULT NULL,
  `catatan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_tagihan` bigint(20) DEFAULT NULL,
  `flag_sj_keluar` int(11) DEFAULT NULL,
  `ppn` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tran_so_header`
--

INSERT INTO `tran_so_header` (`id`, `no_sales_order`, `pelanggan_id`, `tanggal`, `due_date`, `catatan`, `total_tagihan`, `flag_sj_keluar`, `ppn`, `created_at`, `updated_at`) VALUES
(1, 'SV-201611-00001', 15, '2016-11-21 21:44:39', NULL, 'pembelian 4 unit pembayaran tunai', 20900000, 1, 0, '2016-11-21 21:44:39', '2016-11-21 21:45:13'),
(2, 'SV-201611-00002', 15, '2016-11-25 18:40:15', NULL, '', 5225000, 0, 0, '2016-11-25 18:40:15', '2016-11-25 18:40:15'),
(3, 'SO-201611-00003', 4, '2016-11-29 20:00:20', NULL, 'kirim cepat', 15675000, 1, 0, '2016-11-29 20:00:20', '2016-11-29 20:01:33');

-- --------------------------------------------------------

--
-- Table structure for table `tran_so_pembayaran`
--

CREATE TABLE `tran_so_pembayaran` (
  `id` int(10) UNSIGNED NOT NULL,
  `so_header_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `metode_pembayaran_id` int(11) DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `nomor_pembayaran` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nominal` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tran_so_pembayaran`
--

INSERT INTO `tran_so_pembayaran` (`id`, `so_header_id`, `tanggal`, `metode_pembayaran_id`, `bank_id`, `nomor_pembayaran`, `nominal`, `created_at`, `updated_at`) VALUES
(1, 2, NULL, 6, NULL, NULL, NULL, '2016-11-25 18:40:15', '2016-11-25 18:40:15');

-- --------------------------------------------------------

--
-- Table structure for table `tran_so_voucher`
--

CREATE TABLE `tran_so_voucher` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `so_header_id` bigint(20) NOT NULL,
  `voucher_id` bigint(20) NOT NULL,
  `nominal` bigint(20) DEFAULT NULL,
  `is_dipakai` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_stok_opname`
--

CREATE TABLE `tran_stok_opname` (
  `id` int(10) UNSIGNED NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `tanggal` datetime DEFAULT NULL,
  `total_stok` int(11) DEFAULT NULL,
  `approval_by_user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_stok_opname_detail`
--

CREATE TABLE `tran_stok_opname_detail` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `stok_opname_header_id` int(11) NOT NULL,
  `no_stok_opname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `produk_id` int(11) NOT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jumlah` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_stok_opname_header`
--

CREATE TABLE `tran_stok_opname_header` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `no_stok_opname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `tanggal` datetime DEFAULT NULL,
  `total_stok` int(11) DEFAULT NULL,
  `approval_by_user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `received_by_user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tref_bank`
--

CREATE TABLE `tref_bank` (
  `id` int(10) UNSIGNED NOT NULL,
  `kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nama_singkat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_aktif` int(11) NOT NULL,
  `file_gambar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tref_bank`
--

INSERT INTO `tref_bank` (`id`, `kode`, `nama`, `nama_singkat`, `keterangan`, `is_aktif`, `file_gambar`, `created_at`, `updated_at`) VALUES
(1, '014', 'Bank Central Asia', 'BCA', '', 1, 'BANK79C6927Y.png', NULL, '2016-10-17 22:27:13');

-- --------------------------------------------------------

--
-- Table structure for table `tref_jenis_barang`
--

CREATE TABLE `tref_jenis_barang` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tref_jenis_barang`
--

INSERT INTO `tref_jenis_barang` (`id`, `nama`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 'produk', 'produk pada umumnya', NULL, NULL),
(2, 'hadiah', 'produk yang dijadikan hadiah untuk pelanggan', NULL, NULL),
(3, 'paket produk', 'kumpulan produk yang dijual menjadi satu paket', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tref_konten`
--

CREATE TABLE `tref_konten` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keterangan` longtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tref_kota`
--

CREATE TABLE `tref_kota` (
  `id` int(10) UNSIGNED NOT NULL,
  `kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provinsi_kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tref_kota`
--

INSERT INTO `tref_kota` (`id`, `kode`, `provinsi_kode`, `nama`, `created_at`, `updated_at`) VALUES
(1, '1101', '11', 'KABUPATEN SIMEULUE', NULL, NULL),
(2, '1102', '11', 'KABUPATEN ACEH SINGKIL', NULL, NULL),
(3, '1103', '11', 'KABUPATEN ACEH SELATAN', NULL, NULL),
(4, '1104', '11', 'KABUPATEN ACEH TENGGARA', NULL, NULL),
(5, '1105', '11', 'KABUPATEN ACEH TIMUR', NULL, NULL),
(6, '1106', '11', 'KABUPATEN ACEH TENGAH', NULL, NULL),
(7, '1107', '11', 'KABUPATEN ACEH BARAT', NULL, NULL),
(8, '1108', '11', 'KABUPATEN ACEH BESAR', NULL, NULL),
(9, '1109', '11', 'KABUPATEN PIDIE', NULL, NULL),
(10, '1110', '11', 'KABUPATEN BIREUEN', NULL, NULL),
(11, '1111', '11', 'KABUPATEN ACEH UTARA', NULL, NULL),
(12, '1112', '11', 'KABUPATEN ACEH BARAT DAYA', NULL, NULL),
(13, '1113', '11', 'KABUPATEN GAYO LUES', NULL, NULL),
(14, '1114', '11', 'KABUPATEN ACEH TAMIANG', NULL, NULL),
(15, '1115', '11', 'KABUPATEN NAGAN RAYA', NULL, NULL),
(16, '1116', '11', 'KABUPATEN ACEH JAYA', NULL, NULL),
(17, '1117', '11', 'KABUPATEN BENER MERIAH', NULL, NULL),
(18, '1118', '11', 'KABUPATEN PIDIE JAYA', NULL, NULL),
(19, '1171', '11', 'KOTA BANDA ACEH', NULL, NULL),
(20, '1172', '11', 'KOTA SABANG', NULL, NULL),
(21, '1173', '11', 'KOTA LANGSA', NULL, NULL),
(22, '1174', '11', 'KOTA LHOKSEUMAWE', NULL, NULL),
(23, '1175', '11', 'KOTA SUBULUSSALAM', NULL, NULL),
(24, '1201', '12', 'KABUPATEN NIAS', NULL, NULL),
(25, '1202', '12', 'KABUPATEN MANDAILING NATAL', NULL, NULL),
(26, '1203', '12', 'KABUPATEN TAPANULI SELATAN', NULL, NULL),
(27, '1204', '12', 'KABUPATEN TAPANULI TENGAH', NULL, NULL),
(28, '1205', '12', 'KABUPATEN TAPANULI UTARA', NULL, NULL),
(29, '1206', '12', 'KABUPATEN TOBA SAMOSIR', NULL, NULL),
(30, '1207', '12', 'KABUPATEN LABUHAN BATU', NULL, NULL),
(31, '1208', '12', 'KABUPATEN ASAHAN', NULL, NULL),
(32, '1209', '12', 'KABUPATEN SIMALUNGUN', NULL, NULL),
(33, '1210', '12', 'KABUPATEN DAIRI', NULL, NULL),
(34, '1211', '12', 'KABUPATEN KARO', NULL, NULL),
(35, '1212', '12', 'KABUPATEN DELI SERDANG', NULL, NULL),
(36, '1213', '12', 'KABUPATEN LANGKAT', NULL, NULL),
(37, '1214', '12', 'KABUPATEN NIAS SELATAN', NULL, NULL),
(38, '1215', '12', 'KABUPATEN HUMBANG HASUNDUTAN', NULL, NULL),
(39, '1216', '12', 'KABUPATEN PAKPAK BHARAT', NULL, NULL),
(40, '1217', '12', 'KABUPATEN SAMOSIR', NULL, NULL),
(41, '1218', '12', 'KABUPATEN SERDANG BEDAGAI', NULL, NULL),
(42, '1219', '12', 'KABUPATEN BATU BARA', NULL, NULL),
(43, '1220', '12', 'KABUPATEN PADANG LAWAS UTARA', NULL, NULL),
(44, '1221', '12', 'KABUPATEN PADANG LAWAS', NULL, NULL),
(45, '1222', '12', 'KABUPATEN LABUHAN BATU SELATAN', NULL, NULL),
(46, '1223', '12', 'KABUPATEN LABUHAN BATU UTARA', NULL, NULL),
(47, '1224', '12', 'KABUPATEN NIAS UTARA', NULL, NULL),
(48, '1225', '12', 'KABUPATEN NIAS BARAT', NULL, NULL),
(49, '1271', '12', 'KOTA SIBOLGA', NULL, NULL),
(50, '1272', '12', 'KOTA TANJUNG BALAI', NULL, NULL),
(51, '1273', '12', 'KOTA PEMATANG SIANTAR', NULL, NULL),
(52, '1274', '12', 'KOTA TEBING TINGGI', NULL, NULL),
(53, '1275', '12', 'KOTA MEDAN', NULL, NULL),
(54, '1276', '12', 'KOTA BINJAI', NULL, NULL),
(55, '1277', '12', 'KOTA PADANGSIDIMPUAN', NULL, NULL),
(56, '1278', '12', 'KOTA GUNUNGSITOLI', NULL, NULL),
(57, '1301', '13', 'KABUPATEN KEPULAUAN MENTAWAI', NULL, NULL),
(58, '1302', '13', 'KABUPATEN PESISIR SELATAN', NULL, NULL),
(59, '1303', '13', 'KABUPATEN SOLOK', NULL, NULL),
(60, '1304', '13', 'KABUPATEN SIJUNJUNG', NULL, NULL),
(61, '1305', '13', 'KABUPATEN TANAH DATAR', NULL, NULL),
(62, '1306', '13', 'KABUPATEN PADANG PARIAMAN', NULL, NULL),
(63, '1307', '13', 'KABUPATEN AGAM', NULL, NULL),
(64, '1308', '13', 'KABUPATEN LIMA PULUH KOTA', NULL, NULL),
(65, '1309', '13', 'KABUPATEN PASAMAN', NULL, NULL),
(66, '1310', '13', 'KABUPATEN SOLOK SELATAN', NULL, NULL),
(67, '1311', '13', 'KABUPATEN DHARMASRAYA', NULL, NULL),
(68, '1312', '13', 'KABUPATEN PASAMAN BARAT', NULL, NULL),
(69, '1371', '13', 'KOTA PADANG', NULL, NULL),
(70, '1372', '13', 'KOTA SOLOK', NULL, NULL),
(71, '1373', '13', 'KOTA SAWAH LUNTO', NULL, NULL),
(72, '1374', '13', 'KOTA PADANG PANJANG', NULL, NULL),
(73, '1375', '13', 'KOTA BUKITTINGGI', NULL, NULL),
(74, '1376', '13', 'KOTA PAYAKUMBUH', NULL, NULL),
(75, '1377', '13', 'KOTA PARIAMAN', NULL, NULL),
(76, '1401', '14', 'KABUPATEN KUANTAN SINGINGI', NULL, NULL),
(77, '1402', '14', 'KABUPATEN INDRAGIRI HULU', NULL, NULL),
(78, '1403', '14', 'KABUPATEN INDRAGIRI HILIR', NULL, NULL),
(79, '1404', '14', 'KABUPATEN PELALAWAN', NULL, NULL),
(80, '1405', '14', 'KABUPATEN S I A K', NULL, NULL),
(81, '1406', '14', 'KABUPATEN KAMPAR', NULL, NULL),
(82, '1407', '14', 'KABUPATEN ROKAN HULU', NULL, NULL),
(83, '1408', '14', 'KABUPATEN BENGKALIS', NULL, NULL),
(84, '1409', '14', 'KABUPATEN ROKAN HILIR', NULL, NULL),
(85, '1410', '14', 'KABUPATEN KEPULAUAN MERANTI', NULL, NULL),
(86, '1471', '14', 'KOTA PEKANBARU', NULL, NULL),
(87, '1473', '14', 'KOTA D U M A I', NULL, NULL),
(88, '1501', '15', 'KABUPATEN KERINCI', NULL, NULL),
(89, '1502', '15', 'KABUPATEN MERANGIN', NULL, NULL),
(90, '1503', '15', 'KABUPATEN SAROLANGUN', NULL, NULL),
(91, '1504', '15', 'KABUPATEN BATANG HARI', NULL, NULL),
(92, '1505', '15', 'KABUPATEN MUARO JAMBI', NULL, NULL),
(93, '1506', '15', 'KABUPATEN TANJUNG JABUNG TIMUR', NULL, NULL),
(94, '1507', '15', 'KABUPATEN TANJUNG JABUNG BARAT', NULL, NULL),
(95, '1508', '15', 'KABUPATEN TEBO', NULL, NULL),
(96, '1509', '15', 'KABUPATEN BUNGO', NULL, NULL),
(97, '1571', '15', 'KOTA JAMBI', NULL, NULL),
(98, '1572', '15', 'KOTA SUNGAI PENUH', NULL, NULL),
(99, '1601', '16', 'KABUPATEN OGAN KOMERING ULU', NULL, NULL),
(100, '1602', '16', 'KABUPATEN OGAN KOMERING ILIR', NULL, NULL),
(101, '1603', '16', 'KABUPATEN MUARA ENIM', NULL, NULL),
(102, '1604', '16', 'KABUPATEN LAHAT', NULL, NULL),
(103, '1605', '16', 'KABUPATEN MUSI RAWAS', NULL, NULL),
(104, '1606', '16', 'KABUPATEN MUSI BANYUASIN', NULL, NULL),
(105, '1607', '16', 'KABUPATEN BANYU ASIN', NULL, NULL),
(106, '1608', '16', 'KABUPATEN OGAN KOMERING ULU SELATAN', NULL, NULL),
(107, '1609', '16', 'KABUPATEN OGAN KOMERING ULU TIMUR', NULL, NULL),
(108, '1610', '16', 'KABUPATEN OGAN ILIR', NULL, NULL),
(109, '1611', '16', 'KABUPATEN EMPAT LAWANG', NULL, NULL),
(110, '1612', '16', 'KABUPATEN PENUKAL ABAB LEMATANG ILIR', NULL, NULL),
(111, '1613', '16', 'KABUPATEN MUSI RAWAS UTARA', NULL, NULL),
(112, '1671', '16', 'KOTA PALEMBANG', NULL, NULL),
(113, '1672', '16', 'KOTA PRABUMULIH', NULL, NULL),
(114, '1673', '16', 'KOTA PAGAR ALAM', NULL, NULL),
(115, '1674', '16', 'KOTA LUBUKLINGGAU', NULL, NULL),
(116, '1701', '17', 'KABUPATEN BENGKULU SELATAN', NULL, NULL),
(117, '1702', '17', 'KABUPATEN REJANG LEBONG', NULL, NULL),
(118, '1703', '17', 'KABUPATEN BENGKULU UTARA', NULL, NULL),
(119, '1704', '17', 'KABUPATEN KAUR', NULL, NULL),
(120, '1705', '17', 'KABUPATEN SELUMA', NULL, NULL),
(121, '1706', '17', 'KABUPATEN MUKOMUKO', NULL, NULL),
(122, '1707', '17', 'KABUPATEN LEBONG', NULL, NULL),
(123, '1708', '17', 'KABUPATEN KEPAHIANG', NULL, NULL),
(124, '1709', '17', 'KABUPATEN BENGKULU TENGAH', NULL, NULL),
(125, '1771', '17', 'KOTA BENGKULU', NULL, NULL),
(126, '1801', '18', 'KABUPATEN LAMPUNG BARAT', NULL, NULL),
(127, '1802', '18', 'KABUPATEN TANGGAMUS', NULL, NULL),
(128, '1803', '18', 'KABUPATEN LAMPUNG SELATAN', NULL, NULL),
(129, '1804', '18', 'KABUPATEN LAMPUNG TIMUR', NULL, NULL),
(130, '1805', '18', 'KABUPATEN LAMPUNG TENGAH', NULL, NULL),
(131, '1806', '18', 'KABUPATEN LAMPUNG UTARA', NULL, NULL),
(132, '1807', '18', 'KABUPATEN WAY KANAN', NULL, NULL),
(133, '1808', '18', 'KABUPATEN TULANGBAWANG', NULL, NULL),
(134, '1809', '18', 'KABUPATEN PESAWARAN', NULL, NULL),
(135, '1810', '18', 'KABUPATEN PRINGSEWU', NULL, NULL),
(136, '1811', '18', 'KABUPATEN MESUJI', NULL, NULL),
(137, '1812', '18', 'KABUPATEN TULANG BAWANG BARAT', NULL, NULL),
(138, '1813', '18', 'KABUPATEN PESISIR BARAT', NULL, NULL),
(139, '1871', '18', 'KOTA BANDAR LAMPUNG', NULL, NULL),
(140, '1872', '18', 'KOTA METRO', NULL, NULL),
(141, '1901', '19', 'KABUPATEN BANGKA', NULL, NULL),
(142, '1902', '19', 'KABUPATEN BELITUNG', NULL, NULL),
(143, '1903', '19', 'KABUPATEN BANGKA BARAT', NULL, NULL),
(144, '1904', '19', 'KABUPATEN BANGKA TENGAH', NULL, NULL),
(145, '1905', '19', 'KABUPATEN BANGKA SELATAN', NULL, NULL),
(146, '1906', '19', 'KABUPATEN BELITUNG TIMUR', NULL, NULL),
(147, '1971', '19', 'KOTA PANGKAL PINANG', NULL, NULL),
(148, '2101', '21', 'KABUPATEN KARIMUN', NULL, NULL),
(149, '2102', '21', 'KABUPATEN BINTAN', NULL, NULL),
(150, '2103', '21', 'KABUPATEN NATUNA', NULL, NULL),
(151, '2104', '21', 'KABUPATEN LINGGA', NULL, NULL),
(152, '2105', '21', 'KABUPATEN KEPULAUAN ANAMBAS', NULL, NULL),
(153, '2171', '21', 'KOTA B A T A M', NULL, NULL),
(154, '2172', '21', 'KOTA TANJUNG PINANG', NULL, NULL),
(155, '3101', '31', 'KABUPATEN KEPULAUAN SERIBU', NULL, NULL),
(156, '3171', '31', 'KOTA JAKARTA SELATAN', NULL, NULL),
(157, '3172', '31', 'KOTA JAKARTA TIMUR', NULL, NULL),
(158, '3173', '31', 'KOTA JAKARTA PUSAT', NULL, NULL),
(159, '3174', '31', 'KOTA JAKARTA BARAT', NULL, NULL),
(160, '3175', '31', 'KOTA JAKARTA UTARA', NULL, NULL),
(161, '3201', '32', 'KABUPATEN BOGOR', NULL, NULL),
(162, '3202', '32', 'KABUPATEN SUKABUMI', NULL, NULL),
(163, '3203', '32', 'KABUPATEN CIANJUR', NULL, NULL),
(164, '3204', '32', 'KABUPATEN BANDUNG', NULL, NULL),
(165, '3205', '32', 'KABUPATEN GARUT', NULL, NULL),
(166, '3206', '32', 'KABUPATEN TASIKMALAYA', NULL, NULL),
(167, '3207', '32', 'KABUPATEN CIAMIS', NULL, NULL),
(168, '3208', '32', 'KABUPATEN KUNINGAN', NULL, NULL),
(169, '3209', '32', 'KABUPATEN CIREBON', NULL, NULL),
(170, '3210', '32', 'KABUPATEN MAJALENGKA', NULL, NULL),
(171, '3211', '32', 'KABUPATEN SUMEDANG', NULL, NULL),
(172, '3212', '32', 'KABUPATEN INDRAMAYU', NULL, NULL),
(173, '3213', '32', 'KABUPATEN SUBANG', NULL, NULL),
(174, '3214', '32', 'KABUPATEN PURWAKARTA', NULL, NULL),
(175, '3215', '32', 'KABUPATEN KARAWANG', NULL, NULL),
(176, '3216', '32', 'KABUPATEN BEKASI', NULL, NULL),
(177, '3217', '32', 'KABUPATEN BANDUNG BARAT', NULL, NULL),
(178, '3218', '32', 'KABUPATEN PANGANDARAN', NULL, NULL),
(179, '3271', '32', 'KOTA BOGOR', NULL, NULL),
(180, '3272', '32', 'KOTA SUKABUMI', NULL, NULL),
(181, '3273', '32', 'KOTA BANDUNG', NULL, NULL),
(182, '3274', '32', 'KOTA CIREBON', NULL, NULL),
(183, '3275', '32', 'KOTA BEKASI', NULL, NULL),
(184, '3276', '32', 'KOTA DEPOK', NULL, NULL),
(185, '3277', '32', 'KOTA CIMAHI', NULL, NULL),
(186, '3278', '32', 'KOTA TASIKMALAYA', NULL, NULL),
(187, '3279', '32', 'KOTA BANJAR', NULL, NULL),
(188, '3301', '33', 'KABUPATEN CILACAP', NULL, NULL),
(189, '3302', '33', 'KABUPATEN BANYUMAS', NULL, NULL),
(190, '3303', '33', 'KABUPATEN PURBALINGGA', NULL, NULL),
(191, '3304', '33', 'KABUPATEN BANJARNEGARA', NULL, NULL),
(192, '3305', '33', 'KABUPATEN KEBUMEN', NULL, NULL),
(193, '3306', '33', 'KABUPATEN PURWOREJO', NULL, NULL),
(194, '3307', '33', 'KABUPATEN WONOSOBO', NULL, NULL),
(195, '3308', '33', 'KABUPATEN MAGELANG', NULL, NULL),
(196, '3309', '33', 'KABUPATEN BOYOLALI', NULL, NULL),
(197, '3310', '33', 'KABUPATEN KLATEN', NULL, NULL),
(198, '3311', '33', 'KABUPATEN SUKOHARJO', NULL, NULL),
(199, '3312', '33', 'KABUPATEN WONOGIRI', NULL, NULL),
(200, '3313', '33', 'KABUPATEN KARANGANYAR', NULL, NULL),
(201, '3314', '33', 'KABUPATEN SRAGEN', NULL, NULL),
(202, '3315', '33', 'KABUPATEN GROBOGAN', NULL, NULL),
(203, '3316', '33', 'KABUPATEN BLORA', NULL, NULL),
(204, '3317', '33', 'KABUPATEN REMBANG', NULL, NULL),
(205, '3318', '33', 'KABUPATEN PATI', NULL, NULL),
(206, '3319', '33', 'KABUPATEN KUDUS', NULL, NULL),
(207, '3320', '33', 'KABUPATEN JEPARA', NULL, NULL),
(208, '3321', '33', 'KABUPATEN DEMAK', NULL, NULL),
(209, '3322', '33', 'KABUPATEN SEMARANG', NULL, NULL),
(210, '3323', '33', 'KABUPATEN TEMANGGUNG', NULL, NULL),
(211, '3324', '33', 'KABUPATEN KENDAL', NULL, NULL),
(212, '3325', '33', 'KABUPATEN BATANG', NULL, NULL),
(213, '3326', '33', 'KABUPATEN PEKALONGAN', NULL, NULL),
(214, '3327', '33', 'KABUPATEN PEMALANG', NULL, NULL),
(215, '3328', '33', 'KABUPATEN TEGAL', NULL, NULL),
(216, '3329', '33', 'KABUPATEN BREBES', NULL, NULL),
(217, '3371', '33', 'KOTA MAGELANG', NULL, NULL),
(218, '3372', '33', 'KOTA SURAKARTA', NULL, NULL),
(219, '3373', '33', 'KOTA SALATIGA', NULL, NULL),
(220, '3374', '33', 'KOTA SEMARANG', NULL, NULL),
(221, '3375', '33', 'KOTA PEKALONGAN', NULL, NULL),
(222, '3376', '33', 'KOTA TEGAL', NULL, NULL),
(223, '3401', '34', 'KABUPATEN KULON PROGO', NULL, NULL),
(224, '3402', '34', 'KABUPATEN BANTUL', NULL, NULL),
(225, '3403', '34', 'KABUPATEN GUNUNG KIDUL', NULL, NULL),
(226, '3404', '34', 'KABUPATEN SLEMAN', NULL, NULL),
(227, '3471', '34', 'KOTA YOGYAKARTA', NULL, NULL),
(228, '3501', '35', 'KABUPATEN PACITAN', NULL, NULL),
(229, '3502', '35', 'KABUPATEN PONOROGO', NULL, NULL),
(230, '3503', '35', 'KABUPATEN TRENGGALEK', NULL, NULL),
(231, '3504', '35', 'KABUPATEN TULUNGAGUNG', NULL, NULL),
(232, '3505', '35', 'KABUPATEN BLITAR', NULL, NULL),
(233, '3506', '35', 'KABUPATEN KEDIRI', NULL, NULL),
(234, '3507', '35', 'KABUPATEN MALANG', NULL, NULL),
(235, '3508', '35', 'KABUPATEN LUMAJANG', NULL, NULL),
(236, '3509', '35', 'KABUPATEN JEMBER', NULL, NULL),
(237, '3510', '35', 'KABUPATEN BANYUWANGI', NULL, NULL),
(238, '3511', '35', 'KABUPATEN BONDOWOSO', NULL, NULL),
(239, '3512', '35', 'KABUPATEN SITUBONDO', NULL, NULL),
(240, '3513', '35', 'KABUPATEN PROBOLINGGO', NULL, NULL),
(241, '3514', '35', 'KABUPATEN PASURUAN', NULL, NULL),
(242, '3515', '35', 'KABUPATEN SIDOARJO', NULL, NULL),
(243, '3516', '35', 'KABUPATEN MOJOKERTO', NULL, NULL),
(244, '3517', '35', 'KABUPATEN JOMBANG', NULL, NULL),
(245, '3518', '35', 'KABUPATEN NGANJUK', NULL, NULL),
(246, '3519', '35', 'KABUPATEN MADIUN', NULL, NULL),
(247, '3520', '35', 'KABUPATEN MAGETAN', NULL, NULL),
(248, '3521', '35', 'KABUPATEN NGAWI', NULL, NULL),
(249, '3522', '35', 'KABUPATEN BOJONEGORO', NULL, NULL),
(250, '3523', '35', 'KABUPATEN TUBAN', NULL, NULL),
(251, '3524', '35', 'KABUPATEN LAMONGAN', NULL, NULL),
(252, '3525', '35', 'KABUPATEN GRESIK', NULL, NULL),
(253, '3526', '35', 'KABUPATEN BANGKALAN', NULL, NULL),
(254, '3527', '35', 'KABUPATEN SAMPANG', NULL, NULL),
(255, '3528', '35', 'KABUPATEN PAMEKASAN', NULL, NULL),
(256, '3529', '35', 'KABUPATEN SUMENEP', NULL, NULL),
(257, '3571', '35', 'KOTA KEDIRI', NULL, NULL),
(258, '3572', '35', 'KOTA BLITAR', NULL, NULL),
(259, '3573', '35', 'KOTA MALANG', NULL, NULL),
(260, '3574', '35', 'KOTA PROBOLINGGO', NULL, NULL),
(261, '3575', '35', 'KOTA PASURUAN', NULL, NULL),
(262, '3576', '35', 'KOTA MOJOKERTO', NULL, NULL),
(263, '3577', '35', 'KOTA MADIUN', NULL, NULL),
(264, '3578', '35', 'KOTA SURABAYA', NULL, NULL),
(265, '3579', '35', 'KOTA BATU', NULL, NULL),
(266, '3601', '36', 'KABUPATEN PANDEGLANG', NULL, NULL),
(267, '3602', '36', 'KABUPATEN LEBAK', NULL, NULL),
(268, '3603', '36', 'KABUPATEN TANGERANG', NULL, NULL),
(269, '3604', '36', 'KABUPATEN SERANG', NULL, NULL),
(270, '3671', '36', 'KOTA TANGERANG', NULL, NULL),
(271, '3672', '36', 'KOTA CILEGON', NULL, NULL),
(272, '3673', '36', 'KOTA SERANG', NULL, NULL),
(273, '3674', '36', 'KOTA TANGERANG SELATAN', NULL, NULL),
(274, '5101', '51', 'KABUPATEN JEMBRANA', NULL, NULL),
(275, '5102', '51', 'KABUPATEN TABANAN', NULL, NULL),
(276, '5103', '51', 'KABUPATEN BADUNG', NULL, NULL),
(277, '5104', '51', 'KABUPATEN GIANYAR', NULL, NULL),
(278, '5105', '51', 'KABUPATEN KLUNGKUNG', NULL, NULL),
(279, '5106', '51', 'KABUPATEN BANGLI', NULL, NULL),
(280, '5107', '51', 'KABUPATEN KARANG ASEM', NULL, NULL),
(281, '5108', '51', 'KABUPATEN BULELENG', NULL, NULL),
(282, '5171', '51', 'KOTA DENPASAR', NULL, NULL),
(283, '5201', '52', 'KABUPATEN LOMBOK BARAT', NULL, NULL),
(284, '5202', '52', 'KABUPATEN LOMBOK TENGAH', NULL, NULL),
(285, '5203', '52', 'KABUPATEN LOMBOK TIMUR', NULL, NULL),
(286, '5204', '52', 'KABUPATEN SUMBAWA', NULL, NULL),
(287, '5205', '52', 'KABUPATEN DOMPU', NULL, NULL),
(288, '5206', '52', 'KABUPATEN BIMA', NULL, NULL),
(289, '5207', '52', 'KABUPATEN SUMBAWA BARAT', NULL, NULL),
(290, '5208', '52', 'KABUPATEN LOMBOK UTARA', NULL, NULL),
(291, '5271', '52', 'KOTA MATARAM', NULL, NULL),
(292, '5272', '52', 'KOTA BIMA', NULL, NULL),
(293, '5301', '53', 'KABUPATEN SUMBA BARAT', NULL, NULL),
(294, '5302', '53', 'KABUPATEN SUMBA TIMUR', NULL, NULL),
(295, '5303', '53', 'KABUPATEN KUPANG', NULL, NULL),
(296, '5304', '53', 'KABUPATEN TIMOR TENGAH SELATAN', NULL, NULL),
(297, '5305', '53', 'KABUPATEN TIMOR TENGAH UTARA', NULL, NULL),
(298, '5306', '53', 'KABUPATEN BELU', NULL, NULL),
(299, '5307', '53', 'KABUPATEN ALOR', NULL, NULL),
(300, '5308', '53', 'KABUPATEN LEMBATA', NULL, NULL),
(301, '5309', '53', 'KABUPATEN FLORES TIMUR', NULL, NULL),
(302, '5310', '53', 'KABUPATEN SIKKA', NULL, NULL),
(303, '5311', '53', 'KABUPATEN ENDE', NULL, NULL),
(304, '5312', '53', 'KABUPATEN NGADA', NULL, NULL),
(305, '5313', '53', 'KABUPATEN MANGGARAI', NULL, NULL),
(306, '5314', '53', 'KABUPATEN ROTE NDAO', NULL, NULL),
(307, '5315', '53', 'KABUPATEN MANGGARAI BARAT', NULL, NULL),
(308, '5316', '53', 'KABUPATEN SUMBA TENGAH', NULL, NULL),
(309, '5317', '53', 'KABUPATEN SUMBA BARAT DAYA', NULL, NULL),
(310, '5318', '53', 'KABUPATEN NAGEKEO', NULL, NULL),
(311, '5319', '53', 'KABUPATEN MANGGARAI TIMUR', NULL, NULL),
(312, '5320', '53', 'KABUPATEN SABU RAIJUA', NULL, NULL),
(313, '5321', '53', 'KABUPATEN MALAKA', NULL, NULL),
(314, '5371', '53', 'KOTA KUPANG', NULL, NULL),
(315, '6101', '61', 'KABUPATEN SAMBAS', NULL, NULL),
(316, '6102', '61', 'KABUPATEN BENGKAYANG', NULL, NULL),
(317, '6103', '61', 'KABUPATEN LANDAK', NULL, NULL),
(318, '6104', '61', 'KABUPATEN MEMPAWAH', NULL, NULL),
(319, '6105', '61', 'KABUPATEN SANGGAU', NULL, NULL),
(320, '6106', '61', 'KABUPATEN KETAPANG', NULL, NULL),
(321, '6107', '61', 'KABUPATEN SINTANG', NULL, NULL),
(322, '6108', '61', 'KABUPATEN KAPUAS HULU', NULL, NULL),
(323, '6109', '61', 'KABUPATEN SEKADAU', NULL, NULL),
(324, '6110', '61', 'KABUPATEN MELAWI', NULL, NULL),
(325, '6111', '61', 'KABUPATEN KAYONG UTARA', NULL, NULL),
(326, '6112', '61', 'KABUPATEN KUBU RAYA', NULL, NULL),
(327, '6171', '61', 'KOTA PONTIANAK', NULL, NULL),
(328, '6172', '61', 'KOTA SINGKAWANG', NULL, NULL),
(329, '6201', '62', 'KABUPATEN KOTAWARINGIN BARAT', NULL, NULL),
(330, '6202', '62', 'KABUPATEN KOTAWARINGIN TIMUR', NULL, NULL),
(331, '6203', '62', 'KABUPATEN KAPUAS', NULL, NULL),
(332, '6204', '62', 'KABUPATEN BARITO SELATAN', NULL, NULL),
(333, '6205', '62', 'KABUPATEN BARITO UTARA', NULL, NULL),
(334, '6206', '62', 'KABUPATEN SUKAMARA', NULL, NULL),
(335, '6207', '62', 'KABUPATEN LAMANDAU', NULL, NULL),
(336, '6208', '62', 'KABUPATEN SERUYAN', NULL, NULL),
(337, '6209', '62', 'KABUPATEN KATINGAN', NULL, NULL),
(338, '6210', '62', 'KABUPATEN PULANG PISAU', NULL, NULL),
(339, '6211', '62', 'KABUPATEN GUNUNG MAS', NULL, NULL),
(340, '6212', '62', 'KABUPATEN BARITO TIMUR', NULL, NULL),
(341, '6213', '62', 'KABUPATEN MURUNG RAYA', NULL, NULL),
(342, '6271', '62', 'KOTA PALANGKA RAYA', NULL, NULL),
(343, '6301', '63', 'KABUPATEN TANAH LAUT', NULL, NULL),
(344, '6302', '63', 'KABUPATEN KOTA BARU', NULL, NULL),
(345, '6303', '63', 'KABUPATEN BANJAR', NULL, NULL),
(346, '6304', '63', 'KABUPATEN BARITO KUALA', NULL, NULL),
(347, '6305', '63', 'KABUPATEN TAPIN', NULL, NULL),
(348, '6306', '63', 'KABUPATEN HULU SUNGAI SELATAN', NULL, NULL),
(349, '6307', '63', 'KABUPATEN HULU SUNGAI TENGAH', NULL, NULL),
(350, '6308', '63', 'KABUPATEN HULU SUNGAI UTARA', NULL, NULL),
(351, '6309', '63', 'KABUPATEN TABALONG', NULL, NULL),
(352, '6310', '63', 'KABUPATEN TANAH BUMBU', NULL, NULL),
(353, '6311', '63', 'KABUPATEN BALANGAN', NULL, NULL),
(354, '6371', '63', 'KOTA BANJARMASIN', NULL, NULL),
(355, '6372', '63', 'KOTA BANJAR BARU', NULL, NULL),
(356, '6401', '64', 'KABUPATEN PASER', NULL, NULL),
(357, '6402', '64', 'KABUPATEN KUTAI BARAT', NULL, NULL),
(358, '6403', '64', 'KABUPATEN KUTAI KARTANEGARA', NULL, NULL),
(359, '6404', '64', 'KABUPATEN KUTAI TIMUR', NULL, NULL),
(360, '6405', '64', 'KABUPATEN BERAU', NULL, NULL),
(361, '6409', '64', 'KABUPATEN PENAJAM PASER UTARA', NULL, NULL),
(362, '6411', '64', 'KABUPATEN MAHAKAM HULU', NULL, NULL),
(363, '6471', '64', 'KOTA BALIKPAPAN', NULL, NULL),
(364, '6472', '64', 'KOTA SAMARINDA', NULL, NULL),
(365, '6474', '64', 'KOTA BONTANG', NULL, NULL),
(366, '6501', '65', 'KABUPATEN MALINAU', NULL, NULL),
(367, '6502', '65', 'KABUPATEN BULUNGAN', NULL, NULL),
(368, '6503', '65', 'KABUPATEN TANA TIDUNG', NULL, NULL),
(369, '6504', '65', 'KABUPATEN NUNUKAN', NULL, NULL),
(370, '6571', '65', 'KOTA TARAKAN', NULL, NULL),
(371, '7101', '71', 'KABUPATEN BOLAANG MONGONDOW', NULL, NULL),
(372, '7102', '71', 'KABUPATEN MINAHASA', NULL, NULL),
(373, '7103', '71', 'KABUPATEN KEPULAUAN SANGIHE', NULL, NULL),
(374, '7104', '71', 'KABUPATEN KEPULAUAN TALAUD', NULL, NULL),
(375, '7105', '71', 'KABUPATEN MINAHASA SELATAN', NULL, NULL),
(376, '7106', '71', 'KABUPATEN MINAHASA UTARA', NULL, NULL),
(377, '7107', '71', 'KABUPATEN BOLAANG MONGONDOW UTARA', NULL, NULL),
(378, '7108', '71', 'KABUPATEN SIAU TAGULANDANG BIARO', NULL, NULL),
(379, '7109', '71', 'KABUPATEN MINAHASA TENGGARA', NULL, NULL),
(380, '7110', '71', 'KABUPATEN BOLAANG MONGONDOW SELATAN', NULL, NULL),
(381, '7111', '71', 'KABUPATEN BOLAANG MONGONDOW TIMUR', NULL, NULL),
(382, '7171', '71', 'KOTA MANADO', NULL, NULL),
(383, '7172', '71', 'KOTA BITUNG', NULL, NULL),
(384, '7173', '71', 'KOTA TOMOHON', NULL, NULL),
(385, '7174', '71', 'KOTA KOTAMOBAGU', NULL, NULL),
(386, '7201', '72', 'KABUPATEN BANGGAI KEPULAUAN', NULL, NULL),
(387, '7202', '72', 'KABUPATEN BANGGAI', NULL, NULL),
(388, '7203', '72', 'KABUPATEN MOROWALI', NULL, NULL),
(389, '7204', '72', 'KABUPATEN POSO', NULL, NULL),
(390, '7205', '72', 'KABUPATEN DONGGALA', NULL, NULL),
(391, '7206', '72', 'KABUPATEN TOLI-TOLI', NULL, NULL),
(392, '7207', '72', 'KABUPATEN BUOL', NULL, NULL),
(393, '7208', '72', 'KABUPATEN PARIGI MOUTONG', NULL, NULL),
(394, '7209', '72', 'KABUPATEN TOJO UNA-UNA', NULL, NULL),
(395, '7210', '72', 'KABUPATEN SIGI', NULL, NULL),
(396, '7211', '72', 'KABUPATEN BANGGAI LAUT', NULL, NULL),
(397, '7212', '72', 'KABUPATEN MOROWALI UTARA', NULL, NULL),
(398, '7271', '72', 'KOTA PALU', NULL, NULL),
(399, '7301', '73', 'KABUPATEN KEPULAUAN SELAYAR', NULL, NULL),
(400, '7302', '73', 'KABUPATEN BULUKUMBA', NULL, NULL),
(401, '7303', '73', 'KABUPATEN BANTAENG', NULL, NULL),
(402, '7304', '73', 'KABUPATEN JENEPONTO', NULL, NULL),
(403, '7305', '73', 'KABUPATEN TAKALAR', NULL, NULL),
(404, '7306', '73', 'KABUPATEN GOWA', NULL, NULL),
(405, '7307', '73', 'KABUPATEN SINJAI', NULL, NULL),
(406, '7308', '73', 'KABUPATEN MAROS', NULL, NULL),
(407, '7309', '73', 'KABUPATEN PANGKAJENE DAN KEPULAUAN', NULL, NULL),
(408, '7310', '73', 'KABUPATEN BARRU', NULL, NULL),
(409, '7311', '73', 'KABUPATEN BONE', NULL, NULL),
(410, '7312', '73', 'KABUPATEN SOPPENG', NULL, NULL),
(411, '7313', '73', 'KABUPATEN WAJO', NULL, NULL),
(412, '7314', '73', 'KABUPATEN SIDENRENG RAPPANG', NULL, NULL),
(413, '7315', '73', 'KABUPATEN PINRANG', NULL, NULL),
(414, '7316', '73', 'KABUPATEN ENREKANG', NULL, NULL),
(415, '7317', '73', 'KABUPATEN LUWU', NULL, NULL),
(416, '7318', '73', 'KABUPATEN TANA TORAJA', NULL, NULL),
(417, '7322', '73', 'KABUPATEN LUWU UTARA', NULL, NULL),
(418, '7325', '73', 'KABUPATEN LUWU TIMUR', NULL, NULL),
(419, '7326', '73', 'KABUPATEN TORAJA UTARA', NULL, NULL),
(420, '7371', '73', 'KOTA MAKASSAR', NULL, NULL),
(421, '7372', '73', 'KOTA PAREPARE', NULL, NULL),
(422, '7373', '73', 'KOTA PALOPO', NULL, NULL),
(423, '7401', '74', 'KABUPATEN BUTON', NULL, NULL),
(424, '7402', '74', 'KABUPATEN MUNA', NULL, NULL),
(425, '7403', '74', 'KABUPATEN KONAWE', NULL, NULL),
(426, '7404', '74', 'KABUPATEN KOLAKA', NULL, NULL),
(427, '7405', '74', 'KABUPATEN KONAWE SELATAN', NULL, NULL),
(428, '7406', '74', 'KABUPATEN BOMBANA', NULL, NULL),
(429, '7407', '74', 'KABUPATEN WAKATOBI', NULL, NULL),
(430, '7408', '74', 'KABUPATEN KOLAKA UTARA', NULL, NULL),
(431, '7409', '74', 'KABUPATEN BUTON UTARA', NULL, NULL),
(432, '7410', '74', 'KABUPATEN KONAWE UTARA', NULL, NULL),
(433, '7411', '74', 'KABUPATEN KOLAKA TIMUR', NULL, NULL),
(434, '7412', '74', 'KABUPATEN KONAWE KEPULAUAN', NULL, NULL),
(435, '7413', '74', 'KABUPATEN MUNA BARAT', NULL, NULL),
(436, '7414', '74', 'KABUPATEN BUTON TENGAH', NULL, NULL),
(437, '7415', '74', 'KABUPATEN BUTON SELATAN', NULL, NULL),
(438, '7471', '74', 'KOTA KENDARI', NULL, NULL),
(439, '7472', '74', 'KOTA BAUBAU', NULL, NULL),
(440, '7501', '75', 'KABUPATEN BOALEMO', NULL, NULL),
(441, '7502', '75', 'KABUPATEN GORONTALO', NULL, NULL),
(442, '7503', '75', 'KABUPATEN POHUWATO', NULL, NULL),
(443, '7504', '75', 'KABUPATEN BONE BOLANGO', NULL, NULL),
(444, '7505', '75', 'KABUPATEN GORONTALO UTARA', NULL, NULL),
(445, '7571', '75', 'KOTA GORONTALO', NULL, NULL),
(446, '7601', '76', 'KABUPATEN MAJENE', NULL, NULL),
(447, '7602', '76', 'KABUPATEN POLEWALI MANDAR', NULL, NULL),
(448, '7603', '76', 'KABUPATEN MAMASA', NULL, NULL),
(449, '7604', '76', 'KABUPATEN MAMUJU', NULL, NULL),
(450, '7605', '76', 'KABUPATEN MAMUJU UTARA', NULL, NULL),
(451, '7606', '76', 'KABUPATEN MAMUJU TENGAH', NULL, NULL),
(452, '8101', '81', 'KABUPATEN MALUKU TENGGARA BARAT', NULL, NULL),
(453, '8102', '81', 'KABUPATEN MALUKU TENGGARA', NULL, NULL),
(454, '8103', '81', 'KABUPATEN MALUKU TENGAH', NULL, NULL),
(455, '8104', '81', 'KABUPATEN BURU', NULL, NULL),
(456, '8105', '81', 'KABUPATEN KEPULAUAN ARU', NULL, NULL),
(457, '8106', '81', 'KABUPATEN SERAM BAGIAN BARAT', NULL, NULL),
(458, '8107', '81', 'KABUPATEN SERAM BAGIAN TIMUR', NULL, NULL),
(459, '8108', '81', 'KABUPATEN MALUKU BARAT DAYA', NULL, NULL),
(460, '8109', '81', 'KABUPATEN BURU SELATAN', NULL, NULL),
(461, '8171', '81', 'KOTA AMBON', NULL, NULL),
(462, '8172', '81', 'KOTA TUAL', NULL, NULL),
(463, '8201', '82', 'KABUPATEN HALMAHERA BARAT', NULL, NULL),
(464, '8202', '82', 'KABUPATEN HALMAHERA TENGAH', NULL, NULL),
(465, '8203', '82', 'KABUPATEN KEPULAUAN SULA', NULL, NULL),
(466, '8204', '82', 'KABUPATEN HALMAHERA SELATAN', NULL, NULL),
(467, '8205', '82', 'KABUPATEN HALMAHERA UTARA', NULL, NULL),
(468, '8206', '82', 'KABUPATEN HALMAHERA TIMUR', NULL, NULL),
(469, '8207', '82', 'KABUPATEN PULAU MOROTAI', NULL, NULL),
(470, '8208', '82', 'KABUPATEN PULAU TALIABU', NULL, NULL),
(471, '8271', '82', 'KOTA TERNATE', NULL, NULL),
(472, '8272', '82', 'KOTA TIDORE KEPULAUAN', NULL, NULL),
(473, '9101', '91', 'KABUPATEN FAKFAK', NULL, NULL),
(474, '9102', '91', 'KABUPATEN KAIMANA', NULL, NULL),
(475, '9103', '91', 'KABUPATEN TELUK WONDAMA', NULL, NULL),
(476, '9104', '91', 'KABUPATEN TELUK BINTUNI', NULL, NULL),
(477, '9105', '91', 'KABUPATEN MANOKWARI', NULL, NULL),
(478, '9106', '91', 'KABUPATEN SORONG SELATAN', NULL, NULL),
(479, '9107', '91', 'KABUPATEN SORONG', NULL, NULL),
(480, '9108', '91', 'KABUPATEN RAJA AMPAT', NULL, NULL),
(481, '9109', '91', 'KABUPATEN TAMBRAUW', NULL, NULL),
(482, '9110', '91', 'KABUPATEN MAYBRAT', NULL, NULL),
(483, '9111', '91', 'KABUPATEN MANOKWARI SELATAN', NULL, NULL),
(484, '9112', '91', 'KABUPATEN PEGUNUNGAN ARFAK', NULL, NULL),
(485, '9171', '91', 'KOTA SORONG', NULL, NULL),
(486, '9401', '94', 'KABUPATEN MERAUKE', NULL, NULL),
(487, '9402', '94', 'KABUPATEN JAYAWIJAYA', NULL, NULL),
(488, '9403', '94', 'KABUPATEN JAYAPURA', NULL, NULL),
(489, '9404', '94', 'KABUPATEN NABIRE', NULL, NULL),
(490, '9408', '94', 'KABUPATEN KEPULAUAN YAPEN', NULL, NULL),
(491, '9409', '94', 'KABUPATEN BIAK NUMFOR', NULL, NULL),
(492, '9410', '94', 'KABUPATEN PANIAI', NULL, NULL),
(493, '9411', '94', 'KABUPATEN PUNCAK JAYA', NULL, NULL),
(494, '9412', '94', 'KABUPATEN MIMIKA', NULL, NULL),
(495, '9413', '94', 'KABUPATEN BOVEN DIGOEL', NULL, NULL),
(496, '9414', '94', 'KABUPATEN MAPPI', NULL, NULL),
(497, '9415', '94', 'KABUPATEN ASMAT', NULL, NULL),
(498, '9416', '94', 'KABUPATEN YAHUKIMO', NULL, NULL),
(499, '9417', '94', 'KABUPATEN PEGUNUNGAN BINTANG', NULL, NULL),
(500, '9418', '94', 'KABUPATEN TOLIKARA', NULL, NULL),
(501, '9419', '94', 'KABUPATEN SARMI', NULL, NULL),
(502, '9420', '94', 'KABUPATEN KEEROM', NULL, NULL),
(503, '9426', '94', 'KABUPATEN WAROPEN', NULL, NULL),
(504, '9427', '94', 'KABUPATEN SUPIORI', NULL, NULL),
(505, '9428', '94', 'KABUPATEN MAMBERAMO RAYA', NULL, NULL),
(506, '9429', '94', 'KABUPATEN NDUGA', NULL, NULL),
(507, '9430', '94', 'KABUPATEN LANNY JAYA', NULL, NULL),
(508, '9431', '94', 'KABUPATEN MAMBERAMO TENGAH', NULL, NULL),
(509, '9432', '94', 'KABUPATEN YALIMO', NULL, NULL),
(510, '9433', '94', 'KABUPATEN PUNCAK', NULL, NULL),
(511, '9434', '94', 'KABUPATEN DOGIYAI', NULL, NULL),
(512, '9435', '94', 'KABUPATEN INTAN JAYA', NULL, NULL),
(513, '9436', '94', 'KABUPATEN DEIYAI', NULL, NULL),
(514, '9471', '94', 'KOTA JAYAPURA', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tref_metode_pembayaran`
--

CREATE TABLE `tref_metode_pembayaran` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_aktif` int(11) NOT NULL,
  `file_gambar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jenis` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tref_metode_pembayaran`
--

INSERT INTO `tref_metode_pembayaran` (`id`, `nama`, `keterangan`, `is_aktif`, `file_gambar`, `jenis`, `created_at`, `updated_at`) VALUES
(1, 'Tunai', 'pembayaran secara tunai', 1, '', '', NULL, NULL),
(2, 'Transfer', 'pembayaran secara transfer', 1, '', '', NULL, NULL),
(3, 'Kartu Kedit', 'pembayaran menggunakan kartu kredit', 1, '', '', NULL, NULL),
(4, 'Giro', 'pembayaran menggunakan giro', 1, '', '', NULL, NULL),
(5, 'Cek', 'pembayaran menggunakan cek', 1, '', '', NULL, NULL),
(6, 'Debit BCA', 'Pembayaran menggunakan Kartu Debit BCA', 1, '', 'non_tunai', '2016-10-14 05:34:03', '2016-10-14 05:34:03'),
(7, 'Debit ATM Bersama', 'Pembayaran menggunakan Kartu Debit ATM Bersama', 1, '', 'non_tunai', '2016-10-14 05:34:39', '2016-10-14 05:34:39');

-- --------------------------------------------------------

--
-- Table structure for table `tref_metode_pengiriman`
--

CREATE TABLE `tref_metode_pengiriman` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tarif_dasar` bigint(20) DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_aktif` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_gambar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tref_metode_pengiriman`
--

INSERT INTO `tref_metode_pengiriman` (`id`, `nama`, `tarif_dasar`, `keterangan`, `is_aktif`, `file_gambar`, `created_at`, `updated_at`) VALUES
(1, 'reguler', 0, '', '1', '', '2016-10-15 07:09:52', '2016-10-15 07:09:52'),
(2, 'kilat', 0, '', '1', '', '2016-10-15 07:10:03', '2016-10-15 07:10:03');

-- --------------------------------------------------------

--
-- Table structure for table `tref_nama_transaksi`
--

CREATE TABLE `tref_nama_transaksi` (
  `id` bigint(20) NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `transaksi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tref_nama_transaksi`
--

INSERT INTO `tref_nama_transaksi` (`id`, `nama`, `transaksi`, `created_at`, `updated_at`) VALUES
(1, 'Total Harga Beli Nett', 'nota_beli', NULL, NULL),
(2, 'Diskon Pembelian', 'nota_beli', NULL, NULL),
(3, 'Total Harga Jual Nett', 'nota_jual', NULL, NULL),
(4, 'Penjualan (HPP)', 'nota_jual', NULL, NULL),
(5, 'Voucher Jual', 'nota_jual', NULL, NULL),
(6, 'Total Retur Beli', 'retur_beli', NULL, NULL),
(7, 'Potongan Retur Beli', 'retur_beli', NULL, NULL),
(8, 'Total Retur Jual', 'retur_jual', NULL, NULL),
(9, 'Potongan Retur Jual', 'retur_jual', NULL, NULL),
(10, 'Total Jasa', 'jasa_servis', NULL, NULL),
(11, 'Total Pembayaran Supplier', 'pembayaran_supplier', NULL, NULL),
(12, 'Total Pembayaran Pelanggan', 'pembayaran_pelanggan', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tref_nomor_invoice`
--

CREATE TABLE `tref_nomor_invoice` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `format` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tref_nomor_invoice`
--

INSERT INTO `tref_nomor_invoice` (`id`, `nama`, `format`, `created_at`, `updated_at`) VALUES
(1, 'Purchase Order', 'PO-YYYYMM-#####', NULL, NULL),
(2, 'Surat Jalan Masuk', 'SJM-YYYYMM-#####', NULL, NULL),
(3, 'Nota Beli', 'NB-YYYYMM-#####', NULL, NULL),
(4, 'Retur Beli', 'RB-YYYYMM-#####', NULL, NULL),
(5, 'Sales Order', 'SO-YYYYMM-#####', NULL, NULL),
(6, 'Retur Jual', 'RJ-YYYYMM-#####', NULL, NULL),
(7, 'Surat Jalan Keluar', 'SJK-YYYYMM-#####', NULL, NULL),
(8, 'Nota Jual', 'NJ-YYYYMM-#####', NULL, NULL),
(9, 'Service Order', 'SV-YYYYYMM-#####', NULL, NULL),
(10, 'Mutasi Stok', 'MTS/DD/MM/YY/####', NULL, NULL),
(11, 'Stok Opname', 'STO/DD/MM/YY/####', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tref_pengaturan_po`
--

CREATE TABLE `tref_pengaturan_po` (
  `id` int(10) UNSIGNED NOT NULL,
  `alamat_pengiriman` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `syarat_ketentuan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ppn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tref_pengaturan_po`
--

INSERT INTO `tref_pengaturan_po` (`id`, `alamat_pengiriman`, `syarat_ketentuan`, `ppn`, `created_at`, `updated_at`) VALUES
(1, 'Galerindo Teknologi\r\nHi Tech Mall Lt. Dasar Blok.D No.17-19\r\nJl. Kusuma Bangsa No.116-118 Surabaya\r\nT. 031 5477085 | 031 5348998\r\nUp. Bpk. Gondo/ Ibu Vera', '', '', '2016-11-21 14:39:28', '2016-11-21 14:39:28');

-- --------------------------------------------------------

--
-- Table structure for table `tref_pengaturan_so`
--

CREATE TABLE `tref_pengaturan_so` (
  `id` int(10) UNSIGNED NOT NULL,
  `syarat_ketentuan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ppn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tref_provinsi`
--

CREATE TABLE `tref_provinsi` (
  `id` int(10) UNSIGNED NOT NULL,
  `kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tref_provinsi`
--

INSERT INTO `tref_provinsi` (`id`, `kode`, `nama`, `created_at`, `updated_at`) VALUES
(1, '11', 'ACEH', NULL, NULL),
(2, '51', 'BALI', NULL, NULL),
(3, '36', 'BANTEN', NULL, NULL),
(4, '17', 'BENGKULU', NULL, NULL),
(5, '34', 'DI YOGYAKARTA', NULL, NULL),
(6, '31', 'DKI JAKARTA', NULL, NULL),
(7, '75', 'GORONTALO', NULL, NULL),
(8, '15', 'JAMBI', NULL, NULL),
(9, '32', 'JAWA BARAT', NULL, NULL),
(10, '33', 'JAWA TENGAH', NULL, NULL),
(11, '35', 'JAWA TIMUR', NULL, NULL),
(12, '61', 'KALIMANTAN BARAT', NULL, NULL),
(13, '63', 'KALIMANTAN SELATAN', NULL, NULL),
(14, '62', 'KALIMANTAN TENGAH', NULL, NULL),
(15, '64', 'KALIMANTAN TIMUR', NULL, NULL),
(16, '65', 'KALIMANTAN UTARA', NULL, NULL),
(17, '19', 'KEPULAUAN BANGKA BELITUNG', NULL, NULL),
(18, '21', 'KEPULAUAN RIAU', NULL, NULL),
(19, '18', 'LAMPUNG', NULL, NULL),
(20, '81', 'MALUKU', NULL, NULL),
(21, '82', 'MALUKU UTARA', NULL, NULL),
(22, '52', 'NUSA TENGGARA BARAT', NULL, NULL),
(23, '53', 'NUSA TENGGARA TIMUR', NULL, NULL),
(24, '94', 'PAPUA', NULL, NULL),
(25, '91', 'PAPUA BARAT', NULL, NULL),
(26, '14', 'RIAU', NULL, NULL),
(27, '76', 'SULAWESI BARAT', NULL, NULL),
(28, '73', 'SULAWESI SELATAN', NULL, NULL),
(29, '72', 'SULAWESI TENGAH', NULL, NULL),
(30, '74', 'SULAWESI TENGGARA', NULL, NULL),
(31, '71', 'SULAWESI UTARA', NULL, NULL),
(32, '13', 'SUMATERA BARAT', NULL, NULL),
(33, '16', 'SUMATERA SELATAN', NULL, NULL),
(34, '12', 'SUMATERA UTARA', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tref_sumber_data`
--

CREATE TABLE `tref_sumber_data` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tref_sumber_data`
--

INSERT INTO `tref_sumber_data` (`id`, `nama`, `created_at`, `updated_at`) VALUES
(1, 'Surat Jalan Masuk', NULL, NULL),
(2, 'Surat Jalan Keluar', NULL, NULL),
(3, 'Retur Jual', NULL, NULL),
(4, 'Retur Beli', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kota_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kode_pos` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hak_akses_id` int(11) NOT NULL,
  `tanggal_register` datetime NOT NULL,
  `tanggal_unregister` datetime NOT NULL,
  `is_aktif` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `alamat`, `kota_id`, `kode_pos`, `telp`, `hp`, `hak_akses_id`, `tanggal_register`, `tanggal_unregister`, `is_aktif`, `remember_token`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@pos.com', '$2y$10$t2XMDcEsnndbaq/nMKl6GOHSpMARjZXHaFQ.ijp2xAtijMJy5Gph.', 'Surabaya', '264', '61256', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'j9pzKfp1wfOtnIedmBnq1ULZZVHXmAMwuwCazoTKFizyfxD00np0oh6yevrG', NULL, NULL, '2016-12-03 08:12:40'),
(2, 'Silvi Anita Putri', 'silvi.galerindo@gmail.com', '$2y$10$2mxkHSnGCCCY2QGd/5uI5.zf4.qW3yY/5z4qSt9V4xAV776qZOkiu', 'Jl. Nginden Semolo No.42 Blok.B-20', '264', '', '', '(+62)8523-4628857', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL, NULL, NULL),
(4, 'Peter Parker', 'peter@gmail.com', '$2y$10$BvezPN8Rc.Pru8aF5A0dnePIGnDr5f9Y8p48u33VX9jGcXAPHlboW', 'jalan raya nginden semolo', '264', '60111', '031-89898989', '(+62)098-9089-0809', 2, '2016-10-03 08:13:26', '0000-00-00 00:00:00', 1, 'J8knut1zVr4XBZnrH1AM1FUr3L0asXE11EoVRvo1GRXDsqYXcic9F4LzXzsN', NULL, NULL, '2016-10-28 13:31:14'),
(5, 'Barry Alan', 'barry@gmail.com', '$2y$10$u6tWF9N.7Q6uVsnhoFeNCOoXh82K17qF5y6L1heFTfovkiRhGbj1G', 'keputih', '264', '60111', '898-80980980', '(+62)808-0980-9808', 2, '2016-10-03 11:12:16', '0000-00-00 00:00:00', 1, NULL, NULL, NULL, NULL),
(6, 'Datamart Computer', 'dodyformello@gmail.com', '$2y$10$0.Za1UqNP/yRQbvNrdepkefZxb3XZ7RhlN7fT7.INXG/8Uu7F/aIe', 'Berbek Industri II/18', '242', '61256', '0318547652', '(+62)812-3278-760', 2, '2016-10-06 02:02:10', '0000-00-00 00:00:00', 1, 'EQNr8SH28jJzHcukzAUEemN7q1sJxZ29WD1ec9cql7VxwVWXIqU4S1LdQzFo', NULL, NULL, '2016-10-28 13:59:49'),
(7, 'Budi', 'budi@gmail.com', '$2y$10$OVJ50tTH7uijlMIB87YYLuRQI54jqVRZN2QMmVNOLLw4eautZKP2W', 'Jl. Undaan No.43', '264', '', '', '(+62)812-3043-4343', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL, NULL, NULL),
(8, 'Muhammad Riduwan', 'ridwan.yaa71@gmail.com', '$2y$10$923iEj7mH5NKvpqIkQKlteEK.V3Wsde1713PO01WYECcqtiAXzvai', 'keputih Gg Makam Blok D-11, Sukolilo', '264', '60111', '', '(+62)857-3511-2973', 2, '2016-10-20 13:05:26', '0000-00-00 00:00:00', 1, 'RsROlyfnBSmuUkrvL9izTiJcys4t55iew5jqGINSwERLDjjsjRSeIAOMlrJf', NULL, NULL, '2016-11-25 11:41:25'),
(9, 'Dody Soegiarto', 'dody_formello@yahoo.com', '$2y$10$V610UHp/bCt8JxKEuj4TKeW7z9ueBEQupJpRWQEaWHJYtxBUwL.Wi', 'Berbek Industri II/18', '264', '61256', '', '', 2, '2016-10-24 06:41:02', '0000-00-00 00:00:00', 1, NULL, NULL, NULL, NULL),
(10, 'Red', 'red_chm@yahoo.co.id', '$2y$10$LmTrbWoB4bpvsKa3yHQTeeuqXYNoNwiKfBicfIvswx9/EuCkaEA8G', 'cek123', '12', '98089', '8089080980', '(+62)080-9890-8908', 2, '2016-10-24 06:32:50', '0000-00-00 00:00:00', 1, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `group_account`
--
ALTER TABLE `group_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hak_akses`
--
ALTER TABLE `hak_akses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `halaman_promo`
--
ALTER TABLE `halaman_promo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_chat`
--
ALTER TABLE `log_chat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifikasi`
--
ALTER TABLE `notifikasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `static`
--
ALTER TABLE `static`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `template_background`
--
ALTER TABLE `template_background`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `template_layout`
--
ALTER TABLE `template_layout`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `template_tombol`
--
ALTER TABLE `template_tombol`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_alamat_pengiriman`
--
ALTER TABLE `temp_alamat_pengiriman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_pembayaran`
--
ALTER TABLE `temp_pembayaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_so_voucher`
--
ALTER TABLE `temp_so_voucher`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmst_cabang_perusahaan`
--
ALTER TABLE `tmst_cabang_perusahaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmst_gudang`
--
ALTER TABLE `tmst_gudang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmst_hadiah`
--
ALTER TABLE `tmst_hadiah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmst_kategori_pelanggan`
--
ALTER TABLE `tmst_kategori_pelanggan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmst_kategori_produk`
--
ALTER TABLE `tmst_kategori_produk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmst_master_coa`
--
ALTER TABLE `tmst_master_coa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmst_paket`
--
ALTER TABLE `tmst_paket`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmst_pelanggan`
--
ALTER TABLE `tmst_pelanggan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmst_perusahaan`
--
ALTER TABLE `tmst_perusahaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmst_produk`
--
ALTER TABLE `tmst_produk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmst_supplier`
--
ALTER TABLE `tmst_supplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmst_voucher`
--
ALTER TABLE `tmst_voucher`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_bayar_pembelian_detail`
--
ALTER TABLE `tran_bayar_pembelian_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_bayar_pembelian_header`
--
ALTER TABLE `tran_bayar_pembelian_header`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_bayar_penjualan_detail`
--
ALTER TABLE `tran_bayar_penjualan_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_bayar_penjualan_header`
--
ALTER TABLE `tran_bayar_penjualan_header`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_detail_stok_opname`
--
ALTER TABLE `tran_detail_stok_opname`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_hadiah_gudang`
--
ALTER TABLE `tran_hadiah_gudang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_jurnal_umum`
--
ALTER TABLE `tran_jurnal_umum`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_jurnal_umum_detail`
--
ALTER TABLE `tran_jurnal_umum_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_jurnal_umum_header`
--
ALTER TABLE `tran_jurnal_umum_header`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_kartu_stok_detail`
--
ALTER TABLE `tran_kartu_stok_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_kartu_stok_header`
--
ALTER TABLE `tran_kartu_stok_header`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_laporan_stok_detail`
--
ALTER TABLE `tran_laporan_stok_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_laporan_stok_header`
--
ALTER TABLE `tran_laporan_stok_header`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_mini_banner`
--
ALTER TABLE `tran_mini_banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_mutasi_stok`
--
ALTER TABLE `tran_mutasi_stok`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_mutasi_stok_detail`
--
ALTER TABLE `tran_mutasi_stok_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_mutasi_stok_header`
--
ALTER TABLE `tran_mutasi_stok_header`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_nilai_spesifikasi`
--
ALTER TABLE `tran_nilai_spesifikasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_nota_beli`
--
ALTER TABLE `tran_nota_beli`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_nota_jual`
--
ALTER TABLE `tran_nota_jual`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_paket_gudang`
--
ALTER TABLE `tran_paket_gudang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_parameter_spesifikasi`
--
ALTER TABLE `tran_parameter_spesifikasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_pengaturan_jurnal`
--
ALTER TABLE `tran_pengaturan_jurnal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_penyesuaian_stok`
--
ALTER TABLE `tran_penyesuaian_stok`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_po_alamat_pengiriman`
--
ALTER TABLE `tran_po_alamat_pengiriman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_po_detail`
--
ALTER TABLE `tran_po_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_po_diskon`
--
ALTER TABLE `tran_po_diskon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_po_header`
--
ALTER TABLE `tran_po_header`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_produk_galeri`
--
ALTER TABLE `tran_produk_galeri`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_produk_gudang`
--
ALTER TABLE `tran_produk_gudang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_produk_harga`
--
ALTER TABLE `tran_produk_harga`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_produk_pilihan`
--
ALTER TABLE `tran_produk_pilihan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_produk_serial_number`
--
ALTER TABLE `tran_produk_serial_number`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_produk_supplier`
--
ALTER TABLE `tran_produk_supplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_produk_tampil_di_beranda`
--
ALTER TABLE `tran_produk_tampil_di_beranda`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_promo_cashback`
--
ALTER TABLE `tran_promo_cashback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_promo_diskon`
--
ALTER TABLE `tran_promo_diskon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_promo_hadiah`
--
ALTER TABLE `tran_promo_hadiah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_rb_detail`
--
ALTER TABLE `tran_rb_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_rb_header`
--
ALTER TABLE `tran_rb_header`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_rekening_supplier`
--
ALTER TABLE `tran_rekening_supplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_rj_detail`
--
ALTER TABLE `tran_rj_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_rj_header`
--
ALTER TABLE `tran_rj_header`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_service_order`
--
ALTER TABLE `tran_service_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_sj_keluar_detail`
--
ALTER TABLE `tran_sj_keluar_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_sj_keluar_header`
--
ALTER TABLE `tran_sj_keluar_header`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_sj_masuk_detail`
--
ALTER TABLE `tran_sj_masuk_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_sj_masuk_header`
--
ALTER TABLE `tran_sj_masuk_header`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_slider_promo`
--
ALTER TABLE `tran_slider_promo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_so_alamat_pengiriman`
--
ALTER TABLE `tran_so_alamat_pengiriman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_so_detail`
--
ALTER TABLE `tran_so_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_so_header`
--
ALTER TABLE `tran_so_header`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_so_pembayaran`
--
ALTER TABLE `tran_so_pembayaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_so_voucher`
--
ALTER TABLE `tran_so_voucher`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_stok_opname`
--
ALTER TABLE `tran_stok_opname`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_stok_opname_detail`
--
ALTER TABLE `tran_stok_opname_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tran_stok_opname_header`
--
ALTER TABLE `tran_stok_opname_header`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tref_bank`
--
ALTER TABLE `tref_bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tref_jenis_barang`
--
ALTER TABLE `tref_jenis_barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tref_konten`
--
ALTER TABLE `tref_konten`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tref_kota`
--
ALTER TABLE `tref_kota`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tref_metode_pembayaran`
--
ALTER TABLE `tref_metode_pembayaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tref_metode_pengiriman`
--
ALTER TABLE `tref_metode_pengiriman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tref_nomor_invoice`
--
ALTER TABLE `tref_nomor_invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tref_pengaturan_po`
--
ALTER TABLE `tref_pengaturan_po`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tref_pengaturan_so`
--
ALTER TABLE `tref_pengaturan_so`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tref_provinsi`
--
ALTER TABLE `tref_provinsi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tref_sumber_data`
--
ALTER TABLE `tref_sumber_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `group_account`
--
ALTER TABLE `group_account`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `hak_akses`
--
ALTER TABLE `hak_akses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `halaman_promo`
--
ALTER TABLE `halaman_promo`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `log_chat`
--
ALTER TABLE `log_chat`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `notifikasi`
--
ALTER TABLE `notifikasi`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `static`
--
ALTER TABLE `static`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `template_background`
--
ALTER TABLE `template_background`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `template_layout`
--
ALTER TABLE `template_layout`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `template_tombol`
--
ALTER TABLE `template_tombol`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `temp_alamat_pengiriman`
--
ALTER TABLE `temp_alamat_pengiriman`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `temp_pembayaran`
--
ALTER TABLE `temp_pembayaran`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `temp_so_voucher`
--
ALTER TABLE `temp_so_voucher`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tmst_cabang_perusahaan`
--
ALTER TABLE `tmst_cabang_perusahaan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tmst_gudang`
--
ALTER TABLE `tmst_gudang`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tmst_hadiah`
--
ALTER TABLE `tmst_hadiah`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tmst_kategori_pelanggan`
--
ALTER TABLE `tmst_kategori_pelanggan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tmst_kategori_produk`
--
ALTER TABLE `tmst_kategori_produk`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tmst_master_coa`
--
ALTER TABLE `tmst_master_coa`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `tmst_paket`
--
ALTER TABLE `tmst_paket`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tmst_pelanggan`
--
ALTER TABLE `tmst_pelanggan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `tmst_perusahaan`
--
ALTER TABLE `tmst_perusahaan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tmst_produk`
--
ALTER TABLE `tmst_produk`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tmst_supplier`
--
ALTER TABLE `tmst_supplier`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tmst_voucher`
--
ALTER TABLE `tmst_voucher`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tran_bayar_pembelian_detail`
--
ALTER TABLE `tran_bayar_pembelian_detail`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tran_bayar_pembelian_header`
--
ALTER TABLE `tran_bayar_pembelian_header`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tran_bayar_penjualan_detail`
--
ALTER TABLE `tran_bayar_penjualan_detail`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tran_bayar_penjualan_header`
--
ALTER TABLE `tran_bayar_penjualan_header`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tran_detail_stok_opname`
--
ALTER TABLE `tran_detail_stok_opname`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tran_hadiah_gudang`
--
ALTER TABLE `tran_hadiah_gudang`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tran_jurnal_umum`
--
ALTER TABLE `tran_jurnal_umum`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tran_jurnal_umum_detail`
--
ALTER TABLE `tran_jurnal_umum_detail`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tran_jurnal_umum_header`
--
ALTER TABLE `tran_jurnal_umum_header`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tran_kartu_stok_detail`
--
ALTER TABLE `tran_kartu_stok_detail`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=155;
--
-- AUTO_INCREMENT for table `tran_kartu_stok_header`
--
ALTER TABLE `tran_kartu_stok_header`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `tran_laporan_stok_detail`
--
ALTER TABLE `tran_laporan_stok_detail`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT for table `tran_laporan_stok_header`
--
ALTER TABLE `tran_laporan_stok_header`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `tran_mini_banner`
--
ALTER TABLE `tran_mini_banner`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tran_mutasi_stok`
--
ALTER TABLE `tran_mutasi_stok`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tran_mutasi_stok_detail`
--
ALTER TABLE `tran_mutasi_stok_detail`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tran_mutasi_stok_header`
--
ALTER TABLE `tran_mutasi_stok_header`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tran_nilai_spesifikasi`
--
ALTER TABLE `tran_nilai_spesifikasi`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tran_nota_beli`
--
ALTER TABLE `tran_nota_beli`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tran_nota_jual`
--
ALTER TABLE `tran_nota_jual`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tran_paket_gudang`
--
ALTER TABLE `tran_paket_gudang`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tran_parameter_spesifikasi`
--
ALTER TABLE `tran_parameter_spesifikasi`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tran_pengaturan_jurnal`
--
ALTER TABLE `tran_pengaturan_jurnal`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `tran_penyesuaian_stok`
--
ALTER TABLE `tran_penyesuaian_stok`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tran_po_alamat_pengiriman`
--
ALTER TABLE `tran_po_alamat_pengiriman`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tran_po_detail`
--
ALTER TABLE `tran_po_detail`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tran_po_diskon`
--
ALTER TABLE `tran_po_diskon`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tran_po_header`
--
ALTER TABLE `tran_po_header`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tran_produk_galeri`
--
ALTER TABLE `tran_produk_galeri`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tran_produk_gudang`
--
ALTER TABLE `tran_produk_gudang`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `tran_produk_harga`
--
ALTER TABLE `tran_produk_harga`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tran_produk_pilihan`
--
ALTER TABLE `tran_produk_pilihan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tran_produk_serial_number`
--
ALTER TABLE `tran_produk_serial_number`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=181;
--
-- AUTO_INCREMENT for table `tran_produk_supplier`
--
ALTER TABLE `tran_produk_supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tran_produk_tampil_di_beranda`
--
ALTER TABLE `tran_produk_tampil_di_beranda`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tran_promo_cashback`
--
ALTER TABLE `tran_promo_cashback`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tran_promo_diskon`
--
ALTER TABLE `tran_promo_diskon`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tran_promo_hadiah`
--
ALTER TABLE `tran_promo_hadiah`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tran_rb_detail`
--
ALTER TABLE `tran_rb_detail`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tran_rb_header`
--
ALTER TABLE `tran_rb_header`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tran_rekening_supplier`
--
ALTER TABLE `tran_rekening_supplier`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tran_rj_detail`
--
ALTER TABLE `tran_rj_detail`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tran_rj_header`
--
ALTER TABLE `tran_rj_header`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tran_service_order`
--
ALTER TABLE `tran_service_order`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tran_sj_keluar_detail`
--
ALTER TABLE `tran_sj_keluar_detail`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tran_sj_keluar_header`
--
ALTER TABLE `tran_sj_keluar_header`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tran_sj_masuk_detail`
--
ALTER TABLE `tran_sj_masuk_detail`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `tran_sj_masuk_header`
--
ALTER TABLE `tran_sj_masuk_header`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tran_slider_promo`
--
ALTER TABLE `tran_slider_promo`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tran_so_alamat_pengiriman`
--
ALTER TABLE `tran_so_alamat_pengiriman`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tran_so_detail`
--
ALTER TABLE `tran_so_detail`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tran_so_header`
--
ALTER TABLE `tran_so_header`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tran_so_pembayaran`
--
ALTER TABLE `tran_so_pembayaran`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tran_so_voucher`
--
ALTER TABLE `tran_so_voucher`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tran_stok_opname`
--
ALTER TABLE `tran_stok_opname`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tran_stok_opname_detail`
--
ALTER TABLE `tran_stok_opname_detail`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tran_stok_opname_header`
--
ALTER TABLE `tran_stok_opname_header`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tref_bank`
--
ALTER TABLE `tref_bank`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tref_jenis_barang`
--
ALTER TABLE `tref_jenis_barang`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tref_konten`
--
ALTER TABLE `tref_konten`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tref_kota`
--
ALTER TABLE `tref_kota`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=515;
--
-- AUTO_INCREMENT for table `tref_metode_pembayaran`
--
ALTER TABLE `tref_metode_pembayaran`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tref_metode_pengiriman`
--
ALTER TABLE `tref_metode_pengiriman`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tref_nomor_invoice`
--
ALTER TABLE `tref_nomor_invoice`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `tref_pengaturan_po`
--
ALTER TABLE `tref_pengaturan_po`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tref_pengaturan_so`
--
ALTER TABLE `tref_pengaturan_so`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tref_provinsi`
--
ALTER TABLE `tref_provinsi`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `tref_sumber_data`
--
ALTER TABLE `tref_sumber_data`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
