<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@pos.com',
            'password' => bcrypt('123'),
            'kota_id' => '264',
            'hak_akses_id' => '1',
            'is_aktif' => 1,
        ]);

        DB::table('hak_akses')->insert([
            'nama' => 'administrator',
            'deskripsi' => 'administrator',
            'menu_akses' => '1-2-3-4-5-6-7-8-9-10-11-12-13-14-15-16-17-18-19-20-21-22-23-24-25-26-27-28-29-30-31-32-33-34-35-36'
        ]);

        DB::table('hak_akses')->insert([
            'nama' => 'pelanggan',
            'deskripsi' => 'pelanggan',
        ]);

        DB::table('tmst_pelanggan')->insert([
            'id' => 1,
            'nama' => 'guest',
        ]);
    }
}
