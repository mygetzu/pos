<?php

use Illuminate\Database\Seeder;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menu')->insert(['id' => '1', 'nama' => 'dashboard', 'route' => 'beranda_admin']);
        DB::table('menu')->insert(['id' => '2', 'nama' => 'perusahaan', 'route' => 'perusahaan']);
        DB::table('menu')->insert(['id' => '3', 'nama' => 'chat', 'route' => 'chat']);
        DB::table('menu')->insert(['id' => '4', 'nama' => 'data referensi', 'route' => 'data_referensi']);
        DB::table('menu')->insert(['id' => '5', 'nama' => 'ecommerce', 'route' => 'ecommerce']);
        DB::table('menu')->insert(['id' => '6', 'nama' => 'group account', 'route' => 'group_account']);
        DB::table('menu')->insert(['id' => '7', 'nama' => 'master coa', 'route' => 'master_coa']);
        DB::table('menu')->insert(['id' => '8', 'nama' => 'pengaturan jurnal', 'route' => 'pengaturan_jurnal']);
        DB::table('menu')->insert(['id' => '9', 'nama' => 'akun saya', 'route' => 'akun_saya']);
        DB::table('menu')->insert(['id' => '10', 'nama' => 'data akun', 'route' => 'data_user']);
        DB::table('menu')->insert(['id' => '11', 'nama' => 'hak akses', 'route' => 'hak_akses']);
        DB::table('menu')->insert(['id' => '12', 'nama' => 'kategori pelanggan', 'route' => 'kategori_pelanggan']);
        DB::table('menu')->insert(['id' => '13', 'nama' => 'pelanggan', 'route' => 'pelanggan']);
        DB::table('menu')->insert(['id' => '14', 'nama' => 'supplier', 'route' => 'supplier']);
        DB::table('menu')->insert(['id' => '15', 'nama' => 'kategori produk', 'route' => 'kategori_produk']);
        DB::table('menu')->insert(['id' => '16', 'nama' => 'gudang', 'route' => 'gudang']);
        DB::table('menu')->insert(['id' => '17', 'nama' => 'produk', 'route' => 'produk']);
        DB::table('menu')->insert(['id' => '18', 'nama' => 'hadiah', 'route' => 'hadiah']);
        DB::table('menu')->insert(['id' => '19', 'nama' => 'paket produk', 'route' => 'paket_produk']);
        DB::table('menu')->insert(['id' => '20', 'nama' => 'purchase order', 'route' => 'purchase_order']);
        DB::table('menu')->insert(['id' => '21', 'nama' => 'surat jalan_masuk', 'route' => 'surat_jalan_masuk']);
        DB::table('menu')->insert(['id' => '22', 'nama' => 'nota beli', 'route' => 'nota_beli']);
        DB::table('menu')->insert(['id' => '23', 'nama' => 'sales order', 'route' => 'sales_order']);
        DB::table('menu')->insert(['id' => '24', 'nama' => 'surat jalan_keluar', 'route' => 'surat_jalan_keluar']);
        DB::table('menu')->insert(['id' => '25', 'nama' => 'nota jual', 'route' => 'nota_jual']);
        DB::table('menu')->insert(['id' => '26', 'nama' => 'retur jual', 'route' => 'retur_jual']);
        DB::table('menu')->insert(['id' => '27', 'nama' => 'retur beli', 'route' => 'retur_beli']);
        DB::table('menu')->insert(['id' => '28', 'nama' => 'servis', 'route' => 'servis']);
        DB::table('menu')->insert(['id' => '29', 'nama' => 'voucher', 'route' => 'voucher']);
        DB::table('menu')->insert(['id' => '30', 'nama' => 'penyesuaian stok', 'route' => 'penyesuaian_stok']);
        DB::table('menu')->insert(['id' => '31', 'nama' => 'mutasi stok', 'route' => 'mutasi_stok']);
        DB::table('menu')->insert(['id' => '32', 'nama' => 'stok opname', 'route' => 'stok_opname']);
        DB::table('menu')->insert(['id' => '33', 'nama' => 'laporan stok', 'route' => 'laporan_stok']);
        DB::table('menu')->insert(['id' => '34', 'nama' => 'kartu stok', 'route' => 'kartu_stok']);
        DB::table('menu')->insert(['id' => '35', 'nama' => 'laporan penjualan', 'route' => 'laporan_penjualan']);
        DB::table('menu')->insert(['id' => '36', 'nama' => 'jurnal umum', 'route' => 'jurnal_umum']);
    }
}
