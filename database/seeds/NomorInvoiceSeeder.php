<?php

use Illuminate\Database\Seeder;

class NomorInvoiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tref_nomor_invoice')->insert([
            'id' => 1,
            'nama' => 'purchase_order',
            'format' => 'PO/',
        ]);

        DB::table('tref_nomor_invoice')->insert([
            'id' => 2,
            'nama' => 'sales_order',
            'format' => 'SO/',
        ]);

        DB::table('tref_nomor_invoice')->insert([
            'id' => 3,
            'nama' => 'retur_jual',
            'format' => 'RJ/',
        ]);

        DB::table('tref_nomor_invoice')->insert([
            'id' => 4,
            'nama' => 'retur_beli',
            'format' => 'RB/',
        ]);

        DB::table('tref_nomor_invoice')->insert([
            'id' => 5,
            'nama' => 'service_order',
            'format' => 'SV/',
        ]);

        DB::table('tref_nomor_invoice')->insert([
            'id' => 6,
            'nama' => 'nota_beli',
        ]);

        DB::table('tref_nomor_invoice')->insert([
            'id' => 7,
            'nama' => 'nota_jual',
        ]);

        DB::table('tref_nomor_invoice')->insert([
            'id' => 8,
            'nama' => 'packing_list',
        ]);

        DB::table('tref_nomor_invoice')->insert([
            'id' => 9,
            'nama' => 'surat_jalan',
        ]);
    }
}
