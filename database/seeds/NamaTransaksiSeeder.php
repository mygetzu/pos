<?php

use Illuminate\Database\Seeder;

class NamaTransaksiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tref_nama_transaksi')->insert([
            'id' => 1,
            'nama' => 'total harga',
            'transaksi' => 'pembelian',
        ]);

        DB::table('tref_nama_transaksi')->insert([
            'id' => 2,
            'nama' => 'diskon',
            'transaksi' => 'pembelian',
        ]);

        DB::table('tref_nama_transaksi')->insert([
            'id' => 3,
            'nama' => 'total harga',
            'transaksi' => 'penjualan',
        ]);

        DB::table('tref_nama_transaksi')->insert([
            'id' => 4,
            'nama' => 'voucher',
            'transaksi' => 'penjualan',
        ]);
    }
}
