<?php

use Illuminate\Database\Seeder;

class MetodePembayaranSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tref_metode_pembayaran')->insert([
            'id' => 1,
            'nama' => 'Tunai',
            'keterangan' => 'pembayaran secara tunai',
            'is_aktif' => 1,
        ]);

        DB::table('tref_metode_pembayaran')->insert([
            'id' => 2,
            'nama' => 'Transfer',
            'keterangan' => 'pembayaran secara transfer',
            'is_aktif' => 1,
        ]);

        DB::table('tref_metode_pembayaran')->insert([
            'id' => 3,
            'nama' => 'Kartu Kedit',
            'keterangan' => 'pembayaran menggunakan kartu kredit',
            'is_aktif' => 1,
        ]);

        DB::table('tref_metode_pembayaran')->insert([
            'id' => 4,
            'nama' => 'Giro',
            'keterangan' => 'pembayaran menggunakan giro',
            'is_aktif' => 1,
        ]);

        DB::table('tref_metode_pembayaran')->insert([
            'id' => 5,
            'nama' => 'Cek',
            'keterangan' => 'pembayaran menggunakan cek',
            'is_aktif' => 1,
        ]);
    }
}
