<?php

use Illuminate\Database\Seeder;

class ProvinsiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tref_provinsi')->insert([
            'kode' => '11',
            'nama' => 'ACEH',
        ]);

        DB::table('tref_provinsi')->insert([
            'kode' => '51',
            'nama' => 'BALI',
        ]);

        DB::table('tref_provinsi')->insert([
            'kode' => '36',
            'nama' => 'BANTEN',
        ]);

        DB::table('tref_provinsi')->insert([
            'kode' => '17',
            'nama' => 'BENGKULU',
        ]);

        DB::table('tref_provinsi')->insert([
            'kode' => '34',
            'nama' => 'DI YOGYAKARTA',
        ]);

        DB::table('tref_provinsi')->insert([
            'kode' => '31',
            'nama' => 'DKI JAKARTA',
        ]);

        DB::table('tref_provinsi')->insert([
            'kode' => '75',
            'nama' => 'GORONTALO',
        ]);

        DB::table('tref_provinsi')->insert([
            'kode' => '15',
            'nama' => 'JAMBI',
        ]);

        DB::table('tref_provinsi')->insert([
            'kode' => '32',
            'nama' => 'JAWA BARAT',
        ]);

        DB::table('tref_provinsi')->insert([
            'kode' => '33',
            'nama' => 'JAWA TENGAH',
        ]);

        DB::table('tref_provinsi')->insert([
            'kode' => '35',
            'nama' => 'JAWA TIMUR',
        ]);

        DB::table('tref_provinsi')->insert([
            'kode' => '61',
            'nama' => 'KALIMANTAN BARAT',
        ]);

        DB::table('tref_provinsi')->insert([
            'kode' => '63',
            'nama' => 'KALIMANTAN SELATAN',
        ]);

        DB::table('tref_provinsi')->insert([
            'kode' => '62',
            'nama' => 'KALIMANTAN TENGAH',
        ]);

        DB::table('tref_provinsi')->insert([
            'kode' => '64',
            'nama' => 'KALIMANTAN TIMUR',
        ]);

        DB::table('tref_provinsi')->insert([
            'kode' => '65',
            'nama' => 'KALIMANTAN UTARA',
        ]);

        DB::table('tref_provinsi')->insert([
            'kode' => '19',
            'nama' => 'KEPULAUAN BANGKA BELITUNG',
        ]);

        DB::table('tref_provinsi')->insert([
            'kode' => '21',
            'nama' => 'KEPULAUAN RIAU',
        ]);

        DB::table('tref_provinsi')->insert([
            'kode' => '18',
            'nama' => 'LAMPUNG',
        ]);

        DB::table('tref_provinsi')->insert([
            'kode' => '81',
            'nama' => 'MALUKU',
        ]);

        DB::table('tref_provinsi')->insert([
            'kode' => '82',
            'nama' => 'MALUKU UTARA',
        ]);

        DB::table('tref_provinsi')->insert([
            'kode' => '52',
            'nama' => 'NUSA TENGGARA BARAT',
        ]);

        DB::table('tref_provinsi')->insert([
            'kode' => '53',
            'nama' => 'NUSA TENGGARA TIMUR',
        ]);

        DB::table('tref_provinsi')->insert([
            'kode' => '94',
            'nama' => 'PAPUA',
        ]);

        DB::table('tref_provinsi')->insert([
            'kode' => '91',
            'nama' => 'PAPUA BARAT',
        ]);

        DB::table('tref_provinsi')->insert([
            'kode' => '14',
            'nama' => 'RIAU',
        ]);

        DB::table('tref_provinsi')->insert([
            'kode' => '76',
            'nama' => 'SULAWESI BARAT',
        ]);

        DB::table('tref_provinsi')->insert([
            'kode' => '73',
            'nama' => 'SULAWESI SELATAN',
        ]);

        DB::table('tref_provinsi')->insert([
            'kode' => '72',
            'nama' => 'SULAWESI TENGAH',
        ]);

        DB::table('tref_provinsi')->insert([
            'kode' => '74',
            'nama' => 'SULAWESI TENGGARA',
        ]);

        DB::table('tref_provinsi')->insert([
            'kode' => '71',
            'nama' => 'SULAWESI UTARA',
        ]);

        DB::table('tref_provinsi')->insert([
            'kode' => '13',
            'nama' => 'SUMATERA BARAT',
        ]);

        DB::table('tref_provinsi')->insert([
            'kode' => '16',
            'nama' => 'SUMATERA SELATAN',
        ]);

        DB::table('tref_provinsi')->insert([
            'kode' => '12',
            'nama' => 'SUMATERA UTARA',
        ]);
    }
}
