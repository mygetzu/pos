<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        /*$this->call(ProvinsiTableSeeder::class);
        $this->call(PerusahaanTableSeeder::class);
        $this->call(JenisBarangSeeder::class);
        $this->call(MetodePembayaranSeeder::class);
        $this->call(SumberDataSeeder::class);
        $this->call(NomorInvoiceSeeder::class);
        $this->call(NamaTransaksiSeeder::class);
        $this->call(StaticSeeder::class);*/
        $this->call(OAuthClientSeeder::class);
    }
}
