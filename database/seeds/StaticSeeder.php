<?php

use Illuminate\Database\Seeder;

class StaticSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('static')->insert([
            'id' => 1,
            'nama' => 'gambar_produk_default',
            'value' => '',
        ]);
    }
}
