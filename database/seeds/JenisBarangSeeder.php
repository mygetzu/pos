<?php

use Illuminate\Database\Seeder;

class JenisBarangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tref_jenis_barang')->insert([
            'id' => 1,
            'nama' => 'produk',
            'keterangan' => 'produk pada umumnya',
        ]);

        DB::table('tref_jenis_barang')->insert([
            'id' => 2,
            'nama' => 'hadiah',
            'keterangan' => 'produk yang dijadikan hadiah untuk pelanggan',
        ]);

        DB::table('tref_jenis_barang')->insert([
            'id' => 3,
            'nama' => 'paket produk',
            'keterangan' => 'kumpulan produk yang dijual menjadi satu paket',
        ]);
    }
}
