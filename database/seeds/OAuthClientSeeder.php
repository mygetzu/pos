<?php

use Illuminate\Database\Seeder;

class OAuthClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('oauth_clients')->insert([
            'id'        => bcrypt('mobile_device'),
            'secret'    => bcrypt('mobile_device_Key'),
            'name'      => 'API Mobile'
        ]);
    }
}
