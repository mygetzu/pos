<?php

use Illuminate\Database\Seeder;

class PerusahaanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tmst_perusahaan')->insert([
            'nama' => 'POS',
            'alamat' => 'POS',
            'kota_id' => 264,
        ]);
    }
}
