<?php

use Illuminate\Database\Seeder;

class SumberDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tref_sumber_data')->insert([
            'id' => '1',
            'nama' => 'Surat Jalan',
        ]);

        DB::table('tref_sumber_data')->insert([
            'id' => '2',
            'nama' => 'Packing List',
        ]);

        DB::table('tref_sumber_data')->insert([
            'id' => '3',
            'nama' => 'Retur Jual',
        ]);

        DB::table('tref_sumber_data')->insert([
            'id' => '4',
            'nama' => 'Retur Beli',
        ]);
    }
}
