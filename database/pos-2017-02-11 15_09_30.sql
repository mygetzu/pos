-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versi server:                 10.1.19-MariaDB - mariadb.org binary distribution
-- OS Server:                    Win32
-- HeidiSQL Versi:               9.4.0.5144
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Membuang struktur basisdata untuk pos
CREATE DATABASE IF NOT EXISTS `pos` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `pos`;

-- membuang struktur untuk table pos.group_account
CREATE TABLE IF NOT EXISTS `group_account` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `debit_or_kredit` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_kode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.group_account: ~3 rows (lebih kurang)
DELETE FROM `group_account`;
/*!40000 ALTER TABLE `group_account` DISABLE KEYS */;
INSERT INTO `group_account` (`id`, `kode`, `nama`, `debit_or_kredit`, `group_kode`) VALUES
	(1, '001', 'Aktiva Tetap', 'D', ''),
	(2, '002', 'Aktiva Lancar', 'D', ''),
	(3, '003', 'Passiva', 'K', '');
/*!40000 ALTER TABLE `group_account` ENABLE KEYS */;

-- membuang struktur untuk table pos.hak_akses
CREATE TABLE IF NOT EXISTS `hak_akses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deskripsi` longtext COLLATE utf8_unicode_ci NOT NULL,
  `menu_akses` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.hak_akses: ~3 rows (lebih kurang)
DELETE FROM `hak_akses`;
/*!40000 ALTER TABLE `hak_akses` DISABLE KEYS */;
INSERT INTO `hak_akses` (`id`, `nama`, `deskripsi`, `menu_akses`, `created_at`, `updated_at`) VALUES
	(1, 'administrator', 'administrator', '1-2-3-4-5-6-7-8-9-10-11-12-13-14-15-16-17-18-19-20-21-22-23-24-25-26-27-28-29-30-31-32-33-34-35-36', NULL, NULL),
	(2, 'pelanggan', 'pelanggan', '', NULL, NULL),
	(3, 'teknisi', 'teknisi', '101-102-103-104-105-106', NULL, NULL);
/*!40000 ALTER TABLE `hak_akses` ENABLE KEYS */;

-- membuang struktur untuk table pos.halaman_promo
CREATE TABLE IF NOT EXISTS `halaman_promo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `file_gambar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_aktif` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.halaman_promo: ~6 rows (lebih kurang)
DELETE FROM `halaman_promo`;
/*!40000 ALTER TABLE `halaman_promo` DISABLE KEYS */;
INSERT INTO `halaman_promo` (`id`, `file_gambar`, `is_aktif`, `item_order`, `created_at`, `updated_at`) VALUES
	(1, 'I4H-banner.jpg', '1', 1, NULL, NULL),
	(2, '01S-banner.jpg', '1', 2, NULL, NULL),
	(3, 'ZP0-banner.jpg', '1', 3, NULL, NULL),
	(4, 'SZC-banner.jpg', '1', 4, NULL, NULL),
	(5, 'T8U-banner.png', '1', 5, NULL, NULL),
	(6, 'SNS-banner.jpg', '1', 6, NULL, NULL);
/*!40000 ALTER TABLE `halaman_promo` ENABLE KEYS */;

-- membuang struktur untuk table pos.log_chat
CREATE TABLE IF NOT EXISTS `log_chat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id_a` int(11) NOT NULL,
  `user_id_b` int(11) NOT NULL,
  `pesan` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.log_chat: ~3 rows (lebih kurang)
DELETE FROM `log_chat`;
/*!40000 ALTER TABLE `log_chat` DISABLE KEYS */;
INSERT INTO `log_chat` (`id`, `user_id_a`, `user_id_b`, `pesan`, `created_at`, `updated_at`) VALUES
	(1, 4, 1, '[{"user_id":"1","tanggal":"28\\/10\\/2016 19:39:39","pesan":"cek"},{"user_id":"4","tanggal":"28\\/10\\/2016 19:39:48","pesan":"iya"},{"user_id":"1","tanggal":"28\\/10\\/2016 19:40:24","pesan":"cek"},{"user_id":"4","tanggal":"28\\/10\\/2016 19:40:44","pesan":"selamat malam"},{"user_id":"1","tanggal":"28\\/10\\/2016 19:40:48","pesan":"oke"},{"user_id":"1","tanggal":"28\\/10\\/2016 19:41:9","pesan":"baik"},{"user_id":"4","tanggal":"31\\/10\\/2016 17:29:42","pesan":"halo"},{"user_id":"1","tanggal":"31\\/10\\/2016 17:29:57","pesan":"iyo"},{"user_id":"1","tanggal":"31\\/10\\/2016 17:30:5","pesan":"iso jadine"},{"user_id":"1","tanggal":"31\\/10\\/2016 17:31:48","pesan":"selamat sore apa ada yang bisa saya bantu??"},{"user_id":"4","tanggal":"31\\/10\\/2016 17:33:14","pesan":"cuy"}]', NULL, NULL),
	(2, 1, 8, '[{"user_id":"8","tanggal":"31\\/10\\/2016 17:33:23","pesan":"cek"},{"user_id":"8","tanggal":"8\\/11\\/2016 19:56:56","pesan":"cek"},{"user_id":"8","tanggal":"8\\/11\\/2016 19:56:56","pesan":"cek"},{"user_id":"8","tanggal":"8\\/11\\/2016 19:56:56","pesan":"cek"},{"user_id":"1","tanggal":"8\\/11\\/2016 19:57:9","pesan":"tes 123"}]', NULL, NULL),
	(3, 1, 6, '[{"user_id":"6","tanggal":"8\\/11\\/2016 20:0:9","pesan":"apa Kabar?"},{"user_id":"6","tanggal":"8\\/11\\/2016 20:0:9","pesan":"apa Kabar?"},{"user_id":"6","tanggal":"8\\/11\\/2016 20:0:10","pesan":"apa Kabar?"},{"user_id":"1","tanggal":"8\\/11\\/2016 20:0:24","pesan":"yuppppp"}]', NULL, NULL);
/*!40000 ALTER TABLE `log_chat` ENABLE KEYS */;

-- membuang struktur untuk table pos.menu
CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `route` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.menu: ~42 rows (lebih kurang)
DELETE FROM `menu`;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` (`id`, `nama`, `route`, `created_at`, `updated_at`) VALUES
	(1, 'dashboard', 'beranda_admin', NULL, NULL),
	(2, 'perusahaan', 'perusahaan', NULL, NULL),
	(3, 'chat', 'chat', NULL, NULL),
	(4, 'data referensi', 'data_referensi', NULL, NULL),
	(5, 'ecommerce', 'ecommerce', NULL, NULL),
	(6, 'group account', 'group_account', NULL, NULL),
	(7, 'master coa', 'master_coa', NULL, NULL),
	(8, 'pengaturan jurnal', 'pengaturan_jurnal', NULL, NULL),
	(9, 'akun saya', 'akun_saya', NULL, NULL),
	(10, 'data akun', 'data_user', NULL, NULL),
	(11, 'hak akses', 'hak_akses', NULL, NULL),
	(12, 'kategori pelanggan', 'kategori_pelanggan', NULL, NULL),
	(13, 'pelanggan', 'pelanggan', NULL, NULL),
	(14, 'supplier', 'supplier', NULL, NULL),
	(15, 'kategori produk', 'kategori_produk', NULL, NULL),
	(16, 'gudang', 'gudang', NULL, NULL),
	(17, 'produk', 'produk', NULL, NULL),
	(18, 'hadiah', 'hadiah', NULL, NULL),
	(19, 'paket produk', 'paket_produk', NULL, NULL),
	(20, 'purchase order', 'purchase_order', NULL, NULL),
	(21, 'surat jalan_masuk', 'surat_jalan_masuk', NULL, NULL),
	(22, 'nota beli', 'nota_beli', NULL, NULL),
	(23, 'sales order', 'sales_order', NULL, NULL),
	(24, 'surat jalan_keluar', 'surat_jalan_keluar', NULL, NULL),
	(25, 'nota jual', 'nota_jual', NULL, NULL),
	(26, 'retur jual', 'retur_jual', NULL, NULL),
	(27, 'retur beli', 'retur_beli', NULL, NULL),
	(28, 'servis', 'servis', NULL, NULL),
	(29, 'voucher', 'voucher', NULL, NULL),
	(30, 'penyesuaian stok', 'penyesuaian_stok', NULL, NULL),
	(31, 'mutasi stok', 'mutasi_stok', NULL, NULL),
	(32, 'stok opname', 'stok_opname', NULL, NULL),
	(33, 'laporan stok', 'laporan_stok', NULL, NULL),
	(34, 'kartu stok', 'kartu_stok', NULL, NULL),
	(35, 'laporan penjualan', 'laporan_penjualan', NULL, NULL),
	(36, 'jurnal umum', 'jurnal_umum', NULL, NULL),
	(101, 'dashboard teknisi', 'dashboard_teknisi', NULL, NULL),
	(102, 'akun teknisi', 'akun_teknisi', NULL, NULL),
	(103, 'produk akan servis', 'produk_akan_servis', NULL, NULL),
	(104, 'peoduk sedang servis', 'produk_sedang_servis', NULL, NULL),
	(105, 'produk telah servis', 'produk_telah_servis', NULL, NULL),
	(106, 'laporan servis', 'laporan_servis', NULL, NULL);
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;

-- membuang struktur untuk table pos.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.migrations: ~105 rows (lebih kurang)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`migration`, `batch`) VALUES
	('2014_04_24_110151_create_oauth_scopes_table', 1),
	('2014_04_24_110304_create_oauth_grants_table', 1),
	('2014_04_24_110403_create_oauth_grant_scopes_table', 1),
	('2014_04_24_110459_create_oauth_clients_table', 1),
	('2014_04_24_110557_create_oauth_client_endpoints_table', 1),
	('2014_04_24_110705_create_oauth_client_scopes_table', 1),
	('2014_04_24_110817_create_oauth_client_grants_table', 1),
	('2014_04_24_111002_create_oauth_sessions_table', 1),
	('2014_04_24_111109_create_oauth_session_scopes_table', 1),
	('2014_04_24_111254_create_oauth_auth_codes_table', 1),
	('2014_04_24_111403_create_oauth_auth_code_scopes_table', 1),
	('2014_04_24_111518_create_oauth_access_tokens_table', 1),
	('2014_04_24_111657_create_oauth_access_token_scopes_table', 1),
	('2014_04_24_111810_create_oauth_refresh_tokens_table', 1),
	('2014_10_12_000000_create_users_table', 1),
	('2014_10_12_100000_create_password_resets_table', 1),
	('2016_09_04_174722_table_master_tmst', 1),
	('2016_09_04_174811_table_reference', 1),
	('2016_09_04_174852_table_transaction', 1),
	('2016_09_04_174939_table_temp_and_others', 1),
	('2016_09_22_120241_group_account', 1),
	('2016_09_22_144937_tref_nomor_invoice', 1),
	('2016_09_23_120450_tmst_jurnal_umum', 1),
	('2016_09_28_065807_tabel_static', 1),
	('2016_10_04_143101_tabel_checkout_ecommerce', 1),
	('2016_10_07_062132_notifikasi', 1),
	('2016_10_08_074901_produk_tampil_di_beranda', 1),
	('2016_10_12_175938_temp_hadiah_voucher', 1),
	('2016_10_13_195657_paket_gudang', 1),
	('2016_10_17_084317_mutasi_stok', 1),
	('2016_10_19_094417_stok_opname', 1),
	('2016_10_20_143558_laporan_kartu_stok', 1),
	('2016_10_26_112745_pengaturan_po_so', 1),
	('2016_10_27_075459_menu', 1),
	('2016_11_03_085148_new_transaksi', 1),
	('2016_11_10_103450_jurnal_umum', 1),
	('2016_11_20_124115_promo_creator', 1),
	('2016_11_22_102411_so_pembayaran', 1),
	('2016_12_02_102408_tabel_pembayaran', 1),
	('2016_12_05_111040_tabel_spesifikasi', 1),
	('2016_12_06_142150_tambah_file_gambar_kategori_produk', 1),
	('2016_12_13_114241_add_flag_servis', 1),
	('2016_12_13_142054_paket_pilihan', 1),
	('2016_12_16_104002_transaksi_jasa', 1),
	('2016_12_19_095414_create_shoppingcart_table', 1),
	('2016_12_19_124114_tambah_beberapa_tabel_kolom', 1),
	('2016_12_26_142509_tambah_kolom_jenis_barang_id', 1),
	('2016_12_27_123410_tambah_kolom_pelanggan_cart', 1),
	('2014_10_12_000000_create_users_table', 1),
	('2014_10_12_100000_create_password_resets_table', 1),
	('2016_09_04_174722_table_master_tmst', 1),
	('2016_09_04_174811_table_reference', 1),
	('2016_09_04_174852_table_transaction', 1),
	('2016_09_04_174939_table_temp_and_others', 1),
	('2016_09_22_120241_group_account', 2),
	('2016_09_22_144937_tref_nomor_invoice', 2),
	('2016_09_23_120450_tmst_jurnal_umum', 3),
	('2016_09_28_065807_tabel_static', 4),
	('2016_10_03_052820_tran_po_diskon', 5),
	('2016_10_04_143101_tabel_checkout_ecommerce', 6),
	('2016_10_07_062132_notifikasi', 7),
	('2016_10_08_074901_produk_tampil_di_beranda', 8),
	('2016_10_12_175938_temp_hadiah_voucher', 9),
	('2016_10_13_195657_paket_gudang', 10),
	('2016_10_17_084317_mutasi_stok', 11),
	('2016_10_19_094417_stok_opname', 12),
	('2016_10_20_143558_laporan_kartu_stok', 13),
	('2016_10_26_112745_pengaturan_po_so', 14),
	('2016_10_27_075459_menu', 14),
	('2016_11_10_103450_jurnal_umum', 16),
	('2016_11_03_085148_new_transaksi', 17),
	('2016_11_22_102411_so_pembayaran', 19),
	('2016_11_20_124115_promo_creator', 20),
	('2016_12_02_102408_tabel_pembayaran', 21),
	('2016_12_05_111040_tabel_spesifikasi', 22),
	('2016_12_06_142150_tambah_file_gambar_kategori_produk', 23),
	('2016_12_13_114241_add_flag_servis', 24),
	('2016_12_13_142054_paket_pilihan', 25),
	('2016_12_16_104002_transaksi_jasa', 26),
	('2014_04_24_110151_create_oauth_scopes_table', 27),
	('2014_04_24_110304_create_oauth_grants_table', 27),
	('2014_04_24_110403_create_oauth_grant_scopes_table', 27),
	('2014_04_24_110459_create_oauth_clients_table', 27),
	('2014_04_24_110557_create_oauth_client_endpoints_table', 27),
	('2014_04_24_110705_create_oauth_client_scopes_table', 27),
	('2014_04_24_110817_create_oauth_client_grants_table', 27),
	('2014_04_24_111002_create_oauth_sessions_table', 27),
	('2014_04_24_111109_create_oauth_session_scopes_table', 27),
	('2014_04_24_111254_create_oauth_auth_codes_table', 27),
	('2014_04_24_111403_create_oauth_auth_code_scopes_table', 27),
	('2014_04_24_111518_create_oauth_access_tokens_table', 27),
	('2014_04_24_111657_create_oauth_access_token_scopes_table', 27),
	('2014_04_24_111810_create_oauth_refresh_tokens_table', 27),
	('2016_12_19_095414_create_shoppingcart_table', 27),
	('2016_12_19_124114_tambah_beberapa_tabel_kolom', 28),
	('2016_12_26_142509_tambah_kolom_jenis_barang_id', 29),
	('2016_12_27_123410_tambah_kolom_pelanggan_cart', 30),
	('2016_12_29_130036_penyesuaian_stok', 31),
	('2017_01_13_063414_add_file_gambar_to_tran_pelanggan_cart', 32),
	('2017_01_13_064813_add_jasa_pengiriman_to_nota_jual', 32),
	('2017_01_13_083416_add_pengambilan_barang_to_trans_sj_keluar_header', 32),
	('2017_01_13_083653_add_garansi_to_tran_service_order', 32),
	('2017_01_13_083953_create_tref_syarat_ketentuan', 33),
	('2017_01_14_065938_create_tran_service_invoice_header', 33),
	('2017_01_14_070316_create_tran_service_invoice_detail', 33);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- membuang struktur untuk table pos.notifikasi
CREATE TABLE IF NOT EXISTS `notifikasi` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pesan` text COLLATE utf8_unicode_ci,
  `user_id_target` int(11) DEFAULT NULL,
  `hak_akses_id_target` int(11) DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_aktif` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.notifikasi: ~24 rows (lebih kurang)
DELETE FROM `notifikasi`;
/*!40000 ALTER TABLE `notifikasi` DISABLE KEYS */;
INSERT INTO `notifikasi` (`id`, `kode`, `pesan`, `user_id_target`, `hak_akses_id_target`, `link`, `is_aktif`, `created_at`, `updated_at`) VALUES
	(1, 'registrasi', '<i class="fa fa-user"> Registrasi baru akun Muhammad Riduwan</i>', NULL, 1, 'data_user', 0, '2016-10-20 19:30:18', '2016-10-20 19:30:50'),
	(2, 'sales_order', '<i class="fa fa-shopping-cart"> Order Baru oleh akun guest</i>', NULL, 1, 'sales_order', 0, '2016-10-21 03:26:55', '2016-10-31 20:04:30'),
	(3, 'registrasi', '<i class="fa fa-user"> Registrasi baru akun Dody Soegiarto</i>', NULL, 1, 'data_user', 0, '2016-10-24 10:18:59', '2016-10-31 20:04:45'),
	(4, 'registrasi', '<i class="fa fa-user"> Registrasi baru akun Red</i>', NULL, 1, 'data_user', 0, '2016-10-24 13:31:51', '2016-10-31 20:04:35'),
	(5, 'sales_order', '<i class="fa fa-shopping-cart"> Order Baru oleh akun Datamart Computer</i>', NULL, 1, 'sales_order', 0, '2016-10-29 03:31:17', '2016-10-31 20:04:48'),
	(6, 'sales_order', '<i class="fa fa-shopping-cart"> Order Baru oleh akun Muhammad Riduwan</i>', NULL, 1, 'sales_order', 0, '2016-10-29 03:33:39', '2016-10-31 20:04:43'),
	(7, 'sales_order', '<i class="fa fa-shopping-cart"> Order Baru oleh akun Muhammad Riduwan</i>', NULL, 1, 'sales_order', 0, '2016-10-29 03:35:16', '2016-10-31 20:04:40'),
	(8, 'sales_order', '<i class="fa fa-shopping-cart"> Order Baru oleh akun Datamart Computer</i>', NULL, 1, 'sales_order', 0, '2016-10-29 03:39:37', '2016-10-31 20:04:27'),
	(9, 'sales_order', '<i class="fa fa-shopping-cart"> Order Baru oleh akun guest</i>', NULL, 1, 'sales_order', 0, '2016-11-01 16:17:05', '2016-11-01 09:18:42'),
	(10, 'sales_order', '<i class="fa fa-shopping-cart"> Order Baru oleh akun Muhammad Riduwan</i>', NULL, 1, 'sales_order', 0, '2016-11-05 03:04:56', '2016-11-04 20:32:19'),
	(11, 'sales_order', '<i class="fa fa-shopping-cart"> Order Baru oleh akun guest</i>', NULL, 1, 'sales_order', 0, '2016-11-07 23:27:22', '2016-11-07 16:32:46'),
	(12, 'sales_order', '<i class="fa fa-shopping-cart"> Order Baru oleh akun Muhammad Riduwan</i>', NULL, 1, 'sales_order', 0, '2016-11-26 01:40:15', '2017-01-06 07:26:37'),
	(13, 'sales_order', '<i class="fa fa-shopping-cart"> Order Baru oleh akun guest</i>', NULL, 1, 'sales_order', 1, '2016-12-07 23:18:27', '2016-12-07 23:18:27'),
	(14, 'sales_order', '<i class="fa fa-shopping-cart"> Order Baru oleh akun guest</i>', NULL, 1, 'sales_order', 1, '2016-12-09 01:54:56', '2016-12-09 01:54:56'),
	(15, 'registrasi', '<i class="fa fa-user"> Registrasi baru akun M Beny Pangestu</i>', NULL, 1, 'data_user', 1, '2016-12-13 20:06:29', '2016-12-13 20:06:29'),
	(16, 'sales_order', '<i class="fa fa-shopping-cart"> Order Baru oleh akun guest</i>', NULL, 1, 'sales_order', 1, '2016-12-20 02:15:29', '2016-12-20 02:15:29'),
	(17, 'registrasi', '<i class="fa fa-user"> Registrasi baru akun Dino Anggoro</i>', NULL, 1, 'data_user', 0, '2016-12-19 19:19:16', '2016-12-21 08:57:25'),
	(18, 'sales_order', '<i class="fa fa-shopping-cart"> Order Baru oleh akun Muhammad Riduwan</i>', NULL, 1, 'sales_order', 1, '2016-12-20 02:21:42', '2016-12-20 02:21:42'),
	(19, 'sales_order', '<i class="fa fa-shopping-cart"> Order Baru oleh akun Muhammad Riduwan</i>', NULL, 1, 'sales_order', 1, '2016-12-20 02:32:09', '2016-12-20 02:32:09'),
	(20, 'registrasi', '<i class="fa fa-user"> Registrasi baru akun Taufiq Rohman</i>', NULL, 1, 'data_user', 1, '2016-12-21 13:41:44', '2016-12-21 13:41:44'),
	(21, 'sales_order', '<i class="fa fa-shopping-cart"> Order Baru oleh akun guest</i>', NULL, 1, 'sales_order', 1, '2016-12-28 04:39:03', '2016-12-28 04:39:03'),
	(22, 'registrasi', '<i class="fa fa-user"> Registrasi baru akun Maysarah</i>', NULL, 1, 'data_user', 1, '2016-12-28 13:34:37', '2016-12-28 13:34:37'),
	(23, 'registrasi', '<i class="fa fa-user"> Registrasi baru akun Fendi Septiawan</i>', NULL, 1, 'data_user', 1, '2017-01-06 07:46:20', '2017-01-06 07:46:20'),
	(24, 'registrasi', '<i class="fa fa-user"> Registrasi baru akun Fendi Septiawan</i>', NULL, 1, 'data_user', 1, '2017-01-16 09:10:17', '2017-01-16 09:10:17');
/*!40000 ALTER TABLE `notifikasi` ENABLE KEYS */;

-- membuang struktur untuk table pos.notif_chat
CREATE TABLE IF NOT EXISTS `notif_chat` (
  `user_id` int(11) NOT NULL,
  `user_id_from` int(11) NOT NULL,
  `pesan` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.notif_chat: ~10 rows (lebih kurang)
DELETE FROM `notif_chat`;
/*!40000 ALTER TABLE `notif_chat` DISABLE KEYS */;
INSERT INTO `notif_chat` (`user_id`, `user_id_from`, `pesan`) VALUES
	(4, 1, '{"id_from": "4","tanggal": "28/10/2016 19:39:39","pesan": "cek"}'),
	(4, 1, '{"id_from": "4","tanggal": "28/10/2016 19:40:24","pesan": "cek"}'),
	(4, 1, '{"id_from": "4","tanggal": "28/10/2016 19:40:48","pesan": "oke"}'),
	(4, 1, '{"id_from": "4","tanggal": "28/10/2016 19:41:9","pesan": "baik"}'),
	(4, 1, '{"id_from": "4","tanggal": "31/10/2016 17:29:57","pesan": "iyo"}'),
	(4, 1, '{"id_from": "4","tanggal": "31/10/2016 17:30:5","pesan": "iso jadine"}'),
	(4, 1, '{"id_from": "4","tanggal": "31/10/2016 17:31:48","pesan": "selamat sore apa ada yang bisa saya bantu??"}'),
	(1, 6, '{"id_from": "1","tanggal": "8/11/2016 20:0:9","pesan": "apa Kabar?"}'),
	(1, 6, '{"id_from": "1","tanggal": "8/11/2016 20:0:9","pesan": "apa Kabar?"}'),
	(1, 6, '{"id_from": "1","tanggal": "8/11/2016 20:0:10","pesan": "apa Kabar?"}');
/*!40000 ALTER TABLE `notif_chat` ENABLE KEYS */;

-- membuang struktur untuk table pos.oauth_access_tokens
CREATE TABLE IF NOT EXISTS `oauth_access_tokens` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `session_id` int(10) unsigned NOT NULL,
  `expire_time` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `oauth_access_tokens_id_session_id_unique` (`id`,`session_id`),
  KEY `oauth_access_tokens_session_id_index` (`session_id`),
  CONSTRAINT `oauth_access_tokens_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `oauth_sessions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.oauth_access_tokens: ~0 rows (lebih kurang)
DELETE FROM `oauth_access_tokens`;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;

-- membuang struktur untuk table pos.oauth_access_token_scopes
CREATE TABLE IF NOT EXISTS `oauth_access_token_scopes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `access_token_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `scope_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_token_scopes_access_token_id_index` (`access_token_id`),
  KEY `oauth_access_token_scopes_scope_id_index` (`scope_id`),
  CONSTRAINT `oauth_access_token_scopes_access_token_id_foreign` FOREIGN KEY (`access_token_id`) REFERENCES `oauth_access_tokens` (`id`) ON DELETE CASCADE,
  CONSTRAINT `oauth_access_token_scopes_scope_id_foreign` FOREIGN KEY (`scope_id`) REFERENCES `oauth_scopes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.oauth_access_token_scopes: ~0 rows (lebih kurang)
DELETE FROM `oauth_access_token_scopes`;
/*!40000 ALTER TABLE `oauth_access_token_scopes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_access_token_scopes` ENABLE KEYS */;

-- membuang struktur untuk table pos.oauth_auth_codes
CREATE TABLE IF NOT EXISTS `oauth_auth_codes` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `session_id` int(10) unsigned NOT NULL,
  `redirect_uri` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expire_time` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_auth_codes_session_id_index` (`session_id`),
  CONSTRAINT `oauth_auth_codes_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `oauth_sessions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.oauth_auth_codes: ~0 rows (lebih kurang)
DELETE FROM `oauth_auth_codes`;
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;

-- membuang struktur untuk table pos.oauth_auth_code_scopes
CREATE TABLE IF NOT EXISTS `oauth_auth_code_scopes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `auth_code_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `scope_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_auth_code_scopes_auth_code_id_index` (`auth_code_id`),
  KEY `oauth_auth_code_scopes_scope_id_index` (`scope_id`),
  CONSTRAINT `oauth_auth_code_scopes_auth_code_id_foreign` FOREIGN KEY (`auth_code_id`) REFERENCES `oauth_auth_codes` (`id`) ON DELETE CASCADE,
  CONSTRAINT `oauth_auth_code_scopes_scope_id_foreign` FOREIGN KEY (`scope_id`) REFERENCES `oauth_scopes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.oauth_auth_code_scopes: ~0 rows (lebih kurang)
DELETE FROM `oauth_auth_code_scopes`;
/*!40000 ALTER TABLE `oauth_auth_code_scopes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_code_scopes` ENABLE KEYS */;

-- membuang struktur untuk table pos.oauth_clients
CREATE TABLE IF NOT EXISTS `oauth_clients` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `secret` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `oauth_clients_id_secret_unique` (`id`,`secret`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.oauth_clients: ~0 rows (lebih kurang)
DELETE FROM `oauth_clients`;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
INSERT INTO `oauth_clients` (`id`, `secret`, `name`, `created_at`, `updated_at`) VALUES
	('$2y$10$AWOeyHTKkMQvPbmgDuvAj.Uq8M4DTKsVH', '$2y$10$a5X2uHImYOBjbqIC2pPsBeMSmnGxW4Hz0', 'API Mobile', NULL, NULL);
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;

-- membuang struktur untuk table pos.oauth_client_endpoints
CREATE TABLE IF NOT EXISTS `oauth_client_endpoints` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `redirect_uri` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `oauth_client_endpoints_client_id_redirect_uri_unique` (`client_id`,`redirect_uri`),
  CONSTRAINT `oauth_client_endpoints_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `oauth_clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.oauth_client_endpoints: ~0 rows (lebih kurang)
DELETE FROM `oauth_client_endpoints`;
/*!40000 ALTER TABLE `oauth_client_endpoints` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_client_endpoints` ENABLE KEYS */;

-- membuang struktur untuk table pos.oauth_client_grants
CREATE TABLE IF NOT EXISTS `oauth_client_grants` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `grant_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_client_grants_client_id_index` (`client_id`),
  KEY `oauth_client_grants_grant_id_index` (`grant_id`),
  CONSTRAINT `oauth_client_grants_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `oauth_clients` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `oauth_client_grants_grant_id_foreign` FOREIGN KEY (`grant_id`) REFERENCES `oauth_grants` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.oauth_client_grants: ~0 rows (lebih kurang)
DELETE FROM `oauth_client_grants`;
/*!40000 ALTER TABLE `oauth_client_grants` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_client_grants` ENABLE KEYS */;

-- membuang struktur untuk table pos.oauth_client_scopes
CREATE TABLE IF NOT EXISTS `oauth_client_scopes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `scope_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_client_scopes_client_id_index` (`client_id`),
  KEY `oauth_client_scopes_scope_id_index` (`scope_id`),
  CONSTRAINT `oauth_client_scopes_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `oauth_clients` (`id`) ON DELETE CASCADE,
  CONSTRAINT `oauth_client_scopes_scope_id_foreign` FOREIGN KEY (`scope_id`) REFERENCES `oauth_scopes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.oauth_client_scopes: ~0 rows (lebih kurang)
DELETE FROM `oauth_client_scopes`;
/*!40000 ALTER TABLE `oauth_client_scopes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_client_scopes` ENABLE KEYS */;

-- membuang struktur untuk table pos.oauth_grants
CREATE TABLE IF NOT EXISTS `oauth_grants` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.oauth_grants: ~0 rows (lebih kurang)
DELETE FROM `oauth_grants`;
/*!40000 ALTER TABLE `oauth_grants` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_grants` ENABLE KEYS */;

-- membuang struktur untuk table pos.oauth_grant_scopes
CREATE TABLE IF NOT EXISTS `oauth_grant_scopes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `grant_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `scope_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_grant_scopes_grant_id_index` (`grant_id`),
  KEY `oauth_grant_scopes_scope_id_index` (`scope_id`),
  CONSTRAINT `oauth_grant_scopes_grant_id_foreign` FOREIGN KEY (`grant_id`) REFERENCES `oauth_grants` (`id`) ON DELETE CASCADE,
  CONSTRAINT `oauth_grant_scopes_scope_id_foreign` FOREIGN KEY (`scope_id`) REFERENCES `oauth_scopes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.oauth_grant_scopes: ~0 rows (lebih kurang)
DELETE FROM `oauth_grant_scopes`;
/*!40000 ALTER TABLE `oauth_grant_scopes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_grant_scopes` ENABLE KEYS */;

-- membuang struktur untuk table pos.oauth_refresh_tokens
CREATE TABLE IF NOT EXISTS `oauth_refresh_tokens` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `access_token_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `expire_time` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`access_token_id`),
  UNIQUE KEY `oauth_refresh_tokens_id_unique` (`id`),
  CONSTRAINT `oauth_refresh_tokens_access_token_id_foreign` FOREIGN KEY (`access_token_id`) REFERENCES `oauth_access_tokens` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.oauth_refresh_tokens: ~0 rows (lebih kurang)
DELETE FROM `oauth_refresh_tokens`;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;

-- membuang struktur untuk table pos.oauth_scopes
CREATE TABLE IF NOT EXISTS `oauth_scopes` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.oauth_scopes: ~0 rows (lebih kurang)
DELETE FROM `oauth_scopes`;
/*!40000 ALTER TABLE `oauth_scopes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_scopes` ENABLE KEYS */;

-- membuang struktur untuk table pos.oauth_sessions
CREATE TABLE IF NOT EXISTS `oauth_sessions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `owner_type` enum('client','user') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'user',
  `owner_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_redirect_uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_sessions_client_id_owner_type_owner_id_index` (`client_id`,`owner_type`,`owner_id`),
  CONSTRAINT `oauth_sessions_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `oauth_clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.oauth_sessions: ~0 rows (lebih kurang)
DELETE FROM `oauth_sessions`;
/*!40000 ALTER TABLE `oauth_sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_sessions` ENABLE KEYS */;

-- membuang struktur untuk table pos.oauth_session_scopes
CREATE TABLE IF NOT EXISTS `oauth_session_scopes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` int(10) unsigned NOT NULL,
  `scope_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_session_scopes_session_id_index` (`session_id`),
  KEY `oauth_session_scopes_scope_id_index` (`scope_id`),
  CONSTRAINT `oauth_session_scopes_scope_id_foreign` FOREIGN KEY (`scope_id`) REFERENCES `oauth_scopes` (`id`) ON DELETE CASCADE,
  CONSTRAINT `oauth_session_scopes_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `oauth_sessions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.oauth_session_scopes: ~0 rows (lebih kurang)
DELETE FROM `oauth_session_scopes`;
/*!40000 ALTER TABLE `oauth_session_scopes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_session_scopes` ENABLE KEYS */;

-- membuang struktur untuk table pos.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.password_resets: ~0 rows (lebih kurang)
DELETE FROM `password_resets`;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- membuang struktur untuk table pos.shoppingcart
CREATE TABLE IF NOT EXISTS `shoppingcart` (
  `identifier` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `instance` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`identifier`,`instance`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.shoppingcart: ~0 rows (lebih kurang)
DELETE FROM `shoppingcart`;
/*!40000 ALTER TABLE `shoppingcart` DISABLE KEYS */;
/*!40000 ALTER TABLE `shoppingcart` ENABLE KEYS */;

-- membuang struktur untuk table pos.static
CREATE TABLE IF NOT EXISTS `static` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.static: ~2 rows (lebih kurang)
DELETE FROM `static`;
/*!40000 ALTER TABLE `static` DISABLE KEYS */;
INSERT INTO `static` (`id`, `nama`, `value`, `created_at`, `updated_at`) VALUES
	(1, 'gambar_produk_default', 'GPDQHAPO77Y.jpg', NULL, NULL),
	(2, 'cara_belanja', '<h2>Belanja di galerindoteknologi.com kini semakin Lengkap, mudah, dan aman</h2>\r\n<p>Belanja gadget, notebook, kamera, elektronik, dan seluruh kebutuhan Anda, Anti Repot! Dari mulai sensasi belanja online, berkonsultasi langsung via telepon/email/chat, hingga kunjungan langsung ke store. Bahkan sekarang bisa belanja via Smartphone dan Gadget Anda dengan Bhinneka.Com Mobile Version, Bhinneka Apps bahkan bisa langsung hitung Tarif Kirim dengan Aplikasi Tarif Kirim.<br />Belanja Online Via Shopcart<br />Cara belanja paling MUDAH hanya dengan klik tombol \'BELI\'di halaman \'Product List\' atau di \'Detail Page\' produk yang Anda inginkan, maka produk tersebut akan masuk ke keranjang belanja Anda (Shop Cart). Selanjutnya ikuti langkah demi langkah PERINCIAN BELANJA ANDA : Data Pengiriman &rarr; Cara Pembayaran &rarr; Konfirmasi hingga selesai.</p>\r\n<h2>Cara Berbelanja</h2>\r\n<p>Saran kami, untuk memudahkan proses berbelanja Online di masa yang akan datang, daftarlah menjadi Member galerindoteknologi.com terlebih dahulu. Selain dapat menghemat waktu berbelanja, Anda juga dapat melihat histori transaksi belanja, memiliki kesempatan berbelanja dengan diskon khusus member berupa eCoupon, dan masih banyak lagi keuntungan lainnya.</p>\r\n<p>&nbsp;</p>', NULL, NULL);
/*!40000 ALTER TABLE `static` ENABLE KEYS */;

-- membuang struktur untuk table pos.tema
CREATE TABLE IF NOT EXISTS `tema` (
  `warna_dasar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `warna_huruf` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `layout_id` int(11) NOT NULL,
  `background_id` int(11) NOT NULL,
  `tombol_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tema: ~0 rows (lebih kurang)
DELETE FROM `tema`;
/*!40000 ALTER TABLE `tema` DISABLE KEYS */;
/*!40000 ALTER TABLE `tema` ENABLE KEYS */;

-- membuang struktur untuk table pos.template_background
CREATE TABLE IF NOT EXISTS `template_background` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_gambar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.template_background: ~0 rows (lebih kurang)
DELETE FROM `template_background`;
/*!40000 ALTER TABLE `template_background` DISABLE KEYS */;
/*!40000 ALTER TABLE `template_background` ENABLE KEYS */;

-- membuang struktur untuk table pos.template_layout
CREATE TABLE IF NOT EXISTS `template_layout` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.template_layout: ~0 rows (lebih kurang)
DELETE FROM `template_layout`;
/*!40000 ALTER TABLE `template_layout` DISABLE KEYS */;
/*!40000 ALTER TABLE `template_layout` ENABLE KEYS */;

-- membuang struktur untuk table pos.template_tombol
CREATE TABLE IF NOT EXISTS `template_tombol` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jenis` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_icon_button` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `btn_primary` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `btn_default` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `btn_warning` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `btn_danger` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `btn_info` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.template_tombol: ~0 rows (lebih kurang)
DELETE FROM `template_tombol`;
/*!40000 ALTER TABLE `template_tombol` DISABLE KEYS */;
/*!40000 ALTER TABLE `template_tombol` ENABLE KEYS */;

-- membuang struktur untuk table pos.temp_alamat_pengiriman
CREATE TABLE IF NOT EXISTS `temp_alamat_pengiriman` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `no_nota` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alamat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kota_id` int(11) DEFAULT NULL,
  `kode_pos` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metode_pengiriman_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.temp_alamat_pengiriman: ~4 rows (lebih kurang)
DELETE FROM `temp_alamat_pengiriman`;
/*!40000 ALTER TABLE `temp_alamat_pengiriman` DISABLE KEYS */;
INSERT INTO `temp_alamat_pengiriman` (`id`, `no_nota`, `tanggal`, `nama`, `email`, `alamat`, `kota_id`, `kode_pos`, `hp`, `metode_pengiriman_id`, `created_at`, `updated_at`) VALUES
	(13, NULL, '2016-10-28 20:38:30', 'Datamart Computer', 'dodyformello@gmail.com', 'Berbek Industri II/18', 242, '61256', '(+62)812-3278-760', 1, '2016-10-28 13:38:30', '2016-10-28 13:38:30'),
	(19, NULL, '2016-12-07 15:30:17', 'Muhammad Riduwan', 'ridwan.yaa71@gmail.com', 'aadasdasdjiououiouuoijio', 363, '09099', '908908098', 2, '2016-12-07 08:30:17', '2016-12-07 08:30:17'),
	(24, NULL, '2016-12-19 19:24:20', 'Muhammad Riduwan', 'ridwan.yaa71@gmail.com', 'keputih Gg Makam Blok D-11, Sukolilo', 264, '60111', '(+62)857-3511-2973', 2, '2016-12-19 12:24:20', '2016-12-19 12:24:20'),
	(26, NULL, '2016-12-27 18:57:00', 'M Beny Pangestu', 'mbenypangestu@gmail.com', 'Surabaya', 240, '67100', '(+62)823-3490-1664', 1, '2016-12-27 11:57:00', '2016-12-27 11:57:00');
/*!40000 ALTER TABLE `temp_alamat_pengiriman` ENABLE KEYS */;

-- membuang struktur untuk table pos.temp_pembayaran
CREATE TABLE IF NOT EXISTS `temp_pembayaran` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `no_nota` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tanggal` bigint(20) DEFAULT NULL,
  `metode_pembayaran_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bayar_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomor_pembayaran` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.temp_pembayaran: ~10 rows (lebih kurang)
DELETE FROM `temp_pembayaran`;
/*!40000 ALTER TABLE `temp_pembayaran` DISABLE KEYS */;
INSERT INTO `temp_pembayaran` (`id`, `no_nota`, `tanggal`, `metode_pembayaran_id`, `bayar_id`, `nomor_pembayaran`, `bank_id`, `created_at`, `updated_at`) VALUES
	(13, NULL, 2016, '6', NULL, NULL, NULL, '2016-10-28 13:35:39', '2016-10-28 13:35:39'),
	(14, NULL, 2016, '6', NULL, NULL, NULL, '2016-10-28 13:38:35', '2016-10-28 13:38:35'),
	(19, NULL, 2016, '6', NULL, NULL, NULL, '2016-11-25 11:39:18', '2016-11-25 11:39:18'),
	(20, NULL, 2016, '6', NULL, NULL, NULL, '2016-12-07 08:31:05', '2016-12-07 08:31:05'),
	(21, NULL, 2016, '6', NULL, NULL, NULL, '2016-12-07 09:18:24', '2016-12-07 09:18:24'),
	(22, NULL, 2016, '6', NULL, NULL, NULL, '2016-12-08 11:54:49', '2016-12-08 11:54:49'),
	(23, NULL, 2016, '6', NULL, NULL, NULL, '2016-12-19 12:15:08', '2016-12-19 12:15:08'),
	(24, NULL, 2016, '6', NULL, NULL, NULL, '2016-12-19 12:21:31', '2016-12-19 12:21:31'),
	(25, NULL, 2016, '6', NULL, NULL, NULL, '2016-12-19 12:31:51', '2016-12-19 12:31:51'),
	(26, NULL, 2016, '6', NULL, NULL, NULL, '2016-12-27 14:38:51', '2016-12-27 14:38:51');
/*!40000 ALTER TABLE `temp_pembayaran` ENABLE KEYS */;

-- membuang struktur untuk table pos.temp_so_voucher
CREATE TABLE IF NOT EXISTS `temp_so_voucher` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `no_nota` int(11) DEFAULT NULL,
  `voucher_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.temp_so_voucher: ~0 rows (lebih kurang)
DELETE FROM `temp_so_voucher`;
/*!40000 ALTER TABLE `temp_so_voucher` DISABLE KEYS */;
/*!40000 ALTER TABLE `temp_so_voucher` ENABLE KEYS */;

-- membuang struktur untuk table pos.temp_stok_opname
CREATE TABLE IF NOT EXISTS `temp_stok_opname` (
  `id` int(11) NOT NULL,
  `gudang_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `produk_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `stok` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.temp_stok_opname: ~0 rows (lebih kurang)
DELETE FROM `temp_stok_opname`;
/*!40000 ALTER TABLE `temp_stok_opname` DISABLE KEYS */;
/*!40000 ALTER TABLE `temp_stok_opname` ENABLE KEYS */;

-- membuang struktur untuk table pos.tmst_cabang_perusahaan
CREATE TABLE IF NOT EXISTS `tmst_cabang_perusahaan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alamat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kota_id` int(11) DEFAULT NULL,
  `telp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kontak_person` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `perusahaan_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tmst_cabang_perusahaan: ~0 rows (lebih kurang)
DELETE FROM `tmst_cabang_perusahaan`;
/*!40000 ALTER TABLE `tmst_cabang_perusahaan` DISABLE KEYS */;
INSERT INTO `tmst_cabang_perusahaan` (`id`, `nama`, `alamat`, `kota_id`, `telp`, `kontak_person`, `perusahaan_id`, `created_at`, `updated_at`) VALUES
	(1, 'Indonusa Solutama', 'Ruko Palacio B-20 Nginden', 264, '(0812)-3332243', 'Novi Rogojampii', 1, NULL, '2017-01-07 06:44:57');
/*!40000 ALTER TABLE `tmst_cabang_perusahaan` ENABLE KEYS */;

-- membuang struktur untuk table pos.tmst_gudang
CREATE TABLE IF NOT EXISTS `tmst_gudang` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alamat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kota_id` int(11) DEFAULT NULL,
  `telp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supervisor` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hp_supervisor` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_aktif` int(11) DEFAULT NULL,
  `format_no_mutasi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tmst_gudang: ~2 rows (lebih kurang)
DELETE FROM `tmst_gudang`;
/*!40000 ALTER TABLE `tmst_gudang` DISABLE KEYS */;
INSERT INTO `tmst_gudang` (`id`, `nama`, `alamat`, `kota_id`, `telp`, `supervisor`, `hp_supervisor`, `longitude`, `latitude`, `is_aktif`, `format_no_mutasi`, `created_at`, `updated_at`) VALUES
	(1, 'Gudang Toko', 'Hi Tech Mall Lt.Dasar Blok.D No.17-19\r\nJl. Kusuma Bangsa No.116-118', 264, '0315477085', 'Vera', '(+62)8123-0434343', '', '', 1, '', NULL, NULL),
	(2, 'Gudang Kantor', 'Ruko Palacio | City Pride\r\nJl. Nginden Semolo No.42 Blok.B20', 264, '0315913493', 'Dian', '(+62)8123-456789', '', '', 1, '', NULL, NULL);
/*!40000 ALTER TABLE `tmst_gudang` ENABLE KEYS */;

-- membuang struktur untuk table pos.tmst_hadiah
CREATE TABLE IF NOT EXISTS `tmst_hadiah` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `satuan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `berat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deskripsi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stok` int(11) DEFAULT NULL,
  `stok_dipesan` int(11) DEFAULT NULL,
  `file_gambar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_aktif` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `jenis_barang_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tmst_hadiah: ~3 rows (lebih kurang)
DELETE FROM `tmst_hadiah`;
/*!40000 ALTER TABLE `tmst_hadiah` DISABLE KEYS */;
INSERT INTO `tmst_hadiah` (`id`, `kode`, `nama`, `satuan`, `berat`, `deskripsi`, `stok`, `stok_dipesan`, `file_gambar`, `is_aktif`, `created_at`, `updated_at`, `jenis_barang_id`) VALUES
	(1, NULL, 'Flashdisk 16GB', 'Biji', '0', '<p>Flashdisk 16GB recovery windows</p>', 0, 0, 'HDHGEKNCRUS.jpg', NULL, NULL, NULL, NULL),
	(2, NULL, 'Flashdisk 8GB', 'Biji', '0', '<p>Flashdisk 8GB recovery windows</p>', 0, 0, '', NULL, NULL, NULL, NULL),
	(3, NULL, 'Keyboard Protector 15" Clear', 'Biji', '0', '<p>Free Keyboard Protector u/ pembelian Notebook 15"</p>', 0, 0, '', 1, NULL, NULL, NULL);
/*!40000 ALTER TABLE `tmst_hadiah` ENABLE KEYS */;

-- membuang struktur untuk table pos.tmst_kategori_pelanggan
CREATE TABLE IF NOT EXISTS `tmst_kategori_pelanggan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deskripsi` longtext COLLATE utf8_unicode_ci,
  `default_diskon` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tmst_kategori_pelanggan: ~0 rows (lebih kurang)
DELETE FROM `tmst_kategori_pelanggan`;
/*!40000 ALTER TABLE `tmst_kategori_pelanggan` DISABLE KEYS */;
INSERT INTO `tmst_kategori_pelanggan` (`id`, `nama`, `deskripsi`, `default_diskon`, `created_at`, `updated_at`) VALUES
	(1, 'Diamond Customer', 'Omzet Penjualan di atas Rp. 200,000,000 per bulan', 5, NULL, NULL);
/*!40000 ALTER TABLE `tmst_kategori_pelanggan` ENABLE KEYS */;

-- membuang struktur untuk table pos.tmst_kategori_produk
CREATE TABLE IF NOT EXISTS `tmst_kategori_produk` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug_nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deskripsi` longtext COLLATE utf8_unicode_ci,
  `kolom_spesifikasi` text COLLATE utf8_unicode_ci,
  `is_aktif` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `file_gambar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tmst_kategori_produk: ~10 rows (lebih kurang)
DELETE FROM `tmst_kategori_produk`;
/*!40000 ALTER TABLE `tmst_kategori_produk` DISABLE KEYS */;
INSERT INTO `tmst_kategori_produk` (`id`, `nama`, `slug_nama`, `deskripsi`, `kolom_spesifikasi`, `is_aktif`, `created_at`, `updated_at`, `file_gambar`) VALUES
	(1, 'Accesories Notebook', 'accesories-notebook', 'Accesories Notebook', '', 1, NULL, '2016-12-06 07:56:44', 'accesories-notebook.png'),
	(2, 'All In One', 'all-in-one', 'All In One', '', 1, NULL, '2016-12-06 07:57:01', 'all-in-one.png'),
	(3, 'LED Monitor', 'led-monitor', 'LED Monitor', '', 1, NULL, '2016-12-06 07:57:17', 'led-monitor.png'),
	(4, 'Notebook', 'notebook', 'Notebook', '', 1, NULL, '2016-12-06 07:57:40', 'notebook.png'),
	(5, 'PC Desktop', 'pc-desktop', 'PC Desktop', '', 1, NULL, '2016-12-06 07:58:47', 'pc-desktop.png'),
	(6, 'Server', 'server', 'Server', '', 1, NULL, '2016-12-06 07:59:03', 'server.png'),
	(7, 'Software', 'software', 'Software', '', 1, NULL, '2016-12-06 07:59:29', 'software.png'),
	(8, 'Sparepart', 'sparepart', 'Sparepart', '', 1, NULL, '2016-12-06 08:00:02', 'sparepart.png'),
	(9, 'Workstation', 'workstation', 'Workstation', '', 1, NULL, '2016-12-06 08:00:18', 'workstation.png'),
	(11, 'Semua Kategori', 'semua-kategori', NULL, NULL, 0, NULL, NULL, 'semua-kategori.png');
/*!40000 ALTER TABLE `tmst_kategori_produk` ENABLE KEYS */;

-- membuang struktur untuk table pos.tmst_master_coa
CREATE TABLE IF NOT EXISTS `tmst_master_coa` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `parent` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `debit_or_kredit` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_kode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tmst_master_coa: ~9 rows (lebih kurang)
DELETE FROM `tmst_master_coa`;
/*!40000 ALTER TABLE `tmst_master_coa` DISABLE KEYS */;
INSERT INTO `tmst_master_coa` (`id`, `kode`, `nama`, `level`, `parent`, `debit_or_kredit`, `group_kode`, `created_at`, `updated_at`) VALUES
	(1, '001', 'Kas', 0, '-', '', '002', '2016-10-03 08:07:05', '2016-11-21 06:11:14'),
	(2, '001.001', 'Kas rupiah', 1, '001', '', '', '2016-10-05 19:13:45', '2016-10-05 19:13:45'),
	(3, '001.002', 'Kas USD', 1, '001', '', '', '2016-10-05 19:14:21', '2016-10-05 19:14:21'),
	(4, '002', 'Persediaan', 0, '-', '', '002', '2016-10-05 19:19:35', '2016-11-21 06:11:38'),
	(5, '003', 'Piutang', 0, '-', '', '002', '2016-10-05 19:20:47', '2016-11-21 06:12:04'),
	(6, '003.001', 'Piutang Usaha', 1, '003', '', '', '2016-10-05 19:26:14', '2016-11-21 07:06:45'),
	(10, '004', 'Hutang', 0, '-', '', '003', '2016-11-21 06:12:29', '2016-11-21 06:12:29'),
	(11, '004.001', 'Hutang Usaha', 1, '004', '', '', '2016-11-21 06:12:43', '2016-11-21 06:12:43'),
	(13, '005.001', 'Uji', 1, '005', NULL, '', '2017-01-06 15:04:27', '2017-01-06 15:04:27');
/*!40000 ALTER TABLE `tmst_master_coa` ENABLE KEYS */;

-- membuang struktur untuk table pos.tmst_paket
CREATE TABLE IF NOT EXISTS `tmst_paket` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug_nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `harga_total` bigint(20) DEFAULT NULL,
  `deskripsi` longtext COLLATE utf8_unicode_ci,
  `file_gambar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stok` int(11) DEFAULT NULL,
  `stok_dipesan` int(11) DEFAULT NULL,
  `is_aktif` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `jenis_barang_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tmst_paket: ~2 rows (lebih kurang)
DELETE FROM `tmst_paket`;
/*!40000 ALTER TABLE `tmst_paket` DISABLE KEYS */;
INSERT INTO `tmst_paket` (`id`, `kode`, `nama`, `slug_nama`, `harga_total`, `deskripsi`, `file_gambar`, `stok`, `stok_dipesan`, `is_aktif`, `created_at`, `updated_at`, `jenis_barang_id`) VALUES
	(1, 'PROMO-2017', 'Paket Promo Awal Tahun', 'paket-promo-awal-tahun', 7000000, NULL, '', 0, 0, 1, NULL, NULL, 3),
	(2, 'ABC', 'Paket Awal Tahun', 'paket-awal-tahun', 10000000, NULL, 'PKTDQ3EP2NQ.jpg', 0, 0, 1, NULL, NULL, 3);
/*!40000 ALTER TABLE `tmst_paket` ENABLE KEYS */;

-- membuang struktur untuk table pos.tmst_pelanggan
CREATE TABLE IF NOT EXISTS `tmst_pelanggan` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alamat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kota_id` int(11) DEFAULT NULL,
  `telp1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telp2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kode_pos` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama_sales` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hp_sales` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deskripsi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kategori_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tmst_pelanggan: ~12 rows (lebih kurang)
DELETE FROM `tmst_pelanggan`;
/*!40000 ALTER TABLE `tmst_pelanggan` DISABLE KEYS */;
INSERT INTO `tmst_pelanggan` (`id`, `user_id`, `nama`, `email`, `alamat`, `kota_id`, `telp1`, `telp2`, `kode_pos`, `nama_sales`, `hp_sales`, `deskripsi`, `kategori_id`, `created_at`, `updated_at`) VALUES
	(1, NULL, 'guest', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 4, 'Peter Parker', 'peter@gmail.com', 'jalan raya nginden semolo', 264, '031-89898989', NULL, '60111', NULL, '(+62)098-9089-0809', NULL, 0, NULL, NULL),
	(3, 5, 'Barry Alan', 'barry@gmail.com', 'keputih', 264, '898-80980980', NULL, '60111', NULL, '(+62)808-0980-9808', NULL, 0, NULL, NULL),
	(4, 6, 'Datamart Computer', 'dodyformello@gmail.com', 'Berbek Industri II/18', 242, '0318547652', NULL, '61256', NULL, '(+62)812-3278-760', NULL, 1, NULL, NULL),
	(5, 7, 'Budi', 'budi@gmail.com', 'Jl. Undaan No.43', 264, NULL, NULL, NULL, NULL, '(+62)812-3043-4343', NULL, NULL, NULL, NULL),
	(6, 2, 'Silvi Anita Putri', 'silvi.galerindo@gmail.com', 'Jl. Nginden Semolo No.42 Blok.B-20', 264, NULL, NULL, NULL, NULL, '(+62)8523-4628857', NULL, NULL, NULL, NULL),
	(15, 8, 'Muhammad Riduwan', 'ridwan.yaa71@gmail.com', 'keputih Gg Makam Blok D-11, Sukolilo', 264, '', NULL, '60111', NULL, '(+62)857-3511-2973', NULL, 1, NULL, NULL),
	(16, 10, 'Red', 'red_chm@yahoo.co.id', 'cek123', 12, '8089080980', NULL, '98089', NULL, '(+62)080-9890-8908', NULL, 1, NULL, NULL),
	(17, 9, 'Dody Soegiarto', 'dody_formello@yahoo.com', 'Berbek Industri II/18', 264, '', NULL, '61256', NULL, '', NULL, 1, NULL, NULL),
	(18, 11, 'M Beny Pangestu', 'mbenypangestu@gmail.com', 'Surabaya', 240, '0335841231', NULL, '67100', NULL, '(+62)823-3490-1664', NULL, 0, NULL, NULL),
	(19, 2, 'Fendi Septiawan', 'fendi_septiawan0709@yahoo.co.id', 'RT 09,\r\nRW 04,\r\nDesa Ngrami,\r\nKecamatan Sukomoro,\r\nKabupaten Nganjuk,\r\nJawa Timur,\r\nIndonesia.', 245, '', NULL, '64481', NULL, '', NULL, 1, NULL, NULL),
	(20, NULL, 'Joko', 'fendi_septiawan0709@yahoo.co.id', 'Jl. Kedurus 4.\r\nSurabaya', NULL, '085707824789', '085707824789', NULL, NULL, NULL, NULL, NULL, '2017-01-16 08:23:21', '2017-01-16 08:25:36'),
	(21, NULL, 'Fendi', 'fendi_septiawan0709@yahoo.co.id', 'Nganjuk', NULL, '085707824788', NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-10 22:05:16', '2017-02-10 22:05:16');
/*!40000 ALTER TABLE `tmst_pelanggan` ENABLE KEYS */;

-- membuang struktur untuk table pos.tmst_perusahaan
CREATE TABLE IF NOT EXISTS `tmst_perusahaan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kota_id` int(11) DEFAULT NULL,
  `telp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kontak_person` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bidang_usaha` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jam_operasional` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expired` datetime DEFAULT NULL,
  `pos_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `telp2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tmst_perusahaan: ~0 rows (lebih kurang)
DELETE FROM `tmst_perusahaan`;
/*!40000 ALTER TABLE `tmst_perusahaan` DISABLE KEYS */;
INSERT INTO `tmst_perusahaan` (`id`, `nama`, `alamat`, `kota_id`, `telp`, `kontak_person`, `bidang_usaha`, `jam_operasional`, `email`, `expired`, `pos_id`, `created_at`, `updated_at`, `telp2`) VALUES
	(1, 'Galerindo Teknologi', 'Hi Tech Mall Lt. Dasar Jl. Kusuma Bangsa D17-19\r\nJl. Kusuma Bangsa No.116-118 ', 264, '031-5348998', 'Vera Herawati', 'Dell Products & Parts', 'Mon - Fri [10:00 - 17:30] , Sat  [10:00 - 16:30]', 'admin@galerindoteknologi.com', NULL, 1, NULL, '2016-10-13 22:31:36', NULL);
/*!40000 ALTER TABLE `tmst_perusahaan` ENABLE KEYS */;

-- membuang struktur untuk table pos.tmst_produk
CREATE TABLE IF NOT EXISTS `tmst_produk` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug_nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `satuan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `berat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `spesifikasi` longtext COLLATE utf8_unicode_ci,
  `deskripsi` longtext COLLATE utf8_unicode_ci,
  `harga_retail` bigint(20) DEFAULT NULL,
  `hpp` bigint(20) DEFAULT NULL,
  `stok` int(11) DEFAULT NULL,
  `stok_dipesan` int(11) DEFAULT NULL,
  `is_aktif` int(11) DEFAULT NULL,
  `kategori_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `jenis_barang_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tmst_produk: ~0 rows (lebih kurang)
DELETE FROM `tmst_produk`;
/*!40000 ALTER TABLE `tmst_produk` DISABLE KEYS */;
INSERT INTO `tmst_produk` (`id`, `kode`, `nama`, `slug_nama`, `satuan`, `berat`, `spesifikasi`, `deskripsi`, `harga_retail`, `hpp`, `stok`, `stok_dipesan`, `is_aktif`, `kategori_id`, `created_at`, `updated_at`, `jenis_barang_id`) VALUES
	(1, 'QWERTY', 'Dell XPS 123', 'dell-xps-123', 'Buah', '3', NULL, '<p>Laptop tangguh untuk gaming</p>', 5000000, 6500000, 29, 0, 1, 4, '2017-02-09 14:28:48', '2017-02-09 21:44:16', 1);
/*!40000 ALTER TABLE `tmst_produk` ENABLE KEYS */;

-- membuang struktur untuk table pos.tmst_supplier
CREATE TABLE IF NOT EXISTS `tmst_supplier` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alamat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kota_id` int(11) DEFAULT NULL,
  `telp1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telp2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama_sales` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hp_sales` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deskripsi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `umur_hutang` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_aktif` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tmst_supplier: ~6 rows (lebih kurang)
DELETE FROM `tmst_supplier`;
/*!40000 ALTER TABLE `tmst_supplier` DISABLE KEYS */;
INSERT INTO `tmst_supplier` (`id`, `nama`, `alamat`, `kota_id`, `telp1`, `telp2`, `nama_sales`, `hp_sales`, `deskripsi`, `umur_hutang`, `is_aktif`, `created_at`, `updated_at`) VALUES
	(1, 'PT. TIXPRO INFORMATIKA MEGAH', 'RUKO ORION DUSIT NO.19-20', 158, '021-30005040', '', 'Hadiyanto Tarsley', '(+62)8128-1726999', 'Dell Counsumer Notebook Distributor', '30', 1, NULL, NULL),
	(2, 'PT. ADAKOM INTERNATIONAL TECHNOLOGY', 'Ruko Mangga Dua Square Blok H No 24 Jl.Gunung Sahari Raya No.1 Jakarta 14420', 159, '021-62317952', '021-62310458', 'Wahyoe Otnawamseor', '(+62)8211-4164770', 'Dell Consumer & Commercial Distributor', '30', 1, NULL, NULL),
	(3, 'PT. SYNNEX METRODATA INDONESIA', 'Intiland Tower Lt.7 Suite 5A  Jl. Panglima Sudirman 101-103 Surabaya 60271', 264, '031-5474218_', '031-53471479', 'Emmy', '(+62)8214-3713233', 'Dell Consumer & Comercial Distributor', '30', 1, NULL, NULL),
	(4, 'PT. SMARTINDO INTEGRASI SYSTEM', 'Jl. Mangga Dua Raya, Komplek Perkantoran Harco Mangga Dua Blok H No.37', 158, '0216125559__', '', 'Jimmy', '', 'Dell ', '', 1, NULL, NULL),
	(5, 'Coba', 'Apa Tahunya', 231, '2323232323__', '23232323____', 'Bejo Suhendar', '(+62)812-3278-760_', 'percobaan', '20', 1, NULL, NULL),
	(6, 'Test', 'Tidak Tahu Alamatnya', 264, '', '', '', '', '', '', 1, '2017-01-07 07:35:19', '2017-01-07 07:35:19');
/*!40000 ALTER TABLE `tmst_supplier` ENABLE KEYS */;

-- membuang struktur untuk table pos.tmst_voucher
CREATE TABLE IF NOT EXISTS `tmst_voucher` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nominal` bigint(20) DEFAULT NULL,
  `awal_periode` datetime DEFAULT NULL,
  `akhir_periode` datetime DEFAULT NULL,
  `tanggal_dipakai` datetime DEFAULT NULL,
  `is_sekali_pakai` int(11) DEFAULT NULL,
  `is_aktif` int(11) DEFAULT NULL,
  `pelanggan_id_target` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pelanggan_id_pakai` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tmst_voucher: ~0 rows (lebih kurang)
DELETE FROM `tmst_voucher`;
/*!40000 ALTER TABLE `tmst_voucher` DISABLE KEYS */;
/*!40000 ALTER TABLE `tmst_voucher` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_bayar_pembelian_detail
CREATE TABLE IF NOT EXISTS `tran_bayar_pembelian_detail` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `bayar_pembelian_header_id` bigint(20) DEFAULT NULL,
  `metode_pembayaran_id` int(11) DEFAULT NULL,
  `nomor_pembayaran` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nominal` bigint(20) DEFAULT NULL,
  `bank` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_bayar_pembelian_detail: ~0 rows (lebih kurang)
DELETE FROM `tran_bayar_pembelian_detail`;
/*!40000 ALTER TABLE `tran_bayar_pembelian_detail` DISABLE KEYS */;
INSERT INTO `tran_bayar_pembelian_detail` (`id`, `bayar_pembelian_header_id`, `metode_pembayaran_id`, `nomor_pembayaran`, `nominal`, `bank`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, '', 221650000, '', '2017-02-09 14:35:00', '2017-02-09 14:35:00');
/*!40000 ALTER TABLE `tran_bayar_pembelian_detail` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_bayar_pembelian_header
CREATE TABLE IF NOT EXISTS `tran_bayar_pembelian_header` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nota_id` bigint(20) DEFAULT NULL,
  `nota_supplier` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tanggal_nota_supplier` datetime DEFAULT NULL,
  `jenis` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `total_tagihan` bigint(20) DEFAULT NULL,
  `total_pembayaran` bigint(20) DEFAULT NULL,
  `keterangan` text COLLATE utf8_unicode_ci,
  `no_jurnal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_bayar_pembelian_header: ~0 rows (lebih kurang)
DELETE FROM `tran_bayar_pembelian_header`;
/*!40000 ALTER TABLE `tran_bayar_pembelian_header` DISABLE KEYS */;
INSERT INTO `tran_bayar_pembelian_header` (`id`, `nota_id`, `nota_supplier`, `tanggal_nota_supplier`, `jenis`, `supplier_id`, `tanggal`, `total_tagihan`, `total_pembayaran`, `keterangan`, `no_jurnal`, `created_at`, `updated_at`) VALUES
	(1, 1, 'DGHFGHFGHVHJV', '0000-00-00 00:00:00', 'nota_beli', 3, '2017-02-09 00:00:00', 221650000, 221650000, 'Lunas yo...', NULL, '2017-02-09 14:35:00', '2017-02-09 14:35:00');
/*!40000 ALTER TABLE `tran_bayar_pembelian_header` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_bayar_penjualan_detail
CREATE TABLE IF NOT EXISTS `tran_bayar_penjualan_detail` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `bayar_penjualan_header_id` bigint(20) DEFAULT NULL,
  `metode_pembayaran_id` int(11) DEFAULT NULL,
  `nomor_pembayaran` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nominal` bigint(20) DEFAULT NULL,
  `bank` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_bayar_penjualan_detail: ~0 rows (lebih kurang)
DELETE FROM `tran_bayar_penjualan_detail`;
/*!40000 ALTER TABLE `tran_bayar_penjualan_detail` DISABLE KEYS */;
INSERT INTO `tran_bayar_penjualan_detail` (`id`, `bayar_penjualan_header_id`, `metode_pembayaran_id`, `nomor_pembayaran`, `nominal`, `bank`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, '', 10000000, '', '2017-02-09 15:09:27', '2017-02-09 15:09:27');
/*!40000 ALTER TABLE `tran_bayar_penjualan_detail` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_bayar_penjualan_header
CREATE TABLE IF NOT EXISTS `tran_bayar_penjualan_header` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nota_id` bigint(20) DEFAULT NULL,
  `jenis` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pelanggan_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `total_tagihan` bigint(20) DEFAULT NULL,
  `total_pembayaran` bigint(20) DEFAULT NULL,
  `keterangan` text COLLATE utf8_unicode_ci,
  `no_jurnal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_bayar_penjualan_header: ~0 rows (lebih kurang)
DELETE FROM `tran_bayar_penjualan_header`;
/*!40000 ALTER TABLE `tran_bayar_penjualan_header` DISABLE KEYS */;
INSERT INTO `tran_bayar_penjualan_header` (`id`, `nota_id`, `jenis`, `pelanggan_id`, `tanggal`, `total_tagihan`, `total_pembayaran`, `keterangan`, `no_jurnal`, `created_at`, `updated_at`) VALUES
	(1, 1, 'nota_jual', 2, '2017-02-09 00:00:00', 10000000, 10000000, 'Lunas Om', NULL, '2017-02-09 15:09:27', '2017-02-09 15:09:27');
/*!40000 ALTER TABLE `tran_bayar_penjualan_header` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_hadiah_gudang
CREATE TABLE IF NOT EXISTS `tran_hadiah_gudang` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `hadiah_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `stok` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_hadiah_gudang: ~0 rows (lebih kurang)
DELETE FROM `tran_hadiah_gudang`;
/*!40000 ALTER TABLE `tran_hadiah_gudang` DISABLE KEYS */;
/*!40000 ALTER TABLE `tran_hadiah_gudang` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_hadiah_supplier
CREATE TABLE IF NOT EXISTS `tran_hadiah_supplier` (
  `hadiah_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `harga_terakhir` bigint(20) DEFAULT NULL,
  `tanggal_terakhir` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_hadiah_supplier: ~0 rows (lebih kurang)
DELETE FROM `tran_hadiah_supplier`;
/*!40000 ALTER TABLE `tran_hadiah_supplier` DISABLE KEYS */;
/*!40000 ALTER TABLE `tran_hadiah_supplier` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_jurnal_umum_detail
CREATE TABLE IF NOT EXISTS `tran_jurnal_umum_detail` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `jurnal_umum_header_id` bigint(20) NOT NULL,
  `no_jurnal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `akun_kode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `akun_nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `referensi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `debit` bigint(20) DEFAULT NULL,
  `kredit` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_jurnal_umum_detail: ~0 rows (lebih kurang)
DELETE FROM `tran_jurnal_umum_detail`;
/*!40000 ALTER TABLE `tran_jurnal_umum_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `tran_jurnal_umum_detail` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_jurnal_umum_header
CREATE TABLE IF NOT EXISTS `tran_jurnal_umum_header` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` datetime DEFAULT NULL,
  `is_editable` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_jurnal_umum_header: ~0 rows (lebih kurang)
DELETE FROM `tran_jurnal_umum_header`;
/*!40000 ALTER TABLE `tran_jurnal_umum_header` DISABLE KEYS */;
/*!40000 ALTER TABLE `tran_jurnal_umum_header` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_kartu_stok_detail
CREATE TABLE IF NOT EXISTS `tran_kartu_stok_detail` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `kartu_stok_header_id` int(11) NOT NULL,
  `sumber_data_id` int(11) DEFAULT NULL,
  `supplier_or_pelanggan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_nota` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `harga` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_kartu_stok_detail: ~0 rows (lebih kurang)
DELETE FROM `tran_kartu_stok_detail`;
/*!40000 ALTER TABLE `tran_kartu_stok_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `tran_kartu_stok_detail` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_kartu_stok_header
CREATE TABLE IF NOT EXISTS `tran_kartu_stok_header` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `gudang_id` int(11) DEFAULT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `tanggal_awal` datetime DEFAULT NULL,
  `tanggal_akhir` datetime DEFAULT NULL,
  `total_stok` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_kartu_stok_header: ~0 rows (lebih kurang)
DELETE FROM `tran_kartu_stok_header`;
/*!40000 ALTER TABLE `tran_kartu_stok_header` DISABLE KEYS */;
/*!40000 ALTER TABLE `tran_kartu_stok_header` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_laporan_stok_detail
CREATE TABLE IF NOT EXISTS `tran_laporan_stok_detail` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `laporan_stok_header_id` int(11) DEFAULT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_laporan_stok_detail: ~0 rows (lebih kurang)
DELETE FROM `tran_laporan_stok_detail`;
/*!40000 ALTER TABLE `tran_laporan_stok_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `tran_laporan_stok_detail` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_laporan_stok_header
CREATE TABLE IF NOT EXISTS `tran_laporan_stok_header` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `gudang_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `total_stok` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_laporan_stok_header: ~0 rows (lebih kurang)
DELETE FROM `tran_laporan_stok_header`;
/*!40000 ALTER TABLE `tran_laporan_stok_header` DISABLE KEYS */;
INSERT INTO `tran_laporan_stok_header` (`id`, `gudang_id`, `tanggal`, `total_stok`, `created_at`, `updated_at`) VALUES
	(71, 1, '2017-01-13 00:00:00', 0, '2017-01-13 10:35:16', '2017-01-13 10:35:16');
/*!40000 ALTER TABLE `tran_laporan_stok_header` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_mini_banner
CREATE TABLE IF NOT EXISTS `tran_mini_banner` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_gambar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `list_produk` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_mini_banner: ~4 rows (lebih kurang)
DELETE FROM `tran_mini_banner`;
/*!40000 ALTER TABLE `tran_mini_banner` DISABLE KEYS */;
INSERT INTO `tran_mini_banner` (`id`, `nama`, `url`, `file_gambar`, `list_produk`, `item_order`, `created_at`, `updated_at`) VALUES
	(1, 'Promo 1', 'promo-1', 'mb-ffkf-promo-1.jpg', NULL, NULL, '2016-12-06 06:26:31', '2016-12-06 06:26:31'),
	(2, 'Promo 2', 'promo-2', 'mb-c0nl-promo-2.jpg', NULL, NULL, '2016-12-06 06:27:16', '2016-12-06 06:27:16'),
	(3, 'Promo 3', 'promo-3', 'mb-rf6k-promo-3.png', NULL, NULL, '2016-12-06 06:27:33', '2016-12-06 06:27:33'),
	(4, 'Promo 4', 'promo-4-1', 'mb-1dk6-promo-4-1.png', NULL, NULL, '2016-12-06 06:27:47', '2016-12-06 06:28:48');
/*!40000 ALTER TABLE `tran_mini_banner` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_mutasi_stok_detail
CREATE TABLE IF NOT EXISTS `tran_mutasi_stok_detail` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `mutasi_stok_header_id` int(11) NOT NULL,
  `no_mutasi_stok` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `produk_id` int(11) NOT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jumlah` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_mutasi_stok_detail: ~0 rows (lebih kurang)
DELETE FROM `tran_mutasi_stok_detail`;
/*!40000 ALTER TABLE `tran_mutasi_stok_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `tran_mutasi_stok_detail` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_mutasi_stok_header
CREATE TABLE IF NOT EXISTS `tran_mutasi_stok_header` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `no_mutasi_stok` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gudang_id_asal` int(11) NOT NULL,
  `gudang_id_tujuan` int(11) NOT NULL,
  `tanggal` datetime DEFAULT NULL,
  `approval_by_user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `received_by_user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_mutasi_stok_header: ~0 rows (lebih kurang)
DELETE FROM `tran_mutasi_stok_header`;
/*!40000 ALTER TABLE `tran_mutasi_stok_header` DISABLE KEYS */;
/*!40000 ALTER TABLE `tran_mutasi_stok_header` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_nilai_spesifikasi
CREATE TABLE IF NOT EXISTS `tran_nilai_spesifikasi` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `produk_id` bigint(20) DEFAULT NULL,
  `parameter_spesifikasi_id` bigint(20) DEFAULT NULL,
  `nilai` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `satuan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_nilai_spesifikasi: ~0 rows (lebih kurang)
DELETE FROM `tran_nilai_spesifikasi`;
/*!40000 ALTER TABLE `tran_nilai_spesifikasi` DISABLE KEYS */;
/*!40000 ALTER TABLE `tran_nilai_spesifikasi` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_nota_beli
CREATE TABLE IF NOT EXISTS `tran_nota_beli` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `sj_masuk_header_id` bigint(20) NOT NULL,
  `no_nota` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `jatuh_tempo` datetime DEFAULT NULL,
  `no_jurnal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_lunas` int(11) DEFAULT NULL,
  `ppn` int(11) DEFAULT NULL,
  `total_harga` bigint(20) DEFAULT NULL,
  `total_diskon` bigint(20) DEFAULT NULL,
  `total_tagihan` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_nota_beli: ~0 rows (lebih kurang)
DELETE FROM `tran_nota_beli`;
/*!40000 ALTER TABLE `tran_nota_beli` DISABLE KEYS */;
INSERT INTO `tran_nota_beli` (`id`, `sj_masuk_header_id`, `no_nota`, `supplier_id`, `tanggal`, `jatuh_tempo`, `no_jurnal`, `is_lunas`, `ppn`, `total_harga`, `total_diskon`, `total_tagihan`, `created_at`, `updated_at`) VALUES
	(1, 1, 'NB-201702-00001', 3, '2017-02-09 00:00:00', '2017-02-28 00:00:00', NULL, 1, 10, 201500000, 0, 221650000, '2017-02-09 14:34:16', '2017-02-09 14:35:00');
/*!40000 ALTER TABLE `tran_nota_beli` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_nota_jual
CREATE TABLE IF NOT EXISTS `tran_nota_jual` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `sj_keluar_header_id` bigint(20) NOT NULL,
  `no_nota` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pelanggan_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `jatuh_tempo` datetime DEFAULT NULL,
  `no_jurnal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_lunas` int(11) DEFAULT NULL,
  `ppn` int(11) DEFAULT NULL,
  `total_harga` bigint(20) DEFAULT NULL,
  `total_voucher` bigint(20) DEFAULT NULL,
  `total_tagihan` bigint(20) DEFAULT NULL,
  `kirim_via` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_resi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `foto_resi` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_nota_jual: ~0 rows (lebih kurang)
DELETE FROM `tran_nota_jual`;
/*!40000 ALTER TABLE `tran_nota_jual` DISABLE KEYS */;
INSERT INTO `tran_nota_jual` (`id`, `sj_keluar_header_id`, `no_nota`, `pelanggan_id`, `tanggal`, `jatuh_tempo`, `no_jurnal`, `is_lunas`, `ppn`, `total_harga`, `total_voucher`, `total_tagihan`, `kirim_via`, `no_resi`, `foto_resi`, `created_at`, `updated_at`) VALUES
	(1, 2, 'NJ-201702-00001', 2, '2017-02-09 00:00:00', '2017-02-22 00:00:00', NULL, 1, 0, 10000000, 0, 10000000, 'JNE', 'HESOYAM', 'resi_jual/NJ-201702-00001.png', '2017-02-09 14:45:11', '2017-02-09 15:09:27');
/*!40000 ALTER TABLE `tran_nota_jual` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_paket_gudang
CREATE TABLE IF NOT EXISTS `tran_paket_gudang` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `paket_id` int(11) DEFAULT NULL,
  `gudang_id` int(11) DEFAULT NULL,
  `stok` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_paket_gudang: ~0 rows (lebih kurang)
DELETE FROM `tran_paket_gudang`;
/*!40000 ALTER TABLE `tran_paket_gudang` DISABLE KEYS */;
/*!40000 ALTER TABLE `tran_paket_gudang` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_paket_pilihan
CREATE TABLE IF NOT EXISTS `tran_paket_pilihan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_gambar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `list_paket` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_paket_pilihan: ~0 rows (lebih kurang)
DELETE FROM `tran_paket_pilihan`;
/*!40000 ALTER TABLE `tran_paket_pilihan` DISABLE KEYS */;
/*!40000 ALTER TABLE `tran_paket_pilihan` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_paket_produk
CREATE TABLE IF NOT EXISTS `tran_paket_produk` (
  `paket_id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `qty_produk` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_paket_produk: ~0 rows (lebih kurang)
DELETE FROM `tran_paket_produk`;
/*!40000 ALTER TABLE `tran_paket_produk` DISABLE KEYS */;
/*!40000 ALTER TABLE `tran_paket_produk` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_parameter_spesifikasi
CREATE TABLE IF NOT EXISTS `tran_parameter_spesifikasi` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `kategori_produk_id` int(11) DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_parameter_spesifikasi: ~4 rows (lebih kurang)
DELETE FROM `tran_parameter_spesifikasi`;
/*!40000 ALTER TABLE `tran_parameter_spesifikasi` DISABLE KEYS */;
INSERT INTO `tran_parameter_spesifikasi` (`id`, `kategori_produk_id`, `nama`, `created_at`, `updated_at`) VALUES
	(1, 2, 'Memory', '2016-12-08 07:19:52', '2016-12-08 07:19:52'),
	(2, 2, 'Layar', '2016-12-08 07:19:52', '2016-12-08 07:19:52'),
	(3, 2, 'Optical Drive', '2016-12-08 07:19:52', '2016-12-08 07:19:52'),
	(4, 2, 'Operating System', '2016-12-08 07:19:52', '2016-12-08 07:19:52');
/*!40000 ALTER TABLE `tran_parameter_spesifikasi` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_pelanggan_cart
CREATE TABLE IF NOT EXISTS `tran_pelanggan_cart` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pelanggan_id` bigint(20) DEFAULT NULL,
  `produk_id` bigint(20) DEFAULT NULL,
  `jenis_barang_id` int(11) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `file_gambar` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `harga_retail` bigint(20) NOT NULL,
  `harga_akhir` bigint(20) NOT NULL,
  `jenis_promo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_pelanggan_cart: ~0 rows (lebih kurang)
DELETE FROM `tran_pelanggan_cart`;
/*!40000 ALTER TABLE `tran_pelanggan_cart` DISABLE KEYS */;
/*!40000 ALTER TABLE `tran_pelanggan_cart` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_pengaturan_jurnal
CREATE TABLE IF NOT EXISTS `tran_pengaturan_jurnal` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `transaksi_id` int(11) DEFAULT NULL,
  `akun_id` int(11) DEFAULT NULL,
  `debit_or_kredit` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_pengaturan_jurnal: ~15 rows (lebih kurang)
DELETE FROM `tran_pengaturan_jurnal`;
/*!40000 ALTER TABLE `tran_pengaturan_jurnal` DISABLE KEYS */;
INSERT INTO `tran_pengaturan_jurnal` (`id`, `transaksi_id`, `akun_id`, `debit_or_kredit`, `created_at`, `updated_at`) VALUES
	(8, 1, 4, 'D', '2016-11-18 04:54:02', '2016-11-21 06:13:11'),
	(9, 1, 11, 'D', '2016-11-18 04:54:10', '2016-11-21 06:13:22'),
	(10, 6, 4, 'K', '2016-11-18 04:54:21', '2016-11-21 06:15:31'),
	(11, 8, 4, 'D', '2016-11-18 04:54:31', '2016-11-21 06:16:01'),
	(12, 2, 11, 'K', '2016-11-21 06:13:31', '2016-11-21 06:13:31'),
	(13, 3, 4, 'K', '2016-11-21 06:13:41', '2016-11-21 06:13:41'),
	(14, 3, 6, 'D', '2016-11-21 06:14:07', '2016-11-21 06:14:07'),
	(15, 5, 6, 'K', '2016-11-21 06:14:17', '2016-11-21 06:14:17'),
	(16, 6, 11, 'D', '2016-11-21 06:15:24', '2016-11-21 06:15:24'),
	(17, 8, 6, 'K', '2016-11-21 06:16:16', '2016-11-21 06:16:16'),
	(18, 10, 6, 'D', '2016-11-21 06:16:26', '2016-11-21 06:16:26'),
	(19, 11, 2, 'K', '2016-11-21 06:16:42', '2016-11-21 06:16:51'),
	(20, 11, 11, 'K', '2016-11-21 06:17:24', '2016-11-21 06:17:24'),
	(21, 12, 2, 'D', '2016-11-21 06:17:48', '2016-11-21 06:17:48'),
	(22, 12, 6, 'K', '2016-11-21 06:18:32', '2016-11-21 06:18:32');
/*!40000 ALTER TABLE `tran_pengaturan_jurnal` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_penyesuaian_stok
CREATE TABLE IF NOT EXISTS `tran_penyesuaian_stok` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `gudang_id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `stok_awal` int(11) DEFAULT NULL,
  `stok_ubah` int(11) DEFAULT NULL,
  `stok_baru` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `approval_by_user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `received_by_user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_penyesuaian_stok: ~0 rows (lebih kurang)
DELETE FROM `tran_penyesuaian_stok`;
/*!40000 ALTER TABLE `tran_penyesuaian_stok` DISABLE KEYS */;
/*!40000 ALTER TABLE `tran_penyesuaian_stok` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_penyesuaian_stok_detail
CREATE TABLE IF NOT EXISTS `tran_penyesuaian_stok_detail` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `penyesuaian_stok_header_id` int(11) DEFAULT NULL,
  `produk_id` bigint(20) DEFAULT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stok_awal` int(11) DEFAULT NULL,
  `stok_ubah` int(11) DEFAULT NULL,
  `stok_baru` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_penyesuaian_stok_detail: ~0 rows (lebih kurang)
DELETE FROM `tran_penyesuaian_stok_detail`;
/*!40000 ALTER TABLE `tran_penyesuaian_stok_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `tran_penyesuaian_stok_detail` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_penyesuaian_stok_header
CREATE TABLE IF NOT EXISTS `tran_penyesuaian_stok_header` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `no_penyesuaian_stok` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gudang_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `keterangan` text COLLATE utf8_unicode_ci,
  `approval_by_user_id` int(11) DEFAULT NULL,
  `received_by_user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_penyesuaian_stok_header: ~0 rows (lebih kurang)
DELETE FROM `tran_penyesuaian_stok_header`;
/*!40000 ALTER TABLE `tran_penyesuaian_stok_header` DISABLE KEYS */;
/*!40000 ALTER TABLE `tran_penyesuaian_stok_header` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_po_alamat_pengiriman
CREATE TABLE IF NOT EXISTS `tran_po_alamat_pengiriman` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `po_header_id` bigint(20) NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alamat` text COLLATE utf8_unicode_ci,
  `kota_id` int(11) DEFAULT NULL,
  `kode_pos` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metode_pengiriman_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_po_alamat_pengiriman: ~0 rows (lebih kurang)
DELETE FROM `tran_po_alamat_pengiriman`;
/*!40000 ALTER TABLE `tran_po_alamat_pengiriman` DISABLE KEYS */;
INSERT INTO `tran_po_alamat_pengiriman` (`id`, `po_header_id`, `nama`, `email`, `alamat`, `kota_id`, `kode_pos`, `hp`, `metode_pengiriman_id`, `created_at`, `updated_at`) VALUES
	(1, 1, NULL, NULL, 'Galerindo Teknologi\r\nHi Tech Mall Lt. Dasar Blok.D No.17-19\r\nJl. Kusuma Bangsa No.116-118 Surabaya\r\nT. 031 5477085 | 031 5348998\r\nUp. Bpk. Gondo/ Ibu Vera', NULL, NULL, NULL, NULL, '2017-02-09 21:30:17', '2017-02-09 21:30:17');
/*!40000 ALTER TABLE `tran_po_alamat_pengiriman` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_po_detail
CREATE TABLE IF NOT EXISTS `tran_po_detail` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `po_header_id` bigint(20) NOT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `jenis_barang_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `jumlah_terkirim` int(11) DEFAULT NULL,
  `deskripsi` text COLLATE utf8_unicode_ci,
  `harga` bigint(20) DEFAULT NULL,
  `harga_retail` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_po_detail: ~0 rows (lebih kurang)
DELETE FROM `tran_po_detail`;
/*!40000 ALTER TABLE `tran_po_detail` DISABLE KEYS */;
INSERT INTO `tran_po_detail` (`id`, `po_header_id`, `produk_id`, `jenis_barang_id`, `jumlah`, `jumlah_terkirim`, `deskripsi`, `harga`, `harga_retail`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, '1', 31, 31, 'Intel Core i-7', 6500000, NULL, '2017-02-09 21:30:17', '2017-02-09 21:32:18');
/*!40000 ALTER TABLE `tran_po_detail` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_po_diskon
CREATE TABLE IF NOT EXISTS `tran_po_diskon` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `po_header_id` bigint(20) NOT NULL,
  `nominal` bigint(20) DEFAULT NULL,
  `is_dipakai` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_po_diskon: ~0 rows (lebih kurang)
DELETE FROM `tran_po_diskon`;
/*!40000 ALTER TABLE `tran_po_diskon` DISABLE KEYS */;
/*!40000 ALTER TABLE `tran_po_diskon` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_po_header
CREATE TABLE IF NOT EXISTS `tran_po_header` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `no_purchase_order` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `due_date` datetime DEFAULT NULL,
  `catatan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_tagihan` bigint(20) DEFAULT NULL,
  `flag_sj_masuk` int(11) DEFAULT NULL,
  `syarat_ketentuan` text COLLATE utf8_unicode_ci,
  `ppn` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_po_header: ~0 rows (lebih kurang)
DELETE FROM `tran_po_header`;
/*!40000 ALTER TABLE `tran_po_header` DISABLE KEYS */;
INSERT INTO `tran_po_header` (`id`, `no_purchase_order`, `supplier_id`, `tanggal`, `due_date`, `catatan`, `total_tagihan`, `flag_sj_masuk`, `syarat_ketentuan`, `ppn`, `created_at`, `updated_at`) VALUES
	(1, 'PO-201702-00001', 3, '2017-02-09 21:30:16', NULL, 'Catatan Purchase', 707850000, 0, '', 10, '2017-02-09 21:30:16', '2017-02-09 21:30:17');
/*!40000 ALTER TABLE `tran_po_header` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_produk_akan_datang
CREATE TABLE IF NOT EXISTS `tran_produk_akan_datang` (
  `produk_id` int(11) NOT NULL,
  `tanggal_input` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_produk_akan_datang: ~0 rows (lebih kurang)
DELETE FROM `tran_produk_akan_datang`;
/*!40000 ALTER TABLE `tran_produk_akan_datang` DISABLE KEYS */;
/*!40000 ALTER TABLE `tran_produk_akan_datang` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_produk_baru
CREATE TABLE IF NOT EXISTS `tran_produk_baru` (
  `produk_id` int(11) NOT NULL,
  `tanggal_input` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_produk_baru: ~0 rows (lebih kurang)
DELETE FROM `tran_produk_baru`;
/*!40000 ALTER TABLE `tran_produk_baru` DISABLE KEYS */;
/*!40000 ALTER TABLE `tran_produk_baru` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_produk_galeri
CREATE TABLE IF NOT EXISTS `tran_produk_galeri` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `produk_id` int(11) DEFAULT NULL,
  `file_gambar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_produk_galeri: ~0 rows (lebih kurang)
DELETE FROM `tran_produk_galeri`;
/*!40000 ALTER TABLE `tran_produk_galeri` DISABLE KEYS */;
INSERT INTO `tran_produk_galeri` (`id`, `produk_id`, `file_gambar`, `created_at`, `updated_at`) VALUES
	(1, 1, 'PRDKBAE7YU1.png', NULL, NULL);
/*!40000 ALTER TABLE `tran_produk_galeri` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_produk_gudang
CREATE TABLE IF NOT EXISTS `tran_produk_gudang` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `produk_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `stok` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_produk_gudang: ~0 rows (lebih kurang)
DELETE FROM `tran_produk_gudang`;
/*!40000 ALTER TABLE `tran_produk_gudang` DISABLE KEYS */;
INSERT INTO `tran_produk_gudang` (`id`, `produk_id`, `gudang_id`, `stok`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, 29, '2017-02-09 21:32:13', '2017-02-09 21:44:16');
/*!40000 ALTER TABLE `tran_produk_gudang` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_produk_harga
CREATE TABLE IF NOT EXISTS `tran_produk_harga` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `produk_id` int(11) DEFAULT NULL,
  `kategori_pelanggan_id` int(11) DEFAULT NULL,
  `diskon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `awal_periode` datetime DEFAULT NULL,
  `akhir_periode` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_produk_harga: ~0 rows (lebih kurang)
DELETE FROM `tran_produk_harga`;
/*!40000 ALTER TABLE `tran_produk_harga` DISABLE KEYS */;
/*!40000 ALTER TABLE `tran_produk_harga` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_produk_pilihan
CREATE TABLE IF NOT EXISTS `tran_produk_pilihan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_gambar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `list_produk` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_produk_pilihan: ~0 rows (lebih kurang)
DELETE FROM `tran_produk_pilihan`;
/*!40000 ALTER TABLE `tran_produk_pilihan` DISABLE KEYS */;
/*!40000 ALTER TABLE `tran_produk_pilihan` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_produk_serial_number
CREATE TABLE IF NOT EXISTS `tran_produk_serial_number` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `gudang_id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stok` int(11) DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_produk_serial_number: ~31 rows (lebih kurang)
DELETE FROM `tran_produk_serial_number`;
/*!40000 ALTER TABLE `tran_produk_serial_number` DISABLE KEYS */;
INSERT INTO `tran_produk_serial_number` (`id`, `gudang_id`, `produk_id`, `serial_number`, `stok`, `keterangan`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, 'QQ', 1, NULL, '2017-02-09 21:32:13', '2017-02-09 21:32:13'),
	(2, 1, 1, 'KHJH', 0, NULL, '2017-02-09 21:32:13', '2017-02-09 21:44:16'),
	(3, 1, 1, 'VJH', 1, NULL, '2017-02-09 21:32:13', '2017-02-09 21:32:13'),
	(4, 1, 1, 'HSFHKSVBD', 0, NULL, '2017-02-09 21:32:13', '2017-02-09 21:44:16'),
	(5, 1, 1, 'SGSDF', 1, NULL, '2017-02-09 21:32:14', '2017-02-09 21:32:14'),
	(6, 1, 1, 'WDCSFV', 1, NULL, '2017-02-09 21:32:14', '2017-02-09 21:32:14'),
	(7, 1, 1, 'FHTRH', 1, NULL, '2017-02-09 21:32:14', '2017-02-09 21:32:14'),
	(8, 1, 1, 'U', 1, NULL, '2017-02-09 21:32:14', '2017-02-09 21:32:14'),
	(9, 1, 1, 'YJJTYN', 1, NULL, '2017-02-09 21:32:14', '2017-02-09 21:32:14'),
	(10, 1, 1, 'SDFS', 1, NULL, '2017-02-09 21:32:14', '2017-02-09 21:32:14'),
	(11, 1, 1, 'CSDC', 1, NULL, '2017-02-09 21:32:15', '2017-02-09 21:32:15'),
	(12, 1, 1, 'ASDFDFK', 1, NULL, '2017-02-09 21:32:15', '2017-02-09 21:32:15'),
	(13, 1, 1, 'DSDFSD', 1, NULL, '2017-02-09 21:32:15', '2017-02-09 21:32:15'),
	(14, 1, 1, 'ERRE', 1, NULL, '2017-02-09 21:32:15', '2017-02-09 21:32:15'),
	(15, 1, 1, 'TUI]UYJ', 1, NULL, '2017-02-09 21:32:15', '2017-02-09 21:32:15'),
	(16, 1, 1, 'IUOU', 1, NULL, '2017-02-09 21:32:16', '2017-02-09 21:32:16'),
	(17, 1, 1, 'FGHBFG', 1, NULL, '2017-02-09 21:32:16', '2017-02-09 21:32:16'),
	(18, 1, 1, 'ZFCSD', 1, NULL, '2017-02-09 21:32:16', '2017-02-09 21:32:16'),
	(19, 1, 1, 'ASFSADC', 1, NULL, '2017-02-09 21:32:16', '2017-02-09 21:32:16'),
	(20, 1, 1, 'DFFGFD', 1, NULL, '2017-02-09 21:32:17', '2017-02-09 21:32:17'),
	(21, 1, 1, 'ADAQ', 1, NULL, '2017-02-09 21:32:17', '2017-02-09 21:32:17'),
	(22, 1, 1, 'SDVSVSDV', 1, NULL, '2017-02-09 21:32:17', '2017-02-09 21:32:17'),
	(23, 1, 1, 'DHGFB', 1, NULL, '2017-02-09 21:32:17', '2017-02-09 21:32:17'),
	(24, 1, 1, 'GHFGTH', 1, NULL, '2017-02-09 21:32:17', '2017-02-09 21:32:17'),
	(25, 1, 1, 'NHFHGN', 1, NULL, '2017-02-09 21:32:17', '2017-02-09 21:32:17'),
	(26, 1, 1, 'TYJYTHNFG', 1, NULL, '2017-02-09 21:32:18', '2017-02-09 21:32:18'),
	(27, 1, 1, 'SFGSDG', 1, NULL, '2017-02-09 21:32:18', '2017-02-09 21:32:18'),
	(28, 1, 1, 'ASFASF', 1, NULL, '2017-02-09 21:32:18', '2017-02-09 21:32:18'),
	(29, 1, 1, 'WERFEWR', 1, NULL, '2017-02-09 21:32:18', '2017-02-09 21:32:18'),
	(30, 1, 1, 'BTRHNRT', 1, NULL, '2017-02-09 21:32:18', '2017-02-09 21:32:18'),
	(31, 1, 1, 'YTMNYTJ', 1, NULL, '2017-02-09 21:32:18', '2017-02-09 21:32:18');
/*!40000 ALTER TABLE `tran_produk_serial_number` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_produk_supplier
CREATE TABLE IF NOT EXISTS `tran_produk_supplier` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `produk_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `harga_terakhir` bigint(20) DEFAULT NULL,
  `tanggal_terakhir` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_produk_supplier: ~0 rows (lebih kurang)
DELETE FROM `tran_produk_supplier`;
/*!40000 ALTER TABLE `tran_produk_supplier` DISABLE KEYS */;
INSERT INTO `tran_produk_supplier` (`id`, `produk_id`, `supplier_id`, `harga_terakhir`, `tanggal_terakhir`, `created_at`, `updated_at`) VALUES
	(1, 1, 3, 6500000, '2017-02-09 21:30:16', NULL, NULL);
/*!40000 ALTER TABLE `tran_produk_supplier` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_produk_tampil_di_beranda
CREATE TABLE IF NOT EXISTS `tran_produk_tampil_di_beranda` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `produk_id` int(11) DEFAULT NULL,
  `tanggal_input` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_produk_tampil_di_beranda: ~0 rows (lebih kurang)
DELETE FROM `tran_produk_tampil_di_beranda`;
/*!40000 ALTER TABLE `tran_produk_tampil_di_beranda` DISABLE KEYS */;
/*!40000 ALTER TABLE `tran_produk_tampil_di_beranda` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_promo_cashback
CREATE TABLE IF NOT EXISTS `tran_promo_cashback` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `produk_id` int(11) DEFAULT NULL,
  `kode_promo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qty_beli` int(11) DEFAULT NULL,
  `cashback` int(11) DEFAULT NULL,
  `awal_periode` datetime DEFAULT NULL,
  `akhir_periode` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_promo_cashback: ~0 rows (lebih kurang)
DELETE FROM `tran_promo_cashback`;
/*!40000 ALTER TABLE `tran_promo_cashback` DISABLE KEYS */;
/*!40000 ALTER TABLE `tran_promo_cashback` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_promo_diskon
CREATE TABLE IF NOT EXISTS `tran_promo_diskon` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `produk_id` int(11) DEFAULT NULL,
  `kode_promo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qty_beli` int(11) DEFAULT NULL,
  `diskon` int(11) DEFAULT NULL,
  `awal_periode` datetime DEFAULT NULL,
  `akhir_periode` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_promo_diskon: ~0 rows (lebih kurang)
DELETE FROM `tran_promo_diskon`;
/*!40000 ALTER TABLE `tran_promo_diskon` DISABLE KEYS */;
/*!40000 ALTER TABLE `tran_promo_diskon` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_promo_hadiah
CREATE TABLE IF NOT EXISTS `tran_promo_hadiah` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `produk_id` int(11) DEFAULT NULL,
  `hadiah_id` int(11) DEFAULT NULL,
  `kode_promo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qty_beli` int(11) DEFAULT NULL,
  `qty_hadiah` int(11) DEFAULT NULL,
  `awal_periode` datetime DEFAULT NULL,
  `akhir_periode` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_promo_hadiah: ~0 rows (lebih kurang)
DELETE FROM `tran_promo_hadiah`;
/*!40000 ALTER TABLE `tran_promo_hadiah` DISABLE KEYS */;
/*!40000 ALTER TABLE `tran_promo_hadiah` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_rb_detail
CREATE TABLE IF NOT EXISTS `tran_rb_detail` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `rb_header_id` int(11) NOT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `jenis_barang_id` int(11) DEFAULT NULL,
  `gudang_id` int(11) DEFAULT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `deskripsi` text COLLATE utf8_unicode_ci,
  `harga` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_rb_detail: ~0 rows (lebih kurang)
DELETE FROM `tran_rb_detail`;
/*!40000 ALTER TABLE `tran_rb_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `tran_rb_detail` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_rb_header
CREATE TABLE IF NOT EXISTS `tran_rb_header` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nota_beli_id` bigint(20) NOT NULL,
  `no_nota` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `total_retur` bigint(20) DEFAULT NULL,
  `potongan_retur` bigint(20) DEFAULT NULL,
  `total_tagihan` bigint(20) DEFAULT NULL,
  `catatan` text COLLATE utf8_unicode_ci,
  `no_jurnal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_lunas` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_rb_header: ~0 rows (lebih kurang)
DELETE FROM `tran_rb_header`;
/*!40000 ALTER TABLE `tran_rb_header` DISABLE KEYS */;
/*!40000 ALTER TABLE `tran_rb_header` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_rekening_supplier
CREATE TABLE IF NOT EXISTS `tran_rekening_supplier` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama_bank` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomor_rekening` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `atas_nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mata_uang` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supplier_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_rekening_supplier: ~2 rows (lebih kurang)
DELETE FROM `tran_rekening_supplier`;
/*!40000 ALTER TABLE `tran_rekening_supplier` DISABLE KEYS */;
INSERT INTO `tran_rekening_supplier` (`id`, `nama_bank`, `nomor_rekening`, `atas_nama`, `mata_uang`, `supplier_id`, `created_at`, `updated_at`) VALUES
	(1, 'BCA', '928882003222', 'Sdsdsds', 'dollar', '5', NULL, NULL),
	(2, 'BRI', '2932323232312', 'Joko', 'rupiah', '6', NULL, NULL);
/*!40000 ALTER TABLE `tran_rekening_supplier` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_rj_detail
CREATE TABLE IF NOT EXISTS `tran_rj_detail` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `rj_header_id` int(11) NOT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `jenis_barang_id` int(11) DEFAULT NULL,
  `gudang_id` int(11) DEFAULT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `deskripsi` text COLLATE utf8_unicode_ci,
  `harga` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_rj_detail: ~0 rows (lebih kurang)
DELETE FROM `tran_rj_detail`;
/*!40000 ALTER TABLE `tran_rj_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `tran_rj_detail` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_rj_header
CREATE TABLE IF NOT EXISTS `tran_rj_header` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nota_jual_id` bigint(20) NOT NULL,
  `no_nota` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pelanggan_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `total_retur` bigint(20) DEFAULT NULL,
  `potongan_retur` bigint(20) DEFAULT NULL,
  `total_tagihan` bigint(20) DEFAULT NULL,
  `catatan` text COLLATE utf8_unicode_ci,
  `no_jurnal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_lunas` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_rj_header: ~0 rows (lebih kurang)
DELETE FROM `tran_rj_header`;
/*!40000 ALTER TABLE `tran_rj_header` DISABLE KEYS */;
/*!40000 ALTER TABLE `tran_rj_header` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_service_invoice_detail
CREATE TABLE IF NOT EXISTS `tran_service_invoice_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `service_invoice_header_id` int(11) NOT NULL,
  `tindakan` text COLLATE utf8_unicode_ci NOT NULL,
  `biaya` bigint(20) NOT NULL,
  `is_repair` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_service_invoice_detail: ~0 rows (lebih kurang)
DELETE FROM `tran_service_invoice_detail`;
/*!40000 ALTER TABLE `tran_service_invoice_detail` DISABLE KEYS */;
INSERT INTO `tran_service_invoice_detail` (`id`, `service_invoice_header_id`, `tindakan`, `biaya`, `is_repair`, `created_at`, `updated_at`) VALUES
	(11, 5, 'tindakan satu', 150000, 1, '2017-02-10 20:39:23', '2017-02-10 20:43:36');
/*!40000 ALTER TABLE `tran_service_invoice_detail` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_service_invoice_header
CREATE TABLE IF NOT EXISTS `tran_service_invoice_header` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `service_order_id` int(11) NOT NULL,
  `teknisi_id` int(11) NOT NULL,
  `pelanggan_id` int(11) NOT NULL,
  `harga_total` bigint(20) NOT NULL,
  `catatan` text COLLATE utf8_unicode_ci NOT NULL,
  `is_submit` tinyint(4) NOT NULL DEFAULT '0',
  `is_repair` tinyint(4) NOT NULL DEFAULT '0',
  `is_lunas` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_service_invoice_header: ~0 rows (lebih kurang)
DELETE FROM `tran_service_invoice_header`;
/*!40000 ALTER TABLE `tran_service_invoice_header` DISABLE KEYS */;
INSERT INTO `tran_service_invoice_header` (`id`, `service_order_id`, `teknisi_id`, `pelanggan_id`, `harga_total`, `catatan`, `is_submit`, `is_repair`, `is_lunas`, `created_at`, `updated_at`) VALUES
	(5, 1, 3, 20, 150000, 'catatan pertama', 1, 1, 0, '2017-02-10 20:39:23', '2017-02-10 20:43:42');
/*!40000 ALTER TABLE `tran_service_invoice_header` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_service_order
CREATE TABLE IF NOT EXISTS `tran_service_order` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `no_nota` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pelanggan_id` int(11) DEFAULT NULL,
  `teknisi_id` int(11) DEFAULT NULL,
  `model_produk` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status_garansi_produk` tinyint(4) DEFAULT '0',
  `keluhan_pelanggan` text COLLATE utf8_unicode_ci,
  `deskripsi_produk` text COLLATE utf8_unicode_ci,
  `kode_verifikasi` text COLLATE utf8_unicode_ci,
  `kode_verifikasi_ambil_barang` text COLLATE utf8_unicode_ci,
  `tanggal_diterima` datetime DEFAULT NULL,
  `tanggal_cek` datetime DEFAULT NULL,
  `tanggal_acc` datetime DEFAULT NULL,
  `tanggal_diperbaiki` datetime DEFAULT NULL,
  `tanggal_selesai` datetime DEFAULT NULL,
  `tanggal_diambil` datetime DEFAULT NULL,
  `garansi_service` datetime DEFAULT NULL,
  `is_check` tinyint(4) DEFAULT '0',
  `flag_acc` tinyint(4) DEFAULT '0' COMMENT '0 = Belum disetujui pelanggan',
  `flag_case` tinyint(4) DEFAULT '0',
  `is_lunas` tinyint(4) DEFAULT '0',
  `status_service` varchar(50) COLLATE utf8_unicode_ci DEFAULT 'Belum' COMMENT 'Belum || Proses || Selesai || Batal. Jika status = Belum || Batal, jangan cek ke tabel service_teknisi',
  `catatan` text COLLATE utf8_unicode_ci,
  `no_jurnal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_service_order: ~4 rows (lebih kurang)
DELETE FROM `tran_service_order`;
/*!40000 ALTER TABLE `tran_service_order` DISABLE KEYS */;
INSERT INTO `tran_service_order` (`id`, `no_nota`, `pelanggan_id`, `teknisi_id`, `model_produk`, `serial_number`, `status_garansi_produk`, `keluhan_pelanggan`, `deskripsi_produk`, `kode_verifikasi`, `kode_verifikasi_ambil_barang`, `tanggal_diterima`, `tanggal_cek`, `tanggal_acc`, `tanggal_diperbaiki`, `tanggal_selesai`, `tanggal_diambil`, `garansi_service`, `is_check`, `flag_acc`, `flag_case`, `is_lunas`, `status_service`, `catatan`, `no_jurnal`, `created_at`, `updated_at`) VALUES
	(1, 'SV-201701-00001', 20, 3, 'Dell Inspiron 5', 'HSABJKH76898', 0, 'Laptop cepat panas', 'Laptop agak susah dinyalakan, harus nunggu 5 menit dulu.', 'd7f0b0e455fc0d079dab617d0fe0db7b', '56c3d28d11e4144565877b48a3168356', '2017-01-16 00:00:00', '2017-02-10 20:41:25', '2017-02-10 20:41:25', '2017-02-10 20:17:16', '2017-02-10 20:43:42', '2017-02-11 15:08:56', NULL, 1, 1, 1, 1, 'Selesai', NULL, NULL, '2017-01-16 08:25:36', '2017-02-11 15:08:56'),
	(2, 'SV-201702-00002', 18, 3, 'XPS', 'SDFSDF123123JHJKH', 0, 'Keluhansdbfdsnfkj', 'Deskripsijbsjnsdf', '72e3894437d98716389c56b82028136a', NULL, '2017-02-07 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 'Proses', NULL, NULL, '2017-02-07 12:34:55', '2017-02-08 03:59:43'),
	(4, 'SV-201702-00003', 2, 3, 'Dell XPS 123', 'KHJH', 0, 'Keluhan pelanggan spiderman', 'Rubber feet hanya 2', '1f02d286326ed971a64b55a8df0abf49', NULL, '2017-02-09 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 'Proses', NULL, NULL, '2017-02-09 15:57:45', '2017-02-10 02:23:08'),
	(5, 'SV-201702-00005', 21, NULL, 'Dell XPS Ultimate', 'HHBBGG66', 0, 'fjsdfjk', 'skjdfgnsdjfnsfkj', '06d81c73bd6fa4ffc35e4f43ca24965f', NULL, '2017-02-10 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 'Belum', NULL, NULL, '2017-02-10 22:05:17', '2017-02-10 22:05:17');
/*!40000 ALTER TABLE `tran_service_order` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_service_perbaikan
CREATE TABLE IF NOT EXISTS `tran_service_perbaikan` (
  `id` int(11) DEFAULT NULL,
  `service_order_id` int(11) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `kerusakan_produk` text,
  `flag_service` tinyint(4) DEFAULT NULL,
  `tanggal_diterima` datetime DEFAULT NULL,
  `tanggal_selesai` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel pos.tran_service_perbaikan: ~0 rows (lebih kurang)
DELETE FROM `tran_service_perbaikan`;
/*!40000 ALTER TABLE `tran_service_perbaikan` DISABLE KEYS */;
/*!40000 ALTER TABLE `tran_service_perbaikan` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_sj_keluar_detail
CREATE TABLE IF NOT EXISTS `tran_sj_keluar_detail` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `sj_keluar_header_id` bigint(20) NOT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `jenis_barang_id` int(11) DEFAULT NULL,
  `gudang_id` int(11) DEFAULT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `harga` bigint(20) DEFAULT NULL,
  `harga_retail` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_sj_keluar_detail: ~2 rows (lebih kurang)
DELETE FROM `tran_sj_keluar_detail`;
/*!40000 ALTER TABLE `tran_sj_keluar_detail` DISABLE KEYS */;
INSERT INTO `tran_sj_keluar_detail` (`id`, `sj_keluar_header_id`, `produk_id`, `jenis_barang_id`, `gudang_id`, `serial_number`, `jumlah`, `harga`, `harga_retail`, `created_at`, `updated_at`) VALUES
	(2, 2, 1, 1, 1, 'KHJH', 1, 5000000, 5000000, '2017-02-09 21:44:16', '2017-02-09 21:44:16'),
	(3, 2, 1, 1, 1, 'HSFHKSVBD', 1, 5000000, 5000000, '2017-02-09 21:44:16', '2017-02-09 21:44:16');
/*!40000 ALTER TABLE `tran_sj_keluar_detail` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_sj_keluar_header
CREATE TABLE IF NOT EXISTS `tran_sj_keluar_header` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `so_header_id` bigint(20) NOT NULL,
  `no_surat_jalan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pelanggan_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `flag_nota_jual` int(11) DEFAULT NULL,
  `ppn` int(11) DEFAULT NULL,
  `total_harga` bigint(20) DEFAULT NULL,
  `total_voucher` bigint(20) DEFAULT NULL,
  `total_tagihan` bigint(20) DEFAULT NULL,
  `pengambilan_barang` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_sj_keluar_header: ~0 rows (lebih kurang)
DELETE FROM `tran_sj_keluar_header`;
/*!40000 ALTER TABLE `tran_sj_keluar_header` DISABLE KEYS */;
INSERT INTO `tran_sj_keluar_header` (`id`, `so_header_id`, `no_surat_jalan`, `pelanggan_id`, `tanggal`, `flag_nota_jual`, `ppn`, `total_harga`, `total_voucher`, `total_tagihan`, `pengambilan_barang`, `created_at`, `updated_at`) VALUES
	(2, 1, 'SJK-201702-00001', 2, '2017-02-09 21:44:16', 1, 0, 10000000, 0, 10000000, NULL, '2017-02-09 21:44:16', '2017-02-09 14:45:11');
/*!40000 ALTER TABLE `tran_sj_keluar_header` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_sj_masuk_detail
CREATE TABLE IF NOT EXISTS `tran_sj_masuk_detail` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `sj_masuk_header_id` bigint(20) NOT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `jenis_barang_id` int(11) DEFAULT NULL,
  `gudang_id` int(11) DEFAULT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `harga` bigint(20) DEFAULT NULL,
  `harga_retail` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_sj_masuk_detail: ~31 rows (lebih kurang)
DELETE FROM `tran_sj_masuk_detail`;
/*!40000 ALTER TABLE `tran_sj_masuk_detail` DISABLE KEYS */;
INSERT INTO `tran_sj_masuk_detail` (`id`, `sj_masuk_header_id`, `produk_id`, `jenis_barang_id`, `gudang_id`, `serial_number`, `jumlah`, `harga`, `harga_retail`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, 1, 1, 'QQ', 1, 6500000, NULL, '2017-02-09 21:32:13', '2017-02-09 21:32:13'),
	(2, 1, 1, 1, 1, 'KHJH', 1, 6500000, NULL, '2017-02-09 21:32:13', '2017-02-09 21:32:13'),
	(3, 1, 1, 1, 1, 'VJH', 1, 6500000, NULL, '2017-02-09 21:32:13', '2017-02-09 21:32:13'),
	(4, 1, 1, 1, 1, 'HSFHKSVBD', 1, 6500000, NULL, '2017-02-09 21:32:13', '2017-02-09 21:32:13'),
	(5, 1, 1, 1, 1, 'SGSDF', 1, 6500000, NULL, '2017-02-09 21:32:14', '2017-02-09 21:32:14'),
	(6, 1, 1, 1, 1, 'WDCSFV', 1, 6500000, NULL, '2017-02-09 21:32:14', '2017-02-09 21:32:14'),
	(7, 1, 1, 1, 1, 'FHTRH', 1, 6500000, NULL, '2017-02-09 21:32:14', '2017-02-09 21:32:14'),
	(8, 1, 1, 1, 1, 'U', 1, 6500000, NULL, '2017-02-09 21:32:14', '2017-02-09 21:32:14'),
	(9, 1, 1, 1, 1, 'YJJTYN', 1, 6500000, NULL, '2017-02-09 21:32:14', '2017-02-09 21:32:14'),
	(10, 1, 1, 1, 1, 'SDFS', 1, 6500000, NULL, '2017-02-09 21:32:14', '2017-02-09 21:32:14'),
	(11, 1, 1, 1, 1, 'CSDC', 1, 6500000, NULL, '2017-02-09 21:32:15', '2017-02-09 21:32:15'),
	(12, 1, 1, 1, 1, 'ASDFDFK', 1, 6500000, NULL, '2017-02-09 21:32:15', '2017-02-09 21:32:15'),
	(13, 1, 1, 1, 1, 'DSDFSD', 1, 6500000, NULL, '2017-02-09 21:32:15', '2017-02-09 21:32:15'),
	(14, 1, 1, 1, 1, 'ERRE', 1, 6500000, NULL, '2017-02-09 21:32:15', '2017-02-09 21:32:15'),
	(15, 1, 1, 1, 1, 'TUI]UYJ', 1, 6500000, NULL, '2017-02-09 21:32:15', '2017-02-09 21:32:15'),
	(16, 1, 1, 1, 1, 'IUOU', 1, 6500000, NULL, '2017-02-09 21:32:16', '2017-02-09 21:32:16'),
	(17, 1, 1, 1, 1, 'FGHBFG', 1, 6500000, NULL, '2017-02-09 21:32:16', '2017-02-09 21:32:16'),
	(18, 1, 1, 1, 1, 'ZFCSD', 1, 6500000, NULL, '2017-02-09 21:32:16', '2017-02-09 21:32:16'),
	(19, 1, 1, 1, 1, 'ASFSADC', 1, 6500000, NULL, '2017-02-09 21:32:16', '2017-02-09 21:32:16'),
	(20, 1, 1, 1, 1, 'DFFGFD', 1, 6500000, NULL, '2017-02-09 21:32:16', '2017-02-09 21:32:16'),
	(21, 1, 1, 1, 1, 'ADAQ', 1, 6500000, NULL, '2017-02-09 21:32:17', '2017-02-09 21:32:17'),
	(22, 1, 1, 1, 1, 'SDVSVSDV', 1, 6500000, NULL, '2017-02-09 21:32:17', '2017-02-09 21:32:17'),
	(23, 1, 1, 1, 1, 'DHGFB', 1, 6500000, NULL, '2017-02-09 21:32:17', '2017-02-09 21:32:17'),
	(24, 1, 1, 1, 1, 'GHFGTH', 1, 6500000, NULL, '2017-02-09 21:32:17', '2017-02-09 21:32:17'),
	(25, 1, 1, 1, 1, 'NHFHGN', 1, 6500000, NULL, '2017-02-09 21:32:17', '2017-02-09 21:32:17'),
	(26, 1, 1, 1, 1, 'TYJYTHNFG', 1, 6500000, NULL, '2017-02-09 21:32:17', '2017-02-09 21:32:17'),
	(27, 1, 1, 1, 1, 'SFGSDG', 1, 6500000, NULL, '2017-02-09 21:32:18', '2017-02-09 21:32:18'),
	(28, 1, 1, 1, 1, 'ASFASF', 1, 6500000, NULL, '2017-02-09 21:32:18', '2017-02-09 21:32:18'),
	(29, 1, 1, 1, 1, 'WERFEWR', 1, 6500000, NULL, '2017-02-09 21:32:18', '2017-02-09 21:32:18'),
	(30, 1, 1, 1, 1, 'BTRHNRT', 1, 6500000, NULL, '2017-02-09 21:32:18', '2017-02-09 21:32:18'),
	(31, 1, 1, 1, 1, 'YTMNYTJ', 1, 6500000, NULL, '2017-02-09 21:32:18', '2017-02-09 21:32:18');
/*!40000 ALTER TABLE `tran_sj_masuk_detail` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_sj_masuk_header
CREATE TABLE IF NOT EXISTS `tran_sj_masuk_header` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `po_header_id` bigint(20) NOT NULL,
  `no_surat_jalan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `flag_nota_beli` int(11) DEFAULT NULL,
  `total_harga` bigint(20) DEFAULT NULL,
  `ppn` int(11) DEFAULT NULL,
  `total_diskon` bigint(20) DEFAULT NULL,
  `total_tagihan` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_sj_masuk_header: ~0 rows (lebih kurang)
DELETE FROM `tran_sj_masuk_header`;
/*!40000 ALTER TABLE `tran_sj_masuk_header` DISABLE KEYS */;
INSERT INTO `tran_sj_masuk_header` (`id`, `po_header_id`, `no_surat_jalan`, `supplier_id`, `tanggal`, `flag_nota_beli`, `total_harga`, `ppn`, `total_diskon`, `total_tagihan`, `created_at`, `updated_at`) VALUES
	(1, 1, 'SJM-201702-00001', 3, '2017-02-09 21:32:13', 1, 201500000, 10, 0, 221650000, '2017-02-09 21:32:13', '2017-02-09 14:34:16');
/*!40000 ALTER TABLE `tran_sj_masuk_header` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_slider_promo
CREATE TABLE IF NOT EXISTS `tran_slider_promo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_gambar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `list_produk` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_slider_promo: ~0 rows (lebih kurang)
DELETE FROM `tran_slider_promo`;
/*!40000 ALTER TABLE `tran_slider_promo` DISABLE KEYS */;
/*!40000 ALTER TABLE `tran_slider_promo` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_so_alamat_pengiriman
CREATE TABLE IF NOT EXISTS `tran_so_alamat_pengiriman` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `so_header_id` bigint(20) NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alamat` text COLLATE utf8_unicode_ci,
  `kota_id` int(11) DEFAULT NULL,
  `kode_pos` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metode_pengiriman_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_so_alamat_pengiriman: ~0 rows (lebih kurang)
DELETE FROM `tran_so_alamat_pengiriman`;
/*!40000 ALTER TABLE `tran_so_alamat_pengiriman` DISABLE KEYS */;
INSERT INTO `tran_so_alamat_pengiriman` (`id`, `so_header_id`, `nama`, `email`, `alamat`, `kota_id`, `kode_pos`, `hp`, `metode_pengiriman_id`, `created_at`, `updated_at`) VALUES
	(1, 1, 'Peter Parker', 'peter@gmail.com', 'jalan raya nginden semolo', 264, '60111', '(+62)098-9089-0809', NULL, '2017-02-09 21:42:40', '2017-02-09 21:42:40');
/*!40000 ALTER TABLE `tran_so_alamat_pengiriman` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_so_detail
CREATE TABLE IF NOT EXISTS `tran_so_detail` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `so_header_id` bigint(20) NOT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `jenis_barang_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `jumlah_terkirim` int(11) DEFAULT NULL,
  `deskripsi` text COLLATE utf8_unicode_ci,
  `harga` bigint(20) DEFAULT NULL,
  `harga_retail` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_so_detail: ~0 rows (lebih kurang)
DELETE FROM `tran_so_detail`;
/*!40000 ALTER TABLE `tran_so_detail` DISABLE KEYS */;
INSERT INTO `tran_so_detail` (`id`, `so_header_id`, `produk_id`, `jenis_barang_id`, `jumlah`, `jumlah_terkirim`, `deskripsi`, `harga`, `harga_retail`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, '1', 2, 2, '<p>Laptop tangguh untuk gaming</p>', 5000000, 5000000, '2017-02-09 21:42:40', '2017-02-09 21:44:16');
/*!40000 ALTER TABLE `tran_so_detail` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_so_header
CREATE TABLE IF NOT EXISTS `tran_so_header` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `no_sales_order` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pelanggan_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `due_date` datetime DEFAULT NULL,
  `catatan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_tagihan` bigint(20) DEFAULT NULL,
  `flag_sj_keluar` int(11) DEFAULT NULL,
  `ppn` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_so_header: ~0 rows (lebih kurang)
DELETE FROM `tran_so_header`;
/*!40000 ALTER TABLE `tran_so_header` DISABLE KEYS */;
INSERT INTO `tran_so_header` (`id`, `no_sales_order`, `pelanggan_id`, `tanggal`, `due_date`, `catatan`, `total_tagihan`, `flag_sj_keluar`, `ppn`, `created_at`, `updated_at`) VALUES
	(1, 'SO-201702-00001', 2, '2017-02-09 21:42:40', NULL, 'Catatan pelanggan', 10000000, 1, 0, '2017-02-09 21:42:40', '2017-02-09 21:44:16');
/*!40000 ALTER TABLE `tran_so_header` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_so_pembayaran
CREATE TABLE IF NOT EXISTS `tran_so_pembayaran` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `so_header_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `metode_pembayaran_id` int(11) DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `nomor_pembayaran` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nominal` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_so_pembayaran: ~0 rows (lebih kurang)
DELETE FROM `tran_so_pembayaran`;
/*!40000 ALTER TABLE `tran_so_pembayaran` DISABLE KEYS */;
/*!40000 ALTER TABLE `tran_so_pembayaran` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_so_voucher
CREATE TABLE IF NOT EXISTS `tran_so_voucher` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `so_header_id` bigint(20) NOT NULL,
  `voucher_id` bigint(20) NOT NULL,
  `nominal` bigint(20) DEFAULT NULL,
  `is_dipakai` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_so_voucher: ~0 rows (lebih kurang)
DELETE FROM `tran_so_voucher`;
/*!40000 ALTER TABLE `tran_so_voucher` DISABLE KEYS */;
/*!40000 ALTER TABLE `tran_so_voucher` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_stok_opname_detail
CREATE TABLE IF NOT EXISTS `tran_stok_opname_detail` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `stok_opname_header_id` int(11) NOT NULL,
  `no_stok_opname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `produk_id` int(11) NOT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jumlah` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_stok_opname_detail: ~0 rows (lebih kurang)
DELETE FROM `tran_stok_opname_detail`;
/*!40000 ALTER TABLE `tran_stok_opname_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `tran_stok_opname_detail` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_stok_opname_header
CREATE TABLE IF NOT EXISTS `tran_stok_opname_header` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `no_stok_opname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `tanggal` datetime DEFAULT NULL,
  `total_stok` int(11) DEFAULT NULL,
  `approval_by_user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `received_by_user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_stok_opname_header: ~0 rows (lebih kurang)
DELETE FROM `tran_stok_opname_header`;
/*!40000 ALTER TABLE `tran_stok_opname_header` DISABLE KEYS */;
/*!40000 ALTER TABLE `tran_stok_opname_header` ENABLE KEYS */;

-- membuang struktur untuk table pos.tran_transaksi_jasa
CREATE TABLE IF NOT EXISTS `tran_transaksi_jasa` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `no_nota` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pelanggan_id` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `deskripsi` text COLLATE utf8_unicode_ci,
  `biaya` bigint(20) DEFAULT NULL,
  `no_jurnal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flag` int(11) DEFAULT NULL,
  `is_lunas` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tran_transaksi_jasa: ~0 rows (lebih kurang)
DELETE FROM `tran_transaksi_jasa`;
/*!40000 ALTER TABLE `tran_transaksi_jasa` DISABLE KEYS */;
/*!40000 ALTER TABLE `tran_transaksi_jasa` ENABLE KEYS */;

-- membuang struktur untuk table pos.tref_bank
CREATE TABLE IF NOT EXISTS `tref_bank` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nama_singkat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_aktif` int(11) NOT NULL,
  `file_gambar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tref_bank: ~0 rows (lebih kurang)
DELETE FROM `tref_bank`;
/*!40000 ALTER TABLE `tref_bank` DISABLE KEYS */;
INSERT INTO `tref_bank` (`id`, `kode`, `nama`, `nama_singkat`, `keterangan`, `is_aktif`, `file_gambar`, `created_at`, `updated_at`) VALUES
	(1, '014', 'Bank Central Asia', 'BCA', '', 1, 'BANK79C6927Y.png', NULL, '2016-10-17 15:27:13');
/*!40000 ALTER TABLE `tref_bank` ENABLE KEYS */;

-- membuang struktur untuk table pos.tref_jenis_barang
CREATE TABLE IF NOT EXISTS `tref_jenis_barang` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tref_jenis_barang: ~3 rows (lebih kurang)
DELETE FROM `tref_jenis_barang`;
/*!40000 ALTER TABLE `tref_jenis_barang` DISABLE KEYS */;
INSERT INTO `tref_jenis_barang` (`id`, `nama`, `keterangan`, `created_at`, `updated_at`) VALUES
	(1, 'produk', 'produk pada umumnya', NULL, NULL),
	(2, 'hadiah', 'produk yang dijadikan hadiah untuk pelanggan', NULL, NULL),
	(3, 'paket produk', 'kumpulan produk yang dijual menjadi satu paket', NULL, NULL);
/*!40000 ALTER TABLE `tref_jenis_barang` ENABLE KEYS */;

-- membuang struktur untuk table pos.tref_konten
CREATE TABLE IF NOT EXISTS `tref_konten` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keterangan` longtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tref_konten: ~4 rows (lebih kurang)
DELETE FROM `tref_konten`;
/*!40000 ALTER TABLE `tref_konten` DISABLE KEYS */;
INSERT INTO `tref_konten` (`id`, `nama`, `keterangan`, `created_at`, `updated_at`) VALUES
	(1, 'cara_belanja', '<p><img src="http://id-live-01.slatic.net/cms/faq-screenshot/ORDER-1.jpg" alt="" width="95%" /><a>Saya telah memesan dan konfirmasi pembayaran? Apa yang saya harus lakukan selanjutnya?</a></p>\r\n<p>Kami akan menginformasikan status pesanan terkini melalui email dan/atau SMS. Anda juga dapat melakukan pengecekan status pesanan pada link berikut <a href="http://www.lazada.co.id/order-tracking" target="_blank" rel="noopener noreferrer">http://www.lazada.co.id/order-tracking/</a>.</p>\r\n<p><a>Bagaimana caranya saya bisa memesan tanpa akun di website Lazada?</a></p>\r\n<p>Anda tentunya bisa memesan di Lazada tanpa menggunakan akun:</p>\r\n<ol>\r\n<ol>\r\n<li>Pilih produk yang Anda inginkan dan lanjutkan ke konfirmasi pesanan</li>\r\n<li>Pada langkah Login Email, masukkan email Anda dan pilih <strong>"Lanjut tanpa password"</strong><br /> <br /><img style="width: 80%; height: 80%;" src="http://id-live-01.slatic.net/cms/faq-screenshot/OP-1-1.jpg" /></li>\r\n</ol>\r\n</ol>\r\n<p>&nbsp;</p>\r\n<ol>\r\n<li>Masukkan informasi pengiriman Anda dan pilih metode pembayaran</li>\r\n<li>Konfirmasi pesanan Anda</li>\r\n</ol>\r\n<p>&nbsp;</p>\r\n<p>Untuk informasi detail bagaimana cara memesan, klik di sini <a href="http://www.lazada.co.id/how-to-buy/" target="_blank" rel="noopener noreferrer">www.lazada.co.id/how-to-buy</a>.</p>\r\n<p>Kami mempersilahkan Anda untuk membuat akun untuk mendapatkan informasi terbaru dan menarik dari Lazada, ditambah memiliki informasi dan riwayat order Anda tersimpan untuk pengalaman berbelanja Anda di toko online.</p>\r\n<p><a>Bisakah saya menghubungi Lazada untuk pembuatan pesanan?</a></p>\r\n<p>Kami dapat membantu Anda untuk membuat pesanan Anda melalui telpon jika Anda menginginkan metode pembayaran Bayar Di Tempat (COD).</p>\r\n<p>Namun, kami sangat merekomendasikan untuk membuat pesanan secara online disini <a href="http://www.lazada.co.id/how-to-buy/" target="_blank" rel="noopener noreferrer">www.lazada.co.id/how-to-buy</a>.</p>\r\n<p>Jika ada kondisi Anda tidak dapat membuat pesanan secara online, silahkan menghubungi Customer Service [<a href="http://www.lazada.co.id/helpcenter/contact-us/#answer-faq-contactus-ans" target="_blank" rel="noopener noreferrer"><strong>Klik</strong></a>]. Kami akan senang hati membantu Anda.</p>\r\n<p><a>Bagaimana menghapus item dalam keranjang belanja Anda?</a></p>\r\n<p>Untuk menghapus item dalam keranjang belanja Anda, Anda dapat:</p>\r\n<ul>\r\n<ul>\r\n<li>Klik pada logo Belanja di bagian atas halaman Lazada untuk menuju keranjang belanja Anda<br /> <br /><img style="width: 80%; height: 80%;" src="http://id-live-01.slatic.net/cms/faq-screenshot/OP-1-2.jpg" /></li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Klik pada <strong>&ldquo;Hapus Item&rdquo;</strong><br /> <br /><img style="width: 80%; height: 80%;" src="http://id-live-01.slatic.net/cms/faq-screenshot/cart2.png" /></li>\r\n</ul>\r\n<p>Keranjang Anda akan diperbaharui dan item akan dihapus.</p>\r\n<p><a>Apakah saya akan menerima konfirmasi pesanan setelah selesai melakukan pemesanan?</a></p>\r\n<p>Konfirmasi dengan ringkasan pesanan akan dikirimkan melalui SMS dan email sesaat setelah anda selesai melakukan pemesanan. <br />Kami juga akan mengirimkan email dan pemberitahuan SMS untuk memberikan informasi terbaru tentang status pesanan Anda.</p>\r\n<p><a>Apakah Saya bisa membeli produk dalam jumlah banyak?</a></p>\r\n<p>Bisa. Apabila Anda ingin melakukan pemesanan dalam jumlah banyak atau pemesanan untuk produk yang sama lebih dari 5 unit, silahkan isi data pemesanan Anda di <a href="http://www.lazada.co.id/bulk-order/">www.lazada.co.id/bulk-order/</a></p>\r\n<p>Mohon cantumkan informasi sebagai berikut pada email yang Anda kirimkan:</p>\r\n<ul>\r\n<li>Nama pemesan</li>\r\n<li>Alamat email yang digunakan untuk login Lazada</li>\r\n<li>Nomor telepon yang bisa dihubungi</li>\r\n<li>Nama produk dan SKU yang akan dipesan</li>\r\n<li>Jumlah produk yang akan dipesan</li>\r\n<li>Metode pembayaran yang digunakan</li>\r\n</ul>\r\n<p>Tim terkait kami akan menghubungi Anda untuk informasi lebih lanjut.</p>\r\n<p>Berikut adalah ketentuan untuk pemesanan dalam jumlah banyak:</p>\r\n<ul>\r\n<ul>1. Tidak berlaku untuk pembayaran dengan COD (Bayar ditempat)</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>2. Pemesanan tidak dapat menggunakan voucher dan tidak dapat mengikuti program cicilan</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>3. Alasan pengembalian barang (Return) tidak berlaku untuk alasan berubah pikiran (Change mind)</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>4. Hanya berlaku untuk pemesanan atas nama personal (non corporate)</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>5. Untuk pembayaran dengan Bank Transfer, mohon lakukan pembayaran maksimum 1 jam setelah pemesanan</ul>\r\n<p>&nbsp;</p>', '2016-12-08 06:54:22', '2016-12-08 06:54:22'),
	(2, 'cara_pembayaran', '', '2016-12-08 06:54:22', '2016-12-08 06:54:22'),
	(3, 'retur_barang', '', '2016-12-08 06:54:22', '2016-12-08 06:54:22'),
	(4, 'sistem_pengiriman', '', '2016-12-08 06:54:22', '2016-12-08 06:54:22');
/*!40000 ALTER TABLE `tref_konten` ENABLE KEYS */;

-- membuang struktur untuk table pos.tref_kota
CREATE TABLE IF NOT EXISTS `tref_kota` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provinsi_kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=515 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tref_kota: ~514 rows (lebih kurang)
DELETE FROM `tref_kota`;
/*!40000 ALTER TABLE `tref_kota` DISABLE KEYS */;
INSERT INTO `tref_kota` (`id`, `kode`, `provinsi_kode`, `nama`, `created_at`, `updated_at`) VALUES
	(1, '1101', '11', 'KABUPATEN SIMEULUE', NULL, NULL),
	(2, '1102', '11', 'KABUPATEN ACEH SINGKIL', NULL, NULL),
	(3, '1103', '11', 'KABUPATEN ACEH SELATAN', NULL, NULL),
	(4, '1104', '11', 'KABUPATEN ACEH TENGGARA', NULL, NULL),
	(5, '1105', '11', 'KABUPATEN ACEH TIMUR', NULL, NULL),
	(6, '1106', '11', 'KABUPATEN ACEH TENGAH', NULL, NULL),
	(7, '1107', '11', 'KABUPATEN ACEH BARAT', NULL, NULL),
	(8, '1108', '11', 'KABUPATEN ACEH BESAR', NULL, NULL),
	(9, '1109', '11', 'KABUPATEN PIDIE', NULL, NULL),
	(10, '1110', '11', 'KABUPATEN BIREUEN', NULL, NULL),
	(11, '1111', '11', 'KABUPATEN ACEH UTARA', NULL, NULL),
	(12, '1112', '11', 'KABUPATEN ACEH BARAT DAYA', NULL, NULL),
	(13, '1113', '11', 'KABUPATEN GAYO LUES', NULL, NULL),
	(14, '1114', '11', 'KABUPATEN ACEH TAMIANG', NULL, NULL),
	(15, '1115', '11', 'KABUPATEN NAGAN RAYA', NULL, NULL),
	(16, '1116', '11', 'KABUPATEN ACEH JAYA', NULL, NULL),
	(17, '1117', '11', 'KABUPATEN BENER MERIAH', NULL, NULL),
	(18, '1118', '11', 'KABUPATEN PIDIE JAYA', NULL, NULL),
	(19, '1171', '11', 'KOTA BANDA ACEH', NULL, NULL),
	(20, '1172', '11', 'KOTA SABANG', NULL, NULL),
	(21, '1173', '11', 'KOTA LANGSA', NULL, NULL),
	(22, '1174', '11', 'KOTA LHOKSEUMAWE', NULL, NULL),
	(23, '1175', '11', 'KOTA SUBULUSSALAM', NULL, NULL),
	(24, '1201', '12', 'KABUPATEN NIAS', NULL, NULL),
	(25, '1202', '12', 'KABUPATEN MANDAILING NATAL', NULL, NULL),
	(26, '1203', '12', 'KABUPATEN TAPANULI SELATAN', NULL, NULL),
	(27, '1204', '12', 'KABUPATEN TAPANULI TENGAH', NULL, NULL),
	(28, '1205', '12', 'KABUPATEN TAPANULI UTARA', NULL, NULL),
	(29, '1206', '12', 'KABUPATEN TOBA SAMOSIR', NULL, NULL),
	(30, '1207', '12', 'KABUPATEN LABUHAN BATU', NULL, NULL),
	(31, '1208', '12', 'KABUPATEN ASAHAN', NULL, NULL),
	(32, '1209', '12', 'KABUPATEN SIMALUNGUN', NULL, NULL),
	(33, '1210', '12', 'KABUPATEN DAIRI', NULL, NULL),
	(34, '1211', '12', 'KABUPATEN KARO', NULL, NULL),
	(35, '1212', '12', 'KABUPATEN DELI SERDANG', NULL, NULL),
	(36, '1213', '12', 'KABUPATEN LANGKAT', NULL, NULL),
	(37, '1214', '12', 'KABUPATEN NIAS SELATAN', NULL, NULL),
	(38, '1215', '12', 'KABUPATEN HUMBANG HASUNDUTAN', NULL, NULL),
	(39, '1216', '12', 'KABUPATEN PAKPAK BHARAT', NULL, NULL),
	(40, '1217', '12', 'KABUPATEN SAMOSIR', NULL, NULL),
	(41, '1218', '12', 'KABUPATEN SERDANG BEDAGAI', NULL, NULL),
	(42, '1219', '12', 'KABUPATEN BATU BARA', NULL, NULL),
	(43, '1220', '12', 'KABUPATEN PADANG LAWAS UTARA', NULL, NULL),
	(44, '1221', '12', 'KABUPATEN PADANG LAWAS', NULL, NULL),
	(45, '1222', '12', 'KABUPATEN LABUHAN BATU SELATAN', NULL, NULL),
	(46, '1223', '12', 'KABUPATEN LABUHAN BATU UTARA', NULL, NULL),
	(47, '1224', '12', 'KABUPATEN NIAS UTARA', NULL, NULL),
	(48, '1225', '12', 'KABUPATEN NIAS BARAT', NULL, NULL),
	(49, '1271', '12', 'KOTA SIBOLGA', NULL, NULL),
	(50, '1272', '12', 'KOTA TANJUNG BALAI', NULL, NULL),
	(51, '1273', '12', 'KOTA PEMATANG SIANTAR', NULL, NULL),
	(52, '1274', '12', 'KOTA TEBING TINGGI', NULL, NULL),
	(53, '1275', '12', 'KOTA MEDAN', NULL, NULL),
	(54, '1276', '12', 'KOTA BINJAI', NULL, NULL),
	(55, '1277', '12', 'KOTA PADANGSIDIMPUAN', NULL, NULL),
	(56, '1278', '12', 'KOTA GUNUNGSITOLI', NULL, NULL),
	(57, '1301', '13', 'KABUPATEN KEPULAUAN MENTAWAI', NULL, NULL),
	(58, '1302', '13', 'KABUPATEN PESISIR SELATAN', NULL, NULL),
	(59, '1303', '13', 'KABUPATEN SOLOK', NULL, NULL),
	(60, '1304', '13', 'KABUPATEN SIJUNJUNG', NULL, NULL),
	(61, '1305', '13', 'KABUPATEN TANAH DATAR', NULL, NULL),
	(62, '1306', '13', 'KABUPATEN PADANG PARIAMAN', NULL, NULL),
	(63, '1307', '13', 'KABUPATEN AGAM', NULL, NULL),
	(64, '1308', '13', 'KABUPATEN LIMA PULUH KOTA', NULL, NULL),
	(65, '1309', '13', 'KABUPATEN PASAMAN', NULL, NULL),
	(66, '1310', '13', 'KABUPATEN SOLOK SELATAN', NULL, NULL),
	(67, '1311', '13', 'KABUPATEN DHARMASRAYA', NULL, NULL),
	(68, '1312', '13', 'KABUPATEN PASAMAN BARAT', NULL, NULL),
	(69, '1371', '13', 'KOTA PADANG', NULL, NULL),
	(70, '1372', '13', 'KOTA SOLOK', NULL, NULL),
	(71, '1373', '13', 'KOTA SAWAH LUNTO', NULL, NULL),
	(72, '1374', '13', 'KOTA PADANG PANJANG', NULL, NULL),
	(73, '1375', '13', 'KOTA BUKITTINGGI', NULL, NULL),
	(74, '1376', '13', 'KOTA PAYAKUMBUH', NULL, NULL),
	(75, '1377', '13', 'KOTA PARIAMAN', NULL, NULL),
	(76, '1401', '14', 'KABUPATEN KUANTAN SINGINGI', NULL, NULL),
	(77, '1402', '14', 'KABUPATEN INDRAGIRI HULU', NULL, NULL),
	(78, '1403', '14', 'KABUPATEN INDRAGIRI HILIR', NULL, NULL),
	(79, '1404', '14', 'KABUPATEN PELALAWAN', NULL, NULL),
	(80, '1405', '14', 'KABUPATEN S I A K', NULL, NULL),
	(81, '1406', '14', 'KABUPATEN KAMPAR', NULL, NULL),
	(82, '1407', '14', 'KABUPATEN ROKAN HULU', NULL, NULL),
	(83, '1408', '14', 'KABUPATEN BENGKALIS', NULL, NULL),
	(84, '1409', '14', 'KABUPATEN ROKAN HILIR', NULL, NULL),
	(85, '1410', '14', 'KABUPATEN KEPULAUAN MERANTI', NULL, NULL),
	(86, '1471', '14', 'KOTA PEKANBARU', NULL, NULL),
	(87, '1473', '14', 'KOTA D U M A I', NULL, NULL),
	(88, '1501', '15', 'KABUPATEN KERINCI', NULL, NULL),
	(89, '1502', '15', 'KABUPATEN MERANGIN', NULL, NULL),
	(90, '1503', '15', 'KABUPATEN SAROLANGUN', NULL, NULL),
	(91, '1504', '15', 'KABUPATEN BATANG HARI', NULL, NULL),
	(92, '1505', '15', 'KABUPATEN MUARO JAMBI', NULL, NULL),
	(93, '1506', '15', 'KABUPATEN TANJUNG JABUNG TIMUR', NULL, NULL),
	(94, '1507', '15', 'KABUPATEN TANJUNG JABUNG BARAT', NULL, NULL),
	(95, '1508', '15', 'KABUPATEN TEBO', NULL, NULL),
	(96, '1509', '15', 'KABUPATEN BUNGO', NULL, NULL),
	(97, '1571', '15', 'KOTA JAMBI', NULL, NULL),
	(98, '1572', '15', 'KOTA SUNGAI PENUH', NULL, NULL),
	(99, '1601', '16', 'KABUPATEN OGAN KOMERING ULU', NULL, NULL),
	(100, '1602', '16', 'KABUPATEN OGAN KOMERING ILIR', NULL, NULL),
	(101, '1603', '16', 'KABUPATEN MUARA ENIM', NULL, NULL),
	(102, '1604', '16', 'KABUPATEN LAHAT', NULL, NULL),
	(103, '1605', '16', 'KABUPATEN MUSI RAWAS', NULL, NULL),
	(104, '1606', '16', 'KABUPATEN MUSI BANYUASIN', NULL, NULL),
	(105, '1607', '16', 'KABUPATEN BANYU ASIN', NULL, NULL),
	(106, '1608', '16', 'KABUPATEN OGAN KOMERING ULU SELATAN', NULL, NULL),
	(107, '1609', '16', 'KABUPATEN OGAN KOMERING ULU TIMUR', NULL, NULL),
	(108, '1610', '16', 'KABUPATEN OGAN ILIR', NULL, NULL),
	(109, '1611', '16', 'KABUPATEN EMPAT LAWANG', NULL, NULL),
	(110, '1612', '16', 'KABUPATEN PENUKAL ABAB LEMATANG ILIR', NULL, NULL),
	(111, '1613', '16', 'KABUPATEN MUSI RAWAS UTARA', NULL, NULL),
	(112, '1671', '16', 'KOTA PALEMBANG', NULL, NULL),
	(113, '1672', '16', 'KOTA PRABUMULIH', NULL, NULL),
	(114, '1673', '16', 'KOTA PAGAR ALAM', NULL, NULL),
	(115, '1674', '16', 'KOTA LUBUKLINGGAU', NULL, NULL),
	(116, '1701', '17', 'KABUPATEN BENGKULU SELATAN', NULL, NULL),
	(117, '1702', '17', 'KABUPATEN REJANG LEBONG', NULL, NULL),
	(118, '1703', '17', 'KABUPATEN BENGKULU UTARA', NULL, NULL),
	(119, '1704', '17', 'KABUPATEN KAUR', NULL, NULL),
	(120, '1705', '17', 'KABUPATEN SELUMA', NULL, NULL),
	(121, '1706', '17', 'KABUPATEN MUKOMUKO', NULL, NULL),
	(122, '1707', '17', 'KABUPATEN LEBONG', NULL, NULL),
	(123, '1708', '17', 'KABUPATEN KEPAHIANG', NULL, NULL),
	(124, '1709', '17', 'KABUPATEN BENGKULU TENGAH', NULL, NULL),
	(125, '1771', '17', 'KOTA BENGKULU', NULL, NULL),
	(126, '1801', '18', 'KABUPATEN LAMPUNG BARAT', NULL, NULL),
	(127, '1802', '18', 'KABUPATEN TANGGAMUS', NULL, NULL),
	(128, '1803', '18', 'KABUPATEN LAMPUNG SELATAN', NULL, NULL),
	(129, '1804', '18', 'KABUPATEN LAMPUNG TIMUR', NULL, NULL),
	(130, '1805', '18', 'KABUPATEN LAMPUNG TENGAH', NULL, NULL),
	(131, '1806', '18', 'KABUPATEN LAMPUNG UTARA', NULL, NULL),
	(132, '1807', '18', 'KABUPATEN WAY KANAN', NULL, NULL),
	(133, '1808', '18', 'KABUPATEN TULANGBAWANG', NULL, NULL),
	(134, '1809', '18', 'KABUPATEN PESAWARAN', NULL, NULL),
	(135, '1810', '18', 'KABUPATEN PRINGSEWU', NULL, NULL),
	(136, '1811', '18', 'KABUPATEN MESUJI', NULL, NULL),
	(137, '1812', '18', 'KABUPATEN TULANG BAWANG BARAT', NULL, NULL),
	(138, '1813', '18', 'KABUPATEN PESISIR BARAT', NULL, NULL),
	(139, '1871', '18', 'KOTA BANDAR LAMPUNG', NULL, NULL),
	(140, '1872', '18', 'KOTA METRO', NULL, NULL),
	(141, '1901', '19', 'KABUPATEN BANGKA', NULL, NULL),
	(142, '1902', '19', 'KABUPATEN BELITUNG', NULL, NULL),
	(143, '1903', '19', 'KABUPATEN BANGKA BARAT', NULL, NULL),
	(144, '1904', '19', 'KABUPATEN BANGKA TENGAH', NULL, NULL),
	(145, '1905', '19', 'KABUPATEN BANGKA SELATAN', NULL, NULL),
	(146, '1906', '19', 'KABUPATEN BELITUNG TIMUR', NULL, NULL),
	(147, '1971', '19', 'KOTA PANGKAL PINANG', NULL, NULL),
	(148, '2101', '21', 'KABUPATEN KARIMUN', NULL, NULL),
	(149, '2102', '21', 'KABUPATEN BINTAN', NULL, NULL),
	(150, '2103', '21', 'KABUPATEN NATUNA', NULL, NULL),
	(151, '2104', '21', 'KABUPATEN LINGGA', NULL, NULL),
	(152, '2105', '21', 'KABUPATEN KEPULAUAN ANAMBAS', NULL, NULL),
	(153, '2171', '21', 'KOTA B A T A M', NULL, NULL),
	(154, '2172', '21', 'KOTA TANJUNG PINANG', NULL, NULL),
	(155, '3101', '31', 'KABUPATEN KEPULAUAN SERIBU', NULL, NULL),
	(156, '3171', '31', 'KOTA JAKARTA SELATAN', NULL, NULL),
	(157, '3172', '31', 'KOTA JAKARTA TIMUR', NULL, NULL),
	(158, '3173', '31', 'KOTA JAKARTA PUSAT', NULL, NULL),
	(159, '3174', '31', 'KOTA JAKARTA BARAT', NULL, NULL),
	(160, '3175', '31', 'KOTA JAKARTA UTARA', NULL, NULL),
	(161, '3201', '32', 'KABUPATEN BOGOR', NULL, NULL),
	(162, '3202', '32', 'KABUPATEN SUKABUMI', NULL, NULL),
	(163, '3203', '32', 'KABUPATEN CIANJUR', NULL, NULL),
	(164, '3204', '32', 'KABUPATEN BANDUNG', NULL, NULL),
	(165, '3205', '32', 'KABUPATEN GARUT', NULL, NULL),
	(166, '3206', '32', 'KABUPATEN TASIKMALAYA', NULL, NULL),
	(167, '3207', '32', 'KABUPATEN CIAMIS', NULL, NULL),
	(168, '3208', '32', 'KABUPATEN KUNINGAN', NULL, NULL),
	(169, '3209', '32', 'KABUPATEN CIREBON', NULL, NULL),
	(170, '3210', '32', 'KABUPATEN MAJALENGKA', NULL, NULL),
	(171, '3211', '32', 'KABUPATEN SUMEDANG', NULL, NULL),
	(172, '3212', '32', 'KABUPATEN INDRAMAYU', NULL, NULL),
	(173, '3213', '32', 'KABUPATEN SUBANG', NULL, NULL),
	(174, '3214', '32', 'KABUPATEN PURWAKARTA', NULL, NULL),
	(175, '3215', '32', 'KABUPATEN KARAWANG', NULL, NULL),
	(176, '3216', '32', 'KABUPATEN BEKASI', NULL, NULL),
	(177, '3217', '32', 'KABUPATEN BANDUNG BARAT', NULL, NULL),
	(178, '3218', '32', 'KABUPATEN PANGANDARAN', NULL, NULL),
	(179, '3271', '32', 'KOTA BOGOR', NULL, NULL),
	(180, '3272', '32', 'KOTA SUKABUMI', NULL, NULL),
	(181, '3273', '32', 'KOTA BANDUNG', NULL, NULL),
	(182, '3274', '32', 'KOTA CIREBON', NULL, NULL),
	(183, '3275', '32', 'KOTA BEKASI', NULL, NULL),
	(184, '3276', '32', 'KOTA DEPOK', NULL, NULL),
	(185, '3277', '32', 'KOTA CIMAHI', NULL, NULL),
	(186, '3278', '32', 'KOTA TASIKMALAYA', NULL, NULL),
	(187, '3279', '32', 'KOTA BANJAR', NULL, NULL),
	(188, '3301', '33', 'KABUPATEN CILACAP', NULL, NULL),
	(189, '3302', '33', 'KABUPATEN BANYUMAS', NULL, NULL),
	(190, '3303', '33', 'KABUPATEN PURBALINGGA', NULL, NULL),
	(191, '3304', '33', 'KABUPATEN BANJARNEGARA', NULL, NULL),
	(192, '3305', '33', 'KABUPATEN KEBUMEN', NULL, NULL),
	(193, '3306', '33', 'KABUPATEN PURWOREJO', NULL, NULL),
	(194, '3307', '33', 'KABUPATEN WONOSOBO', NULL, NULL),
	(195, '3308', '33', 'KABUPATEN MAGELANG', NULL, NULL),
	(196, '3309', '33', 'KABUPATEN BOYOLALI', NULL, NULL),
	(197, '3310', '33', 'KABUPATEN KLATEN', NULL, NULL),
	(198, '3311', '33', 'KABUPATEN SUKOHARJO', NULL, NULL),
	(199, '3312', '33', 'KABUPATEN WONOGIRI', NULL, NULL),
	(200, '3313', '33', 'KABUPATEN KARANGANYAR', NULL, NULL),
	(201, '3314', '33', 'KABUPATEN SRAGEN', NULL, NULL),
	(202, '3315', '33', 'KABUPATEN GROBOGAN', NULL, NULL),
	(203, '3316', '33', 'KABUPATEN BLORA', NULL, NULL),
	(204, '3317', '33', 'KABUPATEN REMBANG', NULL, NULL),
	(205, '3318', '33', 'KABUPATEN PATI', NULL, NULL),
	(206, '3319', '33', 'KABUPATEN KUDUS', NULL, NULL),
	(207, '3320', '33', 'KABUPATEN JEPARA', NULL, NULL),
	(208, '3321', '33', 'KABUPATEN DEMAK', NULL, NULL),
	(209, '3322', '33', 'KABUPATEN SEMARANG', NULL, NULL),
	(210, '3323', '33', 'KABUPATEN TEMANGGUNG', NULL, NULL),
	(211, '3324', '33', 'KABUPATEN KENDAL', NULL, NULL),
	(212, '3325', '33', 'KABUPATEN BATANG', NULL, NULL),
	(213, '3326', '33', 'KABUPATEN PEKALONGAN', NULL, NULL),
	(214, '3327', '33', 'KABUPATEN PEMALANG', NULL, NULL),
	(215, '3328', '33', 'KABUPATEN TEGAL', NULL, NULL),
	(216, '3329', '33', 'KABUPATEN BREBES', NULL, NULL),
	(217, '3371', '33', 'KOTA MAGELANG', NULL, NULL),
	(218, '3372', '33', 'KOTA SURAKARTA', NULL, NULL),
	(219, '3373', '33', 'KOTA SALATIGA', NULL, NULL),
	(220, '3374', '33', 'KOTA SEMARANG', NULL, NULL),
	(221, '3375', '33', 'KOTA PEKALONGAN', NULL, NULL),
	(222, '3376', '33', 'KOTA TEGAL', NULL, NULL),
	(223, '3401', '34', 'KABUPATEN KULON PROGO', NULL, NULL),
	(224, '3402', '34', 'KABUPATEN BANTUL', NULL, NULL),
	(225, '3403', '34', 'KABUPATEN GUNUNG KIDUL', NULL, NULL),
	(226, '3404', '34', 'KABUPATEN SLEMAN', NULL, NULL),
	(227, '3471', '34', 'KOTA YOGYAKARTA', NULL, NULL),
	(228, '3501', '35', 'KABUPATEN PACITAN', NULL, NULL),
	(229, '3502', '35', 'KABUPATEN PONOROGO', NULL, NULL),
	(230, '3503', '35', 'KABUPATEN TRENGGALEK', NULL, NULL),
	(231, '3504', '35', 'KABUPATEN TULUNGAGUNG', NULL, NULL),
	(232, '3505', '35', 'KABUPATEN BLITAR', NULL, NULL),
	(233, '3506', '35', 'KABUPATEN KEDIRI', NULL, NULL),
	(234, '3507', '35', 'KABUPATEN MALANG', NULL, NULL),
	(235, '3508', '35', 'KABUPATEN LUMAJANG', NULL, NULL),
	(236, '3509', '35', 'KABUPATEN JEMBER', NULL, NULL),
	(237, '3510', '35', 'KABUPATEN BANYUWANGI', NULL, NULL),
	(238, '3511', '35', 'KABUPATEN BONDOWOSO', NULL, NULL),
	(239, '3512', '35', 'KABUPATEN SITUBONDO', NULL, NULL),
	(240, '3513', '35', 'KABUPATEN PROBOLINGGO', NULL, NULL),
	(241, '3514', '35', 'KABUPATEN PASURUAN', NULL, NULL),
	(242, '3515', '35', 'KABUPATEN SIDOARJO', NULL, NULL),
	(243, '3516', '35', 'KABUPATEN MOJOKERTO', NULL, NULL),
	(244, '3517', '35', 'KABUPATEN JOMBANG', NULL, NULL),
	(245, '3518', '35', 'KABUPATEN NGANJUK', NULL, NULL),
	(246, '3519', '35', 'KABUPATEN MADIUN', NULL, NULL),
	(247, '3520', '35', 'KABUPATEN MAGETAN', NULL, NULL),
	(248, '3521', '35', 'KABUPATEN NGAWI', NULL, NULL),
	(249, '3522', '35', 'KABUPATEN BOJONEGORO', NULL, NULL),
	(250, '3523', '35', 'KABUPATEN TUBAN', NULL, NULL),
	(251, '3524', '35', 'KABUPATEN LAMONGAN', NULL, NULL),
	(252, '3525', '35', 'KABUPATEN GRESIK', NULL, NULL),
	(253, '3526', '35', 'KABUPATEN BANGKALAN', NULL, NULL),
	(254, '3527', '35', 'KABUPATEN SAMPANG', NULL, NULL),
	(255, '3528', '35', 'KABUPATEN PAMEKASAN', NULL, NULL),
	(256, '3529', '35', 'KABUPATEN SUMENEP', NULL, NULL),
	(257, '3571', '35', 'KOTA KEDIRI', NULL, NULL),
	(258, '3572', '35', 'KOTA BLITAR', NULL, NULL),
	(259, '3573', '35', 'KOTA MALANG', NULL, NULL),
	(260, '3574', '35', 'KOTA PROBOLINGGO', NULL, NULL),
	(261, '3575', '35', 'KOTA PASURUAN', NULL, NULL),
	(262, '3576', '35', 'KOTA MOJOKERTO', NULL, NULL),
	(263, '3577', '35', 'KOTA MADIUN', NULL, NULL),
	(264, '3578', '35', 'KOTA SURABAYA', NULL, NULL),
	(265, '3579', '35', 'KOTA BATU', NULL, NULL),
	(266, '3601', '36', 'KABUPATEN PANDEGLANG', NULL, NULL),
	(267, '3602', '36', 'KABUPATEN LEBAK', NULL, NULL),
	(268, '3603', '36', 'KABUPATEN TANGERANG', NULL, NULL),
	(269, '3604', '36', 'KABUPATEN SERANG', NULL, NULL),
	(270, '3671', '36', 'KOTA TANGERANG', NULL, NULL),
	(271, '3672', '36', 'KOTA CILEGON', NULL, NULL),
	(272, '3673', '36', 'KOTA SERANG', NULL, NULL),
	(273, '3674', '36', 'KOTA TANGERANG SELATAN', NULL, NULL),
	(274, '5101', '51', 'KABUPATEN JEMBRANA', NULL, NULL),
	(275, '5102', '51', 'KABUPATEN TABANAN', NULL, NULL),
	(276, '5103', '51', 'KABUPATEN BADUNG', NULL, NULL),
	(277, '5104', '51', 'KABUPATEN GIANYAR', NULL, NULL),
	(278, '5105', '51', 'KABUPATEN KLUNGKUNG', NULL, NULL),
	(279, '5106', '51', 'KABUPATEN BANGLI', NULL, NULL),
	(280, '5107', '51', 'KABUPATEN KARANG ASEM', NULL, NULL),
	(281, '5108', '51', 'KABUPATEN BULELENG', NULL, NULL),
	(282, '5171', '51', 'KOTA DENPASAR', NULL, NULL),
	(283, '5201', '52', 'KABUPATEN LOMBOK BARAT', NULL, NULL),
	(284, '5202', '52', 'KABUPATEN LOMBOK TENGAH', NULL, NULL),
	(285, '5203', '52', 'KABUPATEN LOMBOK TIMUR', NULL, NULL),
	(286, '5204', '52', 'KABUPATEN SUMBAWA', NULL, NULL),
	(287, '5205', '52', 'KABUPATEN DOMPU', NULL, NULL),
	(288, '5206', '52', 'KABUPATEN BIMA', NULL, NULL),
	(289, '5207', '52', 'KABUPATEN SUMBAWA BARAT', NULL, NULL),
	(290, '5208', '52', 'KABUPATEN LOMBOK UTARA', NULL, NULL),
	(291, '5271', '52', 'KOTA MATARAM', NULL, NULL),
	(292, '5272', '52', 'KOTA BIMA', NULL, NULL),
	(293, '5301', '53', 'KABUPATEN SUMBA BARAT', NULL, NULL),
	(294, '5302', '53', 'KABUPATEN SUMBA TIMUR', NULL, NULL),
	(295, '5303', '53', 'KABUPATEN KUPANG', NULL, NULL),
	(296, '5304', '53', 'KABUPATEN TIMOR TENGAH SELATAN', NULL, NULL),
	(297, '5305', '53', 'KABUPATEN TIMOR TENGAH UTARA', NULL, NULL),
	(298, '5306', '53', 'KABUPATEN BELU', NULL, NULL),
	(299, '5307', '53', 'KABUPATEN ALOR', NULL, NULL),
	(300, '5308', '53', 'KABUPATEN LEMBATA', NULL, NULL),
	(301, '5309', '53', 'KABUPATEN FLORES TIMUR', NULL, NULL),
	(302, '5310', '53', 'KABUPATEN SIKKA', NULL, NULL),
	(303, '5311', '53', 'KABUPATEN ENDE', NULL, NULL),
	(304, '5312', '53', 'KABUPATEN NGADA', NULL, NULL),
	(305, '5313', '53', 'KABUPATEN MANGGARAI', NULL, NULL),
	(306, '5314', '53', 'KABUPATEN ROTE NDAO', NULL, NULL),
	(307, '5315', '53', 'KABUPATEN MANGGARAI BARAT', NULL, NULL),
	(308, '5316', '53', 'KABUPATEN SUMBA TENGAH', NULL, NULL),
	(309, '5317', '53', 'KABUPATEN SUMBA BARAT DAYA', NULL, NULL),
	(310, '5318', '53', 'KABUPATEN NAGEKEO', NULL, NULL),
	(311, '5319', '53', 'KABUPATEN MANGGARAI TIMUR', NULL, NULL),
	(312, '5320', '53', 'KABUPATEN SABU RAIJUA', NULL, NULL),
	(313, '5321', '53', 'KABUPATEN MALAKA', NULL, NULL),
	(314, '5371', '53', 'KOTA KUPANG', NULL, NULL),
	(315, '6101', '61', 'KABUPATEN SAMBAS', NULL, NULL),
	(316, '6102', '61', 'KABUPATEN BENGKAYANG', NULL, NULL),
	(317, '6103', '61', 'KABUPATEN LANDAK', NULL, NULL),
	(318, '6104', '61', 'KABUPATEN MEMPAWAH', NULL, NULL),
	(319, '6105', '61', 'KABUPATEN SANGGAU', NULL, NULL),
	(320, '6106', '61', 'KABUPATEN KETAPANG', NULL, NULL),
	(321, '6107', '61', 'KABUPATEN SINTANG', NULL, NULL),
	(322, '6108', '61', 'KABUPATEN KAPUAS HULU', NULL, NULL),
	(323, '6109', '61', 'KABUPATEN SEKADAU', NULL, NULL),
	(324, '6110', '61', 'KABUPATEN MELAWI', NULL, NULL),
	(325, '6111', '61', 'KABUPATEN KAYONG UTARA', NULL, NULL),
	(326, '6112', '61', 'KABUPATEN KUBU RAYA', NULL, NULL),
	(327, '6171', '61', 'KOTA PONTIANAK', NULL, NULL),
	(328, '6172', '61', 'KOTA SINGKAWANG', NULL, NULL),
	(329, '6201', '62', 'KABUPATEN KOTAWARINGIN BARAT', NULL, NULL),
	(330, '6202', '62', 'KABUPATEN KOTAWARINGIN TIMUR', NULL, NULL),
	(331, '6203', '62', 'KABUPATEN KAPUAS', NULL, NULL),
	(332, '6204', '62', 'KABUPATEN BARITO SELATAN', NULL, NULL),
	(333, '6205', '62', 'KABUPATEN BARITO UTARA', NULL, NULL),
	(334, '6206', '62', 'KABUPATEN SUKAMARA', NULL, NULL),
	(335, '6207', '62', 'KABUPATEN LAMANDAU', NULL, NULL),
	(336, '6208', '62', 'KABUPATEN SERUYAN', NULL, NULL),
	(337, '6209', '62', 'KABUPATEN KATINGAN', NULL, NULL),
	(338, '6210', '62', 'KABUPATEN PULANG PISAU', NULL, NULL),
	(339, '6211', '62', 'KABUPATEN GUNUNG MAS', NULL, NULL),
	(340, '6212', '62', 'KABUPATEN BARITO TIMUR', NULL, NULL),
	(341, '6213', '62', 'KABUPATEN MURUNG RAYA', NULL, NULL),
	(342, '6271', '62', 'KOTA PALANGKA RAYA', NULL, NULL),
	(343, '6301', '63', 'KABUPATEN TANAH LAUT', NULL, NULL),
	(344, '6302', '63', 'KABUPATEN KOTA BARU', NULL, NULL),
	(345, '6303', '63', 'KABUPATEN BANJAR', NULL, NULL),
	(346, '6304', '63', 'KABUPATEN BARITO KUALA', NULL, NULL),
	(347, '6305', '63', 'KABUPATEN TAPIN', NULL, NULL),
	(348, '6306', '63', 'KABUPATEN HULU SUNGAI SELATAN', NULL, NULL),
	(349, '6307', '63', 'KABUPATEN HULU SUNGAI TENGAH', NULL, NULL),
	(350, '6308', '63', 'KABUPATEN HULU SUNGAI UTARA', NULL, NULL),
	(351, '6309', '63', 'KABUPATEN TABALONG', NULL, NULL),
	(352, '6310', '63', 'KABUPATEN TANAH BUMBU', NULL, NULL),
	(353, '6311', '63', 'KABUPATEN BALANGAN', NULL, NULL),
	(354, '6371', '63', 'KOTA BANJARMASIN', NULL, NULL),
	(355, '6372', '63', 'KOTA BANJAR BARU', NULL, NULL),
	(356, '6401', '64', 'KABUPATEN PASER', NULL, NULL),
	(357, '6402', '64', 'KABUPATEN KUTAI BARAT', NULL, NULL),
	(358, '6403', '64', 'KABUPATEN KUTAI KARTANEGARA', NULL, NULL),
	(359, '6404', '64', 'KABUPATEN KUTAI TIMUR', NULL, NULL),
	(360, '6405', '64', 'KABUPATEN BERAU', NULL, NULL),
	(361, '6409', '64', 'KABUPATEN PENAJAM PASER UTARA', NULL, NULL),
	(362, '6411', '64', 'KABUPATEN MAHAKAM HULU', NULL, NULL),
	(363, '6471', '64', 'KOTA BALIKPAPAN', NULL, NULL),
	(364, '6472', '64', 'KOTA SAMARINDA', NULL, NULL),
	(365, '6474', '64', 'KOTA BONTANG', NULL, NULL),
	(366, '6501', '65', 'KABUPATEN MALINAU', NULL, NULL),
	(367, '6502', '65', 'KABUPATEN BULUNGAN', NULL, NULL),
	(368, '6503', '65', 'KABUPATEN TANA TIDUNG', NULL, NULL),
	(369, '6504', '65', 'KABUPATEN NUNUKAN', NULL, NULL),
	(370, '6571', '65', 'KOTA TARAKAN', NULL, NULL),
	(371, '7101', '71', 'KABUPATEN BOLAANG MONGONDOW', NULL, NULL),
	(372, '7102', '71', 'KABUPATEN MINAHASA', NULL, NULL),
	(373, '7103', '71', 'KABUPATEN KEPULAUAN SANGIHE', NULL, NULL),
	(374, '7104', '71', 'KABUPATEN KEPULAUAN TALAUD', NULL, NULL),
	(375, '7105', '71', 'KABUPATEN MINAHASA SELATAN', NULL, NULL),
	(376, '7106', '71', 'KABUPATEN MINAHASA UTARA', NULL, NULL),
	(377, '7107', '71', 'KABUPATEN BOLAANG MONGONDOW UTARA', NULL, NULL),
	(378, '7108', '71', 'KABUPATEN SIAU TAGULANDANG BIARO', NULL, NULL),
	(379, '7109', '71', 'KABUPATEN MINAHASA TENGGARA', NULL, NULL),
	(380, '7110', '71', 'KABUPATEN BOLAANG MONGONDOW SELATAN', NULL, NULL),
	(381, '7111', '71', 'KABUPATEN BOLAANG MONGONDOW TIMUR', NULL, NULL),
	(382, '7171', '71', 'KOTA MANADO', NULL, NULL),
	(383, '7172', '71', 'KOTA BITUNG', NULL, NULL),
	(384, '7173', '71', 'KOTA TOMOHON', NULL, NULL),
	(385, '7174', '71', 'KOTA KOTAMOBAGU', NULL, NULL),
	(386, '7201', '72', 'KABUPATEN BANGGAI KEPULAUAN', NULL, NULL),
	(387, '7202', '72', 'KABUPATEN BANGGAI', NULL, NULL),
	(388, '7203', '72', 'KABUPATEN MOROWALI', NULL, NULL),
	(389, '7204', '72', 'KABUPATEN POSO', NULL, NULL),
	(390, '7205', '72', 'KABUPATEN DONGGALA', NULL, NULL),
	(391, '7206', '72', 'KABUPATEN TOLI-TOLI', NULL, NULL),
	(392, '7207', '72', 'KABUPATEN BUOL', NULL, NULL),
	(393, '7208', '72', 'KABUPATEN PARIGI MOUTONG', NULL, NULL),
	(394, '7209', '72', 'KABUPATEN TOJO UNA-UNA', NULL, NULL),
	(395, '7210', '72', 'KABUPATEN SIGI', NULL, NULL),
	(396, '7211', '72', 'KABUPATEN BANGGAI LAUT', NULL, NULL),
	(397, '7212', '72', 'KABUPATEN MOROWALI UTARA', NULL, NULL),
	(398, '7271', '72', 'KOTA PALU', NULL, NULL),
	(399, '7301', '73', 'KABUPATEN KEPULAUAN SELAYAR', NULL, NULL),
	(400, '7302', '73', 'KABUPATEN BULUKUMBA', NULL, NULL),
	(401, '7303', '73', 'KABUPATEN BANTAENG', NULL, NULL),
	(402, '7304', '73', 'KABUPATEN JENEPONTO', NULL, NULL),
	(403, '7305', '73', 'KABUPATEN TAKALAR', NULL, NULL),
	(404, '7306', '73', 'KABUPATEN GOWA', NULL, NULL),
	(405, '7307', '73', 'KABUPATEN SINJAI', NULL, NULL),
	(406, '7308', '73', 'KABUPATEN MAROS', NULL, NULL),
	(407, '7309', '73', 'KABUPATEN PANGKAJENE DAN KEPULAUAN', NULL, NULL),
	(408, '7310', '73', 'KABUPATEN BARRU', NULL, NULL),
	(409, '7311', '73', 'KABUPATEN BONE', NULL, NULL),
	(410, '7312', '73', 'KABUPATEN SOPPENG', NULL, NULL),
	(411, '7313', '73', 'KABUPATEN WAJO', NULL, NULL),
	(412, '7314', '73', 'KABUPATEN SIDENRENG RAPPANG', NULL, NULL),
	(413, '7315', '73', 'KABUPATEN PINRANG', NULL, NULL),
	(414, '7316', '73', 'KABUPATEN ENREKANG', NULL, NULL),
	(415, '7317', '73', 'KABUPATEN LUWU', NULL, NULL),
	(416, '7318', '73', 'KABUPATEN TANA TORAJA', NULL, NULL),
	(417, '7322', '73', 'KABUPATEN LUWU UTARA', NULL, NULL),
	(418, '7325', '73', 'KABUPATEN LUWU TIMUR', NULL, NULL),
	(419, '7326', '73', 'KABUPATEN TORAJA UTARA', NULL, NULL),
	(420, '7371', '73', 'KOTA MAKASSAR', NULL, NULL),
	(421, '7372', '73', 'KOTA PAREPARE', NULL, NULL),
	(422, '7373', '73', 'KOTA PALOPO', NULL, NULL),
	(423, '7401', '74', 'KABUPATEN BUTON', NULL, NULL),
	(424, '7402', '74', 'KABUPATEN MUNA', NULL, NULL),
	(425, '7403', '74', 'KABUPATEN KONAWE', NULL, NULL),
	(426, '7404', '74', 'KABUPATEN KOLAKA', NULL, NULL),
	(427, '7405', '74', 'KABUPATEN KONAWE SELATAN', NULL, NULL),
	(428, '7406', '74', 'KABUPATEN BOMBANA', NULL, NULL),
	(429, '7407', '74', 'KABUPATEN WAKATOBI', NULL, NULL),
	(430, '7408', '74', 'KABUPATEN KOLAKA UTARA', NULL, NULL),
	(431, '7409', '74', 'KABUPATEN BUTON UTARA', NULL, NULL),
	(432, '7410', '74', 'KABUPATEN KONAWE UTARA', NULL, NULL),
	(433, '7411', '74', 'KABUPATEN KOLAKA TIMUR', NULL, NULL),
	(434, '7412', '74', 'KABUPATEN KONAWE KEPULAUAN', NULL, NULL),
	(435, '7413', '74', 'KABUPATEN MUNA BARAT', NULL, NULL),
	(436, '7414', '74', 'KABUPATEN BUTON TENGAH', NULL, NULL),
	(437, '7415', '74', 'KABUPATEN BUTON SELATAN', NULL, NULL),
	(438, '7471', '74', 'KOTA KENDARI', NULL, NULL),
	(439, '7472', '74', 'KOTA BAUBAU', NULL, NULL),
	(440, '7501', '75', 'KABUPATEN BOALEMO', NULL, NULL),
	(441, '7502', '75', 'KABUPATEN GORONTALO', NULL, NULL),
	(442, '7503', '75', 'KABUPATEN POHUWATO', NULL, NULL),
	(443, '7504', '75', 'KABUPATEN BONE BOLANGO', NULL, NULL),
	(444, '7505', '75', 'KABUPATEN GORONTALO UTARA', NULL, NULL),
	(445, '7571', '75', 'KOTA GORONTALO', NULL, NULL),
	(446, '7601', '76', 'KABUPATEN MAJENE', NULL, NULL),
	(447, '7602', '76', 'KABUPATEN POLEWALI MANDAR', NULL, NULL),
	(448, '7603', '76', 'KABUPATEN MAMASA', NULL, NULL),
	(449, '7604', '76', 'KABUPATEN MAMUJU', NULL, NULL),
	(450, '7605', '76', 'KABUPATEN MAMUJU UTARA', NULL, NULL),
	(451, '7606', '76', 'KABUPATEN MAMUJU TENGAH', NULL, NULL),
	(452, '8101', '81', 'KABUPATEN MALUKU TENGGARA BARAT', NULL, NULL),
	(453, '8102', '81', 'KABUPATEN MALUKU TENGGARA', NULL, NULL),
	(454, '8103', '81', 'KABUPATEN MALUKU TENGAH', NULL, NULL),
	(455, '8104', '81', 'KABUPATEN BURU', NULL, NULL),
	(456, '8105', '81', 'KABUPATEN KEPULAUAN ARU', NULL, NULL),
	(457, '8106', '81', 'KABUPATEN SERAM BAGIAN BARAT', NULL, NULL),
	(458, '8107', '81', 'KABUPATEN SERAM BAGIAN TIMUR', NULL, NULL),
	(459, '8108', '81', 'KABUPATEN MALUKU BARAT DAYA', NULL, NULL),
	(460, '8109', '81', 'KABUPATEN BURU SELATAN', NULL, NULL),
	(461, '8171', '81', 'KOTA AMBON', NULL, NULL),
	(462, '8172', '81', 'KOTA TUAL', NULL, NULL),
	(463, '8201', '82', 'KABUPATEN HALMAHERA BARAT', NULL, NULL),
	(464, '8202', '82', 'KABUPATEN HALMAHERA TENGAH', NULL, NULL),
	(465, '8203', '82', 'KABUPATEN KEPULAUAN SULA', NULL, NULL),
	(466, '8204', '82', 'KABUPATEN HALMAHERA SELATAN', NULL, NULL),
	(467, '8205', '82', 'KABUPATEN HALMAHERA UTARA', NULL, NULL),
	(468, '8206', '82', 'KABUPATEN HALMAHERA TIMUR', NULL, NULL),
	(469, '8207', '82', 'KABUPATEN PULAU MOROTAI', NULL, NULL),
	(470, '8208', '82', 'KABUPATEN PULAU TALIABU', NULL, NULL),
	(471, '8271', '82', 'KOTA TERNATE', NULL, NULL),
	(472, '8272', '82', 'KOTA TIDORE KEPULAUAN', NULL, NULL),
	(473, '9101', '91', 'KABUPATEN FAKFAK', NULL, NULL),
	(474, '9102', '91', 'KABUPATEN KAIMANA', NULL, NULL),
	(475, '9103', '91', 'KABUPATEN TELUK WONDAMA', NULL, NULL),
	(476, '9104', '91', 'KABUPATEN TELUK BINTUNI', NULL, NULL),
	(477, '9105', '91', 'KABUPATEN MANOKWARI', NULL, NULL),
	(478, '9106', '91', 'KABUPATEN SORONG SELATAN', NULL, NULL),
	(479, '9107', '91', 'KABUPATEN SORONG', NULL, NULL),
	(480, '9108', '91', 'KABUPATEN RAJA AMPAT', NULL, NULL),
	(481, '9109', '91', 'KABUPATEN TAMBRAUW', NULL, NULL),
	(482, '9110', '91', 'KABUPATEN MAYBRAT', NULL, NULL),
	(483, '9111', '91', 'KABUPATEN MANOKWARI SELATAN', NULL, NULL),
	(484, '9112', '91', 'KABUPATEN PEGUNUNGAN ARFAK', NULL, NULL),
	(485, '9171', '91', 'KOTA SORONG', NULL, NULL),
	(486, '9401', '94', 'KABUPATEN MERAUKE', NULL, NULL),
	(487, '9402', '94', 'KABUPATEN JAYAWIJAYA', NULL, NULL),
	(488, '9403', '94', 'KABUPATEN JAYAPURA', NULL, NULL),
	(489, '9404', '94', 'KABUPATEN NABIRE', NULL, NULL),
	(490, '9408', '94', 'KABUPATEN KEPULAUAN YAPEN', NULL, NULL),
	(491, '9409', '94', 'KABUPATEN BIAK NUMFOR', NULL, NULL),
	(492, '9410', '94', 'KABUPATEN PANIAI', NULL, NULL),
	(493, '9411', '94', 'KABUPATEN PUNCAK JAYA', NULL, NULL),
	(494, '9412', '94', 'KABUPATEN MIMIKA', NULL, NULL),
	(495, '9413', '94', 'KABUPATEN BOVEN DIGOEL', NULL, NULL),
	(496, '9414', '94', 'KABUPATEN MAPPI', NULL, NULL),
	(497, '9415', '94', 'KABUPATEN ASMAT', NULL, NULL),
	(498, '9416', '94', 'KABUPATEN YAHUKIMO', NULL, NULL),
	(499, '9417', '94', 'KABUPATEN PEGUNUNGAN BINTANG', NULL, NULL),
	(500, '9418', '94', 'KABUPATEN TOLIKARA', NULL, NULL),
	(501, '9419', '94', 'KABUPATEN SARMI', NULL, NULL),
	(502, '9420', '94', 'KABUPATEN KEEROM', NULL, NULL),
	(503, '9426', '94', 'KABUPATEN WAROPEN', NULL, NULL),
	(504, '9427', '94', 'KABUPATEN SUPIORI', NULL, NULL),
	(505, '9428', '94', 'KABUPATEN MAMBERAMO RAYA', NULL, NULL),
	(506, '9429', '94', 'KABUPATEN NDUGA', NULL, NULL),
	(507, '9430', '94', 'KABUPATEN LANNY JAYA', NULL, NULL),
	(508, '9431', '94', 'KABUPATEN MAMBERAMO TENGAH', NULL, NULL),
	(509, '9432', '94', 'KABUPATEN YALIMO', NULL, NULL),
	(510, '9433', '94', 'KABUPATEN PUNCAK', NULL, NULL),
	(511, '9434', '94', 'KABUPATEN DOGIYAI', NULL, NULL),
	(512, '9435', '94', 'KABUPATEN INTAN JAYA', NULL, NULL),
	(513, '9436', '94', 'KABUPATEN DEIYAI', NULL, NULL),
	(514, '9471', '94', 'KOTA JAYAPURA', NULL, NULL);
/*!40000 ALTER TABLE `tref_kota` ENABLE KEYS */;

-- membuang struktur untuk table pos.tref_metode_pembayaran
CREATE TABLE IF NOT EXISTS `tref_metode_pembayaran` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_aktif` int(11) NOT NULL,
  `file_gambar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jenis` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tref_metode_pembayaran: ~7 rows (lebih kurang)
DELETE FROM `tref_metode_pembayaran`;
/*!40000 ALTER TABLE `tref_metode_pembayaran` DISABLE KEYS */;
INSERT INTO `tref_metode_pembayaran` (`id`, `nama`, `keterangan`, `is_aktif`, `file_gambar`, `jenis`, `created_at`, `updated_at`) VALUES
	(1, 'Tunai', 'pembayaran secara tunai', 1, '', '', NULL, NULL),
	(2, 'Transfer', 'pembayaran secara transfer', 1, '', '', NULL, NULL),
	(3, 'Kartu Kedit', 'pembayaran menggunakan kartu kredit', 1, '', '', NULL, NULL),
	(4, 'Giro', 'pembayaran menggunakan giro', 1, '', '', NULL, NULL),
	(5, 'Cek', 'pembayaran menggunakan cek', 1, '', '', NULL, NULL),
	(6, 'Debit BCA', 'Pembayaran menggunakan Kartu Debit BCA', 1, '', 'non_tunai', '2016-10-13 22:34:03', '2016-10-13 22:34:03'),
	(7, 'Debit ATM Bersama', 'Pembayaran menggunakan Kartu Debit ATM Bersama', 1, '', 'non_tunai', '2016-10-13 22:34:39', '2016-10-13 22:34:39');
/*!40000 ALTER TABLE `tref_metode_pembayaran` ENABLE KEYS */;

-- membuang struktur untuk table pos.tref_metode_pengiriman
CREATE TABLE IF NOT EXISTS `tref_metode_pengiriman` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tarif_dasar` bigint(20) DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_aktif` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_gambar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tref_metode_pengiriman: ~2 rows (lebih kurang)
DELETE FROM `tref_metode_pengiriman`;
/*!40000 ALTER TABLE `tref_metode_pengiriman` DISABLE KEYS */;
INSERT INTO `tref_metode_pengiriman` (`id`, `nama`, `tarif_dasar`, `keterangan`, `is_aktif`, `file_gambar`, `created_at`, `updated_at`) VALUES
	(1, 'reguler', 0, '', '1', '', '2016-10-15 00:09:52', '2016-10-15 00:09:52'),
	(2, 'kilat', 0, '', '1', '', '2016-10-15 00:10:03', '2016-10-15 00:10:03');
/*!40000 ALTER TABLE `tref_metode_pengiriman` ENABLE KEYS */;

-- membuang struktur untuk table pos.tref_nama_transaksi
CREATE TABLE IF NOT EXISTS `tref_nama_transaksi` (
  `id` bigint(20) NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `transaksi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tref_nama_transaksi: ~13 rows (lebih kurang)
DELETE FROM `tref_nama_transaksi`;
/*!40000 ALTER TABLE `tref_nama_transaksi` DISABLE KEYS */;
INSERT INTO `tref_nama_transaksi` (`id`, `nama`, `transaksi`, `created_at`, `updated_at`) VALUES
	(1, 'Total Harga Beli Nett', 'nota_beli', NULL, NULL),
	(2, 'Diskon Pembelian', 'nota_beli', NULL, NULL),
	(3, 'Total Harga Jual Nett', 'nota_jual', NULL, NULL),
	(4, 'Penjualan (HPP)', 'nota_jual', NULL, NULL),
	(5, 'Voucher Jual', 'nota_jual', NULL, NULL),
	(6, 'Total Retur Beli', 'retur_beli', NULL, NULL),
	(7, 'Potongan Retur Beli', 'retur_beli', NULL, NULL),
	(8, 'Total Retur Jual', 'retur_jual', NULL, NULL),
	(9, 'Potongan Retur Jual', 'retur_jual', NULL, NULL),
	(10, 'Total Jasa', 'jasa_servis', NULL, NULL),
	(11, 'Total Pembayaran Supplier', 'pembayaran_supplier', NULL, NULL),
	(12, 'Total Pembayaran Pelanggan', 'pembayaran_pelanggan', NULL, NULL),
	(13, 'Biaya', 'transaksi_jasa', NULL, NULL);
/*!40000 ALTER TABLE `tref_nama_transaksi` ENABLE KEYS */;

-- membuang struktur untuk table pos.tref_nomor_invoice
CREATE TABLE IF NOT EXISTS `tref_nomor_invoice` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `format` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tref_nomor_invoice: ~12 rows (lebih kurang)
DELETE FROM `tref_nomor_invoice`;
/*!40000 ALTER TABLE `tref_nomor_invoice` DISABLE KEYS */;
INSERT INTO `tref_nomor_invoice` (`id`, `nama`, `format`, `created_at`, `updated_at`) VALUES
	(1, 'Purchase Order', 'PO-YYYYMM-#####', NULL, NULL),
	(2, 'Surat Jalan Masuk', 'SJM-YYYYMM-#####', NULL, NULL),
	(3, 'Nota Beli', 'NB-YYYYMM-#####', NULL, NULL),
	(4, 'Retur Beli', 'RB-YYYYMM-#####', NULL, NULL),
	(5, 'Sales Order', 'SO-YYYYMM-#####', NULL, NULL),
	(6, 'Retur Jual', 'RJ-YYYYMM-#####', NULL, NULL),
	(7, 'Surat Jalan Keluar', 'SJK-YYYYMM-#####', NULL, NULL),
	(8, 'Nota Jual', 'NJ-YYYYMM-#####', NULL, NULL),
	(9, 'Service Order', 'SV-YYYYMM-#####', NULL, NULL),
	(10, 'Mutasi Stok', 'MTS/DD/MM/YY/####', NULL, NULL),
	(11, 'Stok Opname', 'STO/DD/MM/YY/####', NULL, NULL),
	(12, '', 'TJ-YYYYMM-#####', NULL, NULL);
/*!40000 ALTER TABLE `tref_nomor_invoice` ENABLE KEYS */;

-- membuang struktur untuk table pos.tref_pengaturan_po
CREATE TABLE IF NOT EXISTS `tref_pengaturan_po` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `alamat_pengiriman` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `syarat_ketentuan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ppn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tref_pengaturan_po: ~0 rows (lebih kurang)
DELETE FROM `tref_pengaturan_po`;
/*!40000 ALTER TABLE `tref_pengaturan_po` DISABLE KEYS */;
INSERT INTO `tref_pengaturan_po` (`id`, `alamat_pengiriman`, `syarat_ketentuan`, `ppn`, `created_at`, `updated_at`) VALUES
	(1, 'Galerindo Teknologi\r\nHi Tech Mall Lt. Dasar Blok.D No.17-19\r\nJl. Kusuma Bangsa No.116-118 Surabaya\r\nT. 031 5477085 | 031 5348998\r\nUp. Bpk. Gondo/ Ibu Vera', '', '', '2016-11-21 07:39:28', '2016-11-21 07:39:28');
/*!40000 ALTER TABLE `tref_pengaturan_po` ENABLE KEYS */;

-- membuang struktur untuk table pos.tref_pengaturan_so
CREATE TABLE IF NOT EXISTS `tref_pengaturan_so` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `syarat_ketentuan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ppn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tref_pengaturan_so: ~0 rows (lebih kurang)
DELETE FROM `tref_pengaturan_so`;
/*!40000 ALTER TABLE `tref_pengaturan_so` DISABLE KEYS */;
/*!40000 ALTER TABLE `tref_pengaturan_so` ENABLE KEYS */;

-- membuang struktur untuk table pos.tref_provinsi
CREATE TABLE IF NOT EXISTS `tref_provinsi` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tref_provinsi: ~34 rows (lebih kurang)
DELETE FROM `tref_provinsi`;
/*!40000 ALTER TABLE `tref_provinsi` DISABLE KEYS */;
INSERT INTO `tref_provinsi` (`id`, `kode`, `nama`, `created_at`, `updated_at`) VALUES
	(1, '11', 'ACEH', NULL, NULL),
	(2, '51', 'BALI', NULL, NULL),
	(3, '36', 'BANTEN', NULL, NULL),
	(4, '17', 'BENGKULU', NULL, NULL),
	(5, '34', 'DI YOGYAKARTA', NULL, NULL),
	(6, '31', 'DKI JAKARTA', NULL, NULL),
	(7, '75', 'GORONTALO', NULL, NULL),
	(8, '15', 'JAMBI', NULL, NULL),
	(9, '32', 'JAWA BARAT', NULL, NULL),
	(10, '33', 'JAWA TENGAH', NULL, NULL),
	(11, '35', 'JAWA TIMUR', NULL, NULL),
	(12, '61', 'KALIMANTAN BARAT', NULL, NULL),
	(13, '63', 'KALIMANTAN SELATAN', NULL, NULL),
	(14, '62', 'KALIMANTAN TENGAH', NULL, NULL),
	(15, '64', 'KALIMANTAN TIMUR', NULL, NULL),
	(16, '65', 'KALIMANTAN UTARA', NULL, NULL),
	(17, '19', 'KEPULAUAN BANGKA BELITUNG', NULL, NULL),
	(18, '21', 'KEPULAUAN RIAU', NULL, NULL),
	(19, '18', 'LAMPUNG', NULL, NULL),
	(20, '81', 'MALUKU', NULL, NULL),
	(21, '82', 'MALUKU UTARA', NULL, NULL),
	(22, '52', 'NUSA TENGGARA BARAT', NULL, NULL),
	(23, '53', 'NUSA TENGGARA TIMUR', NULL, NULL),
	(24, '94', 'PAPUA', NULL, NULL),
	(25, '91', 'PAPUA BARAT', NULL, NULL),
	(26, '14', 'RIAU', NULL, NULL),
	(27, '76', 'SULAWESI BARAT', NULL, NULL),
	(28, '73', 'SULAWESI SELATAN', NULL, NULL),
	(29, '72', 'SULAWESI TENGAH', NULL, NULL),
	(30, '74', 'SULAWESI TENGGARA', NULL, NULL),
	(31, '71', 'SULAWESI UTARA', NULL, NULL),
	(32, '13', 'SUMATERA BARAT', NULL, NULL),
	(33, '16', 'SUMATERA SELATAN', NULL, NULL),
	(34, '12', 'SUMATERA UTARA', NULL, NULL);
/*!40000 ALTER TABLE `tref_provinsi` ENABLE KEYS */;

-- membuang struktur untuk table pos.tref_sumber_data
CREATE TABLE IF NOT EXISTS `tref_sumber_data` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.tref_sumber_data: ~4 rows (lebih kurang)
DELETE FROM `tref_sumber_data`;
/*!40000 ALTER TABLE `tref_sumber_data` DISABLE KEYS */;
INSERT INTO `tref_sumber_data` (`id`, `nama`, `created_at`, `updated_at`) VALUES
	(1, 'Surat Jalan Masuk', NULL, NULL),
	(2, 'Surat Jalan Keluar', NULL, NULL),
	(3, 'Retur Jual', NULL, NULL),
	(4, 'Retur Beli', NULL, NULL);
/*!40000 ALTER TABLE `tref_sumber_data` ENABLE KEYS */;

-- membuang struktur untuk table pos.tref_syarat_ketentuan
CREATE TABLE IF NOT EXISTS `tref_syarat_ketentuan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `perihal` varchar(50) DEFAULT NULL,
  `konten` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel pos.tref_syarat_ketentuan: ~2 rows (lebih kurang)
DELETE FROM `tref_syarat_ketentuan`;
/*!40000 ALTER TABLE `tref_syarat_ketentuan` DISABLE KEYS */;
INSERT INTO `tref_syarat_ketentuan` (`id`, `perihal`, `konten`, `created_at`, `updated_at`) VALUES
	(1, 'Nota Beli', 'fdgfdgfdg', '2017-01-13 16:20:27', '2017-01-13 16:20:27'),
	(2, 'Nota Jual', NULL, '2017-01-13 16:21:44', '2017-01-13 16:21:47');
/*!40000 ALTER TABLE `tref_syarat_ketentuan` ENABLE KEYS */;

-- membuang struktur untuk table pos.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kota_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kode_pos` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hak_akses_id` int(11) NOT NULL,
  `tanggal_register` datetime NOT NULL,
  `tanggal_unregister` datetime NOT NULL,
  `is_aktif` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel pos.users: ~3 rows (lebih kurang)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `password`, `alamat`, `kota_id`, `kode_pos`, `telp`, `hp`, `hak_akses_id`, `tanggal_register`, `tanggal_unregister`, `is_aktif`, `remember_token`, `deleted_at`, `created_at`, `updated_at`) VALUES
	(1, 'admin', 'admin@pos.com', '$2y$10$V.3AQnu1gsfyEnEObtKQ0u6j4qM3jIVgUu/bIe6jNRra5iOrlZE/G', '', '264', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'wUC7kHqBfX30GPg6BBGBJ2FsfqqObseDv9fTb8vuXqVSBYr1jf917sKgtGvK', NULL, NULL, '2017-02-06 13:03:38'),
	(2, 'Fendi Septiawan', 'fendi_septiawan0709@yahoo.co.id', '$2y$10$gWPWL7WXBJH9m/97Lg/Hb.6vPRutnrS/d9Vtl0Tc4utwLYFSzkFli', 'RT 09,\r\nRW 04,\r\nDesa Ngrami,\r\nKecamatan Sukomoro,\r\nKabupaten Nganjuk,\r\nJawa Timur,\r\nIndonesia.', '245', '64481', '', '', 2, '2017-01-06 07:51:45', '0000-00-00 00:00:00', 1, NULL, NULL, NULL, NULL),
	(3, 'Teknisi Indonusa', 'teknisi@pos.com', '$2y$10$hLd56oIg7nwFh7gyZGq/D.Ygaz2xNUe4O9ZGTGdjDBakDUn.hs6.a', 'RT 09,\r\nRW 04,\r\nDesa Ngrami,\r\nKecamatan Sukomoro,\r\nKabupaten Nganjuk,\r\nJawa Timur,\r\nIndonesia.', '245', '64481', '(6231)-3423213', '(+62)857-0782-4788', 3, '2017-01-16 10:08:30', '0000-00-00 00:00:00', 1, 'UpyKlYeOGPAQhssDjz2wz1mjENHmmGMTsbl2lqEkQMzfgJTDpe2idF6kqNiu', NULL, NULL, '2017-02-10 22:03:57');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
