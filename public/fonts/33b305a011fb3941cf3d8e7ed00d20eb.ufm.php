<?php return array (
  'codeToName' => 
  array (
  ),
  'isUnicode' => true,
  'EncodingScheme' => 'FontSpecific',
  'FontName' => 'Calibri',
  'FullName' => 'Calibri Bold',
  'Version' => 'Version 1.00',
  'PostScriptName' => 'Calibri-Bold',
  'Weight' => 'Bold',
  'ItalicAngle' => '0',
  'IsFixedPitch' => 'false',
  'UnderlineThickness' => '91',
  'UnderlinePosition' => '-87',
  'FontHeightOffset' => '221',
  'Ascender' => '750',
  'Descender' => '-250',
  'FontBBox' => 
  array (
    0 => '-493',
    1 => '-194',
    2 => '1239',
    3 => '952',
  ),
  'StartCharMetrics' => '1121',
  'C' => 
  array (
    0 => 0.0,
    13 => 0.0,
    32 => 226.0,
    33 => 326.0,
    34 => 438.0,
    35 => 498.0,
    36 => 507.0,
    37 => 729.0,
    38 => 705.0,
    39 => 233.0,
    40 => 312.0,
    41 => 312.0,
    42 => 498.0,
    43 => 498.0,
    44 => 258.0,
    45 => 306.0,
    46 => 267.0,
    47 => 430.0,
    48 => 507.0,
    49 => 507.0,
    50 => 507.0,
    51 => 507.0,
    52 => 507.0,
    53 => 507.0,
    54 => 507.0,
    55 => 507.0,
    56 => 507.0,
    57 => 507.0,
    58 => 276.0,
    59 => 276.0,
    60 => 498.0,
    61 => 498.0,
    62 => 498.0,
    63 => 463.0,
    64 => 898.0,
    65 => 606.0,
    66 => 561.0,
    67 => 529.0,
    68 => 630.0,
    69 => 488.0,
    70 => 459.0,
    71 => 637.0,
    72 => 631.0,
    73 => 267.0,
    74 => 331.0,
    75 => 547.0,
    76 => 423.0,
    77 => 874.0,
    78 => 659.0,
    79 => 676.0,
    80 => 532.0,
    81 => 686.0,
    82 => 563.0,
    83 => 473.0,
    84 => 495.0,
    85 => 653.0,
    86 => 591.0,
    87 => 906.0,
    88 => 551.0,
    89 => 520.0,
    90 => 478.0,
    91 => 325.0,
    92 => 430.0,
    93 => 325.0,
    94 => 498.0,
    95 => 498.0,
    96 => 300.0,
    97 => 494.0,
    98 => 537.0,
    99 => 418.0,
    100 => 537.0,
    101 => 503.0,
    102 => 316.0,
    103 => 474.0,
    104 => 537.0,
    105 => 246.0,
    106 => 255.0,
    107 => 480.0,
    108 => 246.0,
    109 => 813.0,
    110 => 537.0,
    111 => 538.0,
    112 => 537.0,
    113 => 537.0,
    114 => 355.0,
    115 => 399.0,
    116 => 347.0,
    117 => 537.0,
    118 => 473.0,
    119 => 745.0,
    120 => 459.0,
    121 => 474.0,
    122 => 397.0,
    123 => 344.0,
    124 => 475.0,
    125 => 344.0,
    126 => 498.0,
    160 => 226.0,
    161 => 326.0,
    162 => 498.0,
    163 => 507.0,
    164 => 498.0,
    165 => 507.0,
    166 => 498.0,
    167 => 498.0,
    168 => 415.0,
    169 => 834.0,
    170 => 416.0,
    171 => 539.0,
    172 => 498.0,
    173 => 306.0,
    174 => 507.0,
    175 => 390.0,
    176 => 342.0,
    177 => 498.0,
    178 => 338.0,
    179 => 336.0,
    180 => 301.0,
    181 => 563.0,
    182 => 598.0,
    183 => 268.0,
    184 => 303.0,
    185 => 252.0,
    186 => 435.0,
    187 => 539.0,
    188 => 658.0,
    189 => 691.0,
    190 => 702.0,
    191 => 463.0,
    192 => 606.0,
    193 => 606.0,
    194 => 606.0,
    195 => 606.0,
    196 => 606.0,
    197 => 606.0,
    198 => 775.0,
    199 => 529.0,
    200 => 488.0,
    201 => 488.0,
    202 => 488.0,
    203 => 488.0,
    204 => 267.0,
    205 => 267.0,
    206 => 267.0,
    207 => 267.0,
    208 => 639.0,
    209 => 659.0,
    210 => 676.0,
    211 => 676.0,
    212 => 676.0,
    213 => 676.0,
    214 => 676.0,
    215 => 498.0,
    216 => 681.0,
    217 => 653.0,
    218 => 653.0,
    219 => 653.0,
    220 => 653.0,
    221 => 520.0,
    222 => 532.0,
    223 => 555.0,
    224 => 494.0,
    225 => 494.0,
    226 => 494.0,
    227 => 494.0,
    228 => 494.0,
    229 => 494.0,
    230 => 775.0,
    231 => 418.0,
    232 => 503.0,
    233 => 503.0,
    234 => 503.0,
    235 => 503.0,
    236 => 246.0,
    237 => 246.0,
    238 => 246.0,
    239 => 246.0,
    240 => 537.0,
    241 => 537.0,
    242 => 538.0,
    243 => 538.0,
    244 => 538.0,
    245 => 538.0,
    246 => 538.0,
    247 => 498.0,
    248 => 544.0,
    249 => 537.0,
    250 => 537.0,
    251 => 537.0,
    252 => 537.0,
    253 => 474.0,
    254 => 537.0,
    255 => 474.0,
    256 => 606.0,
    257 => 494.0,
    258 => 606.0,
    259 => 494.0,
    260 => 606.0,
    261 => 494.0,
    262 => 529.0,
    263 => 418.0,
    264 => 529.0,
    265 => 418.0,
    266 => 529.0,
    267 => 418.0,
    268 => 529.0,
    269 => 418.0,
    270 => 630.0,
    271 => 597.0,
    272 => 639.0,
    273 => 569.0,
    274 => 488.0,
    275 => 503.0,
    276 => 488.0,
    277 => 503.0,
    278 => 488.0,
    279 => 503.0,
    280 => 488.0,
    281 => 503.0,
    282 => 488.0,
    283 => 503.0,
    284 => 637.0,
    285 => 474.0,
    286 => 637.0,
    287 => 474.0,
    288 => 637.0,
    289 => 474.0,
    290 => 637.0,
    291 => 474.0,
    292 => 631.0,
    293 => 537.0,
    294 => 658.0,
    295 => 547.0,
    296 => 267.0,
    297 => 246.0,
    298 => 267.0,
    299 => 246.0,
    300 => 267.0,
    301 => 246.0,
    302 => 267.0,
    303 => 246.0,
    304 => 267.0,
    305 => 246.0,
    306 => 598.0,
    307 => 501.0,
    308 => 331.0,
    309 => 255.0,
    310 => 547.0,
    311 => 480.0,
    312 => 480.0,
    313 => 423.0,
    314 => 246.0,
    315 => 423.0,
    316 => 246.0,
    317 => 430.0,
    318 => 306.0,
    319 => 562.0,
    320 => 422.0,
    321 => 433.0,
    322 => 264.0,
    323 => 659.0,
    324 => 537.0,
    325 => 659.0,
    326 => 537.0,
    327 => 659.0,
    328 => 537.0,
    329 => 622.0,
    330 => 641.0,
    331 => 537.0,
    332 => 676.0,
    333 => 538.0,
    334 => 676.0,
    335 => 538.0,
    336 => 676.0,
    337 => 538.0,
    338 => 874.0,
    339 => 843.0,
    340 => 563.0,
    341 => 355.0,
    342 => 563.0,
    343 => 355.0,
    344 => 563.0,
    345 => 355.0,
    346 => 473.0,
    347 => 399.0,
    348 => 473.0,
    349 => 399.0,
    350 => 473.0,
    351 => 399.0,
    352 => 473.0,
    353 => 399.0,
    354 => 495.0,
    355 => 347.0,
    356 => 495.0,
    357 => 363.0,
    358 => 495.0,
    359 => 354.0,
    360 => 653.0,
    361 => 537.0,
    362 => 653.0,
    363 => 537.0,
    364 => 653.0,
    365 => 537.0,
    366 => 653.0,
    367 => 537.0,
    368 => 653.0,
    369 => 537.0,
    370 => 653.0,
    371 => 537.0,
    372 => 906.0,
    373 => 745.0,
    374 => 520.0,
    375 => 474.0,
    376 => 520.0,
    377 => 478.0,
    378 => 397.0,
    379 => 478.0,
    380 => 397.0,
    381 => 478.0,
    382 => 397.0,
    383 => 258.0,
    402 => 498.0,
    506 => 606.0,
    507 => 494.0,
    508 => 775.0,
    509 => 775.0,
    510 => 681.0,
    511 => 544.0,
    536 => 473.0,
    537 => 399.0,
    538 => 495.0,
    539 => 347.0,
    710 => 401.0,
    711 => 401.0,
    713 => 390.0,
    728 => 385.0,
    729 => 235.0,
    730 => 328.0,
    731 => 315.0,
    732 => 444.0,
    733 => 486.0,
    768 => 0.0,
    769 => 0.0,
    770 => 0.0,
    771 => 0.0,
    772 => 0.0,
    774 => 0.0,
    775 => 0.0,
    776 => 0.0,
    778 => 0.0,
    779 => 0.0,
    780 => 0.0,
    786 => 271.0,
    789 => 248.0,
    806 => 251.0,
    807 => 0.0,
    808 => 0.0,
    836 => 0.0,
    884 => 258.0,
    885 => 258.0,
    894 => 276.0,
    900 => 317.0,
    901 => 494.0,
    902 => 606.0,
    903 => 268.0,
    904 => 488.0,
    905 => 631.0,
    906 => 267.0,
    908 => 676.0,
    910 => 520.0,
    911 => 681.0,
    912 => 286.0,
    913 => 606.0,
    914 => 561.0,
    915 => 410.0,
    916 => 578.0,
    917 => 488.0,
    918 => 478.0,
    919 => 631.0,
    920 => 676.0,
    921 => 267.0,
    922 => 547.0,
    923 => 594.0,
    924 => 874.0,
    925 => 659.0,
    926 => 492.0,
    927 => 676.0,
    928 => 631.0,
    929 => 532.0,
    931 => 470.0,
    932 => 495.0,
    933 => 520.0,
    934 => 801.0,
    935 => 551.0,
    936 => 808.0,
    937 => 681.0,
    938 => 267.0,
    939 => 520.0,
    940 => 591.0,
    941 => 458.0,
    942 => 544.0,
    943 => 286.0,
    944 => 548.0,
    945 => 591.0,
    946 => 545.0,
    947 => 467.0,
    948 => 533.0,
    949 => 458.0,
    950 => 364.0,
    951 => 544.0,
    952 => 545.0,
    953 => 286.0,
    954 => 480.0,
    955 => 486.0,
    956 => 563.0,
    957 => 471.0,
    958 => 385.0,
    959 => 538.0,
    960 => 579.0,
    961 => 530.0,
    962 => 407.0,
    963 => 544.0,
    964 => 393.0,
    965 => 548.0,
    966 => 699.0,
    967 => 451.0,
    968 => 738.0,
    969 => 718.0,
    970 => 286.0,
    971 => 548.0,
    972 => 538.0,
    973 => 548.0,
    974 => 718.0,
    1024 => 488.0,
    1025 => 488.0,
    1026 => 636.0,
    1027 => 432.0,
    1028 => 539.0,
    1029 => 473.0,
    1030 => 267.0,
    1031 => 267.0,
    1032 => 331.0,
    1033 => 896.0,
    1034 => 896.0,
    1035 => 632.0,
    1036 => 563.0,
    1037 => 652.0,
    1038 => 547.0,
    1039 => 628.0,
    1040 => 606.0,
    1041 => 556.0,
    1042 => 561.0,
    1043 => 432.0,
    1044 => 673.0,
    1045 => 488.0,
    1046 => 833.0,
    1047 => 482.0,
    1048 => 652.0,
    1049 => 652.0,
    1050 => 563.0,
    1051 => 629.0,
    1052 => 874.0,
    1053 => 631.0,
    1054 => 676.0,
    1055 => 630.0,
    1056 => 532.0,
    1057 => 529.0,
    1058 => 495.0,
    1059 => 547.0,
    1060 => 724.0,
    1061 => 551.0,
    1062 => 658.0,
    1063 => 569.0,
    1064 => 897.0,
    1065 => 927.0,
    1066 => 639.0,
    1067 => 798.0,
    1068 => 553.0,
    1069 => 539.0,
    1070 => 914.0,
    1071 => 574.0,
    1072 => 494.0,
    1073 => 532.0,
    1074 => 495.0,
    1075 => 354.0,
    1076 => 582.0,
    1077 => 503.0,
    1078 => 737.0,
    1079 => 427.0,
    1080 => 557.0,
    1081 => 557.0,
    1082 => 495.0,
    1083 => 526.0,
    1084 => 713.0,
    1085 => 545.0,
    1086 => 538.0,
    1087 => 534.0,
    1088 => 537.0,
    1089 => 418.0,
    1090 => 390.0,
    1091 => 474.0,
    1092 => 675.0,
    1093 => 459.0,
    1094 => 562.0,
    1095 => 488.0,
    1096 => 763.0,
    1097 => 789.0,
    1098 => 558.0,
    1099 => 712.0,
    1100 => 495.0,
    1101 => 437.0,
    1102 => 753.0,
    1103 => 498.0,
    1104 => 503.0,
    1105 => 503.0,
    1106 => 553.0,
    1107 => 354.0,
    1108 => 438.0,
    1109 => 399.0,
    1110 => 246.0,
    1111 => 246.0,
    1112 => 255.0,
    1113 => 776.0,
    1114 => 794.0,
    1115 => 547.0,
    1116 => 495.0,
    1117 => 557.0,
    1118 => 474.0,
    1119 => 537.0,
    1122 => 638.0,
    1123 => 532.0,
    1138 => 676.0,
    1139 => 536.0,
    1140 => 612.0,
    1141 => 497.0,
    1168 => 434.0,
    1169 => 367.0,
    7808 => 906.0,
    7809 => 745.0,
    7810 => 906.0,
    7811 => 745.0,
    7812 => 906.0,
    7813 => 745.0,
    7922 => 520.0,
    7923 => 474.0,
    8208 => 306.0,
    8211 => 498.0,
    8212 => 905.0,
    8213 => 905.0,
    8216 => 258.0,
    8217 => 258.0,
    8218 => 258.0,
    8220 => 435.0,
    8221 => 435.0,
    8222 => 435.0,
    8224 => 498.0,
    8225 => 498.0,
    8226 => 498.0,
    8230 => 711.0,
    8240 => 1062.0,
    8249 => 344.0,
    8250 => 344.0,
    8253 => 505.0,
    8260 => 355.0,
    8304 => 398.0,
    8308 => 366.0,
    8309 => 337.0,
    8310 => 361.0,
    8311 => 327.0,
    8312 => 366.0,
    8313 => 361.0,
    8314 => 355.0,
    8315 => 352.0,
    8316 => 347.0,
    8317 => 222.0,
    8318 => 222.0,
    8319 => 372.0,
    8320 => 398.0,
    8321 => 252.0,
    8322 => 338.0,
    8323 => 336.0,
    8324 => 366.0,
    8325 => 337.0,
    8326 => 361.0,
    8327 => 327.0,
    8328 => 366.0,
    8329 => 361.0,
    8330 => 355.0,
    8331 => 352.0,
    8332 => 347.0,
    8333 => 222.0,
    8334 => 222.0,
    8364 => 507.0,
    8453 => 776.0,
    8467 => 528.0,
    8470 => 1046.0,
    8471 => 834.0,
    8480 => 731.0,
    8482 => 720.0,
    8486 => 681.0,
    8494 => 739.0,
    8531 => 697.0,
    8532 => 747.0,
    8533 => 697.0,
    8534 => 747.0,
    8535 => 740.0,
    8536 => 753.0,
    8537 => 669.0,
    8538 => 711.0,
    8539 => 696.0,
    8540 => 738.0,
    8541 => 738.0,
    8542 => 680.0,
    8543 => 396.0,
    8592 => 905.0,
    8593 => 905.0,
    8594 => 905.0,
    8595 => 905.0,
    8596 => 1310.0,
    8597 => 769.0,
    8598 => 839.0,
    8599 => 839.0,
    8600 => 839.0,
    8601 => 839.0,
    8706 => 537.0,
    8710 => 578.0,
    8719 => 782.0,
    8721 => 550.0,
    8722 => 498.0,
    8729 => 268.0,
    8730 => 498.0,
    8734 => 838.0,
    8747 => 375.0,
    8776 => 498.0,
    8800 => 498.0,
    8804 => 498.0,
    8805 => 498.0,
    9674 => 534.0,
    64256 => 601.0,
    64257 => 554.0,
    64258 => 554.0,
    64259 => 839.0,
    64260 => 839.0,
  ),
  'CIDtoGID_Compressed' => true,
  'CIDtoGID' => 'eJzt2+eXldUVx/H92/vQGRgGEKV3hjZ0GHqvQxl6B6WoRAQckDp0e6GJSmxJ7DEaowlg772b5lv/ADXGxBJju2vWuBZm1hgcR8C1vp+1nmc/9559zt7nuXc999U1WXm83JFjRUyI6+No3J36x6G4LrbF9tiTBsfMKIo5sTLei/fjg/hHfBj/jI/iX/Hv+Dhmx6w0Kg1Po2NS3GjJ6lmOnW5Nrb11sC7WzQbYIBtio2y0Fdhsm2vzbJEts3Ntra2zTbbZtseOWBM742Bs1vty1VW2Gqu5clWoeVqoFVqp1bpQ67VVl+tKXaV9ukGH9ZSe1gt6Ua/FrlgVu+Pa49rh/9t/QdwT98btcX+sjv1eLW7RW3FuGp/Z/689K25LY+Kz+I8+StPiQBR7p/hUb8eK1CV1Sj1jslWxqlbNqlsNq211rZE1s+bWwlpZd+thPa23NbFxNtEm2WSbYoVpqM2wFfYLO8/Ot2Kbo0MKJVVRVVVTTWWpoVqopVqpjRZpsc7UEjXTJm3Tdu3QTu1Kw3SJjuioHtIjellX6FWrqepWSzWsjmpbfdWzBsqx09TAGqq+naEm1lhNrbXaWhu1s7Zqb+3UwVqqteVqqnXUNOuk6dZZM6yr5lueFlgvnWV9tNT6apnla7n109nWX+fYQJ1ng3W+VtlQrbHhKrJhusBGap2N0Fobow02XpttrDZqi01QsU3Vbpumi2y6LrZZuszm62pbqL22QHtssfbbmTpgS3TQztI1drZusuW60c7RzbZSD9sqPWqr9Zit0eN2gZ6wIj1pG/ScbdUrts126HXbpTdtp97QL4/rI76vEr4np5Is1bJs1bGZuvQHz12qa0vuNyqdPtUXmcdu1vfleI639Ubexd1bVErND/WxPvcqnp1Zu6Hneq1MbOyt9fUxNbuVnMv/pfg2L887e9eK9eE3V2zeca9/q99W5r3fHHN9g9/kPfwW7+v9fVDm9ajMMcELfZd3957ey3t7H+/n+T7AB/oQH+rDfLiP8JE+2sdkMsf6OB/vE32ST/YpPtgLfK1v8mLf4dd4ka/z9b7BN/pm3+Jbfbvv9ov8Yr/EL/XL/Uq/yvf41b7X9/lBv86v90O+0w/4Fb7fj+vp9CPvy71+n//N7/Y/+zP+R/+TH/aH/TH/ux/1B/15f8Vv9zv8Tr/L7/Hf+e/9fv+DP+BH/CF/xB/1x/1Jf8qf9mf9OX/RX/KX/VV/zV/3N/xNf8vf9r/4X6N2ZEXdyI4GcVqcEY2jSTSNVtEm2kX7yI1O0SW6RrfoEb2jT/SN/OgfA2JgDIrBMSSGxfBoFKfHiKgXQyMvmkeLaBlto0OMjNbRLHpGv5I9vBOjvnePT0RHfyF6Vej+/DZyKjIPAAAAAAAAAAAAAAAAAAAAAAAAQCW4UM/Yej1rG/X8yW7lJ7NFL53sFvDzFEUl53WxPjaWXM2L+bEkExfF4liaiXtjX2wpzZ1bdn4aUGbFZbG8JBZ+Jy+3khv/ttonpfHz+G98EV/GV/F1sqTkKVLSu6lKqpqqpeqpRqqZaqXaKSvVSXVTdqqXclL9H1H3zsrp/8SJw+WPpYKSjCPxqzKzbs0cpf+jTVP/Z9aEyuotdU7dU9fUI/VKfVJe6pu6pd6pX8pPHSurQrmVZ6U5aXaamxalxWl+mpcWpIU/dc0TLU0qjYVlRqZnjhlpyLHvRXGaUjo6sdwVJ1e4l5E/MH9EaRybxlW0JgAAp4I082R3AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADAqU2dlaexylfBye4EAAAAJ8I3pZb9xg==',
  '_version_' => 6,
);