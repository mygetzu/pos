<?php

namespace App;

use App\Models\Pelanggan\Pelanggan;
use App\Models\TransaksiPenjualan\ServiceOrder;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'password_confirm'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static $validation_rules = [
        'email'                 => 'required|email|max:255',
        'password'              => 'required',
        'password_confirm'      => 'required|same:password',
    ];

    public function pelanggan() {
        return $this->hasOne(Pelanggan::class, 'user_id');
    }

    public function teknisi(){
        return $this->hasMany(ServiceOrder::class, 'teknisi_id');
    }

    public static function findByEmail($email) {
        $user = self::where('email', $email)->first();
        if (!$user) return false;

        return $user;
    }

    public static function findById($id) {
        $user = self::where('id', $id)->first();
        if (!$user) return false;

        return $user;
    }

    public static function isAktif() {
        $user = self::where('is_aktif', 1)->first();

        if (!$user) return false;
        return $user;
    }
}
