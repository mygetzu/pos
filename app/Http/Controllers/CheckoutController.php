<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Gloudemans\Shoppingcart\Facades\Cart;
use Auth;
use Response;
use PDF;
use Hashids;
use stdClass;
use Illuminate\Pagination\Paginator;
use Input;
use Session;
use App\Models\Produk\Produk;
use App\Models\Produk\Hadiah;
use App\Models\Pelanggan\Pelanggan;
use App\Models\Referensi\Provinsi;
use App\Models\Referensi\Kota;
use DateTime;
use App\Models\Temporary\TempAlamatPengiriman;
use App\Models\Temporary\TempPembayaran;
use App\Models\Referensi\MetodePengiriman;
use App\Models\Referensi\MetodePembayaran;
use App\Models\Referensi\Bank;
use App\Models\Perusahaan;
use App\Models\PaketProduk\Paket;
use App\Models\Voucher;
use App\Models\Notifikasi;
use Mail;
use App\Models\TransaksiPenjualan\SOHeader;
use App\Models\TransaksiPenjualan\SODetail;
use App\Models\TransaksiPenjualan\SOVoucher;
use App\Models\TransaksiPenjualan\SOAlamatPengiriman;
use App\Models\TransaksiPenjualan\SOPembayaran;
use App\Models\TransaksiPenjualan\BayarPenjualan;

class CheckoutController extends Controller
{
    public function data_pengiriman(Request $request)
    {
        $lanjut = Input::get('lanjut');
        //cek apakah customer sudah login
        if(!Auth::check() && empty($lanjut)){
            return view('pages.checkout.login_or_checkout');
        }
        elseif(empty($lanjut)){
            //cek apakah dia pelanggan
            if(Auth::user()->hak_akses_id != 2){
                $request->session()->flash('message', 'Anda bukan pelanggan, login sebagai pelanggan terlebih dahulu');
                return redirect('/cart');
            }
        }

        //cek apakah ada produk di keranjang belanja
        $cart_content = Cart::content(1);
        if(empty($cart_content))
        {
            $request->session()->flash('message', 'Keranjang belanja kosong');
            return redirect('/cart');
        }

        if($lanjut == 1){
            $pelanggan  = "";
            $kota       = "";
        }
        else{
            $pelanggan  = Pelanggan::with('kota')->where('user_id', Auth::user()->id)->first();
            $kota       = Kota::where('provinsi_kode', $pelanggan->kota->provinsi_kode)->get();
        }

        $provinsi   = Provinsi::all();
        $metode_pengiriman = MetodePengiriman::where('is_aktif', 1)->get();

        return view('pages.checkout.data_pengiriman', compact('pelanggan', 'provinsi', 'kota', 'metode_pengiriman'));
    }

    public function data_pembayaran(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'email' => 'required',
            'alamat' => 'required',
            'provinsi' => 'required',
            'kota' => 'required',
            'kode_pos' => 'required',
            'hp' => 'required',
            'metode_pengiriman' => 'required',
        ]);

        date_default_timezone_set("Asia/Jakarta");
        $tanggal        = new DateTime;

        //simpan di tabel temp_alamat_pengiriman
        $temp_alamat_pengiriman = new TempAlamatPengiriman;

        $temp_alamat_pengiriman->tanggal    = $tanggal;
        $temp_alamat_pengiriman->nama       = Input::get('nama');
        $temp_alamat_pengiriman->email      = Input::get('email');
        $temp_alamat_pengiriman->alamat     = Input::get('alamat');
        $temp_alamat_pengiriman->kota_id    = Input::get('kota');
        $temp_alamat_pengiriman->kode_pos   = Input::get('kode_pos');
        $temp_alamat_pengiriman->hp         = Input::get('hp');
        $temp_alamat_pengiriman->metode_pengiriman_id = Input::get('metode_pengiriman');
        $temp_alamat_pengiriman->save();
        $temp_alamat_pengiriman_id = $temp_alamat_pengiriman->id;

        $metode_pembayaran = MetodePembayaran::where('is_aktif', 1)->where('jenis', 'non_tunai')->get();

        return view('pages.checkout.data_pembayaran', compact('temp_alamat_pengiriman_id', 'metode_pembayaran'));
    }

    public function generate_number($digit)
    {
        $mytable = DB::table('tran_so_header')->orderBy('no_nota', 'desc')->first();
        if(empty($mytable)){
            $num = 0;
        }
        else{
            //get last_number
            $num = substr($mytable->no_nota, -4);
        }

        $num = $num+1;
        $num_count = strlen((string)$num);
        $nol = (int)$digit-$num_count;
        $kode = "";
        for ($i=0; $i < $nol; $i++) { 
            $kode = $kode."0";
        }
        $kode = $kode.$num;

        return $kode;
    }

    public function review_pesanan(Request $request)
    {
        $this->validate($request, [
            'metode_pembayaran' => 'required',
        ]);

        $temp_alamat_pengiriman_id  = Input::get('temp_alamat_pengiriman_id');
        $metode_pembayaran          = Input::get('metode_pembayaran');

        date_default_timezone_set("Asia/Jakarta");
        $tanggal        = new DateTime;

        //simpan di temp
        $temp_pembayaran    = new TempPembayaran;

        $temp_pembayaran->tanggal               = $tanggal;
        $temp_pembayaran->metode_pembayaran_id  = $metode_pembayaran;
        $temp_pembayaran->save();

        $temp_pembayaran_id = $temp_pembayaran->id;

        $cart_content = Cart::content(1);

        //tampilkan hadiah produk jika ada
        $hadiah = [];
        $indeks = 0;
        foreach ($cart_content as $key => $value) {
            if(substr($value->id, 0, 3) != 'PKT') { //karena paket yang tidak punya hadiah
                //cek apakah punya hadiah
                if(substr($value->id, 0, 3) == 'DIS' || substr($value->id, 0, 3) == 'CAS') {
                    $produk_id = substr($value->id, 3);
                }
                else{
                    $produk_id = $value->id;
                }

                $produk = Produk::with('promo_hadiah')->where('id', $produk_id)->first();
                //cek dia beli berapa, dimodulus 
                if(!empty($produk->promo_hadiah)){
                    //cek berapa jumlah hadiah yang bisa didapat, masukkan ke array, SEMENTARA 1 PRODUK 1 JENIS HADIAH
                    //RUMUS DAPAT BERAPA HADIAH
                    $modulus                = $value->qty%$produk->promo_hadiah->qty_beli;
                    $jumlah_dapat_hadiah    = (($value->qty-$modulus)/$produk->promo_hadiah->qty_beli*$produk->promo_hadiah->qty_hadiah);

                    $hadiah[$indeks]['id']          = $produk->promo_hadiah->hadiah->id;
                    $hadiah[$indeks]['nama']        = $produk->promo_hadiah->hadiah->nama;
                    $hadiah[$indeks]['quantity']    = $jumlah_dapat_hadiah;
                    $indeks++;

                    //hadiah disini cuma untuk ditampilkan, sedangkan di fungsi proses pesanan, akan dicek lagi hadiah
                    //hal ini untuk menghindari ada kebocoran untuk manipulasi data
                }
            }
        }

        $kode_voucher = "";

        return view('pages.checkout.review_pesanan', compact('no_invoice', 'temp_alamat_pengiriman_id', 'temp_pembayaran_id', 'cart_content', 'kode_voucher', 'hadiah'));
    }

    public function proses_pesanan(Request $request)
    {
        $temp_alamat_pengiriman_id  = Input::get('temp_alamat_pengiriman_id');
        $temp_pembayaran_id         = Input::get('temp_pembayaran_id');
        $catatan                    = Input::get('catatan');
        $hadiah                     = Input::get('hadiah');

        if(Auth::check()){
            $pelanggan = Pelanggan::where('user_id', Auth::user()->id)->first();
        }
        else{
            $pelanggan = Pelanggan::where('id', 1)->first();
        }

        date_default_timezone_set("Asia/Jakarta");
        $tanggal        = new DateTime;

        //generate no_nota untuk sales order
        date_default_timezone_set("Asia/Jakarta");
        $tanggal        = new DateTime;

        $template_no_invoice  = DB::table('tref_nomor_invoice')->where('id', 5)->first();
        $no_invoice = $template_no_invoice->format;

        $cek_dd = strpos($no_invoice, 'DD');
        if(!empty($cek_dd))
        {
            $no_invoice = str_replace('DD', $tanggal->format('d'), $no_invoice);
        }

        $cek_mm = strpos($no_invoice, 'MM');
        if(!empty($cek_mm))
        {
            $no_invoice = str_replace('MM', $tanggal->format('m'), $no_invoice);
        }

        $cek_yyyy = strpos($no_invoice, 'YYYY');
        if(!empty($cek_yyyy))
        {
            $no_invoice = str_replace('YYYY', $tanggal->format('Y'), $no_invoice);
        }

        //cek maksimal 10
        $number = '##########';

        for ($i=9; $i > 0; $i--) { 
            $number     = substr($number, 0, $i);
            $cek_number = strpos($no_invoice, $number);
            $nol        = "";
            if(!empty($cek_number))
            {
                $so_header      = SOHeader::orderBy('id', 'desc')->first();
                if(empty($so_header))
                    $get_num = 1;
                else
                    $get_num        = (int)$so_header->id + 1;

                for ($j=0; $j < $i - count($get_num); $j++) { 
                    $nol = $nol."0";
                }

                $get_num = $nol.$get_num;
                $no_invoice = str_replace($number, $get_num, $no_invoice);
                break;
            }
        }

        /* SO HEADER */

        $so_header = new SOHeader;
        $so_header->no_sales_order          = $no_invoice;
        $so_header->pelanggan_id            = $pelanggan->id;
        $so_header->tanggal                 = $tanggal;
        $so_header->catatan                 = $catatan;
        $so_header->flag_sj_keluar          = 0;
        $so_header->ppn                     = 0; //ppn di set nol
        $so_header->save();

        $so_header_id = $so_header->id;        

        /* SO Detail */
        $cart_content   = Cart::content(1);
        $total_tagihan  = 0;
        $array_produk_id = []; //digunakan untuk mengumpulkan produk id
        $indeks = 0;

        foreach ($cart_content as $cart) {
            $id         = $cart->id;
            $so_detail  = new SODetail;

            $so_detail->so_header_id     = $so_header_id;

            //cek apakah dia paket, hadiah atau produk
            //ada 3 macam jenis_barang dapat dilihat ditabel tref_jenis_barang 
            //1. produk, 2. hadiah, 3. paket produk
            if(substr($id, 0, 3) == 'PKT'){
                $paket_id                   = substr($id, 3);
                $so_detail->produk_id       = $paket_id;
                $so_detail->jumlah          = $cart->qty;
                $so_detail->harga           = $cart->price;
                $so_detail->jenis_barang_id = 3; //id untuk jenis barang paket

                $total_tagihan = $total_tagihan+($cart->qty*$cart->price);

                $paket = Paket::where('id', $paket_id)->first();

                $so_detail->deskripsi = $paket->nama;

                //di tabel paket, tambah nilai stok dipesan
                $paket->stok_dipesan = $paket->stok_dipesan + $cart->qty;
                $paket->save();
            }
            else{
                if(substr($id, 0, 3) == 'DIS' || substr($id, 0, 3) == 'CAS'){
                    $produk_id = substr($id, 3);
                }
                else{
                    $produk_id = $id;
                }

                $so_detail->produk_id       = $produk_id;
                $so_detail->jumlah          = $cart->qty;
                $so_detail->harga           = $cart->price;
                $so_detail->jenis_barang_id = 1; //id untuk jenis barang produk

                $total_tagihan = $total_tagihan+($cart->qty*$cart->price);

                $produk = Produk::where('id', $produk_id)->first();

                $so_detail->deskripsi = $produk->deskripsi;

                //di tabel produk, tambah nilai stok dipesan
                $produk->stok_dipesan = $produk->stok_dipesan + $cart->qty;
                $produk->save();

                //cek
                $cek_array = 0;
                foreach ($array_produk_id as $key => $value) {
                    if($value['id'] == $produk_id){
                        $value['quantity'] = $value['quantity'] + $cart->qty;
                        $cek_array = 1;
                    }
                }

                if($cek_array == 0){
                    $array_produk_id[$indeks]['id']         = $produk_id;
                    $array_produk_id[$indeks]['quantity']   = $cart->qty;
                    $indeks++;
                }
            }

            $so_detail->save();
        }

        //cek apakah dia dapat hadiah, jika ada tambahkan, pengecekan dilakukan dikeseluruhan isi cart agar tdk terpengaruh dg split diskon dan cashback
        foreach ($array_produk_id as $value) {
            $produk = Produk::with('promo_hadiah')->where('id', $value['id'])->first();
            //cek dia beli berapa, dimodulus 
            if(!empty($produk->promo_hadiah)){
                //cek berapa jumlah hadiah yang bisa didapat, masukkan ke array, SEMENTARA 1 PRODUK 1 JENIS HADIAH
                //RUMUS DAPAT BERAPA HADIAH
                $modulus                = $value['quantity']%$produk->promo_hadiah->qty_beli;
                $jumlah_dapat_hadiah    = (($value['quantity']-$modulus)/$produk->promo_hadiah->qty_beli*$produk->promo_hadiah->qty_hadiah);

                $hadiah[$indeks]['id']          = $produk->promo_hadiah->hadiah->id;
                $hadiah[$indeks]['nama']        = $produk->promo_hadiah->hadiah->nama;
                $hadiah[$indeks]['quantity']    = $jumlah_dapat_hadiah;
                $indeks++;

                $so_detail = new SODetail;

                $so_detail->no_nota         = $no_invoice;
                $so_detail->produk_id       = $produk->promo_hadiah->hadiah->id;
                $so_detail->jumlah          = $jumlah_dapat_hadiah;
                $so_detail->harga           = 0;
                $so_detail->jenis_barang_id = 2;
                $so_detail->deskripsi       = $produk->promo_hadiah->hadiah->deskripsi;
                $so_detail->save();

                //di tabel hadiah, tambah nilai stok dipesan
                $hadiah = Hadiah::where('id', $produk->promo_hadiah->hadiah->id)->first();
                $hadiah->stok_dipesan = $hadiah->stok_dipesan + $jumlah_dapat_hadiah;
                $hadiah->save();
            }
        }

        /* SO Voucher */
        $kode_voucher = Input::get('input_kode_voucher');
        
        if(empty($kode_voucher)){
            $count_kode_voucher = 0;
        }
        else{
            $kode_voucher       = explode(',', $kode_voucher);
            $count_kode_voucher = count($kode_voucher);
        }        

        for ($i=0; $i < $count_kode_voucher; $i++) { 
            $voucher = Voucher::where('kode', $kode_voucher[$i])->where('is_aktif', 1)->where('awal_periode', '<=', $tanggal->format('Y-m-d'))->where('akhir_periode', '>=', $tanggal->format('Y-m-d'))->first();

            if(!empty($voucher)){
                if(empty($voucher->pelanggan_id_target) || $voucher->pelanggan_id_target == $pelanggan->id){
                    $so_voucher = new SOVoucher;
                    $so_voucher->so_header_id    = $so_header_id;
                    $so_voucher->voucher_id = $voucher->id;
                    $so_voucher->nominal    = $voucher->nominal;
                    $so_voucher->save();

                    $total_tagihan = $total_tagihan - $voucher->nominal;

                    //update tabel tmst_vouhcer
                    $voucher->tanggal_dipakai = $tanggal;
                    $voucher->pelanggan_id_pakai = $pelanggan->id;

                    if($voucher->is_sekali_pakai == 1){
                        $voucher->is_aktif = 0;
                    }

                    $voucher->save();
                }
            }
        }

        $so_header = SOHeader::where('id', $so_header_id)->first();
        $so_header->total_tagihan   = $total_tagihan; 
        $so_header->save();

        Cart::destroy(); //hapus isi cart

        /* SO Alamat Pengiriman */
        $so_alamat_pengiriman   = new SOAlamatPengiriman;
        $temp_alamat_pengiriman = TempAlamatPengiriman::where('id', $temp_alamat_pengiriman_id)->first();

        $so_alamat_pengiriman->so_header_id         = $so_header_id;
        $so_alamat_pengiriman->nama                 = $temp_alamat_pengiriman->nama;
        $so_alamat_pengiriman->email                = $temp_alamat_pengiriman->email;
        $so_alamat_pengiriman->alamat               = $temp_alamat_pengiriman->alamat;
        $so_alamat_pengiriman->kota_id              = $temp_alamat_pengiriman->kota_id;
        $so_alamat_pengiriman->kode_pos             = $temp_alamat_pengiriman->kode_pos;
        $so_alamat_pengiriman->hp                   = $temp_alamat_pengiriman->hp;
        $so_alamat_pengiriman->metode_pengiriman_id = $temp_alamat_pengiriman->metode_pengiriman_id;
        
        $so_alamat_pengiriman->save();
        $temp_alamat_pengiriman->delete();

        /* UNTUK ECOMMERCE BAYAR DISIMPAN DI SO_PEMBAYARAN */
        $temp_pembayaran = TempPembayaran::where('id', $temp_pembayaran_id)->first();

        $so_pembayaran = new SOPembayaran;
        $so_pembayaran->so_header_id            = $so_header_id;
        $so_pembayaran->metode_pembayaran_id    = $temp_pembayaran->metode_pembayaran_id;
        $so_pembayaran->bank_id                 = $temp_pembayaran->bank_id;
        $so_pembayaran->save();

        //create notif
        $notifikasi = new Notifikasi;

        $notifikasi->kode                   = 'sales_order';
        $notifikasi->pesan                  = '<i class="fa fa-shopping-cart"> Order Baru oleh akun '.$pelanggan->nama.'</i>';
        $notifikasi->hak_akses_id_target    = 1;
        $notifikasi->link                   = "sales_order";
        $notifikasi->is_aktif               = 1;
        $notifikasi->save();

        $judul                  = "INVOICE";
        $so_header              = SOHeader::with('pelanggan')->where('id', $so_header_id)->first();
        $so_detail              = SODetail::with('produk', 'hadiah', 'paket')->where('so_header_id', $so_header_id)->get();
        $so_voucher             = SOVoucher::where('so_header_id', $so_header_id)->get();
        $so_alamat_pengiriman   = SOAlamatPengiriman::with('kota')->where('so_header_id', $so_header_id)->first();
        //$bayar_invoice_penjualan  = BayarInvoicePenjualan::where('invoice_penjualan_id', $invoice_penjualan_id)->first();
        
        $perusahaan     = Perusahaan::with('kota')->first();

        $total_voucher = 0;
        foreach ($so_voucher as $key => $value) {
            $total_voucher = $total_voucher + $value->nominal;
        }

        date_default_timezone_set("Asia/Jakarta"); 
        $print_time = new DateTime;

        $judul      = "Invoice";
        $fileName   = $judul.'_'.$perusahaan->nama.'_'.$print_time->format('Ymd').'_'.$print_time->format('Gis').'.pdf';
        $filePath   = base_path().'/public/pdf/';

        $pdf = PDF::loadView('pages.transaksi.pelanggan_invoice_pdf', compact('judul', 'so_header', 'so_detail', 'so_voucher', 'so_alamat_pengiriman', 'perusahaan', 'print_time'))->setPaper('a4', 'landscape')->save($filePath.$fileName);;
        $mailAttachment = $filePath.$fileName;
        //kirim email
        Mail::send('pages.checkout.email_invoice_pelanggan', ['so_header' => $so_header, 'perusahaan' => $perusahaan, 'so_alamat_pengiriman' => $so_alamat_pengiriman], function ($mail) use ($so_alamat_pengiriman, $mailAttachment)
        {
            $mail->from('noreplay@pos.com', "noreplay");
            $mail->to($so_alamat_pengiriman->email, $so_alamat_pengiriman->nama); 
            $mail->subject('Email Pesanan POS');
            $mail->attach($mailAttachment);
        }); 
        
        return redirect('/checkout/invoice_pelanggan/'.$so_header_id);
    }

    public function invoice_pelanggan($so_header_id)
    {
        $judul                  = "INVOICE";
        $so_header              = SOHeader::with('pelanggan')->where('id', $so_header_id)->first();
        $so_detail_obj          = SODetail::where('so_header_id', $so_header_id)->get();
        $so_voucher             = SOVoucher::where('so_header_id', $so_header_id)->get();
        $so_alamat_pengiriman   = SOAlamatPengiriman::with('kota')->where('so_header_id', $so_header_id)->first();
        //$bayar_invoice_penjualan  = BayarInvoicePenjualan::where('invoice_penjualan_id', $so_header->invoice_penjualan_id)->first();
        
        $perusahaan     = Perusahaan::with('kota')->first();

        //dapatkan detail data produk, paket, hadiah di SODetail
        $so_detail = [];
        foreach ($so_detail_obj as $key => $value) {
            if($value->jenis_barang_id == 1){
                $produk = Produk::where('id', $value->produk_id)->first();
                $so_detail[$key]['nama']    = $produk->nama;
                $so_detail[$key]['satuan']  = $produk->satuan;
            }
            elseif($value->jenis_barang_id == 2) {
                $hadiah = Hadiah::where('id', $value->produk_id)->first();
                $so_detail[$key]['nama']    = $hadiah->nama;
                $so_detail[$key]['satuan']  = $hadiah->satuan;
            }
            elseif($value->jenis_barang_id == 3) {
                $paket = Paket::where('id', $value->produk_id)->first();
                $so_detail[$key]['nama']    = $paket->nama;
                $so_detail[$key]['satuan']  = "paket";
            }

            $so_detail[$key]['harga']       = $value->harga;
            $so_detail[$key]['jumlah']    = $value->jumlah;
        }

        $total_voucher = 0;
        foreach ($so_voucher as $key => $value) {
            $total_voucher = $total_voucher + $value->nominal;
        }

        $terbilang = $so_header->total_tagihan;

        Cart::destroy();

        return view('pages.checkout.invoice_pelanggan', compact('so_header', 'so_detail', 'invoice_detail', 'invoice', 'so_voucher', 'perusahaan', 'bayar_invoice', 'so_alamat_pengiriman', 'judul', 'terbilang'));
    }

    public function cek_voucher_home()
    {
        $total_bayar    = Input::get('total_bayar');
        $kode_voucher   = Input::get('kode_voucher');

        $pelanggan = Pelanggan::where('user_id', Auth::user()->id)->first();

        date_default_timezone_set("Asia/Jakarta");
        $tanggal        = new DateTime;

        //cek apakah voucher masih berlaku dan diperuntukkan untuk pelanggan tsb
        $voucher = Voucher::where('kode', $kode_voucher)->where('is_aktif', 1)->where('awal_periode', '<=', $tanggal->format('Y-m-d'))->where('akhir_periode', '>=', $tanggal->format('Y-m-d'))->first();

        $myvoucher['kode']      = "";
        $myvoucher['nominal']   = "";

        if(!empty($voucher)){
            if($total_bayar >= $voucher->nominal){
                if(empty($voucher->pelanggan_id_target) || $voucher->pelanggan_id_target == $pelanggan->id){
                    $myvoucher['kode']      = $voucher->kode;
                    $myvoucher['nominal']   = $voucher->nominal;
                }
            }
        }

        return Response::json($myvoucher);
    }
}
