<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Input;
use Validator;
use DateTime;
use App\Models\Pengaturan\MasterCOA;
use App\Models\Pengaturan\NamaTransaksi;
use App\Models\Pengaturan\PengaturanJurnal;
use App\Models\Pengaturan\GroupAccount;
use File;
use Response;

class PengaturanController extends Controller {

    public function ecommerce() {
        $general['title'] = "E-Commerce";
        $general['menu1'] = "Pengaturan";
        $general['menu2'] = $general['title'];
        $general['kode_menu'] = 21;

        // for ($i=1; $i <= 6 ; $i++) { 
        //     $halaman_promo[$i] = DB::table('halaman_promo')->where('id', $i)->first();
        // }

        $produk = DB::table("tmst_produk")->get();
        $produk_baru = DB::table('tran_produk_baru')->join('tmst_produk', 'tmst_produk.id', '=', 'tran_produk_baru.produk_id')
                        ->orderBy('tran_produk_baru.tanggal_input', 'desc')->get();
        $produk_akan_datang = DB::table('tran_produk_akan_datang')->join('tmst_produk', 'tmst_produk.id', '=', 'tran_produk_akan_datang.produk_id')->get();

        $gambar_produk_default = DB::table('static')->where('nama', 'gambar_produk_default')->first();

        $halaman_promo = DB::table('halaman_promo')->orderBy('item_order', 'asc')->get();
        $produk_tampil_di_beranda = DB::table('tran_produk_tampil_di_beranda')->join('tmst_produk', 'tmst_produk.id', '=', 'tran_produk_tampil_di_beranda.produk_id')->get();

        $cara_belanja = "";
        $static = DB::table('static')->where('id', 2)->first();

        if (!empty($static)) {
            $cara_belanja = $static->value;
        }

        return view('pages.pengaturan.ecommerce', compact('general', 'halaman_promo', 'produk', 'produk_baru', 'produk_akan_datang', 'gambar_produk_default', 'produk_tampil_di_beranda', 'cara_belanja'));
    }

    public function generate_kode($index) {
        $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $res = "";
        for ($i = 0; $i < $index; $i++) {
            $res .= $chars[mt_rand(0, strlen($chars) - 1)];
        }
        return $res;
    }

    public function ubah_banner_promo($id, Request $request) {
        $file = array('banner' => Input::file('banner'));

        if (Input::hasFile('banner')) {
            $destinationPath = base_path() . '/public/img/banner/';
            $extension = Input::file('banner')->getClientOriginalExtension();
            $file_gambar = $this->generate_kode(3) . "-banner";
            $fileName = $file_gambar . '.' . $extension;
            $banner = DB::table('halaman_promo')->where('id', $id)->first();

            if (empty($banner)) {
                $halaman_promo = DB::table('halaman_promo')->orderBy('item_order', 'desc')->first();
                $item_order = (int) $halaman_promo->item_order + 1;

                DB::table('halaman_promo')->insert(['file_gambar' => $fileName, 'is_aktif' => 1, 'item_order' => $item_order]);
            } else {
                File::Delete(base_path() . '/public/img/banner/' . $banner->file_gambar);
                DB::table('halaman_promo')->where('id', $id)->update(['file_gambar' => $fileName, 'is_aktif' => 1]);
            }

            Input::file('banner')->move($destinationPath, $fileName);

            $request->session()->flash('message', 'Gambar berhasil ditambahkan');
            return redirect('/ecommerce');
        }
    }

    public function ubah_status_halaman_promo($id, Request $request) {
        $halaman_promo = DB::table('halaman_promo')->where('id', $id)->first();

        if ($halaman_promo->is_aktif == 1) {
            DB::table('halaman_promo')->where('id', $id)->update(['is_aktif' => 0]);
        } else if ($halaman_promo->is_aktif == 0) {
            DB::table('halaman_promo')->where('id', $id)->update(['is_aktif' => 1]);
        }

        $request->session()->flash('message', 'Status berhasil diperbarui');
        return redirect('/ecommerce');
    }

    public function group_account() {
        $general['title'] = "Group Account";
        $general['menu1'] = "Pengaturan";
        $general['menu2'] = $general['title'];
        $general['kode_menu'] = 23;

        $group_account = DB::table('group_account')->get();

        return view('pages.pengaturan.group_account', compact('general', 'group_account'));
    }

    public function set_order_halaman_promo(Request $request) {
        $list_order = Input::get('list_order');

        $list_order = explode(',', $list_order);
        $halaman_promo = DB::table('halaman_promo')->orderBy('item_order', 'asc')->get();
        $i = 0;
        foreach ($halaman_promo as $val) {
            DB::table('halaman_promo')->where('id', $val->id)->update(['item_order' => $list_order[$i]]);
            $i++;
        }

        $new_halaman_promo = DB::table('halaman_promo')->orderBy('item_order', 'asc')->get();
    }

    public function tambah_banner_promo(Request $request) {
        $file = array('banner' => Input::file('banner'));

        if (Input::hasFile('banner')) {
            $destinationPath = base_path() . '/public/img/banner/';
            $extension = Input::file('banner')->getClientOriginalExtension();
            $file_gambar = $this->generate_kode(3) . "-banner";
            $fileName = $file_gambar . '.' . $extension;
            $halaman_promo = DB::table('halaman_promo')->orderBy('item_order', 'desc')->first();
            $item_order = (int) $halaman_promo->item_order + 1;

            DB::table('halaman_promo')->insert(['file_gambar' => $fileName, 'is_aktif' => 1, 'item_order' => $item_order]);

            Input::file('banner')->move($destinationPath, $fileName);

            $request->session()->flash('message', 'Gambar berhasil ditambahkan');
            return redirect('/ecommerce');
        }
    }

    public function hapus_halaman_promo(Request $request) {
        $id = Input::get('id');

        $banner = DB::table('halaman_promo')->where('id', $id)->first();
        File::Delete(base_path() . '/public/img/banner/' . $banner->file_gambar);
        DB::table('halaman_promo')->where('id', $id)->delete();

        $request->session()->flash('message', 'Slider promo berhasil dihapus');
        return redirect('/ecommerce');
    }

    public function tambah_root_coa() {
        $row = Input::get('row');
        $master_coa = MasterCOA::where('level', 0)->orderBy('kode', 'desc')->first();
        if (empty($master_coa)) {
            $kode = 1;
        } else {
            $kode = (int) $master_coa->kode + 1;
        }

        if ($kode < 10)
            
        {
            $kode = "00" . $kode;
        }

        if ($kode < 100 && $kode > 10) {
            $kode = "0" . $kode;
        }

        $level = 0;
        $parent = '-';

        return view('pages.pengaturan.ajax_tambah_root', compact('kode', 'level', 'parent'));
    }

    public function do_tambah_root_coa(Request $request) {
        $master_coa = new MasterCOA;

        $master_coa->level = Input::get('level');
        $master_coa->kode = Input::get('kode');
        $master_coa->nama = Input::get('nama');
        $master_coa->debit_or_kredit = Input::get('debit_or_kredit');
        $master_coa->parent = Input::get('parent');
        $master_coa->save();

        $request->session()->flash('message', 'Root berhasil ditambahkan');
        return redirect('/master_coa');
    }

    public function get_row_child_coa() {
        //cek dia punya berapa child
        $row = Input::get('row');
        $kode = Input::get('kode');
        $jumlah = strlen($kode);

        $master_coa = MasterCOA::all();
        $n = 0;
        foreach ($master_coa as $val) {
            $cek = substr($val->kode, 0, $jumlah);

            if ($cek == $kode) {
                $n++;
            }
        }

        $final_row = $row + $n;

        return $final_row;
    }

    public function tambah_child_coa() {
        $kode = Input::get('kode');
        $parent_coa = MasterCOA::where('kode', $kode)->orderBy('kode', 'desc')->first();
        $master_coa = MasterCOA::where('parent', $kode)->orderBy('kode', 'desc')->get();

        $child_kode = count($master_coa) + 1;

        if ($child_kode < 10) {
            $child_kode = "00" . $child_kode;
        }

        if ($child_kode < 100 && $child_kode > 10) {
            $child_kode = "0" . $child_kode;
        }

        $kode = $kode . '.' . $child_kode;

        $level = (int) $parent_coa->level + 1;
        $parent = $parent_coa->kode;

        return view('pages.pengaturan.ajax_tambah_root', compact('kode', 'level', 'parent'));
    }

    function tambah_coa(Request $request) {
        $master_coa = new MasterCOA;

        $master_coa->level = Input::get('level');
        $master_coa->kode = Input::get('kode');
        $master_coa->nama = Input::get('nama');
        $master_coa->debit_or_kredit = Input::get('debit_or_kredit');
        $master_coa->parent = Input::get('parent');
        $master_coa->save();

        $request->session()->flash('message', 'Akun berhasil ditambahkan');
        return redirect('/master_coa');
    }

    public function tambah_produk_tampil_di_beranda(Request $request) {
        $produk_id = Input::get("id");
        date_default_timezone_set("Asia/Jakarta");
        $tanggal = date("Y-m-d G:i:s");

        DB::table('tran_produk_tampil_di_beranda')->insert(['produk_id' => $produk_id, 'tanggal_input' => $tanggal]);

        $request->session()->flash('message3', 'Produk tampil di beranda berhasil ditambahkan');
        return redirect('/promo_creator');
    }

    public function hapus_produk_tampil_di_beranda(Request $request) {
        $produk_id = Input::get('id');

        DB::table('tran_produk_tampil_di_beranda')->where('produk_id', $produk_id)->delete();

        $request->session()->flash('message3', 'Produk tampil di beranda berhasil dihapus');
        return redirect('/promo_creator');
    }

    public function tambah_cara_belanja(Request $request) {
        $cara_belanja = Input::get('cara_belanja');

        //jika kosong maka tambah

        $cek = DB::table('static')->where('id', 2)->first();

        if (empty($cek)) {
            DB::table('static')->insert(['id' => 2, 'nama' => 'cara_belanja', 'value' => $cara_belanja]);
        } else {
            DB::table('static')->where('id', 2)->update(['nama' => 'cara_belanja', 'value' => $cara_belanja]);
        }

        $request->session()->flash('message7', 'Cara belanja berhasil ditambahkan');
        return redirect('/ecommerce');
    }

    public function tambah_syarat_ketentuan(Request $request) {
        $perihal = Input::get('nama_syarat_ketentuan');
    }

}
