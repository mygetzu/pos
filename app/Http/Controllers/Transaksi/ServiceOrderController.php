<?php

namespace App\Http\Controllers\Transaksi;

use App\Models\Cabang;
use App\Models\Master\Produk;
use App\Models\Notifikasi;
use App\Models\TransaksiPenjualan\SJKeluarDetail;
use App\Models\TransaksiPenjualan\SJKeluarHeader;
use App\Models\TransaksiService\InvoiceDetail;
use App\Models\TransaksiService\InvoiceHeader;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Mail;
use Input;
use Auth;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Session;
use Promocodes;
use Response;
use DateTime;
use PDF;
use Hashids\Hashids;
use Symfony\Component\EventDispatcher\Tests\Service;
use Validator;
use Illuminate\Http\RedirectResponse;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use App\User;
use App\Models\TransaksiPenjualan\ServiceOrder;
use App\Models\Pelanggan\Pelanggan;
use App\Models\Referensi\Provinsi;
use App\Models\Referensi\Kota;
use App\Models\Perusahaan;
use PHPMailer;

class ServiceOrderController extends Controller
{

    public function servis()
    {
        $general['title'] = "Servis";
        $general['menu1'] = "Transaksi";
        $general['menu2'] = $general['title'];
        $general['kode_menu'] = 55;

        $teknisi = User::where('hak_akses_id', 3)->where('is_aktif', 1)->get();
        $service_order = ServiceOrder::with('pelanggan')->orderBy('tanggal_diterima', 'desc')->orderBy('id', 'desc')->get();
        foreach ($service_order as $key => $value) {
            $service_order_id = $value->id;
            $teknisi_id = $value->teknisi_id;
            $service_header[$key]['tanggal_diambil'] = $value->tanggal_diambil;
            $service_header[$key]['invoice_header_count'] = InvoiceHeader::where('service_order_id', $service_order_id)->count();
            $service_header[$key]['invoice_header'] = InvoiceHeader::where('service_order_id', $service_order_id)->get();
            if ($service_header[$key]['invoice_header_count'] == 0) {
                $service_header[$key]['is_submit'] = 0;
                $service_header[$key]['harga_total'] = 0;
                $service_header[$key]['is_lunas'] = 0;
            } else {
                foreach ($service_header[$key]['invoice_header'] as $val) {
                    $service_header[$key]['is_submit'] = $val->is_submit;
                    $service_header[$key]['harga_total'] = $val->harga_total;
                    $service_header[$key]['is_lunas'] = $val->is_lunas;
                }
            }
            $service_header[$key]['teknisi'] = User::where('id', $teknisi_id)->get();
            foreach ($service_header[$key]['teknisi'] as $val) {
                $teknisi_data[$key]['nama'] = $val->name;
                $teknisi_data[$key]['email'] = $val->email;
                $teknisi_data[$key]['alamat'] = $val->alamat;
                $teknisi_data[$key]['hp'] = $val->hp;
            }
        }

        return view('pages.transaksi.servis', compact('general', 'service_order', 'teknisi', 'teknisi_data', 'service_header'));
    }

    public function tambah_servis()
    {
        $general['title'] = "Tambah Servis";
        $general['menu1'] = "Transaksi";
        $general['menu2'] = "Servis";
        $general['menu3'] = $general['title'];
        $general['kode_menu'] = 55;

        $pelanggan = Pelanggan::with('kota')->where('kota_id', "!=", 1)->get();

        $provinsi = Provinsi::all();

        date_default_timezone_set("Asia/Jakarta");
        $tanggal = new DateTime;

        $nomor_invoice = DB::table('tref_nomor_invoice')->where('id', 9)->first();
        $format_invoice = $nomor_invoice->format;
        $ori_format_invoice = $format_invoice;

        $cek_dd = strpos($format_invoice, 'DD');
        if (!empty($cek_dd)) {
            $format_invoice = str_replace('DD', $tanggal->format('d'), $format_invoice);
        }

        $cek_mm = strpos($format_invoice, 'MM');
        if (!empty($cek_mm)) {
            $format_invoice = str_replace('MM', $tanggal->format('m'), $format_invoice);
        }

        $cek_yyyy = strpos($format_invoice, 'YYYY');
        if (!empty($cek_yyyy)) {
            $format_invoice = str_replace('YYYY', $tanggal->format('Y'), $format_invoice);
        }

        $cek_yy = strpos($format_invoice, 'YY');
        if (!empty($cek_yy)) {
            $format_invoice = str_replace('YY', $tanggal->format('y'), $format_invoice);
        }

        //cek maksimal 10
        $number = '##########';

        for ($i = 9; $i > 0; $i--) {
            $number = substr($number, 0, $i);
            $cek_number = strpos($format_invoice, $number);
            $nol = "";
            if (!empty($cek_number)) {
                $service_order = ServiceOrder::orderBy('id', 'desc')->first();
                if (empty($service_order))
                    $get_num = 1;
                else
                    $get_num = (int)$service_order->id + 1;

                for ($j = 0; $j < $i - count($get_num); $j++) {
                    $nol = $nol . "0";
                }

                $get_num = $nol . $get_num;
                $format_invoice = str_replace($number, $get_num, $format_invoice);
                break;
            }
        }

        $no_invoice = $format_invoice;

        /*-- Menampilkan transaksi penjualan --*/
        $data['get_sj_keluar_detail'] = SJKeluarDetail::get();
        foreach ($data['get_sj_keluar_detail'] as $key => $value) {
            $data[$key]['sj_keluar_detail_id'] = $value->id;
            $data[$key]['serial_number'] = $value->serial_number;
            $data[$key]['produk_id'] = $value->produk_id;
            $data[$key]['get_produk'] = Produk::where('id', $data[$key]['produk_id'])->first();
            $data[$key]['produk_nama'] = $data[$key]['get_produk']->nama;
            $data[$key]['sj_keluar_header_id'] = $value->sj_keluar_header_id;
            $data[$key]['get_sj_keluar_header'] = SJKeluarHeader::where('id', $data[$key]['sj_keluar_header_id'])->first();
            $data[$key]['pelanggan_id'] = $data[$key]['get_sj_keluar_header']->pelanggan_id;
            $data[$key]['tanggal'] = $data[$key]['get_sj_keluar_header']->tanggal;
            $data[$key]['get_pelanggan'] = Pelanggan::where('id', $data[$key]['pelanggan_id'])->first();
            $data[$key]['pelanggan_nama'] = $data[$key]['get_pelanggan']->nama;
            $data[$key]['pelanggan_telp1'] = $data[$key]['get_pelanggan']->telp1;
            $data[$key]['pelanggan_telp2'] = $data[$key]['get_pelanggan']->telp2;
            $data[$key]['pelanggan_email'] = $data[$key]['get_pelanggan']->email;
            $data[$key]['pelanggan_alamat'] = $data[$key]['get_pelanggan']->alamat;
            $data[$key]['pelanggan_kota_id'] = $data[$key]['get_pelanggan']->kota_id;
            $data[$key]['pelanggan_provinsi_id'] = Kota::where('id', $data[$key]['pelanggan_kota_id'])->value('provinsi_kode');
        }


        return view('pages.transaksi.tambah_servis', compact('general', 'pelanggan', 'provinsi', 'format_invoice', 'no_invoice', 'ori_format_invoice', 'data'));
    }

    public function get_pelanggan_by_serial_number($serial_number)
    {
        $sj_keluar_header_id = SJKeluarDetail::where('serial_number', $serial_number)->value('sj_keluar_header_id');
        if ($sj_keluar_header_id == NULL) {
            //Data tidak ditemukan
            $data['pelanggan'] = NULL;
        } else {
            $pelanggan_id = SJKeluarHeader::where('id', $sj_keluar_header_id)->value('pelanggan_id');
            $data['pelanggan'] = Pelanggan::where('id', $pelanggan_id)->first();
        }
        return $data['pelanggan'];
    }

    public function servis_checkout(Request $request)
    {
        $this->validate($request, [
            'pelanggan_nama' => 'required',
            'pelanggan_email' => 'required',
            'pelanggan_ponsel' => 'required',
            'pelanggan_alamat' => 'required',
            'model_produk' => 'required',
            'status_garansi' => 'required',
            'serial_number' => 'required',
            'keluhan_pelanggan' => 'required',
            'tanggal_diterima' => 'required',
        ]);

        $no_nota = Input::get('no_service');
        $serial_number      = strtoupper(strtolower(Input::get('serial_number')));
        $model_produk       = Input::get('model_produk');
        $pelanggan_id       = Input::get('pelanggan_id');
        $status_garansi     = Input::get('status_garansi');
        $keluhan_pelanggan  = Input::get('keluhan_pelanggan');
        $deskripsi_produk   = Input::get('deskripsi_produk');
        $tanggal_diterima   = date('Y-m-d H:i:s');
        //Kelengkapan unit
        $kelengkapan_baterai    = Input::get('kelengkapan_baterai');
        $kelengkapan_adapter    = Input::get('kelengkapan_adapter');
        $kelengkapan_memory     = Input::get('kelengkapan_memory');
        $kelengkapan_harddisk   = Input::get('kelengkapan_harddisk');
        $kelengkapan_other      = Input::get('kelengkapan_other');
        $kelengkapan_odd        = Input::get('kelengkapan_odd');
        $kelengkapan_scrb       = Input::get('kelengkapan_scrb');
        $kelengkapan_wifi       = Input::get('kelengkapan_wifi');
        $kelengkapan_modem      = Input::get('kelengkapan_modem');
        $kelengkapan_lan        = Input::get('kelengkapan_lan');
        $kelengkapan_bluetooth  = Input::get('kelengkapan_bluetooth');
        //Data Pelanggan
        $pelanggan_email        = Input::get('pelanggan_email');
        $pelanggan_nama         = Input::get('pelanggan_nama');
        $pelanggan_ponsel       = Input::get('pelanggan_ponsel');
        $pelanggan_alamat       = Input::get('pelanggan_alamat');

        $check_email = Pelanggan::where('email', $pelanggan_email)->count();
        $pelanggan_id_new = 0;
        if($check_email == 0){
            //Tambahkan tabel pelanggan
            $pelanggan = new Pelanggan;

            $pelanggan->nama = $pelanggan_nama;
            $pelanggan->email = $pelanggan_email;
            $pelanggan->alamat = $pelanggan_alamat;
            $pelanggan->telp1 = $pelanggan_ponsel;

            $pelanggan->save();

            $pelanggan_id_new = $pelanggan->id;
        } else {
            //Update tabel pelanggan
            $pelanggan = Pelanggan::where('email', $pelanggan_email)->first();

            $pelanggan->nama    = $pelanggan_nama;
            $pelanggan->alamat    = $pelanggan_alamat;
            $pelanggan->telp2     = $pelanggan_ponsel;

            $pelanggan->save();

            $pelanggan_id_new = $pelanggan->id;
        }

        $service_order = new ServiceOrder;

        $service_order->no_nota         = $no_nota;
        $service_order->pelanggan_id    = $pelanggan_id_new;
        $service_order->teknisi_id      = NULL;
        $service_order->model_produk    = $model_produk;
        $service_order->serial_number   = $serial_number;
        //Kelengkapan unit
        $service_order->kelengkapan_baterai     = $kelengkapan_baterai;
        $service_order->kelengkapan_adapter     = $kelengkapan_adapter;
        $service_order->kelengkapan_memory      = $kelengkapan_memory;
        $service_order->kelengkapan_harddisk    = $kelengkapan_harddisk;
        $service_order->kelengkapan_other       = $kelengkapan_other;
        $service_order->kelengkapan_odd         = $kelengkapan_odd;
        $service_order->kelengkapan_scrb        = $kelengkapan_scrb;
        $service_order->kelengkapan_wifi        = $kelengkapan_wifi;
        $service_order->kelengkapan_modem       = $kelengkapan_modem;
        $service_order->kelengkapan_lan         = $kelengkapan_lan;
        $service_order->kelengkapan_bluetooth   = $kelengkapan_bluetooth;
        //Keluhan pelanggan
        $service_order->status_garansi_produk   = $status_garansi;
        $service_order->keluhan_pelanggan       = $keluhan_pelanggan;
        $service_order->deskripsi_produk        = $deskripsi_produk;
        $service_order->kode_verifikasi         = md5($no_nota . $serial_number);
        $service_order->garansi_service         = NULL;
        $service_order->tanggal_diterima        = $tanggal_diterima;
        $service_order->tanggal_selesai         = NULL;
        $service_order->tanggal_diambil         = NULL;
        $service_order->flag_case               = 0;
        $service_order->is_check                = 0;
        $service_order->status_service          = 'Belum';
        $service_order->no_jurnal               = NULL;
        $service_order->save();

        //sendmail to customer
        $data['service_id'] = $service_order->id;
        $data['invoice_header_id'] = InvoiceHeader::where('service_order_id', $data['service_id'])->value('id');
        $this->sendmail('check', $data['service_id'], $data['invoice_header_id']);

        //save to notifikasi
        $notifikasi = new Notifikasi;

        $notifikasi->kode                   = 'servis_order';
        $notifikasi->pesan                  = '<i class="fa fa-cog"> Pilih teknisi untuk servis '.$model_produk.'</i>';
        $notifikasi->hak_akses_id_target    = 1;
        $notifikasi->link                   = "servis";
        $notifikasi->is_aktif               = 1;
        $notifikasi->save();

        $service_order_id = $service_order->id;

        return redirect('/nota_service_order/' . $service_order_id);
    }

    /*
     * Fungsi servis_checkout_old merupakan function yang dibuat oleh mas Ridwan. 
     * Kini telah digantikan dengan servis_checkout 
     */

    public function servis_checkout_old(Request $request)
    {
        $this->validate($request, [
            'pelanggan_id' => 'required',
            'model' => 'required',
            'serial_number' => 'required',
            'keluhan' => 'required',
            'tanggal' => 'required',
        ]);

        /* SERVICE ORDER */

        $no_nota = Input::get('no_service');
        $catatan = Input::get('catatan');
        $pelanggan_id = Input::get('pelanggan_id');
        $tanggal = Input::get('tanggal');
        $model = Input::get('model');
        $serial_number = Input::get('serial_number');
        $garansi = Input::get('garansi');
        $keluhan = Input::get('keluhan');

        $tanggal = new DateTime($tanggal);

        $service_order = new ServiceOrder;

        $service_order->no_nota = $no_nota;
        $service_order->catatan = $catatan;
        $service_order->pelanggan_id = $pelanggan_id;
        $service_order->tanggal = $tanggal;
        $service_order->model = $model;
        $service_order->serial_number = $serial_number;
        $service_order->garansi = $garansi;
        $service_order->keluhan = $keluhan;
        $service_order->flag = 0;
        $service_order->is_lunas = 0;
        $service_order->save();

        $service_order_id = $service_order->id;

        return redirect('/nota_service_order/' . $service_order_id);
    }

    public function nota_service_order($service_order_id)
    {
        $service_order = ServiceOrder::with('pelanggan')->where('id', $service_order_id)->first();

        $judul = "Nota Service Order";
        $general['title'] = $judul;
        $general['menu1'] = "Transaksi";
        $general['menu2'] = "Service Order";
        $general['menu3'] = $general['title'];
        $general['kode_menu'] = 55;

        //$perusahaan = Perusahaan::with('kota')->first();
        $perusahaan = Cabang::with('kota')->first();

        date_default_timezone_set("Asia/Jakarta");
        $print_time = new DateTime;

        return view('pages.transaksi.service_order.nota_service_order', compact('general', 'judul', 'service_order', 'perusahaan', 'print_time'));
    }

    public function invoice_service_order($service_order_id)
    {
        $service_order = ServiceOrder::with('pelanggan')->where('id', $service_order_id)->first();

        $judul = "Nota Service Order";
        $general['title'] = $judul;
        $general['menu1'] = "Transaksi";
        $general['menu2'] = "Service Order";
        $general['menu3'] = $general['title'];
        $general['kode_menu'] = 55;

        //$perusahaan = Perusahaan::with('kota')->first();
        $perusahaan = Cabang::with('kota')->first();

        date_default_timezone_set("Asia/Jakarta");
        $print_time = new DateTime;

        return view('pages.transaksi.service_order.invoice_service_order', compact('general', 'judul', 'service_order', 'perusahaan', 'print_time'));
    }

    public function nota_service_order_print($service_order_id)
    {
        $judul = "Nota Service Order";
        $service_order = ServiceOrder::with('pelanggan')->where('id', $service_order_id)->first();

        $perusahaan = Perusahaan::with('kota')->first();

        date_default_timezone_set("Asia/Jakarta");
        $print_time = new DateTime;

        return view('pages.transaksi.service_order.nota_service_order_print', compact('judul', 'service_order', 'perusahaan', 'print_time'));
    }

    public function invoice_service_order_print($service_order_id)
    {
        $judul = "Nota Service Order";
        $service_order = ServiceOrder::with('pelanggan')->where('id', $service_order_id)->first();

        $perusahaan = Perusahaan::with('kota')->first();

        date_default_timezone_set("Asia/Jakarta");
        $print_time = new DateTime;

        return view('pages.transaksi.service_order.invoice_service_order_print', compact('judul', 'service_order', 'perusahaan', 'print_time'));
    }

    public function nota_service_order_pdf($service_order_id)
    {
        $judul = "Nota Service Order";
        $service_order = ServiceOrder::with('pelanggan')->where('id', $service_order_id)->first();

        $perusahaan = Perusahaan::with('kota')->first();

        date_default_timezone_set("Asia/Jakarta");
        $print_time = new DateTime;

        $pdf = PDF::loadView('pages.transaksi.service_order.nota_service_order_pdf', compact('judul', 'service_order', 'perusahaan', 'print_time'))->setPaper('a4', 'landscape');
        return $pdf->stream();
    }

    public function invoice_service_order_pdf($service_order_id)
    {
        $judul = "Nota Service Order";
        $service_order = ServiceOrder::with('pelanggan')->where('id', $service_order_id)->first();

        $perusahaan = Perusahaan::with('kota')->first();

        date_default_timezone_set("Asia/Jakarta");
        $print_time = new DateTime;

        $pdf = PDF::loadView('pages.transaksi.service_order.nota_service_order_pdf', compact('judul', 'service_order', 'perusahaan', 'print_time'))->setPaper('a4', 'landscape');
        return $pdf->stream();
    }

    public function detail_servis($service_order_id)
    {
        $general['title'] = "Detail Servis";
        $general['menu1'] = "Transaksi";
        $general['menu2'] = "Servis";
        $general['menu3'] = $general['title'];
        $general['kode_menu'] = 55;

        $service_order = ServiceOrder::with('pelanggan')->where('id', $service_order_id)->first();
        $service_order['nama_teknisi'] = User::where('id', $service_order->teknisi_id)->value('name');
        $service_order_id = $service_order->id;
        $invoice_header['count'] = InvoiceHeader::where('service_order_id', $service_order_id)->count();
        if ($invoice_header['count'] == 0) {
            $invoice_detail = NULL;
            $invoice_header['id'] = NULL;
            $invoice_header['biaya'] = NULL;
            $invoice_header['status_bayar'] = NULL;
        } else {
            $get_invoice_header = InvoiceHeader::where('service_order_id', $service_order_id)->first();
            $invoice_header['id'] = $get_invoice_header->id;
            $invoice_header['biaya'] = $get_invoice_header->harga_total;
            $invoice_header['status_bayar'] = $get_invoice_header->is_lunas;

            $invoice_detail = InvoiceDetail::where('service_invoice_header_id', $invoice_header['id'])->get();
        }

        return view('pages.transaksi.detail_servis', compact('general', 'service_order', 'invoice_header', 'invoice_detail'));
    }

    public function servis_selesai(Request $request)
    {
        $this->validate($request, [
            'no_nota' => 'required',
            'tanggal_selesai' => 'required',
            'deskripsi_penyelesaian' => 'required',
            'jasa_servis' => 'required',
            'pelanggan_id' => 'required',
        ]);

        $no_nota = Input::get('no_nota');
        $tanggal_selesai = Input::get('tanggal_selesai');
        $deskripsi_penyelesaian = Input::get('deskripsi_penyelesaian');
        $jasa_servis = Input::get('jasa_servis');
        $pelanggan_id = Input::get('pelanggan_id');

        $jasa_servis = str_replace(".", "", $jasa_servis);

        $tanggal_selesai = new DateTime($tanggal_selesai);

        $service_order = ServiceOrder::where('no_nota', $no_nota)->first();
        $service_order->tanggal_selesai = $tanggal_selesai;
        $service_order->deskripsi_penyelesaian = $deskripsi_penyelesaian;
        $service_order->jasa_service = $jasa_servis;
        $service_order->flag = 1;
        $service_order->save();

        $service_order_id = $service_order->id;

        return redirect('/nota_service_order/' . $service_order_id);
    }

    public function tambah_pelanggan_baru_servis(Request $request)
    {
        //data pelanggan baru insert ke user dan customer
        $validator = Validator::make($request->all(), [
            'nama' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'alamat' => 'required|min:4',
            'provinsi' => 'required',
            'kota_id' => 'required',
        ]);

        if ($validator->fails()) {
            $request->session()->flash('message_error', 'Gagal menambahkan pelanggan, periksa kelengkapan data');
            return redirect('/tambah_servis');
        }

        $nama = ucwords(Input::get('nama'));
        $email = Input::get('email');
        $alamat = Input::get('alamat');
        $kota_id = Input::get('kota_id');
        $hp = Input::get('hp');

        //Auth::guard($this->create($request->all()));
        $user_id = DB::table('users')->insertGetId(['name' => $nama, 'email' => $email, 'password' => bcrypt(123)]);
        //get id
        $user = DB::table('users')->where('name', $nama)->where('email', $email)->first();
        DB::table('users')->where('id', $user->id)->update(['alamat' => $alamat, 'kota_id' => $kota_id, 'hp' => $hp, 'hak_akses_id' => '2', 'is_aktif' => '1']);

        $user = DB::table('users')->where('id', $user->id)->first();

        //tambahkan juga di tmst_pelanggan
        DB::table('tmst_pelanggan')->insert(['user_id' => $user_id, 'nama' => $nama, 'email' => $email, 'alamat' => $alamat, 'kota_id' => $kota_id, 'hp_sales' => $hp]);

        $request->session()->flash('message', 'Pelanggan baru berhasil ditambahkan');
        return redirect('/tambah_servis');
    }

    public function ubah_format_no_nota_servis(Request $request)
    {
        $format = strtoupper(Input::get('format'));

        DB::table('tref_nomor_invoice')->where('id', 9)->update(['format' => $format]);

        $request->session()->flash('message', 'Nomor Nota Servis berhasil diperbarui');
        return redirect('/tambah_servis');
    }

    public function pilih_teknisi($id)
    {
        $general['title'] = "Servis";
        $general['menu1'] = "Transaksi";
        $general['menu2'] = $general['title'];
        $general['kode_menu'] = 55;
        $general['service_id'] = $id;

        $teknisi = User::where('hak_akses_id', 3)->where('is_aktif', 1)->get();
        return view('pages.transaksi.servis_pilih_teknisi', compact('general', 'teknisi'));
    }

    public function update_teknisi()
    {
        $service_order_id = Input::get('service_order_id');
        $teknisi_id = Input::get('teknisi_id');

        ServiceOrder::where('id', $service_order_id)->update(
            array(
                'teknisi_id' => $teknisi_id,
                'status_service' => 'Proses',
                'tanggal_diperbaiki' => date('Y-m-d H:i:s')
            )
        );

        $model_produk = ServiceOrder::where('id', $service_order_id)->value('model_produk');

        //notifikasi untuk teknisi
        $notifikasi = new Notifikasi;

        $notifikasi->kode                   = 'servis_order';
        $notifikasi->pesan                  = '<i class="fa fa-cog"> Segera cek perbaikan pada '.$model_produk.'</i>';
        $notifikasi->hak_akses_id_target    = 3;
        $notifikasi->link                   = "teknisi/produk_sedang_servis";
        $notifikasi->is_aktif               = 1;
        $notifikasi->save();

        return redirect('/servis');
    }

    public function servis_harga($id)
    {
        $service_id = $id;

        $general['title'] = "Biaya Servis";
        $general['menu1'] = "Transaksi";
        $general['menu2'] = "Biaya Servis";
        $general['menu3'] = $general['title'];
        $general['kode_menu'] = 55;

        $data['service'] = ServiceOrder::with('pelanggan')->where('id', $service_id)->get();
        $data['invoice_header'] = InvoiceHeader::where('service_order_id', $service_id)->get();
        $data['invoice_header_id'] = InvoiceHeader::where('service_order_id', $service_id)->value('id');
        $data['catatan'] = InvoiceHeader::where('service_order_id', $service_id)->value('catatan');
        $data['invoice_detail'] = InvoiceDetail::where('service_invoice_header_id', $data['invoice_header_id'])->get();

        return view('pages.transaksi.servis_biaya', compact('general', 'data'));
    }

    public function servis_harga_submit()
    {
        $general['title'] = "Biaya Servis";
        $general['menu1'] = "Transaksi";
        $general['menu2'] = "Biaya Servis";
        $general['menu3'] = $general['title'];
        $general['kode_menu'] = 55;

        $service_id = Input::get('id_service');
        $invoice_header_id = InvoiceHeader::where('service_order_id', $service_id)->value('id');
        $invoice_detail = InvoiceDetail::where('service_invoice_header_id', $invoice_header_id)->get();
        $biaya_total = 0;
        foreach ($invoice_detail as $key => $value) {
            $invoice_detail_id = $value->id;
            $biaya = Input::get('biaya_invoice_detail_' . ++$key);
            InvoiceDetail::where('id', $invoice_detail_id)->update(
                array(
                    'biaya' => $biaya
                )
            );
            $biaya_total = $biaya + $biaya_total;
        }

        InvoiceHeader::where('id', $invoice_header_id)->update(
            array(
                'harga_total' => $biaya_total
            )
        );

        $model_produk = ServiceOrder::where('id', $service_id)->value('model_produk');

        //notifikasi untuk teknisi
        $notifikasi = new Notifikasi;

        $notifikasi->kode                   = 'servis_order';
        $notifikasi->pesan                  = '<i class="fa fa-cog"> Segera lakukan perbaikan pada '.$model_produk.'</i>';
        $notifikasi->hak_akses_id_target    = 3;
        $notifikasi->link                   = "teknisi/produk_sedang_servis";
        $notifikasi->is_aktif               = 1;
        $notifikasi->save();

        //Do send mail to customer
        $this->sendmail('confirm', $service_id, $invoice_header_id);

        return redirect('/nota_service_order/' . $service_id);
    }

    public function servis_ambil()
    {
        $service_id = Input::get('service');
        $pelanggan_id = Input::get('customer');
        $kode_verifikasi = Input::get('verification_code');
        $status = Input::get('status');

        if ($status == 'get_device') {
            //check at service order table
            $checkService = ServiceOrder::where('id', $service_id)->where('pelanggan_id', $pelanggan_id)->where('kode_verifikasi', $kode_verifikasi)->count();
            if ($checkService == 0 || $checkService == NULL) {
                //Tidak ada data
            } else {
                $service = ServiceOrder::where('id', $service_id)->where('pelanggan_id', $pelanggan_id)->where('kode_verifikasi', $kode_verifikasi)->first();
                $invoice_header_id = InvoiceHeader::where('service_order_id', $service_id)->value('id');
                $kode_verifikasi_ambil_barang = md5($service->kode_verifikasi . date('Y-m-d H:i:s'));
                ServiceOrder::where('id', $service_id)->update(
                    array(
                        'kode_verifikasi_ambil_barang' => $kode_verifikasi_ambil_barang
                    )
                );
                //kirim kode verifikasi ke email pelanggan
                $this->sendmail('get_device', $service_id, $invoice_header_id);
            }
        }
        return redirect('/servis');
    }

    //Untuk menampilkan halaman konfirmasi servis dan submit jawaban
    public function servis_confirm()
    {
        $aksi = Input::get('action');
        $no_nota = Input::get('nota');
        $kode_verifikasi = Input::get('verification_code');
        $kode_verifikasi_ambil = Input::get('verification_code_get_device');

        $data['service'] = ServiceOrder::where('no_nota', $no_nota)->where('kode_verifikasi', $kode_verifikasi)->first();
        if ($data['service'] == NULL) {
            //Data tidak ditemukan
            $data['status'] = '404';
        } else {
            $data['service']['id'] = $data['service']->id;
            $data['service']['pelanggan_id'] = $data['service']->pelanggan_id;
            $data['service']['flag_acc'] = $data['service']->flag_acc;
            $data['service']['status'] = $data['service']->status_service;
            $data['pelanggan'] = Pelanggan::where('id', $data['service']['pelanggan_id'])->first();
            $data['count_invoice_header'] = 0;
            $data['count_invoice_header'] = InvoiceHeader::where('service_order_id', $data['service']['id'])->count();
            if ($data['count_invoice_header'] !== 0) {
                $data['invoice_header'] = InvoiceHeader::where('service_order_id', $data['service']['id'])->first();
                $data['invoice_header']['id'] = $data['invoice_header']->id;
                $data['invoice_header']['biaya'] = $data['invoice_header']->harga_total;
                $data['invoice_header']['is_submit'] = $data['invoice_header']->is_submit;
                $data['invoice_header']['is_repair'] = $data['invoice_header']->is_repair;
            }
            //Mulai pengecekan
            if (strtolower($aksi) == 'check') {
                if (strtolower($data['service']['status']) == 'belum') {
                    $data['status'] = 'check';
                    $data['invoice_header'] = InvoiceHeader::where('service_order_id', $data['service']['id'])->first();
                } elseif (strtolower($data['service']['status']) == 'proses' || strtolower($data['service']['status']) == 'selesai' || strtolower($data['service']['status']) == 'batal') {
                    $param = array(
                        'action' => 'info',
                        'nota' => $no_nota,
                        'verification_code' => $kode_verifikasi
                    );
                    return redirect('/service_confirm?' . http_build_query($param));
                }
            } elseif (strtolower($aksi) == 'confirm') {
                if (strtolower($data['service']['status']) == 'belum' || strtolower($data['service']['status']) == 'selesai' || strtolower($data['service']['status']) == 'batal') {
                    $param = array(
                        'action' => 'info',
                        'nota' => $no_nota,
                        'verification_code' => $kode_verifikasi
                    );
                    return redirect('/service_confirm?' . http_build_query($param));
                } elseif (strtolower($data['service']['status']) == 'proses') {
                    if ($data['service']['flag_acc'] == 1) {
                        $param = array(
                            'action' => 'info',
                            'nota' => $no_nota,
                            'verification_code' => $kode_verifikasi
                        );
                        return redirect('/service_confirm?' . http_build_query($param));
                    } else {
                        ServiceOrder::where('no_nota', $no_nota)->where('kode_verifikasi', $kode_verifikasi)->update(
                            array(
                                'tanggal_cek' => date('Y-m-d H:i:s'),
                                'is_check' => 1
                            )
                        );
                        $data['invoice_header'] = InvoiceHeader::where('service_order_id', $data['service']['id'])->first();
                        $data['invoice_detail'] = InvoiceDetail::where('service_invoice_header_id', $data['invoice_header']->id)->get();
                        $data['status'] = 'confirm';
                    }
                }
            } elseif (strtolower($aksi) == 'verify') {
                if (strtolower($data['service']['status']) == 'belum' || strtolower($data['service']['status']) == 'selesai') {
                    $param = array(
                        'action' => 'info',
                        'nota' => $no_nota,
                        'verification_code' => $kode_verifikasi
                    );
                    return redirect('/service_confirm?' . http_build_query($param));
                } elseif (strtolower($data['service']['status']) == 'proses') {
                    if($data['service']['flag_acc'] == 0){
                        $this->sendmail('info', $data['service']['id'], $data['invoice_header']['id']);
                        ServiceOrder::where('no_nota', $no_nota)->where('kode_verifikasi', $kode_verifikasi)->update(
                            array(
                                'tanggal_cek' => date('Y-m-d H:i:s'),
                                'tanggal_acc' => date('Y-m-d H:i:s'),
                                'is_check' => 1,
                                'flag_acc' => 1
                            )
                        );
                    }
                    $param = array(
                        'action' => 'info',
                        'nota' => $no_nota,
                        'verification_code' => $kode_verifikasi
                    );
                    return redirect('/service_confirm?' . http_build_query($param));
                } elseif (strtolower($data['service']['status']) == 'batal') {
                    $param = array(
                        'action' => 'cancel',
                        'nota' => $no_nota,
                        'verification_code' => $kode_verifikasi
                    );
                    return redirect('/service_confirm?' . http_build_query($param));
                }
            } elseif (strtolower($aksi) == 'info') {
                if (strtolower($data['service']['status']) == 'belum') {
                    $param = array(
                        'action' => 'check',
                        'nota' => $no_nota,
                        'verification_code' => $kode_verifikasi
                    );
                    return redirect('/service_confirm?' . http_build_query($param));
                } elseif (strtolower($data['service']['status']) == 'proses') {
                    if ($data['count_invoice_header'] == 0) {
                        $data['status'] = 'info';
                        $data['invoice_header'] = InvoiceHeader::where('service_order_id', $data['service']['id'])->first();
                    } else {
                        if ($data['invoice_header']['is_submit'] == 1) {
                            if ($data['invoice_header']['biaya'] == NULL || $data['invoice_header']['biaya'] == 0) {
                                $data['status'] = 'info';
                                $data['invoice_header'] = InvoiceHeader::where('service_order_id', $data['service']['id'])->first();
                                $data['invoice_detail'] = InvoiceDetail::where('service_invoice_header_id', $data['invoice_header']->id)->get();
                            } else {
                                if ($data['service']['flag_acc'] == 0) {
                                    $param = array(
                                        'action' => 'confirm',
                                        'nota' => $no_nota,
                                        'verification_code' => $kode_verifikasi
                                    );
                                    return redirect('/service_confirm?' . http_build_query($param));
                                } else {
                                    $data['status'] = 'info';
                                    $data['invoice_header'] = InvoiceHeader::where('service_order_id', $data['service']['id'])->first();
                                    $data['invoice_detail'] = InvoiceDetail::where('service_invoice_header_id', $data['invoice_header']->id)->get();
                                }
                            }
                        } else {
                            $data['status'] = 'info';
                            $data['invoice_header'] = InvoiceHeader::where('service_order_id', $data['service']['id'])->first();
                            $data['invoice_detail'] = InvoiceDetail::where('service_invoice_header_id', $data['invoice_header']->id)->get();
                        }
                    }
                } elseif (strtolower($data['service']['status']) == 'selesai') {
                    $data['status'] = 'get_device';
                    $data['invoice_header'] = InvoiceHeader::where('service_order_id', $data['service']['id'])->first();
                    $data['invoice_detail'] = InvoiceDetail::where('service_invoice_header_id', $data['invoice_header']->id)->get();
                } elseif (strtolower($data['service']['status']) == 'batal') {
                    $data['status'] = 'cancel';
                    $data['invoice_header'] = InvoiceHeader::where('service_order_id', $data['service']['id'])->first();
                }
            } elseif (strtolower($aksi) == 'cancel') {
//                if (strtolower($data['service']['status']) == 'belum' || strtolower($data['service']['status']) == 'selesai' || strtolower($data['service']['status']) == 'batal') {
//                    $param = array(
//                        'action' => 'info',
//                        'nota' => $no_nota,
//                        'verification_code' => $kode_verifikasi
//                    );
//                    return redirect('/service_confirm?' . http_build_query($param));
//                } elseif (strtolower($data['service']['status']) == 'proses') {
                    if ($data['service']['flag_acc'] == 0) {
                        if (strtolower($data['service']['status']) == 'batal') {
                            //servis telah dibatalkan sebelumnya
                        } else {
                            $this->sendmail('cancel', $data['service']['id'], NULL);
                            ServiceOrder::where('no_nota', $no_nota)->where('kode_verifikasi', $kode_verifikasi)->update(
                                array(
                                    'is_check' => 1,
                                    'flag_case' => 1,
                                    'status_service' => 'Batal'
                                )
                            );
                        }
                        $data['status'] = 'cancel';
                        $data['invoice_header'] = InvoiceHeader::where('service_order_id', $data['service']['id'])->first();
//                        $data['invoice_detail'] = InvoiceDetail::where('service_invoice_header_id', $data['invoice_header']->id)->get();
                    } else {
                        $param = array(
                            'action' => 'check',
                            'nota' => $no_nota,
                            'verification_code' => $kode_verifikasi
                        );
                        return redirect('/service_confirm?' . http_build_query($param));
                    }
//                }
            } elseif (strtolower($aksi) == 'get_device') {
                if (strtolower($data['service']['status']) == 'belum' || strtolower($data['service']['status']) == 'proses') {
                    $param = array(
                        'action' => 'info',
                        'nota' => $no_nota,
                        'verification_code' => $kode_verifikasi
                    );
                    return redirect('/service_confirm?' . http_build_query($param));
                } elseif (strtolower($data['service']['status']) == 'selesai') {
                    $checkService = ServiceOrder::where('id', $data['service']['id'])->where('kode_verifikasi', $kode_verifikasi)->where('kode_verifikasi_ambil_barang', $kode_verifikasi_ambil)->count();
                    if ($checkService == 0 || $checkService == NULL) {
                        $param = array(
                            'action' => 'info',
                            'nota' => $no_nota,
                            'verification_code' => $kode_verifikasi
                        );
                        return redirect('/service_confirm?' . http_build_query($param));
                    } else {
                        ServiceOrder::where('id', $data['service']['id'])->update(
                            array(
                                'tanggal_diambil' => date('Y-m-d H:i:s'),
                                'garansi_service' => date('Y-m-d H:i:s', strtotime("+7 day")),
                                'status_service' => 'Selesai',
                                'is_lunas' => 1,
                                'flag_case' => 1
                            )
                        );
                        $data['invoice_header'] = InvoiceHeader::where('service_order_id', $data['service']['id'])->first();
                        $data['invoice_detail'] = InvoiceDetail::where('service_invoice_header_id', $data['invoice_header']->id)->get();
                        $data['status'] = 'get_device';
                    }
                } elseif (strtolower($data['service']['status']) == 'batal') {
                    $param = array(
                        'action' => 'cancel',
                        'nota' => $no_nota,
                        'verification_code' => $kode_verifikasi
                    );
                    return redirect('/service_confirm?' . http_build_query($param));
                }
            }
        }
//        return var_dump($data);
        return view('pages.transaksi.servis_konfirmasi', compact('data'));
    }

    public function sendmail($action, $service_id, $invoice_header_id = NULL)
    {
        /*
         * Action :
         *  - check
         *  - confirm
         *  - info
         *  - cancel
         *  - done
         *  - get_device
         */

        $pelanggan_id = ServiceOrder::where('id', $service_id)->value('pelanggan_id');
        $get_pelanggan = Pelanggan::where('id', $pelanggan_id)->first();
        $data['pelanggan_nama'] = $get_pelanggan->nama;
        $data['pelanggan_email'] = $get_pelanggan->email;

        $mail = new PHPMailer;

        //Enable SMTP debugging.
        $mail->SMTPDebug = 3;
        //Set PHPMailer to use SMTP.
        $mail->isSMTP();
        //Set SMTP host name
        $mail->Host = "smtp.gmail.com";
        //Set this to true if SMTP host requires authentication to send email
        $mail->SMTPAuth = true;
        //Provide username and password
        $mail->Username = "fendi.septiawan0709@gmail.com";
        $mail->Password = "vkizffivxocftlfd";
        //If SMTP requires TLS encryption then set it
        $mail->SMTPSecure = "tls";
        //Set TCP port to connect to
        $mail->Port = 587;

        $mail->From = "admin@galerindoteknologi.com";
        $mail->FromName = "Admin Galerindo Teknologi";

        $mail->addAddress($data['pelanggan_email'], $data['pelanggan_nama']);

        $mail->isHTML(true);

        $get_service = ServiceOrder::where('id', $service_id)->first();
        $count_invoice = InvoiceHeader::where('id', $invoice_header_id)->count();
        if ($count_invoice == 0) {
            //Data tindakan tidak ada
        } else {
            $get_invoice = InvoiceHeader::where('id', $invoice_header_id)->first();
            $get_invoice_detail = InvoiceDetail::where('service_invoice_header_id', $get_invoice->id)->get();
        }

        if ($action == 'check') {
            $subject = "Galerindo Teknologi - Notifikasi Perbaikan Perangkat";
            $link = url('/service_confirm?') . http_build_query(array(
                    'action' => 'check',
                    'nota' => $get_service->no_nota,
                    'verification_code' => $get_service->kode_verifikasi
                ));
            $body = "
            <html>
                <head>
                    <meta charset=\"utf-8\" />
                    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\">
                    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\">
                    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css\">
                    <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>
                    <title>Galerindo Teknologi - Notifikasi Perbaikan Perangkat</title>
                </head>
                <body>
                    <h3>Galerindo Teknologi - Notifikasi Perbaikan Perangkat</h3>
                    <p>Perangkat Anda saat ini sedang kami lakukan pengecekan untuk tindakan lebih lanjut.</p>
                    <table>
                        <tr>
                            <td style='width: 30%;'>No. Nota</td>
                            <td style='width: 70%;'>" . $get_service->no_nota . "</td>
                        </tr>
                        <tr>
                            <td>Tanggal</td>
                            <td>" . $get_service->tanggal_diterima . "</td>
                        </tr>
                        <tr>
                            <td>Model Perangkat</td>
                            <td>" . $get_service->model_produk . "</td>
                        </tr>
                        <tr>
                            <td>No. Serial Produk</td>
                            <td>" . $get_service->serial_number . "</td>
                        </tr>
                        <tr>
                            <td>Keluhan Pelanggan</td>
                            <td>" . $get_service->keluhan_pelanggan . "</td>
                        </tr>
                    </table>
                    <p style='font-size: 16px'>Anda dapat memantau perbaikan perangkat Anda melalui tautan berikut,<br/> <a href='" . $link . "' target='_blank'>" . $link . "</a></p>
                </body>
            </html>
            ";
            $mail->AltBody = nl2br(strip_tags($body));
        } elseif ($action == 'confirm') {
            $subject = "Galerindo Teknologi - Konfirmasi Perbaikan Perangkat";
            $link = url('/service_confirm?') . http_build_query(array(
                    'action' => 'confirm',
                    'nota' => $get_service->no_nota,
                    'verification_code' => $get_service->kode_verifikasi
                ));
            $body = "
            <html>
                <head>
                    <meta charset=\"utf-8\" />
                    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\">
                    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\">
                    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css\">
                    <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>
                    <title>Galerindo Teknologi - Konfirmasi Perbaikan Perangkat</title>
                </head>
                <body>
                    <h3>Galerindo Teknologi - Konfirmasi Perbaikan Perangkat</h3>
                    <p>Perangkat Anda saat ini telah kami lakukan pengecekan.<br/>Berikut data perangkat Anda serta kerusakannya.</p>
                    <table>
                        <tr>
                            <td style='width: 30%;'>No. Nota</td>
                            <td style='width: 70%;'>" . $get_service->no_nota . "</td>
                        </tr>
                        <tr>
                            <td>Tanggal</td>
                            <td>" . $get_service->tanggal_diterima . "</td>
                        </tr>
                        <tr>
                            <td>Model Perangkat</td>
                            <td>" . $get_service->model_produk . "</td>
                        </tr>
                        <tr>
                            <td>No. Serial Produk</td>
                            <td>" . $get_service->serial_number . "</td>
                        </tr>
                        <tr>
                            <td>Keluhan Pelanggan</td>
                            <td>" . $get_service->keluhan_pelanggan . "</td>
                        </tr>
                    </table>
                    <table class='table table-stripped'>
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Tindakan</th>
                                <th>Biaya</th>
                            </tr>
                        </thead>
                        <tbody>";
            foreach ($get_invoice_detail as $key => $value) {
                $body = "       <tr>
                                <td>" . ++$key . "</td>
                                <td>" . $value->tindakan . "</td>
                                <td>" . $value->biaya . "</td>
                            </tr>";
            }
            $body .= "
                            <tr style='font-size: 18px'>
                                <td colspan='2' style='text-align: right'>Biaya Total</td>
                                <td>Rp. " . number_format($get_invoice->harga_total, 0, ',', '.') . "</td>
                            </tr>
                        </tbody>
                    </table>
                    <p style='font-size: 20px'>Anda dapat memberikan konfirmasi (Ya / Tidak) perbaikan perangkat Anda melalui tautan berikut,<br/> <a href='" . $link . "' target='_blank'>" . $link . "</a></p>
                </body>
            </html>";
        } elseif ($action == 'info') {
            $subject = "Galerindo Teknologi - Informasi Perbaikan Perangkat";
            $link = url('/service_confirm?') . http_build_query(array(
                    'action' => 'info',
                    'nota' => $get_service->no_nota,
                    'verification_code' => $get_service->kode_verifikasi
                ));
            $body = "
            <html>
                <head>
                    <meta charset=\"utf-8\" />
                    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\">
                    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\">
                    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css\">
                    <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>
                    <title>Galerindo Teknologi - Informasi Perbaikan Perangkat</title>
                </head>
                <body>
                    <h3>Galerindo Teknologi - Informasi Perbaikan Perangkat</h3>
                    <p>Perangkat Anda saat ini telah kami lakukan pengecekan.<br/>Berikut data perangkat Anda serta kerusakannya.</p>
                    <table>
                        <tr>
                            <td style='width: 30%;'>No. Nota</td>
                            <td style='width: 70%;'>" . $get_service->no_nota . "</td>
                        </tr>
                        <tr>
                            <td>Tanggal</td>
                            <td>" . $get_service->tanggal_diterima . "</td>
                        </tr>
                        <tr>
                            <td>Model Perangkat</td>
                            <td>" . $get_service->model_produk . "</td>
                        </tr>
                        <tr>
                            <td>No. Serial Produk</td>
                            <td>" . $get_service->serial_number . "</td>
                        </tr>
                        <tr>
                            <td>Keluhan Pelanggan</td>
                            <td>" . $get_service->keluhan_pelanggan . "</td>
                        </tr>
                    </table>
                    <table>
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Tindakan</th>
                                <th>Biaya</th>
                            </tr>
                        </thead>
                        <tbody>";
            foreach ($get_invoice_detail as $key => $value) {
                $body = "       <tr>
                                <td>" . ++$key . "</td>
                                <td>" . $value->tindakan . "</td>
                                <td>" . $value->biaya . "</td>
                            </tr>";
            }
            $body .= "
                            <tr style='font-size: 18px'>
                                <td colspan='2' style='text-align: right'>Biaya Total</td>
                                <td>Rp. " . number_format($get_invoice->harga_total, 0, ',', '.') . "</td>
                            </tr>
                        </tbody>
                    </table>
                    <p style='font-size: 20px'>Anda telah menyetujui perbaikan perangkat. Untuk memantau status perbaikan perangkat Anda, klik tautan berikut,<br/> <a href='" . $link . "' target='_blank'>" . $link . "</a></p>
                </body>
            </html>";
        } elseif ($action == 'cancel') {
            $subject = "Galerindo Teknologi - Pembatalan Perbaikan Perangkat";
            $body = "
            <html>
                <head>
                    <meta charset=\"utf-8\" />
                    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\">
                    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\">
                    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css\">
                    <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>
                    <title>Galerindo Teknologi - Pembatalan Perbaikan Perangkat</title>
                </head>
                <body>
                    <h3>Galerindo Teknologi - Pembatalan Perbaikan Perangkat</h3>
                    <p>Perbaikan perangkat Anda telah kami batalkan sesuai permintaan Anda.</p>
                    <p>Anda dapat mengambil perangkat Anda pada Galerindo Teknologi sesuai jam kerja.</p>
                </body>
            </html>";
        } elseif ($action == 'done') {
            $subject = "Galerindo Teknologi - Informasi Perbaikan Perangkat Selesai";
            $body = "
            <html>
                <head>
                    <meta charset=\"utf-8\" />
                    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\">
                    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\">
                    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css\">
                    <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>
                    <title>Galerindo Teknologi - Informasi Perbaikan Perangkat Selesai</title>
                </head>
                <body>
                    <h3>Galerindo Teknologi - Informasi Perbaikan Perangkat Selesai</h3>
                    <p>Perbaikan perangkat Anda telah kami kerjakan." . $pelanggan_email . "</p>
                    <p>Anda dapat mengambil perangkat Anda pada Galerindo Teknologi sesuai jam kerja.</p>
                </body>
            </html>";
        } elseif ($action == 'get_device') {
            $link = url('/service_confirm?') . http_build_query(array(
                    'action' => 'get_device',
                    'nota' => $get_service->no_nota,
                    'verification_code' => $get_service->kode_verifikasi,
                    'verification_code_get_device' => $get_service->kode_verifikasi_ambil_barang
                ));
            $subject = "Galerindo Teknologi - Konfirmasi Pengambilan Perangkat";
            $body = "
            <html>
                <head>
                    <meta charset=\"utf-8\" />
                    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\">
                    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\">
                    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css\">
                    <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>
                    <title>Galerindo Teknologi - Konfirmasi Pengambilan Perangkat</title>
                </head>
                <body>
                    <h3>Galerindo Teknologi - Konfirmasi Pengambilan Perangkat</h3>
                    <p>Kami telah menandai bahwa Anda ingin mengambil perangkat Anda.</p>
                    <p style='font-size: 18px;'>Jika memang Anda ingin mengambil barang Anda, klik link berikut,<br/><a href='" . $link . "' target='_blank'>" . $link . " </a>.</p>
                    <p>Jika tidak, Anda dapat mengabaikan email ini.</p>
                </body>
            </html>";
        }
        $mail->Subject = $subject;
        $mail->Body = $body;

        return $mail->send();
    }

    public function cek_email()
    {

        return $this->sendmail('info', 2, 2);

        $data['status'] = 'check';
        return view('pages.transaksi.servis_konfirmasi', compact('data'));

        $mail = new PHPMailer;

        //Enable SMTP debugging.
        $mail->SMTPDebug = 3;
        //Set PHPMailer to use SMTP.
        $mail->isSMTP();
        //Set SMTP host name
        $mail->Host = "smtp.gmail.com";
        //Set this to true if SMTP host requires authentication to send email
        $mail->SMTPAuth = true;
        //Provide username and password
        $mail->Username = "fendi.septiawan0709@gmail.com";
        $mail->Password = "vkizffivxocftlfd";
        //If SMTP requires TLS encryption then set it
        $mail->SMTPSecure = "tls";
        //Set TCP port to connect to
        $mail->Port = 587;

        $mail->From = "admin@galerindoteknologi.com";
        $mail->FromName = "Admin Galerindo Teknologi";

        $mail->addAddress("fendi_septiawan0709@yahoo.co.id", "Recipient");

        $mail->isHTML(true);

        $mail->Subject = "Informasi Perbaikan Perangkat";
        $mail->Body = "<i>Mail body in HTML</i>";
        $mail->AltBody = "This is the plain text version of the email content";

        if (!$mail->send()) {
            echo "Mailer Error: " . $mail->ErrorInfo;
        } else {
            echo "Message has been sent successfully";
        }


//        $data = array(
//            'name' => "Fendi Septiawan"
//        );
//        $mail = Mail::send('welcome', $data, function ($message) {
//            $message->to('fendi.septiawan0709.02@gmail.com', 'Tutorials Point')
//                ->subject('Laravel HTML Testing Mail')
//                ->from('fendi.septiawan0709@gmail.com', 'Fendi Septiawan');
//        });
//        if (!$mail) {
//            echo 'Success';
//        } else {
//            echo 'Failed';
//        }
    }

}
