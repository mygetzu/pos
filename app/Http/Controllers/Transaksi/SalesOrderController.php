<?php

namespace App\Http\Controllers\Transaksi;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Gloudemans\Shoppingcart\Facades\Cart;
use Input;
use Auth;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Session;
use Promocodes;
use Response;
use DateTime;
use PDF;
use Hashids\Hashids;
use Validator;
use Illuminate\Http\RedirectResponse;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use App\User;
use App\Models\Produk\Produk;
use App\Models\Produk\KategoriProduk;
use App\Models\Produk\Hadiah;
use App\Models\Produk\ProdukSerialNumber;
use App\Models\Pelanggan\Pelanggan;
use App\Models\Referensi\Provinsi;
use App\Models\PaketProduk\Paket;
use App\Models\Perusahaan;
use App\Models\Gudang\Gudang;
use App\Models\Gudang\ProdukGudang;
use App\Models\Gudang\HadiahGudang;
use App\Models\Gudang\PaketGudang;
use App\Models\Pengaturan\PengaturanSO;
use App\Models\TransaksiPenjualan\InvoicePenjualan;
use App\Models\TransaksiPenjualan\BayarInvoicePenjualan;
use App\Models\TransaksiPenjualan\SOHeader;
use App\Models\TransaksiPenjualan\SODetail;
use App\Models\TransaksiPenjualan\SOVoucher;
use App\Models\TransaksiPenjualan\SOAlamatPengiriman;
use App\Models\AdminEcommerce\Konten;

class SalesOrderController extends Controller
{
    public function sales_order()
    {
        $general['title']       = "Sales Order";
        $general['menu1']       = "Transaksi";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 52;

        $so_header      = SOHeader::with('pelanggan')->orderBy('tanggal', 'desc')->get();

        return view('pages.transaksi.sales_order', compact('general', 'so_header'));
    }

    public function tambah_sales_order(Request $request)
    {
        $general['title']       = "Tambah Sales Order";
        $general['menu1']       = "Transaksi";
        $general['menu2']       = "Sales Order";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 52;

        $provinsi           = Provinsi::orderBy('nama')->get();
        $kategori_produk    = KategoriProduk::where('is_aktif')->get();

        $pelanggan = Pelanggan::with('kategori_pelanggan')->get();

        Cart::destroy();
        
        date_default_timezone_set("Asia/Jakarta");
        $tanggal        = new DateTime;

        $nomor_invoice  = DB::table('tref_nomor_invoice')->where('id', 5)->first();
        $format_invoice = $nomor_invoice->format;
        $ori_format_invoice = $format_invoice;

        $cek_dd = strpos($format_invoice, 'DD');
        if(!empty($cek_dd))
        {
            $format_invoice = str_replace('DD', $tanggal->format('d'), $format_invoice);
        }

        $cek_mm = strpos($format_invoice, 'MM');
        if(!empty($cek_mm))
        {
            $format_invoice = str_replace('MM', $tanggal->format('m'), $format_invoice);
        }

        $cek_yyyy = strpos($format_invoice, 'YYYY');
        if(!empty($cek_yyyy))
        {
            $format_invoice = str_replace('YYYY', $tanggal->format('Y'), $format_invoice);
        }

        $cek_yy = strpos($format_invoice, 'YY');
        if(!empty($cek_yy))
        {
            $format_invoice = str_replace('YY', $tanggal->format('y'), $format_invoice);
        }

        //cek maksimal 10
        $number = '##########';

        for ($i=9; $i > 0; $i--) { 
            $number     = substr($number, 0, $i);
            $cek_number = strpos($format_invoice, $number);
            $nol        = "";
            if(!empty($cek_number))
            {
                $so_header      = SOHeader::orderBy('id', 'desc')->first();
                if(empty($so_header))
                    $get_num = 1;
                else
                    $get_num        = (int)$so_header->id + 1;

                for ($j=0; $j < $i - strlen($get_num); $j++) { 
                    $nol = $nol."0";
                }

                $get_num = $nol.$get_num;
                $format_invoice = str_replace($number, $get_num, $format_invoice);
                break;
            }
        }

        $no_invoice     = $format_invoice;
        $pengaturan_so  = PengaturanSO::first();
        // return Response::json($pelanggan);
        return view('pages.transaksi.tambah_sales_order', compact('general', 'pelanggan', 'kategori_produk', 'provinsi', 'format_invoice', 'no_invoice', 'ori_format_invoice', 'pengaturan_so'));
    }

    public function get_so_produk(Request $request)
    {
        $id         = Input::get('id');

        $kategori_pelanggan_id  = 0;

        if(Auth::check()){
            $pelanggan              = Pelanggan::with('kategori_pelanggan')->where('id', $id)->first();
            if(!empty($pelanggan))
                $kategori_pelanggan_id  = $pelanggan->kategori_id;
        }

        $produk = Produk::with(['produk_galeri', 'promo_diskon', 'promo_cashback', 'promo_hadiah' => function($query) 
            {return $query->with('hadiah');}, 'produk_harga' 
            => function ($query) use ($kategori_pelanggan_id) 
            {$query->where('kategori_pelanggan_id', $kategori_pelanggan_id); }])
            ->where('is_aktif', 1)->orderBy('nama', 'asc')->get();

        $obj_gambar_produk_default   = Konten::where('nama', 'gambar_produk_default')->first();
        if(empty($obj_gambar_produk_default)){
            $gambar_produk_default = "";
        }
        else{
            $gambar_produk_default  = $obj_gambar_produk_default->keterangan;
        }

        return view('pages.transaksi.ajax_so_produk', compact('produk', 'pelanggan', 'gambar_produk_default'));
    }

    public function tambah_pelanggan_baru(Request $request)
    {
        //data pelanggan baru insert ke user dan customer
        $validator = Validator::make($request->all(), [
            'nama' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'alamat' => 'required|min:4',
            'provinsi' => 'required',
            'kota_id' => 'required',
        ]);

        if ($validator->fails()) {

            return redirect('/tambah_sales_order')->withErrors($validator);
        }

        $nama       = ucwords(Input::get('nama'));
        $email      = Input::get('email');
        $alamat     = Input::get('alamat');
        $kota_id    = Input::get('kota_id');
        $hp         = Input::get('hp');

        //Auth::guard($this->create($request->all()));
        $user_id = DB::table('users')->insertGetId(['name' => $nama, 'email' => $email, 'password' => bcrypt(123)]);
        //get id
        $user = DB::table('users')->where('name', $nama)->where('email', $email)->first();
        DB::table('users')->where('id', $user->id)->update(['alamat' => $alamat, 'kota_id' => $kota_id, 'hp' => $hp, 'hak_akses_id' => '2', 'is_aktif' => '1']);

        $user = DB::table('users')->where('id', $user->id)->first();

        //tambahkan juga di tmst_pelanggan
        DB::table('tmst_pelanggan')->insert(['user_id' => $user_id, 'nama' => $nama, 'email' => $email, 'alamat' => $alamat, 'kota_id' => $kota_id, 'hp_sales' => $hp]);

        $request->session()->flash('message', 'Pelanggan baru berhasil ditambahkan');
        return redirect('/tambah_sales_order');
    }

    public function ubah_format_no_nota_sales_order(Request $request)
    {
        $format = strtoupper(Input::get('format'));

        DB::table('tref_nomor_invoice')->where('id', 6)->update(['format' => $format]);

        $request->session()->flash('message', 'Format No. Invoice berhasil diperbarui');
        return redirect('/tambah_sales_order');
    }

    public function so_proses()
    {
        //output berupa invoice SO. Nomor invoice SO disimpan di No_nota so_header dan invoice detail
        $no_nota        = Input::get('no_nota');
        $catatan        = Input::get('catatan');
        $pelanggan_id   = Input::get('pelanggan_id');
        $tunai          = Input::get('tunai');
        $k_debit        = Input::get('k_debit');
        $k_kredit       = Input::get('k_kredit');
        $ppn            = Input::get('ppn');

        if(empty($ppn)){
            $ppn = 0;
        }

        $tunai      = str_replace('.', '', $tunai);
        $k_debit    = str_replace('.', '', $k_debit);
        $k_kredit   = str_replace('.', '', $k_kredit);

        date_default_timezone_set("Asia/Jakarta");
        $tanggal        = new DateTime;

        /* SO HEADER */

        $so_header = new SOHeader;
        $so_header->no_sales_order          = $no_nota;
        $so_header->pelanggan_id            = $pelanggan_id;
        $so_header->tanggal                 = $tanggal;
        $so_header->catatan                 = $catatan;
        $so_header->flag_sj_keluar          = 0;
        $so_header->ppn                     = $ppn;
        $so_header->save();

        $so_header_id = $so_header->id;

        /* SO DETAIL */

        $cart_content       = Cart::content(1);
        $total_harga        = 0;
        $array_produk_id    = []; //digunakan untuk mengumpulkan produk id
        $indeks             = 0;

        foreach ($cart_content as $cart) {
            $id         = $cart->id;
            $so_detail  = new SODetail;

            $so_detail->so_header_id     = $so_header_id;

            //cek apakah dia paket, hadiah atau produk
            //ada 3 macam jenis_barang dapat dilihat ditabel tref_jenis_barang 
            //1. produk, 3. paket produk
            if(substr($id, 0, 3) == 'PKT'){
                $paket_id                   = substr($id, 3);
                $so_detail->produk_id       = $paket_id;
                $so_detail->jumlah          = $cart->qty;
                $so_detail->harga           = $cart->price;
                $so_detail->jenis_barang_id = 3; //id untuk jenis barang paket

                $total_harga = $total_harga+($cart->qty*$cart->price);

                $paket = Paket::where('id', $paket_id)->first();

                $so_detail->deskripsi       = $paket->nama;
                $so_detail->harga_retail    = $paket->harga_total;

                //di tabel paket, tambah nilai stok dipesan
                $paket->stok_dipesan = $paket->stok_dipesan + $cart->qty;
                $paket->save();
            }
            else{
                if(substr($id, 0, 3) == 'DIS' || substr($id, 0, 3) == 'CAS'){
                    $produk_id = substr($id, 3);
                }
                else{
                    $produk_id = $id;
                }

                $so_detail->produk_id       = $produk_id;
                $so_detail->jumlah          = $cart->qty;
                $so_detail->harga           = $cart->price;
                $so_detail->jenis_barang_id = 1; //id untuk jenis barang produk

                $total_harga = $total_harga+($cart->qty*$cart->price);

                $produk = Produk::where('id', $produk_id)->first();

                $so_detail->deskripsi       = $produk->deskripsi;
                $so_detail->harga_retail    = $produk->harga_retail;

                //di tabel produk, tambah nilai stok dipesan
                $produk->stok_dipesan = $produk->stok_dipesan + $cart->qty;
                $produk->save();

                //cek
                $cek_array = 0;
                foreach ($array_produk_id as $key => $value) {
                    if($value['id'] == $produk_id){
                        $value['quantity'] = $value['quantity'] + $cart->qty;
                        $cek_array = 1;
                    }
                }

                if($cek_array == 0){
                    $array_produk_id[$indeks]['id']         = $produk_id;
                    $array_produk_id[$indeks]['quantity']   = $cart->qty;
                    $indeks++;
                }
            }

            $so_detail->save();
        }

        //cek apakah dia dapat hadiah, jika ada tambahkan, pengecekan dilakukan dikeseluruhan isi cart agar tdk terpengaruh dg split diskon dan cashback
        foreach ($array_produk_id as $value) {
            $produk = Produk::with('promo_hadiah')->where('id', $value['id'])->first();
            //cek dia beli berapa, dimodulus 
            if(!empty($produk->promo_hadiah)){
                //cek berapa jumlah hadiah yang bisa didapat, masukkan ke array, SEMENTARA 1 PRODUK 1 JENIS HADIAH
                //RUMUS DAPAT BERAPA HADIAH
                $modulus                = $value['quantity']%$produk->promo_hadiah->qty_beli;
                $jumlah_dapat_hadiah    = (($value['quantity']-$modulus)/$produk->promo_hadiah->qty_beli*$produk->promo_hadiah->qty_hadiah);

                $hadiah[$indeks]['id']          = $produk->promo_hadiah->hadiah->id;
                $hadiah[$indeks]['nama']        = $produk->promo_hadiah->hadiah->nama;
                $hadiah[$indeks]['quantity']    = $jumlah_dapat_hadiah;
                $indeks++;

                $so_detail = new SODetail;

                $so_detail->no_nota         = $no_nota;
                $so_detail->produk_id       = $produk->promo_hadiah->hadiah->id;
                $so_detail->jumlah          = $jumlah_dapat_hadiah;
                $so_detail->harga           = 0;
                $so_detail->jenis_barang_id = 2;
                $so_detail->deskripsi       = $produk->promo_hadiah->hadiah->deskripsi;
                $so_detail->save();

                //di tabel hadiah, tambah nilai stok dipesan
                $hadiah = Hadiah::where('id', $produk->promo_hadiah->hadiah->id)->first();
                $hadiah->stok_dipesan = $hadiah->stok_dipesan + $jumlah_dapat_hadiah;
                $hadiah->save();
            }
        }

        /* SO Voucher */
        $total_voucher = 0;
        $kode_voucher = Input::get('input_kode_voucher');
        
        if(empty($kode_voucher)){
            $count_kode_voucher = 0;
        }
        else{
            $kode_voucher       = explode(',', $kode_voucher);
            $count_kode_voucher = count($kode_voucher);
        }        

        $pelanggan = Pelanggan::where('id', $pelanggan_id)->first();

        for ($i=0; $i < $count_kode_voucher; $i++) { 
            $voucher = Voucher::where('kode', $kode_voucher[$i])->where('is_aktif', 1)->where('awal_periode', '<=', $tanggal->format('Y-m-d'))->where('akhir_periode', '>=', $tanggal->format('Y-m-d'))->first();

            if(!empty($voucher)){
                if(empty($voucher->pelanggan_id_target) || $voucher->pelanggan_id_target == $pelanggan->id){
                    $so_voucher = new SOVoucher;
                    $so_voucher->so_header_id    = $so_header_id;
                    $so_voucher->voucher_id = $voucher->id;
                    $so_voucher->nominal    = $voucher->nominal;
                    $so_voucher->save();

                    $total_voucher = $total_voucher - $voucher->nominal;

                    //update tabel tmst_vouhcer
                    $voucher->tanggal_dipakai = $tanggal;
                    $voucher->pelanggan_id_pakai = $pelanggan->id;

                    if($voucher->is_sekali_pakai == 1){
                        $voucher->is_aktif = 0;
                    }

                    $voucher->save();
                }
            }
        }

        $harga_ppn = $total_harga*$ppn/100;

        $so_header = SOHeader::where('id', $so_header_id)->first();

        $so_header->total_tagihan   = $total_harga + $harga_ppn - $total_voucher;
        $so_header->save();

        /* SO ALAMAT PENGIRIMAN */
        $so_alamat_pengiriman = new SOAlamatPengiriman;
        $so_alamat_pengiriman->so_header_id = $so_header_id;
        $so_alamat_pengiriman->nama         = $pelanggan->nama;
        $so_alamat_pengiriman->email        = $pelanggan->email;
        $so_alamat_pengiriman->alamat       = $pelanggan->alamat;
        $so_alamat_pengiriman->kota_id      = $pelanggan->kota_id;
        $so_alamat_pengiriman->kode_pos     = $pelanggan->kode_pos;
        $so_alamat_pengiriman->hp           = $pelanggan->hp_sales;
        $so_alamat_pengiriman->save();

        Cart::destroy(); //hapus isi cart

        return redirect('/nota_sales_order/'.$so_header_id);
    }

    public function nota_sales_order($so_header_id)
    {
        $so_header              = SOHeader::with('pelanggan')->where('id', $so_header_id)->first();
        $so_detail              = SODetail::with('produk', 'hadiah', 'paket')->where('so_header_id', $so_header_id)->get();
        $so_voucher             = SOVoucher::where('so_header_id', $so_header_id)->get();
        $so_alamat_pengiriman   = SOAlamatPengiriman::where('so_header_id', $so_header_id)->first();

        $perusahaan     = Perusahaan::with('kota')->first();

        $kota_perusahaan = strtoupper($perusahaan->kota->nama);

        if(strpos($kota_perusahaan, 'KABUPATEN') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KABUPATEN', '', $kota_perusahaan)));
        }
        elseif(strpos($kota_perusahaan, 'KOTA') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KOTA', '', $kota_perusahaan)));
        }

        Cart::destroy();

        $judul  = "Sales Order";
        $general['title']       = $judul;
        $general['menu1']       = "Transaksi";
        $general['menu2']       = "Sales Order";
        $general['menu3']       = $general['title'];
        $general['kode_menu']   = 52;

        date_default_timezone_set("Asia/Jakarta"); 
        $print_time = new DateTime;

        $pelanggan['nama']   = $so_header->pelanggan->nama;
        $pelanggan['alamat'] = $so_header->pelanggan->alamat;

        $kota = strtoupper($so_header->pelanggan->kota->nama);

        if(strpos($kota, 'KABUPATEN') !== false){
            $kota = ucwords(strtolower(str_replace('KABUPATEN', '', $kota)));
        }
        elseif(strpos($kota, 'KOTA') !== false){
            $kota = ucwords(strtolower(str_replace('KOTA', '', $kota)));
        }

        $pelanggan['alamat'] = $pelanggan['alamat'].' '.$kota;
        $pelanggan['telp']   = $so_header->pelanggan->telp1;

        return view('pages.transaksi.sales_order.nota_sales_order', compact('general', 'so_header', 'so_detail', 'so_voucher', 'perusahaan', 'judul', 'terbilang', 'print_time', 'so_alamat_pengiriman', 'pelanggan', 'kota_perusahaan'));   
    }

    public function nota_sales_order_print($so_header_id)
    {
        $so_header              = SOHeader::with('pelanggan')->where('id', $so_header_id)->first();
        $so_detail              = SODetail::with('produk', 'hadiah', 'paket')->where('so_header_id', $so_header_id)->get();
        $so_voucher             = SOVoucher::where('so_header_id', $so_header_id)->get();
        $so_alamat_pengiriman   = SOAlamatPengiriman::where('so_header_id', $so_header_id)->first();

        $perusahaan     = Perusahaan::with('kota')->first();

        $kota_perusahaan = strtoupper($perusahaan->kota->nama);

        if(strpos($kota_perusahaan, 'KABUPATEN') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KABUPATEN', '', $kota_perusahaan)));
        }
        elseif(strpos($kota_perusahaan, 'KOTA') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KOTA', '', $kota_perusahaan)));
        }

        Cart::destroy();

        $judul  = "Sales Order"; 

        date_default_timezone_set("Asia/Jakarta"); 
        $print_time = new DateTime;

        $pelanggan['nama']   = $so_header->pelanggan->nama;
        $pelanggan['alamat'] = $so_header->pelanggan->alamat;

        $kota = strtoupper($so_header->pelanggan->kota->nama);

        if(strpos($kota, 'KABUPATEN') !== false){
            $kota = ucwords(strtolower(str_replace('KABUPATEN', '', $kota)));
        }
        elseif(strpos($kota, 'KOTA') !== false){
            $kota = ucwords(strtolower(str_replace('KOTA', '', $kota)));
        }

        $pelanggan['alamat'] = $pelanggan['alamat'].' '.$kota;
        $pelanggan['telp']   = $so_header->pelanggan->telp1;

        return view('pages.transaksi.sales_order.nota_sales_order_print', compact('general', 'so_header', 'so_detail', 'so_voucher', 'perusahaan', 'judul', 'terbilang', 'print_time', 'so_alamat_pengiriman', 'pelanggan', 'kota_perusahaan'));
    }

    public function nota_sales_order_pdf($so_header_id)
    {
        $so_header              = SOHeader::with('pelanggan')->where('id', $so_header_id)->first();
        $so_detail              = SODetail::with('produk', 'hadiah', 'paket')->where('so_header_id', $so_header_id)->get();
        $so_voucher             = SOVoucher::where('so_header_id', $so_header_id)->get();
        $so_alamat_pengiriman   = SOAlamatPengiriman::where('so_header_id', $so_header_id)->first();

        $perusahaan     = Perusahaan::with('kota')->first();
        
        $kota_perusahaan = strtoupper($perusahaan->kota->nama);

        if(strpos($kota_perusahaan, 'KABUPATEN') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KABUPATEN', '', $kota_perusahaan)));
        }
        elseif(strpos($kota_perusahaan, 'KOTA') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KOTA', '', $kota_perusahaan)));
        }

        Cart::destroy();

        $judul  = "Sales Order"; 

        date_default_timezone_set("Asia/Jakarta"); 
        $print_time = new DateTime;

        $pelanggan['nama']   = $so_header->pelanggan->nama;
        $pelanggan['alamat'] = $so_header->pelanggan->alamat;

        $kota = strtoupper($so_header->pelanggan->kota->nama);

        if(strpos($kota, 'KABUPATEN') !== false){
            $kota = ucwords(strtolower(str_replace('KABUPATEN', '', $kota)));
        }
        elseif(strpos($kota, 'KOTA') !== false){
            $kota = ucwords(strtolower(str_replace('KOTA', '', $kota)));
        }

        $pelanggan['alamat'] = $pelanggan['alamat'].', '.$kota;
        $pelanggan['telp']   = $so_header->pelanggan->telp1;
        
        $pdf = PDF::loadView('pages.transaksi.sales_order.nota_sales_order_pdf', compact('general', 'so_header', 'so_detail', 'so_voucher', 'perusahaan', 'judul', 'terbilang', 'print_time', 'so_alamat_pengiriman', 'pelanggan', 'kota_perusahaan'))->setPaper('a4', 'landscape');
        return $pdf->stream();
        //return view('pages.transaksi.sales_order.nota_sales_order_pdf', compact('general', 'so_header', 'so_detail', 'so_voucher', 'perusahaan', 'judul', 'terbilang', 'print_time', 'so_alamat_pengiriman', 'pelanggan', 'kota_perusahaan'));
    }

    public function detail_sales_order($so_header_id)
    {
        $so_header              = SOHeader::with('pelanggan')->where('id', $so_header_id)->first();
        $so_detail              = SODetail::with('produk', 'hadiah', 'paket')->where('so_header_id', $so_header_id)->get();
        $so_voucher             = SOVoucher::where('so_header_id', $so_header_id)->get();
        $so_alamat_pengiriman   = SOAlamatPengiriman::where('so_header_id', $so_header_id)->first();

        $general['title']       = "Sales Order #".$so_header->no_sales_order;
        $general['menu1']       = "Transaksi";
        $general['menu2']       =  "Sales Order";
        $general['menu3']       = $general['title'];
        $general['kode_menu']   = 52; 

        return view('pages.transaksi.detail_sales_order', compact('general', 'so_header', 'so_detail', 'so_voucher', 'so_alamat_pengiriman'));
    }

    public function pengaturan_so(Request $request)
    {
        $syarat_ketentuan   = Input::get('syarat_ketentuan');
        $checkbox_ppn       = Input::get('checkbox_ppn');
        $ppn                = Input::get('ppn');

        $pengaturan_so = PengaturanSO::where('id', 1)->first();

        if(empty($pengaturan_so)){
            $pengaturan_so = new PengaturanSO;
            $pengaturan_so->syarat_ketentuan    = $syarat_ketentuan;
            if($checkbox_ppn){
                $pengaturan_so->ppn             = $ppn;
            }
            else{
                $pengaturan_so->ppn             = "";
            }
            $pengaturan_so->save();
        }
        else{
            $pengaturan_so->syarat_ketentuan    = $syarat_ketentuan;
            if($checkbox_ppn){
                $pengaturan_so->ppn             = $ppn;
            }
            else{
                $pengaturan_so->ppn             = "";
            }
            $pengaturan_so->save();
        }

        $request->session()->flash('message', 'Pengaturan Sales Order telah diperbarui');
        return redirect('/tambah_sales_order');
    }
}
