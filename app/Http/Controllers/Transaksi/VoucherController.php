<?php

namespace App\Http\Controllers\Transaksi;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Produk\Produk;
use App\Produk\ProdukGaleri;
use App\Produk\ProdukPromo;
use App\Produk\ProdukKategori;
use Illuminate\Support\Facades\DB;
use Gloudemans\Shoppingcart\Facades\Cart;
use Input;
use Auth;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Session;
use Promocodes;
use Response;
use DateTime;
use PDF;
use Hashids\Hashids;
use Validator;
use Illuminate\Http\RedirectResponse;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use App\User;

class VoucherController extends Controller
{
    public function generate_no_nota()
    {
        $mytable = DB::table('tran_invoice')->orderBy('invoice_no', 'desc')->first();
        if(empty($mytable)){
            $num = 0;
        }
        else{
            //get last_number
            $num = $mytable->invoice_no;
        }

        $num = $num+1;
        $num_count = strlen((string)$num);
        $nol = 10-$num_count;
        $kode = "";
        for ($i=0; $i < $nol; $i++) { 
            $kode = $kode."0";
        }
        $kode = $kode.$num;

        return $kode;
    }
    
    public function voucher()
    {
        $general['title']       = "Voucher";
        $general['menu1']       = "Transaksi";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 56;

        $voucher = DB::table('tmst_voucher')->get();

        return view('pages.transaksi.voucher', compact('general', 'voucher'));
    }

    public function tambah_voucher()
    {
        $general['title']       = "Tambah Voucher";
        $general['menu1']       = "Transaksi";
        $general['menu2']       =  "Voucher";
        $general['menu3']       = $general['title'];
        $general['kode_menu']   = 56;        

        $pelanggan = DB::table('tmst_pelanggan')->select('tmst_pelanggan.*', 'tref_kota.nama as kota_nama')->join('tref_kota', 'tref_kota.id', '=', 'tmst_pelanggan.kota_id')->get();

        return view('pages.transaksi.tambah_voucher', compact('general', 'pelanggan'));
    }

    public function generate_kode_voucher()
    {
        $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $res = "";
        for ($i = 0; $i < 32; $i++) {
            $res .= $chars[mt_rand(0, strlen($chars)-1)];
        }
        return $res;
    }

    public function do_tambah_voucher(Request $request)
    {
        $this->validate($request, [
            'kode_voucher'      => 'required',
            'nominal'           => 'required',
            'awal_periode'      => 'required|date|before:akhir_periode',
            'akhir_periode'     => 'required|date|after:awal_periode',
        ]);

        $kode               = Input::get('kode_voucher');
        $nominal            = Input::get('nominal');
        $awal_periode       = Input::get('awal_periode');
        $akhir_periode      = Input::get('akhir_periode');
        $is_sekali_pakai    = Input::get('is_sekali_pakai');
        $target_pelanggan   = Input::get('target_pelanggan');

        $nominal            = str_replace(".", "", $nominal);

        $awal_periode   = new DateTime($awal_periode);
        $akhir_periode  = new DateTime($akhir_periode);

        if((int)$target_pelanggan == 1)
        {
            DB::table('tmst_voucher')->insert(['kode' => $kode, 'nominal' => $nominal, 
                'awal_periode' => $awal_periode, 'akhir_periode' => $akhir_periode, 
                'is_sekali_pakai' => $is_sekali_pakai, 'is_aktif' => 1]);
        }
        else
        {
            $this->validate($request, [
                'pelanggan_id' => 'required',
            ]);
            $pelanggan_id = Input::get('pelanggan_id2');
            DB::table('tmst_voucher')->insert(['kode' => $kode, 'nominal' => $nominal, 
                'awal_periode' => $awal_periode, 'akhir_periode' => $akhir_periode, 
                'is_sekali_pakai' => $is_sekali_pakai, 'is_aktif' => 1, 'pelanggan_id_target' => $pelanggan_id]);
        }

        $request->session()->flash('message', 'Voucher berhasil ditambah');
        return redirect('/tambah_voucher');
    }

    public function detail_voucher($id)
    {
        $general['title']       = "Detail Voucher";
        $general['menu1']       = "Transaksi";
        $general['menu2']       =  "Voucher";
        $general['menu3']       = $general['title'];
        $general['kode_menu']   = 56;     

        // $voucher    = DB::select("SELECT *, 
        // (SELECT nama FROM tmst_pelanggan WHERE id=pelanggan_id_target ) AS pelanggan_nama_target, 
        // (SELECT nama FROM tmst_pelanggan WHERE id=pelanggan_id_pakai ) AS pelanggan_nama_pakai FROM tmst_voucher 
        // where id = '$id'");

        $voucher                = DB::table('tmst_voucher')->where('id', $id)->first();
        $pelanggan_nama_target  = DB::table('tmst_pelanggan')->where('id', $voucher->pelanggan_id_target)->first();
        $pelanggan_nama_pakai   = DB::table('tmst_pelanggan')->where('id', $voucher->pelanggan_id_pakai)->first();

        return view('pages.transaksi.detail_voucher', compact('general', 'voucher', 'pelanggan_nama_target', 'pelanggan_nama_pakai'));
    }
}
