<?php

namespace App\Http\Controllers\Transaksi;

use App\Models\TransaksiService\InvoiceHeader;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\TransaksiPenjualan\BayarPenjualanHeader;
use App\Models\TransaksiPenjualan\BayarPenjualanDetail;
use App\Models\TransaksiPenjualan\SOHeader;
use App\Models\TransaksiPenjualan\SODetail;
use App\Models\TransaksiPenjualan\SOVoucher;
use App\Models\TransaksiPenjualan\SOAlamatPengiriman;
use App\Models\TransaksiPenjualan\SJKeluarHeader;
use App\Models\TransaksiPenjualan\SJKeluarDetail;
use App\Models\TransaksiPenjualan\NotaJual;
use App\Models\TransaksiPenjualan\RJHeader;
use App\Models\TransaksiPenjualan\ServiceOrder;
use App\Models\TransaksiPenjualan\TransaksiJasa;
use App\Models\Referensi\MetodePembayaran;
use Response;
use DateTime;
use Input;
use App\Models\Supplier\Supplier;
use App\Models\Supplier\RekeningSupplier;
use Validator;
use App\Models\Referensi\Bank;
use Session;

class BayarPenjualanController extends Controller
{
    public function pembayaran_pelanggan()
    {
        $general['title']       = "Pembayaran Pelanggan";
        $general['menu1']       = "Transaksi";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 512;

        $pembayaran_pelanggan = BayarPenjualanHeader::with('nota_jual', 'rj_header', 'service_order', 'pelanggan')->get();

        return view('pages.transaksi.pembayaran.pembayaran_pelanggan', compact('general', 'pembayaran_pelanggan'));
    }

    public function tambah_pembayaran_pelanggan()
    {
        $general['title']       = "Tambah Pembayaran Pelanggan";
        $general['menu1']       = "Transaksi";
        $general['menu3']       = "Pembayaran Pelanggan";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 512;

        $all_nota_jual  = NotaJual::with('pelanggan')->get();

        $nota_jual  = [];
        $key     = 0;
        foreach ($all_nota_jual as $value) {
            $nota_jual[$key]['id']              = $value->id;
            $nota_jual[$key]['jenis_id']        = 1;
            $nota_jual[$key]['jenis']           = 'Nota Jual';
            $nota_jual[$key]['no_nota']         = $value->no_nota;
            $nota_jual[$key]['pelanggan']       = $value->pelanggan->nama;
            $nota_jual[$key]['pelanggan_id']    = $value->pelanggan_id;

            $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
            
            $tanggal = new DateTime($value->tanggal);
            $nota_jual[$key]['tanggal']         = $tanggal->format('d')." ".$bulan[(int)$tanggal->format('m')]." ".$tanggal->format('Y');

            $nota_jual[$key]['total_diskon']    = $value->total_voucher;
            $nota_jual[$key]['total_harga']     = $value->total_harga;
            $nota_jual[$key]['ppn']             = $value->ppn;
            $nota_jual[$key]['total_tagihan']   = $value->total_tagihan;


            //hitung total tagihan
            $total_tagihan = 0;
            $sj_keluar_detail = SJKeluarDetail::where('sj_keluar_header_id', $value->sj_keluar_header_id)->get();

            foreach ($sj_keluar_detail as $key2 => $value2) {
                $total_tagihan = $total_tagihan + $value2->harga;
            }

            $nota_jual[$key]['ppn']    = $value->ppn;

            $nota_jual[$key]['total_tagihan']   = $total_tagihan;

            $key++;
        }

        $all_retur_jual  = RJHeader::with('pelanggan')->where('is_lunas', 0)->get();

        foreach ($all_retur_jual as $value) {
            $nota_jual[$key]['id']              = $value->id;
            $nota_jual[$key]['jenis_id']        = 2;
            $nota_jual[$key]['jenis']           = 'Retur Jual';
            $nota_jual[$key]['no_nota']         = $value->no_nota;
            $nota_jual[$key]['pelanggan']       = $value->pelanggan->nama;
            $nota_jual[$key]['pelanggan_id']    = $value->pelanggan_id;

            $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
            
            $tanggal = new DateTime($value->tanggal);
            $nota_jual[$key]['tanggal']         = $tanggal->format('d')." ".$bulan[(int)$tanggal->format('m')]." ".$tanggal->format('Y');

            $nota_jual[$key]['total_harga']   = $value->total_retur;

            if(empty($value->potongan_retur)){
                $nota_jual[$key]['total_diskon'] = 0;
            }
            else{
                $nota_jual[$key]['total_diskon']    = $value->potongan_retur;
            }

            $nota_jual[$key]['ppn']    = 0;
            $nota_jual[$key]['total_tagihan']   = $value->total_tagihan;

            $key++;
        }

        $all_invoice_service_order  = InvoiceHeader::with('pelanggan')->where('is_lunas', 0)->where('harga_total', '!=', 0)->get();

        foreach ($all_invoice_service_order as $value) {
            $nota_jual[$key]['id']              = $value->id;
            $nota_jual[$key]['service_order_id']= $value->service_order_id;
            $nota_jual[$key]['jenis_id']        = 3;
            $nota_jual[$key]['jenis']           = 'Servis';
            $nota_jual[$key]['pelanggan']       = $value->pelanggan->nama;
            $nota_jual[$key]['pelanggan_id']    = $value->pelanggan_id;
            $all_service_order                  = ServiceOrder::where('id', $nota_jual[$key]['service_order_id'])->first();
            $nota_jual[$key]['no_nota']         = $all_service_order->no_nota;

            $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');

            $tanggal = new DateTime($value->tanggal_diterima);
            $nota_jual[$key]['tanggal']         = $tanggal->format('d')." ".$bulan[(int)$tanggal->format('m')]." ".$tanggal->format('Y');

            $nota_jual[$key]['total_harga']     = 0;
            $nota_jual[$key]['total_diskon']    = 0;
            $nota_jual[$key]['ppn']             = 0;
            $nota_jual[$key]['total_tagihan']   = $value->harga_total;

            $key++;
        }

        $all_transaksi_jasa  = TransaksiJasa::with('pelanggan')->where('is_lunas', 0)->get();

        foreach ($all_transaksi_jasa as $value) {
            $nota_jual[$key]['id']              = $value->id;
            $nota_jual[$key]['jenis_id']        = 4;
            $nota_jual[$key]['jenis']           = 'Transaksi Jasa';
            $nota_jual[$key]['no_nota']         = $value->no_nota;
            $nota_jual[$key]['pelanggan']       = $value->pelanggan->nama;
            $nota_jual[$key]['pelanggan_id']    = $value->pelanggan_id;

            $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
            
            $tanggal = new DateTime($value->tanggal);
            $nota_jual[$key]['tanggal']         = $tanggal->format('d')." ".$bulan[(int)$tanggal->format('m')]." ".$tanggal->format('Y');

            $nota_jual[$key]['total_harga']     = 0;
            $nota_jual[$key]['total_diskon']    = 0;
            $nota_jual[$key]['ppn']             = 0;
            $nota_jual[$key]['total_tagihan']   = $value->biaya;

            $key++;
        }

        $tanggal = new DateTime();
        $tanggal = $tanggal->format('d-m-Y');
  
        $metode_pembayaran = MetodePembayaran::where('jenis', 'non_tunai')->get();
        $bank               = Bank::all();

        return view('pages.transaksi.pembayaran.tambah_pembayaran_pelanggan', compact('general', 'nota_jual', 'tanggal', 'metode_pembayaran', 'bank'));
    }

    public function do_pembayaran_pelanggan(Request $request)
    {
        $this->validate($request, [
            'nota_id' => 'required',
            'jenis_id' => 'required',
            'tanggal' => 'required',
        ]);

        //nanti harus dicek bahwa nominal harus sama dengan tagihan-diskon

        $nota_id    = Input::get('nota_id');
        $jenis_id   = Input::get('jenis_id');
        $tanggal    = Input::get('tanggal');
        $tunai      = Input::get('tunai');
        $keterangan                     = Input::get('keterangan');
        $jumlah_pembayaran_non_tunai    = Input::get('jumlah_pembayaran_non_tunai');

        $tanggal    = new DateTime($tanggal);
        $tunai      = str_replace('.', '', $tunai);

        if(empty($tunai)){
            $tunai = 0;
        }

        $bayar_penjualan_header            = new BayarPenjualanHeader;
        $bayar_penjualan_header->nota_id   = $nota_id;
        $total_tagihan = 0;
        if($jenis_id == 1){
            $jenis      = 'nota_jual';
            $nota_jual  = NotaJual::where('id', $nota_id)->first();
            $nota_jual->is_lunas = 1;
            $nota_jual->save();
            $pelanggan_id   = $nota_jual->pelanggan_id;
            $total_tagihan  = $nota_jual->total_tagihan;
        }
        elseif($jenis_id == 2){
            $jenis      = 'retur_jual';
            $rj_header  = RJHeader::where('id', $nota_id)->first();
            $rj_header->is_lunas = 1;
            $rj_header->save();
            $pelanggan_id   = $rj_header->pelanggan_id;
            $total_tagihan  = $rj_header->total_tagihan;
        }
        elseif($jenis_id == 3){
            $jenis      = 'service_order';
            $invoice_service_order  = InvoiceHeader::where('id', $nota_id)->first();
            $invoice_service_order->is_lunas = 1;
            $invoice_service_order->save();
            $pelanggan_id   = $invoice_service_order->pelanggan_id;
            $total_tagihan  = $invoice_service_order->harga_total;
            $service_order_id = $invoice_service_order->service_order_id;
            //Update Service Order
            ServiceOrder::where('id', $service_order_id)->update(
                array(
                    'flag_case'         => 1,
                    'tanggal_diambil'   => date('Y-m-d H:i:s'),
                    'garansi_service'   => date('Y-m-d H:i:s', strtotime('+7 days'))
                )
            );
        }
        elseif($jenis_id == 4){
            $jenis      = 'transaksi_jasa';
            $transaksi_jasa = TransaksiJasa::where('id', $nota_id)->first();
            $transaksi_jasa->is_lunas = 1;
            $transaksi_jasa->save();
            $pelanggan_id   = $transaksi_jasa->pelanggan_id;
            $total_tagihan  = $transaksi_jasa->biaya;
        }

        $bayar_penjualan_header->jenis              = $jenis;
        $bayar_penjualan_header->pelanggan_id        = $pelanggan_id;
        $bayar_penjualan_header->tanggal            = $tanggal;
        $bayar_penjualan_header->total_tagihan      = $total_tagihan;
        $bayar_penjualan_header->keterangan         = $keterangan;
        $bayar_penjualan_header->save();

        $bayar_penjualan_header_id  = $bayar_penjualan_header->id;
        $total_pembayaran           = 0;

        //tunai
        $bayar_penjualan_detail = new BayarPenjualanDetail;
        $bayar_penjualan_detail->bayar_penjualan_header_id  = $bayar_penjualan_header_id;
        $bayar_penjualan_detail->metode_pembayaran_id       = 1;
        $bayar_penjualan_detail->nomor_pembayaran           = "";
        $bayar_penjualan_detail->nominal                    = $tunai;
        $bayar_penjualan_detail->bank                       = "";
        $bayar_penjualan_detail->save();

        $total_pembayaran = $total_pembayaran + $tunai;

        //non tunai
        for ($i=1; $i <= (int)$jumlah_pembayaran_non_tunai; $i++) { 
            $nominal                = Input::get('non_tunai'.$i);
            $metode_pembayaran_id   = Input::get('metode_pembayaran'.$i);
            $no_rekening            = Input::get('no_rekening'.$i);
            $bank                   = Input::get('bank'.$i);

            if(!empty($nominal) && !empty($metode_pembayaran_id) && !empty($no_rekening) && !empty($bank)){
                $nominal = str_replace('.', '', $nominal);

                $bayar_penjualan_detail = new BayarPenjualanDetail;
                $bayar_penjualan_detail->bayar_penjualan_header_id  = $bayar_penjualan_header_id;
                $bayar_penjualan_detail->metode_pembayaran_id       = $metode_pembayaran_id;
                $bayar_penjualan_detail->nomor_pembayaran           = $no_rekening;
                $bayar_penjualan_detail->nominal                    = $nominal;
                $bayar_penjualan_detail->bank                       = $bank;
                $bayar_penjualan_detail->save();

                $total_pembayaran = $total_pembayaran + $nominal;
            }
        }

        $bayar_penjualan_header = BayarPenjualanHeader::where('id', $bayar_penjualan_header_id)->first();
        $bayar_penjualan_header->total_pembayaran = $total_pembayaran;
        $bayar_penjualan_header->save();

        //sendmail to customer, invoice service

        $request->session()->flash('message', 'Pembayaran Pelanggan berhasil dilakukan');
        return redirect('/pembayaran_pelanggan');
    }

    public function hapus_bayar_penjualan(Request $request)
    {
        $id = Input::get('id');

        $bayar_penjualan = BayarPenjualan::where('id', $id)->first();

        //cek jenis

        if($bayar_penjualan->jenis == 'nota_jual'){
            $nota_jual = NotaJual::where('id', $bayar_penjualan->nota_id)->first();
            $nota_jual->is_lunas = 0;
            $nota_jual->save();
        }
        elseif($bayar_penjualan->jenis == 'retur_jual'){
            $rj_header = RJHeader::where('id', $bayar_penjualan->nota_id)->first();
            $rj_header->is_lunas = 0;
            $rj_header->save();
        }
        elseif($bayar_penjualan->jenis == 'service_order'){
            $service_order = ServiceOrder::where('id', $bayar_penjualan->nota_id)->first();
            $service_order->is_lunas = 0;
            $service_order->save();
        }
        elseif($bayar_penjualan->jenis == 'transaksi_jasa'){
            $transaksi_jasa = TransaksiJasa::where('id', $bayar_penjualan->nota_id)->first();
            $transaksi_jasa->is_lunas = 0;
            $transaksi_jasa->save();
        }

        $bayar_penjualan->delete();

        $request->session()->flash('message', 'Pembayaran Pelanggan berhasil dihapus');
        return redirect('/pembayaran_pelanggan');
    }
}
