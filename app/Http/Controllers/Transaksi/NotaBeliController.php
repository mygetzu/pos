<?php

namespace App\Http\Controllers\Transaksi;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;
use Input;
use DateTime;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Models\Perusahaan;
use PDF;
use App\Models\TransaksiPembelian\InvoicePembelian;
use App\Models\TransaksiPembelian\BayarInvoicePembelian;
use App\Models\TransaksiPembelian\POHeader;
use App\Models\TransaksiPembelian\PODetail;
use App\Models\TransaksiPembelian\PODiskon;
use App\Models\TransaksiPembelian\POAlamatPengiriman;
use App\Models\TransaksiPembelian\SJMasukHeader;
use App\Models\TransaksiPembelian\SJMasukDetail;
use App\Models\TransaksiPembelian\NotaBeli;
use App\Models\Referensi\MetodePembayaran;
use App\Models\Produk\Produk;

class NotaBeliController extends Controller
{
    public function nota_beli()
    {
        $general['title']       = "Nota Beli";
        $general['menu1']       = "Transaksi";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 58;

        $nota_beli = NotaBeli::with('sj_masuk_header', 'supplier')->get();

        return view('pages.transaksi.nota_beli', compact('general', 'nota_beli'));
    }

    public function tambah_nota_beli()
    {
        $general['title']       = "Tambah Nota Beli";
        $general['menu1']       = "Transaksi";
        $general['menu2']       = "Nota Beli";
        $general['menu3']       = $general['title'];
        $general['kode_menu']   = 58;

        $sj_masuk_header = SJMasukHeader::all();

        //ambil surat_jalan yang belum punya nota_beli
        $surat_jalan = [];
        $indeks = 0;

        foreach ($sj_masuk_header as $value) {
            $nota_beli = NotaBeli::where('sj_masuk_header_id', $value->id)->first();
            
            if(empty($nota_beli)){
                $surat_jalan[$indeks]['sj_masuk_header_id'] = $value->id;
                $surat_jalan[$indeks]['no_surat_jalan']     = $value->no_surat_jalan;
                $surat_jalan[$indeks]['supplier']           = $value->supplier->nama;
                $surat_jalan[$indeks]['tanggal']            = $value->tanggal;
                $indeks++;
            }
        }

        date_default_timezone_set("Asia/Jakarta");
        $tanggal        = new DateTime;

        $nomor_invoice      = DB::table('tref_nomor_invoice')->where('id', 3)->first();
        $format_invoice     = $nomor_invoice->format;
        $ori_format_invoice = $format_invoice;

        $cek_dd = strpos($format_invoice, 'DD');
        if(!empty($cek_dd))
        {
            $format_invoice = str_replace('DD', $tanggal->format('d'), $format_invoice);
        }

        $cek_mm = strpos($format_invoice, 'MM');
        if(!empty($cek_mm))
        {
            $format_invoice = str_replace('MM', $tanggal->format('m'), $format_invoice);
        }

        $cek_yyyy = strpos($format_invoice, 'YYYY');
        if(!empty($cek_yyyy))
        {
            $format_invoice = str_replace('YYYY', $tanggal->format('Y'), $format_invoice);
        }

        $cek_yy = strpos($format_invoice, 'YY');
        if(!empty($cek_yy))
        {
            $format_invoice = str_replace('YY', $tanggal->format('y'), $format_invoice);
        }

        //cek maksimal 10
        $number = '##########';

        for ($i=9; $i > 0; $i--) { 
            $number     = substr($number, 0, $i);
            $cek_number = strpos($format_invoice, $number);
            $nol        = "";
            if(!empty($cek_number))
            {
                $nota_beli    = NotaBeli::orderBy('id', 'desc')->first();
                if(empty($nota_beli))
                    $get_num = 1;
                else
                    $get_num        = (int)$nota_beli->id + 1;
                
                for ($j=0; $j < $i - strlen($get_num); $j++) { 
                    $nol = $nol."0";
                }

                $get_num = $nol.$get_num;
                $format_invoice = str_replace($number, $get_num, $format_invoice);
                break;
            }
        }

        $no_nota_beli     = $format_invoice;

        return view('pages.transaksi.tambah_nota_beli', compact('general', 'indeks', 'surat_jalan', 'no_nota_beli', 'pembayaran_tunai', 'pembayaran_non_tunai'));
    }

    public function nb_get_data()
    {
        $sj_masuk_header_id = Input::get('sj_masuk_header_id');

        $sj_masuk_header    = SJMasukHeader::where('id', $sj_masuk_header_id)->first();
        $po_header          = POHeader::where('id', $sj_masuk_header->po_header_id)->first();

        //total tagihan dilihat dari total harga surat jalan
        $total_tagihan = 0;

        $sj_masuk_detail = SJMasukDetail::where('sj_masuk_header_id', $sj_masuk_header_id)->get();
        
        foreach ($sj_masuk_detail as $key => $value) {
            $total_tagihan = $total_tagihan + $value->harga;
        }

        $data['total_tagihan']  = $total_tagihan;
        $data['diskon']         = $sj_masuk_header->total_diskon;
        $data['ppn']            = $sj_masuk_header->ppn;

        echo json_encode($data);
    }

    public function do_tambah_nota_beli(Request $request)
    {
        $this->validate($request, [
            'sj_masuk_header_id' => 'required',
            'nota_beli' => 'required',
            'tanggal' => 'required',
            'jatuh_tempo' => 'required',
        ]);

        $sj_masuk_header_id = Input::get('sj_masuk_header_id');
        $no_nota            = Input::get('nota_beli');
        $tanggal            = Input::get('tanggal');
        $jatuh_tempo        = Input::get('jatuh_tempo');

        $tanggal        = new DateTime($tanggal);
        $jatuh_tempo    = new DateTime($jatuh_tempo);

        $sj_masuk_header                    = SJMasukHeader::where('id', $sj_masuk_header_id)->first();
        $sj_masuk_header->flag_nota_beli    = 1;
        $sj_masuk_header->save();

        $nota_beli = new NotaBeli;
        $nota_beli->sj_masuk_header_id  = $sj_masuk_header_id;
        $nota_beli->no_nota             = $no_nota;
        $nota_beli->supplier_id         = $sj_masuk_header->supplier_id;
        $nota_beli->tanggal             = $tanggal;
        $nota_beli->jatuh_tempo         = $jatuh_tempo;
        $nota_beli->is_lunas            = 0;
        $nota_beli->ppn                 = $sj_masuk_header->ppn;
        $nota_beli->total_harga         = $sj_masuk_header->total_harga;
        $nota_beli->total_diskon        = $sj_masuk_header->total_diskon;
        $nota_beli->total_tagihan       = $sj_masuk_header->total_tagihan;
        $nota_beli->save();

        $nota_beli_id = $nota_beli->id;

        //update nilai HPP, 
        $sj_masuk_detail = SJMasukDetail::where('sj_masuk_header_id', $sj_masuk_header_id)->groupBy('produk_id', 'jenis_barang_id')->selectRaw('tran_sj_masuk_detail.*, sum(jumlah) as jumlah_total, sum(harga) as harga_total')->get();

        foreach ($sj_masuk_detail as $key => $value) {
            if($value->jenis_barang_id == 1){ //hanya untuk produk
                $produk = Produk::where('id', $value->produk_id)->first();
                $hpp = 0;
                if(!empty($produk->hpp)){
                    $hpp = $produk->hpp;
                }

                $avg = (($hpp * ($produk->stok - $produk->jumlah_total)) + $value->harga_total)/($produk->stok);

                $produk->hpp = $avg;
                $produk->save();
            } 
        }

        return redirect('/invoice_nota_beli/'.$nota_beli_id);
    }

    public function invoice_nota_beli($nota_beli_id)
    {
        $nota_beli = NotaBeli::with('supplier')->where('id', $nota_beli_id)->first();

        $sj_masuk_header    = SJMasukHeader::where('id', $nota_beli->sj_masuk_header_id)->first();
        $po_header          = POHeader::where('id', $sj_masuk_header->po_header_id)->first();

        $judul  = "Nota Beli";
        $general['title']       = $judul;
        $general['menu1']       = "Transaksi";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 58;  

        $perusahaan = Perusahaan::with('kota')->first();
        $kota_perusahaan = strtoupper($perusahaan->kota->nama);

        if(strpos($kota_perusahaan, 'KABUPATEN') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KABUPATEN', '', $kota_perusahaan)));
        }
        elseif(strpos($kota_perusahaan, 'KOTA') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KOTA', '', $kota_perusahaan)));
        }

        date_default_timezone_set("Asia/Jakarta"); 
        $print_time = new DateTime;

        $sj_masuk_detail    = SJMasukDetail::where('sj_masuk_header_id', $nota_beli->sj_masuk_header_id)
        ->groupBy('produk_id', 'jenis_barang_id')->selectRaw('tran_sj_masuk_detail.*, sum(jumlah) as jumlah_total')->get();

        $produk = [];

        foreach ($sj_masuk_detail as $key => $value) {
            if($value->jenis_barang_id == 1){
                $produk[$key]['nama'] = $value->produk->nama;
                $produk[$key]['satuan'] = $value->produk->satuan;
            }
            elseif($value->jenis_barang_id == 2){
                $produk[$key]['nama'] = $value->hadiah->nama;
                $produk[$key]['satuan'] = $value->hadiah->satuan;
            }

            //ambil spesifikasi di PO
            $po_detail = PODetail::where('po_header_id', $po_header->id)->where('produk_id', $value->produk_id)->where('jenis_barang_id', $value->jenis_barang_id)->first();
            $spesifikasi = $po_detail->deskripsi;

            $produk[$key]['spesifikasi']    = $spesifikasi;
            $produk[$key]['jumlah']         = $value->jumlah_total;

            $po_detail = PODetail::where('po_header_id', $po_header->id)->where('produk_id', $value->produk_id)
            ->where('jenis_barang_id', $value->jenis_barang_id)->where('jumlah_terkirim', '>', '0')->first();
            $produk[$key]['harga']          = $po_detail->harga;
        }

        $supplier['nama']   = $nota_beli->supplier->nama;
        $supplier['alamat'] = $nota_beli->supplier->alamat;

        $kota = strtoupper($nota_beli->supplier->kota->nama);

        if(strpos($kota, 'KABUPATEN') !== false){
            $kota = str_replace('KABUPATEN', '', $kota);
        }
        elseif(strpos($kota, 'KOTA') !== false){
            $kota = str_replace('KOTA', '', $kota);
        }

        $supplier['alamat'] = $supplier['alamat'].', '.$kota;
        $supplier['telp']   = $nota_beli->supplier->telp1;
        
        return view('pages.transaksi.nota_beli.invoice_nota_beli', compact('general', 'sj_masuk_header', 'po_header', 'sj_masuk_detail', 'judul', 'perusahaan', 'produk', 'print_time', 'nota_beli', 'supplier', 'kota_perusahaan')); 
    }

    public function invoice_nota_beli_print($nota_beli_id)
    {
        $nota_beli = NotaBeli::with('supplier')->where('id', $nota_beli_id)->first();

        $sj_masuk_header    = SJMasukHeader::where('id', $nota_beli->sj_masuk_header_id)->first();
        $po_header          = POHeader::where('id', $sj_masuk_header->po_header_id)->first();

        $judul  = "Nota Beli"; 

        $perusahaan = Perusahaan::with('kota')->first();
        $kota_perusahaan = strtoupper($perusahaan->kota->nama);

        if(strpos($kota_perusahaan, 'KABUPATEN') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KABUPATEN', '', $kota_perusahaan)));
        }
        elseif(strpos($kota_perusahaan, 'KOTA') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KOTA', '', $kota_perusahaan)));
        }

        date_default_timezone_set("Asia/Jakarta"); 
        $print_time = new DateTime;

        $sj_masuk_detail    = SJMasukDetail::where('sj_masuk_header_id', $nota_beli->sj_masuk_header_id)
        ->groupBy('produk_id', 'jenis_barang_id')->selectRaw('tran_sj_masuk_detail.*, sum(jumlah) as jumlah_total')->get();

        $produk = [];

        foreach ($sj_masuk_detail as $key => $value) {
            if($value->jenis_barang_id == 1){
                $produk[$key]['nama'] = $value->produk->nama;
                $produk[$key]['satuan'] = $value->produk->satuan;
            }
            elseif($value->jenis_barang_id == 2){
                $produk[$key]['nama'] = $value->hadiah->nama;
                $produk[$key]['satuan'] = $value->hadiah->satuan;
            }

            //ambil spesifikasi di PO
            $po_detail = PODetail::where('po_header_id', $po_header->id)->where('produk_id', $value->produk_id)->where('jenis_barang_id', $value->jenis_barang_id)->first();
            $spesifikasi = $po_detail->deskripsi;

            $produk[$key]['spesifikasi']    = $spesifikasi;
            $produk[$key]['jumlah']         = $value->jumlah_total;

            $po_detail = PODetail::where('po_header_id', $po_header->id)->where('produk_id', $value->produk_id)
            ->where('jenis_barang_id', $value->jenis_barang_id)->where('jumlah_terkirim', '>', '0')->first();
            $produk[$key]['harga']          = $po_detail->harga;
        }

        $supplier['nama']   = $nota_beli->supplier->nama;
        $supplier['alamat'] = $nota_beli->supplier->alamat;

        $kota = strtoupper($nota_beli->supplier->kota->nama);

        if(strpos($kota, 'KABUPATEN') !== false){
            $kota = str_replace('KABUPATEN', '', $kota);
        }
        elseif(strpos($kota, 'KOTA') !== false){
            $kota = str_replace('KOTA', '', $kota);
        }

        $supplier['alamat'] = $supplier['alamat'].', '.$kota;
        $supplier['telp']   = $nota_beli->supplier->telp1;
        
        return view('pages.transaksi.nota_beli.invoice_nota_beli_print', compact('general', 'sj_masuk_header', 'po_header', 'sj_masuk_detail', 'judul', 'perusahaan', 'produk', 'print_time', 'nota_beli', 'supplier', 'kota_perusahaan'));
    }

    public function invoice_nota_beli_pdf($nota_beli_id)
    {
        $nota_beli = NotaBeli::where('id', $nota_beli_id)->first();

        $sj_masuk_header    = SJMasukHeader::where('id', $nota_beli->sj_masuk_header_id)->first();
        $po_header          = POHeader::where('id', $sj_masuk_header->po_header_id)->first();

        $judul  = "Nota Beli"; 

        $perusahaan = Perusahaan::with('kota')->first();
        $kota_perusahaan = strtoupper($perusahaan->kota->nama);

        if(strpos($kota_perusahaan, 'KABUPATEN') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KABUPATEN', '', $kota_perusahaan)));
        }
        elseif(strpos($kota_perusahaan, 'KOTA') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KOTA', '', $kota_perusahaan)));
        }

        date_default_timezone_set("Asia/Jakarta"); 
        $print_time = new DateTime;

        $sj_masuk_detail    = SJMasukDetail::where('sj_masuk_header_id', $nota_beli->sj_masuk_header_id)
        ->groupBy('produk_id', 'jenis_barang_id')->selectRaw('tran_sj_masuk_detail.*, sum(jumlah) as jumlah_total')->get();

        $produk = [];

        foreach ($sj_masuk_detail as $key => $value) {
            if($value->jenis_barang_id == 1){
                $produk[$key]['nama'] = $value->produk->nama;
                $produk[$key]['satuan'] = $value->produk->satuan;
            }
            elseif($value->jenis_barang_id == 2){
                $produk[$key]['nama'] = $value->hadiah->nama;
                $produk[$key]['satuan'] = $value->hadiah->satuan;
            }

            //ambil spesifikasi di PO
            $po_detail = PODetail::where('po_header_id', $po_header->id)->where('produk_id', $value->produk_id)->where('jenis_barang_id', $value->jenis_barang_id)->first();
            $spesifikasi = $po_detail->deskripsi;

            $produk[$key]['spesifikasi']    = $spesifikasi;
            $produk[$key]['jumlah']         = $value->jumlah_total;

            $po_detail = PODetail::where('po_header_id', $po_header->id)->where('produk_id', $value->produk_id)
            ->where('jenis_barang_id', $value->jenis_barang_id)->where('jumlah_terkirim', '>', '0')->first();
            $produk[$key]['harga']          = $po_detail->harga;
        }

        $supplier['nama']   = $nota_beli->supplier->nama;
        $supplier['alamat'] = $nota_beli->supplier->alamat;

        $kota = strtoupper($nota_beli->supplier->kota->nama);

        if(strpos($kota, 'KABUPATEN') !== false){
            $kota = str_replace('KABUPATEN', '', $kota);
        }
        elseif(strpos($kota, 'KOTA') !== false){
            $kota = str_replace('KOTA', '', $kota);
        }

        $supplier['alamat'] = $supplier['alamat'].', '.$kota;
        $supplier['telp']   = $nota_beli->supplier->telp1;

        $pdf = PDF::loadView('pages.transaksi.nota_beli.invoice_nota_beli_pdf', compact('general', 'sj_masuk_header', 'po_header', 'sj_masuk_detail', 'judul', 'perusahaan', 'produk', 'print_time', 'nota_beli', 'supplier', 'kota_perusahaan'))->setPaper('a4', 'landscape');
        return $pdf->stream();
    }

    public function pembayaran_nota_beli($id)
    {
        
    }

    public function hapus_nota_beli(Request $request)
    {
        $id = Input::get('id');

        $nota_beli = NotaBeli::where('id', $id)->first();
        $sj_masuk_header = SJMasukHeader::where('id', $nota_beli->sj_masuk_header_id)->first();
        $sj_masuk_header->flag_nota_beli    = 0;
        $sj_masuk_header->save();

        $nota_beli->delete();

        $request->session()->flash('message', 'Nota beli berhasil dihapus');
        return redirect('/nota_beli');
    }
}
