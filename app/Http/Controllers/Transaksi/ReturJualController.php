<?php

namespace App\Http\Controllers\Transaksi;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Gloudemans\Shoppingcart\Facades\Cart;
use Input;
use Auth;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Session;
use Promocodes;
use Response;
use DateTime;
use PDF;
use Hashids\Hashids;
use Validator;
use Illuminate\Http\RedirectResponse;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use App\User;
use App\Models\Produk\Produk;
use App\Models\Pelanggan\Pelanggan;
use App\Models\PaketProduk\Paket;
use App\Models\Produk\Hadiah;
use App\Models\Perusahaan;
use App\Models\Gudang\Gudang;
use App\Models\Gudang\ProdukGudang;
use App\Models\Gudang\PaketGudang;
use App\Models\Produk\ProdukSerialNumber;
use App\Models\TransaksiPenjualan\RJHeader;
use App\Models\TransaksiPenjualan\RJDetail;
use App\Models\TransaksiPenjualan\SOHeader;
use App\Models\TransaksiPenjualan\SODetail;
use App\Models\TransaksiPenjualan\SJKeluarHeader;
use App\Models\TransaksiPenjualan\SJKeluarDetail;
use App\Models\TransaksiPenjualan\NotaJual;

class ReturJualController extends Controller
{
    public function retur_jual()
    {
        $general['title']       = "Retur Jual";
        $general['menu1']       = "Transaksi";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 53; 
        
        $rj_header = RJHeader::with('pelanggan')->orderBy('tanggal', 'desc')->get();

        return view('pages.transaksi.retur_jual', compact('general', 'rj_header'));
    }

    public function tambah_retur_jual()
    {
        $general['title']       = "Tambah Retur Jual";
        $general['menu1']       = "Transaksi";
        $general['menu2']       = "Retur Jual";
        $general['menu3']       = $general['title'];
        $general['kode_menu']   = 53; 

        Cart::destroy();
        date_default_timezone_set("Asia/Jakarta");
        $tanggal        = new DateTime;

        $pelanggan          = Pelanggan::with('kota')->get();

        $nomor_invoice  = DB::table('tref_nomor_invoice')->where('id', 6)->first();
        $format_invoice = $nomor_invoice->format;
        $ori_format_invoice = $format_invoice;

        $cek_dd = strpos($format_invoice, 'DD');
        if(!empty($cek_dd))
        {
            $format_invoice = str_replace('DD', $tanggal->format('d'), $format_invoice);
        }

        $cek_mm = strpos($format_invoice, 'MM');
        if(!empty($cek_mm))
        {
            $format_invoice = str_replace('MM', $tanggal->format('m'), $format_invoice);
        }

        $cek_yyyy = strpos($format_invoice, 'YYYY');
        if(!empty($cek_yyyy))
        {
            $format_invoice = str_replace('YYYY', $tanggal->format('Y'), $format_invoice);
        }

        //cek maksimal 10
        $number = '##########';

        for ($i=9; $i > 0; $i--) { 
            $number     = substr($number, 0, $i);
            $cek_number = strpos($format_invoice, $number);
            $nol        = "";
            if(!empty($cek_number))
            {
                $rj_header      = RJHeader::orderBy('id', 'desc')->first();
                if(empty($rj_header))
                    $get_num = 1;
                else
                    $get_num        = (int)$rj_header->id + 1;

                for ($j=0; $j < $i - strlen($get_num); $j++) { 
                    $nol = $nol."0";
                }
               
                $get_num = $nol.$get_num;
                $format_invoice = str_replace($number, $get_num, $format_invoice);
                break;
            }
        }

        $no_retur_jual     = $format_invoice;

        $gudang = Gudang::where('is_aktif', 1)->get();

        return view('pages.transaksi.tambah_retur_jual', compact('general', 'pelanggan', 'format_invoice', 'no_retur_jual', 'ori_format_invoice', 'gudang'));
    }

    public function ubah_format_no_invoice_rj(Request $request)
    {
        $format = strtoupper(Input::get('format'));

        DB::table('tref_nomor_invoice')->where('id', 5)->update(['format' => $format]);

        $request->session()->flash('message', 'Format No. Retur Jual berhasil diperbarui');
        return redirect('/tambah_retur_jual');
    }

    public function get_nota_pelanggan()
    {
        $pelanggan_id = Input::get('pelanggan_id');

        //$so_header = SOHeader::where('pelanggan_id', $pelanggan_id)->get();

        $nota_jual_obj = NotaJual::where('pelanggan_id', $pelanggan_id)->get();

        $nota_jual = [];
        $indeks = 0;

        foreach ($nota_jual_obj as $key => $value) {
            $nota_jual[$indeks]['id']       = $value->id;
            $nota_jual[$indeks]['no_nota']  = $value->no_nota;
            $nota_jual[$indeks]['nama_pelanggan']  = DB::table('tmst_pelanggan')->where('id', $pelanggan_id)->value('nama');
            $nota_jual[$indeks]['tanggal']  = $value->tanggal;
            $indeks++;
        }

        return view('pages.transaksi.ajax_so_header', compact('nota_jual'));
    }

    public function get_so_detail()
    {
        $nota_jual_id = Input::get('nota_jual_id');

        $nota_jual = NotaJual::where('id', $nota_jual_id)->first();

        $sj_keluar_header = SJKeluarHeader::where('id', $nota_jual->sj_keluar_header_id)->first();

        $so_detail = [];
        $indeks = 0;
        $sj_keluar_detail = SJKeluarDetail::where('sj_keluar_header_id', $sj_keluar_header->id)->get();

        foreach ($sj_keluar_detail as $value2) {
            if($value2->jenis_barang_id == 1){
                $produk = Produk::where('id', $value2->produk_id)->first();
                $so_detail[$indeks]['nama']    = $produk->nama;
                $so_detail[$indeks]['satuan']  = $produk->satuan;
            }
            elseif($value2->jenis_barang_id == 2) {
                $hadiah = Hadiah::where('id', $value2->produk_id)->first();
                $so_detail[$indeks]['nama']    = $hadiah->nama;
                $so_detail[$indeks]['satuan']  = $hadiah->satuan;
            }
            elseif($value2->jenis_barang_id == 3) {
                $paket = Paket::where('id', $value2->produk_id)->first();
                $so_detail[$indeks]['nama']    = $paket->nama;
                $so_detail[$indeks]['satuan']  = "paket";
            }

            $so_detail[$indeks]['produk_id']       = $value2->produk_id;
            $so_detail[$indeks]['harga']           = $value2->harga;
            $so_detail[$indeks]['jumlah']        = $value2->jumlah;
            $so_detail[$indeks]['serial_number']   = $value2->serial_number;
            $so_detail[$indeks]['jenis_barang_id'] = $value2->jenis_barang_id;
            $so_detail[$indeks]['gudang_id']       = $value2->gudang_id;

            $indeks++;
        }

        return view('pages.transaksi.ajax_so_detail', compact('so_detail'));
    }

    public function retur_jual_tambah_cart()
    {
        $nota_jual_id       = Input::get('nota_jual_id');
        $produk_id          = Input::get('produk_id');
        $serial_number      = Input::get('serial_number');
        $harga              = Input::get('harga');
        $jenis_barang_id    = Input::get('jenis_barang_id');
        $gudang_id          = Input::get('gudang_id');
        $jumlah           = 1;

        if(empty($harga)){
            $harga = 0;
        }

        if($jenis_barang_id == 1){
            $produk = Produk::where('id', $produk_id)->first();
            $jenis  = "PRD";
        }
        elseif ($jenis_barang_id == 2) {
            echo '<div class="alert alert-danger flat">';
                echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
                  echo '<span aria-hidden="true">&times;</span>';
                echo '</button>';
                echo 'Hadiah tidak bisa diretur';
            echo '</div>';
            
            $cart_content = Cart::content(1);
        
            return view('pages.transaksi.ajax_retur_admin_cart', compact('cart_content'));
        }
        elseif ($jenis_barang_id == 3) {
            $produk = Paket::where('id', $produk_id)->first();
            $jenis  = "PKT";
        }

        //cek apakah jumlah sesuai dengan sales order
        $cek_jumlah     = 1;
        $cart_content   = Cart::content(1);
        foreach ($cart_content as $key => $value) 
        {
            if($value->id == $jenis.$produk_id."---".$serial_number)
            {
                $cek_jumlah = $cek_jumlah + $value->qty;
            }
        }

        $nota_jual = NotaJual::where('id', $nota_jual_id)->first();
        $sj_keluar_header = SJKeluarHeader::where('id', $nota_jual->sj_keluar_header_id)->first();

        $sj_keluar_detail = SJKeluarDetail::where('produk_id', $produk_id)->where('jenis_barang_id', $jenis_barang_id)->where('serial_number', $serial_number)->groupBy('produk_id', 'jenis_barang_id', 'serial_number')->selectRaw('tran_sj_keluar_detail.*, sum(jumlah) as jumlah_total')->first();      

        if($sj_keluar_detail->jumlah_total < $cek_jumlah)
        {
            echo '<div class="alert alert-danger flat">';
                echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
                  echo '<span aria-hidden="true">&times;</span>';
                echo '</button>';
                echo 'Jumlah tidak sesuai';
            echo '</div>';
        }
        else
        {
            $data = array('id'          => $jenis.$produk_id."---".$serial_number."---".$gudang_id, 
                  'name'        => $produk->nama, 
                  'qty'         => $jumlah, 
                  'price'       => $harga, 
                  'options'     => array('size' => 'large'));
            Cart::add($data);
        }

        $cart_content = Cart::content(1);
        
        return view('pages.transaksi.ajax_retur_admin_cart', compact('cart_content'));
    }

    public function retur_cart_hapus()
    {
        $rowId = Input::get('rowId');
        Cart::remove($rowId);
        $cart_content = Cart::content(1);
        
        return view('pages.transaksi.ajax_retur_admin_cart', compact('cart_content'));
    }

    public function retur_jual_checkout(Request $request)
    {       
        $this->validate($request, [
            'gudang_id' => 'required',
            'no_retur' => 'required',
            'nota_jual_id' => 'required',
            'pelanggan_id' => 'required',
        ]);

        //output berupa Nota Retur Jual. Nomor nota disimpan di No_nota rj_header dan invoice detail

        /* RJ HEADER */
        $no_retur       = Input::get('no_retur');
        $nota_jual_id   = Input::get('nota_jual_id');
        $pelanggan_id   = Input::get('pelanggan_id');
        $potongan_retur = Input::get('potongan_retur');
        $catatan        = Input::get('catatan');
        $gudang_id      = Input::get('gudang_id');

        $nota_jual = NotaJual::where('id', $nota_jual_id)->first();
        $sj_keluar_header = SJKeluarHeader::where('id', $nota_jual->sj_keluar_header_id)->first();

        date_default_timezone_set("Asia/Jakarta");
        $tanggal        = date("Y-m-d G:i:s");
        
        $rj_header = new RJHeader;

        if(empty($potongan_retur))
        {
            $potongan_retur = 0;
        }
        else{
            $potongan_retur   = str_replace(".", "", $potongan_retur);
        }

        $rj_header->nota_jual_id            = $nota_jual_id;
        $rj_header->no_nota                 = $no_retur;
        $rj_header->pelanggan_id            = $pelanggan_id;
        $rj_header->tanggal                 = $tanggal;
        $rj_header->potongan_retur          = $potongan_retur;
        $rj_header->total_retur             = 0; //sementara 0
        $rj_header->catatan                 = $catatan;
        $rj_header->is_lunas                = 0;
        $rj_header->save();    

        $rj_header_id = $rj_header->id;    

        /* RJ DETAIL */

        $cart_content = Cart::content(1);

        $total_retur = 0;

        foreach ($cart_content as $cart) 
        {
            if(substr($cart->id, 0, 3) == "PRD"){
                $cart_id            = substr($cart->id, 3); 
                $cart_id            = explode("---", $cart_id);
                $produk_id          = $cart_id[0];
                $serial_number      = $cart_id[1];
                //$gudang_id          = $cart_id[2];
                $jumlah           = $cart->qty;
                $harga              = $cart->price;
                $jenis_barang_id    = 1;

                $produk             = Produk::where('id', $produk_id)->first();
                $deskripsi          = $produk->deskripsi;

                //tambah stok di produk, produk_gudang, produk_serial_number
                $produk->stok   = $produk->stok + $jumlah;
                $produk->save();

                $produk_gudang = ProdukGudang::where('produk_id', $produk_id)->where('gudang_id', $gudang_id)->first();
                $produk_gudang->stok    = $produk_gudang->stok +  $jumlah;
                $produk_gudang->save();

                $produk_serial_number = ProdukSerialNumber::where('produk_id', $produk_id)->where('gudang_id', $gudang_id)->where('serial_number', $serial_number)->first();

                if(empty($produk_serial_number)){
                    //jika kosong maka buat baru
                    $produk_serial_number = new ProdukSerialNumber;
                    $produk_serial_number->gudang_id        = $gudang_id;
                    $produk_serial_number->produk_id        = $produk_id;
                    $produk_serial_number->serial_number    = $serial_number;
                    $produk_serial_number->stok             = $jumlah;
                }
                else{
                    $produk_serial_number->stok = $produk_serial_number->stok + $jumlah;
                    $produk_serial_number->save();
                }  
            }
            elseif (substr($cart->id, 0, 3) == "PKT") {
                $cart_id            = substr($cart->id, 3); 
                $cart_id            = explode("---", $cart_id);
                $produk_id          = $cart_id[0];
                $serial_number      = $cart_id[1];
                //$gudang_id          = $cart_id[2];
                $jumlah           = $cart->qty;
                $harga              = $cart->price;
                $jenis_barang_id    = 3;

                $paket              = Paket::where('id', $produk_id)->first();
                $deskripsi          = $paket->nama;

                //tambah stok di paket, produk_gudang
                $paket->stok   = $paket->stok + $jumlah;
                $paket->save();

                $paket_gudang = PaketGudang::where('paket_id', $produk_id)->where('gudang_id', $gudang_id)->first();
                $paket_gudang->stok    = $paket_gudang->stok +  $jumlah;
                $paket_gudang->save();
            }

            $rj_detail = new RJDetail;

            $rj_detail->rj_header_id    = $rj_header_id;
            $rj_detail->gudang_id       = $gudang_id;
            $rj_detail->produk_id       = $produk_id;
            $rj_detail->jenis_barang_id = $jenis_barang_id;
            $rj_detail->serial_number   = $serial_number;
            $rj_detail->jumlah        = $jumlah;
            $rj_detail->deskripsi       = $deskripsi;
            $rj_detail->harga           = $harga;
            $rj_detail->save();

            $total_retur = $total_retur + $harga;
        }

        $total_tagihan = $total_retur - $potongan_retur;

        $rj_header = RJHeader::where('id', $rj_header_id)->first();
        $rj_header->total_retur     = $total_retur;
        $rj_header->total_tagihan   = $total_tagihan;
        $rj_header->save();

        Cart::destroy();

        return redirect('/nota_retur_jual/'.$rj_header_id);
    }

    public function nota_retur_jual($rj_header_id)
    {
        $rj_header      = RJHeader::with('pelanggan')->where('id', $rj_header_id)->first();
        $rj_detail_obj  = RJDetail::where('rj_header_id', $rj_header_id)->get();
        
        $nota_jual      = DB::table('tran_nota_jual')->where('id', $rj_header->nota_jual_id)->get();
        
        $ind = 0;
        foreach($nota_jual as $r_nota_jual){
            $invoice_detail['no_nota'] = $r_nota_jual->no_nota;
            $invoice_detail['jatuh_tempo'] = $r_nota_jual->jatuh_tempo;
        }
        
        $perusahaan     = Perusahaan::with('kota')->first();
        $kota_perusahaan = strtoupper($perusahaan->kota->nama);

        if(strpos($kota_perusahaan, 'KABUPATEN') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KABUPATEN', '', $kota_perusahaan)));
        }
        elseif(strpos($kota_perusahaan, 'KOTA') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KOTA', '', $kota_perusahaan)));
        }

        //dapatkan detail data produk, paket di RJDetail
        $rj_detail = [];
        foreach ($rj_detail_obj as $key => $value) {
            if($value->jenis_barang_id == 1){
                $produk = Produk::where('id', $value->produk_id)->first();
                $rj_detail[$key]['nama']    = $produk->nama;
                $rj_detail[$key]['satuan']  = $produk->satuan;
            }
            elseif($value->jenis_barang_id == 2) {
                $hadiah = Hadiah::where('id', $value->produk_id)->first();
                $rj_detail[$key]['nama']    = $hadiah->nama;
                $rj_detail[$key]['satuan']  = $hadiah->satuan;
            }
            elseif($value->jenis_barang_id == 3) {
                $paket = Paket::where('id', $value->produk_id)->first();
                $rj_detail[$key]['nama']    = $paket->nama;
                $rj_detail[$key]['satuan']  = "paket";
            }

            $rj_detail[$key]['serial_number']   = $value->serial_number;
            $rj_detail[$key]['harga']           = $value->harga;
            $rj_detail[$key]['jumlah']        = $value->jumlah;
        }

        $terbilang = $rj_header->total_tagihan;

        Cart::destroy();

        $judul  = "Retur Jual";
        $general['title']       = $judul;
        $general['menu1']       = "Transaksi";
        $general['menu2']       = "Retur Jual";
        $general['menu3']       = $general['title'];
        $general['kode_menu']   = 53;  

        $pelanggan['nama']   = $rj_header->pelanggan->nama;
        $pelanggan['alamat'] = $rj_header->pelanggan->alamat;

        $kota = strtoupper($rj_header->pelanggan->kota->nama);

        if(strpos($kota, 'KABUPATEN') !== false){
            $kota = str_replace('KABUPATEN', '', $kota);
        }
        elseif(strpos($kota, 'KOTA') !== false){
            $kota = str_replace('KOTA', '', $kota);
        }

        $pelanggan['alamat'] = $pelanggan['alamat'].', '.$kota;
        $pelanggan['telp']   = $rj_header->pelanggan->telp1;

        date_default_timezone_set("Asia/Jakarta"); 
        $print_time = new DateTime;

        return view('pages.transaksi.retur_jual.nota_retur_jual', compact('general', 'rj_header', 'rj_detail', 'invoice_detail', 'invoice', 'perusahaan', 'judul', 'terbilang', 'pelanggan', 'print_time', 'kota_perusahaan'));  
    }

    public function nota_retur_jual_print($rj_header_id)
    {
        $judul                  = "Retur Jual";
        $rj_header              = RJHeader::with('pelanggan')->where('id', $rj_header_id)->first();
        $rj_detail_obj              = RJDetail::with('produk', 'hadiah', 'paket')->where('rj_header_id', $rj_header_id)->get();

        $perusahaan             = Perusahaan::with('kota')->first();
        $kota_perusahaan = strtoupper($perusahaan->kota->nama);

        if(strpos($kota_perusahaan, 'KABUPATEN') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KABUPATEN', '', $kota_perusahaan)));
        }
        elseif(strpos($kota_perusahaan, 'KOTA') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KOTA', '', $kota_perusahaan)));
        }

        $rj_detail = [];
        foreach ($rj_detail_obj as $key => $value) {
            if($value->jenis_barang_id == 1){
                $produk = Produk::where('id', $value->produk_id)->first();
                $rj_detail[$key]['nama']    = $produk->nama;
                $rj_detail[$key]['satuan']  = $produk->satuan;
            }
            elseif($value->jenis_barang_id == 2) {
                $hadiah = Hadiah::where('id', $value->produk_id)->first();
                $rj_detail[$key]['nama']    = $hadiah->nama;
                $rj_detail[$key]['satuan']  = $hadiah->satuan;
            }
            elseif($value->jenis_barang_id == 3) {
                $paket = Paket::where('id', $value->produk_id)->first();
                $rj_detail[$key]['nama']    = $paket->nama;
                $rj_detail[$key]['satuan']  = "paket";
            }

            $rj_detail[$key]['serial_number']   = $value->serial_number;
            $rj_detail[$key]['harga']           = $value->harga;
            $rj_detail[$key]['jumlah']        = $value->jumlah;
        }

        $terbilang = $rj_header->total_tagihan;

        date_default_timezone_set("Asia/Jakarta"); 
        $print_time = new DateTime;

        $pelanggan['nama']   = $rj_header->pelanggan->nama;
        $pelanggan['alamat'] = $rj_header->pelanggan->alamat;

        $kota = strtoupper($rj_header->pelanggan->kota->nama);

        if(strpos($kota, 'KABUPATEN') !== false){
            $kota = str_replace('KABUPATEN', '', $kota);
        }
        elseif(strpos($kota, 'KOTA') !== false){
            $kota = str_replace('KOTA', '', $kota);
        }

        $pelanggan['alamat'] = $pelanggan['alamat'].', '.$kota;
        $pelanggan['telp']   = $rj_header->pelanggan->telp1;

        return view('pages.transaksi.retur_jual.nota_retur_jual_print', compact('judul', 'rj_header', 'rj_detail', 'perusahaan', 'print_time', 'pelanggan', 'kota_perusahaan'));
    }

    public function nota_retur_jual_pdf($rj_header_id)
    {
        $judul                  = "Retur Jual";
        $rj_header              = RJHeader::with('pelanggan')->where('id', $rj_header_id)->first();
        $rj_detail_obj              = RJDetail::with('produk', 'hadiah', 'paket')->where('rj_header_id', $rj_header_id)->get();

        $perusahaan             = Perusahaan::with('kota')->first();
        $kota_perusahaan = strtoupper($perusahaan->kota->nama);

        if(strpos($kota_perusahaan, 'KABUPATEN') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KABUPATEN', '', $kota_perusahaan)));
        }
        elseif(strpos($kota_perusahaan, 'KOTA') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KOTA', '', $kota_perusahaan)));
        }

        $rj_detail = [];
        foreach ($rj_detail_obj as $key => $value) {
            if($value->jenis_barang_id == 1){
                $produk = Produk::where('id', $value->produk_id)->first();
                $rj_detail[$key]['nama']    = $produk->nama;
                $rj_detail[$key]['satuan']  = $produk->satuan;
            }
            elseif($value->jenis_barang_id == 2) {
                $hadiah = Hadiah::where('id', $value->produk_id)->first();
                $rj_detail[$key]['nama']    = $hadiah->nama;
                $rj_detail[$key]['satuan']  = $hadiah->satuan;
            }
            elseif($value->jenis_barang_id == 3) {
                $paket = Paket::where('id', $value->produk_id)->first();
                $rj_detail[$key]['nama']    = $paket->nama;
                $rj_detail[$key]['satuan']  = "paket";
            }

            $rj_detail[$key]['serial_number']   = $value->serial_number;
            $rj_detail[$key]['harga']           = $value->harga;
            $rj_detail[$key]['jumlah']        = $value->jumlah;
        }

        $terbilang = $rj_header->total_tagihan;

        date_default_timezone_set("Asia/Jakarta"); 
        $print_time = new DateTime;

        $pelanggan['nama']   = $rj_header->pelanggan->nama;
        $pelanggan['alamat'] = $rj_header->pelanggan->alamat;

        $kota = strtoupper($rj_header->pelanggan->kota->nama);

        if(strpos($kota, 'KABUPATEN') !== false){
            $kota = str_replace('KABUPATEN', '', $kota);
        }
        elseif(strpos($kota, 'KOTA') !== false){
            $kota = str_replace('KOTA', '', $kota);
        }

        $pelanggan['alamat'] = $pelanggan['alamat'].', '.$kota;
        $pelanggan['telp']   = $rj_header->pelanggan->telp1;

        $pdf = PDF::loadView('pages.transaksi.retur_jual.nota_retur_jual_pdf', compact('judul', 'rj_header', 'rj_detail', 'perusahaan', 'print_time', 'pelanggan', 'kota_perusahaan'))->setPaper('a4', 'landscape');

        return $pdf->stream();
    }

    public function detail_retur_jual($rj_header_id)
    {
        $rj_header      = RJHeader::with('pelanggan')->where('id', $rj_header_id)->first();
        $rj_detail_obj  = RJDetail::where('rj_header_id', $rj_header_id)->get();

        $general['title']       = "Retur Jual #".$rj_header->no_nota;
        $general['menu1']       = "Transaksi";
        $general['menu2']       = "Retur Jual";
        $general['menu3']       = $general['title'];
        $general['kode_menu']   = 53;

        $perusahaan     = Perusahaan::with('kota')->first();

        //dapatkan detail data produk, paket di RJDetail
        $rj_detail = [];
        foreach ($rj_detail_obj as $key => $value) {
            if($value->jenis_barang_id == 1){
                $produk = Produk::where('id', $value->produk_id)->first();
                $rj_detail[$key]['nama']    = $produk->nama;
                $rj_detail[$key]['satuan']  = $produk->satuan;
            }
            elseif($value->jenis_barang_id == 2) {
                $hadiah = Hadiah::where('id', $value->produk_id)->first();
                $rj_detail[$key]['nama']    = $hadiah->nama;
                $rj_detail[$key]['satuan']  = $hadiah->satuan;
            }
            elseif($value->jenis_barang_id == 3) {
                $paket = Paket::where('id', $value->produk_id)->first();
                $rj_detail[$key]['nama']    = $paket->nama;
                $rj_detail[$key]['satuan']  = "paket";
            }

            $rj_detail[$key]['serial_number']   = $value->serial_number;
            $rj_detail[$key]['harga']           = $value->harga;
            $rj_detail[$key]['jumlah']        = $value->jumlah;
        }

        $terbilang = $rj_header->total_tagihan;

        return view('pages.transaksi.detail_retur_jual', compact('general', 'rj_header', 'rj_detail', 'invoice_detail', 'invoice'));
    }
}
