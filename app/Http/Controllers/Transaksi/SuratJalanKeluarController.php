<?php

namespace App\Http\Controllers\Transaksi;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Gloudemans\Shoppingcart\Facades\Cart;
use Input;
use Auth;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Session;
use Promocodes;
use Response;
use DateTime;
use PDF;
use Hashids\Hashids;
use Validator;
use Illuminate\Http\RedirectResponse;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use App\User;
use App\Models\Perusahaan;
use App\Models\Gudang\Gudang;
use App\Models\Gudang\ProdukGudang;
use App\Models\Gudang\HadiahGudang;
use App\Models\Produk\Produk;
use App\Models\Produk\Hadiah;
use App\Models\Produk\ProdukSerialNumber;
use App\Models\Supplier\Supplier;
use App\Models\TransaksiPenjualan\InvoicePenjualan;
use App\Models\TransaksiPenjualan\BayarInvoicePenjualan;
use App\Models\TransaksiPenjualan\SOHeader;
use App\Models\TransaksiPenjualan\SODetail;
use App\Models\TransaksiPenjualan\SOVoucher;
use App\Models\TransaksiPenjualan\SOAlamatPengiriman;
use App\Models\TransaksiPenjualan\SJKeluarHeader;
use App\Models\TransaksiPenjualan\SJKeluarDetail;
use App\Models\PaketProduk\PaketProduk;
use App\Models\PaketProduk\Paket;

class SuratJalanKeluarController extends Controller
{
    public function surat_jalan_keluar()
    {
        $general['title']       = "Surat Jalan Keluar";
        $general['menu1']       = "Transaksi";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 59;

        $sj_keluar_header  = SJKeluarHeader::with('so_header', 'pelanggan')->orderBy('tanggal', 'desc')->get();

        return view('pages.transaksi.surat_jalan_keluar', compact('general', 'sj_keluar_header'));
    }

    public function tambah_surat_jalan_keluar()
    {
        $general['title']       = "Tambah Surat Jalan Keluar";
        $general['menu1']       = "Transaksi";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 59;

        //ambil so yang belum punya surat jalan
        $so_header  = SOHeader::with('pelanggan')->where('flag_sj_keluar', 0)->get();
        $gudang     = Gudang::where('is_aktif', 1)->get();

        date_default_timezone_set("Asia/Jakarta");
        $tanggal        = new DateTime;

        $nomor_invoice      = DB::table('tref_nomor_invoice')->where('id', 7)->first();
        $format_invoice     = $nomor_invoice->format;
        $ori_format_invoice = $format_invoice;

        $cek_dd = strpos($format_invoice, 'DD');
        if(!empty($cek_dd))
        {
            $format_invoice = str_replace('DD', $tanggal->format('d'), $format_invoice);
        }

        $cek_mm = strpos($format_invoice, 'MM');
        if(!empty($cek_mm))
        {
            $format_invoice = str_replace('MM', $tanggal->format('m'), $format_invoice);
        }

        $cek_yyyy = strpos($format_invoice, 'YYYY');
        if(!empty($cek_yyyy))
        {
            $format_invoice = str_replace('YYYY', $tanggal->format('Y'), $format_invoice);
        }

        $cek_yy = strpos($format_invoice, 'YY');
        if(!empty($cek_yy))
        {
            $format_invoice = str_replace('YY', $tanggal->format('y'), $format_invoice);
        }

        //cek maksimal 10
        $number = '##########';

        for ($i=9; $i > 0; $i--) { 
            $number     = substr($number, 0, $i);
            $cek_number = strpos($format_invoice, $number);
            $nol        = "";
            if(!empty($cek_number))
            {
                $sj_keluar_header    = SJKeluarHeader::orderBy('id', 'desc')->first();
                if(empty($sj_keluar_header))
                    $get_num = 1;
                else
                    $get_num        = (int)$sj_keluar_header->id + 1;
                
                for ($j=0; $j < $i - strlen($get_num); $j++) { 
                    $nol = $nol."0";
                }

                $get_num = $nol.$get_num;
                $format_invoice = str_replace($number, $get_num, $format_invoice);
                break;
            }
        }

        $no_surat_jalan     = $format_invoice;

        return view('pages.transaksi.tambah_surat_jalan_keluar', compact('general', 'so_header', 'no_surat_jalan', 'gudang'));
    }

    public function sjk_get_so_detail()
    {
        $so_header_id = Input::get('so_header_id');

        $so_detail  = SODetail::with('produk', 'hadiah', 'paket')->where('so_header_id', $so_header_id)->get();
        
        return view('pages.transaksi.ajax_sjk_get_so_detail', compact('so_detail'));
    }

    public function sjm_get_arr_so_detail()
    {
        $so_header_id     = Input::get('so_header_id');
        $so_detail      = SODetail::where('so_header_id', $so_header_id)->get();
        $arr_so_detail  = [];

        foreach ($so_detail as $key => $val) {
            $arr_so_detail[$key]['produk_id']       = $val->produk_id;
            $arr_so_detail[$key]['jenis_barang_id'] = $val->jenis_barang_id;
            $arr_so_detail[$key]['jumlah']        = $val->jumlah - $val->jumlah_terkirim;
        }

        foreach($arr_so_detail as $key => $val){
            echo '<input type="hidden" id="arr_so_detail'.$val['produk_id'].'-'.$val['jenis_barang_id'].'" value="'.$val['jumlah'].'">';
        }
    }

    public function sjk_get_arr_packing_list()
    {
        $so_header_id   = Input::get('so_header_id');
        $so_detail      = SODetail::where('so_header_id', $so_header_id)->get();

        $jumlah_pesanan     = 0;
        $arr_packing_list   = [];

        foreach ($so_detail as $key => $val) {
            $jumlah_pesanan = $jumlah_pesanan + $val->jumlah - $val->jumlah_terkirim;
        }

        for ($i=0; $i < $jumlah_pesanan; $i++) { 
            $arr_packing_list[$i]['id']                = $i;
            $arr_packing_list[$i]['gudang_id']         = "";
            $arr_packing_list[$i]['produk_id']         = "";
            $arr_packing_list[$i]['jenis_barang_id']   = "";
            $arr_packing_list[$i]['serial_number']     = "";
        }

        foreach($arr_packing_list as $key => $val) {
            echo '<input type="hidden" name="arr_packing_list_id'.$key.'" id="arr_packing_list_id'.$key.'" value="'.$val['id'].'">';
            echo '<input type="hidden" name="arr_packing_list_gudang_id'.$key.'" id="arr_packing_list_gudang_id'.$key.'" value="'.$val['gudang_id'].'">';
            echo '<input type="hidden" name="arr_packing_list_produk_id'.$key.'" id="arr_packing_list_produk_id'.$key.'" value="'.$val['produk_id'].'">';
            echo '<input type="hidden" name="arr_packing_list_jenis_barang_id'.$key.'" id="arr_packing_list_jenis_barang_id'.$key.'" value="'.$val['jenis_barang_id'].'">';
            echo '<input type="hidden" name="arr_packing_list_serial_number'.$key.'" id="arr_packing_list_serial_number'.$key.'" value="'.$val['serial_number'].'">';
        }
    }

    public function sjk_get_produk_gudang()
    {
        $so_header_id   = Input::get('so_header_id');
        $gudang_id      = Input::get('gudang_id');

        //INI MASIH SALAH, NANTI CARI PRODUK PER GUDANGNYA

        $so_detail  = SODetail::with('produk', 'hadiah', 'paket')->where('so_header_id', $so_header_id)->get();
                        
        echo '<option value="">-- Pilih Produk --</option>';
        foreach($so_detail as $val){
            if(!empty($val->produk))
                echo '<option value="'.$val->produk_id.','.$val->produk->nama.','.$val->jenis_barang_id.'">'.$val->produk->nama.'</option>';
            elseif(!empty($val->hadiah))
                echo '<option value="'.$val->produk_id.','.$val->hadiah->nama.','.$val->jenis_barang_id.'">'.$val->hadiah->nama.'</option>';
            elseif(!empty($val->paket))
                echo '<option value="'.$val->produk_id.','.$val->paket->nama.','.$val->jenis_barang_id.'">'.$val->paket->nama.'</option>';
        }
    }

    public function get_so_produk_serial_number()
    {
        $gudang_id  = Input::get('gudang_id');
        $produk_id  = Input::get('produk_id');

        $produk_serial_number = ProdukSerialNumber::with('produk', 'gudang')->where('gudang_id', $gudang_id)->where('produk_id', $produk_id)->where('stok', '>', 0)->get();
        
        return view('pages.transaksi.ajax_so_produk_serial_number', compact('produk_serial_number'));
    }

    public function surat_jalan_keluar_proses(Request $request)
    {
        //hapus nilai di so_detail, ganti dengan nilai arr_serial number
        //update pula nilai stok di produk, produk_gudang dan produk_serial_number
        $no_surat_jalan     = Input::get('no_surat_jalan_keluar');
        $so_header_id       = Input::get('so_header_id');
        $jumlah_pesanan     = Input::get('jumlah_pesanan');

        $so_header = SOHeader::where('id', $so_header_id)->first();

        date_default_timezone_set("Asia/Jakarta");
        $tanggal        = new DateTime;

        $sj_keluar_header = new SJKeluarHeader;
        $sj_keluar_header->so_header_id     = $so_header_id;
        $sj_keluar_header->pelanggan_id     = $so_header->pelanggan_id;
        $sj_keluar_header->no_surat_jalan   = $no_surat_jalan;
        $sj_keluar_header->tanggal          = $tanggal;
        $sj_keluar_header->flag_nota_jual   = 0;
        $sj_keluar_header->ppn              = $so_header->ppn;
        $sj_keluar_header->save();

        $ppn = $so_header->ppn;

        $sj_keluar_header_id = $sj_keluar_header->id;

        $total_harga = 0;

        for ($i=0; $i < $jumlah_pesanan; $i++) { 
            $arr_packing_list_gudang_id        = Input::get('arr_packing_list_gudang_id'.$i);
            $arr_packing_list_produk_id        = Input::get('arr_packing_list_produk_id'.$i);
            $arr_packing_list_jenis_barang_id  = Input::get('arr_packing_list_jenis_barang_id'.$i);
            $arr_packing_list_serial_number    = Input::get('arr_packing_list_serial_number'.$i);

            //jika isi serial number kosong, maka alokasikan jumlah ke NULL kemudian lewati proses
            if(empty($arr_packing_list_serial_number)){
                continue;
            }

            //tambah jumlah_terkirim di so_detail
            $so_detail = SODetail::where('so_header_id', $so_header_id)->where('produk_id', $arr_packing_list_produk_id)
            ->where('jenis_barang_id', $arr_packing_list_jenis_barang_id)->first();
            $so_detail->jumlah_terkirim = $so_detail->jumlah_terkirim + 1;
            $so_detail->save();

            $total_harga = $total_harga + $so_detail->harga;

            //cek surat jalan sudah ada atau belum, jika sudah maka cukup tambah quantity
            $sj_keluar_detail = SJKeluarDetail::where('sj_keluar_header_id', $sj_keluar_header_id)
            ->where('produk_id', $arr_packing_list_produk_id)
            ->where('jenis_barang_id', $arr_packing_list_jenis_barang_id)->where('gudang_id', $arr_packing_list_gudang_id)
            ->where('serial_number', $arr_packing_list_serial_number)->first();

            if(!empty($sj_keluar_detail)){
                $sj_keluar_detail->jumlah = $sj_keluar_detail->jumlah + 1;
                $sj_keluar_detail->save();
            }
            else{
                $sj_keluar_detail = new SJKeluarDetail;
                $sj_keluar_detail->sj_keluar_header_id  = $sj_keluar_header_id;
                $sj_keluar_detail->produk_id            = $arr_packing_list_produk_id;
                $sj_keluar_detail->jenis_barang_id      = $arr_packing_list_jenis_barang_id;
                $sj_keluar_detail->gudang_id            = $arr_packing_list_gudang_id;
                $sj_keluar_detail->serial_number        = $arr_packing_list_serial_number;
                $sj_keluar_detail->jumlah               = 1;

                //get harga dari so_detail
                $sj_keluar_detail->harga                = $so_detail->harga;

                $sj_keluar_detail->harga_retail         = $so_detail->harga_retail;
                
                $sj_keluar_detail->save();
            }

            //update stok produk/hadiah/paket
            if($arr_packing_list_jenis_barang_id == 1){
                //untuk produk update stok di tmst_produk, tran_produk_gudang, tran_produk_serial_number
                $produk = Produk::where('id', $arr_packing_list_produk_id)->first();
                $produk->stok           = $produk->stok - 1;
                $produk->stok_dipesan   = $produk->stok_dipesan - 1;
                $produk->save();

                $produk_gudang = ProdukGudang::where('produk_id', $arr_packing_list_produk_id)->where('gudang_id', $arr_packing_list_gudang_id)->first();

                $produk_gudang->stok        = $produk_gudang->stok - 1;
                $produk_gudang->save();

                $produk_serial_number = ProdukSerialNumber::where('produk_id', $arr_packing_list_produk_id)->where('gudang_id', $arr_packing_list_gudang_id)->where('serial_number', $arr_packing_list_serial_number)->first();

                $produk_serial_number->stok             = $produk_serial_number->stok - 1;
                $produk_serial_number->save();
            }
            elseif($arr_packing_list_jenis_barang_id == 2){
                //untuk hadiah update stok di tmst_hadiah, tran_hadiah_gudang
                $hadiah = Hadiah::where('id', $arr_packing_list_produk_id)->first();
                $hadiah->stok           = $hadiah->stok - 1;
                $hadiah->stok_dipesan   = $hadiah->stok_dipesan - 1;
                $hadiah->save();

                $hadiah_gudang = HadiahGudang::where('hadiah_id', $arr_packing_list_produk_id)->where('gudang_id', $arr_packing_list_gudang_id)->first();

                $hadiah_gudang->stok        = $hadiah_gudang->stok - 1;
                $hadiah_gudang->save();
            }
            elseif($arr_packing_list_jenis_barang_id == 3){
                //untuk paket update stok di tmst_paket, tran_paket_gudang
                $paket = Paket::where('id', $arr_packing_list_produk_id)->first();
                $paket->stok            = $paket->stok - 1;
                $paket->stok_dipesan    = $paket->stok_dipesan - 1;
                $paket->save();

                $paket_gudang = PaketGudang::where('paket_id', $arr_packing_list_produk_id)->where('gudang_id', $arr_packing_list_gudang_id)->first();

                $paket_gudang->stok        = $paket_gudang->stok - 1;
                $paket_gudang->save();
            }
        }

        //AMBIL VOUCHER
        $so_voucher = SOVoucher::where('so_header_id', $so_header_id)->get();
        $total_voucher = 0;

        foreach ($so_voucher as $key => $value) {
            $total_voucher = $total_voucher + $value->nominal;
            $this_so_voucher = SOVoucher::where('id', $value->id)->first();
            $this_so_voucher->is_dipakai = 1;
            $this_so_voucher->save();
        }

        //CEK APAKAH SURAT JALAN SUDAH SEMUA ATAU BELUM
        $flag_sj_keluar = 1;

        $so_detail = SODetail::where('so_header_id', $so_header_id)->get();

        foreach ($so_detail as $key => $value) {
            if($value->jumlah > $value->jumlah_terkirim){
                $flag_sj_keluar = 0;
                break;
            }
        }

        $so_detail = SOHeader::where('id', $so_header_id)->first();
        $so_detail->flag_sj_keluar = $flag_sj_keluar;
        $so_detail->save();

        $harga_ppn = $total_harga*$ppn/100;

        //update SJK
        $sj_keluar_header = SJKeluarHeader::where('id', $sj_keluar_header_id)->first();
        $sj_keluar_header->total_harga      = $total_harga;
        $sj_keluar_header->total_voucher    = $total_voucher;
        $sj_keluar_header->total_tagihan    = $total_harga + $harga_ppn - $total_voucher;
        $sj_keluar_header->save();

        return redirect('/nota_surat_jalan_keluar/'.$sj_keluar_header_id);
    }

    public function nota_surat_jalan_keluar($sj_keluar_header_id)
    {
        $sj_keluar_detail = SJKeluarDetail::with('produk', 'hadiah', 'paket')->where('sj_keluar_header_id', $sj_keluar_header_id)->groupBy('produk_id', 'jenis_barang_id')->selectRaw('tran_sj_keluar_detail.*, sum(jumlah) as jumlah_total')->get();

        foreach ($sj_keluar_detail as $key => $value) {
            $produk_sn[$key] = SJKeluarDetail::where('sj_keluar_header_id', $sj_keluar_header_id)->where('produk_id', $value->produk_id)->where('jenis_barang_id', $value->jenis_barang_id)->get();
        }

        $judul  = "Surat Jalan Keluar";
        $general['title']       = $judul;
        $general['menu1']       = "Transaksi";
        $general['menu2']       = "Surat Jalan Keluar";
        $general['menu3']       = $general['title'];
        $general['kode_menu']   = 59;  

        $perusahaan = Perusahaan::with('kota')->first();
        $kota_perusahaan = strtoupper($perusahaan->kota->nama);

        if(strpos($kota_perusahaan, 'KABUPATEN') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KABUPATEN', '', $kota_perusahaan)));
        }
        elseif(strpos($kota_perusahaan, 'KOTA') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KOTA', '', $kota_perusahaan)));
        }

        date_default_timezone_set("Asia/Jakarta"); 
        $print_time = new DateTime;

        $sj_keluar_header = SJKeluarHeader::with('pelanggan')->where('id', $sj_keluar_header_id)->first();

        $so_header = SOHeader::where('id', $sj_keluar_header->so_header_id)->first();
        $so_alamat_pengiriman   = SOAlamatPengiriman::where('so_header_id', $so_header->id)->first();

        $pelanggan['nama']   = $sj_keluar_header->pelanggan->nama;
        $pelanggan['alamat'] = $sj_keluar_header->pelanggan->alamat;

        $kota = strtoupper($sj_keluar_header->pelanggan->kota->nama);

        if(strpos($kota, 'KABUPATEN') !== false){
            $kota = str_replace('KABUPATEN', '', $kota);
        }
        elseif(strpos($kota, 'KOTA') !== false){
            $kota = str_replace('KOTA', '', $kota);
        }

        $pelanggan['alamat'] = $pelanggan['alamat'].', '.$kota;
        $pelanggan['telp']   = $sj_keluar_header->pelanggan->telp1;
        
        return view('pages.transaksi.surat_jalan_keluar.nota_surat_jalan_keluar', compact('general', 'sj_keluar_detail', 'sj_keluar_header', 'judul', 'perusahaan', 'so_header', 'produk_sn', 'print_time', 'so_alamat_pengiriman', 'pelanggan', 'kota_perusahaan'));
    }

    public function nota_surat_jalan_keluar_print($sj_keluar_header_id)
    {
        $sj_keluar_detail = SJKeluarDetail::with('produk', 'hadiah', 'paket')->where('sj_keluar_header_id', $sj_keluar_header_id)->groupBy('produk_id', 'jenis_barang_id')->selectRaw('tran_sj_keluar_detail.*, sum(jumlah) as jumlah_total')->get();

        foreach ($sj_keluar_detail as $key => $value) {
            $produk_sn[$key] = SJKeluarDetail::where('sj_keluar_header_id', $sj_keluar_header_id)->where('produk_id', $value->produk_id)->where('jenis_barang_id', $value->jenis_barang_id)->get();
        }

        $judul  = "Surat Jalan Keluar"; 

        $perusahaan = Perusahaan::with('kota')->first();
        $kota_perusahaan = strtoupper($perusahaan->kota->nama);

        if(strpos($kota_perusahaan, 'KABUPATEN') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KABUPATEN', '', $kota_perusahaan)));
        }
        elseif(strpos($kota_perusahaan, 'KOTA') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KOTA', '', $kota_perusahaan)));
        }

        date_default_timezone_set("Asia/Jakarta"); 
        $print_time = new DateTime;

        $sj_keluar_header = SJKeluarHeader::with('pelanggan')->where('id', $sj_keluar_header_id)->first();

        $so_header = SOHeader::where('id', $sj_keluar_header->so_header_id)->first();
        $so_alamat_pengiriman   = SOAlamatPengiriman::where('so_header_id', $so_header->id)->first();

        $pelanggan['nama']   = $sj_keluar_header->pelanggan->nama;
        $pelanggan['alamat'] = $sj_keluar_header->pelanggan->alamat;

        $kota = strtoupper($sj_keluar_header->pelanggan->kota->nama);

        if(strpos($kota, 'KABUPATEN') !== false){
            $kota = str_replace('KABUPATEN', '', $kota);
        }
        elseif(strpos($kota, 'KOTA') !== false){
            $kota = str_replace('KOTA', '', $kota);
        }

        $pelanggan['alamat'] = $pelanggan['alamat'].', '.$kota;
        $pelanggan['telp']   = $sj_keluar_header->pelanggan->telp1;
        
        return view('pages.transaksi.surat_jalan_keluar.nota_surat_jalan_keluar_print', compact('general', 'sj_keluar_detail', 'sj_keluar_header', 'judul', 'perusahaan', 'so_header', 'produk_sn', 'print_time', 'so_alamat_pengiriman', 'pelanggan', 'kota_perusahaan'));
    }
    public function nota_surat_jalan_keluar_pdf($sj_keluar_header_id)
    {
        $sj_keluar_detail = SJKeluarDetail::with('produk', 'hadiah', 'paket')->where('sj_keluar_header_id', $sj_keluar_header_id)->groupBy('produk_id', 'jenis_barang_id')->selectRaw('tran_sj_keluar_detail.*, sum(jumlah) as jumlah_total')->get();

        foreach ($sj_keluar_detail as $key => $value) {
            $produk_sn[$key] = SJKeluarDetail::where('sj_keluar_header_id', $sj_keluar_header_id)->where('produk_id', $value->produk_id)->where('jenis_barang_id', $value->jenis_barang_id)->get();
        }

        $judul  = "Surat Jalan Keluar"; 

        $perusahaan = Perusahaan::with('kota')->first();
        $kota_perusahaan = strtoupper($perusahaan->kota->nama);

        if(strpos($kota_perusahaan, 'KABUPATEN') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KABUPATEN', '', $kota_perusahaan)));
        }
        elseif(strpos($kota_perusahaan, 'KOTA') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KOTA', '', $kota_perusahaan)));
        }

        date_default_timezone_set("Asia/Jakarta"); 
        $print_time = new DateTime;

        $sj_keluar_header = SJKeluarHeader::with('pelanggan')->where('id', $sj_keluar_header_id)->first();

        $so_header = SOHeader::where('id', $sj_keluar_header->so_header_id)->first();
        $so_alamat_pengiriman   = SOAlamatPengiriman::where('so_header_id', $so_header->id)->first();

        $pelanggan['nama']   = $sj_keluar_header->pelanggan->nama;
        $pelanggan['alamat'] = $sj_keluar_header->pelanggan->alamat;

        $kota = strtoupper($sj_keluar_header->pelanggan->kota->nama);

        if(strpos($kota, 'KABUPATEN') !== false){
            $kota = str_replace('KABUPATEN', '', $kota);
        }
        elseif(strpos($kota, 'KOTA') !== false){
            $kota = str_replace('KOTA', '', $kota);
        }

        $pelanggan['alamat'] = $pelanggan['alamat'].', '.$kota;
        $pelanggan['telp']   = $sj_keluar_header->pelanggan->telp1;

        $pdf = PDF::loadView('pages.transaksi.surat_jalan_keluar.nota_surat_jalan_keluar_pdf', compact('general', 'sj_keluar_detail', 'sj_keluar_header', 'judul', 'perusahaan', 'so_header', 'produk_sn', 'print_time', 'so_alamat_pengiriman', 'pelanggan', 'kota_perusahaan'))->setPaper('a4', 'landscape');
        return $pdf->stream();
    }

    public function hapus_surat_jalan_keluar(Request $request)
    {
        $id = Input::get('id');

        $sj_keluar_header = SJKeluarHeader::where('id', $id)->first();

        $so_header = SOHeader::where('id', $sj_keluar_header->so_header_id)->first();
        $so_header->flag_sj_keluar = 0;
        $so_header->save();

        //kembalikan stok
        $sj_keluar_detail = SJKeluarDetail::where('sj_keluar_header_id', $id)->get();
        foreach ($sj_keluar_detail as $key => $value) {
            if($value->jenis_barang_id == 1){
                //update di produk
                $produk = Produk::where('id', $value->produk_id)->first();
                $produk->stok           = $produk->stok + $value->jumlah;
                $produk->stok_dipesan   = $produk->stok_dipesan + $value->jumlah;
                $produk->save();

                //update di produk_gudang
                $produk_gudang = ProdukGudang::where('produk_id', $value->produk_id)->where('gudang_id', $value->gudang_id)->first();
                $produk_gudang->stok = $produk_gudang->stok + $value->jumlah;
                $produk_gudang->save();

                //update di produk_serial_number
                $produk_serial_number = ProdukSerialNumber::where('produk_id', $value->produk_id)->where('gudang_id', $value->gudang_id)->where('serial_number', $value->serial_number)->first();
                $produk_serial_number->stok = $produk_serial_number->stok + $value->jumlah;
                $produk_serial_number->save();
            }

            if($value->jenis_barang_id == 2){
                //update di hadiah
                $hadiah = Hadiah::where('id', $value->produk_id)->first();
                $hadiah->stok           = $hadiah->stok + $value->jumlah;
                $hadiah->stok_dipesan   = $hadiah->stok_dipesan + $value->jumlah;
                $hadiah->save();

                //update di hadiah gudang
                $hadiah_gudang = HadiahGudang::where('hadiah_id', $value->produk_id)->where('gudang_id', $value->gudang_id)->first();
                $hadiah_gudang->stok = $hadiah_gudang->stok + $value->jumlah;
                $hadiah_gudang->save();
            }

            if($value->jenis_barang_id == 3){
                //update di paket produk
                $paket = Paket::where('id', $value->produk_id)->first();
                $paket->stok            = $paket->stok + $value->jumlah;
                $paket->stok_dipesan    = $paket->stok_dipesan + $value->jumlah;
                $paket->save();

                //update di paket gudang (sementara belum)
                // $hadiah_gudang = HadiahGudang::where('hadiah_id', $value->produk_id)->where('gudang_id', $value->gudang_id)->first();
                // $hadiah_gudang->stok = $hadiah_gudang->stok + $value->jumlah;
                // $hadiah_gudang->save();
            }

            //UPDATE JUMLAH TERKIRIM
            $so_detail = SODetail::where('so_header_id', $sj_keluar_header->so_header_id)->where('produk_id', $value->produk_id)->where('jenis_barang_id', $value->jenis_barang_id)->first();
            $so_detail->jumlah_terkirim = $so_detail->jumlah_terkirim - $value->jumlah;
            $so_detail->save();

            //hapus sj_keluar_detail
            $this_sj_keluar_detail = SJKeluarDetail::where('id', $value->id)->first();
            $this_sj_keluar_detail->delete();
        }

        $sj_keluar_header->delete();

        $request->session()->flash('message', 'Surat jalan masuk berhasil dihapus');
        return redirect('/surat_jalan_keluar');
    }
}
