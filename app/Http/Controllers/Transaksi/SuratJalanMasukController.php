<?php

namespace App\Http\Controllers\Transaksi;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Gloudemans\Shoppingcart\Facades\Cart;
use Input;
use Auth;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Session;
use Promocodes;
use Response;
use DateTime;
use PDF;
use Hashids\Hashids;
use Validator;
use Illuminate\Http\RedirectResponse;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use App\User;
use App\Models\Perusahaan;
use App\Models\Gudang\Gudang;
use App\Models\Gudang\ProdukGudang;
use App\Models\Gudang\HadiahGudang;
use App\Models\Produk\Produk;
use App\Models\Produk\Hadiah;
use App\Models\Produk\ProdukSerialNumber;
use App\Models\Supplier\Supplier;
use App\Models\TransaksiPembelian\POHeader;
use App\Models\TransaksiPembelian\PODetail;
use App\Models\TransaksiPembelian\PODiskon;
use App\Models\TransaksiPembelian\POAlamatPengiriman;
use App\Models\TransaksiPembelian\SJMasukHeader;
use App\Models\TransaksiPembelian\SJMasukDetail;

class SuratJalanMasukController extends Controller
{
    public function surat_jalan_masuk()
    {
        $general['title']       = "Surat Jalan Masuk";
        $general['menu1']       = "Transaksi";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 57;

        $sj_masuk_header  = SJMasukHeader::with('po_header', 'supplier')->orderBy('tanggal', 'desc')->get();

        return view('pages.transaksi.surat_jalan_masuk', compact('general', 'sj_masuk_header'));
    }

    public function tambah_surat_jalan_masuk()
    {
        $general['title']       = "Tambah Surat Jalan Masuk";
        $general['menu1']       = "Transaksi";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 57;

        //ambil po yang belum punya surat jalan
        $po_header  = POHeader::with('supplier')->where('flag_sj_masuk', 0)->get();
        $gudang     = Gudang::where('is_aktif', 1)->get();

        date_default_timezone_set("Asia/Jakarta");
        $tanggal        = new DateTime;

        $nomor_invoice      = DB::table('tref_nomor_invoice')->where('id', 2)->first();
        $format_invoice     = $nomor_invoice->format;
        $ori_format_invoice = $format_invoice;

        $cek_dd = strpos($format_invoice, 'DD');
        if(!empty($cek_dd))
        {
            $format_invoice = str_replace('DD', $tanggal->format('d'), $format_invoice);
        }

        $cek_mm = strpos($format_invoice, 'MM');
        if(!empty($cek_mm))
        {
            $format_invoice = str_replace('MM', $tanggal->format('m'), $format_invoice);
        }

        $cek_yyyy = strpos($format_invoice, 'YYYY');
        if(!empty($cek_yyyy))
        {
            $format_invoice = str_replace('YYYY', $tanggal->format('Y'), $format_invoice);
        }

        $cek_yy = strpos($format_invoice, 'YY');
        if(!empty($cek_yy))
        {
            $format_invoice = str_replace('YY', $tanggal->format('y'), $format_invoice);
        }

        //cek maksimal 10
        $number = '##########';

        for ($i=9; $i > 0; $i--) { 
            $number     = substr($number, 0, $i);
            $cek_number = strpos($format_invoice, $number);
            $nol        = "";
            if(!empty($cek_number))
            {
                $sj_masuk_header    = SJMasukHeader::orderBy('id', 'desc')->first();
                if(empty($sj_masuk_header))
                    $get_num = 1;
                else
                    $get_num        = (int)$sj_masuk_header->id + 1;
                
                for ($j=0; $j < $i - strlen($get_num); $j++) { 
                    $nol = $nol."0";
                }

                $get_num = $nol.$get_num;
                $format_invoice = str_replace($number, $get_num, $format_invoice);
                break;
            }
        }

        $no_surat_jalan     = $format_invoice;

        return view('pages.transaksi.tambah_surat_jalan_masuk', compact('general', 'po_header', 'gudang', 'no_surat_jalan'));
    }

    public function sjm_get_produk()
    {
        $po_header_id   = Input::get('po_header_id');

        $po_detail      = PODetail::with('produk', 'hadiah')->where('po_header_id', $po_header_id)->get();
        $produk     = [];

        foreach ($po_detail as $key => $val) {

            $produk[$key]['id']     = $val->produk_id;
            $produk[$key]['jenis'] = $val->jenis_barang_id;

            if($val->jenis_barang_id == 1){
                $get_produk = Produk::where('id', $val->produk_id)->first();
                $produk[$key]['nama']   = $get_produk->nama;
            }
            elseif($val->jenis_barang_id == 2){
                $get_hadiah = Hadiah::where('id', $val->produk_id)->first();
                $produk[$key]['nama']   = $get_hadiah->nama;
            }
        }

        echo '<option value="">-- Pilih Produk --</option>';
        foreach($produk as $val){
            echo '<option value="'.$val['id'].','.$val['nama'].','.$val['jenis'].'">'.$val['nama'].'</option>';
        }
    }

    public function sjm_get_po_detail()
    {
        $po_header_id   = Input::get('po_header_id');
        $po_detail      = PODetail::with('produk', 'hadiah')->where('po_header_id', $po_header_id)->get();
        
        return view('pages.transaksi.ajax_sjm_get_po_detail', compact('po_detail'));
    }

    public function sjm_get_arr_po_detail()
    {
        $po_header_id   = Input::get('po_header_id');
        $po_detail      = PODetail::with('produk', 'hadiah')->where('po_header_id', $po_header_id)->get();
        $arr_po_detail      = [];

        foreach ($po_detail as $key => $val) {
            $arr_po_detail[$key]['produk_id']       = $val->produk_id;
            $arr_po_detail[$key]['jenis_barang_id'] = $val->jenis_barang_id;
            $arr_po_detail[$key]['jumlah']          = $val->jumlah - $val->jumlah_terkirim;
        }

        foreach($arr_po_detail as $key => $val){
            echo '<input type="hidden" id="arr_po_detail'.$val['produk_id'].'-'.$val['jenis_barang_id'].'" value="'.$val['jumlah'].'">';
        }
    }

    public function sjm_get_arr_serial_number()
    {
        $po_header_id   = Input::get('po_header_id');
        $po_detail      = PODetail::with('produk', 'hadiah')->where('po_header_id', $po_header_id)->get();

        $arr_serial_number  = [];
        $jumlah_pesanan = 0;
        foreach ($po_detail as $key => $val) {
            $jumlah_pesanan = $jumlah_pesanan + $val->jumlah - $val->jumlah_terkirim;
        }

        for ($i=0; $i < $jumlah_pesanan; $i++) { 
            $arr_serial_number[$i]['id']                = $i;
            $arr_serial_number[$i]['gudang_id']         = "";
            $arr_serial_number[$i]['produk_id']         = "";
            $arr_serial_number[$i]['jenis_barang_id']   = "";
            $arr_serial_number[$i]['serial_number']     = "";
        }

        foreach($arr_serial_number as $key => $val) {
            echo '<input type="hidden" name="arr_serial_number_id'.$key.'" id="arr_serial_number_id'.$key.'" value="'.$val['id'].'">';
            echo '<input type="hidden" name="arr_serial_number_gudang_id'.$key.'" id="arr_serial_number_gudang_id'.$key.'" value="'.$val['gudang_id'].'">';
            echo '<input type="hidden" name="arr_serial_number_produk_id'.$key.'" id="arr_serial_number_produk_id'.$key.'" value="'.$val['produk_id'].'">';
            echo '<input type="hidden" name="arr_serial_number_jenis_barang_id'.$key.'" id="arr_serial_number_jenis_barang_id'.$key.'" value="'.$val['jenis_barang_id'].'">';
            echo '<input type="hidden" name="arr_serial_number_serial_number'.$key.'" id="arr_serial_number_serial_number'.$key.'" value="'.$val['serial_number'].'">';
        }
    }

    public function surat_jalan_masuk_proses(Request $request)
    {
        //hapus nilai di po_detail, ganti dengan nilai arr_serial number
        //update pula nilai stok di produk, produk_gudang dan produk_serial_number
        $no_surat_jalan = Input::get('no_surat_jalan');
        $po_header_id   = Input::get('po_header_id');
        $jumlah_pesanan = Input::get('jumlah_pesanan');

        $po_header = POHeader::where('id', $po_header_id)->first();

        date_default_timezone_set("Asia/Jakarta");
        $tanggal        = new DateTime;

        $sj_masuk_header = new SJMasukHeader;
        $sj_masuk_header->po_header_id          = $po_header_id;
        $sj_masuk_header->supplier_id           = $po_header->supplier_id;
        $sj_masuk_header->no_surat_jalan        = $no_surat_jalan;
        $sj_masuk_header->tanggal               = $tanggal;
        $sj_masuk_header->flag_nota_beli        = 0;
        $sj_masuk_header->ppn                   = $po_header->ppn;
        $ppn = $po_header->ppn;

        //ambil diskon di tran_po_diskon lalu ubah status jadi 1
        $po_diskon = PODiskon::where('po_header_id', $po_header_id)->first();

        if(!empty($po_diskon)){
            $total_diskon           = $po_diskon->nominal;
            $po_diskon->is_dipakai  = 1;
            $po_diskon->save();
        }
        else{
             $total_diskon = 0;
        }

        $sj_masuk_header->total_diskon = $total_diskon;
        $sj_masuk_header->save();

        $sj_masuk_header_id = $sj_masuk_header->id;
        $total_harga        = 0;
        

        //cek nomor surat jalan, jika ada yang sama maka transaksi batal, ulangi lagi jadi cek di ajax saja
        $jumlah_input_serial_number = 0;
        for ($i=0; $i < $jumlah_pesanan; $i++) { 
            $arr_serial_number_gudang_id        = Input::get('arr_serial_number_gudang_id'.$i);
            $arr_serial_number_produk_id        = Input::get('arr_serial_number_produk_id'.$i);
            $arr_serial_number_jenis_barang_id  = Input::get('arr_serial_number_jenis_barang_id'.$i);
            $arr_serial_number_serial_number    = Input::get('arr_serial_number_serial_number'.$i);

            //jika isi serial number kosong lewati proses
            if(empty($arr_serial_number_serial_number)){
                continue;
            }

            //tambah jumlah_terkirim di po_detail
            $po_detail = PODetail::where('po_header_id', $po_header_id)->where('produk_id', $arr_serial_number_produk_id)
            ->where('jenis_barang_id', $arr_serial_number_jenis_barang_id)->first();
            $po_detail->jumlah_terkirim = $po_detail->jumlah_terkirim + 1;
            $po_detail->save();

            $total_harga = $total_harga + $po_detail->harga;

            //cek surat jalan sudah ada atau belum, jika sudah maka cukup tambah quantity
            $sj_masuk_detail = SJMasukDetail::where('sj_masuk_header_id', $sj_masuk_header_id)
            ->where('produk_id', $arr_serial_number_produk_id)
            ->where('jenis_barang_id', $arr_serial_number_jenis_barang_id)->where('gudang_id', $arr_serial_number_gudang_id)
            ->where('serial_number', $arr_serial_number_serial_number)->first();

            if(!empty($sj_masuk_detail)){
                $sj_masuk_detail->jumlah = $sj_masuk_detail->jumlah + 1;
                $sj_masuk_detail->save();
            }
            else{
                $sj_masuk_detail = new SJMasukDetail;
                $sj_masuk_detail->sj_masuk_header_id    = $sj_masuk_header_id;
                $sj_masuk_detail->produk_id             = $arr_serial_number_produk_id;
                $sj_masuk_detail->jenis_barang_id       = $arr_serial_number_jenis_barang_id;
                $sj_masuk_detail->gudang_id             = $arr_serial_number_gudang_id;
                $sj_masuk_detail->serial_number         = $arr_serial_number_serial_number;
                $sj_masuk_detail->jumlah                = 1;

                //get harga dari po_detail
                $sj_masuk_detail->harga                = $po_detail->harga; //NANTI PERLU DIPERBAIKI, HARGA MANA YG PERLU DIAMBIL

                $sj_masuk_detail->save();
            }

            //update stok produk/hadiah
            if($arr_serial_number_jenis_barang_id == 1){
                //untuk produk update stok di tmst_produk, tran_produk_gudang, tran_produk_serial_number
                $produk = Produk::where('id', $arr_serial_number_produk_id)->first();
                $produk->stok = $produk->stok + 1;
                $produk->save();

                $produk_gudang = ProdukGudang::where('produk_id', $arr_serial_number_produk_id)->where('gudang_id', $arr_serial_number_gudang_id)->first();

                if(empty($produk_gudang)){ //jika kosong maka insert
                    $produk_gudang = new ProdukGudang;
                    $produk_gudang->produk_id   = $arr_serial_number_produk_id;
                    $produk_gudang->gudang_id   = $arr_serial_number_gudang_id;
                    $produk_gudang->stok        = 1;
                    $produk_gudang->save();
                }
                else{
                    $produk_gudang->stok        = $produk_gudang->stok + 1;
                    $produk_gudang->save();
                }

                $produk_serial_number = ProdukSerialNumber::where('produk_id', $arr_serial_number_produk_id)->where('gudang_id', $arr_serial_number_gudang_id)->where('serial_number', $arr_serial_number_serial_number)->first();

                if(empty($produk_serial_number)){ //jika kosong maka insert
                    $produk_serial_number = new ProdukSerialNumber;
                    $produk_serial_number->produk_id        = $arr_serial_number_produk_id;
                    $produk_serial_number->gudang_id        = $arr_serial_number_gudang_id;
                    $produk_serial_number->serial_number    = $arr_serial_number_serial_number;
                    $produk_serial_number->stok             = 1;
                    $produk_serial_number->save();
                }
                else{
                    $produk_serial_number->stok             = $produk_serial_number->stok + 1;
                    $produk_serial_number->save();
                }
            }
            elseif($arr_serial_number_jenis_barang_id == 2){
                //untuk hadiah update stok di tmst_hadiah, tran_hadiah_gudang
                $hadiah = Hadiah::where('id', $arr_serial_number_produk_id)->first();
                $hadiah->stok = $hadiah->stok+1;
                $hadiah->save();

                $hadiah_gudang = HadiahGudang::where('hadiah_id', $arr_serial_number_produk_id)->where('gudang_id', $arr_serial_number_gudang_id)->first();

                if(empty($hadiah_gudang)){ //jika kosong maka insert
                    $hadiah_gudang = new HadiahGudang;
                    $hadiah_gudang->hadiah_id   = $arr_serial_number_produk_id;
                    $hadiah_gudang->gudang_id   = $arr_serial_number_gudang_id;
                    $hadiah_gudang->stok        = 1;
                    $hadiah_gudang->save();
                }
                else{
                    $hadiah_gudang->stok        = $hadiah_gudang->stok + 1;
                    $hadiah_gudang->save();
                }
            }

            $jumlah_input_serial_number++;
        }

        //CEK APAKAH SURAT JALAN SUDAH SEMUA ATAU BELUM
        $flag_sj_masuk = 1;

        $po_detail = PODetail::where('po_header_id', $po_header_id)->get();

        foreach ($po_detail as $key => $value) {
            if($value->jumlah > $value->jumlah_terkirim){
                $flag_sj_masuk = 0;
                break;
            }
        }

        $po_header = POHeader::where('id', $po_header_id)->first();
        $po_header->flag_sj_masuk = $flag_sj_masuk;
        $po_header->save();

        $harga_ppn = ($total_harga*$ppn/100);

        //update SJM
        $sj_masuk_header = SJMasukHeader::where('id', $sj_masuk_header_id)->first();
        $sj_masuk_header->total_harga      = $total_harga;
        $sj_masuk_header->total_diskon     = $total_diskon;
        $sj_masuk_header->total_tagihan    = $total_harga + $harga_ppn - $total_diskon;
        $sj_masuk_header->save();

        return redirect('/nota_surat_jalan_masuk/'.$sj_masuk_header_id);
    }

    public function nota_surat_jalan_masuk($sj_masuk_header_id)
    {
        $sj_masuk_detail = SJMasukDetail::with('produk', 'hadiah')->where('sj_masuk_header_id', $sj_masuk_header_id)->groupBy('produk_id', 'jenis_barang_id')->selectRaw('tran_sj_masuk_detail.*, sum(jumlah) as jumlah_total')->get();
        $sj_masuk_header = SJMasukHeader::with('supplier')->where('id', $sj_masuk_header_id)->first();

        $po_header = POHeader::where('id', $sj_masuk_header->po_header_id)->first();
        $po_alamat_pengiriman = POAlamatPengiriman::where('po_header_id', $sj_masuk_header->po_header_id)->first();

        foreach ($sj_masuk_detail as $key => $value) {
            $produk_sn[$key] = SJMasukDetail::where('sj_masuk_header_id', $sj_masuk_header_id)->where('produk_id', $value->produk_id)->where('jenis_barang_id', $value->jenis_barang_id)->get();
        }

        $judul  = "Surat Jalan Masuk";
        $general['title']       = $judul;
        $general['menu1']       = "Transaksi";
        $general['menu2']       = "Surat Jalan Masuk";
        $general['menu3']       = $general['title'];
        $general['kode_menu']   = 57;  

        $perusahaan = Perusahaan::with('kota')->first();

        $kota_perusahaan = strtoupper($perusahaan->kota->nama);

        if(strpos($kota_perusahaan, 'KABUPATEN') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KABUPATEN', '', $kota_perusahaan)));
        }
        elseif(strpos($kota_perusahaan, 'KOTA') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KOTA', '', $kota_perusahaan)));
        }

        $supplier['nama']   = $sj_masuk_header->supplier->nama;
        $supplier['alamat'] = $sj_masuk_header->supplier->alamat;

        $kota = strtoupper($sj_masuk_header->supplier->kota->nama);

        if(strpos($kota, 'KABUPATEN') !== false){
            $kota = str_replace('KABUPATEN', '', $kota);
        }
        elseif(strpos($kota, 'KOTA') !== false){
            $kota = str_replace('KOTA', '', $kota);
        }

        $supplier['alamat'] = $supplier['alamat'].', '.$kota;
        $supplier['telp']   = $sj_masuk_header->supplier->telp1;

        date_default_timezone_set("Asia/Jakarta"); 
        $print_time = new DateTime;

        
        
        return view('pages.transaksi.surat_jalan_masuk.nota_surat_jalan_masuk', compact('general', 'sj_masuk_detail', 'sj_masuk_header', 'judul', 'perusahaan', 'po_header', 'produk_sn', 'print_time', 'po_alamat_pengiriman', 'supplier', 'kota_perusahaan')); 
    }

    public function nota_surat_jalan_masuk_print($sj_masuk_header_id)
    {
        $sj_masuk_detail = SJMasukDetail::with('produk', 'hadiah')->where('sj_masuk_header_id', $sj_masuk_header_id)->groupBy('produk_id', 'jenis_barang_id')->selectRaw('tran_sj_masuk_detail.*, sum(jumlah) as jumlah_total')->get();
        foreach ($sj_masuk_detail as $key => $value) {
            $produk_sn[$key] = SJMasukDetail::where('sj_masuk_header_id', $sj_masuk_header_id)->where('produk_id', $value->produk_id)->where('jenis_barang_id', $value->jenis_barang_id)->get();
        }

        $judul  = "Surat Jalan Masuk";

        $perusahaan = Perusahaan::with('kota')->first();

        $kota_perusahaan = strtoupper($perusahaan->kota->nama);

        if(strpos($kota_perusahaan, 'KABUPATEN') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KABUPATEN', '', $kota_perusahaan)));
        }
        elseif(strpos($kota_perusahaan, 'KOTA') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KOTA', '', $kota_perusahaan)));
        }

        date_default_timezone_set("Asia/Jakarta"); 
        $print_time = new DateTime;

        $sj_masuk_header = SJMasukHeader::with('supplier')->where('id', $sj_masuk_header_id)->first();

        $po_header = POHeader::where('id', $sj_masuk_header->po_header_id)->first();
        $po_alamat_pengiriman = POAlamatPengiriman::where('po_header_id', $sj_masuk_header->po_header_id)->first();

        $supplier['nama']   = $sj_masuk_header->supplier->nama;
        $supplier['alamat'] = $sj_masuk_header->supplier->alamat;

        $kota = strtoupper($sj_masuk_header->supplier->kota->nama);

        if(strpos($kota, 'KABUPATEN') !== false){
            $kota = str_replace('KABUPATEN', '', $kota);
        }
        elseif(strpos($kota, 'KOTA') !== false){
            $kota = str_replace('KOTA', '', $kota);
        }

        $supplier['alamat'] = $supplier['alamat'].', '.$kota;
        $supplier['telp']   = $sj_masuk_header->supplier->telp1;

        return view('pages.transaksi.surat_jalan_masuk.nota_surat_jalan_masuk_print', compact('general', 'sj_masuk_detail', 'sj_masuk_header', 'judul', 'perusahaan', 'po_header', 'produk_sn', 'print_time', 'po_alamat_pengiriman', 'supplier', 'kota_perusahaan')); 
    }

    public function nota_surat_jalan_masuk_pdf($sj_masuk_header_id)
    {
        $sj_masuk_detail = SJMasukDetail::with('produk', 'hadiah')->where('sj_masuk_header_id', $sj_masuk_header_id)->groupBy('produk_id', 'jenis_barang_id')->selectRaw('tran_sj_masuk_detail.*, sum(jumlah) as jumlah_total')->get();
        foreach ($sj_masuk_detail as $key => $value) {
            $produk_sn[$key] = SJMasukDetail::where('sj_masuk_header_id', $sj_masuk_header_id)->where('produk_id', $value->produk_id)->where('jenis_barang_id', $value->jenis_barang_id)->get();
        }

        $judul  = "Surat Jalan Masuk";

        $perusahaan = Perusahaan::with('kota')->first();

        $kota_perusahaan = strtoupper($perusahaan->kota->nama);

        if(strpos($kota_perusahaan, 'KABUPATEN') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KABUPATEN', '', $kota_perusahaan)));
        }
        elseif(strpos($kota_perusahaan, 'KOTA') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KOTA', '', $kota_perusahaan)));
        }

        date_default_timezone_set("Asia/Jakarta"); 
        $print_time = new DateTime;

        $sj_masuk_header = SJMasukHeader::where('id', $sj_masuk_header_id)->first();

        $po_header = POHeader::where('id', $sj_masuk_header->po_header_id)->first();
        $po_alamat_pengiriman = POAlamatPengiriman::where('po_header_id', $sj_masuk_header->po_header_id)->first();

        $supplier['nama']   = $sj_masuk_header->supplier->nama;
        $supplier['alamat'] = $sj_masuk_header->supplier->alamat;

        $kota = strtoupper($sj_masuk_header->supplier->kota->nama);

        if(strpos($kota, 'KABUPATEN') !== false){
            $kota = str_replace('KABUPATEN', '', $kota);
        }
        elseif(strpos($kota, 'KOTA') !== false){
            $kota = str_replace('KOTA', '', $kota);
        }

        $supplier['alamat'] = $supplier['alamat'].', '.$kota;
        $supplier['telp']   = $sj_masuk_header->supplier->telp1;

        $pdf = PDF::loadView('pages.transaksi.surat_jalan_masuk.nota_surat_jalan_masuk_pdf', compact('general', 'sj_masuk_detail', 'sj_masuk_header', 'judul', 'perusahaan', 'po_header', 'produk_sn', 'print_time', 'po_alamat_pengiriman', 'supplier', 'kota_perusahaan'))->setPaper('a4', 'landscape');
        return $pdf->stream();
    }

    public function hapus_surat_jalan_masuk(Request $request)
    {
        $id = Input::get('id');

        $sj_masuk_header = SJMasukHeader::where('id', $id)->first();

        $po_header = POHeader::where('id', $sj_masuk_header->po_header_id)->first();
        $po_header->flag_sj_masuk = 0;
        $po_header->save();

        //kembalikan stok
        $sj_masuk_detail = SJMasukDetail::where('sj_masuk_header_id', $id)->get();
        foreach ($sj_masuk_detail as $key => $value) {
            if($value->jenis_barang_id == 1){
                //update di produk
                $produk = Produk::where('id', $value->produk_id)->first();
                $produk->stok = $produk->stok - $value->jumlah;
                $produk->save();

                //update di produk_gudang
                $produk_gudang = ProdukGudang::where('produk_id', $value->produk_id)->where('gudang_id', $value->gudang_id)->first();
                $produk_gudang->stok = $produk_gudang->stok - $value->jumlah;
                $produk_gudang->save();

                //update di produk_serial_number
                $produk_serial_number = ProdukSerialNumber::where('produk_id', $value->produk_id)->where('gudang_id', $value->gudang_id)->where('serial_number', $value->serial_number)->first();
                $produk_serial_number->stok = $produk_serial_number->stok - $value->jumlah;
                $produk_serial_number->save();
            }

            if($value->jenis_barang_id == 2){
                //update di hadiah
                $hadiah = Hadiah::where('id', $value->produk_id)->first();
                $hadiah->stok = $hadiah->stok - $value->jumlah;
                $hadiah->save();

                //update di hadiah gudang
                $hadiah_gudang = HadiahGudang::where('hadiah_id', $value->produk_id)->where('gudang_id', $value->gudang_id)->first();
                $hadiah_gudang->stok = $hadiah_gudang->stok - $value->jumlah;
                $hadiah_gudang->save();
            }

            //UPDATE JUMLAH TERKIRIM
            $po_detail = PODetail::where('po_header_id', $sj_masuk_header->po_header_id)->where('produk_id', $value->produk_id)->where('jenis_barang_id', $value->jenis_barang_id)->first();
            $po_detail->jumlah_terkirim = $po_detail->jumlah_terkirim - $value->jumlah;
            $po_detail->save();

            //hapus sj_masuk_detail
            $this_sj_masuk_detail = SJMasukDetail::where('id', $value->id)->first();
            $this_sj_masuk_detail->delete();
        }

        $sj_masuk_header->delete();

        $request->session()->flash('message', 'Surat jalan masuk berhasil dihapus');
        return redirect('/surat_jalan_masuk');
    }
}
