<?php

namespace App\Http\Controllers\Transaksi;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;
use Input;
use DateTime;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Models\Perusahaan;
use PDF;
use App\Models\TransaksiPenjualan\InvoicePenjualan;
use App\Models\TransaksiPenjualan\BayarInvoicePenjualan;
use App\Models\TransaksiPenjualan\SOHeader;
use App\Models\TransaksiPenjualan\SODetail;
use App\Models\TransaksiPenjualan\SOVoucher;
use App\Models\TransaksiPenjualan\SOAlamatPengiriman;
use App\Models\TransaksiPenjualan\SJKeluarHeader;
use App\Models\TransaksiPenjualan\SJKeluarDetail;
use App\Models\TransaksiPenjualan\NotaJual;

class NotaJualController extends Controller
{
    public function nota_jual()
    {
        $general['title']       = "Nota Jual";
        $general['menu1']       = "Transaksi";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 510;

        $nota_jual = NotaJual::with('sj_keluar_header', 'pelanggan')->get();

        return view('pages.transaksi.nota_jual', compact('general', 'nota_jual'));
    }

    public function tambah_nota_jual()
    {
        $general['title']       = "Tambah Nota Jual";
        $general['menu1']       = "Transaksi";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 510;

        $sj_keluar_header = SJKeluarHeader::with('pelanggan')->get();

        //ambil surat_jalan yang belum punya nota_jual
        $surat_jalan = [];
        $indeks = 0;

        foreach ($sj_keluar_header as $value) {
            $nota_jual = NotaJual::where('sj_keluar_header_id', $value->id)->first();
            
            if(empty($nota_jual)){
                $surat_jalan[$indeks]['sj_keluar_header_id']    = $value->id;
                $surat_jalan[$indeks]['no_surat_jalan']         = $value->no_surat_jalan;
                $surat_jalan[$indeks]['pelanggan']              = $value->pelanggan->nama;
                $surat_jalan[$indeks]['tanggal']                = $value->tanggal;

                $surat_jalan[$indeks]['total_harga']            = $value->total_harga;
                $surat_jalan[$indeks]['total_voucher']          = $value->total_voucher;
                $surat_jalan[$indeks]['ppn']                    = $value->ppn;
                $indeks++;
            }
        }

        date_default_timezone_set("Asia/Jakarta");
        $tanggal        = new DateTime;

        $nomor_invoice      = DB::table('tref_nomor_invoice')->where('id', 8)->first();
        $format_invoice     = $nomor_invoice->format;
        $ori_format_invoice = $format_invoice;

        $cek_dd = strpos($format_invoice, 'DD');
        if(!empty($cek_dd))
        {
            $format_invoice = str_replace('DD', $tanggal->format('d'), $format_invoice);
        }

        $cek_mm = strpos($format_invoice, 'MM');
        if(!empty($cek_mm))
        {
            $format_invoice = str_replace('MM', $tanggal->format('m'), $format_invoice);
        }

        $cek_yyyy = strpos($format_invoice, 'YYYY');
        if(!empty($cek_yyyy))
        {
            $format_invoice = str_replace('YYYY', $tanggal->format('Y'), $format_invoice);
        }

        $cek_yy = strpos($format_invoice, 'YY');
        if(!empty($cek_yy))
        {
            $format_invoice = str_replace('YY', $tanggal->format('y'), $format_invoice);
        }

        //cek maksimal 10
        $number = '##########';

        for ($i=9; $i > 0; $i--) { 
            $number     = substr($number, 0, $i);
            $cek_number = strpos($format_invoice, $number);
            $nol        = "";
            if(!empty($cek_number))
            {
                $nota_jual    = NotaJual::orderBy('id', 'desc')->first();
                if (empty($nota_jual)) {
                    $get_num = 1;
                } else {
                    $get_num = (int) $nota_jual->id + 1;
                }

                for ($j=0; $j < $i - strlen($get_num); $j++) { 
                    $nol = $nol."0";
                }

                $get_num = $nol.$get_num;
                $format_invoice = str_replace($number, $get_num, $format_invoice);
                break;
            }
        }

        $no_nota_jual     = $format_invoice;

        return view('pages.transaksi.tambah_nota_jual', compact('general', 'indeks', 'surat_jalan', 'no_nota_jual'));
    }

    public function nj_get_data()
    {
        $sj_keluar_header_id = Input::get('sj_keluar_header_id');

        $sj_keluar_header   = SJKeluarHeader::where('id', $sj_keluar_header_id)->first();
        $so_header          = SOHeader::where('id', $sj_keluar_header->so_header_id)->first();

        //total tagihan dilihat dari total harga surat jalan
        $total_tagihan = 0;

        $sj_keluar_detail = SJKeluarDetail::where('sj_keluar_header_id', $sj_keluar_header_id)->get();
        
        foreach ($sj_keluar_detail as $key => $value) {
            $total_tagihan = $total_tagihan + $value->harga;
        }

        $data['total_tagihan'] = $total_tagihan;

        echo json_encode($data);
    }

    public function do_tambah_nota_jual(Request $request)
    {
        $this->validate($request, [
            'sj_keluar_header_id' => 'required',
            'nota_jual' => 'required',
            'tanggal' => 'required',
            'jatuh_tempo' => 'required',
            'kirim_via' => 'required',
            'no_resi' => 'required',
            'foto_resi' => 'required',
        ]);

        $sj_keluar_header_id    = Input::get('sj_keluar_header_id');
        $no_nota                = Input::get('nota_jual');
        $tanggal                = Input::get('tanggal');
        $jatuh_tempo            = Input::get('jatuh_tempo');
        $kirim_via              = Input::get('kirim_via');
        $no_resi                = Input::get('no_resi');
        $foto_resi              = Input::get('foto_resi');

        $tanggal        = new DateTime($tanggal);
        $jatuh_tempo    = new DateTime($jatuh_tempo);

        $sj_keluar_header = SJKeluarHeader::where('id', $sj_keluar_header_id)->first();
        $sj_keluar_header->flag_nota_jual = 1;
        $sj_keluar_header->save();

        $file_name = '';
        if (Input::hasFile('foto_resi')) {
            $destinationPath    = base_path().'/public/img/resi_jual/';
            $extension          = Input::file('foto_resi')->getClientOriginalExtension();
            $file_gambar        = $no_nota;
            $file_name           = $file_gambar.'.'.$extension;

            Input::file('foto_resi')->move($destinationPath, $file_name);
        }

        $nota_jual = new NotaJual;
        $nota_jual->sj_keluar_header_id     = $sj_keluar_header_id;
        $nota_jual->pelanggan_id            = $sj_keluar_header->pelanggan_id;
        $nota_jual->no_nota                 = $no_nota;
        $nota_jual->tanggal                 = $tanggal;
        $nota_jual->jatuh_tempo             = $jatuh_tempo;
        $nota_jual->is_lunas                = 0;
        $nota_jual->ppn                     = $sj_keluar_header->ppn;
        $nota_jual->total_harga             = $sj_keluar_header->total_harga;
        $nota_jual->total_voucher           = $sj_keluar_header->total_voucher;
        $nota_jual->total_tagihan           = $sj_keluar_header->total_tagihan;
        $nota_jual->kirim_via               = $kirim_via;
        $nota_jual->no_resi                 = $no_resi;
        $nota_jual->foto_resi               = 'resi_jual/'.$file_name;
        $nota_jual->save();

        $nota_jual_id = $nota_jual->id;

        //update nilai HPP,
        // $sj_keluar_detail = SJKeluarDetail::where('sj_keluar_header_id', $sj_keluar_header_id)->groupBy('produk_id', 'jenis_barang_id')->selectRaw('tran_sj_keluar_detail.*, sum(jumlah) as jumlah_total, sum(harga) as harga_total')->get();

        // foreach ($sj_keluar_detail as $key => $value) {
        //     if($value->jenis_barang_id == 1){ //hanya untuk produk
        //         $produk = Produk::where('id', $value->produk_id)->first();
        //         $hpp = 0;
        //         if(!empty($produk->hpp)){
        //             $hpp = $produk->hpp;
        //         }

        //         $avg = (($hpp * ($produk->stok - $produk->jumlah_total)) + $value->harga_total)/($produk->stok);

        //         $produk->hpp = $avg;
        //         $produk->save();
        //     }
        // }

        return redirect('/invoice_nota_jual/'.$nota_jual_id);
    }

    public function invoice_nota_jual($nota_jual_id)
    {
        $nota_jual = NotaJual::where('id', $nota_jual_id)->first();

        $sj_keluar_header   = SJKeluarHeader::where('id', $nota_jual->sj_keluar_header_id)->first();
        $so_header          = SOHeader::where('id', $sj_keluar_header->so_header_id)->first();
        $so_alamat_pengiriman   = SOAlamatPengiriman::where('so_header_id', $so_header->id)->first();

        $judul  = "Nota Jual";
        $general['title']       = $judul;
        $general['menu1']       = "Transaksi";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 510;  

        $perusahaan = Perusahaan::with('kota')->first();
        $kota_perusahaan = strtoupper($perusahaan->kota->nama);

        if(strpos($kota_perusahaan, 'KABUPATEN') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KABUPATEN', '', $kota_perusahaan)));
        }
        elseif(strpos($kota_perusahaan, 'KOTA') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KOTA', '', $kota_perusahaan)));
        }

        date_default_timezone_set("Asia/Jakarta"); 
        $print_time = new DateTime;

        $sj_keluar_detail    = SJKeluarDetail::where('sj_keluar_header_id', $nota_jual->sj_keluar_header_id)
        ->groupBy('produk_id', 'jenis_barang_id')->selectRaw('tran_sj_keluar_detail.*, sum(jumlah) as jumlah_total')->get();

        $produk = [];

        foreach ($sj_keluar_detail as $key => $value) {
            if($value->jenis_barang_id == 1){
                $produk[$key]['nama'] = $value->produk->nama;
                $produk[$key]['satuan'] = $value->produk->satuan;
            }
            elseif($value->jenis_barang_id == 2){
                $produk[$key]['nama'] = $value->hadiah->nama;
                $produk[$key]['satuan'] = $value->hadiah->satuan;
            }
            elseif($value->jenis_barang_id == 3){
                $produk[$key]['nama'] = $value->paket->nama;
                $produk[$key]['satuan'] = 'PKT';
            }

            $produk[$key]['spesifikasi']    = $value->deskripsi;
            $produk[$key]['jumlah']         = $value->jumlah_total;

            $so_detail = SODetail::where('so_header_id', $so_header->id)->where('produk_id', $value->produk_id)
            ->where('jenis_barang_id', $value->jenis_barang_id)->where('jumlah_terkirim', '>', '0')->first();
            $produk[$key]['harga']          = $so_detail->harga;
        }

        $pelanggan['nama']   = $so_header->pelanggan->nama;
        $pelanggan['alamat'] = $so_header->pelanggan->alamat;

        $kota = strtoupper($so_header->pelanggan->kota->nama);

        if(strpos($kota, 'KABUPATEN') !== false){
            $kota = str_replace('KABUPATEN', '', $kota);
        }
        elseif(strpos($kota, 'KOTA') !== false){
            $kota = str_replace('KOTA', '', $kota);
        }

        $pelanggan['alamat'] = $pelanggan['alamat'].', '.$kota;
        $pelanggan['telp']   = $so_header->pelanggan->telp1;
        
        return view('pages.transaksi.nota_jual.invoice_nota_jual', compact('general', 'sj_keluar_header', 'so_header', 'sj_keluar_detail', 'judul', 'perusahaan', 'produk', 'print_time', 'nota_jual', 'so_alamat_pengiriman', 'pelanggan', 'kota_perusahaan')); 
    }

    public function invoice_nota_jual_print($nota_jual_id)
    {
        $nota_jual = NotaJual::where('id', $nota_jual_id)->first();

        $sj_keluar_header   = SJKeluarHeader::where('id', $nota_jual->sj_keluar_header_id)->first();
        $so_header          = SOHeader::where('id', $sj_keluar_header->so_header_id)->first();
        $so_alamat_pengiriman   = SOAlamatPengiriman::where('so_header_id', $so_header->id)->first();

        $judul  = "Nota Jual";
        $general['title']       = $judul;
        $general['menu1']       = "Transaksi";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 510;  

        $perusahaan = Perusahaan::with('kota')->first();
        $kota_perusahaan = strtoupper($perusahaan->kota->nama);

        if(strpos($kota_perusahaan, 'KABUPATEN') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KABUPATEN', '', $kota_perusahaan)));
        }
        elseif(strpos($kota_perusahaan, 'KOTA') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KOTA', '', $kota_perusahaan)));
        }

        date_default_timezone_set("Asia/Jakarta"); 
        $print_time = new DateTime;

        $pelanggan['nama']   = $so_header->pelanggan->nama;
        $pelanggan['alamat'] = $so_header->pelanggan->alamat;

        $kota = strtoupper($so_header->pelanggan->kota->nama);

        if(strpos($kota, 'KABUPATEN') !== false){
            $kota = str_replace('KABUPATEN', '', $kota);
        }
        elseif(strpos($kota, 'KOTA') !== false){
            $kota = str_replace('KOTA', '', $kota);
        }

        $pelanggan['alamat'] = $pelanggan['alamat'].', '.$kota;
        $pelanggan['telp']   = $so_header->pelanggan->telp1;

        $sj_keluar_detail    = SJKeluarDetail::where('sj_keluar_header_id', $nota_jual->sj_keluar_header_id)
        ->groupBy('produk_id', 'jenis_barang_id')->selectRaw('tran_sj_keluar_detail.*, sum(jumlah) as jumlah_total')->get();

        $produk = [];

        foreach ($sj_keluar_detail as $key => $value) {
            if($value->jenis_barang_id == 1){
                $produk[$key]['nama'] = $value->produk->nama;
                $produk[$key]['satuan'] = $value->produk->satuan;
            }
            elseif($value->jenis_barang_id == 2){
                $produk[$key]['nama'] = $value->hadiah->nama;
                $produk[$key]['satuan'] = $value->hadiah->satuan;
            }
            elseif($value->jenis_barang_id == 3){
                $produk[$key]['nama'] = $value->paket->nama;
                $produk[$key]['satuan'] = 'PKT';
            }

            $produk[$key]['spesifikasi']    = $value->deskripsi;
            $produk[$key]['jumlah']         = $value->jumlah_total;

            $so_detail = SODetail::where('so_header_id', $so_header->id)->where('produk_id', $value->produk_id)
            ->where('jenis_barang_id', $value->jenis_barang_id)->where('jumlah_terkirim', '>', '0')->first();
            $produk[$key]['harga']          = $so_detail->harga;
        }
        
        return view('pages.transaksi.nota_jual.invoice_nota_jual_print', compact('general', 'sj_keluar_header', 'so_header', 'sj_keluar_detail', 'judul', 'perusahaan', 'produk', 'print_time', 'nota_jual', 'so_alamat_pengiriman', 'pelanggan', 'kota_perusahaan')); 
    }

    public function invoice_nota_jual_pdf($nota_jual_id)
    {
        $nota_jual = NotaJual::where('id', $nota_jual_id)->first();

        $sj_keluar_header   = SJKeluarHeader::where('id', $nota_jual->sj_keluar_header_id)->first();
        $so_header          = SOHeader::where('id', $sj_keluar_header->so_header_id)->first();
        $so_alamat_pengiriman   = SOAlamatPengiriman::where('so_header_id', $so_header->id)->first();

        $judul  = "Nota Jual";
        $general['title']       = $judul;
        $general['menu1']       = "Transaksi";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 510;  

        $perusahaan = Perusahaan::with('kota')->first();
        $kota_perusahaan = strtoupper($perusahaan->kota->nama);

        if(strpos($kota_perusahaan, 'KABUPATEN') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KABUPATEN', '', $kota_perusahaan)));
        }
        elseif(strpos($kota_perusahaan, 'KOTA') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KOTA', '', $kota_perusahaan)));
        }

        date_default_timezone_set("Asia/Jakarta"); 
        $print_time = new DateTime;

        $sj_keluar_detail    = SJKeluarDetail::where('sj_keluar_header_id', $nota_jual->sj_keluar_header_id)
        ->groupBy('produk_id', 'jenis_barang_id')->selectRaw('tran_sj_keluar_detail.*, sum(jumlah) as jumlah_total')->get();

        $produk = [];

        foreach ($sj_keluar_detail as $key => $value) {
            if($value->jenis_barang_id == 1){
                $produk[$key]['nama'] = $value->produk->nama;
                $produk[$key]['satuan'] = $value->produk->satuan;
            }
            elseif($value->jenis_barang_id == 2){
                $produk[$key]['nama'] = $value->hadiah->nama;
                $produk[$key]['satuan'] = $value->hadiah->satuan;
            }
            elseif($value->jenis_barang_id == 3){
                $produk[$key]['nama'] = $value->paket->nama;
                $produk[$key]['satuan'] = 'PKT';
            }

            $produk[$key]['spesifikasi']    = $value->deskripsi;
            $produk[$key]['jumlah']         = $value->jumlah_total;

            $so_detail = SODetail::where('so_header_id', $so_header->id)->where('produk_id', $value->produk_id)
            ->where('jenis_barang_id', $value->jenis_barang_id)->where('jumlah_terkirim', '>', '0')->first();
            $produk[$key]['harga']          = $so_detail->harga;
        }

        $pelanggan['nama']   = $so_header->pelanggan->nama;
        $pelanggan['alamat'] = $so_header->pelanggan->alamat;

        $kota = strtoupper($so_header->pelanggan->kota->nama);

        if(strpos($kota, 'KABUPATEN') !== false){
            $kota = str_replace('KABUPATEN', '', $kota);
        }
        elseif(strpos($kota, 'KOTA') !== false){
            $kota = str_replace('KOTA', '', $kota);
        }

        $pelanggan['alamat'] = $pelanggan['alamat'].', '.$kota;
        $pelanggan['telp']   = $so_header->pelanggan->telp1;

        $pdf = PDF::loadView('pages.transaksi.nota_jual.invoice_nota_jual_pdf', compact('general', 'sj_keluar_header', 'so_header', 'sj_keluar_detail', 'judul', 'perusahaan', 'produk', 'print_time', 'nota_jual', 'so_alamat_pengiriman', 'pelanggan', 'kota_perusahaan'))->setPaper('a4', 'landscape');
        return $pdf->stream();
    }

    public function hapus_nota_jual(Request $request)
    {
        $id = Input::get('id');

        $nota_jual = NotaJual::where('id', $id)->first();
        $sj_keluar_header = SJKeluarHeader::where('id', $nota_jual->sj_keluar_header_id)->first();
        $sj_keluar_header->flag_nota_jual    = 0;
        $sj_keluar_header->save();

        $nota_jual->delete();

        $request->session()->flash('message', 'Nota jual berhasil dihapus');
        return redirect('/nota_jual');
    }
}
