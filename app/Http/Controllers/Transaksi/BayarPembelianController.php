<?php

namespace App\Http\Controllers\Transaksi;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\TransaksiPembelian\BayarPembelianHeader;
use App\Models\TransaksiPembelian\BayarPembelianDetail;
use App\Models\TransaksiPembelian\POHeader;
use App\Models\TransaksiPembelian\PODetail;
use App\Models\TransaksiPembelian\PODiskon;
use App\Models\TransaksiPembelian\POAlamatPengiriman;
use App\Models\TransaksiPembelian\SJMasukHeader;
use App\Models\TransaksiPembelian\SJMasukDetail;
use App\Models\TransaksiPembelian\NotaBeli;
use App\Models\TransaksiPembelian\RBHeader;
use App\Models\TransaksiPenjualan\InvoicePenjualan;
use App\Models\TransaksiPenjualan\BayarPenjualan;
use App\Models\TransaksiPenjualan\SOHeader;
use App\Models\TransaksiPenjualan\SODetail;
use App\Models\TransaksiPenjualan\SOVoucher;
use App\Models\TransaksiPenjualan\SOAlamatPengiriman;
use App\Models\TransaksiPenjualan\SJKeluarHeader;
use App\Models\TransaksiPenjualan\SJKeluarDetail;
use App\Models\TransaksiPenjualan\NotaJual;
use App\Models\TransaksiPenjualan\RJHeader;
use App\Models\Referensi\MetodePembayaran;
use Response;
use DateTime;
use Input;
use App\Models\Supplier\Supplier;
use App\Models\Supplier\RekeningSupplier;
use Validator;
use App\Models\Referensi\Bank;
use Session;


class BayarPembelianController extends Controller
{
    public function pembayaran_supplier()
    {
        $general['title']       = "Pembayaran Supplier";
        $general['menu1']       = "Transaksi";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 511;

        $pembayaran_supplier = BayarPembelianHeader::with('nota_beli', 'rb_header', 'supplier')->get();

        return view('pages.transaksi.pembayaran.pembayaran_supplier', compact('general', 'pembayaran_supplier'));
    }

    public function tambah_pembayaran_supplier()
    {
        $general['title']       = "Tambah Pembayaran Supplier";
        $general['menu1']       = "Transaksi";
        $general['menu3']       = "Pembayaran Supplier";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 511;

        $all_nota_beli  = NotaBeli::with('supplier')->where('is_lunas', 0)->get();

        $nota_beli = [];
        $key = 0;
        foreach ($all_nota_beli as $value) {
            $nota_beli[$key]['id']              = $value->id;
            $nota_beli[$key]['jenis_id']        = 1;
            $nota_beli[$key]['jenis']           = 'Nota Beli';
            $nota_beli[$key]['no_nota']         = $value->no_nota;
            $nota_beli[$key]['supplier']        = $value->supplier->nama;
            $nota_beli[$key]['supplier_id']     = $value->supplier_id;

            $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
            
            $tanggal = new DateTime($value->tanggal);
            $nota_beli[$key]['tanggal']         = $tanggal->format('d')." ".$bulan[(int)$tanggal->format('m')]." ".$tanggal->format('Y');

            $nota_beli[$key]['total_harga']     = $value->total_harga;
            $nota_beli[$key]['ppn']             = $value->ppn;
            $nota_beli[$key]['total_diskon']    = $value->total_diskon;
            $nota_beli[$key]['total_tagihan']   = $value->total_tagihan;

            $key++;
        }

        $all_retur_beli  = RBHeader::with('supplier')->where('is_lunas', 0)->get();

        foreach ($all_retur_beli as $value) {
            $nota_beli[$key]['id']              = $value->id;
            $nota_beli[$key]['jenis_id']        = 2;
            $nota_beli[$key]['jenis']           = 'Retur Beli';
            $nota_beli[$key]['no_nota']         = $value->no_nota;
            $nota_beli[$key]['supplier']        = $value->supplier->nama;
            $nota_beli[$key]['supplier_id']     = $value->supplier_id;

            $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
            
            $tanggal = new DateTime($value->tanggal);
            $nota_beli[$key]['tanggal']         = $tanggal->format('d')." ".$bulan[(int)$tanggal->format('m')]." ".$tanggal->format('Y');

            $nota_beli[$key]['total_harga']   = $value->total_retur;

            if(empty($value->potongan_retur)){
                $nota_beli[$key]['total_diskon'] = 0;
            }
            else{
                $nota_beli[$key]['total_diskon']    = $value->potongan_retur;
            }

            $nota_beli[$key]['ppn'] = 0;
            $nota_beli[$key]['total_tagihan']   = $value->total_tagihan;

            $key++;
        }

        $tanggal = new DateTime();
        $tanggal = $tanggal->format('d-m-Y');
        $tanggal_inv = $tanggal;
  
        $metode_pembayaran = MetodePembayaran::where('jenis', 'non_tunai')->get();

        return view('pages.transaksi.pembayaran.tambah_pembayaran_supplier', compact('general', 'nota_beli', 'tanggal', 'tanggal_inv', 'metode_pembayaran'));
    }

    public function get_rekening_supplier()
    {
        $supplier_id = Input::get('supplier_id');

        $rekening_supplier = RekeningSupplier::where('supplier_id', $supplier_id)->get();

        return view('pages.transaksi.pembayaran.ajax_get_rekening_supplier', compact('rekening_supplier'));
    }

    public function do_pembayaran_supplier(Request $request)
    {
        $this->validate($request, [
            'nota_id' => 'required',
            'jenis_id' => 'required',
            'tanggal' => 'required',
        ]);

        //nanti harus dicek bahwa nominal harus sama dengan tagihan-diskon

        $nota_supplier = Input::get('no_inv_sup');
        $tanggal_nota_supplier = Input::get('tanggal_inv');

        $nota_id    = Input::get('nota_id');
        $jenis_id   = Input::get('jenis_id');
        $tanggal    = Input::get('tanggal');
        $tunai      = Input::get('tunai');
        $keterangan                     = Input::get('keterangan');
        $jumlah_pembayaran_non_tunai    = Input::get('jumlah_pembayaran_non_tunai');

        $tanggal    = new DateTime($tanggal);
        $tunai      = str_replace('.', '', $tunai);

        if(empty($tunai)){
            $tunai = 0;
        }

        $bayar_pembelian_header            = new BayarPembelianHeader;
        $bayar_pembelian_header->nota_id   = $nota_id;
        $total_tagihan = 0;
        if($jenis_id == 1){
            $jenis      = 'nota_beli';
            $nota_beli  = NotaBeli::where('id', $nota_id)->first();
            $nota_beli->is_lunas = 1;
            $nota_beli->save();
            $supplier_id    = $nota_beli->supplier_id;
            $total_tagihan  = $nota_beli->total_tagihan;
        }
        else{
            $jenis      = 'retur_beli';
            $rb_header  = RBHeader::where('id', $nota_id)->first();
            $rb_header->is_lunas = 1;
            $rb_header->save();
            $supplier_id    = $rb_header->supplier_id;
            $total_tagihan  = $rb_header->total_tagihan;
        }

        $bayar_pembelian_header->nota_supplier          = $nota_supplier;
        $bayar_pembelian_header->tanggal_nota_supplier  = $tanggal_nota_supplier;
        $bayar_pembelian_header->jenis              = $jenis;
        $bayar_pembelian_header->supplier_id        = $supplier_id;
        $bayar_pembelian_header->tanggal            = $tanggal;
        $bayar_pembelian_header->total_tagihan      = $total_tagihan;
        $bayar_pembelian_header->keterangan         = $keterangan;
        $bayar_pembelian_header->save(); //nantinya harus ada total_pembayaran

        $bayar_pembelian_header_id  = $bayar_pembelian_header->id;
        $total_pembayaran           = 0;

        //tunai
        $bayar_pembelian_detail = new BayarPembelianDetail;
        $bayar_pembelian_detail->bayar_pembelian_header_id  = $bayar_pembelian_header_id;
        $bayar_pembelian_detail->metode_pembayaran_id       = 1; //tunai set 1
        $bayar_pembelian_detail->nomor_pembayaran           = "";
        $bayar_pembelian_detail->nominal                    = $tunai;
        $bayar_pembelian_detail->bank                       = "";
        $bayar_pembelian_detail->save();

        $total_pembayaran = $total_pembayaran + $tunai;

        //non tunai
        for ($i=1; $i <= (int)$jumlah_pembayaran_non_tunai; $i++) { 
            $nominal                = Input::get('non_tunai'.$i);
            $metode_pembayaran_id   = Input::get('metode_pembayaran'.$i);
            $no_rekening            = Input::get('no_rekening'.$i);
            $bank                   = Input::get('bank'.$i);

            if(!empty($nominal) && !empty($metode_pembayaran_id) && !empty($no_rekening) && !empty($bank)){
                $nominal = str_replace('.', '', $nominal);

                $bayar_pembelian_detail = new BayarPembelianDetail;
                $bayar_pembelian_detail->bayar_pembelian_header_id  = $bayar_pembelian_header_id;
                $bayar_pembelian_detail->metode_pembayaran_id       = $metode_pembayaran_id;
                $bayar_pembelian_detail->nomor_pembayaran           = $no_rekening;
                $bayar_pembelian_detail->nominal                    = $nominal;
                $bayar_pembelian_detail->bank                       = $bank;
                $bayar_pembelian_detail->save();

                $total_pembayaran = $total_pembayaran + $nominal;
            }
        }

        $bayar_pembelian_header = BayarPembelianHeader::where('id', $bayar_pembelian_header_id)->first();
        $bayar_pembelian_header->total_pembayaran = $total_pembayaran;
        $bayar_pembelian_header->save();

        $request->session()->flash('message', 'Pembayaran Supplier berhasil dilakukan');
        return redirect('/pembayaran_supplier');
    }

    public function hapus_bayar_pembelian(Request $request)
    {
        $id = Input::get('id');

        $bayar_pembelian_detail = BayarPembelianDetail::where('bayar_pembelian_header_id', $id)->delete();

        $bayar_pembelian_header = BayarPembelianHeader::where('id', $id)->first();

        //cek jenis

        if($bayar_pembelian_header->jenis == 'nota_beli'){
            $nota_beli = NotaBeli::where('id', $bayar_pembelian_header->nota_id)->first();
            $nota_beli->is_lunas = 0;
            $nota_beli->save();
        }
        elseif($bayar_pembelian_header->jenis == 'retur_beli'){
            $rb_header = RBHeader::where('id', $bayar_pembelian_header->nota_id)->first();
            $rb_header->is_lunas = 0;
            $rb_header->save();
        }

        $bayar_pembelian_header->delete();

        $request->session()->flash('message', 'Pembayaran Supplier berhasil dihapus');
        return redirect('/pembayaran_supplier');
    }
}
