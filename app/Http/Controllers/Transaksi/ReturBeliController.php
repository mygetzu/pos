<?php

namespace App\Http\Controllers\Transaksi;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Gloudemans\Shoppingcart\Facades\Cart;
use Input;
use Auth;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Session;
use Promocodes;
use Response;
use DateTime;
use PDF;
use Hashids\Hashids;
use Validator;
use Illuminate\Http\RedirectResponse;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use App\User;
use App\Models\Produk\Produk;
use App\Models\Supplier\Supplier;
use App\Models\PaketProduk\Paket;
use App\Models\Produk\Hadiah;
use App\Models\Perusahaan;
use App\Models\Gudang\ProdukGudang;
use App\Models\Produk\ProdukSerialNumber;
use App\Models\TransaksiPembelian\RBHeader;
use App\Models\TransaksiPembelian\RBDetail;
use App\Models\TransaksiPembelian\POHeader;
use App\Models\TransaksiPembelian\PODetail;
use App\Models\TransaksiPembelian\SJMasukHeader;
use App\Models\TransaksiPembelian\SJMasukDetail;
use App\Models\TransaksiPembelian\NotaBeli;

class ReturBeliController extends Controller
{
    public function retur_beli()
    {
        $general['title']       = "Retur Beli";
        $general['menu1']       = "Transaksi";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 54; 
        
        $rb_header = RBHeader::with('supplier')->orderBy('tanggal', 'desc')->get();

        return view('pages.transaksi.retur_beli', compact('general', 'rb_header'));
    }

    public function tambah_retur_beli()
    {
        $general['title']       = "Tambah Retur Beli";
        $general['menu1']       = "Transaksi";
        $general['menu2']       = "Retur Beli";
        $general['menu3']       = $general['title'];
        $general['kode_menu']   = 54; 

        Cart::destroy();
        date_default_timezone_set("Asia/Jakarta");
        $tanggal        = new DateTime;

        $supplier          = Supplier::with('kota')->get();

        $nomor_invoice  = DB::table('tref_nomor_invoice')->where('id', 4)->first();
        $format_invoice = $nomor_invoice->format;
        $ori_format_invoice = $format_invoice;

        $cek_dd = strpos($format_invoice, 'DD');
        if(!empty($cek_dd))
        {
            $format_invoice = str_replace('DD', $tanggal->format('d'), $format_invoice);
        }

        $cek_mm = strpos($format_invoice, 'MM');
        if(!empty($cek_mm))
        {
            $format_invoice = str_replace('MM', $tanggal->format('m'), $format_invoice);
        }

        $cek_yyyy = strpos($format_invoice, 'YYYY');
        if(!empty($cek_yyyy))
        {
            $format_invoice = str_replace('YYYY', $tanggal->format('Y'), $format_invoice);
        }

        //cek maksimal 10
        $number = '##########';

        for ($i=9; $i > 0; $i--) { 
            $number     = substr($number, 0, $i);
            $cek_number = strpos($format_invoice, $number);
            $nol        = "";
            if(!empty($cek_number))
            {
                $rb_header      = RBHeader::orderBy('id', 'desc')->first();
                if(empty($rb_header))
                    $get_num = 1;
                else
                    $get_num        = (int)$rb_header->id + 1;

                for ($j=0; $j < $i - strlen($get_num); $j++) { 
                    $nol = $nol."0";
                }
                
                $get_num = $nol.$get_num;
                $format_invoice = str_replace($number, $get_num, $format_invoice);
                break;
            }
        }

        //$no_invoice     = $format_invoice.$this->generate_no_nota();
        $no_retur_beli     = $format_invoice;

        return view('pages.transaksi.tambah_retur_beli', compact('general', 'supplier', 'format_invoice', 'no_retur_beli', 'ori_format_invoice'));
    }

    public function ubah_format_no_invoice_rb(Request $request)
    {
        $format = strtoupper(Input::get('format'));

        DB::table('tref_nomor_invoice')->where('id', 4)->update(['format' => $format]);

        $request->session()->flash('message', 'Nomor Invoice berhasil diperbarui');
        return redirect('/tambah_retur_beli');
    }

    public function get_nota_supplier()
    {
        $supplier_id = Input::get('supplier_id');

        //$po_header = POHeader::where('supplier_id', $supplier_id)->get();
        $nota_beli_obj = NotaBeli::where('supplier_id', $supplier_id)->get();

        $nota_beli = [];
        $indeks = 0;

        foreach ($nota_beli_obj as $key => $value) {
            $nota_beli[$indeks]['id']       = $value->id;
            $nota_beli[$indeks]['no_nota']  = $value->no_nota;
            $nota_beli[$indeks]['tanggal']  = $value->tanggal;
            $indeks++;
        }

        Cart::destroy();

        return view('pages.transaksi.ajax_po_header', compact('nota_beli'));
    }

    public function get_po_detail()
    {
        $nota_beli_id = Input::get('nota_beli_id');

        //$po_header = POHeader::where('id', $po_header_id)->first();

        $nota_beli = NotaBeli::where('id', $nota_beli_id)->first();

        $sj_masuk_header = SJMasukHeader::where('id', $nota_beli->sj_masuk_header_id)->first();

        $po_detail = [];
        $indeks = 0;

        $sj_masuk_detail = SJMasukDetail::where('sj_masuk_header_id', $sj_masuk_header->id)->get();

        foreach ($sj_masuk_detail as $value2) {
            if($value2->jenis_barang_id == 1){
                $produk = Produk::where('id', $value2->produk_id)->first();
                $po_detail[$indeks]['nama']    = $produk->nama;
                $po_detail[$indeks]['satuan']  = $produk->satuan;
            }
            elseif($value2->jenis_barang_id == 2) {
                $hadiah = Hadiah::where('id', $value2->produk_id)->first();
                $po_detail[$indeks]['nama']    = $hadiah->nama;
                $po_detail[$indeks]['satuan']  = $hadiah->satuan;
            }
            elseif($value2->jenis_barang_id == 3) {
                $paket = Paket::where('id', $value2->produk_id)->first();
                $po_detail[$indeks]['nama']    = $paket->nama;
                $po_detail[$indeks]['satuan']  = "paket";
            }

            $po_detail[$indeks]['produk_id']       = $value2->produk_id;
            $po_detail[$indeks]['harga']           = $value2->harga;
            $po_detail[$indeks]['jumlah']        = $value2->jumlah;
            $po_detail[$indeks]['serial_number']   = $value2->serial_number;
            $po_detail[$indeks]['jenis_barang_id'] = $value2->jenis_barang_id;
            $po_detail[$indeks]['gudang_id']       = $value2->gudang_id;

            $indeks++;
        }

        return view('pages.transaksi.ajax_po_detail', compact('po_detail'));
    }

    public function retur_beli_tambah_cart()
    {
        $nota_beli_id            = Input::get('nota_beli_id');
        $produk_id          = Input::get('produk_id');
        $serial_number      = Input::get('serial_number');
        $harga              = Input::get('harga');
        $jenis_barang_id    = Input::get('jenis_barang_id');
        $gudang_id          = Input::get('gudang_id');
        $jumlah           = 1;

        if(empty($harga)){
            $harga = 0;
        }

        if($jenis_barang_id == 1){
            $produk = Produk::where('id', $produk_id)->first();
            $jenis  = "PRD";
        }
        elseif ($jenis_barang_id == 2) {
            $produk = Hadiah::where('id', $produk_id)->first();
            $jenis  = "HDH";
        }

        //cek apakah jumlah sesuai dengan sales order
        $cek_jumlah     = 1;
        $cart_content   = Cart::content(1);
        foreach ($cart_content as $key => $value) 
        {
            if($value->id == $jenis.$produk_id."---".$serial_number."---".$gudang_id)
            {
                $cek_jumlah = $cek_jumlah + $value->qty;
            }
        }

        $nota_beli = NotaBeli::where('id', $nota_beli_id)->first();
        $sj_masuk_header = SJMasukHeader::where('id', $nota_beli->sj_masuk_header_id)->first();

        $sj_masuk_detail = SJMasukDetail::where('produk_id', $produk_id)->where('jenis_barang_id', $jenis_barang_id)->where('serial_number', $serial_number)->groupBy('produk_id', 'jenis_barang_id', 'serial_number')->selectRaw('tran_sj_masuk_detail.*, sum(jumlah) as jumlah_total')->first();
        

        if($sj_masuk_detail->jumlah_total < $cek_jumlah)
        {
            echo '<div class="alert alert-danger flat">';
                echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
                  echo '<span aria-hidden="true">&times;</span>';
                echo '</button>';
                echo 'Jumlah tidak sesuai';
            echo '</div>';
        }
        else
        {
            $data = array('id'          => $jenis.$produk_id."---".$serial_number."---".$gudang_id, 
                  'name'        => $produk->nama, 
                  'qty'         => $jumlah, 
                  'price'       => $harga, 
                  'options'     => array('size' => 'large'));
            Cart::add($data);
        }

        $cart_content = Cart::content(1);
        
        return view('pages.transaksi.ajax_retur_admin_cart', compact('cart_content'));
    }

    public function retur_beli_checkout(Request $request)
    {   
        //output berupa Nota Retur Beli. Nomor nota disimpan di No_nota rb_header dan invoice detail

        /* RB HEADER */
        $no_retur       = Input::get('no_retur');
        $nota_beli_id        = Input::get('nota_beli_id');
        $supplier_id   = Input::get('supplier_id');
        $potongan_retur = Input::get('potongan_retur');
        $catatan        = Input::get('catatan');

        date_default_timezone_set("Asia/Jakarta");
        $tanggal        = date("Y-m-d G:i:s");

        $nota_beli = NotaBeli::where('id', $nota_beli_id)->first();
        $sj_masuk_header = SJMasukHeader::where('id', $nota_beli->sj_masuk_header_id)->first();
        
        $rb_header = new RBHeader;

        if(empty($potongan_retur))
        {
            $potongan_retur = 0;
        }
        else{
            $potongan_retur   = str_replace(".", "", $potongan_retur);
        }

        $rb_header->nota_beli_id    = $nota_beli_id;
        $rb_header->no_nota         = $no_retur;
        $rb_header->supplier_id     = $supplier_id;
        $rb_header->tanggal         = $tanggal;
        $rb_header->potongan_retur  = $potongan_retur;
        $rb_header->total_retur     = 0; //sementara 0
        $rb_header->catatan         = $catatan;
        $rb_header->is_lunas        = 0;
        $rb_header->save();    

        $rb_header_id = $rb_header->id;    

        /* RJ DETAIL */

        $cart_content = Cart::content(1);

        $total_retur = 0;

        foreach ($cart_content as $cart) 
        {
            if(substr($cart->id, 0, 3) == "PRD"){
                $cart_id            = substr($cart->id, 3); 
                $cart_id            = explode("---", $cart_id);
                $produk_id          = $cart_id[0];
                $serial_number      = $cart_id[1];
                $gudang_id          = $cart_id[2];
                $jumlah           = $cart->qty;
                $harga              = $cart->price;
                $jenis_barang_id    = 1;

                $produk             = Produk::where('id', $produk_id)->first();
                $deskripsi          = $produk->deskripsi;

                //kurangi stok di produk, produk_gudang, produk_serial_number
                $produk->stok   = $produk->stok - $jumlah;
                $produk->save();

                $produk_gudang = ProdukGudang::where('produk_id', $produk_id)->where('gudang_id', $gudang_id)->first();
                $produk_gudang->stok    = $produk_gudang->stok -  $jumlah;
                $produk_gudang->save();

                $produk_serial_number = ProdukSerialNumber::where('produk_id', $produk_id)->where('gudang_id', $gudang_id)->where('serial_number', $serial_number)->first();
                $produk_serial_number->stok = $produk_serial_number->stok - $jumlah;
                $produk_serial_number->save();
            }
            elseif (substr($cart->id, 0, 3) == "HDH") {
                $cart_id            = substr($cart->id, 3); 
                $cart_id            = explode("---", $cart_id);
                $produk_id          = $cart_id[0];
                $serial_number      = $cart_id[1];
                $gudang_id          = $cart_id[2];
                $jumlah           = $cart->qty;
                $harga              = $cart->price;
                $jenis_barang_id    = 3;

                $hadiah             = Hadiah::where('id', $produk_id)->first();
                $deskripsi          = $hadiah->nama;

                //kurangi stok di hadiah, hadiah_gudang
                $hadiah->stok   = $hadiah->stok - $jumlah;
                $hadiah->save();

                $hadiah_gudang = HadiahGudang::where('hadiah_id', $produk_id)->where('gudang_id', $gudang_id)->first();
                $hadiah_gudang->stok    = $hadiah_gudang->stok -  $jumlah;
                $hadiah_gudang->save();
            }

            $rb_detail = new RBDetail;

            $rb_detail->rb_header_id         = $rb_header_id;
            $rb_detail->produk_id       = $produk_id;
            $rb_detail->gudang_id       = $gudang_id;
            $rb_detail->jenis_barang_id = $jenis_barang_id;
            $rb_detail->serial_number   = $serial_number;
            $rb_detail->jumlah        = $jumlah;
            $rb_detail->deskripsi       = $deskripsi;
            $rb_detail->harga           = $harga;
            $rb_detail->save();

            $total_retur = $total_retur + $harga;
        }

        $total_tagihan = $total_retur - $potongan_retur;

        $rb_header = RBHeader::where('id', $rb_header_id)->first();
        $rb_header->total_retur     = $total_retur;
        $rb_header->total_tagihan   = $total_tagihan;
        $rb_header->save();


        Cart::destroy();    

        return redirect('/nota_retur_beli/'.$rb_header_id);
    }

    public function nota_retur_beli($rb_header_id)
    {
        $rb_header      = RBHeader::with('supplier')->where('id', $rb_header_id)->first();
        $rb_detail_obj  = RBDetail::where('rb_header_id', $rb_header_id)->get();
        
        $perusahaan     = Perusahaan::with('kota')->first();
        $kota_perusahaan = strtoupper($perusahaan->kota->nama);

        if(strpos($kota_perusahaan, 'KABUPATEN') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KABUPATEN', '', $kota_perusahaan)));
        }
        elseif(strpos($kota_perusahaan, 'KOTA') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KOTA', '', $kota_perusahaan)));
        }

        //dapatkan detail data produk, paket di RBDetail
        $rb_detail = [];
        foreach ($rb_detail_obj as $key => $value) {
            if($value->jenis_barang_id == 1){
                $produk = Produk::where('id', $value->produk_id)->first();
                $rb_detail[$key]['nama']    = $produk->nama;
                $rb_detail[$key]['satuan']  = $produk->satuan;
            }
            elseif($value->jenis_barang_id == 2) {
                $hadiah = Hadiah::where('id', $value->produk_id)->first();
                $rb_detail[$key]['nama']    = $hadiah->nama;
                $rb_detail[$key]['satuan']  = $hadiah->satuan;
            }
            elseif($value->jenis_barang_id == 3) {
                $paket = Paket::where('id', $value->produk_id)->first();
                $rb_detail[$key]['nama']    = $paket->nama;
                $rb_detail[$key]['satuan']  = "paket";
            }

            $rb_detail[$key]['serial_number']   = $value->serial_number;
            $rb_detail[$key]['harga']           = $value->harga;
            $rb_detail[$key]['jumlah']        = $value->jumlah;
        }

        $supplier['nama']   = $rb_header->supplier->nama;
        $supplier['alamat'] = $rb_header->supplier->alamat;

        $kota = strtoupper($rb_header->supplier->kota->nama);

        if(strpos($kota, 'KABUPATEN') !== false){
            $kota = str_replace('KABUPATEN', '', $kota);
        }
        elseif(strpos($kota, 'KOTA') !== false){
            $kota = str_replace('KOTA', '', $kota);
        }

        $supplier['alamat'] = $supplier['alamat'].', '.$kota;
        $supplier['telp']   = $rb_header->supplier->telp1;

        $terbilang = $rb_header->total_tagihan;

        Cart::destroy();

        $judul  = "Retur Beli";
        $general['title']       = $judul;
        $general['menu1']       = "Transaksi";
        $general['menu2']       = "Retur Beli";
        $general['menu3']       = $general['title'];
        $general['kode_menu']   = 54;  

        date_default_timezone_set("Asia/Jakarta"); 
        $print_time = new DateTime;

        return view('pages.transaksi.retur_beli.nota_retur_beli', compact('general', 'rb_header', 'rb_detail', 'invoice_detail', 'invoice', 'perusahaan', 'judul', 'terbilang', 'supplier', 'print_time', 'kota_perusahaan'));  
    }

    public function detail_retur_beli($rb_header_id)
    {
        $rb_header      = RBHeader::with('supplier')->where('id', $rb_header_id)->first();
        $rb_detail_obj  = RBDetail::where('rb_header_id', $rb_header_id)->get();

        $general['title']       = "Retur Beli #".$rb_header->no_nota;
        $general['menu1']       = "Transaksi";
        $general['menu2']       = "Retur Beli";
        $general['menu3']       = $general['title'];
        $general['kode_menu']   = 54;
        
        $perusahaan     = Perusahaan::with('kota')->first();
        $kota_perusahaan = strtoupper($perusahaan->kota->nama);

        if(strpos($kota_perusahaan, 'KABUPATEN') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KABUPATEN', '', $kota_perusahaan)));
        }
        elseif(strpos($kota_perusahaan, 'KOTA') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KOTA', '', $kota_perusahaan)));
        }

        //dapatkan detail data produk, paket di RBDetail
        $rb_detail = [];
        foreach ($rb_detail_obj as $key => $value) {
            if($value->jenis_barang_id == 1){
                $produk = Produk::where('id', $value->produk_id)->first();
                $rb_detail[$key]['nama']    = $produk->nama;
                $rb_detail[$key]['satuan']  = $produk->satuan;
            }
            elseif($value->jenis_barang_id == 2) {
                $hadiah = Hadiah::where('id', $value->produk_id)->first();
                $rb_detail[$key]['nama']    = $hadiah->nama;
                $rb_detail[$key]['satuan']  = $hadiah->satuan;
            }
            elseif($value->jenis_barang_id == 3) {
                $paket = Paket::where('id', $value->produk_id)->first();
                $rb_detail[$key]['nama']    = $paket->nama;
                $rb_detail[$key]['satuan']  = "paket";
            }

            $rb_detail[$key]['serial_number']   = $value->serial_number;
            $rb_detail[$key]['harga']           = $value->harga;
            $rb_detail[$key]['jumlah']        = $value->jumlah;
        } 

        return view('pages.transaksi.detail_retur_beli', compact('general', 'rb_header', 'rb_detail'));  
    }

    public function nota_retur_beli_print($rb_header_id)
    {
        $judul                  = "Retur Beli";
        $rb_header              = RBHeader::with('supplier')->where('id', $rb_header_id)->first();
        $rb_detail_obj  = RBDetail::with('produk', 'hadiah', 'paket')->where('rb_header_id', $rb_header_id)->get();

        $perusahaan             = Perusahaan::with('kota')->first();
        $kota_perusahaan = strtoupper($perusahaan->kota->nama);

        if(strpos($kota_perusahaan, 'KABUPATEN') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KABUPATEN', '', $kota_perusahaan)));
        }
        elseif(strpos($kota_perusahaan, 'KOTA') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KOTA', '', $kota_perusahaan)));
        }

        //dapatkan detail data produk, paket di RBDetail
        $rb_detail = [];
        foreach ($rb_detail_obj as $key => $value) {
            if($value->jenis_barang_id == 1){
                $produk = Produk::where('id', $value->produk_id)->first();
                $rb_detail[$key]['nama']    = $produk->nama;
                $rb_detail[$key]['satuan']  = $produk->satuan;
            }
            elseif($value->jenis_barang_id == 2) {
                $hadiah = Hadiah::where('id', $value->produk_id)->first();
                $rb_detail[$key]['nama']    = $hadiah->nama;
                $rb_detail[$key]['satuan']  = $hadiah->satuan;
            }
            elseif($value->jenis_barang_id == 3) {
                $paket = Paket::where('id', $value->produk_id)->first();
                $rb_detail[$key]['nama']    = $paket->nama;
                $rb_detail[$key]['satuan']  = "paket";
            }

            $rb_detail[$key]['serial_number']   = $value->serial_number;
            $rb_detail[$key]['harga']           = $value->harga;
            $rb_detail[$key]['jumlah']        = $value->jumlah;
        }

        date_default_timezone_set("Asia/Jakarta"); 
        $print_time = new DateTime;

        $supplier['nama']   = $rb_header->supplier->nama;
        $supplier['alamat'] = $rb_header->supplier->alamat;

        $kota = strtoupper($rb_header->supplier->kota->nama);

        if(strpos($kota, 'KABUPATEN') !== false){
            $kota = str_replace('KABUPATEN', '', $kota);
        }
        elseif(strpos($kota, 'KOTA') !== false){
            $kota = str_replace('KOTA', '', $kota);
        }

        $supplier['alamat'] = $supplier['alamat'].', '.$kota;
        $supplier['telp']   = $rb_header->supplier->telp1;

        return view('pages.transaksi.retur_beli.nota_retur_beli_print', compact('judul', 'rb_header', 'rb_detail', 'perusahaan', 'print_time', 'supplier', 'kota_perusahaan'));
    }

    public function nota_retur_beli_pdf($rb_header_id)
    {
        $judul                  = "Retur Beli";
        $rb_header              = RBHeader::with('supplier')->where('id', $rb_header_id)->first();
        $rb_detail_obj  = RBDetail::with('produk', 'hadiah', 'paket')->where('rb_header_id', $rb_header_id)->get();

        $perusahaan             = Perusahaan::with('kota')->first();
        $kota_perusahaan = strtoupper($perusahaan->kota->nama);

        if(strpos($kota_perusahaan, 'KABUPATEN') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KABUPATEN', '', $kota_perusahaan)));
        }
        elseif(strpos($kota_perusahaan, 'KOTA') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KOTA', '', $kota_perusahaan)));
        }

        //dapatkan detail data produk, paket di RBDetail
        $rb_detail = [];
        foreach ($rb_detail_obj as $key => $value) {
            if($value->jenis_barang_id == 1){
                $produk = Produk::where('id', $value->produk_id)->first();
                $rb_detail[$key]['nama']    = $produk->nama;
                $rb_detail[$key]['satuan']  = $produk->satuan;
            }
            elseif($value->jenis_barang_id == 2) {
                $hadiah = Hadiah::where('id', $value->produk_id)->first();
                $rb_detail[$key]['nama']    = $hadiah->nama;
                $rb_detail[$key]['satuan']  = $hadiah->satuan;
            }
            elseif($value->jenis_barang_id == 3) {
                $paket = Paket::where('id', $value->produk_id)->first();
                $rb_detail[$key]['nama']    = $paket->nama;
                $rb_detail[$key]['satuan']  = "paket";
            }

            $rb_detail[$key]['serial_number']   = $value->serial_number;
            $rb_detail[$key]['harga']           = $value->harga;
            $rb_detail[$key]['jumlah']        = $value->jumlah;
        }

        date_default_timezone_set("Asia/Jakarta"); 
        $print_time = new DateTime;

        $supplier['nama']   = $rb_header->supplier->nama;
        $supplier['alamat'] = $rb_header->supplier->alamat;

        $kota = strtoupper($rb_header->supplier->kota->nama);

        if(strpos($kota, 'KABUPATEN') !== false){
            $kota = str_replace('KABUPATEN', '', $kota);
        }
        elseif(strpos($kota, 'KOTA') !== false){
            $kota = str_replace('KOTA', '', $kota);
        }

        $supplier['alamat'] = $supplier['alamat'].', '.$kota;
        $supplier['telp']   = $rb_header->supplier->telp1;

        $pdf = PDF::loadView('pages.transaksi.retur_beli.nota_retur_beli_pdf', compact('judul', 'rb_header', 'rb_detail', 'perusahaan', 'print_time', 'supplier', 'kota_perusahaan'))->setPaper('a4', 'landscape');

        return $pdf->stream();
    }
}
