<?php

namespace App\Http\Controllers\Transaksi;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Gloudemans\Shoppingcart\Facades\Cart;
use App\Models\TransaksiPenjualan\TransaksiJasa;
use App\Models\Pelanggan\Pelanggan;
use PDF;
use Hashids\Hashids;
use Validator;
use DateTime;
use Input;
use App\Models\Perusahaan;

class TransaksiJasaController extends Controller
{
    public function transaksi_jasa()
    {
        $general['title']       = "Transaksi Jasa";
        $general['menu1']       = "Transaksi";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 513;

        $transaksi_jasa = TransaksiJasa::with('pelanggan')->orderBy('tanggal', 'desc')->orderBy('id', 'desc')->get();

        return view('pages.transaksi.transaksi_jasa.transaksi_jasa', compact('general', 'transaksi_jasa'));
    }

    public function tambah_transaksi_jasa()
    {
        $general['title']       = "Tambah Transaksi Jasa";
        $general['menu1']       = "Transaksi";
        $general['menu2']       = "Transaksi Jasa";
        $general['menu3']       = $general['title'];
        $general['kode_menu']   = 513;

        $pelanggan = Pelanggan::orderBy('nama', 'asc')->get();

        date_default_timezone_set("Asia/Jakarta");
        $tanggal        = new DateTime;

        $nomor_invoice  = DB::table('tref_nomor_invoice')->where('id', 12)->first();

        if(empty($nomor_invoice)){
            $format_invoice = "";
        }
        else{
            $format_invoice = $nomor_invoice->format;
        }

        
        $ori_no_nota = $format_invoice;

        $cek_dd = strpos($format_invoice, 'DD');
        if(!empty($cek_dd))
        {
            $format_invoice = str_replace('DD', $tanggal->format('d'), $format_invoice);
        }

        $cek_mm = strpos($format_invoice, 'MM');
        if(!empty($cek_mm))
        {
            $format_invoice = str_replace('MM', $tanggal->format('m'), $format_invoice);
        }

        $cek_yyyy = strpos($format_invoice, 'YYYY');
        if(!empty($cek_yyyy))
        {
            $format_invoice = str_replace('YYYY', $tanggal->format('Y'), $format_invoice);
        }

        $cek_yy = strpos($format_invoice, 'YY');
        if(!empty($cek_yy))
        {
            $format_invoice = str_replace('YY', $tanggal->format('y'), $format_invoice);
        }

        //cek maksimal 10
        $number = '##########';

        for ($i=9; $i > 0; $i--) { 
            $number     = substr($number, 0, $i);
            $cek_number = strpos($format_invoice, $number);
            $nol      = "";
            if(!empty($cek_number))
            {
                $transaksi_jasa      = TransaksiJasa::orderBy('id', 'desc')->first();
                if(empty($transaksi_jasa))
                    $get_num = 1;
                else
                    $get_num        = (int)$transaksi_jasa->id + 1;

                for ($j=0; $j < $i - count($get_num); $j++) { 
                    $nol = $nol."0";
                }

                $get_num = $nol.$get_num;
                $format_invoice = str_replace($number, $get_num, $format_invoice);
                break;
            }
        }

        $no_nota     = $format_invoice;

        return view('pages.transaksi.transaksi_jasa.tambah_transaksi_jasa', compact('general', 'pelanggan', 'no_nota', 'ori_no_nota'));
    }

    public function ubah_format_no_transaksi_jasa(Request $request)
    {
        $this->validate($request, [
            'format' => 'required',
        ]);

        $format = strtoupper(Input::get('format'));

        //cek ada atau tidak
        $cek = DB::table('tref_nomor_invoice')->where('id', 12)->first();

        if(empty($cek)){
            DB::table('tref_nomor_invoice')->insert(['id' => 12, 'format' => $format]);
        }
        else{
            DB::table('tref_nomor_invoice')->where('id', 12)->update(['format' => $format]);
        }        

        $request->session()->flash('message', 'Format nomor transaksi jasa berhasil diperbarui');
        return redirect('/tambah_transaksi_jasa');
    }

    public function do_tambah_transaksi_jasa(Request $request)
    {
        $this->validate($request, [
            'no_nota' => 'required',
            'pelanggan_id' => 'required',
            'tanggal' => 'required',
            'deskripsi' => 'required',
            'biaya' => 'required',
        ]);

        $no_nota        = Input::get('no_nota');
        $pelanggan_id   = Input::get('pelanggan_id');
        $tanggal        = Input::get('tanggal');
        $deskripsi      = Input::get('deskripsi');
        $biaya          = Input::get('biaya');

        $tanggal    = new DateTime($tanggal);
        $biaya      = str_replace('.', '', $biaya);

        $transaksi_jasa = new TransaksiJasa;
        $transaksi_jasa->no_nota        = $no_nota;
        $transaksi_jasa->pelanggan_id   = $pelanggan_id;
        $transaksi_jasa->tanggal        = $tanggal;
        $transaksi_jasa->deskripsi      = $deskripsi;
        $transaksi_jasa->biaya          = $biaya;
        $transaksi_jasa->flag           = 1;  //anggapannya ketika tambah biaya jasa, pekerjaan juga sudah selesai
        $transaksi_jasa->is_lunas       = 0;
        $transaksi_jasa->save();

        return redirect('/nota_transaksi_jasa/'.$transaksi_jasa->id);
    }

    public function nota_transaksi_jasa($id)
    {
        $judul  = "Nota Transaksi Jasa";
        $general['title']       = $judul;
        $general['menu1']       = "Transaksi";
        $general['menu2']       = "Transaksi Jasa";
        $general['menu3']       = $general['title'];
        $general['kode_menu']   = 513;  

        $transaksi_jasa = TransaksiJasa::where('id', $id)->first();
        $perusahaan     = Perusahaan::with('kota')->first();

        date_default_timezone_set("Asia/Jakarta"); 
        $print_time = new DateTime;

        return view('pages.transaksi.transaksi_jasa.nota_transaksi_jasa', compact('general', 'judul', 'transaksi_jasa', 'perusahaan', 'print_time'));
    }

    public function nota_transaksi_jasa_print($id)
    {
        $judul          = "Nota Transaksi Jasa";
        $transaksi_jasa = TransaksiJasa::where('id', $id)->first();
        $perusahaan     = Perusahaan::with('kota')->first();

        date_default_timezone_set("Asia/Jakarta"); 
        $print_time = new DateTime;

        return view('pages.transaksi.transaksi_jasa.nota_transaksi_jasa_print', compact('judul', 'transaksi_jasa', 'perusahaan', 'print_time'));
    }

    public function nota_transaksi_jasa_pdf($id)
    {
        $judul          = "Nota Transaksi Jasa";
        $transaksi_jasa = TransaksiJasa::where('id', $id)->first();
        $perusahaan     = Perusahaan::with('kota')->first();

        date_default_timezone_set("Asia/Jakarta"); 
        $print_time = new DateTime;

        $pdf = PDF::loadView('pages.transaksi.transaksi_jasa.nota_transaksi_jasa_pdf', compact('judul', 'transaksi_jasa', 'perusahaan', 'print_time'))->setPaper('a4', 'landscape');
        return $pdf->stream();
    }
}
