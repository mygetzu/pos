<?php

namespace App\Http\Controllers\Transaksi;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Gloudemans\Shoppingcart\Facades\Cart;
use Input;
use Auth;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Session;
use Promocodes;
use Response;
use DateTime;
use PDF;
use Hashids\Hashids;
use Validator;
use Illuminate\Http\RedirectResponse;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use App\User;
use App\Models\Perusahaan;
use App\Models\Gudang\Gudang;
use App\Models\Gudang\ProdukGudang;
use App\Models\Gudang\HadiahGudang;
use App\Models\Produk\Produk;
use App\Models\Produk\Hadiah;
use App\Models\Produk\ProdukSerialNumber;
use App\Models\Supplier\Supplier;
use App\Models\Pengaturan\PengaturanPO;
use App\Models\TransaksiPembelian\POHeader;
use App\Models\TransaksiPembelian\PODetail;
use App\Models\TransaksiPembelian\PODiskon;
use App\Models\TransaksiPembelian\POAlamatPengiriman;
use App\Models\AdminEcommerce\Konten;

class PurchaseOrderController extends Controller
{
    public function purchase_order()
    {
        $general['title']       = "Purchase Order";
        $general['menu1']       = "Transaksi";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 51;        

        $po_header      = POHeader::with('supplier')->orderBy('tanggal', 'desc')->get();
        $pengaturan_po  = PengaturanPO::first();
        
        return view('pages.transaksi.purchase_order', compact('general', 'po_header', 'pengaturan_po'));
    }

    public function tambah_purchase_order(Request $request)
    {
        $general['title']       = "Tambah Purchase Order";
        $general['menu1']       = "Transaksi";
        $general['menu2']       = "Purchase Order";
        $general['menu3']       = $general['title'];
        $general['kode_menu']   = 51;        

        $supplier = Supplier::with('kota')->get();

        Cart::destroy();  

        date_default_timezone_set("Asia/Jakarta");
        $tanggal        = new DateTime;

        $nomor_invoice      = DB::table('tref_nomor_invoice')->where('id', 1)->first();
        $format_invoice     = $nomor_invoice->format;
        $ori_format_invoice = $format_invoice;

        $cek_dd = strpos($format_invoice, 'DD');
        if(!empty($cek_dd))
        {
            $format_invoice = str_replace('DD', $tanggal->format('d'), $format_invoice);
        }

        $cek_mm = strpos($format_invoice, 'MM');
        if(!empty($cek_mm))
        {
            $format_invoice = str_replace('MM', $tanggal->format('m'), $format_invoice);
        }

        $cek_yyyy = strpos($format_invoice, 'YYYY');
        if(!empty($cek_yyyy))
        {
            $format_invoice = str_replace('YYYY', $tanggal->format('Y'), $format_invoice);
        }
        
        $cek_yy = strpos($format_invoice, 'YY');
        if(!empty($cek_yy))
        {
            $format_invoice = str_replace('YY', $tanggal->format('y'), $format_invoice);
        }

        //cek maksimal 10
        $number = '##########';

        for ($i=9; $i > 0; $i--) { 
            $number     = substr($number, 0, $i);
            $cek_number = strpos($format_invoice, $number);
            $nol        = "";
            if(!empty($cek_number))
            {
                $po_header      = POHeader::orderBy('id', 'desc')->first();
                if(empty($po_header))
                    $get_num = 1;
                else
                    $get_num        = (int)$po_header->id + 1;
                
                for ($j=0; $j < $i - strlen($get_num); $j++) { 
                    $nol = $nol."0";
                }

                $get_num = $nol.$get_num;
                $format_invoice = str_replace($number, $get_num, $format_invoice);
                break;
            }
        }

        $no_invoice     = $format_invoice;
        $pengaturan_po  = PengaturanPO::first();

        return view('pages.transaksi.tambah_purchase_order', compact('general', 'supplier', 'format_invoice', 'no_invoice', 'ori_format_invoice', 'metode_pembayaran', 'pengaturan_po'));
    }

    public function ubah_format_no_invoice_po(Request $request)
    {
        $format = strtoupper(Input::get('format'));

        DB::table('tref_nomor_invoice')->where('id', 1)->update(['format' => $format]);

        $request->session()->flash('message', 'Format No. PO berhasil diperbarui');
        return redirect('/tambah_purchase_order');
    }

    public function get_po_produk(Request $request)
    {
        $supplier_id    = Input::get('id');

        $produk = DB::select("SELECT * FROM (
            SELECT p.id, p.stok, p.stok_dipesan, p.nama, p.harga_retail, 
            (SELECT file_gambar FROM tran_produk_galeri WHERE produk_id = p.id LIMIT 1) AS file_gambar, 
            ps.harga_terakhir, 'P' AS jenis
            FROM tmst_produk p, tran_produk_supplier ps 
            WHERE p.id=ps.produk_id AND ps.supplier_id='$supplier_id' AND p.is_aktif = 1 
            GROUP BY p.id 
            UNION
            SELECT h.id, h.stok, h.stok_dipesan, h.nama, 0 AS harga_retail, h.file_gambar, hs.harga_terakhir, 'H' AS jenis
            FROM tmst_hadiah h, tran_hadiah_supplier hs WHERE h.id=hs.hadiah_id AND hs.supplier_id='$supplier_id' 
            GROUP BY h.id
            ) t
            GROUP BY id");

        $obj_gambar_produk_default   = Konten::where('nama', 'gambar_produk_default')->first();
        if(empty($obj_gambar_produk_default)){
            $gambar_produk_default = "";
        }
        else{
            $gambar_produk_default  = $obj_gambar_produk_default->keterangan;
        }

        return view('pages.transaksi.ajax_po_produk', compact('produk', 'gambar_produk_default'));
    }

    public function po_proses()
    {
        //output berupa invoice PO
        $no_invoice         = Input::get('no_nota');
        $catatan            = Input::get('catatan');
        $supplier_id        = Input::get('supplier_id');
        $diskon             = Input::get('diskon');
        $alamat_pengiriman  = Input::get('alamat_pengiriman');
        $syarat_ketentuan   = Input::get('syarat_ketentuan');
        $ppn                = Input::get('ppn');

        if(empty($ppn)){
            $ppn = 0;
        }

        if(empty($diskon)){
            $diskon = 0;
        }
        else{
            $diskon   = str_replace(".", "", $diskon);
        }

        date_default_timezone_set("Asia/Jakarta");
        $tanggal        = new DateTime;
        $total_tagihan  = 0;

        /* PO HEADER */

        $po_header = new POHeader;
        $po_header->no_purchase_order       = $no_invoice;
        $po_header->supplier_id             = $supplier_id;
        $po_header->tanggal                 = $tanggal;
        $po_header->catatan                 = $catatan;
        $po_header->flag_sj_masuk           = 0;
        $po_header->syarat_ketentuan        = $syarat_ketentuan;
        $po_header->ppn                     = $ppn;
        $po_header->save();

        $po_header_id = $po_header->id;

        /* PO DETAIL */

        $cart_content = Cart::content(1);

        foreach ($cart_content as $cart) 
        {
            $cart_id        = $cart->id;

            if(substr($cart_id, 0, 3) == 'HDH')
            {
                $jenis_barang_id    = 2;
                $id                 = substr($cart_id, 3);
                $produk             = DB::table('tmst_hadiah')->where('id', $id)->first();

                //SIMPAN HARGA TERAKHIR DAN TANGGAL TERAKHIR DI PRODUK SUPPLIER
                DB::table('tran_hadiah_supplier')->where('hadiah_id', $id)->where('supplier_id', $supplier_id)->update(['harga_terakhir' => $cart->price, 'tanggal_terakhir' => $tanggal]);
            }
            else
            {
                $jenis_barang_id    = 1;
                if(substr($cart_id, 0, 3) == 'DIS')
                    $id                 = substr($cart_id, 3);
                else
                    $id                 = $cart_id;

                $produk             = DB::table('tmst_produk')->where('id', $id)->first();

                //SIMPAN HARGA TERAKHIR DAN TANGGAL TERAKHIR DI PRODUK SUPPLIER
                DB::table('tran_produk_supplier')->where('produk_id', $id)->where('supplier_id', $supplier_id)->update(['harga_terakhir' => $cart->price, 'tanggal_terakhir' => $tanggal]);
            }

            $cart_name = $cart->name;
            $cart_name = explode('---', $cart_name);

            if(!empty($cart_name[1]))
                $deskripsi = $cart_name[1];
            else
                $deskripsi = "";

            $jumlah = $cart->qty;
            $harga  = $cart->price;

            $po_detail = new PODetail;
            $po_detail->po_header_id    = $po_header_id;
            $po_detail->produk_id       = $id;
            $po_detail->jenis_barang_id = $jenis_barang_id;
            $po_detail->jumlah          = $jumlah;
            $po_detail->jumlah_terkirim = 0;
            $po_detail->deskripsi       = $deskripsi;
            $po_detail->harga           = $harga;
            $po_detail->save();

            $total_tagihan = $total_tagihan+($jumlah*$harga);
        }

        //total tagihan harus ditambah dengan PPN
        $harga_ppn      = ($total_tagihan*$ppn/100);
        $total_tagihan  = $total_tagihan + $harga_ppn;

        /* PO DISKON */

        if($diskon > 0){
            $po_diskon = new PODiskon;
            $po_diskon->po_header_id    = $po_header_id;
            $po_diskon->nominal         = $diskon;
            $po_diskon->is_dipakai      = 0;
            $po_diskon->save();

            $total_tagihan = $total_tagihan - $diskon;
        }

        $po_header = POHeader::where('id', $po_header_id)->first();
        $po_header->total_tagihan = $total_tagihan;
        $po_header->save();

        /* PO ALAMAT PENGIRIMAN */
        $po_alamat_pengiriman = new POAlamatPengiriman;
        $po_alamat_pengiriman->po_header_id = $po_header_id;
        $po_alamat_pengiriman->alamat = $alamat_pengiriman;
        $po_alamat_pengiriman->save();

        Cart::destroy();

        return redirect('/nota_purchase_order/'.$po_header_id);
    }

    public function nota_purchase_order($po_header_id)
    {
        $po_header = POHeader::with('supplier')->where('id', $po_header_id)->first();
        $po_detail = PODetail::with('produk', 'hadiah')->where('po_header_id', $po_header_id)->get();
        $po_diskon = PODiskon::where('po_header_id', $po_header_id)->first();
        $po_alamat_pengiriman = POAlamatPengiriman::where('po_header_id', $po_header_id)->first();

        $judul  = "Purchase Order";
        $general['title']       = $judul;
        $general['menu1']       = "Transaksi";
        $general['menu2']       = "Purchase Order";
        $general['menu3']       = $general['title'];
        $general['kode_menu']   = 51;  

        $perusahaan = Perusahaan::with('kota')->first();

        $kota_perusahaan = strtoupper($perusahaan->kota->nama);

        if(strpos($kota_perusahaan, 'KABUPATEN') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KABUPATEN', '', $kota_perusahaan)));
        }
        elseif(strpos($kota_perusahaan, 'KOTA') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KOTA', '', $kota_perusahaan)));
        }

        $supplier['nama']   = $po_header->supplier->nama;
        $supplier['alamat'] = $po_header->supplier->alamat;

        $kota = strtoupper($po_header->supplier->kota->nama);

        if(strpos($kota, 'KABUPATEN') !== false){
            $kota = str_replace('KABUPATEN', '', $kota);
        }
        elseif(strpos($kota, 'KOTA') !== false){
            $kota = str_replace('KOTA', '', $kota);
        }

        $supplier['alamat'] = $supplier['alamat'].', '.$kota;
        $supplier['telp']   = $po_header->supplier->telp1;

        date_default_timezone_set("Asia/Jakarta"); 
        $print_time = new DateTime;
        
        return view('pages.transaksi.purchase_order.nota_purchase_order', compact('general', 'po_header', 'po_detail', 'po_diskon', 'judul', 'perusahaan', 'print_time', 'po_alamat_pengiriman', 'supplier', 'kota_perusahaan'));    
    }

    public function nota_purchase_order_print($po_header_id)
    {
        $po_header  = POHeader::with('supplier')->where('id', $po_header_id)->first();
        $po_detail  = PODetail::with('produk', 'hadiah')->where('po_header_id', $po_header_id)->get();
        $po_diskon  = PODiskon::where('po_header_id', $po_header_id)->first();
        $po_alamat_pengiriman = POAlamatPengiriman::where('po_header_id', $po_header_id)->first();
        $perusahaan = Perusahaan::with('kota')->first();

        $kota_perusahaan = strtoupper($perusahaan->kota->nama);

        if(strpos($kota_perusahaan, 'KABUPATEN') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KABUPATEN', '', $kota_perusahaan)));
        }
        elseif(strpos($kota_perusahaan, 'KOTA') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KOTA', '', $kota_perusahaan)));
        }

        $judul  = "Purchase Order";

        date_default_timezone_set("Asia/Jakarta"); 
        $print_time = new DateTime;

        $supplier['nama']   = $po_header->supplier->nama;
        $supplier['alamat'] = $po_header->supplier->alamat;

        $kota = strtoupper($po_header->supplier->kota->nama);

        if(strpos($kota, 'KABUPATEN') !== false){
            $kota = str_replace('KABUPATEN', '', $kota);
        }
        elseif(strpos($kota, 'KOTA') !== false){
            $kota = str_replace('KOTA', '', $kota);
        }

        $supplier['alamat'] = $supplier['alamat'].', '.$kota;
        $supplier['telp']   = $po_header->supplier->telp1;

        return view('pages.transaksi.purchase_order.nota_purchase_order_print', compact('po_header', 'po_detail', 'po_diskon', 'perusahaan', 'judul', 'print_time', 'po_alamat_pengiriman', 'supplier', 'kota_perusahaan'));
    }

    public function nota_purchase_order_pdf($po_header_id)
    {
        $po_header  = POHeader::with('supplier')->where('id', $po_header_id)->first();
        $po_detail  = PODetail::with('produk', 'hadiah')->where('po_header_id', $po_header_id)->get();
        $po_diskon  = PODiskon::where('po_header_id', $po_header_id)->first();
        $po_alamat_pengiriman = POAlamatPengiriman::where('po_header_id', $po_header_id)->first();
        $perusahaan = Perusahaan::with('kota')->first();

        $kota_perusahaan = strtoupper($perusahaan->kota->nama);

        if(strpos($kota_perusahaan, 'KABUPATEN') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KABUPATEN', '', $kota_perusahaan)));
        }
        elseif(strpos($kota_perusahaan, 'KOTA') !== false){
            $kota_perusahaan = ucwords(strtolower(str_replace('KOTA', '', $kota_perusahaan)));
        }

        $judul  = "Purchase Order";

        date_default_timezone_set("Asia/Jakarta"); 
        $print_time = new DateTime;

        $supplier['nama']   = $po_header->supplier->nama;
        $supplier['alamat'] = $po_header->supplier->alamat;

        $kota = strtoupper($po_header->supplier->kota->nama);

        if(strpos($kota, 'KABUPATEN') !== false){
            $kota = str_replace('KABUPATEN', '', $kota);
        }
        elseif(strpos($kota, 'KOTA') !== false){
            $kota = str_replace('KOTA', '', $kota);
        }

        $supplier['alamat'] = $supplier['alamat'].', '.$kota;
        $supplier['telp']   = $po_header->supplier->telp1;
        
        $pdf = PDF::loadView('pages.transaksi.purchase_order.nota_purchase_order_pdf', compact('po_header', 'po_detail', 'po_diskon', 'perusahaan', 'judul', 'print_time', 'po_alamat_pengiriman', 'supplier', 'kota_perusahaan'))->setPaper('a4', 'landscape');
        return $pdf->stream();
    }

    public function detail_purchase_order($po_header_id)
    {
        $po_header = POHeader::with('supplier')->where('id', $po_header_id)->first();

        $general['title']       = "Detail PO #".$po_header->no_purchase_order;
        $general['menu1']       = "Transaksi";
        $general['menu2']       = "Purchase Order";
        $general['menu3']       = $general['title'];
        $general['kode_menu']   = 51;

        $po_detail  = PODetail::with('produk', 'hadiah')->where('po_header_id', $po_header_id)->get();
        $po_diskon  = PODiskon::where('po_header_id', $po_header_id)->first();

        return view('pages.transaksi.detail_purchase_order', compact('general', 'po_detail', 'po_header', 'po_diskon'));
    }

    public function pengaturan_po(Request $request)
    {
        $alamat_pengiriman  = Input::get('alamat_pengiriman');
        $syarat_ketentuan   = Input::get('syarat_ketentuan');
        $checkbox_ppn       = Input::get('checkbox_ppn');
        $ppn                = Input::get('ppn');

        $pengaturan_po = PengaturanPO::where('id', 1)->first();

        if(empty($pengaturan_po)){
            $pengaturan_po = new PengaturanPO;
            $pengaturan_po->alamat_pengiriman   = $alamat_pengiriman;
            $pengaturan_po->syarat_ketentuan    = $syarat_ketentuan;
            if($checkbox_ppn){
                $pengaturan_po->ppn             = $ppn;
            }
            else{
                $pengaturan_po->ppn             = "";
            }
            $pengaturan_po->save();
        }
        else{
            $pengaturan_po->alamat_pengiriman   = $alamat_pengiriman;
            $pengaturan_po->syarat_ketentuan    = $syarat_ketentuan;
            if($checkbox_ppn){
                $pengaturan_po->ppn             = $ppn;
            }
            else{
                $pengaturan_po->ppn             = "";
            }
            $pengaturan_po->save();
        }

        $request->session()->flash('message', 'Pengaturan Purchase Order telah diperbarui');
        return redirect('/tambah_purchase_order');
    }
}
