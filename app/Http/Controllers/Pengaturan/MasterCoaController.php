<?php

namespace App\Http\Controllers\Pengaturan;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Input;
use Validator;
use DateTime;
use App\Models\Pengaturan\MasterCOA;
use App\Models\Pengaturan\NamaTransaksi;
use App\Models\Pengaturan\PengaturanJurnal;
use App\Models\Pengaturan\GroupAccount;
use File;
use Response;

class MasterCoaController extends Controller {

    public function master_coa() {
        $general['title'] = "Master COA (Chart of Account)";
        $general['menu1'] = "Pengaturan";
        $general['menu2'] = $general['title'];
        $general['kode_menu'] = 22;

        $master_coa = MasterCOA::orderBy('kode', 'asc')->get();
        $root = MasterCOA::where('level', 0)->orderBy('kode', 'desc')->first();
        if (empty($master_coa) || empty($root)) {
            $new_kode = 1;
        } else {
            $new_kode = (int) $root->kode + 1;
        }

        if ($new_kode < 10) {
            $new_kode = "00" . $new_kode;
        }

        if ($new_kode < 100 && $new_kode > 10) {
            $new_kode = "0" . $new_kode;
        }

        $group_account = GroupAccount::all();

        //jika master coa sedang digunakan di pengaturan jurnal, maka tidak dapat dihapus

        return view('pages.pengaturan.master_coa', compact('general', 'master_coa', 'new_kode', 'group_account'));
    }

    public function get_data_akun_baru() {
        $kode_parent = Input::get('kode_parent');
        $master_coa = MasterCOA::where('parent', $kode_parent)->get();

        $kode = count($master_coa) + 1;

        if ($kode < 10) {
            $kode = "00" . $kode;
        }

        if ($kode < 100 && $kode > 10) {
            $kode = "0" . $kode;
        }

        $data = [];
        if ($kode_parent == '-') {
            $data['kode'] = $kode;
            $data['level'] = 0;
            $data['nama_parent'] = '-';
            $data['kode_parent'] = '-';
        } else {
            $parent = MasterCOA::where('kode', $kode_parent)->orderBy('kode', 'desc')->first();

            $data['kode'] = $parent->kode . "." . $kode;
            $data['level'] = (int) $parent->level + 1;
            $data['nama_parent'] = $parent->nama;
            $data['kode_parent'] = $parent->kode;
        }

        return Response::json($data);
    }

    public function tambah_master_coa(Request $request) {
        $this->validate($request, [
            'nama' => 'required',
        ]);

        $kode = Input::get('kode');

        //cek apakah kode sudah ada
        $cek_kode = MasterCOA::where('kode', $kode)->first();
        if (!empty($cek_kode)) {
            $request->session()->flash('message_error', 'Kode sudah ada');
            return redirect('/master_coa');
        }

        $master_coa = new MasterCOA;

        $master_coa->kode = $kode;
        $master_coa->nama = ucwords(Input::get('nama'));
        $master_coa->level = Input::get('level');
        $master_coa->parent = Input::get('parent');
        $master_coa->group_kode = Input::get('group_kode');
        $master_coa->save();

        $request->session()->flash('message', 'Akun berhasil ditambahkan');
        return redirect('/master_coa');
    }

    public function ubah_master_coa(Request $request) {
        $this->validate($request, [
            'nama' => 'required',
        ]);

        $kode = Input::get('kode');
        $id = Input::get('id');

        //cek apakah kode sudah ada yang bukan id ini
        $cek_kode = MasterCOA::where('kode', $kode)->where('id', '!=', $id)->first();
        if (!empty($cek_kode)) {
            $request->session()->flash('message_error', 'Kode sudah ada');
            return redirect('/master_coa');
        }

        $master_coa = MasterCOA::where('id', $id)->first();

        if ($master_coa->kode != $kode) { //jika kode berubah
            //kode yang dapat diubah hanya kode dirinya sendiri, kode parent tidak dapat diubah
            $array_kode_baru = explode('.', $kode);
            $array_kode_lama = explode('.', $master_coa->kode);

            //jumlah array kode yang baru harus sama dengan yang lama
            if (count($array_kode_baru) != count($array_kode_lama)) {
                $request->session()->flash('message_error', 'Kode tidak sesuai');
                return redirect('/master_coa');
            }

            for ($i = 0; $i < count($array_kode_baru); $i++) {
                if ($i < count($array_kode_baru) - 1 && $array_kode_baru[$i] != $array_kode_lama[$i]) {
                    $request->session()->flash('message_error', 'Tidak dapat mengubah kode parent');
                    return redirect('/master_coa');
                    //yang dapat diubah hanya kode yang terakhir, kode parent tidak boleh diubah
                }
            }

            $this->ubah_child($master_coa->kode, $kode); //panggil fungsi rekursif ubah child
        }

        $master_coa->kode = $kode;
        $master_coa->nama = ucwords(Input::get('nama'));
        $master_coa->level = Input::get('level');
        $master_coa->parent = Input::get('parent');
        $master_coa->group_kode = Input::get('group_kode');
        $master_coa->save();

        $request->session()->flash('message', 'Akun berhasil diperbarui');
        return redirect('/master_coa');
    }

    function ubah_child($old_kode_parent, $new_kode_parent) {
        //ubah kode child apabila dia punya child
        $get_child = MasterCOA::where('parent', $old_kode_parent)->get();

        foreach ($get_child as $value) {
            $array_kode_child = explode('.', $value->kode);
            //ambil array terakhir

            $last_array_kode = $array_kode_child[count($array_kode_child) - 1];
            $new_kode = $new_kode_parent . '.' . $last_array_kode;

            $child_coa = MasterCOA::where('id', $value->id)->first();
            $child_coa->kode = $new_kode;
            $child_coa->parent = $new_kode_parent;
            $child_coa->save();

            $this->ubah_child($value->kode, $new_kode); //ubah childnya apabila dia punya child
        }

        return;
    }

    public function hapus_master_coa(Request $request) {
        $kode = Input::get('kode');

        $master_coa = MasterCOA::where('kode', $kode)->first();
        $master_coa->delete();

        $request->session()->flash('message', 'Akun berhasil dihapus');
        return redirect('/master_coa');
    }

}
