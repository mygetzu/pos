<?php

namespace App\Http\Controllers\Pengaturan;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Input;
use Validator;
use DateTime;
use File;
use Response;
use App\Models\Pengaturan\MasterCOA;
use App\Models\Pengaturan\NamaTransaksi;
use App\Models\Pengaturan\PengaturanJurnal;
use App\Models\Pengaturan\GroupAccount;

class PengaturanJurnalController extends Controller
{
    public function pengaturan_jurnal()
    {
        $general['title']       = "Pengaturan Jurnal";
        $general['menu1']       = "Pengaturan";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 24;

        $nota_beli              = NamaTransaksi::where('transaksi', 'nota_beli')->get();
        $nota_jual              = NamaTransaksi::where('transaksi', 'nota_jual')->get();
        $retur_beli             = NamaTransaksi::where('transaksi', 'retur_beli')->get();
        $retur_jual             = NamaTransaksi::where('transaksi', 'retur_jual')->get();
        $jasa_servis            = NamaTransaksi::where('transaksi', 'jasa_servis')->get();
        $pembayaran_supplier    = NamaTransaksi::where('transaksi', 'pembayaran_supplier')->get();
        $pembayaran_pelanggan   = NamaTransaksi::where('transaksi', 'pembayaran_pelanggan')->get();
        $transaksi_jasa         = NamaTransaksi::where('transaksi', 'transaksi_jasa')->get();

        $master_coa     = MasterCOA::all();

        //algo get last child
        $kode_akun = [];
        $indeks = 0;
        foreach ($master_coa as $val) {
            //cek apakah ada akun yang mempunyai parentnya kode akun ini
            $cek_master_coa = MasterCOA::where('parent', $val->kode)->first();

            if(empty($cek_master_coa->id)){
                $kode_akun[$indeks]['id']   = $val->id;
                $kode_akun[$indeks]['kode'] = $val->kode;
                $kode_akun[$indeks]['nama'] = $val->nama;
                $indeks++;
            }
        }
        
        $pengaturan_jurnal_nota_beli  = PengaturanJurnal::with(['master_coa', 'nama_transaksi' => function($query) 
             {return $query->where('transaksi', 'nota_beli');}])->select('tran_pengaturan_jurnal.*')->join('tref_nama_transaksi', 'tref_nama_transaksi.id', '=', 'tran_pengaturan_jurnal.transaksi_id')->where('tref_nama_transaksi.transaksi', 'nota_beli')->get();

        $pengaturan_jurnal_nota_jual  = PengaturanJurnal::with(['master_coa', 'nama_transaksi' => function($query) 
             {return $query->where('transaksi', 'nota_jual');}])->select('tran_pengaturan_jurnal.*')->join('tref_nama_transaksi', 'tref_nama_transaksi.id', '=', 'tran_pengaturan_jurnal.transaksi_id')->where('tref_nama_transaksi.transaksi', 'nota_jual')->get();

        $pengaturan_jurnal_retur_beli  = PengaturanJurnal::with(['master_coa', 'nama_transaksi' => function($query) 
             {return $query->where('transaksi', 'retur_beli');}])->select('tran_pengaturan_jurnal.*')->join('tref_nama_transaksi', 'tref_nama_transaksi.id', '=', 'tran_pengaturan_jurnal.transaksi_id')->where('tref_nama_transaksi.transaksi', 'retur_beli')->get();

        $pengaturan_jurnal_retur_jual  = PengaturanJurnal::with(['master_coa', 'nama_transaksi' => function($query) 
             {return $query->where('transaksi', 'retur_jual');}])->select('tran_pengaturan_jurnal.*')->join('tref_nama_transaksi', 'tref_nama_transaksi.id', '=', 'tran_pengaturan_jurnal.transaksi_id')->where('tref_nama_transaksi.transaksi', 'retur_jual')->get();

        $pengaturan_jurnal_jasa_servis  = PengaturanJurnal::with(['master_coa', 'nama_transaksi' => function($query) 
             {return $query->where('transaksi', 'jasa_servis');}])->select('tran_pengaturan_jurnal.*')->join('tref_nama_transaksi', 'tref_nama_transaksi.id', '=', 'tran_pengaturan_jurnal.transaksi_id')->where('tref_nama_transaksi.transaksi', 'jasa_servis')->get();

        $pengaturan_jurnal_pembayaran_supplier  = PengaturanJurnal::with(['master_coa', 'nama_transaksi' => function($query) 
             {return $query->where('transaksi', 'pembayaran_supplier');}])->select('tran_pengaturan_jurnal.*')->join('tref_nama_transaksi', 'tref_nama_transaksi.id', '=', 'tran_pengaturan_jurnal.transaksi_id')->where('tref_nama_transaksi.transaksi', 'pembayaran_supplier')->get();

        $pengaturan_jurnal_pembayaran_pelanggan  = PengaturanJurnal::with(['master_coa', 'nama_transaksi' => function($query) 
             {return $query->where('transaksi', 'pembayaran_pelanggan');}])->select('tran_pengaturan_jurnal.*')->join('tref_nama_transaksi', 'tref_nama_transaksi.id', '=', 'tran_pengaturan_jurnal.transaksi_id')->where('tref_nama_transaksi.transaksi', 'pembayaran_pelanggan')->get();

        $pengaturan_jurnal_transaksi_jasa  = PengaturanJurnal::with(['master_coa', 'nama_transaksi' => function($query) 
             {return $query->where('transaksi', 'transaksi_jasa');}])->select('tran_pengaturan_jurnal.*')->join('tref_nama_transaksi', 'tref_nama_transaksi.id', '=', 'tran_pengaturan_jurnal.transaksi_id')->where('tref_nama_transaksi.transaksi', 'transaksi_jasa')->get();

        //return Response::json($kode_akun);
        return view('pages.pengaturan.pengaturan_jurnal', compact('general', 'nota_beli', 'nota_jual', 'retur_beli', 'retur_jual', 'jasa_servis', 'pembayaran_supplier', 'pembayaran_pelanggan', 'kode_akun', 'pengaturan_jurnal_nota_beli', 'pengaturan_jurnal_nota_jual', 'pengaturan_jurnal_retur_beli', 'pengaturan_jurnal_retur_jual', 'pengaturan_jurnal_jasa_servis', 'pengaturan_jurnal_pembayaran_supplier', 'pengaturan_jurnal_pembayaran_pelanggan', 'master_coa', 'pengaturan_jurnal_transaksi_jasa', 'transaksi_jasa'));
    }

    public function tambah_pengaturan_jurnal(Request $request)
    {
        $this->validate($request, [
            'transaksi_id' => 'required',
            'akun_id'  => 'required',
            'debit_or_kredit' => 'required',
        ]);

        $pengaturan_jurnal = new PengaturanJurnal;

        $pengaturan_jurnal->transaksi_id    = Input::get('transaksi_id');
        $pengaturan_jurnal->akun_id         = Input::get('akun_id');
        $pengaturan_jurnal->debit_or_kredit = Input::get('debit_or_kredit');
        $pengaturan_jurnal->save();

        $cek_nama_transaksi = NamaTransaksi::where('id', $pengaturan_jurnal->transaksi_id)->first();

        if($cek_nama_transaksi->transaksi == 'nota_beli')
            $request->session()->flash('message1', 'pengaturan jurnal untuk pembelian berhasil ditambah');
        else if($cek_nama_transaksi->transaksi == 'nota_jual')
            $request->session()->flash('message2', 'pengaturan jurnal untuk penjualan berhasil ditambah');
        else if($cek_nama_transaksi->transaksi == 'retur_beli')
            $request->session()->flash('message3', 'pengaturan jurnal untuk retur beli berhasil ditambah');
        else if($cek_nama_transaksi->transaksi == 'retur_jual')
            $request->session()->flash('message4', 'pengaturan jurnal untuk retur jual berhasil ditambah');
        else if($cek_nama_transaksi->transaksi == 'jasa_servis')
            $request->session()->flash('message5', 'pengaturan jurnal untuk jasa servis berhasil ditambah');
        else if($cek_nama_transaksi->transaksi == 'pembayaran_supplier')
            $request->session()->flash('message6', 'pengaturan jurnal untuk pembayaran supplier berhasil ditambah');
        else if($cek_nama_transaksi->transaksi == 'pembayaran_pelanggan')
            $request->session()->flash('message7', 'pengaturan jurnal untuk pembayaran pelanggan berhasil ditambah');

        return redirect('/pengaturan_jurnal');
    }

    public function ubah_pengaturan_jurnal(Request $request)
    {
        $this->validate($request, [
            'transaksi_id' => 'required',
            'akun_id'  => 'required',
            'debit_or_kredit' => 'required',
        ]);

        $id = Input::get('id');

        $pengaturan_jurnal = PengaturanJurnal::find($id);

        $pengaturan_jurnal->transaksi_id    = Input::get('transaksi_id');
        $pengaturan_jurnal->akun_id         = Input::get('akun_id');
        $pengaturan_jurnal->debit_or_kredit = Input::get('debit_or_kredit');
        $pengaturan_jurnal->save();

        $cek_nama_transaksi = NamaTransaksi::where('id', $pengaturan_jurnal->transaksi_id)->first();

        if($cek_nama_transaksi->transaksi == 'nota_beli')
            $request->session()->flash('message1', 'pengaturan jurnal untuk pembelian berhasil diperbarui');
        else if($cek_nama_transaksi->transaksi == 'nota_jual')
            $request->session()->flash('message2', 'pengaturan jurnal untuk penjualan berhasil diperbarui');
        else if($cek_nama_transaksi->transaksi == 'retur_beli')
            $request->session()->flash('message3', 'pengaturan jurnal untuk retur beli berhasil diperbarui');
        else if($cek_nama_transaksi->transaksi == 'retur_jual')
            $request->session()->flash('message4', 'pengaturan jurnal untuk retur jual berhasil diperbarui');
        else if($cek_nama_transaksi->transaksi == 'jasa_servis')
            $request->session()->flash('message5', 'pengaturan jurnal untuk jasa servis berhasil diperbarui');
        else if($cek_nama_transaksi->transaksi == 'pembayaran_supplier')
            $request->session()->flash('message6', 'pengaturan jurnal untuk pembayaran supplier berhasil diperbarui');
        else if($cek_nama_transaksi->transaksi == 'pembayaran_pelanggan')
            $request->session()->flash('message7', 'pengaturan jurnal untuk pembayaran pelanggan berhasil diperbarui');

        return redirect('/pengaturan_jurnal');
    }

    public function hapus_pengaturan_jurnal(Request $request)
    {
        $id = Input::get('id');
        $pengaturan_jurnal = PengaturanJurnal::find($id);
        $transaksi_id = $pengaturan_jurnal->transaksi_id;

        $pengaturan_jurnal->delete();

        $cek_nama_transaksi = NamaTransaksi::where('id', $transaksi_id)->first();

        if($cek_nama_transaksi->transaksi == 'nota_beli')
            $request->session()->flash('message1', 'pengaturan jurnal untuk pembelian berhasil dihapus');
        else if($cek_nama_transaksi->transaksi == 'nota_jual')
            $request->session()->flash('message2', 'pengaturan jurnal untuk penjualan berhasil dihapus');
        else if($cek_nama_transaksi->transaksi == 'retur_beli')
            $request->session()->flash('message3', 'pengaturan jurnal untuk retur beli berhasil dihapus');
        else if($cek_nama_transaksi->transaksi == 'retur_jual')
            $request->session()->flash('message4', 'pengaturan jurnal untuk retur jual berhasil dihapus');
        else if($cek_nama_transaksi->transaksi == 'jasa_servis')
            $request->session()->flash('message5', 'pengaturan jurnal untuk jasa servis berhasil dihapus');
        else if($cek_nama_transaksi->transaksi == 'pembayaran_supplier')
            $request->session()->flash('message6', 'pengaturan jurnal untuk pembayaran supplier berhasil dihapus');
        else if($cek_nama_transaksi->transaksi == 'pembayaran_pelanggan')
            $request->session()->flash('message7', 'pengaturan jurnal untuk pembayaran pelanggan berhasil dihapus');

        return redirect('/pengaturan_jurnal');
    }
}
