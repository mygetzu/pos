<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Gloudemans\Shoppingcart\Facades\Cart;
use Input;
use Auth;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Session;
use Promocodes;
use Response;
use DateTime;
use PDF;
use App\Models\Stok\MutasiStokHeader;
use App\Models\Stok\MutasiStokDetail;
use App\Models\Gudang\Gudang;
use App\Models\Gudang\ProdukGudang;
use App\Models\Gudang\HadiahGudang;
use App\Models\Gudang\PaketGudang;
use App\Models\Produk\Produk;
use App\Models\Produk\ProdukSerialNumber;

class StokController extends Controller
{
    public function penyesuaian_stok()
    {
        $general['title']       = "Penyesuaian Stok";
        $general['menu1']       = "Stok";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 61; 

        $gudang = DB::select('SELECT tmst_gudang.*, tref_kota.nama AS kota_nama, (SELECT SUM(stok) FROM tran_produk_gudang WHERE gudang_id=tmst_gudang.id) AS jumlah_stok 
            FROM tmst_gudang, tref_kota WHERE tmst_gudang.kota_id=tref_kota.id');

        $penyesuaian_stok = DB::select('SELECT tps.id, p.nama produk_nama, g.nama AS gudang_nama, tps.serial_number, tps.stok_awal, tps.stok_baru, tps.tanggal, 
            (SELECT NAME FROM users WHERE users.id=tps.approval_by_user_id) AS approval_by,
            (SELECT NAME FROM users WHERE users.id=tps.received_by_user_id) AS received_by 
            FROM tran_penyesuaian_stok tps, tmst_produk p, tmst_gudang g WHERE tps.produk_id=p.id AND tps.gudang_id=g.id order by tps.tanggal desc');

        return view('pages.stok.penyesuaian_stok', compact('general', 'gudang', 'penyesuaian_stok'));
    }

    public function penyesuaian_stok_gudang($id)
    {
        $gudang = DB::table('tmst_gudang')->where('id', $id)->first();
        $my_gudang_id = $id;

        $general['title']       = $gudang->nama;
        $general['menu1']       = "Stok";
        $general['menu2']       = "Penyesuaian Stok";
        $general['menu3']       = $general['title'];
        $general['kode_menu']   = 61; 

        $produk_serial_number = DB::table('tran_produk_serial_number')->join('tmst_produk', 'tmst_produk.id', '=', 'tran_produk_serial_number.produk_id')->where('tran_produk_serial_number.gudang_id', $id)->get();

        return view('pages.stok.penyesuaian_stok_gudang', compact('general', 'produk_serial_number', 'my_gudang_id'));
    }

    public function do_penyesuaian_stok(Request $request)
    {
        $gudang_id    = Input::get('gudang_id');
        $produk_sn      = DB::table('tran_produk_serial_number')->where('gudang_id', $gudang_id)->get();

        $i = 2;
        foreach ($produk_sn as $val) {
            $produk_id      = Input::get('produk_id'.$i);
            $serial_number  = Input::get('serial_number'.$i);
            $stok_awal      = Input::get('stok_awal'.$i);
            $stok_ubah      = Input::get('stok_ubah'.$i);
            $stok_baru      = (int)$stok_awal + (int)$stok_ubah;

            if((int)$stok_ubah != 0)
            {
                //update stok di produk_serial_number
                DB::table('tran_produk_serial_number')->where('produk_id', $produk_id)->where('gudang_id', $gudang_id)->where('serial_number', $serial_number)->update(['stok' => $stok_baru]);

                //update stok di produk_gudang
                $stok_produk_gudang = DB::select('SELECT SUM(stok) as jumlah FROM tran_produk_serial_number WHERE gudang_id="'.$gudang_id.'"');

                DB::table('tran_produk_gudang')->where('produk_id', $produk_id)->where('gudang_id', $gudang_id)->update(['stok' => $stok_produk_gudang[0]->jumlah]);

                //update stok di produk
                $stok_produk = DB::select('SELECT SUM(stok) as jumlah FROM tran_produk_serial_number WHERE produk_id="'.$produk_id.'"');

                DB::table('tmst_produk')->where('id', $produk_id)->update(['stok' => $stok_produk[0]->jumlah]);

                //SIMPAN LOG
                date_default_timezone_set("Asia/Jakarta");
                $tanggal                = date("Y-m-d G:i:s");
                $approval_by_user_id    = Auth::user()->id;

                DB::table('tran_penyesuaian_stok')->insert(['gudang_id' => $gudang_id, 'produk_id' => $produk_id, 'serial_number' => $serial_number, 'stok_awal' => $stok_awal, 'stok_ubah' => $stok_ubah, 'stok_baru' => $stok_baru, 'tanggal' => $tanggal, 'approval_by_user_id' => $approval_by_user_id]);
            }

            $i++;
        }

        $request->session()->flash('message1', 'Penyesuaian Stok Telah Dilakukan');
        return redirect('/penyesuaian_stok');
    }

    public function receive_penyesuaian_stok(Request $request)
    {
        $id = Input::get('id');

        DB::table('tran_penyesuaian_stok')->where('id', $id)->update(['received_by_user_id' => Auth::user()->id]);

        $request->session()->flash('message2', 'Penyesuaian Stok Telah Dikonfirmasi');
        return redirect('/penyesuaian_stok');
    }

    public function get_gudang_tujuan()
    {
        $gudang_id_asal = Input::get('gudang_id_asal');

        $gudang = DB::table('tmst_gudang')->where('id', '!=', $gudang_id_asal)->orderBy('nama', 'asc')->get();

        return Response::json($gudang);
    }

    public function do_mutasi_stok(Request $request)
    {
        $this->validate($request, [
            'gudang_id_tujuan' => 'required',
        ]);

        $produk_id          = Input::get('produk_id');
        $gudang_id_asal     = Input::get('gudang_id_asal');
        $serial_number      = Input::get('serial_number');
        $gudang_id_tujuan   = Input::get('gudang_id_tujuan');
        $stok               = Input::get('stok');
        $keterangan         = Input::get('keterangan');

        //kurangi stok di produk_gudang asal
        $produk_gudang = DB::table('tran_produk_gudang')->where('produk_id', $produk_id)->where('gudang_id', $gudang_id_asal)->first();
        $new_stok = (int)$produk_gudang->stok - (int)$stok;
        DB::table('tran_produk_gudang')->where('produk_id', $produk_id)->where('gudang_id', $gudang_id_asal)->update(['stok' => $new_stok]);

        //kurangi stok di produk_serial_number asal
        $produk_serial_number = DB::table('tran_produk_serial_number')->where('serial_number', $serial_number)->where('produk_id', $produk_id)->where('gudang_id', $gudang_id_asal)->first();
        $new_stok = (int)$produk_serial_number->stok - (int)$stok;
        DB::table('tran_produk_serial_number')->where('serial_number', $serial_number)->where('produk_id', $produk_id)->where('gudang_id', $gudang_id_asal)->update(['stok' => $new_stok]);

        //cek jika produk_gudang sudah ada, maka tambah stok
        //jika tidak insert baru
        $cek_pg = DB::table('tran_produk_gudang')->where('produk_id', $produk_id)->where('gudang_id', $gudang_id_tujuan)->first();

        if(empty($cek_pg))
        {
            DB::table('tran_produk_gudang')->insert(['produk_id' => $produk_id, 'gudang_id' => $gudang_id_tujuan, 'stok' => $stok]);
        }
        else
        {
            $new_stok = (int)$cek_pg->stok + (int)$stok;
            DB::table('tran_produk_gudang')->where('produk_id', $produk_id)->where('gudang_id', $gudang_id_tujuan)->update(['stok' => $new_stok]);
        }

        //cek jika serial number sudah ada, maka tambah stok
        //jika tidak insert baru
        $cek_sn = DB::table('tran_produk_serial_number')->where('serial_number', $serial_number)->where('produk_id', $produk_id)->where('gudang_id', $gudang_id_tujuan)->first();

        if(empty($cek_sn))
        {
            DB::table('tran_produk_serial_number')->insert(['produk_id' => $produk_id, 'keterangan' => $keterangan, 'stok' => $stok, 'serial_number' => $serial_number, 'gudang_id' => $gudang_id_tujuan]);
        }
        else
        {
            $new_stok = (int)$cek_sn->stok + (int)$stok;
            DB::table('tran_produk_serial_number')->where('serial_number', $serial_number)->where('produk_id', $produk_id)->where('gudang_id', $gudang_id_tujuan)->update(['stok' => $new_stok]);
        }

        //SIMPAN TRAN MUTASI STOK
        date_default_timezone_set("Asia/Jakarta");
        $tanggal                = date("Y-m-d G:i:s");
        $approval_by_user_id    = Auth::user()->id;

        DB::table('tran_mutasi_stok')->insert(['gudang_id_asal' => $gudang_id_asal, 'gudang_id_tujuan' => $gudang_id_tujuan, 'produk_id' => $produk_id, 'serial_number' => $serial_number, 'jumlah' => $stok, 'tanggal' => $tanggal, 'approval_by_user_id' => $approval_by_user_id]);

        $request->session()->flash('message1', 'Stok Produk berhasil dimutasi');
        return redirect('/mutasi_stok');
    }

    public function receive_mutasi_stok(Request $request)
    {
        $id = Input::get('id');

        DB::table('tran_mutasi_stok')->where('id', $id)->update(['received_by_user_id' => Auth::user()->id]);

        $request->session()->flash('message2', 'Mutasi Stok Telah Dikonfirmasi');
        return redirect('/mutasi_stok');
    }

    public function get_produk_by_gudang_id($id)
    {
        $produk_gudang = DB::table('tran_produk_gudang')->join('tmst_produk', 'tmst_produk.id', '=', 'tran_produk_gudang.produk_id')->where('tran_produk_gudang.gudang_id', $id)->orderBy('tmst_produk.nama', 'asc')->orderBy('tmst_produk.id', 'asc')->get();

        return Response::json(view('pages.stok.ajax_produk_gudang', compact('produk_gudang', 'id'))->render());
    }

    public function get_id_stok_opname()
    {
        $id         = Input::get('id_stok_opname_tmp');

        if($id == '0')
        {
            $temp_stok_opname = DB::table('temp_stok_opname')->orderBy('id', 'desc')->first();

            if(empty($temp_stok_opname))
            {
                $id = 1;
            }
            else
            {
                $id = (int)$temp_stok_opname->id + 1;
            }
        }

        echo $id;
    }

    public function get_detail_stok_opname()
    {
        $id_tmp         = Input::get('id_stok_opname_tmp');
        $produk_id    = Input::get('produk_id');
        
        $temp_stok_opname = DB::table('temp_stok_opname')->where('id', $id_tmp)->where('produk_id', $produk_id)->get();

        return view('pages.stok.ajax_display_detail_stok_opname', compact('temp_stok_opname'));
    }

    public function do_tambah_stok_opname_detail()
    {
        $id_tmp         = Input::get('id_stok_opname_tmp');
        $produk_id      = Input::get('produk_id');
        $gudang_id      = Input::get('gudang_id');
        $serial_number  = Input::get('serial_number');

        //cek apakah serial number sudah ada
        $stok_op = DB::table('temp_stok_opname')->where('id', $id_tmp)->where('produk_id', $produk_id)->where('serial_number', $serial_number)->first();

        if(!empty($stok_op))
        {
            $total = (int)$stok_op->stok;
            $total++;

            DB::table('temp_stok_opname')->where('id', $id_tmp)->where('produk_id', $produk_id)->where('serial_number', $serial_number)->update(['stok' => $total]);
        }
        else
        {
            DB::table('temp_stok_opname')->insert(['id' => $id_tmp, 'produk_id' => $produk_id, 'serial_number' => $serial_number, 'stok' => 1, 'gudang_id' => $gudang_id]);
        }

        //ambil nilai tmp stok opname
        $temp_stok_opname = DB::table('temp_stok_opname')->where('id', $id_tmp)->where('produk_id', $produk_id)->get();

        return view('pages.stok.ajax_display_detail_stok_opname', compact('temp_stok_opname'));
    }

    public function do_tambah_stok_opname(Request $request)
    {
        $gudang_id            = Input::get('gudang_id');
        $id_tmp                 = Input::get('form_id_tmp');
        date_default_timezone_set("Asia/Jakarta");
        //$tanggal                = date("Y-m-d G:i:s");
        $tanggal                = Input::get('tanggal');
        $tanggal                = new DateTime($tanggal);
        $approval_by_user_id    = Auth::user()->id;

        //cek apakah data sudah ada
        $stok_opname_id = DB::table('tran_stok_opname')->insertGetId(['gudang_id' => $gudang_id, 'tanggal' => $tanggal, 'approval_by_user_id' => $approval_by_user_id]);

        //pindah isi tmp ke detail_stok_opname
        $temp_stok_opname    = DB::table('temp_stok_opname')->where('id', $id_tmp)->get();
        $total_stok         = 0;
        foreach ($temp_stok_opname as $val) {
            DB::table('tran_detail_stok_opname')->insert(['stok_opname_id' => $stok_opname_id, 'produk_id' => $val->produk_id, 'serial_number' => $val->serial_number, 'stok' => $val->stok]);

            $total_stok = (int)$total_stok + (int)$val->stok;
        }

        $stok_opname = DB::table('tran_stok_opname')->where('id', $stok_opname_id)->update(['total_stok' => $total_stok]);

        $request->session()->flash('message', 'Stok Opname Telah Ditambah');
        return redirect('/tambah_stok_opname');
        
    }

    public function detail_stok_opname($id)
    {
        if($id == 0)
            return redirect('/stok_opname');
        
        $stok_opname = DB::table('tran_stok_opname')->join('tmst_gudang', 'tmst_gudang.id', '=', 'tran_stok_opname.gudang_id')->join('users', 'users.id', '=', 'tran_stok_opname.approval_by_user_id')->where('tran_stok_opname.id', $id)->first();

        $general['title']       = "Detail Stok Opname Gudang ".$stok_opname->nama;
        $general['menu1']       = "Stok";
        $general['menu2']       = "Stok Opname";
        $general['menu3']       = $general['title'];
        $general['kode_menu']   = 63;
        
        $detail_stok_opname = DB::table('tran_detail_stok_opname')->select('tran_detail_stok_opname.*', 'tmst_produk.nama as produk_nama')->join('tmst_produk', 'tmst_produk.id', '=', 'tran_detail_stok_opname.produk_id')->where('stok_opname_id', $id)->get();

        return view('pages.stok.detail_stok_opname', compact('general', 'stok_opname', 'detail_stok_opname'));
    }

    public function mutasi_stok()
    {
        $general['title']       = "Mutasi Stok";
        $general['menu1']       = "Stok";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 62; 

        $mutasi_stok_header = MutasiStokHeader::with('gudang_asal', 'gudang_tujuan')->get();

        return view('pages.stok.mutasi_stok', compact('general', 'mutasi_stok_header'));
    }

    public function tambah_mutasi_stok()
    {
        $general['title']       = "Tambah Mutasi Stok";
        $general['menu1']       = "Stok";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 62; 

        $gudang = Gudang::where('is_aktif', 1)->get();        

        return view('pages.stok.tambah_mutasi_stok', compact('general', 'gudang'));
    }

    public function mutasi_stok_get_produk()
    {
        $gudang_id = Input::get('gudang_asal');
        //sementara produk dulu
        $produk_gudang = ProdukGudang::with('produk')->where('gudang_id', $gudang_id)->get();

        return view('pages.stok.ajax_mutasi_stok_get_produk', compact('produk_gudang'));
    }

    public function mutasi_stok_get_serial_number()
    {
        $gudang_id      = Input::get('gudang_asal');
        $produk_id      = Input::get('produk_id');
        //sementara produk dulu
        $produk_serial_number = ProdukSerialNumber::where('gudang_id', $gudang_id)->where('produk_id', $produk_id)->where('stok', '>', 0)->get();

        return view('pages.stok.ajax_mutasi_stok_get_serial_number', compact('produk_serial_number'));
    }

    public function do_tambah_mutasi_stok()
    {
        $gudang_asal    = Input::get('gudang_asal');
        $gudang_tujuan  = Input::get('gudang_tujuan');
        $no_mutasi_stok = Input::get('no_mutasi_stok');
        $tanggal        = Input::get('tanggal');
        $total_mutasi   = Input::get('total_mutasi');

        $mutasi_stok_header = new MutasiStokHeader;
        $mutasi_stok_header->no_mutasi_stok = $no_mutasi_stok;
        $mutasi_stok_header->gudang_id_asal    = $gudang_asal;
        $mutasi_stok_header->gudang_id_tujuan  = $gudang_tujuan;
        $mutasi_stok_header->tanggal        = new DateTime($tanggal);
        $mutasi_stok_header->save();

        $id = $mutasi_stok_header->id;

        for ($i=0; $i < (int)$total_mutasi; $i++) { 
            $produk_id      = Input::get('mutasi_produk'.$i);
            $serial_number  = Input::get('mutasi_serial_number'.$i);
            $jumlah         = Input::get('mutasi_jumlah'.$i);

            if($produk_id != "" && $serial_number != "" && $jumlah != ""){
                $mutasi_stok_detail = new MutasiStokDetail;

                $mutasi_stok_detail->mutasi_stok_header_id  = $id;
                $mutasi_stok_detail->no_mutasi_stok         = $no_mutasi_stok;
                $mutasi_stok_detail->produk_id              = $produk_id;
                $mutasi_stok_detail->serial_number          = $serial_number;
                $mutasi_stok_detail->jumlah                 = $jumlah;
                $mutasi_stok_detail->save();
            }
        }

        return redirect('/no_mutasi_stok/'.$id);
    }

    public function no_mutasi_stok($id)
    {
        $mutasi_stok_header = MutasiStokHeader::with('gudang_asal', 'gudang_tujuan')->where('id', $id)->first();
        $mutasi_stok_detail = MutasiStokDetail::where('mutasi_stok_header_id', $id)->get();

        $general['title']       = "Mutasi Stok ".$mutasi_stok_header->no_mutasi_stok;
        $general['menu1']       = "Stok";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 62;

        $indeks     = 0;

        $produk = MutasiStokDetail::with('produk')->where('mutasi_stok_header_id', $id)->groupBy('produk_id')->get();

        foreach ($produk as $value) {
            $serial_number[$indeks] = MutasiStokDetail::with('produk')->where('mutasi_stok_header_id', $id)->where('produk_id', $value->produk_id)->groupBy('serial_number')->get();
            $indeks++;
        }

        return view('pages.stok.no_mutasi_stok', compact('general', 'mutasi_stok_header', 'produk', 'serial_number'));
    }

    public function detail_mutasi_stok($id)
    {
        $mutasi_stok_header = MutasiStokHeader::with('gudang_asal', 'gudang_tujuan')->where('id', $id)->first();
        $mutasi_stok_detail = MutasiStokDetail::where('mutasi_stok_header_id', $id)->get();

        $general['title']       = "Mutasi Stok ".$mutasi_stok_header->no_mutasi_stok;
        $general['menu1']       = "Stok";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 62;

        $indeks     = 0;

        $produk = MutasiStokDetail::with('produk')->where('mutasi_stok_header_id', $id)->groupBy('produk_id')->get();

        foreach ($produk as $value) {
            $serial_number[$indeks] = MutasiStokDetail::with('produk')->where('mutasi_stok_header_id', $id)->where('produk_id', $value->produk_id)->groupBy('serial_number')->get();
            $indeks++;
        }

        return view('pages.stok.detail_mutasi_stok', compact('general', 'mutasi_stok_header', 'produk', 'serial_number'));
    }

    public function sj_mutasi_stok($id)
    {
        $mutasi_stok_header = MutasiStokHeader::with('gudang_asal', 'gudang_tujuan')->where('id', $id)->first();
        $mutasi_stok_detail = MutasiStokDetail::where('mutasi_stok_header_id', $id)->get();

        $mutasi_stok_detail = MutasiStokDetail::with('produk')->where('mutasi_stok_header_id', $id)->groupBy('produk_id')->selectRaw('tran_mutasi_stok_detail.*, sum(jumlah) as jumlah')->get();

        foreach ($mutasi_stok_detail as $key => $value) {
            $produk_sn[$key] = MutasiStokDetail::where('mutasi_stok_header_id', $id)->where('produk_id', $value->produk_id)->get();
        }

        $judul                  = "Surat Jalan Mutasi Stok";
        $general['title']       = "Mutasi Stok ".$mutasi_stok_header->no_mutasi_stok;
        $general['menu1']       = "Stok";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 62;

        date_default_timezone_set("Asia/Jakarta"); 
        $print_time = new DateTime;

        return view('pages.stok.sj_mutasi_stok', compact('general', 'mutasi_stok_header', 'mutasi_stok_detail', 'produk_sn', 'print_time', 'judul'));
    }

    public function sj_mutasi_stok_print($id)
    {
        $mutasi_stok_header = MutasiStokHeader::with('gudang_asal', 'gudang_tujuan')->where('id', $id)->first();
        $mutasi_stok_detail = MutasiStokDetail::where('mutasi_stok_header_id', $id)->get();

        $mutasi_stok_detail = MutasiStokDetail::with('produk')->where('mutasi_stok_header_id', $id)->groupBy('produk_id')->selectRaw('tran_mutasi_stok_detail.*, sum(jumlah) as jumlah')->get();

        foreach ($mutasi_stok_detail as $key => $value) {
            $produk_sn[$key] = MutasiStokDetail::where('mutasi_stok_header_id', $id)->where('produk_id', $value->produk_id)->get();
        }

        date_default_timezone_set("Asia/Jakarta"); 
        $print_time = new DateTime;
        $judul  = "Surat Jalan Mutasi Stok";

        return view('pages.stok.sj_mutasi_stok_print', compact('general', 'mutasi_stok_header', 'mutasi_stok_detail', 'produk_sn', 'print_time', 'judul'));
    }

    public function sj_mutasi_stok_pdf($id)
    {
        $mutasi_stok_header = MutasiStokHeader::with('gudang_asal', 'gudang_tujuan')->where('id', $id)->first();
        $mutasi_stok_detail = MutasiStokDetail::where('mutasi_stok_header_id', $id)->get();

        $mutasi_stok_detail = MutasiStokDetail::with('produk')->where('mutasi_stok_header_id', $id)->groupBy('produk_id')->selectRaw('tran_mutasi_stok_detail.*, sum(jumlah) as jumlah')->get();

        foreach ($mutasi_stok_detail as $key => $value) {
            $produk_sn[$key] = MutasiStokDetail::where('mutasi_stok_header_id', $id)->where('produk_id', $value->produk_id)->get();
        }

        date_default_timezone_set("Asia/Jakarta"); 
        $print_time = new DateTime;
        $judul  = "Surat Jalan Mutasi Stok";

        $pdf = PDF::loadView('pages.stok.sj_mutasi_stok_pdf', compact('general', 'mutasi_stok_header', 'mutasi_stok_detail', 'produk_sn', 'print_time', 'judul'))->setPaper('a4', 'portrait');
        return $pdf->stream();
    }

    public function get_format_no_mutasi()
    {
        $gudang_id = Input::get('gudang_id');

        $gudang = Gudang::where('id', $gudang_id)->first();
        date_default_timezone_set("Asia/Jakarta");
        $tanggal        = new DateTime;

        $format_invoice = $gudang->format_no_mutasi;
        $ori_no_mutasi_stok = $format_invoice;

        $cek_dd = strpos($format_invoice, 'DD');
        if(!empty($cek_dd))
        {
            $format_invoice = str_replace('DD', $tanggal->format('d'), $format_invoice);
        }

        $cek_mm = strpos($format_invoice, 'MM');
        if(!empty($cek_mm))
        {
            $format_invoice = str_replace('MM', $tanggal->format('m'), $format_invoice);
        }

        $cek_yyyy = strpos($format_invoice, 'YYYY');
        if(!empty($cek_yyyy))
        {
            $format_invoice = str_replace('YYYY', $tanggal->format('Y'), $format_invoice);
        }

        $cek_yy = strpos($format_invoice, 'YY');
        if(!empty($cek_yy))
        {
            $format_invoice = str_replace('YY', $tanggal->format('y'), $format_invoice);
        }

        //cek maksimal 10
        $number = '##########';

        for ($i=9; $i > 0; $i--) { 
            $number     = substr($number, 0, $i);
            $cek_number = strpos($format_invoice, $number);
            $nol        = "";
            if(!empty($cek_number))
            {
                $mutasi_stok_header      = MutasiStokHeader::orderBy('id', 'desc')->first();
                if(empty($mutasi_stok_header))
                    $get_num = 1;
                else
                    $get_num        = (int)$mutasi_stok_header->id + 1;

                for ($j=0; $j < $i - strlen($get_num); $j++) { 
                    $nol = $nol."0";
                }

                $get_num = $nol.$get_num;
                $format_invoice = str_replace($number, $get_num, $format_invoice);
                break;
            }
        }

        return $format_invoice;;
    }
}
