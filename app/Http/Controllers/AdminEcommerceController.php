<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\AdminEcommerce\SliderPromo;
use App\Models\AdminEcommerce\MiniBanner;
use App\Models\AdminEcommerce\ProdukPilihan;
use App\Models\AdminEcommerce\PaketPilihan;
use App\Models\AdminEcommerce\Konten;
use App\Models\Produk\Produk;
use App\Models\Produk\KategoriProduk;
use Response;
use Input;
use DateTime;
use Validator;
use File;
use Illuminate\Support\Facades\DB;
use App\Models\Perusahaan;
use Auth;
use App\Models\Pelanggan\Pelanggan;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use App\Models\PaketProduk\Paket;

class AdminEcommerceController extends Controller
{
    public function promo_creator()
    {
        $general['title']       = "Promo Creator";
        $general['menu1']       = "E-Commerce";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 91;

        $obj_slider_promo   = SliderPromo::all();
        $obj_mini_banner    = MiniBanner::take(4)->get();
        $obj_produk_pilihan = ProdukPilihan::all();
        $obj_paket_pilihan  = PaketPilihan::all();

        $slider_promo   = [];   
        $indeks         = 0;
        foreach ($obj_slider_promo as $key => $value) {
            $slider_promo[$indeks]['id']            = $value->id;
            $slider_promo[$indeks]['nama']          = $value->nama;
            $slider_promo[$indeks]['file_gambar']   = $value->file_gambar;
            $slider_promo[$indeks]['item_order']    = $value->item_order;

            $arr_list_produk = $value->list_produk;
            $arr_list_produk = explode(',', $arr_list_produk);

            $list_produk = [];
            for ($j=0; $j < count($arr_list_produk); $j++) { 
                $list_produk[$j] = Produk::where('id', $arr_list_produk[$j])->first();
            }

            $slider_promo[$indeks]['list_produk']   = $list_produk;
            $indeks++;
        }

        $mini_banner   = [];
        $indeks         = 0;
        foreach ($obj_mini_banner as $key => $value) {
            $mini_banner[$indeks]['id']            = $value->id;
            $mini_banner[$indeks]['nama']          = $value->nama;
            $mini_banner[$indeks]['file_gambar']   = $value->file_gambar;
            $indeks++;
        }

        $produk_pilihan   = [];
        $indeks         = 0;
        foreach ($obj_produk_pilihan as $key => $value) {
            $produk_pilihan[$indeks]['id']            = $value->id;
            $produk_pilihan[$indeks]['nama']          = $value->nama;
            $produk_pilihan[$indeks]['file_gambar']   = $value->file_gambar;
            $produk_pilihan[$indeks]['item_order']    = $value->item_order;

            $arr_list_produk = $value->list_produk;
            $arr_list_produk = explode(',', $arr_list_produk);

            $list_produk = [];
            for ($j=0; $j < count($arr_list_produk); $j++) { 
                $list_produk[$j] = Produk::where('id', $arr_list_produk[$j])->first();
            }

            $produk_pilihan[$indeks]['list_produk']   = $list_produk;
            $indeks++;
        }

        $paket_pilihan   = [];
        $indeks         = 0;
        foreach ($obj_paket_pilihan as $key => $value) {
            $paket_pilihan[$indeks]['id']            = $value->id;
            $paket_pilihan[$indeks]['nama']          = $value->nama;
            $paket_pilihan[$indeks]['file_gambar']   = $value->file_gambar;
            $paket_pilihan[$indeks]['item_order']    = $value->item_order;

            $arr_list_paket = $value->list_paket;
            $arr_list_paket = explode(',', $arr_list_paket);

            $list_paket = [];
            for ($j=0; $j < count($arr_list_paket); $j++) { 
                $list_paket[$j] = Paket::where('id', $arr_list_paket[$j])->first();
            }

            $paket_pilihan[$indeks]['list_paket']   = $list_paket;
            $indeks++;
        }

        $all_produk = KategoriProduk::with(['produk' => function($query) {return $query->orderBy('nama', 'asc');}])
            ->orderBy('nama', 'asc')->get();

        $all_paket = Paket::where('is_aktif', 1)->orderBy('nama', 'asc')->get();

        $produk             = DB::table("tmst_produk")->get();
        $produk_baru        = DB::table('tran_produk_baru')->join('tmst_produk', 'tmst_produk.id', '=', 'tran_produk_baru.produk_id')
            ->orderBy('tran_produk_baru.tanggal_input', 'desc')->get();
        $produk_akan_datang = DB::table('tran_produk_akan_datang')->join('tmst_produk', 'tmst_produk.id', '=', 'tran_produk_akan_datang.produk_id')->get();


        return view('pages.ecommerce.promo_creator', compact('general', 'slider_promo', 'mini_banner', 'produk_pilihan', 'konten', 'all_produk', 'produk', 'produk_baru', 'produk_akan_datang', 'paket_pilihan', 'all_paket'));
    }

    public function generate_kode($index)
    {
        $chars = "0123456789abcdefghijklmnopqrstuvwxyz";
        $res = "";
        for ($i = 0; $i < $index; $i++) {
            $res .= $chars[mt_rand(0, strlen($chars)-1)];
        }
        return $res;
    }

    public function tambah_slider_promo(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'file_gambar' => 'required',
        ]);


        $nama               = ucwords(Input::get('nama'));
        $file               = array('file_gambar' => Input::file('file_gambar'));
        $arr_list_produk    = Input::get('list_produk');
        $url                = str_slug($nama);

        $list_produk = "";
        for ($i=0; $i < count($arr_list_produk); $i++) { 
            if($i == 0)
                $list_produk = $arr_list_produk[$i];
            else
                $list_produk = $list_produk.','.$arr_list_produk[$i];
        }

        //cek apakah ada url yg sama
        $cek_nama       = SliderPromo::where('nama', $nama)->get();
        $jml_cek_nama   = count($cek_nama);
        if($jml_cek_nama > 0)
        {
            $url = $url.'-'.$jml_cek_nama;
        }

        if (Input::hasFile('file_gambar'))
        {
            $destinationPath    = base_path().'/public/img/banner/';
            $extension          = Input::file('file_gambar')->getClientOriginalExtension();
            $file_gambar        = "sp-".$this->generate_kode(4)."-".$url;
            $fileName           = $file_gambar.'.'.$extension;
            $slider_promo       = SliderPromo::orderBy('item_order', 'desc')->first();
            if(empty($slider_promo)){
                $item_order     = 1;
            }
            else{
                $item_order         = (int)$slider_promo->item_order+1;
            }

            $slider_promo = new SliderPromo;
            $slider_promo->nama         = $nama;
            $slider_promo->url          = $url;
            $slider_promo->file_gambar  = $fileName;
            $slider_promo->list_produk  = $list_produk;
            $slider_promo->item_order   = $item_order;
            $slider_promo->save();

            Input::file('file_gambar')->move($destinationPath, $fileName);

            $request->session()->flash('message1', 'Slider promo berhasil ditambahkan');
            return redirect('/promo_creator');
        }
    }

    public function tambah_data_produk_baru(Request $request)
    {
        $produk_id    = Input::get("produk_id");
        date_default_timezone_set("Asia/Jakarta");
        $tanggal        = date("Y-m-d G:i:s");

        DB::table('tran_produk_baru')->insert(['produk_id' => $produk_id, 'tanggal_input' => $tanggal]);

        $request->session()->flash('message4', 'Data produk baru berhasil ditambahkan');
        return redirect('/promo_creator');
    }

    public function hapus_data_produk_baru(Request $request)
    {
        $produk_id = Input::get('produk_id_baru');

        DB::table('tran_produk_baru')->where('produk_id', $produk_id)->delete();

        $request->session()->flash('message4', 'Data produk baru berhasil dihapus');
        return redirect('/promo_creator');
    }

    public function tambah_data_produk_akan_datang(Request $request)
    {
        $produk_id    = Input::get("produk_id");
        date_default_timezone_set("Asia/Jakarta");
        $tanggal        = date("Y-m-d G:i:s");

        DB::table('tran_produk_akan_datang')->insert(['produk_id' => $produk_id, 'tanggal_input' => $tanggal]);

        $request->session()->flash('message5', 'Data produk akan datang berhasil ditambahkan');
        return redirect('/promo_creator');
    }

    public function hapus_data_produk_akan_datang(Request $request)
    {
        $produk_id = Input::get('produk_id_akan_datang');

        DB::table('tran_produk_akan_datang')->where('produk_id', $produk_id)->delete();

        $request->session()->flash('message5', 'Data produk akan datang berhasil dihapus');
        return redirect('/promo_creator');
    }

    public function hapus_slider_promo(Request $request)
    {
        $id = Input::get('id');

        $slider_promo = SliderPromo::where('id', $id)->first();
        $slider_promo->delete();

        $request->session()->flash('message1', 'Slider promo berhasil dihapus');
        return redirect('/promo_creator');
    }

    public function konten()
    {
        $general['title']       = "Konten";
        $general['menu1']       = "E-Commerce";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 92;

        /*
            MAPPING KONTEN : 1.nama_toko
        */

        $obj_nama_toko  = Konten::where('nama', 'nama_toko')->first();
        if(empty($obj_nama_toko)){
            $perusahaan = Perusahaan::first();
            $nama_toko  = $perusahaan->nama;
        }
        else{
            $nama_toko  = $obj_nama_toko->keterangan;
        }

        $obj_jam_buka   = Konten::where('nama', 'jam_buka')->first();
        if(empty($obj_nama_toko)){
            $jam_buka = "";
        }
        else{
            $jam_buka  = $obj_jam_buka->keterangan;
        }

        $obj_tentang_kami   = Konten::where('nama', 'tentang_kami')->first();
        if(empty($obj_nama_toko)){
            $tentang_kami = "";
        }
        else{
            $tentang_kami  = $obj_tentang_kami->keterangan;
        }

        $obj_service_center   = Konten::where('nama', 'service_center')->first();
        if(empty($obj_nama_toko)){
            $service_center = "";
        }
        else{
            $service_center  = $obj_service_center->keterangan;
        }

        $obj_cara_belanja   = Konten::where('nama', 'cara_belanja')->first();
        if(empty($obj_cara_belanja)){
            $cara_belanja = "";
        }
        else{
            $cara_belanja  = $obj_cara_belanja->keterangan;
        }

        $obj_cara_pembayaran   = Konten::where('nama', 'cara_pembayaran')->first();
        if(empty($obj_cara_pembayaran)){
            $cara_pembayaran = "";
        }
        else{
            $cara_pembayaran  = $obj_cara_pembayaran->keterangan;
        }

        $obj_retur_barang   = Konten::where('nama', 'retur_barang')->first();
        if(empty($obj_retur_barang)){
            $retur_barang = "";
        }
        else{
            $retur_barang  = $obj_retur_barang->keterangan;
        }

        $obj_sistem_pengiriman   = Konten::where('nama', 'sistem_pengiriman')->first();
        if(empty($obj_sistem_pengiriman)){
            $sistem_pengiriman = "";
        }
        else{
            $sistem_pengiriman  = $obj_sistem_pengiriman->keterangan;
        }

        $obj_bantuan   = Konten::where('nama', 'bantuan')->first();
        if(empty($obj_bantuan)){
            $bantuan = "";
        }
        else{
            $bantuan  = $obj_bantuan->keterangan;
        }

        $obj_ketentuan_pengguna   = Konten::where('nama', 'ketentuan_pengguna')->first();
        if(empty($obj_ketentuan_pengguna)){
            $ketentuan_pengguna = "";
        }
        else{
            $ketentuan_pengguna  = $obj_ketentuan_pengguna->keterangan;
        }

        $obj_faq   = Konten::where('nama', 'faq')->first();
        if(empty($obj_faq)){
            $faq = "";
        }
        else{
            $faq  = $obj_faq->keterangan;
        }

        $obj_gambar_produk_default   = Konten::where('nama', 'gambar_produk_default')->first();
        if(empty($obj_gambar_produk_default)){
            $gambar_produk_default = "";
        }
        else{
            $gambar_produk_default  = $obj_gambar_produk_default->keterangan;
        }

        return view('pages.ecommerce.konten', compact('general', 'nama_toko', 'jam_buka', 'tentang_kami', 'service_center', 'cara_belanja', 'cara_pembayaran', 'retur_barang', 'sistem_pengiriman', 'bantuan', 'ketentuan_pengguna', 'faq', 'gambar_produk_default'));
    }

    public function tentang_toko(Request $request)
    {
        $nama_toko      = Input::get('nama_toko');
        $jam_buka       = Input::get('jam_buka');
        $tentang_kami   = Input::get('tentang_kami');
        $service_center = Input::get('service_center');

        $konten_nama_toko = Konten::where('nama', 'nama_toko')->first();
        if(empty($konten_nama_toko)){
            $konten_nama_toko               = new Konten;
            $konten_nama_toko->nama         = 'nama_toko';
            $konten_nama_toko->keterangan   = $nama_toko;
            $konten_nama_toko->save();
        }
        else{
            $konten_nama_toko->keterangan   = $nama_toko;
            $konten_nama_toko->save();
        }

        $konten_jam_buka = Konten::where('nama', 'jam_buka')->first();
        if(empty($konten_jam_buka)){
            $konten_jam_buka               = new Konten;
            $konten_jam_buka->nama         = 'jam_buka';
            $konten_jam_buka->keterangan   = $jam_buka;
            $konten_jam_buka->save();
        }
        else{
            $konten_jam_buka->keterangan   = $jam_buka;
            $konten_jam_buka->save();
        }

        $konten_tentang_kami = Konten::where('nama', 'tentang_kami')->first();
        if(empty($konten_tentang_kami)){
            $konten_tentang_kami               = new Konten;
            $konten_tentang_kami->nama         = 'tentang_kami';
            $konten_tentang_kami->keterangan   = $tentang_kami;
            $konten_tentang_kami->save();
        }
        else{
            $konten_tentang_kami->keterangan   = $tentang_kami;
            $konten_tentang_kami->save();
        }

        $konten_service_center = Konten::where('nama', 'service_center')->first();
        if(empty($konten_service_center)){
            $konten_service_center               = new Konten;
            $konten_service_center->nama         = 'service_center';
            $konten_service_center->keterangan   = $service_center;
            $konten_service_center->save();
        }
        else{
            $konten_service_center->keterangan   = $service_center;
            $konten_service_center->save();
        }

        $request->session()->flash('message1', 'Konten tentang toko berhasil disimpan');
        return redirect('/konten');
    }

    public function tata_cara(Request $request)
    {
        $cara_belanja       = Input::get('cara_belanja');
        $cara_pembayaran    = Input::get('cara_pembayaran');
        $retur_barang       = Input::get('retur_barang');
        $sistem_pengiriman  = Input::get('sistem_pengiriman');

        $konten_cara_belanja = Konten::where('nama', 'cara_belanja')->first();
        if(empty($konten_cara_belanja)){
            $konten_cara_belanja               = new Konten;
            $konten_cara_belanja->nama         = 'cara_belanja';
            $konten_cara_belanja->keterangan   = $cara_belanja;
            $konten_cara_belanja->save();
        }
        else{
            $konten_cara_belanja->keterangan   = $cara_belanja;
            $konten_cara_belanja->save();
        }

        $konten_cara_pembayaran = Konten::where('nama', 'cara_pembayaran')->first();
        if(empty($konten_cara_pembayaran)){
            $konten_cara_pembayaran               = new Konten;
            $konten_cara_pembayaran->nama         = 'cara_pembayaran';
            $konten_cara_pembayaran->keterangan   = $cara_pembayaran;
            $konten_cara_pembayaran->save();
        }
        else{
            $konten_cara_pembayaran->keterangan   = $cara_pembayaran;
            $konten_cara_pembayaran->save();
        }

        $konten_retur_barang = Konten::where('nama', 'retur_barang')->first();
        if(empty($konten_retur_barang)){
            $konten_retur_barang               = new Konten;
            $konten_retur_barang->nama         = 'retur_barang';
            $konten_retur_barang->keterangan   = $retur_barang;
            $konten_retur_barang->save();
        }
        else{
            $konten_retur_barang->keterangan   = $retur_barang;
            $konten_retur_barang->save();
        }

        $konten_sistem_pengiriman = Konten::where('nama', 'sistem_pengiriman')->first();
        if(empty($konten_sistem_pengiriman)){
            $konten_sistem_pengiriman               = new Konten;
            $konten_sistem_pengiriman->nama         = 'sistem_pengiriman';
            $konten_sistem_pengiriman->keterangan   = $sistem_pengiriman;
            $konten_sistem_pengiriman->save();
        }
        else{
            $konten_sistem_pengiriman->keterangan   = $sistem_pengiriman;
            $konten_sistem_pengiriman->save();
        }

        $request->session()->flash('message2', 'Konten tata cara berhasil disimpan');
        return redirect('/konten');
    }

    public function form_bantuan(Request $request)
    {
        $bantuan            = Input::get('bantuan');
        $ketentuan_pengguna = Input::get('ketentuan_pengguna');
        $faq                = Input::get('faq');

        $konten_bantuan = Konten::where('nama', 'bantuan')->first();
        if(empty($konten_bantuan)){
            $konten_bantuan               = new Konten;
            $konten_bantuan->nama         = 'bantuan';
            $konten_bantuan->keterangan   = $bantuan;
            $konten_bantuan->save();
        }
        else{
            $konten_bantuan->keterangan   = $bantuan;
            $konten_bantuan->save();
        }

        $konten_ketentuan_pengguna = Konten::where('nama', 'ketentuan_pengguna')->first();
        if(empty($konten_ketentuan_pengguna)){
            $konten_ketentuan_pengguna               = new Konten;
            $konten_ketentuan_pengguna->nama         = 'ketentuan_pengguna';
            $konten_ketentuan_pengguna->keterangan   = $ketentuan_pengguna;
            $konten_ketentuan_pengguna->save();
        }
        else{
            $konten_ketentuan_pengguna->keterangan   = $ketentuan_pengguna;
            $konten_ketentuan_pengguna->save();
        }

        $konten_faq = Konten::where('nama', 'faq')->first();
        if(empty($konten_faq)){
            $konten_faq               = new Konten;
            $konten_faq->nama         = 'faq';
            $konten_faq->keterangan   = $faq;
            $konten_faq->save();
        }
        else{
            $konten_faq->keterangan   = $faq;
            $konten_faq->save();
        }

        $request->session()->flash('message3', 'Konten bantuan berhasil disimpan');
        return redirect('/konten');
    }

    public function set_order_slider_promo()
    {
        
    }

    public function slider_promo($url, Request $request)
    {
        $slider_promo   = SliderPromo::where('url', $url)->first();
        $list_produk    = $slider_promo->list_produk;
        $list_produk    = explode(',', $list_produk);

        $kategori_pelanggan_id  = 0;

        $produk = [];
        for($i=0; $i<count($list_produk); $i++) {
            $range_harga    = Input::get('range_harga');
            $range_min      = "10000";
            $range_max      = "30000000";
            $sorting        = Input::get('sorting');

            if(Auth::check()){
                $pelanggan              = Pelanggan::with('kategori_pelanggan')->where('user_id', Auth::user()->id)->first();
                if(!empty($pelanggan))
                    $kategori_pelanggan_id  = $pelanggan->kategori_id;
            }

            if(!empty($range_harga)){
                $range_harga    = explode(';', $range_harga);
                $range_min      = $range_harga[0];
                $range_max      = $range_harga[1];

                $produk_obj = Produk::with(['produk_galeri', 'promo_diskon', 'promo_cashback', 'promo_hadiah' => function($query) 
                {return $query->with('hadiah');}, 'produk_harga' 
                => function ($query) use ($kategori_pelanggan_id) 
                {$query->where('kategori_pelanggan_id', $kategori_pelanggan_id); }])
                ->where('is_aktif', 1)->where('id', $list_produk[$i])->where('tmst_produk.harga_retail', '>=', (int)$range_min)->where('tmst_produk.harga_retail', '<=', (int)$range_max)->first(); 
            }
            else{
                $produk_obj = Produk::with(['produk_galeri', 'promo_diskon', 'promo_cashback', 'promo_hadiah' => function($query) 
                {return $query->with('hadiah');}, 'produk_harga' 
                => function ($query) use ($kategori_pelanggan_id) 
                {$query->where('kategori_pelanggan_id', $kategori_pelanggan_id); }])
                ->where('is_aktif', 1)->where('id', $list_produk[$i])->first();
            }
            
            if(empty($produk_obj)){
                continue;
            }

            $produk[$i]['slug_nama']    = $produk_obj->slug_nama;
            $produk[$i]['nama']         = $produk_obj->nama;
            $produk[$i]['harga_retail'] = $produk_obj->harga_retail;

            if(!empty($produk_obj->produk_galeri_first->file_gambar)){
                $produk[$i]['file_gambar'] = $produk_obj->produk_galeri_first->file_gambar;
            }
            else{
                $produk[$i]['file_gambar'] = "";
            }

            $produk[$i]['diskon']             = 0; 
            $produk[$i]['qty_beli_diskon']    = 1;
            $produk[$i]['cashback']           = 0; 
            $produk[$i]['qty_beli_cashback']  = 1;
            $produk[$i]['hadiah']             = "";
            $produk[$i]['qty_beli_hadiah']    = 1;
            $produk[$i]['qty_hadiah']         = 1;
            $produk[$i]['stok_tersedia']      = $produk_obj->stok - $produk_obj->stok_dipesan;

            if(!empty($pelanggan->kategori_pelanggan)){
                if($pelanggan->kategori_pelanggan->default_diskon > $produk[$i]['diskon']){
                    $produk[$i]['diskon'] = $pelanggan->kategori_pelanggan->default_diskon;
                }
            }
            
            if(!empty($produk_obj->produk_harga)){
                if($produk_obj->produk_harga->diskon > $produk[$i]['diskon']){
                    $produk[$i]['diskon'] = $produk_obj->produk_harga->diskon;
                }
            }
            
            if(!empty($produk_obj->promo_diskon)){
                if($produk_obj->promo_diskon->diskon > $produk[$i]['diskon']){
                    $produk[$i]['diskon'] = $produk_obj->promo_diskon->diskon;
                    $produk[$i]['qty_beli_diskon'] = $produk_obj->promo_diskon->qty_beli;
                }
            }

            if(!empty($produk_obj->promo_cashback) && empty($produk_obj->produk_harga) && empty($pelanggan->kategori_pelanggan))
            {
                $produk[$i]['cashback']           = $produk_obj->promo_cashback->cashback;
                $produk[$i]['qty_beli_cashback']  = $produk_obj->promo_cashback->qty_beli;
            }
            
            if(!empty($produk_obj->promo_hadiah))
            {
                $produk[$i]['hadiah']             = $produk_obj->promo_hadiah->hadiah->nama;
                $produk[$i]['qty_beli_hadiah']    = $produk_obj->promo_hadiah->qty_beli;
                $produk[$i]['qty_hadiah']         = $produk_obj->promo_hadiah->qty_hadiah;
            }

            $produk[$i]['harga_akhir'] = $produk_obj->harga_retail;

            if($produk[$i]['qty_beli_diskon'] == 1){
                $produk[$i]['harga_akhir'] = $produk[$i]['harga_akhir'] - ($produk[$i]['harga_akhir']*$produk[$i]['diskon']/100);
            }

            if($produk[$i]['qty_beli_cashback'] == 1){
                $produk[$i]['harga_akhir'] = $produk[$i]['harga_akhir'] - $produk[$i]['cashback'];
            }
        }

        if($sorting == 2){
            $nama = array();
            foreach ($produk as $key => $row)
            {
                $nama[$key] = $row['slug_nama'];
            }

            array_multisort($nama, SORT_DESC, SORT_STRING, $produk);
        }
        elseif($sorting == 3){
            $harga = array();
            foreach ($produk as $key => $row)
            {
                $harga[$key] = $row['harga_akhir'];
            }

            array_multisort($harga, SORT_ASC, $produk);
        }
        elseif($sorting == 4){
            $harga = array();
            foreach ($produk as $key => $row)
            {
                $harga[$key] = $row['harga_akhir'];
            }

            array_multisort($harga, SORT_DESC, $produk);
        }
        else{
            $nama = array();
            foreach ($produk as $key => $row)
            {
                $nama[$key] = $row['slug_nama'];
            }

            array_multisort($nama, SORT_ASC, SORT_STRING, $produk);
        }

        $currentPage                = LengthAwarePaginator::resolveCurrentPage();
        $col                        = new Collection($produk);
        $perPage                    = 15;
        $currentPageSearchResults   = $col->slice(($currentPage - 1) * $perPage, $perPage)->all();
        $produk                     = new LengthAwarePaginator($currentPageSearchResults, count($col), $perPage);
        $url = $request->fullUrl();
        $produk->setPath($url);
        
        return view('pages.home.list_produk', compact('produk', 'slug_nama', 'kategori', 'range_min', 'range_max', 'sorting', 'cek_kategori'));
    }

    public function simpan_mini_banner(Request $request)
    {
        $id             = Input::get('id');
        $nama           = ucwords(Input::get('nama'));
        $file_gambar    = array('file_gambar' => Input::file('file_gambar'));

        if(empty($id)){
            $mini_banner = new MiniBanner;
        }
        else{
            $mini_banner = MiniBanner::where('id', $id)->first();
            if(empty($mini_banner)){
                $mini_banner = new MiniBanner;
            }
        }

        $mini_banner->nama = $nama;

        $url                = str_slug($nama);

        //cek apakah ada url yg sama
        $cek_nama       = MiniBanner::where('nama', $nama)->get();
        $jml_cek_nama   = count($cek_nama);
        if($jml_cek_nama > 0)
        {
            $url = $url.'-'.$jml_cek_nama;
        }

        $mini_banner->url = $url;

        if (Input::hasFile('file_gambar'))
        {
            $destinationPath    = base_path().'/public/img/banner/';
            $extension          = Input::file('file_gambar')->getClientOriginalExtension();
            $file_gambar        = "mb-".$this->generate_kode(4)."-".$url;
            $fileName           = $file_gambar.'.'.$extension;

            $mini_banner->file_gambar = $fileName;

            Input::file('file_gambar')->move($destinationPath, $fileName);
        }

        $mini_banner->save();

        $request->session()->flash('message2', 'mini banner berhasil diperbarui');
        return redirect('/promo_creator');
    }

    public function hapus_mini_banner(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $id = Input::get('id');

        $mini_banner = MiniBanner::where('id', $id)->first();

        //hapus gambar
        File::Delete(base_path().'/public/img/produk/'.$mini_banner->file_gambar);

        $mini_banner->delete();

        $request->session()->flash('message2', 'mini banner berhasil dihapus');
        return redirect('/promo_creator');
    }

    public function tambah_produk_pilihan(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'file_gambar' => 'required',
        ]);


        $nama               = ucwords(Input::get('nama'));
        $file               = array('file_gambar' => Input::file('file_gambar'));
        $arr_list_produk    = Input::get('list_produk');
        $url                = str_slug($nama);

        $list_produk = "";
        for ($i=0; $i < count($arr_list_produk); $i++) { 
            if($i == 0)
                $list_produk = $arr_list_produk[$i];
            else
                $list_produk = $list_produk.','.$arr_list_produk[$i];
        }

        //cek apakah ada url yg sama
        $cek_nama       = ProdukPilihan::where('nama', $nama)->get();
        $jml_cek_nama   = count($cek_nama);
        if($jml_cek_nama > 0)
        {
            $url = $url.'-'.$jml_cek_nama;
        }

        if (Input::hasFile('file_gambar'))
        {
            $destinationPath    = base_path().'/public/img/banner/';
            $extension          = Input::file('file_gambar')->getClientOriginalExtension();
            $file_gambar        = "pp-".$this->generate_kode(4)."-".$url;
            $fileName           = $file_gambar.'.'.$extension;
            $produk_pilihan       = ProdukPilihan::orderBy('item_order', 'desc')->first();
            if(empty($produk_pilihan)){
                $item_order     = 1;
            }
            else{
                $item_order         = (int)$produk_pilihan->item_order+1;
            }

            $produk_pilihan = new ProdukPilihan;
            $produk_pilihan->nama         = $nama;
            $produk_pilihan->url          = $url;
            $produk_pilihan->file_gambar  = $fileName;
            $produk_pilihan->list_produk  = $list_produk;
            $produk_pilihan->item_order   = $item_order;
            $produk_pilihan->save();

            Input::file('file_gambar')->move($destinationPath, $fileName);

            $request->session()->flash('message3', 'Produk Pilihan berhasil ditambahkan');
        }


        return redirect('/promo_creator');
    }

    public function hapus_produk_pilihan(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $id = Input::get('id');

        $produk_pilihan = ProdukPilihan::where('id', $id)->first();

        //hapus gambar
        File::Delete(base_path().'/public/img/banner/'.$produk_pilihan->file_gambar);

        $produk_pilihan->delete();

        $request->session()->flash('message3', 'Produk pilihan berhasil dihapus');
        return redirect('/promo_creator');
    }

    public function upload_gambar_produk_default(Request $request)
    {
        $file = array('gambar' => Input::file('gambar'));

        if (Input::hasFile('gambar')) 
        {
                $destinationPath    = base_path().'/public/img/produk/';
                $extension          = Input::file('gambar')->getClientOriginalExtension();
                $file_gambar        = "product-images-".$this->generate_kode(4);
                $fileName           = $file_gambar.'.'.$extension;
                
                $gambar_produk_default = Konten::where('nama', 'gambar_produk_default')->first();
                if(empty($gambar_produk_default)){
                    $gambar_produk_default               = new Konten;
                    $gambar_produk_default->nama         = 'gambar_produk_default';
                    $gambar_produk_default->keterangan   = $fileName;
                    $gambar_produk_default->save();
                }
                else{
                    File::Delete(base_path().'/public/produk/'.$gambar_produk_default->keterangan);
                    $gambar_produk_default->keterangan   = $fileName;
                    $gambar_produk_default->save();
                }

                Input::file('gambar')->move($destinationPath, $fileName);

                $request->session()->flash('message4', 'Gambar berhasil ditambahkan');
                return redirect('/konten');
        }
    }

    public function tambah_paket_pilihan(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'file_gambar' => 'required',
        ]);

        $nama               = ucwords(Input::get('nama'));
        $file               = array('file_gambar' => Input::file('file_gambar'));
        $arr_list_paket     = Input::get('list_paket');
        $url                = str_slug($nama);

        $list_paket = "";
        for ($i=0; $i < count($arr_list_paket); $i++) { 
            if($i == 0)
                $list_paket = $arr_list_paket[$i];
            else
                $list_paket = $list_paket.','.$arr_list_paket[$i];
        }

        //cek apakah ada url yg sama
        $cek_nama       = PaketPilihan::where('nama', $nama)->get();
        $jml_cek_nama   = count($cek_nama);
        if($jml_cek_nama > 0)
        {
            $url = $url.'-'.$jml_cek_nama;
        }

        if (Input::hasFile('file_gambar'))
        {
            $destinationPath    = base_path().'/public/img/banner/';
            $extension          = Input::file('file_gambar')->getClientOriginalExtension();
            $file_gambar        = "pktp-".$this->generate_kode(4)."-".$url;
            $fileName           = $file_gambar.'.'.$extension;
            $paket_pilihan       = PaketPilihan::orderBy('item_order', 'desc')->first();
            if(empty($paket_pilihan)){
                $item_order     = 1;
            }
            else{
                $item_order         = (int)$paket_pilihan->item_order+1;
            }

            $paket_pilihan = new PaketPilihan;
            $paket_pilihan->nama         = $nama;
            $paket_pilihan->url          = $url;
            $paket_pilihan->file_gambar  = $fileName;
            $paket_pilihan->list_paket  = $list_paket;
            $paket_pilihan->item_order   = $item_order;
            $paket_pilihan->save();

            Input::file('file_gambar')->move($destinationPath, $fileName);

            $request->session()->flash('message6', 'paket Pilihan berhasil ditambahkan');
        }

        return redirect('/promo_creator');
    }

    public function produk_pilihan($url, Request $request)
    {
        $produk_pilihan   = ProdukPilihan::where('url', $url)->first();
        $list_produk    = $produk_pilihan->list_produk;
        $list_produk    = explode(',', $list_produk);

        $kategori_pelanggan_id  = 0;

        $produk = [];
        for($i=0; $i<count($list_produk); $i++) {
            $range_harga    = Input::get('range_harga');
            $range_min      = "10000";
            $range_max      = "30000000";
            $sorting        = Input::get('sorting');

            if(Auth::check()){
                $pelanggan              = Pelanggan::with('kategori_pelanggan')->where('user_id', Auth::user()->id)->first();
                if(!empty($pelanggan))
                    $kategori_pelanggan_id  = $pelanggan->kategori_id;
            }

            if(!empty($range_harga)){
                $range_harga    = explode(';', $range_harga);
                $range_min      = $range_harga[0];
                $range_max      = $range_harga[1];

                $produk_obj = Produk::with(['produk_galeri', 'promo_diskon', 'promo_cashback', 'promo_hadiah' => function($query) 
                {return $query->with('hadiah');}, 'produk_harga' 
                => function ($query) use ($kategori_pelanggan_id) 
                {$query->where('kategori_pelanggan_id', $kategori_pelanggan_id); }])
                ->where('is_aktif', 1)->where('id', $list_produk[$i])->where('tmst_produk.harga_retail', '>=', (int)$range_min)->where('tmst_produk.harga_retail', '<=', (int)$range_max)->first(); 
            }
            else{
                $produk_obj = Produk::with(['produk_galeri', 'promo_diskon', 'promo_cashback', 'promo_hadiah' => function($query) 
                {return $query->with('hadiah');}, 'produk_harga' 
                => function ($query) use ($kategori_pelanggan_id) 
                {$query->where('kategori_pelanggan_id', $kategori_pelanggan_id); }])
                ->where('is_aktif', 1)->where('id', $list_produk[$i])->first();
            }
            
            if(empty($produk_obj)){
                continue;
            }

            $produk[$i]['slug_nama']    = $produk_obj->slug_nama;
            $produk[$i]['nama']         = $produk_obj->nama;
            $produk[$i]['harga_retail'] = $produk_obj->harga_retail;

            $obj_gambar_produk_default   = Konten::where('nama', 'gambar_produk_default')->first();
            if(empty($obj_gambar_produk_default)){
                $gambar_produk_default = "";
            }
            else{
                $gambar_produk_default  = $obj_gambar_produk_default->keterangan;
            }

            if(!empty($produk_obj->produk_galeri_first->file_gambar)){
                $produk[$i]['file_gambar'] = $produk_obj->produk_galeri_first->file_gambar;
            }
            else{
                $produk[$i]['file_gambar'] = $gambar_produk_default;
            }

            $produk[$i]['diskon']             = 0; 
            $produk[$i]['qty_beli_diskon']    = 1;
            $produk[$i]['cashback']           = 0; 
            $produk[$i]['qty_beli_cashback']  = 1;
            $produk[$i]['hadiah']             = "";
            $produk[$i]['qty_beli_hadiah']    = 1;
            $produk[$i]['qty_hadiah']         = 1;
            $produk[$i]['stok_tersedia']      = $produk_obj->stok - $produk_obj->stok_dipesan;

            if(!empty($pelanggan->kategori_pelanggan)){
                if($pelanggan->kategori_pelanggan->default_diskon > $produk[$i]['diskon']){
                    $produk[$i]['diskon'] = $pelanggan->kategori_pelanggan->default_diskon;
                }
            }
            
            if(!empty($produk_obj->produk_harga)){
                if($produk_obj->produk_harga->diskon > $produk[$i]['diskon']){
                    $produk[$i]['diskon'] = $produk_obj->produk_harga->diskon;
                }
            }
            
            if(!empty($produk_obj->promo_diskon)){
                if($produk_obj->promo_diskon->diskon > $produk[$i]['diskon']){
                    $produk[$i]['diskon'] = $produk_obj->promo_diskon->diskon;
                    $produk[$i]['qty_beli_diskon'] = $produk_obj->promo_diskon->qty_beli;
                }
            }

            if(!empty($produk_obj->promo_cashback) && empty($produk_obj->produk_harga) && empty($pelanggan->kategori_pelanggan))
            {
                $produk[$i]['cashback']           = $produk_obj->promo_cashback->cashback;
                $produk[$i]['qty_beli_cashback']  = $produk_obj->promo_cashback->qty_beli;
            }
            
            if(!empty($produk_obj->promo_hadiah))
            {
                $produk[$i]['hadiah']             = $produk_obj->promo_hadiah->hadiah->nama;
                $produk[$i]['qty_beli_hadiah']    = $produk_obj->promo_hadiah->qty_beli;
                $produk[$i]['qty_hadiah']         = $produk_obj->promo_hadiah->qty_hadiah;
            }

            $produk[$i]['harga_akhir'] = $produk_obj->harga_retail;

            if($produk[$i]['qty_beli_diskon'] == 1){
                $produk[$i]['harga_akhir'] = $produk[$i]['harga_akhir'] - ($produk[$i]['harga_akhir']*$produk[$i]['diskon']/100);
            }

            if($produk[$i]['qty_beli_cashback'] == 1){
                $produk[$i]['harga_akhir'] = $produk[$i]['harga_akhir'] - $produk[$i]['cashback'];
            }
        }

        if($sorting == 2){
            $nama = array();
            foreach ($produk as $key => $row)
            {
                $nama[$key] = $row['slug_nama'];
            }

            array_multisort($nama, SORT_DESC, SORT_STRING, $produk);
        }
        elseif($sorting == 3){
            $harga = array();
            foreach ($produk as $key => $row)
            {
                $harga[$key] = $row['harga_akhir'];
            }

            array_multisort($harga, SORT_ASC, $produk);
        }
        elseif($sorting == 4){
            $harga = array();
            foreach ($produk as $key => $row)
            {
                $harga[$key] = $row['harga_akhir'];
            }

            array_multisort($harga, SORT_DESC, $produk);
        }
        else{
            $nama = array();
            foreach ($produk as $key => $row)
            {
                $nama[$key] = $row['slug_nama'];
            }

            array_multisort($nama, SORT_ASC, SORT_STRING, $produk);
        }

        $currentPage                = LengthAwarePaginator::resolveCurrentPage();
        $col                        = new Collection($produk);
        $perPage                    = 15;
        $currentPageSearchResults   = $col->slice(($currentPage - 1) * $perPage, $perPage)->all();
        $produk                     = new LengthAwarePaginator($currentPageSearchResults, count($col), $perPage);
        $url = $request->fullUrl();
        $produk->setPath($url);
        
        return view('pages.home.list_produk', compact('produk', 'slug_nama', 'kategori', 'range_min', 'range_max', 'sorting', 'cek_kategori'));
    }
}
