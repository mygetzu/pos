<?php

namespace App\Http\Controllers\Service;

use App\Http\Controllers\Controller;
use App\Models\Notifikasi;
use App\Models\Pelanggan\Pelanggan;
use App\Models\Transaksi\ServiceOrder;
use App\Models\TransaksiService\InvoiceDetail;
use App\Models\TransaksiService\InvoiceHeader;
use Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use PHPMailer;

class ServiceController extends Controller
{
    public function data_service(Request $request)
    {
        $get_url = $request::segment(2);

        $general['title'] = "Perbaikan";
        $general['menu2'] = $general['title'];
        $general['kode_menu'] = 11;

        $active['parent'] = 3;
        if ($get_url == 'produk_belum_servis') {
            $general['menu1'] = "Belum Diperbaiki";
            $active['child'] = 1;
            $status = 'Belum';
        } elseif ($get_url == 'produk_sedang_servis') {
            $general['menu1'] = "Sedang Diperbaiki";
            $active['child'] = 2;
            $status = 'Proses';
        } else {
            $general['menu1'] = "Selesai Diperbaiki";
            $active['child'] = 3;
            $status = 'Selesai';
        }

        //Get data
        $data['count'] = ServiceOrder::where('status_service', $status)->count();
        if ($data['count'] == 0) {
            $data[0]['status_service'] = $status;
            $data['servis'] = FALSE;
        } else {
            $data['servis'] = ServiceOrder::where('status_service', $status)->get();
            $i = 0;
            foreach ($data['servis'] as $r_servis) {
                $data[$i]['service_id'] = $r_servis->id;
                $data[$i]['no_nota'] = $r_servis->no_nota;
                $data[$i]['pelanggan'] = DB::table('tmst_pelanggan')->where('id', $r_servis->pelanggan_id)->value('nama');
                $data[$i]['model_produk'] = $r_servis->model_produk;
                $data[$i]['serial_number'] = $r_servis->serial_number;
                $data[$i]['keluhan_pelanggan'] = $r_servis->keluhan_pelanggan;
                $data[$i]['status_service'] = $r_servis->status_service;
                $data[$i]['flag_acc'] = $r_servis->flag_acc;
                $data[$i]['is_check'] = $r_servis->is_check;
                $data[$i]['invoice_header_count'] = InvoiceHeader::where('service_order_id', $r_servis->id)->count();
                $data[$i]['invoice_header'] = InvoiceHeader::where('service_order_id', $r_servis->id)->get();
                foreach ($data[$i]['invoice_header'] as $r_header) {
                    $data[$i]['id'] = $r_header->service_order_id;
                    $data[$i]['pelanggan_id'] = $r_header->pelanggan_id;
                    $data[$i]['teknisi_id'] = $r_header->teknisi_id;
                    $data[$i]['harga_total'] = $r_header->harga_total;
                    $data[$i]['is_submit'] = $r_header->is_submit;
                    $data[$i]['is_lunas'] = $r_header->is_lunas;
                }
                $i++;
            }
        }

        return view('pages.servis.data_servis', compact('general', 'active', 'data'));
    }

    public function servis_detail($id)
    {
        $general['title'] = "Dashboard";
        $general['menu1'] = "Beranda";
        $general['menu2'] = $general['title'];

        $active['parent'] = 3;
        $active['child'] = 2;

        $data['service_id'] = $id;
        $data['invoice_header'] = InvoiceHeader::where('service_order_id', $data['service_id'])->get();
        foreach ($data['invoice_header'] as $key => $value) {
            $data['invoice_header_id'] = $value->id;
            $data['catatan'] = $value->catatan;
        }
        $data['invoice_detail'] = InvoiceDetail::where('service_invoice_header_id', $data['invoice_header_id'])->get();
        $data['servis'] = ServiceOrder::where('id', $data['service_id'])->get();
        $data['link'] = 'service_detail';

        return view('pages.servis.form_servis', compact('data', 'general', 'active'));
    }

    public function servis_proses($id)
    {
        $general['title'] = "Perbaikan";
        $general['menu1'] = "Perbaikan Perangkat";
        $general['menu2'] = $general['title'];
        $general['kode_menu'] = 11;

        $active['parent'] = 3;
        $active['child'] = 2;
        $data['servis'] = ServiceOrder::where('status_service', 'Proses')->where('id', $id)->where('teknisi_id', Auth::user()->id)->get();
        foreach ($data['servis'] as $i => $r_servis) {
            $data['service_id'] = $r_servis->id;
            $data['no_nota'] = $r_servis->no_nota;
            $data['pelanggan'] = DB::table('tmst_pelanggan')->where('id', $r_servis->pelanggan_id)->value('nama');
            $data['status_garansi_produk'] = $r_servis->status_garansi_produk;
            $data['model_produk'] = $r_servis->model_produk;
            $data['serial_number'] = $r_servis->serial_number;
            $data['keluhan_pelanggan'] = $r_servis->keluhan_pelanggan;
            $data['deskripsi_produk'] = $r_servis->deskripsi_produk;
            $data['tanggal_diterima'] = $r_servis->tanggal_diterima;
            $data['tanggal_diperbaiki'] = $r_servis->tanggal_diperbaiki;
            $data['status_service'] = $r_servis->status_service;
            $data['flag_acc'] = $r_servis->flag_acc;
        }

        $data['invoice_header_count'] = InvoiceHeader::where('service_order_id', $id)->count();
        if ($data['invoice_header_count'] == 0) {
            $data['catatan'] = '';
            $data['invoice_header'] = NULL;
            $data['invoice_header_id'] = NULL;
        } else {
            $data['invoice_header'] = InvoiceHeader::where('service_order_id', $id)->get();
            foreach ($data['invoice_header'] as $value) {
                $data['invoice_header_id'] = $value->id;
                $data['is_submit'] = $value->is_submit;
            }

            $data['invoice_detail'] = InvoiceDetail::where('service_invoice_header_id', $data['invoice_header_id'])->get();
            foreach ($data['invoice_detail'] as $i => $value) {
                $data[$i]['tindakan'] = $value->tindakan;
                $data[$i]['biaya'] = $value->biaya;
                $data[$i]['is_repair'] = $value->is_repair;
                $data['count_invoice_detail'] = $i;
            }
            $data['catatan'] = InvoiceHeader::where('service_order_id', $id)->value('catatan');
        }

//        return var_dump($data['servis']);

        $data['count_service_act'] = NULL;
        $data['count_service_done'] = NULL;
        if ($data['status_service'] == 'Proses') {
            if ($data['invoice_header_count'] == 0) {
                //Do Insert
                $data['link'] = 'service_proses';
            } else {
                if ($data['is_submit'] == 0) {
                    //Do Insert
                    $data['link'] = 'service_proses';
                } else {
                    if ($data['flag_acc'] == 0) {
                        //Do Detail
                        $data['link'] = 'service_detail';
                    } else {
                        //Do Update
                        $data['link'] = 'service_action';
                        $invoice_header_id = InvoiceHeader::where('service_order_id', $id)->value('id');
                        $data['count_service_act'] = InvoiceDetail::where('service_invoice_header_id', $invoice_header_id)->count();
                        $data['count_service_done'] = InvoiceDetail::where('service_invoice_header_id', $invoice_header_id)->where('is_repair', 1)->count();
                    }
                }
            }
        } elseif ($data['status_service'] == 'Selesai') {
            //Do Detail
            $data['link'] = 'service_detail';
        }

        /*
         * Set tanggal_diperbaiki
         */
        if($data['tanggal_diperbaiki'] == NULL){
            ServiceOrder::where('id', $id)->update(
                array(
                    'tanggal_diperbaiki' => date('Y-m-d H:i:s')
                )
            );
        }

        return view('pages.servis.form_servis', compact('general', 'active', 'data'));
    }

    //Untuk menghapus & membatalkan pendataan kerusakan
    public function servis_delete()
    {
        $service_delete = Input::get('delete');
        $service_id = Input::get('service_id');
        $invoice_header_id = Input::get('invoice_header_id');

        if ($service_delete == 'all') {
            InvoiceHeader::where('id', $invoice_header_id)->where('service_order_id', $service_id)->delete();
            InvoiceDetail::where('service_invoice_header_id', $invoice_header_id)->delete();
        } else {
            $invoice_detail_id = Input::get('invoice_detail_id');

            InvoiceDetail::where('id', $invoice_detail_id)->where('service_invoice_header_id', $invoice_header_id)->delete();
        }

        return redirect('/teknisi/service_proses/' . $service_id);
    }

    public function servis_checked()
    {
        $service_id = Input::get('service_id');
        $invoice_header_id = Input::get('invoice_header_id');
        $invoice_detail_id = Input::get('invoice_detail_id');
        $submit = Input::get('submit_repair');

        $get_repair_status = InvoiceDetail::where('id', $invoice_detail_id)->value('is_repair');
        if($get_repair_status == 0){
            InvoiceDetail::where('id', $invoice_detail_id)->where('service_invoice_header_id', $invoice_header_id)->update(['is_repair' => 1]);
        } else {
            InvoiceDetail::where('id', $invoice_detail_id)->where('service_invoice_header_id', $invoice_header_id)->update(['is_repair' => 0]);
        }

        $get_all_repair_list = InvoiceDetail::where('service_invoice_header_id', $invoice_header_id)->count();
        $get_done_repair_list = InvoiceDetail::where('service_invoice_header_id', $invoice_header_id)->where('is_repair', 1)->count();
        if($get_all_repair_list == $get_done_repair_list && $submit == 1){
            ServiceOrder::where('id', $service_id)->update(
                array(
                    'tanggal_selesai' => date('Y-m-d H:i:s'),
                    'flag_case' => 1,
                    'status_service' => 'Selesai'
                )
            );
            InvoiceHeader::where('service_order_id', $service_id)->update(
                array(
                    'is_repair' => 1
                )
            );
            $this->sendmail('done', $service_id, $invoice_header_id);
            return redirect('/teknisi/produk_selesai_servis');
        }

        return redirect('/teknisi/service_action/' . $service_id);
    }

    public function servis_submit()
    {
        $id = Input::get('servis_id');
        $pelanggan_id = Input::get('pelanggan_id');
        $teknisi_id = Input::get('teknisi_id');
        $tindakan = Input::get('tindakan');
        $catatan = Input::get('catatan');
        $action = Input::get('action');

        $check_invoice_header = InvoiceHeader::where('service_order_id', $id)->count();
        if ($check_invoice_header == 0) {
            //If doesn't exist, insert new
            $invoice_header = new InvoiceHeader;

            $invoice_header->service_order_id = $id;
            $invoice_header->teknisi_id = $teknisi_id;
            $invoice_header->pelanggan_id = $pelanggan_id;
            $invoice_header->harga_total = 0;
            $invoice_header->catatan = $catatan;
            $invoice_header->is_lunas = 0;
            $invoice_header->is_submit = 0;

            $invoice_header->save();
        } else {
            //If exist, update
            InvoiceHeader::where('service_order_id', $id)->update(
                array(
                    'catatan' => $catatan
                )
            );
        }

        if ($action == 'add') {
            $invoice_detail = new InvoiceDetail;

            $invoice_header_id = InvoiceHeader::where('service_order_id', $id)->value('id');
            $invoice_detail->service_invoice_header_id = $invoice_header_id;
            $invoice_detail->tindakan = $tindakan;
            $invoice_detail->biaya = 0;
            $invoice_detail->is_repair = 0;

            $invoice_detail->save();

            return redirect('/teknisi/service_proses/' . $id);
        } elseif ($action == 'submit') {
            if($tindakan == NULL || $tindakan == '') {
            } else {
                $invoice_detail = new InvoiceDetail;

                $invoice_header_id = InvoiceHeader::where('service_order_id', $id)->value('id');
                $invoice_detail->service_invoice_header_id = $invoice_header_id;
                $invoice_detail->tindakan = $tindakan;
                $invoice_detail->biaya = 0;
                $invoice_detail->is_repair = 0;

                $invoice_detail->save();
            }
            InvoiceHeader::where('service_order_id', $id)->update(
                array(
                    'is_submit' => 1
                )
            );

            $model_produk = ServiceOrder::where('id', $id)->value('model_produk');
            //notifikasi untuk teknisi
            $notifikasi = new Notifikasi;

            $notifikasi->kode                   = 'servis_order';
            $notifikasi->pesan                  = '<i class="fa fa-cog"> Segera isi biaya perbaikan pada '.$model_produk.'</i>';
            $notifikasi->hak_akses_id_target    = 1;
            $notifikasi->link                   = "servis";
            $notifikasi->is_aktif               = 1;
            $notifikasi->save();

            return redirect('/teknisi/service_detail/' . $id);
        }
    }

    public function coba_email(){
        return $this->sendmail('check', 1, NULL);
    }

    public function sendmail($action, $service_id, $invoice_header_id)
    {
        /*
         * Action :
         *  - check
         *  - confirm
         *  - info
         *  - cancel
         *  - done
         */

        $pelanggan_id = ServiceOrder::where('id', $service_id)->value('pelanggan_id');
        $get_pelanggan = Pelanggan::where('id', $pelanggan_id)->first();
        $data['pelanggan_nama'] = $get_pelanggan->nama;
        $data['pelanggan_email'] = $get_pelanggan->email;

        $mail = new PHPMailer;
        $mail->SMTPDebug = 3;
        $mail->isSMTP();
        $mail->Host = "smtp.gmail.com";
        $mail->SMTPAuth = true;
        $mail->Username = "fendi.septiawan0709@gmail.com";
        $mail->Password = "vkizffivxocftlfd";
        $mail->SMTPSecure = "tls";
        $mail->Port = 587;

        $mail->From = "admin@galerindoteknologi.com";
        $mail->FromName = "Admin Galerindo Teknologi";

        $mail->addAddress($data['pelanggan_email'], $data['pelanggan_nama']);

        $mail->isHTML(true);

        $get_service = ServiceOrder::where('id', $service_id)->first();
        $count_invoice = InvoiceHeader::where('id', $invoice_header_id)->count();
        if ($count_invoice == 0) {
            //Data tindakan tidak ada
        } else {
            $get_invoice = InvoiceHeader::where('id', $invoice_header_id)->first();
            $get_invoice_detail = InvoiceDetail::where('service_invoice_header_id', $get_invoice->id)->get();
        }

        if ($action == 'check') {
            $subject = "Galerindo Teknologi - Notifikasi Perbaikan Perangkat";
            $link = url('/service_confirm?') . http_build_query(array(
                    'action' => 'check',
                    'nota' => $get_service->no_nota,
                    'verification_code' => $get_service->kode_verifikasi
                ));
            $body = "
            <html>
                <head>
                    <meta charset=\"utf-8\" />
                    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\">
                    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\">
                    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css\">
                    <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>
                    <title>Galerindo Teknologi - Notifikasi Perbaikan Perangkat</title>
                </head>
                <body>
                    <h3>Galerindo Teknologi - Notifikasi Perbaikan Perangkat</h3>
                    <p>Perangkat Anda saat ini sedang kami lakukan pengecekan untuk tindakan lebih lanjut.</p>
                    <table>
                        <tr>
                            <td>No. Nota</td>
                            <td>". $get_service->no_nota ."</td>
                        </tr>
                        <tr>
                            <td>Tanggal</td>
                            <td>". date('d-M-Y', strtotime($get_service->tanggal_diterima)) ."</td>
                        </tr>
                        <tr>
                            <td>Model Perangkat</td>
                            <td>". $get_service->model_produk ."</td>
                        </tr>
                        <tr>
                            <td>No. Serial Produk</td>
                            <td>". $get_service->serial_number ."</td>
                        </tr>
                        <tr>
                            <td>Keluhan Pelanggan</td>
                            <td>". $get_service->keluhan_pelanggan ."</td>
                        </tr>
                    </table>
                    <p style='font-size: 18px'>Anda dapat memantau perbaikan perangkat Anda melalui tautan berikut,<br/><a href='" . $link . "' target='_blank'>" . $link . "</a></p>
                </body>
            </html>
            ";
            $mail->AltBody = nl2br(strip_tags($body));
        } elseif ($action == 'confirm') {
            $subject = "Galerindo Teknologi - Konfirmasi Perbaikan Perangkat";
            $link = url('/service_confirm?') . http_build_query(array(
                    'action' => 'check',
                    'nota' => $get_service->no_nota,
                    'verification_code' => $get_service->kode_verifikasi
                ));
            $body = "
            <html>
                <head>
                    <meta charset=\"utf-8\" />
                    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\">
                    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\">
                    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css\">
                    <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>
                    <title>Galerindo Teknologi - Konfirmasi Perbaikan Perangkat</title>
                </head>
                <body>
                    <h3>Galerindo Teknologi - Konfirmasi Perbaikan Perangkat</h3>
                    <p>Perangkat Anda saat ini telah kami lakukan pengecekan.<br/>Berikut data perangkat Anda serta kerusakannya.</p>
                    <div class='col-md-4'>
                        <p>No. Nota</p>
                        <p>Tanggal</p>
                        <p>Model Perangkat</p>
                        <p>No. Serial Produk</p>
                        <p>Keluhan Pelanggan</p>
                    </div>
                    <div class='col-md-4'>
                        <p>" . $get_service->no_nota . "</p>
                        <p>" . $get_service->tanggal_diterima . "</p>
                        <p>" . $get_service->model_produk . "</p>
                        <p>" . $get_service->serial_number . "</p>
                        <p>" . $get_service->keluhan_pelanggan . "</p>
                    </div>
                    <table class='table table-stripped'>
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Tindakan</th>
                                <th>Biaya</th>
                            </tr>
                        </thead>
                        <tbody>";
            foreach ($get_invoice_detail as $key => $value) {
                $body = "       <tr>
                                <td>" . ++$key . "</td>
                                <td>" . $value->tindakan . "</td>
                                <td>" . $value->biaya . "</td>
                            </tr>";
            }
            $body .= "
                            <tr style='font-size: 18px'>
                                <td colspan='2' style='text-align: right'>Biaya Total</td>
                                <td>Rp. " . number_format($get_invoice->harga_total, 0, ',', '.') . "</td>
                            </tr>
                        </tbody>
                    </table>
                    <p style='font-size: 20px'>Anda dapat memberikan konfirmasi (Ya / Tidak) perbaikan perangkat Anda melalui tautan berikut, <a href='" . $link . "' target='_blank'>" . $link . "</a></p>
                </body>
            </html>";
        } elseif ($action == 'info') {
            $subject = "Galerindo Teknologi - Informasi Perbaikan Perangkat";
            $link = url('/service_confirm?') . http_build_query(array(
                    'action' => 'info',
                    'nota' => $get_service->no_nota,
                    'verification_code' => $get_service->kode_verifikasi
                ));
            $body = "
            <html>
                <head>
                    <meta charset=\"utf-8\" />
                    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\">
                    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\">
                    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css\">
                    <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>
                    <title>Galerindo Teknologi - Informasi Perbaikan Perangkat</title>
                </head>
                <body>
                    <h3>Galerindo Teknologi - Informasi Perbaikan Perangkat</h3>
                    <p>Perangkat Anda saat ini telah kami lakukan pengecekan.<br/>Berikut data perangkat Anda serta kerusakannya.</p>
                    <div class='col-md-4'>
                        <p>No. Nota</p>
                        <p>Tanggal</p>
                        <p>Model Perangkat</p>
                        <p>No. Serial Produk</p>
                        <p>Keluhan Pelanggan</p>
                    </div>
                    <div class='col-md-4'>
                        <p>" . $get_service->no_nota . "</p>
                        <p>" . $get_service->tanggal_diterima . "</p>
                        <p>" . $get_service->model_produk . "</p>
                        <p>" . $get_service->serial_number . "</p>
                        <p>" . $get_service->keluhan_pelanggan . "</p>
                    </div>
                    <table>
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Tindakan</th>
                                <th>Biaya</th>
                            </tr>
                        </thead>
                        <tbody>";
            foreach ($get_invoice_detail as $key => $value) {
                $body = "       <tr>
                                <td>" . ++$key . "</td>
                                <td>" . $value->tindakan . "</td>
                                <td>" . $value->biaya . "</td>
                            </tr>";
            }
            $body .= "
                            <tr style='font-size: 18px'>
                                <td colspan='2' style='text-align: right'>Biaya Total</td>
                                <td>Rp. " . number_format($get_invoice->harga_total, 0, ',', '.') . "</td>
                            </tr>
                        </tbody>
                    </table>
                    <p style='font-size: 20px'>Anda dapat memberikan konfirmasi (Ya / Tidak) perbaikan perangkat Anda melalui tautan berikut, <a href='" . $link . "' target='_blank'>" . $link . "</a></p>
                </body>
            </html>";
        } elseif ($action == 'cancel') {
            $subject = "Galerindo Teknologi - Pembatalan Perbaikan Perangkat";
            $body = "
            <html>
                <head>
                    <meta charset=\"utf-8\" />
                    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\">
                    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\">
                    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css\">
                    <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>
                    <title>Galerindo Teknologi - Pembatalan Perbaikan Perangkat</title>
                </head>
                <body>
                    <h3>Galerindo Teknologi - Pembatalan Perbaikan Perangkat</h3>
                    <p>Perbaikan perangkat Anda telah kami batalkan sesuai permintaan Anda.</p>
                    <p>Anda dapat mengambil perangkat Anda pada Galerindo Teknologi sesuai jam kerja.</p>
                </body>
            </html>";
        } elseif ($action == 'done') {
            $subject = "Galerindo Teknologi - Informasi Perbaikan Perangkat Selesai";
            $body = "
            <html>
                <head>
                    <meta charset=\"utf-8\" />
                    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\">
                    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\">
                    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css\">
                    <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>
                    <title>Galerindo Teknologi - Informasi Perbaikan Perangkat Selesai</title>
                </head>
                <body>
                    <h3>Galerindo Teknologi - Informasi Perbaikan Perangkat Selesai</h3>
                    <p>Perbaikan perangkat Anda telah kami kerjakan.</p>
                    <p>Anda dapat mengambil perangkat Anda pada Galerindo Teknologi sesuai jam kerja.</p>
                </body>
            </html>";
        }
        $mail->Subject = $subject;
        $mail->Body = $body;

        $mail->send();
    }
}
