<?php

namespace App\Http\Controllers\Produk;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Produk\Produk;
use App\Produk\ProdukGaleri;
use App\Produk\ProdukPromo;
use App\Produk\ProdukKategori;
use Illuminate\Support\Facades\DB;
use Gloudemans\Shoppingcart\Facades\Cart;
use Input;
use App\Http\Controllers\Paginator;
use Auth;
use Illuminate\Pagination\LengthAwarePaginator;
use DateTime;
use Validator;
use Response;
use Hashids\Hashids;
use File;
use Illuminate\Support\Str;

class HadiahController extends Controller
{
    public function hadiah()
    {
        $general['title']       = "Hadiah";
        $general['menu1']       = "Pengaturan Produk";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 45;

        $hadiah = DB::table('tmst_hadiah')->orderBy('nama', 'asc')->get();

        return view('pages.produk.hadiah', compact('general', 'hadiah'));
    }

    public function tambah_hadiah()
    {
        $general['title']       = "Tambah Hadiah";
        $general['menu1']       = "Pengaturan Produk";
        $general['menu2']       = "Hadiah";
        $general['menu3']       = $general['title'];
        $general['kode_menu']   = 45;

        $gudang = DB::table('tmst_gudang')->get();

        return view('pages.produk.tambah_hadiah', compact('general', 'gudang'));
    }

    public function do_tambah_hadiah(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'deskripsi' => 'required',
            'satuan' => 'required',
            'berat' => 'required',
            'gudang' => 'required',
        ]);

        $nama       = ucwords(Input::get('nama'));
        $deskripsi  = Input::get('deskripsi');
        $satuan     = ucwords(Input::get('satuan'));
        $berat      = Input::get('berat');
        $stok       = 0;
        $gudang_id  = Input::get('gudang');

        $file = array('insert_image' => Input::file('insert_image'));
        $fileName = "";
        if (Input::hasFile('insert_image')) {
            $destinationPath = base_path().'/public/img/produk/';
            $extension = Input::file('insert_image')->getClientOriginalExtension();
            $file_gambar = "HDH".$this->random_kode(8);
            $fileName = $file_gambar.'.'.$extension;
            
            Input::file('insert_image')->move($destinationPath, $fileName);
        }

        $id = DB::table('tmst_hadiah')->insertGetId(['nama' => $nama, 'deskripsi' => $deskripsi, 
                'satuan' => $satuan, 'berat' => $berat, 'stok' => $stok, 'file_gambar' => $fileName, 'stok_dipesan' => 0, 'is_aktif' => 1, 'jenis_barang_id' => 2]);

        DB::table('tran_hadiah_gudang')->insert(['hadiah_id' => $id, 'gudang_id' => $gudang_id, 'stok' => $stok]);

        $request->session()->flash('message', 'Hadiah berhasil ditambah');
        return redirect('/hadiah');
    }

    public function random_kode($digit)
    {
        $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $res = "";

        for ($i = 0; $i < $digit; $i++) {
            $res .= $chars[mt_rand(0, strlen($chars)-1)];
        }

        return $res;
    }

    public function detail_hadiah($id)
    {
        $hadiah = DB::table('tmst_hadiah')->where('id', $id)->first();
        $hadiah_gudang = DB::table('tran_hadiah_gudang')->select('tmst_gudang.*', 'tref_kota.nama as kota_nama', 'tran_hadiah_gudang.*')->join('tmst_gudang', 'tmst_gudang.id', '=', 'tran_hadiah_gudang.gudang_id')->join('tref_kota', 'tref_kota.id', '=', 'tmst_gudang.kota_id')->where('tran_hadiah_gudang.hadiah_id', $id)->get();

        $general['title']       = $hadiah->nama;
        $general['menu1']       = "Pengaturan Produk";
        $general['menu2']       = "Hadiah";
        $general['menu3']       = $general['title'];
        $general['kode_menu']   = 45;

        $hadiah_supplier = DB::table('tran_hadiah_supplier')->select('tmst_supplier.*', 'tref_kota.nama as kota_nama')->join('tmst_supplier', 'tmst_supplier.id', '=', 'tran_hadiah_supplier.supplier_id')->join('tref_kota', 'tref_kota.id', '=', 'tmst_supplier.kota_id')->where('hadiah_id', $id)->get();

        $supplier   = DB::table('tmst_supplier')->where('is_aktif', '1')->orderBy('nama', 'asc')->get();

        return view('pages.produk.detail_hadiah', compact('general', 'hadiah', 'hadiah_gudang', 'hadiah_supplier', 'supplier'));
    }

    public function ubah_hadiah(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'deskripsi' => 'required',
            'satuan' => 'required',
            'berat' => 'required',
        ]);

        $id       = Input::get('id');
        $nama       = ucwords(Input::get('nama'));
        $deskripsi  = Input::get('deskripsi');
        $satuan     = ucwords(Input::get('satuan'));
        $berat      = Input::get('berat');
        $stok       = Input::get('stok');

        DB::table('tmst_hadiah')->where('id', $id)->update(['nama' => $nama, 'deskripsi' => $deskripsi, 
            'satuan' => $satuan, 'berat' => $berat, 'stok' => $stok]);

        $request->session()->flash('message', 'Hadiah berhasil diperbarui');
        return redirect('/detail_hadiah/'.$id);
    }

    public function tambah_hadiah_supplier(Request $request)
    {
        $hadiah_id = Input::get('hadiah_id');
        $supplier_id = Input::get('supplier_id');
        DB::table('tran_hadiah_supplier')->insert(['hadiah_id' => $hadiah_id, 'supplier_id' => $supplier_id]);

        $request->session()->flash('message_hadiah_supplier', 'Hadiah Supplier berhasil ditambah');
        return redirect('/detail_hadiah/'.$hadiah_id);
    }

    public function ubah_gambar_hadiah($id, Request $request)
    {
        $kode = Input::get('hadiah_id');
        $file = array('edit_image' => Input::file('edit_image'));
  
        $validator = Validator::make($request->all(), [
            'edit_image' => 'required',
        ]);


        if ($validator->fails()) {
            return redirect('/detail_hadiah/'.$id);
        }
        else {
            if (Input::hasFile('edit_image')) {
                $destinationPath = base_path().'/public/img/produk/';
                $extension = Input::file('edit_image')->getClientOriginalExtension();
                $pg = DB::table('tmst_hadiah')->where('id', $id)->first();
                File::Delete(base_path().'/public/img/produk/'.$pg->file_gambar);
                $file_gambar = "HDH".$this->random_kode(8);
                $fileName = $file_gambar.'.'.$extension;
                
                Input::file('edit_image')->move($destinationPath, $fileName);

                //update hadiah file_gambar
                DB::table('tmst_hadiah')->where('id', $id)->update(['file_gambar' => $fileName]);
                $request->session()->flash('message4', 'Gambar Hadiah berhasil diperbarui');
                return redirect('/detail_hadiah/'.$id);
            }
            else {
              // sending back with error message.
              return redirect('/detail_hadiah/'.$id);
            }
        }
    }

    public function do_tambah_hadiah_by_ajax(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'desk' => 'required',
            'satuan' => 'required',
            'berat' => 'required',
            'gudang' => 'required',
        ]);

        $nama       = ucwords(Input::get('nama'));
        $deskripsi  = Input::get('desk');
        $satuan     = ucwords(Input::get('satuan'));
        $berat      = Input::get('berat');
        $stok       = 0;
        $gudang_id  = Input::get('gudang');

        
        $file = array('insert_image' => Input::file('insert_image'));
        $fileName = "";
        if (Input::hasFile('insert_image')) {
            $destinationPath = base_path().'/public/img/produk/';
            $extension = Input::file('insert_image')->getClientOriginalExtension();
            $file_gambar = "HDH".$this->random_kode(8);
            $fileName = $file_gambar.'.'.$extension;
            
            Input::file('insert_image')->move($destinationPath, $fileName);
        }

        $id = DB::table('tmst_hadiah')->insertGetId(['nama' => $nama, 'deskripsi' => $deskripsi, 
                'satuan' => $satuan, 'berat' => $berat, 'stok' => $stok, 'file_gambar' => $fileName, 'stok_dipesan' => 0]);

        DB::table('tran_hadiah_gudang')->insert(['hadiah_id' => $id, 'gudang_id' => $gudang_id, 'stok' => $stok]);

        echo '<div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  Hadiah berhasil ditambahkan
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>';
    }
}
