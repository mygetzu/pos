<?php

namespace App\Http\Controllers\Produk;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Gloudemans\Shoppingcart\Facades\Cart;
use Input;
use App\Http\Controllers\Paginator;
use Auth;
use Illuminate\Pagination\LengthAwarePaginator;
use DateTime;
use Validator;
use Response;
use Hashids\Hashids;
use File;
use Illuminate\Support\Str;
use App\Models\PaketProduk\Paket;
use App\Models\PaketProduk\PaketProduk;
use App\Models\Produk\Produk;
use App\Models\Produk\KategoriProduk;
use App\Models\AdminEcommerce\Konten;

class PaketProdukController extends Controller
{
    public function paket_produk()
    {
        $general['title']       = "Paket Produk";
        $general['menu1']       = "Pengaturan Produk";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 46;

        $paket = Paket::with(['paket_produk' => function($query) {
            return $query->with('produk');}])->get();
        // return response()->json($paket);

        return view('pages.produk.paket_produk', compact('general', 'paket'));
    }

    public function tambah_paket_produk()
    {
        $general['title']       = "Tambah Paket Produk";
        $general['menu1']       = "Pengaturan Produk";
        $general['menu2']       = "Paket Produk";
        $general['menu3']       = $general['title'];
        $general['kode_menu']   = 46;
        
        $produk             = Produk::with('produk_galeri')->get();
        $kategori_produk    = KategoriProduk::all();
        $obj_gambar_produk_default = Konten::where('nama', 'gambar_produk_default')->first();

        $gambar_produk_default = '';

        return view('pages.produk.tambah_paket_produk', compact('general', 'produk', 'kategori_produk', 'gambar_produk_default'));
    }

    public function list_add_produk($id)
    {
        $produk = DB::table('tmst_produk')->select('tmst_produk.nama as nama_produk', 'tran_produk_galeri.*', 'tmst_produk.*')->join('tran_produk_galeri', 'tran_produk_galeri.produk_id', '=', 'tmst_produk.id')->where('tmst_produk.id', $id)->first();

        return Response::json($produk);
    }

    public function do_tambah_paket_produk(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'harga_total' => 'required',
        ]);

        $kode           = Input::get('kode');
        $nama           = Input::get('nama');
        $jumlah_produk  = Input::get('jumlah_produk');
        $mypaketproduk  = Input::get('mypaketproduk');
        $harga_total    = Input::get('harga_total');
        $harga_total    = str_replace(".", "", $harga_total);

        $slug_nama = str_slug($nama);
        //cek apakah ada nama yg sama
        $cek_nama       = DB::table('tmst_paket')->where('nama', $nama)->get();
        $jml_cek_nama   = count($cek_nama);
        if($jml_cek_nama > 0)
        {
            $slug_nama = $slug_nama.'-'.$jml_cek_nama;
        }

        $harga = "";
        for ($i=0; $i < strlen($harga_total); $i++) { 
            if (is_numeric($harga_total[$i]))
            {
               $harga = $harga.$harga_total[$i];
            }
        }

        $harga_total = (int)$harga;

        $data_produk = explode("-", $mypaketproduk);

        $file = array('image' => Input::file('image'));

        $fileName = "";

        if (Input::hasFile('image')) {
            $destinationPath = base_path().'/public/img/produk/';
            $extension = Input::file('image')->getClientOriginalExtension();
            $file_gambar = "PKT".$this->random_kode(8);
            $fileName = $file_gambar.'.'.$extension;
            
            Input::file('image')->move($destinationPath, $fileName);
        }

        $id = DB::table('tmst_paket')->insertGetId(['kode' => $kode, 'nama' => $nama, 'slug_nama' => $slug_nama, 'harga_total' => $harga_total, 'file_gambar' => $fileName, 'is_aktif' => 1, 'stok' => 0, 'stok_dipesan' => 0, 'jenis_barang_id' => 3]);

        for($i = 0; $i<$jumlah_produk; $i++)
        {
            //cek jika ada kode yang sama maka update
            $cek_produk = DB::table('tran_paket_produk')->where('paket_id', $id)->where('produk_id',  $data_produk[$i])->first();

            if(empty($cek_produk))
            {
                DB::table('tran_paket_produk')->insert(['paket_id' => $id, 'produk_id' => $data_produk[$i], 'qty_produk' => 1]);
            }
            else
            {
                $new_qty = (int)$cek_produk->qty_produk + 1;
                DB::table('tran_paket_produk')->where('paket_id', $id)->where('produk_id',  $data_produk[$i])->update(['qty_produk' => $new_qty]);
            }
        }

        $request->session()->flash('message', 'Paket Produk berhasil ditambah');
        return redirect('/paket_produk');
    }

    public function ubah_status_paket($id, Request $request)
    {

        $data = DB::table('tmst_paket')->where('id', $id)->first();
        if($data->is_aktif == 1)
        {
            $is_aktif = 0;
        }
        else
        {
            $is_aktif = 1;
        }

        DB::table('tmst_paket')->where('id', $id)->update(['is_aktif' => $is_aktif]);

        $request->session()->flash('message', 'Status paket berhasil diperbarui');
        return redirect('/paket_produk');
    }

    public function detail_paket_produk($id)
    {
        $paket = Paket::with(['paket_produk' => function($query) {
            return $query->with(['produk' => function($query){
                return $query->with('produk_galeri');
            }]);
        }])->where('id', $id)->first();
        // return response()->json($paket);
        $general['title']       = $paket->nama;
        $general['menu1']       = "Pengaturan Produk";
        $general['menu2']       = "Paket Produk";
        $general['menu3']       = $general['title'];
        $general['kode_menu']   = 46;

        
        $produk             = Produk::all();
        $kategori_produk    = KategoriProduk::all();
        $gambar_default     = DB::table('static')->where('nama', 'gambar_produk_default')->first();

        return view('pages.produk.detail_paket_produk', compact('general', 'paket', 'produk', 'kategori_produk', 'gambar_default'));
    }

    public function ubah_paket_produk(Request $request)
    {
        $this->validate($request, [
            'kode' => 'required',
            'nama' => 'required',
            'harga_total' => 'required'
        ]);

        $id             = Input::get('id');
        $kode           = Input::get('kode');
        $nama           = Input::get('nama');
        $harga_total    = Input::get('harga_total');
        $harga_total    = str_replace(".", "", $harga_total);

        $slug_nama      = str_slug($nama);
        //cek apakah ada nama yg sama
        $cek_nama       = DB::table('tmst_paket')->where('nama', $nama)->get();
        $jml_cek_nama   = count($cek_nama);
        if($jml_cek_nama > 1)
        {
            $slug_nama = $slug_nama.'-'.$jml_cek_nama;
        }

        DB::table('tmst_paket')->where('id', $id)->update(['kode' => $kode, 'nama' => $nama, 'slug_nama' => $slug_nama, 'harga_total' => $harga_total]);

        $request->session()->flash('message1', 'Paket Produk berhasil diperbarui');
        return redirect('/detail_paket_produk/'.$id);
    }

    public function hapus_paket_produk(Request $request)
    {
        $produk_id = Input::get('produk_id');
        $paket_id  = Input::get('paket_id');

        DB::table('tran_paket_produk')->where('produk_id', $produk_id)->where('paket_id', $paket_id)->delete();

        $request->session()->flash('message', 'Produk berhasil dihapus');
        return redirect('/detail_paket_produk/'.$paket_id);
    }

    public function random_kode($digit)
    {
        $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $res = "";

        for ($i = 0; $i < $digit; $i++) {
            $res .= $chars[mt_rand(0, strlen($chars)-1)];
        }

        return $res;
    }
}
