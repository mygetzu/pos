<?php

namespace App\Http\Controllers\Produk;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Gloudemans\Shoppingcart\Facades\Cart;
use Input;
use App\Http\Controllers\Paginator;
use Auth;
use Illuminate\Pagination\LengthAwarePaginator;
use DateTime;
use Validator;
use Response;
use Hashids\Hashids;
use File;
use Illuminate\Support\Str;
use App\Models\Produk\Produk;
use App\Models\Produk\KategoriProduk;
use App\Models\Produk\ParameterSpesifikasi;
use App\Models\Produk\NilaiSpesifikasi;

class KategoriProdukController extends Controller {

    public function kategori_produk() {
        $general['title'] = "Kategori Produk";
        $general['menu1'] = "Pengaturan Produk";
        $general['menu2'] = $general['title'];
        $general['kode_menu'] = 42;

        $kategori_produk = KategoriProduk::with('parameter_spesifikasi')->orderBy('nama', 'asc')->get();
        return view('pages.produk.kategori_produk', compact('general', 'kategori_produk'));
    }

    public function ubah_status_kategori_produk($id, Request $request) {
        $data = DB::table('tmst_kategori_produk')->where('id', $id)->first();
        if ($data->is_aktif == 1) {
            $is_aktif = 0;
        } else {
            $is_aktif = 1;
        }
        DB::table('tmst_kategori_produk')->where('id', $id)->update(['is_aktif' => $is_aktif]);

        $request->session()->flash('message', 'Status kategori berhasil diperbarui');
        return redirect('/kategori_produk');
    }

    public function tambah_kategori_produk() {
        $general['title'] = "Tambah Kategori Produk";
        $general['menu1'] = "Pengaturan Produk";
        $general['menu2'] = "Kategori Produk";
        $general['menu3'] = $general['title'];
        $general['kode_menu'] = 42;

        return view('pages.produk.tambah_kategori_produk', compact('general'));
    }

    public function do_tambah_kategori_produk(Request $request) {
        $this->validate($request, [
            'nama' => 'required',
            'deskripsi' => 'required',
        ]);

        $nama = ucwords(Input::get('nama'));
        $deskripsi = Input::get('deskripsi');
        $file_gambar = array('file_gambar' => Input::file('file_gambar'));

        $slug_nama = str_slug($nama);
        //cek apakah ada nama yg sama
        $cek_nama = DB::table('tmst_kategori_produk')->where('nama', $nama)->get();
        $jml_cek_nama = count($cek_nama);

        if ($jml_cek_nama > 0) {
            $slug_nama = $slug_nama . '-' . $jml_cek_nama;
        }

        $nama_file_gambar = "";
        if (Input::hasFile('file_gambar')) {
            $destinationPath = base_path() . '/public/img/kategori/';
            $extension = Input::file('file_gambar')->getClientOriginalExtension();
            $file = $slug_nama;
            $nama_file_gambar = $file . '.' . $extension;

            Input::file('file_gambar')->move($destinationPath, $nama_file_gambar);
        }


        $kategori_produk = new KategoriProduk;
        $kategori_produk->nama = $nama;
        $kategori_produk->slug_nama = $slug_nama;
        $kategori_produk->deskripsi = $deskripsi;
        $kategori_produk->is_aktif = 1;
        $kategori_produk->file_gambar = $nama_file_gambar;
        $kategori_produk->save();

        $kategori_produk_id = $kategori_produk->id;

        $jumlah_kolom_spesifikasi = Input::get('jumlah_kolom_spesifikasi');


        for ($i = 0; $i < (int) $jumlah_kolom_spesifikasi; $i++) {
            $ks = ucwords(Input::get('kolom_spesifikasi' . $i));

            if (!empty($ks)) {
                $parameter_spesifikasi = new ParameterSpesifikasi;
                $parameter_spesifikasi->kategori_produk_id = $kategori_produk_id;
                $parameter_spesifikasi->nama = $ks;
                $parameter_spesifikasi->save();
            }
        }

        $ks = ucwords(Input::get('kolom_spesifikasi'));

        if (!empty($ks)) {
            $parameter_spesifikasi = new ParameterSpesifikasi;
            $parameter_spesifikasi->kategori_produk_id = $kategori_produk_id;
            $parameter_spesifikasi->nama = $ks;
            $parameter_spesifikasi->save();
        }

        $request->session()->flash('message', 'Kategori Produk berhasil ditambah');
        return redirect('/kategori_produk');
    }

    public function ubah_kategori_produk(Request $request) {
        $this->validate($request, [
            'nama' => 'required',
            'deskripsi' => 'required',
        ]);

        $id = Input::get('id');
        $nama = ucwords(Input::get('nama'));
        $deskripsi = Input::get('deskripsi');

        $slug_nama = str_slug($nama);
        //cek apakah ada nama yg sama
        $cek_nama = DB::table('tmst_kategori_produk')->where('nama', $nama)->get();
        $jml_cek_nama = count($cek_nama);
        if ($jml_cek_nama > 1) {
            $slug_nama = $slug_nama . '-' . $jml_cek_nama;
        }

        //cek update dari spesifikasi lama
        $parameter_spesifikasi = ParameterSpesifikasi::where('kategori_produk_id', $id)->get();

        foreach ($parameter_spesifikasi as $key => $value) {
            $ks = ucwords(Input::get('old_kolom_spesifikasi' . $value->id));

            if (empty($ks)) {
                //jika kosong maka hapus
                $parameter_spesifikasi = ParameterSpesifikasi::where('id', $value->id)->delete();

                //jika dihapus, maka hapus pula di tabel nilai_spesifikasi
                $nilai_spesifikasi = NilaiSpesifikasi::where('parameter_spesifikasi_id', $value->id)->delete();
            } else {
                $parameter_spesifikasi = ParameterSpesifikasi::where('id', $value->id)->first();
                $parameter_spesifikasi->nama = $ks;
                $parameter_spesifikasi->save();
            }
        }

        $jumlah_kolom_spesifikasi = Input::get('jumlah_kolom_spesifikasi');

        $kolom_spesifikasi = "";
        for ($i = 0; $i < (int) $jumlah_kolom_spesifikasi; $i++) {
            $ks = ucwords(Input::get('new_kolom_spesifikasi' . $i));
            if (!empty($ks)) {
                $parameter_spesifikasi = new ParameterSpesifikasi;
                $parameter_spesifikasi->kategori_produk_id = $id;
                $parameter_spesifikasi->nama = $ks;
                $parameter_spesifikasi->save();
            }
        }

        $ks = ucwords(Input::get('kolom_spesifikasi'));

        $ks = str_replace(',', '', $ks); //gk boleh ada koma

        if (!empty($ks)) {
            $parameter_spesifikasi = new ParameterSpesifikasi;
            $parameter_spesifikasi->kategori_produk_id = $id;
            $parameter_spesifikasi->nama = $ks;
            $parameter_spesifikasi->save();
        }

        $kategori_produk = KategoriProduk::where('id', $id)->first();
        $kategori_produk->nama = $nama;
        $kategori_produk->slug_nama = $slug_nama;
        $kategori_produk->deskripsi = $deskripsi;
        $kategori_produk->save();

        $request->session()->flash('message', 'Kategori Produk berhasil diperbarui');
        return redirect('/kategori_produk');
    }

    public function ubah_icon_kategori_produk(Request $request) {
        $id = Input::get('id');
        $file_gambar = array('file_gambar' => Input::file('file_gambar'));

        $kategori_produk = KategoriProduk::where('id', $id)->first();

        if (empty($kategori_produk->file_gambar)) {
            $nama_file_gambar = "";
            if (Input::hasFile('file_gambar')) {
                $destinationPath = base_path() . '/public/img/kategori/';
                $extension = Input::file('file_gambar')->getClientOriginalExtension();
                $file = $kategori_produk->slug_nama;
                $nama_file_gambar = $file . '.' . $extension;
                Input::file('file_gambar')->move($destinationPath, $nama_file_gambar);
            }

            $kategori_produk->file_gambar = $nama_file_gambar;
            $kategori_produk->save();
        } else {
            //hapus file gambar lama (jika ada)
            File::Delete(base_path() . '/public/img/kategori/' . $kategori_produk->file_gambar);
            Input::file('file_gambar')->move(base_path() . '/public/img/kategori/', $kategori_produk->slug_nama . '.' . Input::file('file_gambar')->getClientOriginalExtension());
        }

        $request->session()->flash('message', 'Icon berhasil diperbarui');
        return redirect('/kategori_produk');
    }

}
