<?php

namespace App\Http\Controllers\Produk;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Gloudemans\Shoppingcart\Facades\Cart;
use Input;
use App\Http\Controllers\Paginator;
use Auth;
use Illuminate\Pagination\LengthAwarePaginator;
use DateTime;
use Validator;
use Response;
use Hashids\Hashids;
use File;
use Illuminate\Support\Str;
use App\Models\Supplier\Supplier;

class SupplierController extends Controller
{
    public function supplier()
    {
        $general['title']       = "Supplier";
        $general['menu1']       = "Pengaturan Produk";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 41;
        
        $supplier = DB::table('tmst_supplier')->select('tmst_supplier.*', 'tref_kota.nama as kota_nama')->join('tref_kota', 'tref_kota.id', '=', 'tmst_supplier.kota_id')->get();

        return view('pages.produk.supplier', compact('general', 'supplier'));
    }

    public function tambah_supplier()
    {
        $general['title']       = "Tambah Supplier";
        $general['menu1']       = "Pengaturan Produk";
        $general['menu2']       = "Supplier";
        $general['menu3']       = $general['title'];
        $general['kode_menu']   = 41;
        
        $provinsi = DB::table('tref_provinsi')->orderBy('nama', 'asc')->get();

        return view('pages.produk.tambah_supplier', compact('general', 'provinsi'));
    }

    public function do_tambah_supplier(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'alamat' => 'required',
            'provinsi' => 'required',
            'kota' => 'required',
        ]);

        $nama           = ucwords(Input::get('nama'));
        $alamat         = ucwords(Input::get('alamat'));
        $kota_id        = Input::get('kota');
        $telp1          = Input::get('telp1');
        $telp2          = Input::get('telp2');
        $nama_sales     = ucwords(Input::get('nama_sales'));
        $hp_sales       = Input::get('hp_sales');
        $deskripsi      = Input::get('deskripsi');
        $umur_hutang    = Input::get('umur_hutang');

        $supplier = new Supplier;
        $supplier->nama         = $nama;
        $supplier->alamat       = $alamat;
        $supplier->kota_id      = $kota_id;
        $supplier->telp1        = $telp1;
        $supplier->telp2        = $telp2;
        $supplier->nama_sales   = $nama_sales;
        $supplier->hp_sales     = $hp_sales;
        $supplier->deskripsi    = $deskripsi;
        $supplier->umur_hutang  = $umur_hutang;
        $supplier->is_aktif     = 1;
        $supplier->save();

        $request->session()->flash('message', 'Supplier berhasil ditambah');
        return redirect('/supplier');
    }

    public function ubah_status_supplier($id, Request $request)
    {
        $data = DB::table('tmst_supplier')->where('id', $id)->first();
        if($data->is_aktif == 1){
            $is_aktif = 0;
        }
        else
        {
            $is_aktif = 1;
        }

        DB::table('tmst_supplier')->where('id', $id)->update(['is_aktif' => $is_aktif]);

        $request->session()->flash('message', 'Status supplier berhasil diperbarui');
        return redirect('/supplier');
    }

    public function detail_supplier($id)
    {
        $supplier = DB::table('tmst_supplier')->where('id', $id)->first();

        $general['title']       = $supplier->nama;
        $general['menu1']       = "Pengaturan Produk";
        $general['menu2']       = "Supplier";
        $general['menu3']       = $general['title'];
        $general['kode_menu']   = 41;

        $rekening = DB::table('tran_rekening_supplier')->where('supplier_id', $id)->get();

        $my_kota = DB::table('tref_kota')->select('tref_kota.nama', 'tref_kota.id as kota_id', 'tref_kota.provinsi_kode', 'tref_provinsi.id as provinsi_id')->join('tref_provinsi', 'tref_provinsi.kode', '=', 'tref_kota.provinsi_kode')->where('tref_kota.id', $supplier->kota_id)->first();
        $provinsi = DB::table('tref_provinsi')->get();

        $produk_supplier = DB::table('tmst_produk')->join('tran_produk_supplier', 'tran_produk_supplier.produk_id', '=', 'tmst_produk.id')->where('tran_produk_supplier.supplier_id', $id)->get();

        $hadiah_supplier = DB::table('tmst_hadiah')->join('tran_hadiah_supplier', 'tran_hadiah_supplier.hadiah_id', '=', 'tmst_hadiah.id')->where('tran_hadiah_supplier.supplier_id', $id)->get();

        $bank = DB::table('tref_bank')->where('is_aktif', 1)->get();

        return view('pages.produk.detail_supplier', compact('general', 'supplier', 'rekening', 'my_kota', 'provinsi', 'produk_supplier', 'hadiah_supplier', 'bank'));
    }

    public function ubah_supplier(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'provinsi' => 'required',
            'kota' => 'required',
            'deskripsi' => 'required',
        ]);

        $id             = Input::get('id');
        $nama           = ucwords(Input::get('nama'));
        $alamat         = ucwords(Input::get('alamat'));
        $kota_id        = Input::get('kota');
        $telp1          = Input::get('telp1');
        $telp2          = Input::get('telp2');
        $nama_sales     = ucwords(Input::get('nama_sales'));
        $hp_sales       = Input::get('hp_sales');
        $deskripsi      = Input::get('deskripsi');
        $umur_hutang    = Input::get('umur_hutang');

        $telp1   = str_replace("_", "", $telp1);
        $telp2   = str_replace("_", "", $telp2);

        DB::table('tmst_supplier')->where('id', $id)->update(['nama' => $nama, 'alamat' => $alamat, 'kota_id' => $kota_id, 'telp1' => $telp1,
             'telp2' => $telp2, 'nama_sales' => $nama_sales, 'hp_sales' => $hp_sales, 'deskripsi' => $deskripsi, 'umur_hutang' => $umur_hutang]);

        $request->session()->flash('message1', 'Supplier berhasil diperbarui');
        return redirect('/detail_supplier/'.$id);
    }

    public function tambah_rekening_supplier(Request $request)
    {
        $this->validate($request, [
                'nama_bank' => 'required',
                'nomor_rekening' => 'required',
                'atas_nama' => 'required',
                'mata_uang' => 'required',
            ]);

        $supplier_id    = Input::get('supplier_id');
        $nama_bank      = strtoupper(Input::get('nama_bank'));
        $nomor_rekening = Input::get('nomor_rekening');
        $atas_nama      = ucwords(Input::get('atas_nama'));
        $mata_uang      = Input::get('mata_uang');

        DB::table('tran_rekening_supplier')->insert(['supplier_id' => $supplier_id, 'nama_bank' => $nama_bank, 'nomor_rekening' => $nomor_rekening, 'atas_nama' => $atas_nama, 'mata_uang' => $mata_uang]);

        $request->session()->flash('message2', 'Rekening berhasil ditambah');
        return redirect('/detail_supplier/'.$supplier_id);
    }

    public function ubah_rekening_supplier(Request $request)
    {
        $this->validate($request, [
                'nama_bank' => 'required',
                'nomor_rekening' => 'required',
                'atas_nama' => 'required',
                'mata_uang' => 'required',
            ]);

        $supplier_id    = Input::get('supplier_id');
        $rekening_id    = Input::get('rekening_id');
        $nama_bank      = strtoupper(Input::get('nama_bank'));
        $nomor_rekening = Input::get('nomor_rekening');
        $atas_nama      = ucwords(Input::get('atas_nama'));
        $mata_uang      = Input::get('mata_uang');

        DB::table('tran_rekening_supplier')->where('supplier_id', $supplier_id)->where('id', $rekening_id)->update(['nama_bank' => $nama_bank, 'nomor_rekening' => $nomor_rekening, 'atas_nama' => $atas_nama, 'mata_uang' => $mata_uang]);

        $request->session()->flash('message2', 'Rekening berhasil diperbarui');
        return redirect('/detail_supplier/'.$supplier_id);
    }

    public function hapus_rekening_supplier(Request $request)
    {
        $id             = Input::get('supplier_id_hapus');
        $supplier_id    = Input::get('rekening_id_supplier');

        DB::table('tran_rekening_supplier')->where('id', $id)->delete();

        $request->session()->flash('message2', 'Rekening berhasil dihapus');
        return redirect('/detail_supplier/'.$supplier_id);
    }
}
