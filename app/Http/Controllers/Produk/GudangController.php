<?php

namespace App\Http\Controllers\Produk;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Gloudemans\Shoppingcart\Facades\Cart;
use Input;
use App\Http\Controllers\Paginator;
use Auth;
use Illuminate\Pagination\LengthAwarePaginator;
use DateTime;
use Validator;
use Response;
use Hashids\Hashids;
use File;
use Illuminate\Support\Str;
use App\Models\Gudang\Gudang;

class GudangController extends Controller
{
    public function gudang()
    {
        $general['title']       = "Gudang";
        $general['menu1']       = "Pengaturan Produk";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 43;

        $gudang = DB::select("SELECT * FROM (
            SELECT g.id, g.nama, g.alamat, g.is_aktif, k.nama AS kota_nama, '1' AS terpakai 
            FROM tmst_gudang g, tran_produk_gudang pg, tref_kota k 
            WHERE g.id=pg.gudang_id AND g.kota_id=k.id
            UNION
            SELECT g.id, g.nama, g.alamat, g.is_aktif, k.nama AS kota_nama, '0' AS terpakai
            FROM tmst_gudang g, tref_kota k 
            WHERE g.kota_id=k.id
            UNION
            SELECT g.id, g.nama, g.alamat, g.is_aktif, k.nama AS kota_nama, '1' AS terpakai 
            FROM tmst_gudang g, tran_hadiah_gudang hg, tref_kota k 
            WHERE g.id=hg.gudang_id AND g.kota_id=k.id
            ) t
            GROUP BY id ORDER BY nama
        ");

        return view('pages.produk.gudang', compact('general', 'gudang'));
    }

    public function tambah_gudang()
    {
        $general['title']       = "Tambah Gudang";
        $general['menu1']       = "Pengaturan Produk";
        $general['menu2']       = "Gudang";
        $general['menu3']       = $general['title'];
        $general['kode_menu']   = 43;

        $provinsi   = DB::table('tref_provinsi')->get();
        $kota       = DB::table('tref_kota')->get();

        return view('pages.produk.tambah_gudang', compact('general', 'provinsi', 'kota'));
    }

    public function do_tambah_gudang(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'alamat' => 'required',
            'provinsi' => 'required',
            'kota' => 'required',
            'telp' => 'required',
            'supervisor' => 'required',
            'hp_supervisor' => 'required',
        ]);

        $nama           = ucwords(Input::get('nama'));
        $alamat         = ucwords(Input::get('alamat'));
        $kota_id        = Input::get('kota');
        $telp           = Input::get('telp');
        $supervisor     = ucwords(Input::get('supervisor'));
        $hp_supervisor  = Input::get('hp_supervisor');
        $longitude      = Input::get('longitude');
        $latitude       = Input::get('latitude');

        $telp           = str_replace("_", "", $telp);
        $hp_supervisor  = str_replace("_", "", $hp_supervisor);

        DB::table('tmst_gudang')->insert(['nama' => $nama, 'alamat' => $alamat, 'kota_id' => $kota_id, 'telp' => $telp, 
            'supervisor' => $supervisor, 'hp_supervisor' => $hp_supervisor, 'longitude' => $longitude, 'latitude' => $latitude, 'is_aktif' => 1]);

        $request->session()->flash('message', 'Gudang berhasil ditambah');
        return redirect('/gudang');
    }

    public function ubah_status_gudang($id, Request $request)
    {
        $data = DB::table('tmst_gudang')->where('id', $id)->first();
        if($data->is_aktif == 1){
            $is_aktif = 0;
        }
        else
        {
            $is_aktif = 1;
        }

        DB::table('tmst_gudang')->where('id', $id)->update(['is_aktif' => $is_aktif]);

        $request->session()->flash('message', 'Status gudang berhasil diperbarui');
        return redirect('/gudang');
    }

    public function hapus_gudang(Request $request)
    {
        $id = Input::get('gudang_id');

        DB::table('tmst_gudang')->where('id', $id)->delete();
        $request->session()->flash('message', 'Gudang berhasil dihapus');
        return redirect('/gudang');
    }

    public function detail_gudang($id)
    {
        $gudang = DB::table('tmst_gudang')->where('id', $id)->first();

        $general['title']       = $gudang->nama;
        $general['menu1']       = "Pengaturan Produk";
        $general['menu2']       = "Gudang";
        $general['menu3']       = $general['title'];
        $general['kode_menu']   = 43;

        $produk_gudang = DB::table('tmst_gudang')->select('tmst_produk.*', 'tmst_kategori_produk.nama as kategori_produk_nama')->join('tran_produk_gudang', 'tran_produk_gudang.gudang_id', '=', 'tmst_gudang.id')->join('tmst_produk', 'tmst_produk.id', '=', 'tran_produk_gudang.produk_id')->join('tmst_kategori_produk', 'tmst_kategori_produk.id', '=', 'tmst_produk.kategori_id')->where('tmst_gudang.id', $id)->get();

        $hadiah_gudang = DB::table('tmst_gudang')->select('tmst_hadiah.*')->join('tran_hadiah_gudang', 'tran_hadiah_gudang.gudang_id', '=', 'tmst_gudang.id')->join('tmst_hadiah', 'tmst_hadiah.id', '=', 'tran_hadiah_gudang.hadiah_id')->where('tmst_gudang.id', $id)->get();

        $my_kota = DB::table('tref_kota')->select('tref_kota.nama', 'tref_kota.id as kota_id', 'tref_kota.provinsi_kode', 'tref_provinsi.id as provinsi_id')->join('tref_provinsi', 'tref_provinsi.kode', '=', 'tref_kota.provinsi_kode')->where('tref_kota.id', $gudang->kota_id)->first();
        $provinsi = DB::table('tref_provinsi')->get();

        return view('pages.produk.detail_gudang', compact('general', 'gudang', 'produk_gudang', 'hadiah_gudang', 'my_kota', 'provinsi'));
    }

    public function ubah_gudang(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'provinsi' => 'required',
            'kota' => 'required',
            'telp' => 'required',
            'supervisor' => 'required',
            'hp_supervisor' => 'required',
        ]);

        $id           = Input::get('id');
        $nama           = ucwords(Input::get('nama'));
        $alamat         = ucwords(Input::get('alamat'));
        $kota_id        = ucwords(Input::get('kota'));
        $telp           = Input::get('telp');
        $supervisor     = ucwords(Input::get('supervisor'));
        $hp_supervisor  = Input::get('hp_supervisor');
        $longitude      = Input::get('longitude');
        $latitude       = Input::get('latitude');

        DB::table('tmst_gudang')->where('id', $id)->update(['nama' => $nama, 'alamat' => $alamat, 'kota_id' => $kota_id, 'telp' => $telp, 
            'supervisor' => $supervisor, 'hp_supervisor' => $hp_supervisor, 'longitude' => $longitude, 'latitude' => $latitude]);

        $request->session()->flash('message', 'GUdang berhasil diperbarui');
        return redirect('/detail_gudang/'.$id);
    }

    public function ubah_nomor_mutasi_gudang(Request $request)
    {
        $id     = Input::get('id');
        $format = Input::get('format');

        $gudang = Gudang::where('id', $id)->first();
        $gudang->format_no_mutasi = strtoupper($format);
        $gudang->save();

        $request->session()->flash('message1', 'Format Nomor berhasil diperbarui');
        return redirect('/pengaturan_umum');
    }
}
