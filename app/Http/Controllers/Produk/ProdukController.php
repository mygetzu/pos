<?php

namespace App\Http\Controllers\Produk;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Gloudemans\Shoppingcart\Facades\Cart;
use Input;
use App\Http\Controllers\Paginator;
use Auth;
use Illuminate\Pagination\LengthAwarePaginator;
use DateTime;
use Validator;
use Response;
use Hashids\Hashids;
use File;
use Illuminate\Support\Str;
use App\Models\Produk\Produk;
use App\Models\Produk\KategoriProduk;
use App\Models\Produk\ParameterSpesifikasi;
use App\Models\Produk\NilaiSpesifikasi;

class ProdukController extends Controller
{
    public function produk(Request $request)
    {
        $general['title']       = "Produk";
        $general['menu1']       = "Pengaturan Produk";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 44;

        $produk = Produk::with('kategori_produk', 'promo_cashback', 'promo_diskon', 'promo_hadiah')->orderBy('nama')->get();

        return view('pages.produk.produk', compact('general', 'produk')); 
    }

    public function ubah_status_produk($id, Request $request)
    {
        $data = DB::table('tmst_produk')->where('id', $id)->first();
        if($data->is_aktif == 1){
            $is_aktif = 0;
        }
        else
        {
            $is_aktif = 1;
        }

        DB::table('tmst_produk')->where('id', $id)->update(['is_aktif' => $is_aktif]);

        $request->session()->flash('message', 'Status produk berhasil diperbarui');
        return redirect('/produk');
    }

    public function tambah_produk()
    {
        $general['title']       = "Tambah Produk";
        $general['menu1']       = "Pengaturan Produk";
        $general['menu2']       = "Produk";
        $general['menu3']       = $general['title'];
        $general['kode_menu']   = 44;

        $kategori_produk = DB::table('tmst_kategori_produk')->where('is_aktif', '1')->orderBy('nama', 'asc')->get();
        $supplier = DB::table('tmst_supplier')->where('is_aktif', '1')->orderBy('nama', 'asc')->get();
        $gudang   = DB::table('tmst_gudang')->where('is_aktif', '1')->orderBy('nama', 'asc')->get();

        return view('pages.produk.tambah_produk', compact('general', 'kategori_produk', 'supplier', 'gudang'));
    }

    public function do_tambah_produk(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'kategori' => 'required',
            'supplier1' => 'required',
            'harga' => 'required',
            'satuan' => 'required',
            'berat' => 'required',
            'deskripsi' => 'required',
        ]);

        $kode           = Input::get('kode');
        $nama           = ucwords(Input::get('nama'));
        $kategori_id    = Input::get('kategori');
        $harga_retail   = Input::get('harga');
        $satuan         = ucwords(Input::get('satuan'));
        $berat          = Input::get('berat');
        $deskripsi      = Input::get('deskripsi');
        $file           = array('produk_galeri' => Input::file('produk_galeri'));

        $slug_nama      = str_slug($nama);
        $harga_retail   = str_replace(".", "", $harga_retail);

        //cek apakah ada nama yg sama
        $cek_nama       = DB::table('tmst_produk')->where('nama', $nama)->get();
        $jml_cek_nama   = count($cek_nama);
        if($jml_cek_nama > 0)
        {
            $slug_nama = $slug_nama.'-'.$jml_cek_nama;
        }

        $harga = "";
        for ($i=0; $i < strlen($harga_retail); $i++) { 
            if (is_numeric($harga_retail[$i]))
            {
               $harga = $harga.$harga_retail[$i];
            }
        }

        $harga_retail = (int)$harga;

        $produk = new Produk;
        $produk->kode           = $kode;
        $produk->nama           = $nama;
        $produk->slug_nama      = $slug_nama;
        $produk->kategori_id    = $kategori_id;
        $produk->deskripsi      = $deskripsi;
        $produk->satuan         = $satuan;
        $produk->berat          = $berat;
        $produk->harga_retail   = $harga_retail;
        $produk->is_aktif       = 1;
        $produk->stok           = 0;
        $produk->stok_dipesan   = 0;
        $produk->jenis_barang_id   = 1;
        $produk->save();

        $id = $produk->id;

        //supplier
        $jumlah_supplier = Input::get('row_supplier');
        
        for ($i=1; $i <= $jumlah_supplier ; $i++) { 
            
            $supplier_id = Input::get('supplier'.$i);
            
            if($supplier_id != "0" && $supplier_id != ""){
                DB::table('tran_produk_supplier')->insert(['produk_id' => $id, 'supplier_id' => $supplier_id]);
            }
        }

        if (Input::hasFile('produk_galeri')) 
        {
                $destinationPath = base_path().'/public/img/produk/';
                $extension = Input::file('produk_galeri')->getClientOriginalExtension();
                $file_gambar = "PRD".$this->random_kode(8);
                $fileName = $file_gambar.'.'.$extension;
                
                Input::file('produk_galeri')->move($destinationPath, $fileName);

                //insert produk_galeri
                DB::table('tran_produk_galeri')->insert(['produk_id' => $id, 'file_gambar' => $fileName]);
        }

        $jumlah_spesifikasi = Input::get('jumlah_spesifikasi');

        for ($i=0; $i < (int)$jumlah_spesifikasi; $i++) {
            $nilai_spesifikasi = new NilaiSpesifikasi;
            $nilai_spesifikasi->produk_id                  = $id;
            $nilai_spesifikasi->parameter_spesifikasi_id   = ucwords(Input::get('parameter_spesifikasi_id'.$i));
            $nilai_spesifikasi->nilai                      = ucwords(Input::get('nilai'.$i));
            $nilai_spesifikasi->satuan                     = ucwords(Input::get('satuan'.$i));
            $nilai_spesifikasi->save();
        }

        $request->session()->flash('message', 'Produk berhasil ditambah');
        return redirect('/produk');
    }

    public function do_tambah_produk_by_ajax(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'kategori' => 'required',
            'supplier1' => 'required',
            'harga' => 'required',
            'satuan' => 'required',
            'berat' => 'required',
            'desk' => 'required',
        ]);

        $kode           = Input::get('kode');
        $nama           = Input::get('nama');
        $kategori_id  = Input::get('kategori');
        $harga_retail   = Input::get('harga');
        $satuan         = ucwords(Input::get('satuan'));
        $berat          = Input::get('berat');
        $deskripsi      = Input::get('desk');
        $file           = array('produk_galeri' => Input::file('produk_galeri'));

        $slug_nama      = str_slug($nama);
        $harga_retail   = str_replace(".", "", $harga_retail);

        //cek apakah ada nama yg sama
        $cek_nama       = DB::table('tmst_produk')->where('nama', $nama)->get();
        $jml_cek_nama   = count($cek_nama);
        if($jml_cek_nama > 0)
        {
            $slug_nama = $slug_nama.'-'.$jml_cek_nama;
        }

        $harga = "";
        for ($i=0; $i < strlen($harga_retail); $i++) { 
            if (is_numeric($harga_retail[$i]))
            {
               $harga = $harga.$harga_retail[$i];
            }
        }

        $harga_retail = (int)$harga;

        $id = DB::table('tmst_produk')->insertGetId(['kode' => $kode, 'nama' => $nama, 'slug_nama' => $slug_nama, 'kategori_id' => $kategori_id, 
            'deskripsi' => $deskripsi, 'satuan' => $satuan, 'berat' => $berat, 'harga_retail' => $harga_retail, 'is_aktif' => '1', 'stok' => 0, 'stok_dipesan' => 0]);

        //supplier
        $jumlah_supplier = Input::get('row_supplier');
        
        for ($i=1; $i <= $jumlah_supplier ; $i++) { 
            
            $supplier_id = Input::get('supplier'.$i);
            
            if($supplier_id != "0" && $supplier_id != ""){
                DB::table('tran_produk_supplier')->insert(['produk_id' => $id, 'supplier_id' => $supplier_id]);
            }
        }


        if (Input::hasFile('produk_galeri')) 
        {
                $destinationPath = base_path().'/public/img/produk/';
                $extension = Input::file('produk_galeri')->getClientOriginalExtension();
                $file_gambar = "PRD".$this->random_kode(8);
                $fileName = $file_gambar.'.'.$extension;
                
                Input::file('produk_galeri')->move($destinationPath, $fileName);

                //insert produk_galeri
                DB::table('tran_produk_galeri')->insert(['produk_id' => $id, 'file_gambar' => $fileName]);
        }

        $jumlah_spesifikasi = Input::get('jumlah_spesifikasi');

        for ($i=0; $i < (int)$jumlah_spesifikasi; $i++) {
            $nilai_spesifikasi = new NilaiSpesifikasi;
            $nilai_spesifikasi->produk_id                  = $id;
            $nilai_spesifikasi->parameter_spesifikasi_id   = ucwords(Input::get('parameter_spesifikasi_id'.$i));
            $nilai_spesifikasi->nilai                      = ucwords(Input::get('nilai'.$i));
            $nilai_spesifikasi->satuan                     = ucwords(Input::get('satuan'.$i));
            $nilai_spesifikasi->save();
        }

        echo '<div class="alert alert-success alert-dismissable flat" style="margin-left: 0px;">
                  <i class="fa fa-check"></i>
                  Produk berhasil ditambahkan
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>';
    }

    public function detail_produk($id)
    {
        $produk = DB::table('tmst_produk')->select('tmst_produk.*', 'tmst_kategori_produk.nama as kategori_produk_nama', 'tmst_kategori_produk.id as kategori_produk_id')->join('tmst_kategori_produk', 'tmst_kategori_produk.id', '=', 'tmst_produk.kategori_id')->where('tmst_produk.id', $id)->first();

        $spesifikasi = ParameterSpesifikasi::with(['nilai_spesifikasi' => function($query) use ($id)
            {return $query->where('produk_id', $id);} ])->where('kategori_produk_id', $produk->kategori_id)->get();

        $jumlah_spesifikasi = count($spesifikasi);
        

        $general['title']       = $produk->nama;
        $general['menu1']       = "Pengaturan Produk";
        $general['menu2']       = "Produk";
        $general['menu3']       = $general['title'];
        $general['kode_menu']   = 44;

        $kategori_produk = DB::table('tmst_kategori_produk')->where('is_aktif', 1)->get();
        $gudang          = DB::table('tmst_gudang')->where('is_aktif', 1)->get();
        $produk_serial_number   = DB::table('tran_produk_serial_number')->join('tmst_gudang', 'tmst_gudang.id', '=', 'tran_produk_serial_number.gudang_id')->where('produk_id', $id)->where('stok', '>', 0)->get();
        $promo_diskon   = DB::table('tran_promo_diskon')->where('produk_id', $id)->get();
        $promo_cashback = DB::table('tran_promo_cashback')->where('produk_id', $id)->get();
        $promo_hadiah   = DB::table('tran_promo_hadiah')->select('tran_promo_hadiah.*', 'tmst_hadiah.nama', 'tmst_hadiah.file_gambar')->join('tmst_hadiah', 'tmst_hadiah.id', '=', 'tran_promo_hadiah.hadiah_id')->where('produk_id', $id)->get();

        $cek_diskon = DB::table('tran_promo_diskon')->where('tran_promo_diskon.produk_id', $id)
        ->where('tran_promo_diskon.awal_periode', '<=', date('Y-m-d'))
        ->where('tran_promo_diskon.akhir_periode', '>=', date('Y-m-d'))->first();

        $cek_cashback = DB::table('tran_promo_cashback')->where('tran_promo_cashback.produk_id', $id)
        ->where('tran_promo_cashback.awal_periode', '<=', date('Y-m-d'))
        ->where('tran_promo_cashback.akhir_periode', '>=', date('Y-m-d'))->first();

        $hadiah = DB::table('tmst_hadiah')->where('is_aktif', 1)->get();
        $produk_supplier = DB::table('tran_produk_supplier')->select('tmst_supplier.*', 'tref_kota.nama as kota_nama', 'tran_produk_supplier.*')->join('tmst_supplier', 'tmst_supplier.id', '=', 'tran_produk_supplier.supplier_id')->join('tref_kota', 'tref_kota.id', '=', 'tmst_supplier.kota_id')->where('tran_produk_supplier.produk_id', $id)->get();
        $produk_galeri = DB::table('tran_produk_galeri')->where('produk_id', $id)->get();

        $supplier = DB::table('tmst_supplier')->where('is_aktif', 1)->get();

        return view('pages.produk.detail_produk', compact('general', 'produk', 'kategori_produk', 'gudang', 'produk_serial_number', 'promo_diskon', 'promo_cashback', 'promo_hadiah', 'cek_diskon', 'cek_cashback', 'hadiah', 'produk_supplier', 'supplier', 'produk_galeri', 'spesifikasi', 'jumlah_spesifikasi'));
    }

    public function ubah_produk(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'harga' => 'required',
            'satuan' => 'required',
            'berat' => 'required',
            'deskripsi' => 'required',
        ]);

        $id                 = Input::get('id');
        $kode               = Input::get('kode');
        $nama               = ucwords(Input::get('nama'));
        $harga_retail       = Input::get('harga');
        $satuan             = ucwords(Input::get('satuan'));
        $berat              = Input::get('berat');
        $deskripsi          = Input::get('deskripsi');
        $kategori_produk_id = Input::get('kategori_produk_id');

        $harga_retail   = str_replace(".", "", $harga_retail);
        $slug_nama      = str_slug($nama);
        //cek apakah ada nama yg sama
        $cek_nama       = DB::table('tmst_produk')->where('nama', $nama)->get();
        $jml_cek_nama   = count($cek_nama);
        if($jml_cek_nama > 1)
        {
            $slug_nama = $slug_nama.'-'.$jml_cek_nama;
        }

        $jumlah_spesifikasi = Input::get('jumlah_spesifikasi');

        for ($i=0; $i < (int)$jumlah_spesifikasi; $i++) { 
            $spesifikasi_param_id   = Input::get('spesifikasi_param_id'.$i);
            $spesifikasi_nilai      = Input::get('spesifikasi_nilai'.$i);
            $spesifikasi_satuan     = Input::get('spesifikasi_satuan'.$i);

            $nilai_spesifikasi = NilaiSpesifikasi::where('parameter_spesifikasi_id', $spesifikasi_param_id)->where('produk_id', $id)->first();
            if(empty($nilai_spesifikasi)){
                $nilai_spesifikasi = new NilaiSpesifikasi;
                $nilai_spesifikasi->produk_id                   = $id;
                $nilai_spesifikasi->parameter_spesifikasi_id    = $spesifikasi_param_id;
                $nilai_spesifikasi->nilai                       = $spesifikasi_nilai;
                $nilai_spesifikasi->satuan                      = $spesifikasi_satuan;
                $nilai_spesifikasi->save();
            }
            else{
                $nilai_spesifikasi->nilai   = $spesifikasi_nilai;
                $nilai_spesifikasi->satuan  = $spesifikasi_satuan;
                $nilai_spesifikasi->save();
            }
        }

        DB::table('tmst_produk')->where('id', $id)->update(['nama' => $nama, 'slug_nama' => $slug_nama, 'deskripsi' => $deskripsi, 
            'satuan' => $satuan, 'berat' => $berat, 'harga_retail' => $harga_retail, 'kategori_id' => $kategori_produk_id, 'kode' => $kode]);

        $request->session()->flash('message1', 'Produk berhasil diperbarui');
        return redirect('/detail_produk/'.$id);
    }

    public function tambah_serial_number(Request$request)
    {
        $this->validate($request, [
            'gudang_id' => 'required',
            'serial_number1' => 'required',
        ]);

        $produk_id = Input::get('produk_id');
        $gudang_id = Input::get('gudang_id');
        $keterangan = Input::get('keterangan');

        for ($i=1; $i <= 10; $i++) 
        { 
            $serial_number  = Input::get('serial_number'.$i);

            if($serial_number == "")
            {
                continue;
            }

            //cek apakah produk serial number sudah ada
            $cek = DB::table('tran_produk_serial_number')->where('produk_id', $produk_id)->where('serial_number', $serial_number)->where('gudang_id', $gudang_id)->first();

            if(empty($cek))
            {
                //tambahkan produk_serial_number
                DB::table('tran_produk_serial_number')->insert(['produk_id' => $produk_id, 'serial_number' => $serial_number, 'keterangan' => $keterangan, 'stok' => 0, 'gudang_id' => $gudang_id]);

                // cek apakah ada produk_lokasi
                $cek2 = DB::table('tran_produk_gudang')->where('produk_id', $produk_id)->where('gudang_id', $gudang_id)->first();

                if(empty($cek2))
                {
                    DB::table('tran_produk_gudang')->insert(['produk_id' => $produk_id, 'stok' => 0, 'gudang_id' => $gudang_id]);
                }

                $request->session()->flash('message2', 'Serial Number berhasil ditambah');
            }
            else{
                $request->session()->flash('message_error2',  "Serial Number : $serial_number sudah ada");
            }
        }

        return redirect('/detail_produk/'.$produk_id);
    }

    public function hapus_serial_number(Request $request)
    {
        $produk_id      = Input::get('hsn_produk_id');
        $serial_number  = Input::get('hsn_serial_number');
        $gudang_id      = Input::get('hsn_gudang_id');

        DB::table('tran_produk_serial_number')->where('produk_id', $produk_id)
        ->where('serial_number', $serial_number)->where('gudang_id', $gudang_id)->delete();

        $request->session()->flash('message2', 'Serial number berhasil dihapus');
        return redirect('/detail_produk/'.$produk_id);
    }

    public function tambah_promo_diskon(Request $request)
    {
        $this->validate($request, [
            'produk_id' => 'required',
            'qty_beli' => 'required',
            'diskon'  => 'required',
            'awal_periode' => 'required|date|before:akhir_periode',
            'akhir_periode'  => 'required|date|after:awal_periode',
        ]);

        $produk_id      = Input::get('produk_id');
        $qty_beli       = Input::get('qty_beli');
        $diskon         = Input::get('diskon');
        $awal_periode   = Input::get('awal_periode');
        $akhir_periode  = Input::get('akhir_periode');

        $awal_periode   = new DateTime($awal_periode);
        $akhir_periode  = new DateTime($akhir_periode);


        DB::table('tran_promo_diskon')->insert(['produk_id' => $produk_id, 'qty_beli' => $qty_beli, 
            'diskon' => $diskon, 'awal_periode' => $awal_periode, 'akhir_periode' => $akhir_periode]);

        $request->session()->flash('message3', 'Promo diskon berhasil ditambah');
        return redirect('/detail_produk/'.$produk_id);
    }

    public function ubah_promo_diskon(Request $request)
    {
        $this->validate($request, [
            'promo_diskon_id' => 'required',
            'qty_beli' => 'required',
            'diskon'  => 'required',
            'awal_periode' => 'required|date|before:akhir_periode',
            'akhir_periode'  => 'required|date|after:awal_periode',
        ]);

        $promo_diskon_id = Input::get('promo_diskon_id');
        $produk_id       = Input::get('produk_id');
        $qty_beli        = Input::get('qty_beli');
        $diskon          = Input::get('diskon');
        $awal_periode    = Input::get('awal_periode');
        $akhir_periode   = Input::get('akhir_periode');

        $awal_periode   = new DateTime($awal_periode);
        $akhir_periode  = new DateTime($akhir_periode);

        DB::table('tran_promo_diskon')->where('id', $promo_diskon_id)->update(['qty_beli' => $qty_beli, 'diskon' => $diskon, 
            'awal_periode' => $awal_periode, 'akhir_periode' => $akhir_periode]);

        $request->session()->flash('message', 'Produk Promo Berhasil Diperbarui');
        return redirect('/detail_produk/'.$produk_id);
    }

    public function hapus_promo_diskon(Request $request)
    {
        $id         = Input::get('hpd_id');
        $produk_id  = Input::get('produk_id');

        DB::table('tran_promo_diskon')->where('id', $id)->delete();

        $request->session()->flash('message3', 'Promo diskon berhasil dihapus');
        return redirect('/detail_produk/'.$produk_id);
    }

    public function tambah_promo_cashback(Request $request)
    {
        $this->validate($request, [
            'produk_id' => 'required',
            'qty_beli' => 'required',
            'cashback'  => 'required',
            'awal_periode' => 'required|date|before:akhir_periode',
            'akhir_periode'  => 'required|date|after:awal_periode',
        ]);

        $produk_id      = Input::get('produk_id');
        $qty_beli       = Input::get('qty_beli');
        $cashback       = Input::get('cashback');
        $awal_periode   = Input::get('awal_periode');
        $akhir_periode  = Input::get('akhir_periode');

        $cashback   = str_replace(".", "", $cashback);

        $awal_periode   = new DateTime($awal_periode);
        $akhir_periode  = new DateTime($akhir_periode);


        DB::table('tran_promo_cashback')->insert(['produk_id' => $produk_id, 'qty_beli' => $qty_beli, 
            'cashback' => $cashback, 'awal_periode' => $awal_periode, 'akhir_periode' => $akhir_periode]);

        $request->session()->flash('message3', 'Promo cashback berhasil ditambah');
        return redirect('/detail_produk/'.$produk_id);
    }

    public function ubah_promo_cashback(Request $request)
    {
        $this->validate($request, [
            'promo_cashback_id' => 'required',
            'qty_beli' => 'required',
            'cashback'  => 'required',
            'awal_periode' => 'required|date|before:akhir_periode',
            'akhir_periode'  => 'required|date|after:awal_periode',
        ]);

        $promo_cashback_id = Input::get('promo_cashback_id');
        $produk_id       = Input::get('produk_id');
        $qty_beli        = Input::get('qty_beli');
        $cashback          = Input::get('cashback');
        $awal_periode    = Input::get('awal_periode');
        $akhir_periode   = Input::get('akhir_periode');

        $cashback   = str_replace(".", "", $cashback);

        $awal_periode   = new DateTime($awal_periode);
        $akhir_periode  = new DateTime($akhir_periode);

        DB::table('tran_promo_cashback')->where('id', $promo_cashback_id)->update(['qty_beli' => $qty_beli, 'cashback' => $cashback, 
            'awal_periode' => $awal_periode, 'akhir_periode' => $akhir_periode]);

        $request->session()->flash('message', 'Produk Promo Berhasil Diperbarui');
        return redirect('/detail_produk/'.$produk_id);
    }

    public function hapus_promo_cashback(Request $request)
    {
        $id         = Input::get('hpc_id');
        $produk_id  = Input::get('produk_id');

        DB::table('tran_promo_cashback')->where('id', $id)->delete();

        $request->session()->flash('message3', 'Promo cashback berhasil dihapus');
        return redirect('/detail_produk/'.$produk_id);
    }

    public function tambah_promo_hadiah(Request $request)
    {
        $this->validate($request, [
            'produk_id' => 'required',
            'qty_beli' => 'required',
            'hadiah_id' => 'required',
            'qty_hadiah' => 'required',
            'awal_periode' => 'required|date|before:akhir_periode',
            'akhir_periode'  => 'required|date|after:awal_periode',
        ]);

        $produk_id    = Input::get('produk_id');
        $qty_beli       = Input::get('qty_beli');
        $hadiah_id    = Input::get('hadiah_id');
        $qty_hadiah     = Input::get('qty_hadiah');
        $awal_periode   = Input::get('awal_periode');
        $akhir_periode  = Input::get('akhir_periode');

        $awal_periode = new DateTime($awal_periode);
        $akhir_periode = new DateTime($akhir_periode);

        DB::table('tran_promo_hadiah')->insert(['produk_id' => $produk_id, 'qty_beli' => $qty_beli, 
            'hadiah_id' => $hadiah_id, 'qty_hadiah' => $qty_hadiah, 'awal_periode' => $awal_periode, 'akhir_periode' => $akhir_periode]);

        $request->session()->flash('message5', 'Produk Hadiah Produk berhasil ditambah');
        return redirect('/detail_produk/'.$produk_id);
    }

    public function ubah_promo_hadiah(Request $request)
    {
        $id             = Input::get('id');
        $produk_id      = Input::get('produk_id');
        $hadiah_id      = Input::get('hadiah_id');
        $qty_beli       = Input::get('qty_beli');
        $qty_hadiah     = Input::get('qty_hadiah');
        $awal_periode   = Input::get('awal_periode');
        $akhir_periode  = Input::get('akhir_periode');

        $awal_periode = new DateTime($awal_periode);
        $akhir_periode = new DateTime($akhir_periode);

        DB::table('tran_promo_hadiah')->where('id', $id)->update(['hadiah_id' => $hadiah_id, 'qty_beli' => $qty_beli, 'qty_hadiah' => $qty_hadiah, 'awal_periode' => $awal_periode, 'akhir_periode' => $akhir_periode]);

        $request->session()->flash('message5', 'Produk Hadiah Berhasil Diperbarui');
        return redirect('/detail_produk/'.$produk_id);
    }

    public function hapus_promo_hadiah(Request $request)
    {
        $id         = Input::get('hph_id');
        $produk_id  = Input::get('produk_id');

        DB::table('tran_promo_hadiah')->where('id', $id)->delete();

        $request->session()->flash('message5', 'Promo hadiah berhasil dihapus');
        return redirect('/detail_produk/'.$produk_id);
    }

    public function tambah_produk_supplier(Request $request)
    {
        $produk_id = Input::get('produk_id');
        $supplier_id = Input::get('supplier_id');

        $cek = DB::table('tran_produk_supplier')->where('produk_id', $produk_id)->where('supplier_id', $supplier_id)->first();

         if(!empty($cek))
         {
            $request->session()->flash('message6', 'Produk Supplier sudah ada');
            return redirect('/detail_produk/'.$produk_id);
         }
         else
         {
            DB::table('tran_produk_supplier')->insert(['produk_id' => $produk_id, 'supplier_id' => $supplier_id]);
            $request->session()->flash('message6', 'Produk Supplier berhasil ditambah');
            return redirect('/detail_produk/'.$produk_id);
         }        
    }

    public function hapus_produk_supplier(Request $request)
    {
        $produk_id    = Input::get('hs_produk_id');
        $supplier_id  = Input::get('hs_supplier_id');

        DB::table('tran_produk_supplier')->where('produk_id', $produk_id)->where('supplier_id', $supplier_id)->delete();

        $request->session()->flash('message6', 'Produk Supplier berhasil dihapus');
        return redirect('/detail_produk/'.$produk_id);
    }

    public function tambah_produk_galeri(Request $request, $produk_id)
    {
        $file = array('image' => Input::file('image'));

        var_dump($file);
  
        $validator = Validator::make($request->all(), [
            'image' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('/detail_produk/'.$produk_id);
        }
        else {
            if (Input::hasFile('image')) 
            {
                $destinationPath = base_path().'/public/img/produk/';
                $extension = Input::file('image')->getClientOriginalExtension();
                $file_gambar = "PRD".$this->random_kode(8);
                $fileName = $file_gambar.'.'.$extension;
                
                Input::file('image')->move($destinationPath, $fileName);

                //insert produk_galeri
                DB::table('tran_produk_galeri')->insert(['produk_id' => $produk_id, 'file_gambar' => $fileName]);

                $request->session()->flash('message7', 'Produk Galeri berhasil ditambah');
                return redirect('/detail_produk/'.$produk_id);
            }
        }
    }

    public function ubah_produk_galeri(Request $request)
    {
        $produk_id  = Input::get('produk_id');
        $id         = Input::get('id');
        $file       = array('edit_image' => Input::file('edit_image'));
  
        $validator = Validator::make($request->all(), [
            'edit_image' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('/detail_produk/'.$produk_id);
        }
        else {
            if (Input::hasFile('edit_image')) {
                $destinationPath = base_path().'/public/img/produk/';
                $extension = Input::file('edit_image')->getClientOriginalExtension();
                $pg = DB::table('tran_produk_galeri')->where('id', $id)->first();
                File::Delete(base_path().'/public/img/produk/'.$pg->file_gambar);
                $file_gambar = "PRD".$this->random_kode(8);
                $fileName = $file_gambar.'.'.$extension;
                
                Input::file('edit_image')->move($destinationPath, $fileName);

                //update hadiah file_gambar
                DB::table('tran_produk_galeri')->where('id', $id)->update(['file_gambar' => $fileName]);
                $request->session()->flash('message', 'Gambar berhasil diperbarui');
                return redirect('/detail_produk/'.$produk_id);
            }
        }
    }

    public function hapus_produk_galeri(Request $request)
    {
        $id         = Input::get('hg_id');
        $produk_id  = Input::get('produk_id');

        $produk_galeri  = DB::table('tran_produk_galeri')->where('id', $id)->first();

        File::Delete(base_path().'/public/img/produk/'.$produk_galeri->file_gambar);

        DB::table('tran_produk_galeri')->where('id', $id)->delete();

        $request->session()->flash('message_galeri', 'Gambar berhasil dihapus');
        return redirect('/detail_produk/'.$produk_id);
    }

    public function get_produk(Request $request)
    {
        $kategori_id = Input::get('kategori_id');
        if(empty($kategori_id))
        {
            $produk = Produk::with('produk_galeri')->get();
        }
        else
        {
            $produk = Produk::with('produk_galeri')->where('kategori_id', $kategori_id)->get();
        }

        $gambar_produk_default = DB::table('static')->where('nama', 'gambar_produk_default')->first();

        return view('pages.produk.ajax_get_produk', compact('produk', 'gambar_produk_default'));
    }

    public function random_kode($digit)
    {
        $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $res = "";

        for ($i = 0; $i < $digit; $i++) {
            $res .= $chars[mt_rand(0, strlen($chars)-1)];
        }

        return $res;
    }

    public function get_kolom_spesifikasi()
    {
        $kategori_produk_id = Input::get('kategori_id');

        $parameter_spesifikasi = ParameterSpesifikasi::where('kategori_produk_id', $kategori_produk_id)->get();

        return view('pages.produk.ajax_get_kolom_spesifikasi', compact('parameter_spesifikasi')); 
    }
}
