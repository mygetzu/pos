<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Gloudemans\Shoppingcart\Facades\Cart;
use Auth;
use Response;
use PDF;
use Hashids\Hashids;
use stdClass;
use Illuminate\Pagination\Paginator;
use Input;
use Session;
use File;

class ChattingController extends Controller
{
    public function chat()
    {
        $general['title']       = "Chat";
        $general['menu1']       = "Beranda";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 13;  

        $admin      = DB::table('users')->where('hak_akses_id', '1')->where('id', '!=', Auth::user()->id)->get();
        $pelanggan  = DB::table('users')->where('hak_akses_id', '2')->get();
        $notif      = DB::table('notif_chat')->where('user_id', Auth::user()->id)->get();

        $user_id_aktif = 0;

        $my_chat    = DB::table('log_chat')->where('user_id_a', Auth::user()->id)->orWhere('user_id_b', Auth::user()->id)->get();

        return view('pages.chat.chat', compact('general', 'admin', 'pelanggan', 'notif', 'user_id_aktif', 'my_chat'));
    }

    public function show_chat($user_id_aktif)
    {
        if(!Auth::check())
            return redirect('/auth/login');

        $general['title']       = "Chat";
        $general['menu1']       = "Beranda";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 13;

        $admin      = DB::table('users')->where('hak_akses_id', '1')->where('id', '!=', Auth::user()->id)->get();
        $pelanggan   = DB::table('users')->where('hak_akses_id', '2')->get();

        //hapus notif dari id ini
        DB::table('notif_chat')->where('user_id', Auth::user()->id)->where('user_id_from', $user_id_aktif)->delete();

        return view('pages.chat.chat', compact('general', 'admin', 'pelanggan', 'notif', 'user_id_aktif'));
    }

    public function save_log_chat()
    {
        $user_id_a  = Input::get('user_id_a');
        $user_id_b  = Input::get('user_id_b');
        $pesan      = Input::get('pesan');

        //cek apakah log antar user sudah ada
        $cek_log = DB::table('log_chat')->where('user_id_a', $user_id_a)->where('user_id_b', $user_id_b)->first();

        if(empty($cek_log))
        {
            $cek_log = DB::table('log_chat')->where('user_id_a', $user_id_b)->where('user_id_b', $user_id_a)->first();
        }

        if(empty($cek_log))
        {
            $pesan          = json_decode($pesan);
            $log_result[]   = $pesan;
            $log_result     = json_encode($log_result);
            DB::table('log_chat')->insert(['user_id_a' => $user_id_a, 'user_id_b' => $user_id_b, 'pesan' => $log_result]);
           
        }
        else
        {  
            $log        = DB::table('log_chat')->where('id', $cek_log->id)->first();
            $pesan_old  = json_decode($log->pesan, true);
            $pesan      = json_decode($pesan, true);

            $log_result[] = $pesan_old[0];
            for ($i=1; $i < count($pesan_old); $i++) { 
                array_push($log_result, $pesan_old[$i]);
            }

            array_push($log_result, $pesan);
            $log_result = json_encode($log_result);

            DB::table('log_chat')->where('id', $cek_log->id)->update(['pesan' => $log_result]);
        }
    }

    public function delete_pelanggan_notif($user_id_from)
    {
        //hapus notif dari id ini
        DB::table('notif_chat')->where('user_id', Auth::user()->id)->where('user_id_from', $user_id_from)->delete();
    }

    public function delete_notif()
    {
        $id = Input::get('id');

        //hapus notif dari id ini
        DB::table('notif_chat')->where('user_id', Auth::user()->id)->where('user_id_from', $id)->delete();

        $chat = DB::table('notif_chat')->where('user_id', Auth::user()->id)->get();

        $jml_notif = 0;
        $data_notif = null;
        foreach ($chat as $val) {
            $user = DB::table('users')->where('id', $val->user_id_from)->first();
            $data_notif[$jml_notif]['id']       = $val->user_id_from;
            $data_notif[$jml_notif]['nama']     = $user->name;
            $pesan = json_decode($val->pesan);
            $data_notif[$jml_notif]['pesan']    = $pesan->pesan;
            $data_notif[$jml_notif]['tanggal']  = $pesan->tanggal;
            $jml_notif++;
        }

        return Response::json($data_notif);
    }

    public function get_chat_admin()
    { 
        $user_id = Input::get('id');

        $users  = DB::table('users')->where('id', $user_id)->first();
        $chat   = DB::table('log_chat')->where('user_id_a', Auth::user()->id)->Where('user_id_b', $user_id)->first();

        if(empty($chat))
        {
            $chat = DB::table('log_chat')->where('user_id_b', Auth::user()->id)->Where('user_id_a', $user_id)->first();

        }

        if(empty($chat))
        {
            $data = "";
        }
        else{
            $pesan  = json_decode($chat->pesan, true);
            $indeks = 0;
            $data   = null;
            foreach ($pesan as $val) {
                $user = DB::table('users')->where('id', $val['user_id'])->first();
                $data[$indeks]['user_id']     = $val['user_id'];
                $data[$indeks]['nama']        = $user->name;
                $data[$indeks]['tanggal']     = $val['tanggal'];
                $data[$indeks]['pesan']       = $val['pesan'];
                $indeks++;
                if($indeks == 10)
                break;
            }
        }

        return view('pages.chat.ajax_chat', compact('users', 'data'));
    }

    public function save_notif(Request $request)
    {
        $msg_notif      = Input::get('msg_notif');
        $user_id_from   = Input::get('user_id_from');

        DB::table('notif_chat')->insert(['user_id' => Auth::user()->id, 'user_id_from' => $user_id_from, 'pesan' => $msg_notif]);

        $chat = DB::table('notif_chat')->where('user_id', Auth::user()->id)->get();

        $jml_notif = 0;
        $data_notif = null;
        foreach ($chat as $val) {
            $user = DB::table('users')->where('id', $val->user_id_from)->first();
            $data_notif[$jml_notif]['id']       = $val->user_id_from;
            $data_notif[$jml_notif]['nama']     = $user->name;
            $pesan = json_decode($val->pesan);
            $data_notif[$jml_notif]['pesan']    = $pesan->pesan;
            $data_notif[$jml_notif]['tanggal']  = $pesan->tanggal;
            $jml_notif++;
        }

        return Response::json($data_notif);
    }

    public function get_notif()
    {
        $chat = DB::table('notif_chat')->where('user_id', Auth::user()->id)->get();

        $jml_notif = 0;
        $data_notif = null;
        foreach ($chat as $val) {
            $user = DB::table('users')->where('id', $val->user_id_from)->first();
            $data_notif[$jml_notif]['id']       = $val->user_id_from;
            $data_notif[$jml_notif]['nama']     = $user->name;
            $pesan = json_decode($val->pesan);
            $data_notif[$jml_notif]['pesan']    = $pesan->pesan;
            $data_notif[$jml_notif]['tanggal']  = $pesan->tanggal;
            $jml_notif++;
        }

        return Response::json($data_notif);
    }

    public function get_log_chat()
    {
        $id_from = Input::get('id');
        $chat = DB::table('log_chat')->where('user_id_a', Auth::user()->id)->Where('user_id_b', $id_from)->first();

        if(empty($chat))
        {
            $chat = DB::table('log_chat')->where('user_id_b', Auth::user()->id)->Where('user_id_a', $id_from)->first();
        }

        if(empty($chat))
        {
            return;
        }

        $pesan  = json_decode($chat->pesan, true);
        $indeks = 0;
        $data   = null;
        foreach ($pesan as $val) {
            $user = DB::table('users')->where('id', $val['user_id'])->first();
            $data[$indeks]['user_id']     = $val['user_id'];
            $data[$indeks]['nama']        = $user->name;
            $data[$indeks]['tanggal']     = $val['tanggal'];
            $data[$indeks]['pesan']       = $val['pesan'];
            $indeks++;
            if($indeks == 10)
            break;
        }
        
        return Response::json($data);
    }
}
