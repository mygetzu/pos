<?php

namespace App\Http\Controllers;

use App\User;
//use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request;
//use App\Http\Requests;
use App\Models\Transaksi\ServiceOrder;
use App\Models\TransaksiService\InvoiceHeader;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TeknisiController extends Controller
{

    public function beranda_teknisi()
    {
        $general['title'] = "Dashboard";
        $general['menu1'] = "Beranda";
        $general['menu2'] = $general['title'];
        $general['kode_menu'] = 11;

        $active['parent'] = 1;
        $active['child'] = 1;

        $data['akan_servis'] = ServiceOrder::where('status_service', 'Belum')->count();
        $data['sedang_servis'] = ServiceOrder::where('status_service', 'Proses')->where('teknisi_id', Auth::user()->id)->count();
        $data['telah_servis'] = ServiceOrder::where('status_service', 'Selesai')->count();

        $data['servis'] = ServiceOrder::where('status_service', 'Proses')->where('teknisi_id', Auth::user()->id)->orderBy('tanggal_diterima', 'asc')->get();
        foreach ($data['servis'] as $i => $r_servis) {
            $data[$i]['servis_id'] = $r_servis->id;
            $data[$i]['no_nota'] = $r_servis->no_nota;
            $data[$i]['model_produk'] = $r_servis->model_produk;
            $data[$i]['keluhan_pelanggan'] = $r_servis->keluhan_pelanggan;
            $data[$i]['pelanggan'] = DB::table('tmst_pelanggan')->where('id', $r_servis->pelanggan_id)->value('nama');
            $data[$i]['flag_acc'] = $r_servis->flag_acc;
            $data[$i]['tgl_diterima'] = $r_servis->tanggal_diterima;
            $data[$i]['status_service'] = $r_servis->status_service;
            $data[$i]['invoice_header_count'] = InvoiceHeader::where('service_order_id', $data[$i]['servis_id'])->count();
            if($data[$i]['invoice_header_count'] == 0){
                $data[$i]['is_submit'] = 0;
                $data[$i]['harga_total'] = 0;
            } else {
                $checkInvoiceheader = InvoiceHeader::where('service_order_id', $data[$i]['servis_id'])->get();
                foreach($checkInvoiceheader as $val){
                    $data[$i]['is_submit'] = $val->is_submit;
                    $data[$i]['harga_total'] = $val->harga_total;
                }
            }
            //status
            if($data[$i]['status_service'] == 'Belum') {
                $data[$i]['status'] = 'Belum Ada Teknisi';
            } elseif ($data[$i]['status_service'] == 'Proses') {
                if($data[$i]['invoice_header_count'] == 0) {
                    $data[$i]['status'] = 'Belum Diperiksa';
                } else {
                    if($data[$i]['is_submit'] == 0) {
                        $data[$i]['status'] = 'Sedang Diperiksa';
                    } else {
                        if($data[$i]['harga_total'] == 0) {
                            $data[$i]['status'] = 'Belum Diberi Biaya';
                        } else {
                            if($data[$i]['flag_acc'] == 0) {
                                $data[$i]['status'] = 'Belum Disetujui';
                            } else {
                                $data[$i]['status'] = 'Telah Disetujui';
                            }
                        }
                    }
                }
            } elseif ($data[$i]['status_service'] == 'Selesai') {
            $data[$i]['status'] = 'Perbaikan Selesai';
            } elseif ($data[$i]['status_service'] == 'Selesai') {
                $data[$i]['status'] = 'Perbaikan Dibatalkan';
            }
            $i++;
        }

        return view('pages.teknisi.beranda_teknisi', compact('general', 'active', 'data'));
    }

    public function data_akun()
    {
        $general['title'] = "Akun";
        $general['menu1'] = "Data Akun";
        $general['menu2'] = $general['title'];
        $general['kode_menu'] = 21;

        $active['parent'] = 2;
        $active['child'] = 1;

        $user_id = Auth::user()->id;
        $data['akun'] = User::where('id', $user_id)->first();
        $data['kota'] = DB::table('tref_kota')->select('tref_kota.nama', 'tref_kota.id as kota_id', 'tref_kota.provinsi_kode', 'tref_provinsi.id as provinsi_id')->join('tref_provinsi', 'tref_provinsi.kode', '=', 'tref_kota.provinsi_kode')->where('tref_kota.id', $data['akun']->kota_id)->first();
        $data['provinsi'] = DB::table('tref_provinsi')->get();

        return view('pages.teknisi.data_akun', $data, compact('general', 'active'));
    }


}
