<?php

namespace App\Http\Controllers\Stok;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Gloudemans\Shoppingcart\Facades\Cart;
use Input;
use Auth;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Session;
use Promocodes;
use Response;
use DateTime;
use PDF;
use App\Models\Stok\StokOpnameHeader;
use App\Models\Stok\StokOpnameDetail;
use App\Models\Gudang\Gudang;
use App\Models\Gudang\ProdukGudang;
use App\Models\Produk\ProdukSerialNumber;

class StokOpnameController extends Controller
{
    public function stok_opname()
    {
        $general['title']       = "Stok Opname";
        $general['menu1']       = "Stok";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 63;

        $stok_opname = StokOpnameHeader::with('gudang')->select('tran_stok_opname_header.*', 'users.name as name')->leftJoin('users', 'users.id', '=', 'tran_stok_opname_header.approval_by_user_id')->orderBy('tanggal', 'desc')->get();

        return view('pages.stok.stok_opname', compact('general', 'stok_opname'));
    }

    public function tambah_stok_opname()
    {
        $general['title']       = "Tambah Stok Opname";
        $general['menu1']       = "Stok";
        $general['menu2']       = "Stok Opname";
        $general['menu3']       = $general['title'];
        $general['kode_menu']   = 63;

        $gudang = Gudang::where('is_aktif', 1)->get();

        date_default_timezone_set("Asia/Jakarta");
        $tanggal        = new DateTime;

        $nomor_invoice  = DB::table('tref_nomor_invoice')->where('id', 11)->first();
        $format_invoice = $nomor_invoice->format;
        $ori_no_stok_opname = $format_invoice;

        $cek_dd = strpos($format_invoice, 'DD');
        if(!empty($cek_dd))
        {
            $format_invoice = str_replace('DD', $tanggal->format('d'), $format_invoice);
        }

        $cek_mm = strpos($format_invoice, 'MM');
        if(!empty($cek_mm))
        {
            $format_invoice = str_replace('MM', $tanggal->format('m'), $format_invoice);
        }

        $cek_yyyy = strpos($format_invoice, 'YYYY');
        if(!empty($cek_yyyy))
        {
            $format_invoice = str_replace('YYYY', $tanggal->format('Y'), $format_invoice);
        }

        $cek_yy = strpos($format_invoice, 'YY');
        if(!empty($cek_yy))
        {
            $format_invoice = str_replace('YY', $tanggal->format('y'), $format_invoice);
        }

        //cek maksimal 10
        $number = '##########';

        for ($i=9; $i > 0; $i--) { 
            $number     = substr($number, 0, $i);
            $cek_number = strpos($format_invoice, $number);
            $nol        = "";
            if(!empty($cek_number))
            {
                $mutasi_stok_header      = StokOpnameHeader::orderBy('id', 'desc')->first();
                if(empty($mutasi_stok_header))
                    $get_num = 1;
                else
                    $get_num        = (int)$mutasi_stok_header->id + 1;

                for ($j=0; $j < $i - strlen($get_num); $j++) { 
                    $nol = $nol."0";
                }

                $get_num = $nol.$get_num;
                $format_invoice = str_replace($number, $get_num, $format_invoice);
                break;
            }
        }

        $no_stok_opname     = $format_invoice;

        return view('pages.stok.tambah_stok_opname', compact('general', 'gudang', 'no_stok_opname', 'ori_no_stok_opname'));
    }

    public function stok_opname_get_produk()
    {
        $gudang_id = Input::get('gudang');

        $produk_gudang = ProdukGudang::with('produk')->where('gudang_id', $gudang_id)->get();

        return view('pages.stok.ajax_stok_opname_get_produk', compact('produk_gudang'));
    }

    public function stok_opname_get_serial_number()
    {
        $gudang_id      = Input::get('gudang');
        $produk_id      = Input::get('produk_id');
        //sementara produk dulu
        $produk_serial_number = ProdukSerialNumber::where('gudang_id', $gudang_id)->where('produk_id', $produk_id)->get();

        foreach($produk_serial_number as $key => $val){
            echo '<input type="hidden" id="list_serial_number'.$key.'" value="'.$val->serial_number.'">';
        }

        echo '<input type="hidden" id="total_serial_number" value="'.count($produk_serial_number).'">';
    }

    public function do_tambah_stok_opname()
    {
        $no_stok_opname     = Input::get('no_stok_opname');
        $gudang_id          = Input::get('gudang');
        $tanggal            = Input::get('tanggal');
        $total_stok         = Input::get('total_stok');
        $count_stok_opname  = Input::get('count_stok_opname');

        $stok_opname_header = new StokOpnameHeader;

        $stok_opname_header->no_stok_opname         = $no_stok_opname;
        $stok_opname_header->gudang_id              = $gudang_id;
        $stok_opname_header->tanggal                = new DateTime($tanggal);
        $stok_opname_header->total_stok             = $total_stok;
        $stok_opname_header->approval_by_user_id    = Auth::user()->id;
        $stok_opname_header->save();

        $id = $stok_opname_header->id;

        for ($i=0; $i < (int)$count_stok_opname; $i++) { 
            $produk_id      = Input::get('stok_opname_produk_id'.$i);
            $serial_number  = Input::get('stok_opname_serial_number'.$i);
            $jumlah         = Input::get('stok_opname_jumlah'.$i);

            if($produk_id != "" && $serial_number != "" && $jumlah != ""){
                $stok_opname_detail = new StokOpnameDetail;

                $stok_opname_detail->stok_opname_header_id  = $id;
                $stok_opname_detail->no_stok_opname         = $no_stok_opname;
                $stok_opname_detail->produk_id              = $produk_id;
                $stok_opname_detail->serial_number          = $serial_number;
                $stok_opname_detail->jumlah                 = $jumlah;
                $stok_opname_detail->save();
            }
        }

        return redirect('/detail_stok_opname/'.$id);
    }

    public function detail_stok_opname($id)
    {
        $stok_opname_header = StokOpnameHeader::with('gudang')->where('id', $id)->first();
        $produk             = StokOpnameDetail::with('produk')->where('stok_opname_header_id', $id)->groupBy('produk_id')->orderBy('produk_id', 'asc')->get();

        foreach ($produk as $key => $value) {
            $serial_number[$key] = StokOpnameDetail::where('stok_opname_header_id', $id)->where('produk_id', $value->produk_id)->orderBy('serial_number', 'asc')->get();
        }

        $general['title']       = "Stok Opname #".$stok_opname_header->no_stok_opname;
        $general['menu1']       = "Stok";
        $general['menu2']       = "Stok Opname";
        $general['menu3']       = $general['title'];
        $general['kode_menu']   = 63;

        return view('pages.stok.detail_stok_opname', compact('general', 'stok_opname_header', 'produk', 'serial_number'));
    }
}
