<?php

namespace App\Http\Controllers\Stok;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Stok\PenyesuaianStokHeader;
use App\Models\Stok\PenyesuaianStokDetail;
use App\Models\Gudang\Gudang;
use App\Models\Gudang\ProdukGudang;
use App\Models\Produk\ProdukSerialNumber;
use App\Models\Produk\Produk;
use Input;
use DateTime;
use Auth;
use Session;

class PenyesuaianStokController extends Controller
{
    public function penyesuaian_stok()
    {
        $general['title']       = "Penyesuaian Stok";
        $general['menu1']       = "Stok";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 61; 

        $penyesuaian_stok_header = PenyesuaianStokHeader::with('gudang', 'approval_by_user', 'received_by_user')->get();

        return view('pages.stok.penyesuaian_stok.penyesuaian_stok', compact('general', 'penyesuaian_stok_header'));
    }

    public function tambah_penyesuaian_stok()
    {
        $general['title']       = "Tambah Penyesuaian Stok";
        $general['menu1']       = "Stok";
        $general['menu2']       = "Penyesuaian Stok";
        $general['menu3']       = $general['title'];
        $general['kode_menu']   = 61;

        $gudang = Gudang::where('is_aktif', 1)->get();
        $no_penyesuaian_stok = "";

        return view('pages.stok.penyesuaian_stok.tambah_penyesuaian_stok', compact('general', 'gudang', 'no_penyesuaian_stok'));
    }

    public function penyesuaian_stok_get_produk()
    {
        $gudang_id = Input::get('gudang_id');

        $produk_gudang = ProdukGudang::with('produk')->where('gudang_id', $gudang_id)->get();

        return view('pages.stok.penyesuaian_stok.ajax_penyesuaian_stok_get_produk', compact('produk_gudang'));
    }

    public function penyesuaian_stok_get_serial_number()
    {
        $gudang_id = Input::get('gudang_id');
        $produk_id = Input::get('produk_id');

        $produk_serial_number = ProdukSerialNumber::where('gudang_id', $gudang_id)->where('produk_id', $produk_id)->get();

        return view('pages.stok.penyesuaian_stok.ajax_penyesuaian_stok_get_serial_number', compact('produk_serial_number'));
    }

    public function penyesuaian_stok_cek_serial_number()
    {
        $gudang_id      = Input::get('gudang_id');
        $produk_id      = Input::get('produk_id');
        $serial_number  = Input::get('serial_number');

        $produk_serial_number = ProdukSerialNumber::where('gudang_id', $gudang_id)->where('produk_id', $produk_id)->where('serial_number', $serial_number)->first();

        //jika dia ada, maka kembalikan nilai berupa stok, jika tidak ada maka kembalikan string null
        if(!empty($produk_serial_number)){
            return $produk_serial_number->stok;
        }
        else{
            return "null";
        }
    }

    public function do_tambah_penyesuaian_stok(Request $request)
    {
        $no_penyesuaian_stok    = Input::get('no_penyesuaian_stok');
        $gudang_id              = Input::get('gudang_id');
        $tanggal                = Input::get('tanggal');
        $keterangan             = Input::get('keterangan');
        $counter                = Input::get('counter');

        $penyesuaian_stok_header = new PenyesuaianStokHeader;
        $penyesuaian_stok_header->no_penyesuaian_stok   = $no_penyesuaian_stok;
        $penyesuaian_stok_header->gudang_id             = $gudang_id;
        $penyesuaian_stok_header->tanggal               = new DateTime($tanggal);
        $penyesuaian_stok_header->keterangan            = $keterangan;
        $penyesuaian_stok_header->approval_by_user_id   = Auth::user()->id;
        $penyesuaian_stok_header->save();

        $penyesuaian_stok_header_id = $penyesuaian_stok_header->id;

        for ($i=0; $i < (int)$counter; $i++) { 
            $produk_id      = Input::get('produk_id'.$i);
            $serial_number  = Input::get('serial_number'.$i);
            $stok_ubah      = Input::get('stok_ubah'.$i);

            if(empty($produk_id) || empty($serial_number)){
                continue;
            }

            //ambil data produk
            $produk_serial_number = ProdukSerialNumber::where('gudang_id', $gudang_id)->where('produk_id', $produk_id)->where('serial_number', $serial_number)->first();

            if(empty($produk_serial_number)){
                $stok_awal = 0;
                
                $psn = new ProdukSerialNumber;
                $psn->gudang_id     = $gudang_id;
                $psn->produk_id     = $produk_id;
                $psn->serial_number = $serial_number;
                $psn->stok          = $stok_ubah;
                $psn->save();
            }
            else{
                $stok_awal = $produk_serial_number->stok;
                
                $produk_serial_number->stok = $produk_serial_number->stok + (int)$stok_ubah;
                $produk_serial_number->save();
            }

            $penyesuaian_stok_detail = new PenyesuaianStokDetail;
            $penyesuaian_stok_detail->penyesuaian_stok_header_id = $penyesuaian_stok_header_id;
            $penyesuaian_stok_detail->produk_id     = $produk_id;
            $penyesuaian_stok_detail->serial_number = $serial_number;
            $penyesuaian_stok_detail->stok_awal     = $stok_awal;
            $penyesuaian_stok_detail->stok_ubah     = $stok_ubah;
            $penyesuaian_stok_detail->stok_baru     = (int)$stok_awal + (int)$stok_ubah;
            $penyesuaian_stok_detail->save();

            //ubah stok di produk, produk_gudang, produk_serial_number
            $produk = Produk::where('id', $produk_id)->first();
            $produk->stok = $produk->stok + (int)$stok_ubah;
            $produk->save();

            $produk_gudang = ProdukGudang::where('produk_id', $produk_id)->where('gudang_id', $gudang_id)->first();
            $produk_gudang->stok = $produk_gudang->stok + $stok_ubah;
            $produk_gudang->save();            
        }

        $request->session()->flash('message', 'Penyesuaian Stok Telah Dilakukan');
        return redirect('/penyesuaian_stok');
    }

    public function detail_penyesuaian_stok($id)
    {
        $general['title']       = "Detail Penyesuaian Stok";
        $general['menu1']       = "Stok";
        $general['menu2']       = "Penyesuaian Stok";
        $general['menu3']       = $general['title'];
        $general['kode_menu']   = 61;

        $penyesuaian_stok_header = PenyesuaianStokHeader::with('gudang')->where('id', $id)->first();
        $penyesuaian_stok_detail = PenyesuaianStokDetail::with('produk')->where('penyesuaian_stok_header_id', $penyesuaian_stok_header->id)->get();

        return view('pages.stok.penyesuaian_stok.detail_penyesuaian_stok', compact('general', 'penyesuaian_stok_header', 'penyesuaian_stok_detail'));
    }
}
