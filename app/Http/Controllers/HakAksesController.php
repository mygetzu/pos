<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\User\HakAkses;
use Input;
use Validator;
use Illuminate\Support\Facades\DB;

class HakAksesController extends Controller
{
    public function hak_akses()
    {
        $general['title']       = "Hak Akses";
        $general['menu1']       = "Akun dan Pelanggan";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 33;        

        $hak_akses_obj = HakAkses::all();

        $hak_akses = [];
        foreach ($hak_akses_obj as $key => $value) {
            $hak_akses[$key]['id']          = $value->id;
            $hak_akses[$key]['nama']        = $value->nama;
            $hak_akses[$key]['deskripsi']   = $value->deskripsi;
            $my_menu_id = explode('-', $value->menu_akses);
            $my_menu_nama = "";
            for ($i=0; $i < count($my_menu_id); $i++) { 
                $menu = DB::table('menu')->where('id', $my_menu_id[$i])->first();
                
                if(empty($menu)){
                    continue;
                }

                if(empty($my_menu_nama)){
                    $my_menu_nama = $menu->nama;
                }
                else{
                    $my_menu_nama = $my_menu_nama.','.$menu->nama;
                }
            }

            $hak_akses[$key]['menu_akses_nama'] = $my_menu_nama;
            $hak_akses[$key]['menu_akses_id']   = $value->menu_akses;
        }

        $menu = DB::table('menu')->get();

        return view('pages.user.hak_akses', compact('general', 'hak_akses', 'menu'));
    }

    public function tambah_hak_akses()
    {
        $general['title']       = "Tambah Hak Akses";
        $general['menu1']       = "Akun dan Pelanggan";
        $general['menu2']       = "Hak Akses";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 33;    

        $menu = DB::table('menu')->get();

        return view('pages.user.tambah_hak_akses', compact('general', 'menu'));
    }

    public function do_tambah_hak_akses(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
        ]);

        $total_menu = Input::get('total_menu');
        $menu_akses = "";

        for ($i=0; $i < (int)$total_menu; $i++) { 
            $my_menu    = Input::get('menu'.$i);
            if(!empty($my_menu)){
                if(empty($menu_akses)){
                    $menu_akses = $my_menu;
                }
                else{
                    $menu_akses = $menu_akses.'-'.$my_menu;
                }
            }
        }

        $hak_akses = new HakAkses;

        $hak_akses->nama        = Input::get('nama');
        $hak_akses->deskripsi   = Input::get('deskripsi');
        $hak_akses->menu_akses  = $menu_akses;
        $hak_akses->save();

        $request->session()->flash('message', 'Hak akses berhasil ditambahkan');
        return redirect('/hak_akses');
    }

    public function ubah_hak_akses(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
        ]);

        for ($i=0; $i < 36; $i++) { 
            $my_menu    = Input::get('hak_akses_menu'.$i);
            if(!empty($my_menu)){
                if(empty($menu_akses)){
                    $menu_akses = $my_menu;
                }
                else{
                    $menu_akses = $menu_akses.'-'.$my_menu;
                }
            }
        }


        $id         = Input::get('id');
        $hak_akses  = HakAkses::where('id', $id)->first();

        $hak_akses->nama        = Input::get('nama');
        $hak_akses->deskripsi   = Input::get('deskripsi');
        if($id != '1' && $id != '2'){
            $hak_akses->menu_akses  = $menu_akses;
        }
        $hak_akses->save();

        $request->session()->flash('message', 'Hak akses berhasil diperbarui');
        return redirect('/hak_akses');
    }

    public function hapus_hak_akses(Request $request)
    {
        $id         = Input::get('id');
        $hak_akses  = HakAkses::where('id', $id)->first();
        $hak_akses->delete();

        $request->session()->flash('message', 'Hak akses berhasil dihapus');
        return redirect('/hak_akses');
    }
}
