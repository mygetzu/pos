<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Gloudemans\Shoppingcart\Facades\Cart;
use Auth;
use Response;
use PDF;
use Hashids\Hashids;
use stdClass;
use Illuminate\Pagination\Paginator;
use Input;
use Session;
use File;
use App\Models\Perusahaan;
use App\Models\Cabang;
use App\Models\Referensi\Kota;
use App\Models\Referensi\Provinsi;
use App\Models\Referensi\MetodePembayaran;
use App\Models\Referensi\MetodePengiriman;
use App\Models\Referensi\Bank;
use App\Models\Notifikasi;
use App\Models\Gudang\Gudang;

class AdminController extends Controller
{
    public function beranda_admin()
    {
        $general['title']       = "Dashboard";
        $general['menu1']       = "Beranda";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 11;

        $pelanggan  = DB::table('tmst_pelanggan')->join('users', 'users.id', '=', 'tmst_pelanggan.user_id')->where('users.is_aktif', 1)->get();
        $produk     = DB::table('tmst_produk')->where('is_aktif', 1)->get();
        $total_stok = DB::select('SELECT SUM(stok) as stok FROM tran_produk_serial_number');
        $sales_order_bulan_ini = DB::select("SELECT * FROM tran_so_header WHERE DATE_FORMAT(tanggal, '%Y-%m') = DATE_FORMAT(NOW(), '%Y-%m')");

        $data['pelanggan_aktif']        = count($pelanggan);
        $data['produk_aktif']           = count($produk);
        $data['stok_tersedia']          = $total_stok[0]->stok;
        $data['sales_order_bulan_ini']  = count($sales_order_bulan_ini);

        $profil = DB::table('tmst_perusahaan')->select('tmst_perusahaan.*', 'tref_kota.nama as nama_kota')->join('tref_kota', 'tref_kota.id', '=', 'tmst_perusahaan.kota_id')->first();
        $cabang = DB::table('tmst_cabang_perusahaan')->select('tmst_cabang_perusahaan.*', 'tref_kota.nama as nama_kota')->join('tref_kota', 'tref_kota.id', '=', 'tmst_cabang_perusahaan.kota_id')->get();

        return view('pages.admin.beranda_admin', compact('general', 'data', 'profil', 'cabang'));
    }

    public function pengaturan_umum()
    {
        $general['title']       = "Pengaturan Umum";
        $general['menu1']       = "Pengaturan";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 25;

        $nomor_invoice      = DB::table('tref_nomor_invoice')->get();
        $metode_pembayaran  = MetodePembayaran::all();
        $metode_pengiriman  = MetodePengiriman::all();
        $bank               = Bank::all();
        $gudang             = Gudang::where('is_aktif', 1)->get();
        $syarat_ketentuan   = DB::table('tref_syarat_ketentuan')->get();

        return view('pages.admin.data_referensi', compact('general', 'nomor_invoice', 'metode_pembayaran', 'metode_pengiriman', 'bank', 'gudang', 'syarat_ketentuan'));
    }

    public function tambah_bank(Request $request)
    {
        $this->validate($request, [
            'kode' => 'required',
            'nama' => 'required',
            'nama_singkat' => 'required',
        ]);

        $bank = new Bank;

        $bank->kode         = Input::get('kode');
        $bank->nama         = ucwords(Input::get('nama'));
        $bank->nama_singkat = ucwords(Input::get('nama_singkat'));
        $bank->keterangan   = ucwords(Input::get('keterangan'));
        $bank->is_aktif     = 1;

        $fileName = "";
        
        if (Input::hasFile('file_gambar')) 
        {
            $destinationPath    = base_path().'/public/img/ref/';
            $extension          = Input::file('file_gambar')->getClientOriginalExtension();
            $file_gambar        = "BANK".$this->generate_kode(8);
            $fileName           = $file_gambar.'.'.$extension;

            Input::file('file_gambar')->move($destinationPath, $fileName);
        }

        $bank->file_gambar = $fileName;
        $bank->save();

        $request->session()->flash('message4', 'Bank berhasil ditambahkan');
        return redirect('/pengaturan_umum');
    }

    public function ubah_status_bank($id,Request $request)
    {
        $bank = DB::table('tref_bank')->where('id', $id)->first();

        if($bank->is_aktif == 1)
        {
            $is_aktif = 0;
        }
        else
        {
            $is_aktif = 1;
        }

        DB::table('tref_bank')->where('id', $id)->update(['is_aktif' => $is_aktif]);

        $request->session()->flash('message1', 'Status bank berhasil diperbarui');
        return redirect('/pengaturan_umum');
    }

    public function generate_kode($index)
    {
        $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $res = "";
        for ($i = 0; $i < $index; $i++) {
            $res .= $chars[mt_rand(0, strlen($chars)-1)];
        }
        return $res;
    }

    public function tambah_metode_pembayaran(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'jenis' => 'required',
        ]);

        $metode_pembayaran = new MetodePembayaran;

        $metode_pembayaran->nama        = ucwords(Input::get('nama'));
        $metode_pembayaran->jenis       = Input::get('jenis');
        $metode_pembayaran->keterangan  = ucwords(Input::get('keterangan'));
        $metode_pembayaran->is_aktif    = 1;

        $fileName = "";
        
        if (Input::hasFile('file_gambar')) 
        {
            $destinationPath    = base_path().'/public/img/ref/';
            $extension          = Input::file('file_gambar')->getClientOriginalExtension();
            $file_gambar        = "BAYAR".$this->generate_kode(8);
            $fileName           = $file_gambar.'.'.$extension;

            Input::file('file_gambar')->move($destinationPath, $fileName);
        }

        $metode_pembayaran->file_gambar = $fileName;
        $metode_pembayaran->save();

        $request->session()->flash('message2', 'Metode pembayaran berhasil ditambahkan');
        return redirect('/pengaturan_umum');
    }

    public function ubah_metode_pembayaran(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'jenis' => 'required',
        ]);

        $id             = Input::get('id');
        $nama           = ucwords(Input::get('nama'));
        $jenis          = Input::get('jenis');
        $keterangan     = ucwords(Input::get('keterangan'));

        DB::table('tref_metode_pembayaran')->where('id', $id)->update(['nama' => $nama, 'jenis' => $jenis, 'keterangan' => $keterangan]);

        $request->session()->flash('message2', 'Metode pembayaran berhasil diperbarui');
        return redirect('/pengaturan_umum');
    }

    public function ubah_status_metode_pembayaran($id,Request $request)
    {
        $metode_pembayaran = DB::table('tref_metode_pembayaran')->where('id', $id)->first();

        if($metode_pembayaran->is_aktif == 1)
        {
            $is_aktif = 0;
        }
        else
        {
            $is_aktif = 1;
        }

        DB::table('tref_metode_pembayaran')->where('id', $id)->update(['is_aktif' => $is_aktif]);

        $request->session()->flash('message2', 'Status metode pembayaran berhasil diperbarui');
        return redirect('/pengaturan_umum');
    }

    public function ubah_nomor_invoice(Request $request)
    {
        $id     = Input::get('id');
        $format = strtoupper(Input::get('format'));

        DB::table('tref_nomor_invoice')->where('id', $id)->update(['format' => $format]);

        $request->session()->flash('message1', 'Format Nomor berhasil diperbarui');
        return redirect('/pengaturan_umum');
    }

    public function perusahaan()
    {
        $general['title']       = "Perusahaan";
        $general['menu1']       = "Beranda";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 12;

        $perusahaan = Perusahaan::with(['kota' => function($query) 
            { return $query->with('provinsi'); }, 'cabang' => function($query) 
            { return $query->with(['kota' => function($query) 
            { return $query->with('provinsi'); }]); }])->first();

        $provinsi   = Provinsi::all();
        
        return view('pages.admin.perusahaan', compact('general', 'perusahaan', 'provinsi'));
    }

    public function ubah_perusahaan(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'alamat' => 'required',
            'provinsi' => 'required',
            'kota' => 'required',
            'telp' => 'required',
            'kontak_person' => 'required',
            'email' => 'required',
        ]);

        $nama               = ucwords(Input::get('nama'));
        $alamat             = ucwords(Input::get('alamat'));
        $kota_id            = Input::get('kota');
        $telp               = Input::get('telp');
        $telp2              = Input::get('telp2');
        $kontak_person      = Input::get('kontak_person');
        $bidang_usaha       = ucwords(Input::get('bidang_usaha'));
        $jam_operasional    = Input::get('jam_operasional');
        $email              = Input::get('email');

        $telp   = str_replace("_", "", $telp);
        $telp2  = str_replace("_", "", $telp2);

        $perusahaan = Perusahaan::first();

        $perusahaan->nama               = $nama;
        $perusahaan->alamat             = $alamat;
        $perusahaan->kota_id            = $kota_id;
        $perusahaan->telp               = $telp;
        $perusahaan->telp2              = $telp2;
        $perusahaan->kontak_person      = $kontak_person;
        $perusahaan->bidang_usaha       = $bidang_usaha;
        $perusahaan->jam_operasional    = $jam_operasional;
        $perusahaan->email              = $email;
        $perusahaan->save();
        
        $request->session()->flash('message1', 'Perusahaan berhasil diperbarui');
        return redirect('/perusahaan');
    }

    public function tambah_cabang(Request $request)
    {
        $this->validate($request, [
            'alamat_cabang' => 'required',
            'provinsi2' => 'required',
            'kota_id2' => 'required',
            'telp_cabang' => 'required',
            'kontak_person_cabang' => 'required',
        ]);

        $perusahaan_id  = Input::get('perusahaan_id');
        $nama           = ucwords(Input::get('nama_cabang'));
        $alamat         = ucwords(Input::get('alamat_cabang'));
        $kota_id        = Input::get('kota_id2');
        $telp           = Input::get('telp_cabang');
        $kontak_person  = ucwords(Input::get('kontak_person_cabang'));

        $telp   = str_replace("_", "", $telp);

        $cabang = new Cabang;

        $cabang->nama           = $nama;
        $cabang->alamat         = $alamat;
        $cabang->kota_id        = $kota_id;
        $cabang->telp           = $telp;
        $cabang->kontak_person  = $kontak_person;
        $cabang->perusahaan_id  = $perusahaan_id;
        $cabang->save();

        $request->session()->flash('message2', 'Cabang berhasil ditambah');
        return redirect('/perusahaan');
    }

    public function ubah_cabang(Request $request)
    {
        $this->validate($request, [
            'ubah_alamat_cabang' => 'required',
            'kota_id3' => 'required',
            'provinsi3' => 'required',
            'ubah_telp_cabang' => 'required',
            'ubah_kontak_person_cabang' => 'required',
        ]);

        $perusahaan_id  = Input::get('perusahaan_id');
        $nama           = ucwords(Input::get('ubah_nama_cabang'));
        $id_cabang      = Input::get('ubah_id_cabang');
        $alamat         = ucwords(Input::get('ubah_alamat_cabang'));
        $kota_id        = Input::get('kota_id3');
        $telp           = Input::get('ubah_telp_cabang');
        $kontak_person  = ucwords(Input::get('ubah_kontak_person_cabang'));

        $telp   = str_replace("_", "", $telp);

        $cabang = Cabang::find($id_cabang);

        $cabang->nama           = $nama;
        $cabang->alamat         = $alamat;
        $cabang->kota_id        = $kota_id;
        $cabang->telp           = $telp;
        $cabang->kontak_person  = $kontak_person;
        $cabang->perusahaan_id  = $perusahaan_id;
        $cabang->save();

        $request->session()->flash('message2', 'Data cabang berhasil diperbarui');
        return redirect('/perusahaan');
    }

    public function hapus_cabang(Request $request)
    {
        $id = Input::get('id_cabang');

        DB::table('tmst_cabang_perusahaan')->where('id', $id)->delete();

        $request->session()->flash('message2', 'Data cabang berhasil dihapus');
        return redirect('/perusahaan');
    }

    public function ubah_status_metode_pengiriman($id, Request $request)
    {
        $metode_pengiriman = MetodePengiriman::where('id', $id)->first();

        if($metode_pengiriman->is_aktif == 1)
        {
            $is_aktif = 0;
        }
        else
        {
            $is_aktif = 1;
        }

        $metode_pengiriman->is_aktif = $is_aktif;
        $metode_pengiriman->save();

        $request->session()->flash('message3', 'Status metode pengiriman berhasil diperbarui');
        return redirect('/pengaturan_umum');
    }

    public function tambah_metode_pengiriman(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
        ]);

        $nama           = ucwords(Input::get('nama'));
        $tarif_dasar    = Input::get('tarif_dasar');
        $keterangan     = ucwords(Input::get('keterangan'));

        $tarif_dasar = str_replace('.', '', $tarif_dasar);

        $metode_pengiriman = new MetodePengiriman;

        $metode_pengiriman->nama        = $nama;
        $metode_pengiriman->tarif_dasar = $tarif_dasar;
        $metode_pengiriman->keterangan  = $keterangan;
        $metode_pengiriman->is_aktif    = 1;

        $fileName = "";
        
        if (Input::hasFile('file_gambar')) 
        {
            $destinationPath    = base_path().'/public/img/ref/';
            $extension          = Input::file('file_gambar')->getClientOriginalExtension();
            $file_gambar        = "KIRIM".$this->generate_kode(8);
            $fileName           = $file_gambar.'.'.$extension;

            Input::file('file_gambar')->move($destinationPath, $fileName);
        }

        $metode_pengiriman->file_gambar = $fileName;
        $metode_pengiriman->save();

        $request->session()->flash('message3', 'Metode pembayaran berhasil ditambahkan');
        return redirect('/pengaturan_umum');
    }

    public function ubah_metode_pengiriman(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
        ]);

        $id             = Input::get('id');
        $nama           = ucwords(Input::get('nama'));
        $tarif_dasar    = Input::get('tarif_dasar');
        $keterangan     = ucwords(Input::get('keterangan'));

        $tarif_dasar = str_replace('.', '', $tarif_dasar);

        $metode_pengiriman = MetodePengiriman::where('id', $id)->first();

        $metode_pengiriman->nama        = $nama;
        $metode_pengiriman->tarif_dasar = $tarif_dasar;
        $metode_pengiriman->keterangan  = $keterangan;
        $metode_pengiriman->save();        

        $request->session()->flash('message3', 'Metode pengiriman berhasil diperbarui');
        return redirect('/pengaturan_umum');
    }

    public function ubah_bank(Request $request)
    {
        $this->validate($request, [
            'kode' => 'required',
            'nama' => 'required',
            'nama_singkat' => 'required',
        ]);

        $id             = Input::get('id');

        $bank = Bank::where('id', $id)->first();

        $bank->kode         = Input::get('kode');
        $bank->nama         = ucwords(Input::get('nama'));
        $bank->nama_singkat = ucwords(Input::get('nama_singkat'));
        $bank->keterangan   = ucwords(Input::get('keterangan'));

        if (Input::hasFile('file_gambar')) 
        {
            $destinationPath    = base_path().'/public/img/ref/';
            $extension          = Input::file('file_gambar')->getClientOriginalExtension();
            $file_gambar        = "BANK".$this->generate_kode(8);
            $fileName           = $file_gambar.'.'.$extension;

            Input::file('file_gambar')->move($destinationPath, $fileName);

            //hapus gambar yang lama
            File::Delete(base_path().'/public/img/banner/'.$bank->file_gambar);
            //simpan yang baru
            $bank->file_gambar = $fileName;
        }

        $bank->save();        

        $request->session()->flash('message4', 'Bank berhasil diperbarui');
        return redirect('/pengaturan_umum');
    }

    public function nonaktif_notifikasi()
    {
        $id = Input::get('id');

        $notifikasi = Notifikasi::where('id', $id)->first();
        $notifikasi->is_aktif = 0;
        $notifikasi->save();
    }
}
