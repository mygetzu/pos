<?php

namespace App\Http\Controllers\Laporan;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Gloudemans\Shoppingcart\Facades\Cart;
use Input;
use Auth;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Session;
use Promocodes;
use Response;
use DateTime;
use PDF;
use Hashids\Hashids;
use Validator;
use Illuminate\Http\RedirectResponse;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use App\User;
use App\Models\Produk\KategoriProduk;
use App\Models\TransaksiPembelian\BayarPembelian;
use App\Models\TransaksiPembelian\NotaBeli;
use App\Models\TransaksiPembelian\RBHeader;
use App\Models\TransaksiPembelian\RBDetail;
use App\Models\TransaksiPembelian\SJMasukHeader;
use App\Models\TransaksiPembelian\SJMasukDetail;
use App\Models\TransaksiPenjualan\BayarPenjualan;
use App\Models\TransaksiPenjualan\NotaJual;
use App\Models\TransaksiPenjualan\RJHeader;
use App\Models\TransaksiPenjualan\RJDetail;
use App\Models\TransaksiPenjualan\SJKeluarHeader;
use App\Models\TransaksiPenjualan\SJKeluarDetail;
use App\Models\Gudang\Gudang;
use App\Models\Gudang\ProdukGudang;
use App\Models\Laporan\LaporanStokHeader;
use App\Models\Laporan\LaporanStokDetail;

class LaporanStokController extends Controller {

    public function laporan_stok() {
        $general['title'] = "Laporan Stok";
        $general['menu1'] = "Laporan";
        $general['menu2'] = $general['title'];
        $general['kode_menu'] = 71;

        $gudang = Gudang::where('is_aktif', 1)->get();

        return view('pages.laporan.laporan_stok', compact('general', 'gudang'));
    }

    public function do_tambah_laporan_stok(Request $request) {
        $this->validate($request, [
            'gudang_id' => 'required',
            'tanggal' => 'required',
        ]);

        $gudang_id = Input::get('gudang_id');
        $tanggal = Input::get('tanggal');
        $tanggal = new DateTime($tanggal);


        $laporan_stok_header = new LaporanStokHeader;
        $laporan_stok_header->gudang_id = $gudang_id;
        $laporan_stok_header->tanggal = $tanggal;
        $laporan_stok_header->save();

        $id = $laporan_stok_header->id;
        $total_stok = 0;

        $tanggal = $tanggal->format('Y-m-d H:i:s');
        //cek stok produk dari po, so, rb, rj
        //laporan stok berdasarkan surat jalan
        //dapatkan nilai nota beli dari tanggal awal sampai tanggal dipilih
        $sj_masuk_header = SJMasukHeader::where('tanggal', '<=', $tanggal)->orderBy('tanggal', 'asc')->get();

        foreach ($sj_masuk_header as $key => $value) {
            $sj_masuk_detail = SJMasukDetail::where('sj_masuk_header_id', $value->id)->get();

            foreach ($sj_masuk_detail as $key2 => $value2) {
                //cek jika gudang sama maka simpan
                if ($value2->gudang_id == $gudang_id && $value2->jenis_barang_id == 1) { //sementara hanya produk saja
                    //cek apakah di laporan_stok_detail sudah ada
                    $laporan_stok_detail = LaporanStokDetail::where('laporan_stok_header_id', $id)->where('produk_id', $value2->produk_id)->first();

                    if (empty($laporan_stok_detail)) {
                        //jika belum, buat baru
                        $laporan_stok_detail = new LaporanStokDetail;
                        $laporan_stok_detail->laporan_stok_header_id = $id;
                        $laporan_stok_detail->produk_id = $value2->produk_id;
                        $laporan_stok_detail->jumlah = $value2->jumlah;
                        $laporan_stok_detail->save();

                        $total_stok = $total_stok + $value2->jumlah;
                    } else {
                        //jika sudah, update jumlah dan harga
                        $jumlah = (int) $laporan_stok_detail->jumlah + (int) $value2->jumlah;

                        $laporan_stok_detail->jumlah = $jumlah;
                        $laporan_stok_detail->save();

                        $total_stok = $total_stok + $value2->jumlah;
                    }
                }
            }
        }

        //dapatkan nilai nota jual dari tanggal awal sampai tanggal dipilih
        $sj_keluar_header = SJKeluarHeader::where('tanggal', '<=', $tanggal)->orderBy('tanggal', 'asc')->get();

        foreach ($sj_keluar_header as $key => $value) {
            $sj_keluar_detail = SJKeluarDetail::where('sj_keluar_header_id', $value->id)->get(); //sementara pake no_nota, nanti diganti id

            foreach ($sj_keluar_detail as $key2 => $value2) {
                //cek jika gudang sama maka simpan
                if ($value2->gudang_id == $gudang_id && $value2->jenis_barang_id == 1) { //sementara hanya produk saja
                    //kurangi jumlah stok
                    $laporan_stok_detail = LaporanStokDetail::where('laporan_stok_header_id', $id)->where('produk_id', $value2->produk_id)->first();

                    if (empty($laporan_stok_detail)) {
                        //jika belum, buat baru
                        $laporan_stok_detail = new LaporanStokDetail;
                        $laporan_stok_detail->laporan_stok_header_id = $id;
                        $laporan_stok_detail->produk_id = $value2->produk_id;
                        $laporan_stok_detail->jumlah = $value2->jumlah;
                        $laporan_stok_detail->save();

                        $total_stok = $total_stok + $value2->jumlah;
                    } else {
                        //jika sudah, update jumlah dan harga
                        $jumlah = (int) $laporan_stok_detail->jumlah - (int) $value2->jumlah;

                        $laporan_stok_detail->jumlah = $jumlah;
                        $laporan_stok_detail->save();

                        $total_stok = $total_stok - $value2->jumlah;
                    }
                }
            }
        }

        //dapatkan nilai retur jual dari tanggal awal sampai tanggal dipilih
        $rj_header = RJHeader::where('tanggal', '<=', $tanggal)->orderBy('tanggal', 'asc')->get();

        foreach ($rj_header as $key => $value) {
            $rj_detail = RJDetail::where('rj_header_id', $value->id)->get();

            foreach ($rj_detail as $key2 => $value2) {
                //cek jika gudang sama maka simpan
                if ($value2->gudang_id == $gudang_id && $value2->jenis_barang_id == 1) { //sementara hanya produk saja
                    //kurangi jumlah stok
                    $laporan_stok_detail = LaporanStokDetail::where('laporan_stok_header_id', $id)->where('produk_id', $value2->produk_id)->first();

                    $jumlah = (int) $laporan_stok_detail->jumlah + (int) $value2->jumlah;

                    $laporan_stok_detail->jumlah = $jumlah;
                    $laporan_stok_detail->save();

                    $total_stok = $total_stok + $value2->jumlah;
                }
            }
        }

        //dapatkan nilai retur jual dari tanggal awal sampai tanggal dipilih
        $rb_header = RBHeader::where('tanggal', '<=', $tanggal)->orderBy('tanggal', 'asc')->get();

        foreach ($rb_header as $key => $value) {
            $rb_detail = RBDetail::where('rb_header_id', $value->id)->get();

            foreach ($rb_detail as $key2 => $value2) {
                //cek jika gudang sama maka simpan
                if ($value2->gudang_id == $gudang_id && $value2->jenis_barang_id == 1) { //sementara hanya produk saja
                    //kurangi jumlah stok
                    $laporan_stok_detail = LaporanStokDetail::where('laporan_stok_header_id', $id)->where('produk_id', $value2->produk_id)->first();
//                    DB::table()->where('id', $value2->produk_id)->value('serial_number');
//                    $produk[$value]
                    $jumlah = (int) $laporan_stok_detail->jumlah - (int) $value2->jumlah;

                    $laporan_stok_detail->jumlah = $jumlah;
                    $laporan_stok_detail->save();

                    $total_stok = $total_stok - $value2->jumlah;
                }
            }
        }

        $laporan_stok_header = LaporanStokHeader::with('gudang')->where('id', $id)->first();
        $laporan_stok_header->total_stok = $total_stok;
        $laporan_stok_header->save();

        $laporan_stok_detail = LaporanStokDetail::with('produk')->where('laporan_stok_header_id', $id)->get();
        $get_stok_detail = DB::table('tran_laporan_stok_detail')->where('laporan_stok_header_id', $id)->get();
        $index_produk = 0;
        foreach ($get_stok_detail as $r_stok) {
            $produk_id = $r_stok->produk_id;
//            if($produk_id_new == $produk_id){
//                $index_produk = $index_produk;
//            }
            $index_serial = 0;
            $get_serial = DB::table('tran_produk_serial_number')->where('produk_id', $produk_id)->where('gudang_id', $gudang_id)->where('stok', '>', 0)->get();
            foreach ($get_serial as $r_serial) {
                $laporan_stok_detail[$index_produk][$index_serial] = $r_serial->serial_number;
                $index_serial++;
            }
//            $produk_id_new = $produk_id;
            $index_produk++;
        }
        return view('pages.laporan.ajax_laporan_stok', compact('laporan_stok_header', 'laporan_stok_detail', 'index_serial', 'produk_id'));
    }

    public function laporan_stok_cetak($id) {
        $laporan_stok_header = LaporanStokHeader::with('gudang')->where('id', $id)->first();
        $laporan_stok_detail = LaporanStokDetail::with('produk')->where('laporan_stok_header_id', $id)->get();

        date_default_timezone_set("Asia/Jakarta");
        $print_time = new DateTime;

        return view('pages.laporan.laporan_stok_cetak', compact('laporan_stok_header', 'laporan_stok_detail', 'print_time'));
    }

    public function laporan_stok_pdf($id) {
        $laporan_stok_header = LaporanStokHeader::with('gudang')->where('id', $id)->first();
        $laporan_stok_detail = LaporanStokDetail::with('produk')->where('laporan_stok_header_id', $id)->get();

        date_default_timezone_set("Asia/Jakarta");
        $print_time = new DateTime;

        $pdf = PDF::loadView('pages.laporan.laporan_stok_cetak', compact('laporan_stok_header', 'laporan_stok_detail', 'print_time'))->setPaper('a4', 'portrait');
        return $pdf->stream();
    }

}
