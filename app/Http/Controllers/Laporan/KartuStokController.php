<?php

namespace App\Http\Controllers\Laporan;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Gloudemans\Shoppingcart\Facades\Cart;
use Input;
use Auth;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Session;
use Promocodes;
use Response;
use DateTime;
use PDF;
use Hashids\Hashids;
use Validator;
use Illuminate\Http\RedirectResponse;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use App\User;
use App\Models\Gudang\Gudang;
use App\Models\Gudang\ProdukGudang;
use App\Models\Produk\KategoriProduk;
use App\Models\Laporan\KartuStokHeader;
use App\Models\Laporan\KartuStokDetail;
use App\Models\TransaksiPembelian\BayarPembelian;
use App\Models\TransaksiPembelian\NotaBeli;
use App\Models\TransaksiPembelian\RBHeader;
use App\Models\TransaksiPembelian\RBDetail;
use App\Models\TransaksiPembelian\SJMasukHeader;
use App\Models\TransaksiPembelian\SJMasukDetail;
use App\Models\TransaksiPenjualan\BayarPenjualan;
use App\Models\TransaksiPenjualan\NotaJual;
use App\Models\TransaksiPenjualan\RJHeader;
use App\Models\TransaksiPenjualan\RJDetail;
use App\Models\TransaksiPenjualan\SJKeluarHeader;
use App\Models\TransaksiPenjualan\SJKeluarDetail;

class KartuStokController extends Controller
{
    public function kartu_stok()
    {
        $general['title']       = "Kartu Stok";
        $general['menu1']       = "Laporan";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 72;

        $gudang = Gudang::where('is_aktif', 1)->get();

        return view('pages.laporan.kartu_stok', compact('general', 'gudang'));
    }

    public function kartu_stok_produk_gudang()
    {
        $gudang_id          = Input::get('gudang_id');
        $my_kategori_produk = Input::get('kategori_produk_id');

        if(!empty($my_kategori_produk)){
            $produk_gudang   = ProdukGudang::with('produk')->join('tmst_produk', 'tmst_produk.id', '=', 'tran_produk_gudang.produk_id')->where('gudang_id', $gudang_id)->where('kategori_id', $my_kategori_produk)->get();
        }
        else{
            $produk_gudang   = ProdukGudang::with('produk')->where('gudang_id', $gudang_id)->get();
        }

        $kategori_produk = KategoriProduk::where('is_aktif', 1)->get();

        return view('pages.laporan.ajax_kartu_stok_produk_gudang', compact('produk_gudang', 'kategori_produk', 'my_kategori_produk'));
    }

    public function do_tambah_kartu_stok(Request $request)
    {
        $this->validate($request, [
            'gudang_id' => 'required',
            'produk_id' => 'required',
            'tanggal_awal' => 'required',
            'tanggal_akhir' => 'required',
        ]);
        
        $gudang_id      = Input::get("gudang_id");
        $produk_id      = Input::get("produk_id");
        $tanggal_awal   = Input::get("tanggal_awal");
        $tanggal_akhir  = Input::get("tanggal_akhir");

        $tanggal_awal   = new DateTime($tanggal_awal);
        $tanggal_akhir  = new DateTime($tanggal_akhir);

        $kartu_stok_header = new KartuStokHeader;
        $kartu_stok_header->gudang_id       = $gudang_id;
        $kartu_stok_header->produk_id       = $produk_id;
        $kartu_stok_header->tanggal_awal    = $tanggal_awal;
        $kartu_stok_header->tanggal_akhir   = $tanggal_akhir;
        $kartu_stok_header->save();

        $id             = $kartu_stok_header->id;
        $tanggal_awal   = $tanggal_awal->format("Y-m-d 0:0:0");
        $tanggal_akhir  = $tanggal_akhir->format("Y-m-d 24:0:0");
        $total_stok     = 0;

        //kartu stok dihitung sejak surat jalan
        $sj_masuk_header = SJMasukHeader::with('supplier')->where('tanggal', '>=', $tanggal_awal)->where('tanggal', '<=', $tanggal_akhir)->orderBy('tanggal', 'asc')->get();

        foreach ($sj_masuk_header as $key => $value) {
            $sj_masuk_detail = SJMasukDetail::where('sj_masuk_header_id', $value->id)->where('gudang_id', $gudang_id)->where('produk_id', $produk_id)->where('jenis_barang_id', 1)->get(); //sementara pake no_nota, nanti diganti id

            if(!empty($sj_masuk_header)) { //yang disimpan hanya yang memiliki surat jalan
                foreach ($sj_masuk_detail as $key2 => $value2) {
                    
                    $kartu_stok_detail = new KartuStokDetail;
                    $kartu_stok_detail->kartu_stok_header_id        = $id;
                    $kartu_stok_detail->sumber_data_id              = 1;
                    $kartu_stok_detail->supplier_or_pelanggan       = 'Supplier : '.$value->supplier->nama;
                    $kartu_stok_detail->no_nota                     = $value->no_surat_jalan;
                    $kartu_stok_detail->serial_number               = $value2->serial_number;
                    $kartu_stok_detail->tanggal                     = $value->tanggal;
                    $kartu_stok_detail->jumlah                      = $value2->jumlah;
                    $kartu_stok_detail->harga                       = $value2->harga;
                    $kartu_stok_detail->save();

                    $total_stok = $total_stok + $value2->jumlah;
                }
            }
        }

        

        //dapatkan nilai nota jual dari tanggal awal dipilih sampai tanggal akhir dipilih
        $sj_keluar_header = SJKeluarHeader::with('pelanggan')->where('tanggal', '>=', $tanggal_awal)->where('tanggal', '<=', $tanggal_akhir)->orderBy('tanggal', 'asc')->get();

        foreach ($sj_keluar_header as $key => $value) {
            $sj_keluar_detail = SJKeluarDetail::where('sj_keluar_header_id', $value->id)->where('gudang_id', $gudang_id)->where('produk_id', $produk_id)->where('jenis_barang_id', 1)->get(); 

            if(!empty($sj_keluar_header)) { //yang disimpan hanya yang memiliki packing list
                foreach ($sj_keluar_detail as $key2 => $value2) {
                    
                    $kartu_stok_detail = new KartuStokDetail;
                    $kartu_stok_detail->kartu_stok_header_id    = $id;
                    $kartu_stok_detail->sumber_data_id          = 2;
                    $kartu_stok_detail->supplier_or_pelanggan   = 'Pelanggan : '.$value->pelanggan->nama;
                    $kartu_stok_detail->no_nota                 = $value->no_surat_jalan;
                    $kartu_stok_detail->serial_number           = $value2->serial_number;
                    $kartu_stok_detail->tanggal                 = $value->tanggal;
                    $kartu_stok_detail->jumlah                  = $value2->jumlah;
                    $kartu_stok_detail->harga                   = $value2->harga;
                    $kartu_stok_detail->save();

                    $total_stok = $total_stok - $value2->jumlah;
                }
            }
        }

        //dapatkan nilai retur jual dari tanggal awal dipilih sampai tanggal akhir dipilih
        $rj_header = RJHeader::with('pelanggan')->where('tanggal', '>=', $tanggal_awal)->where('tanggal', '<=', $tanggal_akhir)->orderBy('tanggal', 'asc')->get();

        foreach ($rj_header as $key => $value) {
            $rj_detail = RJDetail::where('rj_header_id', $value->id)->where('gudang_id', $gudang_id)->where('produk_id', $produk_id)->where('jenis_barang_id', 1)->get(); //sementara pake no_nota, nanti diganti id

            foreach ($rj_detail as $key2 => $value2) {
                $kartu_stok_detail = new KartuStokDetail;
                $kartu_stok_detail->kartu_stok_header_id    = $id;
                $kartu_stok_detail->sumber_data_id          = 3;
                $kartu_stok_detail->supplier_or_pelanggan   = 'Pelanggan : '.$value->pelanggan->nama;
                $kartu_stok_detail->no_nota                 = $value2->no_nota; //get nota retur jual
                $kartu_stok_detail->serial_number           = $value2->serial_number;
                $kartu_stok_detail->tanggal                 = $value->tanggal;
                $kartu_stok_detail->jumlah                  = $value2->jumlah;
                $kartu_stok_detail->harga                   = $value2->harga;
                $kartu_stok_detail->save();

                $total_stok = $total_stok + $value2->jumlah;
            }
        }

        //dapatkan nilai retur beli dari tanggal awal dipilih sampai tanggal akhir dipilih
        $rb_header = RBHeader::with('supplier')->where('tanggal', '>=', $tanggal_awal)->where('tanggal', '<=', $tanggal_akhir)->orderBy('tanggal', 'asc')->get();

        foreach ($rb_header as $key => $value) {
            $rb_detail = RBDetail::where('rb_header_id', $value->id)->where('gudang_id', $gudang_id)->where('produk_id', $produk_id)->where('jenis_barang_id', 1)->get(); //sementara pake no_nota, nanti diganti id

            foreach ($rb_detail as $key2 => $value2) {
                $kartu_stok_detail = new KartuStokDetail;
                $kartu_stok_detail->kartu_stok_header_id    = $id;
                $kartu_stok_detail->sumber_data_id          = 4;
                $kartu_stok_detail->supplier_or_pelanggan   = 'Supplier : '.$value->supplier->nama;
                $kartu_stok_detail->no_nota                 = $value2->no_nota; //get nota retur jual
                $kartu_stok_detail->serial_number           = $value2->serial_number;
                $kartu_stok_detail->tanggal                 = $value->tanggal;
                $kartu_stok_detail->jumlah                  = $value2->jumlah;
                $kartu_stok_detail->harga                   = $value2->harga;
                $kartu_stok_detail->save();

                $total_stok = $total_stok - $value2->jumlah;
            }
        }


        $kartu_stok_header = KartuStokHeader::with('gudang', 'produk')->where('id', $id)->first();
        $kartu_stok_header->total_stok = $total_stok;
        $kartu_stok_header->save();

        $kartu_stok_detail = KartuStokDetail::where('kartu_stok_header_id', $id)->get();

        return view('pages.laporan.ajax_kartu_stok_detail_stok', compact('kartu_stok_header', 'kartu_stok_detail'));
    }

    public function kartu_stok_cetak($id)
    {
        $kartu_stok_header = KartuStokHeader::with('gudang', 'produk')->where('id', $id)->first();
        $kartu_stok_detail = KartuStokDetail::where('kartu_stok_header_id', $id)->get();

        date_default_timezone_set("Asia/Jakarta"); 
        $print_time = new DateTime;

        return view('pages.laporan.kartu_stok_cetak', compact('kartu_stok_header', 'kartu_stok_detail', 'print_time'));
    }

    public function kartu_stok_pdf($id)
    {
        $kartu_stok_header = KartuStokHeader::with('gudang', 'produk')->where('id', $id)->first();
        $kartu_stok_detail = KartuStokDetail::where('kartu_stok_header_id', $id)->get();

        date_default_timezone_set("Asia/Jakarta"); 
        $print_time = new DateTime;

        $pdf = PDF::loadView('pages.laporan.kartu_stok_cetak', compact('kartu_stok_header', 'kartu_stok_detail', 'print_time'))->setPaper('a4', 'landscape');
        return $pdf->stream();
    }
}
