<?php

namespace App\Http\Controllers\Pelanggan;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Datatables;
use ArrayObject;
use Illuminate\Pagination\LengthAwarePaginator;
use Input;
use DateTime;
use Auth;
use Hashids\Hashids;
use Mail;
use App\Models\Produk\KategoriProduk;
use App\Models\Produk\Produk;
use App\Models\Pelanggan\ProdukHarga;
use Response;

class KategoriPelangganController extends Controller
{
    public function kategori_pelanggan()
    {
        $general['title']       = "Kategori Pelanggan";
        $general['menu1']       = "Akun dan Pelanggan";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 34;

        $kategori_pelanggan = DB::select("SELECT *, (SELECT COUNT(*) FROM tmst_pelanggan WHERE kategori_id=tmst_kategori_pelanggan.id ) AS jumlah,
        (SELECT COUNT(*) FROM tran_produk_harga WHERE kategori_pelanggan_id = tmst_kategori_pelanggan.id 
        AND CURDATE() >= tran_produk_harga.awal_periode AND CURDATE() <= tran_produk_harga.akhir_periode) AS jumlah_promo
        FROM tmst_kategori_pelanggan ORDER BY tmst_kategori_pelanggan.nama asc");

        return view('pages.pelanggan.kategori_pelanggan', compact('general', 'kategori_pelanggan'));
    }

    public function tambah_kategori_pelanggan()
    {
        $general['title']       = "Tambah Kategori Pelanggan";
        $general['menu1']       = "Akun dan Pelanggan";
        $general['menu2']       = "Kategori Pelanggan";
        $general['menu3']       = $general['title'];
        $general['kode_menu']   = 34;

        return view('pages.pelanggan.tambah_kategori_pelanggan', compact('general'));
    }

    public function do_tambah_kategori_pelanggan(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required|unique:tmst_kategori_pelanggan,nama',
            'deskripsi' => 'required',
            'default_diskon' => 'required'
        ]);

        $nama           = ucwords(Input::get('nama'));
        $deskripsi      = Input::get('deskripsi');
        $default_diskon = Input::get('default_diskon');

        DB::table('tmst_kategori_pelanggan')->insert(['nama' => $nama, 'deskripsi' => $deskripsi,
            'default_diskon' => $default_diskon]);

        $request->session()->flash('message', 'Kategori Pelanggan berhasil ditambah');
        return redirect('/kategori_pelanggan');
    }

    public function detail_kategori_pelanggan($id)
    {
        $kategori_pelanggan = DB::table('tmst_kategori_pelanggan')->where('id', $id)->first();

        $general['title']       = $kategori_pelanggan->nama;
        $general['menu1']       = "Akun dan Pelanggan";
        $general['menu2']       = "Kategori Pelanggan";
        $general['menu3']       = $general['title'];
        $general['kode_menu']   = 34;
        
        $pelanggan = DB::table('tmst_pelanggan')->select('tmst_pelanggan.*', 'tref_kota.nama as nama_kota')->join('tref_kota', 'tref_kota.id', '=', 'tmst_pelanggan.kota_id')->where('tmst_pelanggan.kategori_id', $id)->get();

        $produk_harga       = ProdukHarga::with(['produk' => function($query) 
            { return $query->with('produk_galeri_first');} ])->where('kategori_pelanggan_id', $id)->get();
        $kategori_produk    = KategoriProduk::where('is_aktif', 1)->orderBy('nama', 'asc')->get();

        $gambar_default = DB::table('static')->where('nama', 'gambar_produk_default')->first();

        return view('pages.pelanggan.detail_kategori_pelanggan', compact('general', 'kategori_pelanggan', 'pelanggan', 'produk_harga', 'kategori_produk', 'gambar_default'));
    }

    public function ubah_kategori_pelanggan(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'deskripsi' => 'required',
            'default_diskon' => 'required'
        ]);

        $id             = Input::get('id');
        $nama           = ucwords(Input::get('nama'));
        $deskripsi      = Input::get('deskripsi');
        $default_diskon = Input::get('default_diskon');

        DB::table('tmst_kategori_pelanggan')->where('id', $id)->update(['nama' => $nama, 'deskripsi' => $deskripsi,
            'default_diskon' => $default_diskon]);

        $request->session()->flash('message1', 'Kategori Pelanggan berhasil perbarui');

        return redirect('/detail_kategori_pelanggan/'.$id);
    }

    public function get_kategori_produk_harga_promo()
    {
        $kategori_produk_id = Input::get('kategori_produk_id');

        $produk = Produk::where('kategori_id', $kategori_produk_id)->where('is_aktif', 1)->get();

        echo "<option value=''></option>";
        foreach ($produk as $val) {
            echo "<option value=".$val->id.">".$val->nama."</option>";
        }
    }

    public function tambah_produk_harga(Request $request)
    {
        $this->validate($request, [
            'produk_id' => 'required',
            'diskon' => 'required',
            'awal_periode' => 'required|date|before:akhir_periode',
            'akhir_periode'  => 'required|date|after:awal_periode',
        ]);

        $kategori_pelanggan_id  = Input::get('kategori_pelanggan_id');
        $produk_id              = Input::get('produk_id');
        $diskon                 = Input::get('diskon');
        $awal_periode           = Input::get('awal_periode');
        $akhir_periode          = Input::get('akhir_periode');

        $awal_periode   = new DateTime($awal_periode);
        $akhir_periode  = new DateTime($akhir_periode);

        DB::table('tran_produk_harga')->insert(['produk_id' => $produk_id, 'kategori_pelanggan_id' => $kategori_pelanggan_id, 'diskon' => $diskon, 'awal_periode' => $awal_periode, 'akhir_periode' => $akhir_periode]);

        $request->session()->flash('message3', 'Produk Harga berhasil ditambah');
        return redirect('/detail_kategori_pelanggan/'.$kategori_pelanggan_id);
    }

    public function ubah_produk_harga(Request $request)
    {
        $this->validate($request, [
            'produk_harga_id' => 'required',
            'diskon' => 'required',
            'awal_periode' => 'required|date|before:akhir_periode',
            'akhir_periode'  => 'required|date|after:awal_periode',
        ]);

        $kategori_pelanggan_id  = Input::get('kategori_pelanggan_id');
        $produk_harga_id        = Input::get('produk_harga_id');
        $diskon                 = Input::get('diskon');
        $awal_periode           = Input::get('awal_periode');
        $akhir_periode          = Input::get('akhir_periode');

        $awal_periode   = new DateTime($awal_periode);
        $akhir_periode  = new DateTime($akhir_periode);

        $produk_harga = ProdukHarga::where('id', $produk_harga_id)->first();
        $produk_harga->diskon           = $diskon;
        $produk_harga->awal_periode     = $awal_periode;
        $produk_harga->akhir_periode    = $akhir_periode;
        $produk_harga->save();

        $request->session()->flash('message3', 'Produk Harga berhasil diperbarui');
        return redirect('/detail_kategori_pelanggan/'.$kategori_pelanggan_id);
    }

    public function hapus_produk_harga(Request $request)
    {
        $kategori_pelanggan_id  = Input::get('kategori_pelanggan_id');
        $produk_harga_id        = Input::get('produk_harga_id');

        $produk_harga = ProdukHarga::where('id', $produk_harga_id)->first();
        $produk_harga->delete();

        $request->session()->flash('message3', 'Produk Harga berhasil dihapus');
        return redirect('/detail_kategori_pelanggan/'.$kategori_pelanggan_id);
    }

    public function get_produk_harga()
    {
        $status = Input::get('status_promo');
        $id     = Input::get('kategori_pelanggan_id');

        if($status == '0')
        {
            $produk_harga       = ProdukHarga::with(['produk' => function($query) 
            { return $query->with('produk_galeri_first');} ])->where('kategori_pelanggan_id', $id)->get();
        }
        else if($status == '1')
        {
            $produk_harga       = ProdukHarga::with(['produk' => function($query) 
            { return $query->with('produk_galeri_first');} ])->where('kategori_pelanggan_id', $id)->where('tran_produk_harga.awal_periode', '<=', date('Y-m-d'))->where('tran_produk_harga.akhir_periode', '>=', date('Y-m-d'))->get();
        }
        else if($status == '2')
        {
            $produk_harga       = ProdukHarga::with(['produk' => function($query) 
            { return $query->with('produk_galeri_first');} ])->where('kategori_pelanggan_id', $id)
            ->where(function ($query) {
                $query->where('tran_produk_harga.awal_periode', '>', date('Y-m-d'))
                ->orWhere('tran_produk_harga.akhir_periode', '<', date('Y-m-d'));
            })->get();
        }

        $gambar_default = DB::table('static')->where('nama', 'gambar_produk_default')->first();

        return view('pages.pelanggan.ajax_produk_harga', compact('produk_harga', 'gambar_default'));
    }
}
