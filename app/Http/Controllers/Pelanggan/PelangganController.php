<?php

namespace App\Http\Controllers\Pelanggan;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Datatables;
use ArrayObject;
use Illuminate\Pagination\LengthAwarePaginator;
use Input;
use DateTime;
use Auth;
use Hashids\Hashids;
use Mail;
use App\Models\Pelanggan\Pelanggan;
use Response;
use App\Models\Produk\Produk;
use App\Models\Produk\Hadiah;
use App\Models\PaketProduk\Paket;
use App\Models\TransaksiPenjualan\SOHeader;
use App\Models\TransaksiPenjualan\SODetail;
use App\Models\TransaksiPenjualan\SOAlamatPengiriman;
use App\Models\TransaksiPenjualan\SOVoucher;
use App\Models\TransaksiPenjualan\SOPembayaran;
use App\Models\TransaksiPenjualan\InvoicePenjualan;
use App\Models\TransaksiPenjualan\BayarPenjualan;
use App\Models\TransaksiPenjualan\SJKeluarHeader;
use App\Models\TransaksiPenjualan\NotaJual;
use App\Models\Referensi\Bank;
use App\Models\Referensi\MetodePembayaran;
use Hash;

class PelangganController extends Controller
{
    public function pelanggan()
    {
        $general['title']       = "Pelanggan";
        $general['menu1']       = "Akun dan Pelanggan";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 35;        

        $pelanggan = DB::select("SELECT tmst_pelanggan.*, '0' AS kategori_pelanggan_id, '0' AS kategori_pelanggan_nama
            FROM tmst_pelanggan WHERE (tmst_pelanggan.kategori_id IS NULL OR tmst_pelanggan.kategori_id = '')
            UNION
            SELECT tmst_pelanggan.*, tmst_kategori_pelanggan.id AS kategori_pelanggan_id, tmst_kategori_pelanggan.nama AS kategori_pelanggan_nama 
            FROM tmst_pelanggan INNER JOIN tmst_kategori_pelanggan ON tmst_kategori_pelanggan.id = tmst_pelanggan.kategori_id
            ");

        $pelanggan = collect($pelanggan);

        $kategori_pelanggan = DB::table('tmst_kategori_pelanggan')->get();

        return view('pages.pelanggan.pelanggan', compact('general', 'pelanggan', 'kategori_pelanggan'));
    }

    public function tambah_kategori_untuk_pelanggan(Request $request)
    {
        $pelanggan_id = Input::get('add_pelanggan_id');
        $kategori_pelanggan_id = Input::get('kategori_pelanggan_id');
        
        if(empty($kategori_pelanggan_id))
        {
            $request->session()->flash('message_fail', 'Pilih Kategori Pelanggan terlebih dahulu');
            return redirect('/pelanggan');
        }

        DB::table('tmst_pelanggan')->where('id', $pelanggan_id)->update(['kategori_id' => $kategori_pelanggan_id]);

        $request->session()->flash('message', 'Kategori pelanggan berhasil ditambah');
        return redirect('/pelanggan');
    }

    public function ubah_kategori_untuk_pelanggan(Request $request)
    {
        $pelanggan_id = Input::get('ubah_pelanggan_id');
        $kategori_pelanggan_id = Input::get('kategori_pelanggan_id');

        if(empty($kategori_pelanggan_id))
        {
            $request->session()->flash('message_fail', 'Pilih Kategori Pelanggan terlebih dahulu');
            return redirect('/pelanggan');
        }

        DB::table('tmst_pelanggan')->where('id', $pelanggan_id)->update(['kategori_id' => $kategori_pelanggan_id]);

        $request->session()->flash('message', 'Kategori pelanggan berhasil diubah');
        return redirect('/pelanggan');
    }

    public function transaksi_pelanggan($id)
    {
        $general['title']       = "Riwayat Transaksi";
        $general['menu1']       = "Akun dan Pelanggan";
        $general['menu2']       = "Pelanggan";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 35;      
        
        $so_header = SOHeader::where('pelanggan_id', $id)->orderBy('tanggal', 'desc')->get();
        
        return view('pages.pelanggan.transaksi_pelanggan', compact('general', 'so_header'));
    }

    public function transaksi_pelanggan_detail($so_header_id)
    {
        $general['title']       = "Riwayat Transaksi Detail";
        $general['menu1']       = "Akun dan Pelanggan";
        $general['menu2']       = "Pelanggan";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 35;      

        $so_header = SOHeader::where('id', $so_header_id)->first();
        $so_detail = SODetail::with('produk', 'hadiah', 'paket')->where('so_header_id', $so_header_id)->get();

        $pelanggan_id = $so_header->pelanggan_id;
        

        return view('pages.pelanggan.transaksi_pelanggan_detail', compact('general', 'so_detail', 'so_header', 'pelanggan_id'));
    }

    public function akun_pelanggan()
    {
        $title      = "Akun Saya";
        $menu       = "Akun";

        $pelanggan = Pelanggan::where('user_id', Auth::user()->id)->first();

        $my_kota = DB::table('tref_kota')->select('tref_kota.nama', 'tref_kota.id as kota_id', 'tref_kota.provinsi_kode', 'tref_provinsi.id as provinsi_id')->join('tref_provinsi', 'tref_provinsi.kode', '=', 'tref_kota.provinsi_kode')->where('tref_kota.id', $pelanggan->kota_id)->first();
        $provinsi = DB::table('tref_provinsi')->get();
        $kota = DB::table('tref_kota')->get();
        
        return view('pages.home.akun_pelanggan', compact('title', 'menu', 'pelanggan', 'my_kota', 'provinsi', 'kota'));
    }

    public function ubah_akun_pelanggan(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'alamat' => 'required',
            'kode_pos' => 'required',
            'kota' => 'required',
            'provinsi' => 'required',
        ]);

        $id             = Input::get('id');
        $name           = Input::get('name');
        $email          = Input::get('email');
        $pelanggan_id  = Input::get('pelanggan_id');
        $alamat         = Input::get('alamat');
        $kode_pos       = Input::get('kode_pos');
        $kota_id           = Input::get('kota');
        $telp1          = Input::get('telp1');
        $telp2          = Input::get('telp2');
        $hp_sales       = Input::get('hp_sales');
        $deskripsi      = Input::get('deskripsi');
        
        DB::table('users')->where('id', $id)->update(['name' => $name, 'email' => $email, 'alamat' => $alamat,
        'kota_id' => $kota_id, 'telp' => $telp1, 'hp' => $hp_sales, 'kode_pos' => $kode_pos]);
        DB::table('tmst_pelanggan')->where('id', $pelanggan_id)->update(['alamat' => $alamat,
        'kota_id' => $kota_id, 'telp1' => $telp1, 'telp2' => $telp2, 'hp_sales' => $hp_sales, 'deskripsi' => $deskripsi, 'kode_pos' => $kode_pos]);
        
        $request->session()->flash('message', 'Akun berhasil diperbarui');
        return redirect('/akun_pelanggan');
    }

    public function password_pelanggan()
    {
        $title      = "Ubah Password";
        $menu      = "Pengaturan Akun";
        
        return view('pages.home.password_pelanggan', compact('title', 'menu'));
    }

    public function ubah_password_pelanggan(Request $request)
    {
        $this->validate($request, [
            'password_old' => 'required|min:6',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
        ]);

        $id    = Input::get('id');
        $pass_lama  = Input::get('password_old');
        $pass_baru  = Input::get('password');
        $pass_baru2 = Input::get('password_confirmation');

        $mypass = DB::table('users')->select('password')->where('id', $id)->first();

        $is_user = Hash::check($pass_lama, $mypass->password);
 
        if($pass_baru != $pass_baru2)
        {
            return $this->sendFailedLoginResponse($request);
        }
        else if($is_user == false)
        {
            $request->session()->flash('message_error', 'Isian Password Lama Salah');
            return redirect('/password_pelanggan');
        }
        else
        {
            $user = Auth::user();
            $user->password = Hash::make($pass_baru);
            $user->save();
            $request->session()->flash('message', 'Password berhasil diperbarui');
            return redirect('/password_pelanggan');
        }
    }

    public function pesanan()
    {
        $pelanggan      = Pelanggan::where('user_id', Auth::user()->id)->first();
        $so_header_obj  = SOHeader::where('pelanggan_id', $pelanggan->id)->get();

        $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');

        $so_header = [];
        foreach ($so_header_obj as $key => $value) {
            $so_header[$key]['id']                     = $value->id;
            $so_header[$key]['no_invoice']             = $value->no_sales_order;

            date_default_timezone_set("Asia/Jakarta");
            $tanggal = new DateTime($value->tanggal);

            $so_header[$key]['tanggal']             = $tanggal->format('d')." ".$bulan[(int)$tanggal->format('m')]." ".$tanggal->format('Y')." ".$tanggal->format("G:i:s");
            $so_header[$key]['total_tagihan']       = $value->total_tagihan;

            $so_header[$key]['status_pembayaran']   = "Pesanan Belum Dibayar";

            //cek udah pernah konfirm pembayaran atau belum
            $so_header[$key]['flag_konfirmasi_bayar']  = 0;

            $so_pembayaran = SOPembayaran::where('so_header_id', $value->id)->first();
            if(!empty($so_pembayaran->tanggal) && $so_pembayaran->nominal == $value->total_tagihan){
                $so_header[$key]['flag_konfirmasi_bayar']  = 1;
                $so_header[$key]['status_pembayaran']   = "Lunas";
            }

            if($value->is_sj_lengkap == 0){
                $so_header[$key]['status_pengiriman']   = "Pesanan sedang diproses";
            }
            else{
                $so_header[$key]['status_pengiriman']   = "Pesanan sudah dikirim";
            }
            
        }
        
        return view('pages.home.pesanan', compact('so_header'));
    }

    public function detail_pesanan($so_header_id)
    {
        $so_header      = SOHeader::where('id', $so_header_id)->first();
        $so_detail_obj = SODetail::where('so_header_id', $so_header_id)->get();

        //dapatkan detail data produk, paket, hadiah di SODetail
        $so_detail = [];
        foreach ($so_detail_obj as $key => $value) {
            if($value->jenis_barang_id == 1){
                $produk = Produk::where('id', $value->produk_id)->first();
                $so_detail[$key]['nama']    = $produk->nama;
                $so_detail[$key]['satuan']  = $produk->satuan;
            }
            elseif($value->jenis_barang_id == 2) {
                $hadiah = Hadiah::where('id', $value->produk_id)->first();
                $so_detail[$key]['nama']    = $hadiah->nama;
                $so_detail[$key]['satuan']  = $hadiah->satuan;
            }
            elseif($value->jenis_barang_id == 3) {
                $paket = Paket::where('id', $value->produk_id)->first();
                $so_detail[$key]['nama']    = $paket->nama;
                $so_detail[$key]['satuan']  = "paket";
            }

            $so_detail[$key]['harga']       = $value->harga;
            $so_detail[$key]['quantity']    = $value->jumlah;
        }

        $no_nota = $so_header->no_sales_order;

        return view('pages.home.detail_pesanan', compact('no_nota', 'so_detail'));
    }

    public function konfirmasi_pembayaran($id)
    {
        $so_header          = SOHeader::where('id', $id)->first();
        $so_pembayaran      = SOPembayaran::where('so_header_id', $id)->first();
        $bank               = Bank::all();
        $metode_pembayaran  = MetodePembayaran::all();

        return view('pages.home.konfirmasi_pembayaran', compact('so_header', 'so_pembayaran', 'bank', 'metode_pembayaran'));
    }

    public function do_konfirmasi_pembayaran(Request $request)
    {
        $this->validate($request, [
            'so_header_id' => 'required',
            'tanggal' => 'required',
            'metode_pembayaran_id' => 'required',
            'nomor_pembayaran' => 'required',
            'nominal'  => 'required',
        ]);

        $so_header_id           = Input::get('so_header_id');
        $tanggal                = Input::get('tanggal');
        $metode_pembayaran_id   = Input::get('metode_pembayaran_id');
        $bank_id                = Input::get('bank_id');
        $nomor_pembayaran       = Input::get('nomor_pembayaran');
        $nominal                = Input::get('nominal');

        $tanggal = new DateTime($tanggal);
        $nominal = str_replace('.', '', $nominal);

        $so_pembayaran = SOPembayaran::where('so_header_id', $so_header_id)->first();
        $so_pembayaran->tanggal     = $tanggal;
        $so_pembayaran->metode_pembayaran_id   = $metode_pembayaran_id;
        $so_pembayaran->bank_id                = $bank_id;
        $so_pembayaran->nomor_pembayaran       = $nomor_pembayaran;
        $so_pembayaran->nominal                = $nominal;
        $so_pembayaran->save();

        $request->session()->flash('message', 'Konfirmasi Pembayarn berhasil ditambahkan');
        return redirect('/pesanan');
    }
}
