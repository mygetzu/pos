<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Gloudemans\Shoppingcart\Facades\Cart;
use Input;
use Auth;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Session;
use Promocodes;
use Response;
use DateTime;
use PDF;
use Hashids\Hashids;
use Validator;
use Illuminate\Http\RedirectResponse;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use App\User;
use App\Models\Produk\KategoriProduk;
use App\Models\TransaksiPembelian\BayarPembelian;
use App\Models\TransaksiPembelian\NotaBeli;
use App\Models\TransaksiPembelian\RBHeader;
use App\Models\TransaksiPembelian\RBDetail;
use App\Models\TransaksiPembelian\SJMasukHeader;
use App\Models\TransaksiPembelian\SJMasukDetail;
use App\Models\TransaksiPembelian\BayarPembelianHeader;
use App\Models\TransaksiPembelian\BayarPembelianDetail;
use App\Models\TransaksiPenjualan\BayarPenjualan;
use App\Models\TransaksiPenjualan\NotaJual;
use App\Models\TransaksiPenjualan\RJHeader;
use App\Models\TransaksiPenjualan\RJDetail;
use App\Models\TransaksiPenjualan\SJKeluarHeader;
use App\Models\TransaksiPenjualan\SJKeluarDetail;
use App\Models\TransaksiPenjualan\ServiceOrder;
use App\Models\TransaksiPenjualan\BayarPenjualanHeader;
use App\Models\TransaksiPenjualan\BayarPenjualanDetail;

class LaporanController extends Controller
{
    public function laporan_penjualan()
    {
        $general['title']       = "Laporan Penjualan";
        $general['menu1']       = "Laporan";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 73;    

        return view('pages.laporan.laporan_penjualan', compact('general'));
    }

    public function detail_laporan_stok($laporan_stok_id)
    {
        $general['title']       = "Detail Laporan Stok";
        $general['menu1']       = "Laporan";
        $general['menu2']       = "Laporan Stok";
        $general['menu3']       = $general['title'];
        $general['kode_menu']   = 71; 

        $laporan_stok           = DB::table('tran_laporan_stok')
        ->join('tmst_gudang', 'tmst_gudang.id', '=', 'tran_laporan_stok.gudang_id')
        ->where('tran_laporan_stok.id', $laporan_stok_id)->first();

        $laporan_stok_detail    = DB::table('tran_laporan_stok_detail')
        ->select('tran_laporan_stok_detail.*', 'tmst_produk.*', 'tref_sumber_data.nama as sumber_data_nama')
        ->join('tmst_produk', 'tmst_produk.id', '=', 'tran_laporan_stok_detail.produk_id')
        ->join('tref_sumber_data', 'tref_sumber_data.id', '=', 'tran_laporan_stok_detail.sumber_data_id')
        ->where('tran_laporan_stok_detail.laporan_stok_id', $laporan_stok_id)
        ->orderBy('tran_laporan_stok_detail.produk_id', 'asc')->groupBy('tran_laporan_stok_detail.produk_id')->get();

        $indeks = 0;

        foreach ($laporan_stok_detail as $val1) {
            $laporan_stok_detail_produk[$indeks]   = DB::table('tran_laporan_stok_detail')
            ->select('tran_laporan_stok_detail.*', 'tref_sumber_data.nama as sumber_data_nama')
            ->join('tref_sumber_data', 'tref_sumber_data.id', '=', 'tran_laporan_stok_detail.sumber_data_id')
            ->where('tran_laporan_stok_detail.laporan_stok_id', $laporan_stok_id)
            ->where('tran_laporan_stok_detail.produk_id', $val1->produk_id)->orderBy('tran_laporan_stok_detail.tanggal', 'asc')->get();
            $indeks++;
        }

        return view('pages.laporan.detail_laporan_stok', compact('general', 'laporan_stok', 'laporan_stok_detail', 'laporan_stok_detail_produk'));
    }

    public function get_produk_gudang()
    {
        $gudang_id = Input::get('id');
        $my_kategori_produk = Input::get('kategori_produk');

        if(!empty($my_kategori_produk)){
            $produk_gudang   = DB::table('tran_produk_gudang')->join('tmst_produk', 'tmst_produk.id', '=', 'tran_produk_gudang.produk_id')->where('gudang_id', $gudang_id)->where('kategori_id', $my_kategori_produk)->get();
        }
        else{
            $produk_gudang   = DB::table('tran_produk_gudang')->join('tmst_produk', 'tmst_produk.id', '=', 'tran_produk_gudang.produk_id')->where('gudang_id', $gudang_id)->get();
        }

        $kategori_produk = KategoriProduk::where('is_aktif', 1)->get();

        return view('pages.laporan.ajax_get_produk_gudang', compact('produk_gudang', 'kategori_produk', 'my_kategori_produk'));
    }

    public function detail_kartu_stok($kartu_stok_id)
    {
        $general['title']       = "Detail Kartu Stok";
        $general['menu1']       = "Laporan";
        $general['menu2']       = "Kartu Stok";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 72;

        //tampilkan hasil
        $kartu_stok           = DB::table('tran_kartu_stok')->select('tran_kartu_stok.*', 'tmst_gudang.nama as gudang_nama', 'tmst_produk.nama as produk_nama')
        ->join('tmst_gudang', 'tmst_gudang.id', '=', 'tran_kartu_stok.gudang_id')
        ->join('tmst_produk', 'tmst_produk.id', '=', 'tran_kartu_stok.produk_id')
        ->where('tran_kartu_stok.id', $kartu_stok_id)->first();

        $kartu_stok_detail    = DB::table('tran_kartu_stok_detail')
        ->select('tran_kartu_stok_detail.*', 'tref_sumber_data.nama as sumber_data_nama')
        ->join('tref_sumber_data', 'tref_sumber_data.id', '=', 'tran_kartu_stok_detail.sumber_data_id')
        ->where('kartu_stok_id', $kartu_stok_id)
        ->orderBy('tran_kartu_stok_detail.serial_number', 'asc')->groupBy('tran_kartu_stok_detail.serial_number')->get();

        $indeks = 0;

        foreach ($kartu_stok_detail as $val1) {
            $kartu_stok_detail_produk[$indeks]   = DB::table('tran_kartu_stok_detail')
            ->select('tran_kartu_stok_detail.*', 'tref_sumber_data.nama as sumber_data_nama')
            ->join('tref_sumber_data', 'tref_sumber_data.id', '=', 'tran_kartu_stok_detail.sumber_data_id')
            ->where('tran_kartu_stok_detail.kartu_stok_id', $kartu_stok_id)
            ->where('tran_kartu_stok_detail.serial_number', $val1->serial_number)->orderBy('tran_kartu_stok_detail.tanggal', 'asc')->get();
            $indeks++;
        }

        return view('pages.laporan.detail_kartu_stok', compact('general', 'kartu_stok', 'kartu_stok_detail', 'kartu_stok_detail_produk'));
    }

    public function buat_laporan_penjualan()
    {
        $tanggal_awal   = Input::get('tanggal_awal');
        $tanggal_akhir  = Input::get('tanggal_akhir');

        $tanggal_awal   = new DateTime($tanggal_awal);
        $tanggal_akhir  = new DateTime($tanggal_akhir);

        $laporan_penjualan = [];

        //diambil dari nota jual
        $nota_jual = NotaJual::with('pelanggan')->where('tanggal', '>=', $tanggal_awal->format('Y-m-d H:i:s'))
        ->where('tanggal', '<=', $tanggal_akhir->format('Y-m-d H:i:s'))->orderBy('tanggal', 'asc')->get();

        $indeks= 0;
        foreach ($nota_jual as $value) {
            $laporan_penjualan[$indeks]['tanggal']          = $value->tanggal;
            $laporan_penjualan[$indeks]['nota_jual']        = $value->no_nota;
            $laporan_penjualan[$indeks]['pelanggan']        = $value->pelanggan->nama;
            $laporan_penjualan[$indeks]['sj_keluar_header_id'] = $value->sj_keluar_header_id;
            
            $detail_jual = DB::table('tran_sj_keluar_detail')->where('sj_keluar_header_id', $laporan_penjualan[$indeks]['sj_keluar_header_id'])->orderBy('produk_id', 'asc')->get();
            $produk = 0;
            $produk_qty = 0;
            foreach($detail_jual as $r_jual){
                $produk_id = $r_jual->produk_id;
                $laporan_penjualan[$indeks][$produk]['jumlah_produk'] = $produk_qty + $r_jual->jumlah;
                $laporan_penjualan[$indeks][$produk]['serial_produk'] = $r_jual->serial_number;
                $laporan_penjualan[$indeks][$produk]['nama_produk'] = DB::table('tmst_produk')->where('id', $produk_id)->value('nama');
                $produk_qty = $laporan_penjualan[$indeks][$produk]['jumlah_produk'];
                $produk++;
            }
            $laporan_penjualan[$indeks]['jumlah_produk']        = $produk_qty;
            
//            $laporan_penjualan[$indeks]['']

            // $laporan_penjualan[$indeks]['total_penjualan']  = $value->total_tagihan;
            //nanti di total_penjualan nota_jual harus menyimpan total tagihan
            $total_penjualan = 0;

            //ambil di surat jalan
            $sj_keluar_detail = SJKeluarDetail::where('sj_keluar_header_id', $value->sj_keluar_header_id)->get();
            foreach ($sj_keluar_detail as $key2 => $value2) {
                $total_penjualan = $total_penjualan + $value2->harga;
            }

            $laporan_penjualan[$indeks]['total_penjualan']  = $total_penjualan;

            $indeks++;
        }

        return view('pages.laporan.ajax_laporan_penjualan', compact('laporan_penjualan', 'indeks', 'produk'));
    }

    public function laporan_pembelian()
    {
        $general['title']       = "Laporan Pembelian";
        $general['menu1']       = "Laporan";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 74;    

        return view('pages.laporan.laporan_pembelian', compact('general'));
    }

    public function buat_laporan_pembelian()
    {
        $tanggal_awal   = Input::get('tanggal_awal');
        $tanggal_akhir  = Input::get('tanggal_akhir');

        $tanggal_awal   = new DateTime($tanggal_awal);
        $tanggal_akhir  = new DateTime($tanggal_akhir);

        $laporan_pembelian = [];

        //diambil dari nota beli
        $nota_beli = NotaBeli::with('supplier')->where('tanggal', '>=', $tanggal_awal->format('Y-m-d H:i:s'))
        ->where('tanggal', '<=', $tanggal_akhir->format('Y-m-d H:i:s'))->orderBy('tanggal', 'asc')->get();

        $indeks= 0;
        foreach ($nota_beli as $value) {
            $laporan_pembelian[$indeks]['tanggal']          = $value->tanggal;
            $laporan_pembelian[$indeks]['nota_beli']        = $value->no_nota;
            $laporan_pembelian[$indeks]['supplier']         = $value->supplier->nama;
            
            // $laporan_pembelian[$indeks]['total_pembelian']  = $value->total_tagihan;
            //nanti di total_pembelian nota_beli harus menyimpan total tagihan
            $total_pembelian = 0;

            //ambil di surat jalan
            $sj_masuk_detail = SJMasukDetail::where('sj_masuk_header_id', $value->sj_masuk_header_id)->get();

            

            foreach ($sj_masuk_detail as $key2 => $value2) {
                $total_pembelian = $total_pembelian + $value2->harga;
            }

            $laporan_pembelian[$indeks]['total_pembelian']  = $total_pembelian;
            
            $indeks++;
        }
        
        return view('pages.laporan.ajax_laporan_pembelian', compact('laporan_pembelian', 'indeks'));
    }

    public function laporan_surat_jalan_masuk()
    {
        $general['title']       = "Laporan Surat Jalan Masuk";
        $general['menu1']       = "Laporan";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 75;

        return view('pages.laporan.laporan_surat_jalan_masuk', compact('general'));
    }

    public function buat_laporan_surat_jalan_masuk()
    {
        $input_tanggal_awal   = Input::get('tanggal_awal');
        $input_tanggal_akhir  = Input::get('tanggal_akhir');

        $tanggal_awal   = new DateTime($input_tanggal_awal);
        $tanggal_akhir  = new DateTime($input_tanggal_akhir);

        $laporan_surat_jalan_masuk = [];

        //diambil dari surat jalan masuk
        $sj_masuk_header = SJMasukHeader::with('supplier')->where('tanggal', '>=', $tanggal_awal->format('Y-m-d H:i:s'))
        ->where('tanggal', '<=', $tanggal_akhir->format('Y-m-d H:i:s'))->orderBy('tanggal', 'asc')->get();
//        $sj_masuk_header = DB::table('tran_sj_masuk_header')->where('tanggal', date('Y-m-d H:i:s', '>=', strtotime($input_tanggal_akhir)))->where('tanggal', '<=', date('Y-m-d H:i:s', strtotime($input_tanggal_akhir)));

        $indeks= 0;
        foreach ($sj_masuk_header as $value) {
            $laporan_surat_jalan_masuk[$indeks]['tanggal']          = $value->tanggal;
            $laporan_surat_jalan_masuk[$indeks]['no_surat_jalan']   = $value->no_surat_jalan;
            $laporan_surat_jalan_masuk[$indeks]['supplier']         = $value->supplier->nama;
            $laporan_surat_jalan_masuk[$indeks]['id']         = $value->id;

            $sj_masuk_detail = SJMasukDetail::with('produk', 'hadiah')->where('sj_masuk_header_id', $value->id)->groupBy('produk_id', 'jenis_barang_id')->selectRaw('tran_sj_masuk_detail.*, sum(jumlah) as jumlah_total')->get();

            $laporan_surat_jalan_masuk[$indeks]['sj_masuk_detail']  = $sj_masuk_detail;

            $produk_sn = [];
            foreach ($sj_masuk_detail  as $key => $value2) {
                $produk_sn[$value->id][$key] = SJMasukDetail::where('sj_masuk_header_id', $value->id)->where('produk_id', $value2->produk_id)->where('jenis_barang_id', $value2->jenis_barang_id)->get();
            }
            $laporan_surat_jalan_masuk[$indeks]['jumlah_produk']  = $key + 1;

            $indeks++;
        }
        //echo Response::json($produk_sn);
        return view('pages.laporan.ajax_laporan_surat_jalan_masuk', compact('laporan_surat_jalan_masuk', 'indeks', 'produk_sn', 'input_tanggal_awal', 'input_tanggal_akhir'));
    }

    public function laporan_surat_jalan_masuk_cetak($input_tanggal_awal, $input_tanggal_akhir)
    {
        $tanggal_awal   = new DateTime($input_tanggal_awal);
        $tanggal_akhir  = new DateTime($input_tanggal_akhir);

        $laporan_surat_jalan_masuk = [];

        //diambil dari surat jalan masuk
        $sj_masuk_header = SJMasukHeader::with('supplier')->where('tanggal', '>=', $tanggal_awal->format('Y-m-d H:i:s'))
        ->where('tanggal', '<=', $tanggal_akhir->format('Y-m-d H:i:s'))->orderBy('tanggal', 'asc')->get();

        $indeks= 0;
        foreach ($sj_masuk_header as $value) {
            $laporan_surat_jalan_masuk[$indeks]['tanggal']          = $value->tanggal;
            $laporan_surat_jalan_masuk[$indeks]['no_surat_jalan']   = $value->no_surat_jalan;
            $laporan_surat_jalan_masuk[$indeks]['supplier']         = $value->supplier->nama;
            $laporan_surat_jalan_masuk[$indeks]['id']         = $value->id;

            $sj_masuk_detail = SJMasukDetail::with('produk', 'hadiah')->where('sj_masuk_header_id', $value->id)->groupBy('produk_id', 'jenis_barang_id')->selectRaw('tran_sj_masuk_detail.*, sum(jumlah) as jumlah_total')->get();

            $laporan_surat_jalan_masuk[$indeks]['sj_masuk_detail']  = $sj_masuk_detail;

            $produk_sn = [];
            foreach ($sj_masuk_detail  as $key => $value2) {
                $produk_sn[$value->id][$key] = SJMasukDetail::where('sj_masuk_header_id', $value->id)->where('produk_id', $value2->produk_id)->where('jenis_barang_id', $value2->jenis_barang_id)->get();
            }
            $laporan_surat_jalan_masuk[$indeks]['jumlah_produk']  = $key + 1;
            //kurang nampilkan serial number

            $indeks++;
        }

        date_default_timezone_set("Asia/Jakarta"); 
        $print_time = new DateTime;

        return view('pages.laporan.laporan_surat_jalan_masuk_cetak', compact('laporan_surat_jalan_masuk', 'indeks', 'produk_sn', 'input_tanggal_awal', 'input_tanggal_akhir', 'print_time'));
    }

    public function laporan_surat_jalan_masuk_pdf($input_tanggal_awal, $input_tanggal_akhir)
    {
        $tanggal_awal   = new DateTime($input_tanggal_awal);
        $tanggal_akhir  = new DateTime($input_tanggal_akhir);

        $laporan_surat_jalan_masuk = [];

        //diambil dari surat jalan masuk
        $sj_masuk_header = SJMasukHeader::with('supplier')->where('tanggal', '>=', $tanggal_awal->format('Y-m-d H:i:s'))
        ->where('tanggal', '<=', $tanggal_akhir->format('Y-m-d H:i:s'))->orderBy('tanggal', 'asc')->get();

        $indeks= 0;
        foreach ($sj_masuk_header as $value) {
            $laporan_surat_jalan_masuk[$indeks]['tanggal']          = $value->tanggal;
            $laporan_surat_jalan_masuk[$indeks]['no_surat_jalan']   = $value->no_surat_jalan;
            $laporan_surat_jalan_masuk[$indeks]['supplier']         = $value->supplier->nama;
            $laporan_surat_jalan_masuk[$indeks]['id']         = $value->id;

            $sj_masuk_detail = SJMasukDetail::with('produk', 'hadiah')->where('sj_masuk_header_id', $value->id)->groupBy('produk_id', 'jenis_barang_id')->selectRaw('tran_sj_masuk_detail.*, sum(jumlah) as jumlah_total')->get();

            $laporan_surat_jalan_masuk[$indeks]['sj_masuk_detail']  = $sj_masuk_detail;

            $produk_sn = [];
            foreach ($sj_masuk_detail  as $key => $value2) {
                $produk_sn[$value->id][$key] = SJMasukDetail::where('sj_masuk_header_id', $value->id)->where('produk_id', $value2->produk_id)->where('jenis_barang_id', $value2->jenis_barang_id)->get();
            }
            $laporan_surat_jalan_masuk[$indeks]['jumlah_produk']  = $key + 1;
            //kurang nampilkan serial number

            $indeks++;
        }

        date_default_timezone_set("Asia/Jakarta"); 
        $print_time = new DateTime;

        $pdf = PDF::loadView('pages.laporan.laporan_surat_jalan_masuk_cetak', compact('laporan_surat_jalan_masuk', 'indeks', 'produk_sn', 'input_tanggal_awal', 'input_tanggal_akhir', 'print_time'))->setPaper('a4', 'landscape');
        return $pdf->stream();
    }

    public function laporan_surat_jalan_keluar()
    {
        $general['title']       = "Laporan Surat Jalan Keluar";
        $general['menu1']       = "Laporan";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 76;

        return view('pages.laporan.laporan_surat_jalan_keluar', compact('general'));
    }

    public function buat_laporan_surat_jalan_keluar()
    {
        $input_tanggal_awal   = Input::get('tanggal_awal');
        $input_tanggal_akhir  = Input::get('tanggal_akhir');

        $tanggal_awal   = new DateTime($input_tanggal_awal);
        $tanggal_akhir  = new DateTime($input_tanggal_akhir);

        $laporan_surat_jalan_keluar = [];

        //diambil dari surat jalan keluar
        $sj_keluar_header = SJKeluarHeader::with('pelanggan')->where('tanggal', '>=', $tanggal_awal->format('Y-m-d H:i:s'))
        ->where('tanggal', '<=', $tanggal_akhir->format('Y-m-d H:i:s'))->orderBy('tanggal', 'asc')->get();

        $indeks= 0;
        foreach ($sj_keluar_header as $value) {
            $laporan_surat_jalan_keluar[$indeks]['tanggal']          = $value->tanggal;
            $laporan_surat_jalan_keluar[$indeks]['no_surat_jalan']   = $value->no_surat_jalan;
            $laporan_surat_jalan_keluar[$indeks]['pelanggan']         = $value->pelanggan->nama;
            $laporan_surat_jalan_keluar[$indeks]['id']         = $value->id;

            $sj_keluar_detail = SJKeluarDetail::with('produk', 'hadiah')->where('sj_keluar_header_id', $value->id)->groupBy('produk_id', 'jenis_barang_id')->selectRaw('tran_sj_keluar_detail.*, sum(jumlah) as jumlah_total')->get();

            $laporan_surat_jalan_keluar[$indeks]['sj_keluar_detail']  = $sj_keluar_detail;

            $produk_sn = [];
            foreach ($sj_keluar_detail  as $key => $value2) {
                $produk_sn[$value->id][$key] = SJKeluarDetail::where('sj_keluar_header_id', $value->id)->where('produk_id', $value2->produk_id)->where('jenis_barang_id', $value2->jenis_barang_id)->get();
            }
            $laporan_surat_jalan_keluar[$indeks]['jumlah_produk']  = $key + 1;
            //kurang nampilkan serial number

            $indeks++;
        }
        //echo Response::json($produk_sn);
        return view('pages.laporan.ajax_laporan_surat_jalan_keluar', compact('laporan_surat_jalan_keluar', 'indeks', 'produk_sn', 'input_tanggal_awal', 'input_tanggal_akhir'));
    }

    public function laporan_surat_jalan_keluar_cetak($input_tanggal_awal, $input_tanggal_akhir)
    {
        $tanggal_awal   = new DateTime($input_tanggal_awal);
        $tanggal_akhir  = new DateTime($input_tanggal_akhir);

        $laporan_surat_jalan_keluar = [];

        //diambil dari surat jalan keluar
        $sj_keluar_header = SJKeluarHeader::with('pelanggan')->where('tanggal', '>=', $tanggal_awal->format('Y-m-d H:i:s'))
        ->where('tanggal', '<=', $tanggal_akhir->format('Y-m-d H:i:s'))->orderBy('tanggal', 'asc')->get();

        $indeks= 0;
        foreach ($sj_keluar_header as $value) {
            $laporan_surat_jalan_keluar[$indeks]['tanggal']          = $value->tanggal;
            $laporan_surat_jalan_keluar[$indeks]['no_surat_jalan']   = $value->no_surat_jalan;
            $laporan_surat_jalan_keluar[$indeks]['pelanggan']         = $value->pelanggan->nama;
            $laporan_surat_jalan_keluar[$indeks]['id']         = $value->id;

            $sj_keluar_detail = SJKeluarDetail::with('produk', 'hadiah')->where('sj_keluar_header_id', $value->id)->groupBy('produk_id', 'jenis_barang_id')->selectRaw('tran_sj_keluar_detail.*, sum(jumlah) as jumlah_total')->get();

            $laporan_surat_jalan_keluar[$indeks]['sj_keluar_detail']  = $sj_keluar_detail;

            $produk_sn = [];
            foreach ($sj_keluar_detail  as $key => $value2) {
                $produk_sn[$value->id][$key] = SJKeluarDetail::where('sj_keluar_header_id', $value->id)->where('produk_id', $value2->produk_id)->where('jenis_barang_id', $value2->jenis_barang_id)->get();
            }
            $laporan_surat_jalan_keluar[$indeks]['jumlah_produk']  = $key + 1;
            //kurang nampilkan serial number

            $indeks++;
        }

        date_default_timezone_set("Asia/Jakarta"); 
        $print_time = new DateTime;

        //echo Response::json($produk_sn);
        return view('pages.laporan.laporan_surat_jalan_keluar_cetak', compact('laporan_surat_jalan_keluar', 'indeks', 'produk_sn', 'input_tanggal_awal', 'input_tanggal_akhir', 'print_time'));
    }

    public function laporan_surat_jalan_keluar_pdf($input_tanggal_awal, $input_tanggal_akhir)
    {
        $tanggal_awal   = new DateTime($input_tanggal_awal);
        $tanggal_akhir  = new DateTime($input_tanggal_akhir);

        $laporan_surat_jalan_keluar = [];

        //diambil dari surat jalan keluar
        $sj_keluar_header = SJKeluarHeader::with('pelanggan')->where('tanggal', '>=', $tanggal_awal->format('Y-m-d H:i:s'))
        ->where('tanggal', '<=', $tanggal_akhir->format('Y-m-d H:i:s'))->orderBy('tanggal', 'asc')->get();

        $indeks= 0;
        foreach ($sj_keluar_header as $value) {
            $laporan_surat_jalan_keluar[$indeks]['tanggal']          = $value->tanggal;
            $laporan_surat_jalan_keluar[$indeks]['no_surat_jalan']   = $value->no_surat_jalan;
            $laporan_surat_jalan_keluar[$indeks]['pelanggan']         = $value->pelanggan->nama;
            $laporan_surat_jalan_keluar[$indeks]['id']         = $value->id;

            $sj_keluar_detail = SJKeluarDetail::with('produk', 'hadiah')->where('sj_keluar_header_id', $value->id)->groupBy('produk_id', 'jenis_barang_id')->selectRaw('tran_sj_keluar_detail.*, sum(jumlah) as jumlah_total')->get();

            $laporan_surat_jalan_keluar[$indeks]['sj_keluar_detail']  = $sj_keluar_detail;

            $produk_sn = [];
            foreach ($sj_keluar_detail  as $key => $value2) {
                $produk_sn[$value->id][$key] = SJKeluarDetail::where('sj_keluar_header_id', $value->id)->where('produk_id', $value2->produk_id)->where('jenis_barang_id', $value2->jenis_barang_id)->get();
            }
            $laporan_surat_jalan_keluar[$indeks]['jumlah_produk']  = $key + 1;
            //kurang nampilkan serial number

            $indeks++;
        }

        date_default_timezone_set("Asia/Jakarta"); 
        $print_time = new DateTime;

        $pdf = PDF::loadView('pages.laporan.laporan_surat_jalan_keluar_cetak', compact('laporan_surat_jalan_keluar', 'indeks', 'produk_sn', 'input_tanggal_awal', 'input_tanggal_akhir', 'print_time'))->setPaper('a4', 'landscape');
        return $pdf->stream();
    }

    public function laporan_servis()
    {
        $general['title']       = "Laporan Servis";
        $general['menu1']       = "Laporan";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 77;

        return view('pages.laporan.laporan_servis', compact('general'));
    }

    public function buat_laporan_servis()
    {
        $input_tanggal_awal     = Input::get('tanggal_awal');
        $input_tanggal_akhir    = Input::get('tanggal_akhir');
        $status_servis          = Input::get('status_servis');

        $tanggal_awal   = new DateTime($input_tanggal_awal);
        $tanggal_akhir  = new DateTime($input_tanggal_akhir);

        if($status_servis == '1'){
            $servis = ServiceOrder::with('pelanggan')->where('tanggal', '>=', $tanggal_awal->format('Y-m-d H:i:s'))->where('tanggal', '<=', $tanggal_akhir->format('Y-m-d H:i:s'))->orderBy('tanggal', 'asc')->get();
        }
        elseif ($status_servis == '2') {
            $servis = ServiceOrder::with('pelanggan')->where('flag', 1)->where('tanggal', '>=', $tanggal_awal->format('Y-m-d H:i:s'))->where('tanggal', '<=', $tanggal_akhir->format('Y-m-d H:i:s'))->orderBy('tanggal', 'asc')->get();
        }
        elseif ($status_servis == '3') {
            $servis = ServiceOrder::with('pelanggan')->where('flag', 2)->where('tanggal', '>=', $tanggal_awal->format('Y-m-d H:i:s'))->where('tanggal', '<=', $tanggal_akhir->format('Y-m-d H:i:s'))->orderBy('tanggal', 'asc')->get();
        }

        
        return view('pages.laporan.ajax_laporan_servis', compact('servis', 'status_servis', 'input_tanggal_awal', 'input_tanggal_akhir', 'print_time'));
    }

    public function laporan_pembayaran_supplier()
    {
        $general['title']       = "Laporan Pembayaran Supplier";
        $general['menu1']       = "Laporan";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 78;

        return view('pages.laporan.laporan_pembayaran_supplier', compact('general'));
    }

    public function buat_laporan_pembayaran_supplier()
    {
        $input_tanggal_awal     = Input::get('tanggal_awal');
        $input_tanggal_akhir    = Input::get('tanggal_akhir');
        $status_pembayaran      = Input::get('status_pembayaran');

        $tanggal_awal   = new DateTime($input_tanggal_awal);
        $tanggal_akhir  = new DateTime($input_tanggal_akhir);

        $pembayaran = [];
        $indeks = 0;
        if($status_pembayaran == '1'){
            $bayar_pembelian = BayarPembelianHeader::with('supplier', 'nota_beli', 'rb_header')->where('tanggal', '>=', $tanggal_awal->format('Y-m-d H:i:s'))->where('tanggal', '<=', $tanggal_akhir->format('Y-m-d H:i:s'))->orderBy('tanggal', 'asc')->get();
            foreach ($bayar_pembelian as $key => $value) {
                $pembayaran[$indeks]['tanggal_transaksi']   = "";
                if($value->jenis == 'nota_beli'){
                    $pembayaran[$indeks]['tanggal_transaksi'] = $value->nota_beli->tanggal;
                }
                elseif($value->jenis == 'retur_beli'){
                   $pembayaran[$indeks]['tanggal_transaksi'] = $value->rb_header->tanggal;
                }

                $pembayaran[$indeks]['nota_supplier']  = $value->nota_supplier;
                $pembayaran[$indeks]['tanggal_nota_supplier']  = $value->tanggal_nota_supplier;
                $pembayaran[$indeks]['tanggal_pembayaran']  = $value->tanggal;
                $pembayaran[$indeks]['jenis']               = ucwords(str_replace('_', ' ', $value->jenis));
                $pembayaran[$indeks]['supplier']            = $value->supplier->nama;
                $pembayaran[$indeks]['total_tagihan']       = $value->total_tagihan;
                $pembayaran[$indeks]['total_pembayaran']    = $value->total_pembayaran;
                $indeks++;
            }
        }
        elseif ($status_pembayaran == '2') {
            //ambil data transaksi dari nota beli dan retur beli
            $nota_beli = NotaBeli::with('supplier')->where('is_lunas', 0)->where('tanggal', '>=', $tanggal_awal->format('Y-m-d H:i:s'))->where('tanggal', '<=', $tanggal_akhir->format('Y-m-d H:i:s'))->orderBy('tanggal', 'asc')->get();

            foreach ($nota_beli as $key => $value) {
                $pembayaran[$indeks]['nota_supplier']  = $value->nota_supplier;
                $pembayaran[$indeks]['tanggal_nota_supplier']  = $value->tanggal_nota_supplier;
                $pembayaran[$indeks]['tanggal_transaksi']   = $value->tanggal;
                $pembayaran[$indeks]['tanggal_pembayaran']  = "-";
                $pembayaran[$indeks]['jenis']               = "Nota Beli";
                $pembayaran[$indeks]['supplier']            = $value->supplier->nama;
                $pembayaran[$indeks]['total_tagihan']       = $value->total_tagihan;
                $pembayaran[$indeks]['total_pembayaran']    = "-";
                $indeks++;
            }

            $retur_beli = RBHeader::with('supplier')->where('is_lunas', 0)->where('tanggal', '>=', $tanggal_awal->format('Y-m-d H:i:s'))->where('tanggal', '<=', $tanggal_akhir->format('Y-m-d H:i:s'))->orderBy('tanggal', 'asc')->get();

            foreach ($retur_beli as $key => $value) {
                $pembayaran[$indeks]['nota_supplier']  = $value->nota_supplier;
                $pembayaran[$indeks]['tanggal_nota_supplier']  = $value->tanggal_nota_supplier;
                $pembayaran[$indeks]['tanggal_transaksi']   = $value->tanggal;
                $pembayaran[$indeks]['tanggal_pembayaran']  = "-";
                $pembayaran[$indeks]['jenis']               = "Nota Beli";
                $pembayaran[$indeks]['supplier']            = $value->supplier->nama;
                $pembayaran[$indeks]['total_tagihan']       = $value->total_tagihan;
                $pembayaran[$indeks]['total_pembayaran']    = "-";
                $indeks++;
            }
        }
        
        return view('pages.laporan.ajax_laporan_pembayaran_supplier', compact('pembayaran', 'indeks'));
    }

    public function laporan_pembayaran_pelanggan()
    {
        $general['title']       = "Laporan Pembayaran Pelanggan";
        $general['menu1']       = "Laporan";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 79;

        return view('pages.laporan.laporan_pembayaran_pelanggan', compact('general'));
    }

    public function buat_laporan_pembayaran_pelanggan()
    {
        $input_tanggal_awal     = Input::get('tanggal_awal');
        $input_tanggal_akhir    = Input::get('tanggal_akhir');
        $status_pembayaran      = Input::get('status_pembayaran');

        $tanggal_awal   = new DateTime($input_tanggal_awal);
        $tanggal_akhir  = new DateTime($input_tanggal_akhir);

        $pembayaran = [];
        $indeks = 0;
        if($status_pembayaran == '1'){
            $bayar_penjualan = BayarPenjualanHeader::with('pelanggan', 'nota_jual', 'rj_header', 'service_order')->where('tanggal', '>=', $tanggal_awal->format('Y-m-d H:i:s'))->where('tanggal', '<=', $tanggal_akhir->format('Y-m-d H:i:s'))->orderBy('tanggal', 'asc')->get();
            foreach ($bayar_penjualan as $key => $value) {
                $pembayaran[$indeks]['tanggal_transaksi']   = "";
                if($value->jenis == 'nota_jual'){
                    $pembayaran[$indeks]['tanggal_transaksi'] = $value->nota_jual->tanggal;
                }
                elseif($value->jenis == 'retur_jual'){
                   $pembayaran[$indeks]['tanggal_transaksi'] = $value->rj_header->tanggal;
                }

                $pembayaran[$indeks]['tanggal_pembayaran']  = $value->tanggal;
                $pembayaran[$indeks]['jenis']               = ucwords(str_replace('_', ' ', $value->jenis));
                $pembayaran[$indeks]['pelanggan']           = $value->pelanggan->nama;
                $pembayaran[$indeks]['total_tagihan']       = $value->total_tagihan;
                $pembayaran[$indeks]['total_pembayaran']    = $value->total_pembayaran;
                $indeks++;
            }
        }
        elseif ($status_pembayaran == '2') {
            //ambil data transaksi dari nota beli dan retur beli
            $nota_jual = NotaJual::with('pelanggan')->where('is_lunas', 0)->where('tanggal', '>=', $tanggal_awal->format('Y-m-d H:i:s'))->where('tanggal', '<=', $tanggal_akhir->format('Y-m-d H:i:s'))->orderBy('tanggal', 'asc')->get();

            foreach ($nota_jual as $key => $value) {
                $pembayaran[$indeks]['tanggal_transaksi']   = $value->tanggal;
                $pembayaran[$indeks]['tanggal_pembayaran']  = "-";
                $pembayaran[$indeks]['jenis']               = "Nota Beli";
                $pembayaran[$indeks]['pelanggan']            = $value->pelanggan->nama;
                $pembayaran[$indeks]['total_tagihan']       = $value->total_tagihan;
                $pembayaran[$indeks]['total_pembayaran']    = "-";
                $indeks++;
            }

            $retur_jual = RJHeader::with('pelanggan')->where('is_lunas', 0)->where('tanggal', '>=', $tanggal_awal->format('Y-m-d H:i:s'))->where('tanggal', '<=', $tanggal_akhir->format('Y-m-d H:i:s'))->orderBy('tanggal', 'asc')->get();

            foreach ($retur_jual as $key => $value) {
                $pembayaran[$indeks]['tanggal_transaksi']   = $value->tanggal;
                $pembayaran[$indeks]['tanggal_pembayaran']  = "-";
                $pembayaran[$indeks]['jenis']               = "Nota Jual";
                $pembayaran[$indeks]['pelanggan']           = $value->pelanggan->nama;
                $pembayaran[$indeks]['total_tagihan']       = $value->total_tagihan;
                $pembayaran[$indeks]['total_pembayaran']    = "-";
                $indeks++;
            }

            $servis = ServiceOrder::with('pelanggan')->where('is_lunas', 0)->where('tanggal', '>=', $tanggal_awal->format('Y-m-d H:i:s'))->where('tanggal', '<=', $tanggal_akhir->format('Y-m-d H:i:s'))->orderBy('tanggal', 'asc')->get();

            foreach ($servis as $key => $value) {
                $pembayaran[$indeks]['tanggal_transaksi']   = $value->tanggal;
                $pembayaran[$indeks]['tanggal_pembayaran']  = "-";
                $pembayaran[$indeks]['jenis']               = "Servis";
                $pembayaran[$indeks]['pelanggan']           = $value->pelanggan->nama;
                $pembayaran[$indeks]['total_tagihan']       = $value->jasa_service;
                $pembayaran[$indeks]['total_pembayaran']    = "-";
                $indeks++;
            }
        }
        
        return view('pages.laporan.ajax_laporan_pembayaran_pelanggan', compact('pembayaran', 'indeks'));
    }
}
