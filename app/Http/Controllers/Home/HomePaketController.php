<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Gloudemans\Shoppingcart\Facades\Cart;
use Auth;
use Response;
use PDF;
use Hashids\Hashids;
use stdClass;
use Illuminate\Pagination\Paginator;
use Input;
use Session;
use DateTime;
use App\Models\Produk\Produk;
use App\Models\Produk\KategoriProduk;
use App\Models\Pelanggan\Pelanggan;
use App\Models\Transaksi\SOHeader;
use App\Models\Transaksi\SODetail;
use App\Models\Transaksi\SOVoucher;
use App\Models\Transaksi\Invoice;
use App\Models\Transaksi\InvoiceDetail;
use App\Models\Transaksi\BayarInvoice;
use App\Models\Perusahaan;
use App\Models\PaketProduk\Paket;
use App\Models\PaketProduk\PaketProduk;
use App\Models\Referensi\Bank;
use Illuminate\Support\Collection;
use App\Models\AdminEcommerce\MiniBanner;
use App\Models\AdminEcommerce\ProdukPilihan;
use App\Models\AdminEcommerce\PaketPilihan;
use App\Models\AdminEcommerce\Konten;
use App\Models\AdminEcommerce\ProdukBaru;
use App\Models\AdminEcommerce\ProdukAkanDatang;

class HomePaketController extends Controller {

    public function promo_paket_produk(Request $request) {
        $obj_gambar_produk_default = Konten::where('nama', 'gambar_produk_default')->first();
        if (empty($obj_gambar_produk_default)) {
            $gambar_produk_default = "";
        } else {
            $gambar_produk_default = $obj_gambar_produk_default->keterangan;
        }

        $obj_paket_pilihan = PaketPilihan::orderBy('item_order', 'asc')->get();
        $paket_pilihan = [];
        foreach ($obj_paket_pilihan as $key => $value) {
            $paket_pilihan[$key]['id'] = $value->id;
            $paket_pilihan[$key]['nama'] = $value->nama;
            $paket_pilihan[$key]['url'] = $value->url;
            $paket_pilihan[$key]['file_gambar'] = $value->file_gambar;

            $arr_list_paket = $value->list_paket;
            $arr_list_paket = explode(',', $arr_list_paket);

            $list_paket = [];
            for ($j = 0; $j < count($arr_list_paket); $j++) {
                $obj_paket = Paket::where('id', $arr_list_paket[$j])->first();

                $list_paket[$j]['nama'] = $obj_paket->nama;
                $list_paket[$j]['slug_nama'] = $obj_paket->slug_nama;
                $list_paket[$j]['harga'] = $obj_paket->harga_total;

                if (empty($obj_paket->file_gambar)) {
                    $list_paket[$j]['file_gambar'] = $gambar_produk_default;
                } else {
                    $list_paket[$j]['file_gambar'] = $obj_paket->file_gambar;
                }

                $list_paket[$j]['stok_tersedia'] = $obj_paket->stok - $obj_paket->stok_dipesan;
            }

            $paket_pilihan[$key]['list_paket'] = $list_paket;
        }

        // return Response::json($paket_pilihan);
        return view('pages.home.paket.promo_paket_produk', compact('paket_pilihan', 'gambar_produk_default'));
    }

    public function list_paket_produk(Request $request) {
        $paket = Paket::where('is_aktif', 1)->paginate(15);

        $range_harga = Input::get('range_harga');
        $range_min = "10000";
        $range_max = "30000000";
        $sorting = Input::get('sorting');

        if (!empty($range_harga)) {
            $range_harga = explode(';', $range_harga);
            $range_min = $range_harga[0];
            $range_max = $range_harga[1];

            $obj_paket = Paket::where('is_aktif', 1)->where('harga_total', '>=', (int) $range_min)->where('harga_total', '<=', (int) $range_max)->get();
        } else {
            $obj_paket = Paket::where('is_aktif', 1)->get();
        }

        $gambar_default = DB::table('static')->where('nama', 'gambar_produk_default')->first();
        $slug_nama = "";

        $kategori = "Paket Produk";

        $paket = [];
        foreach ($obj_paket as $key => $val) {
            $paket[$key]['slug_nama'] = $val->slug_nama;
            $paket[$key]['nama'] = $val->nama;
            $paket[$key]['harga_total'] = $val->harga_total;
            $paket[$key]['stok_tersedia'] = $val->stok - $val->stok_dipesan;

            if (!empty($val->file_gambar)) {
                $paket[$key]['file_gambar'] = $val->file_gambar;
            } else {
                $paket[$key]['file_gambar'] = "";
            }
        }

        if ($sorting == 2) {
            $nama = array();
            foreach ($paket as $key => $row) {
                $nama[$key] = $row['slug_nama'];
            }

            array_multisort($nama, SORT_DESC, SORT_STRING, $paket);
        } elseif ($sorting == 3) {
            $harga = array();
            foreach ($paket as $key => $row) {
                $harga[$key] = $row['harga_total'];
            }

            array_multisort($harga, SORT_ASC, $paket);
        } elseif ($sorting == 4) {
            $harga = array();
            foreach ($paket as $key => $row) {
                $harga[$key] = $row['harga_total'];
            }

            array_multisort($harga, SORT_DESC, $paket);
        } else {
            $nama = array();
            foreach ($paket as $key => $row) {
                $nama[$key] = $row['slug_nama'];
            }

            array_multisort($nama, SORT_ASC, SORT_STRING, $paket);
        }

        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $col = new Collection($paket);
        $perPage = 15;
        $currentPageSearchResults = $col->slice(($currentPage - 1) * $perPage, $perPage)->all();
        $paket = new LengthAwarePaginator($currentPageSearchResults, count($col), $perPage);
        $url = $request->fullUrl();
        $paket->setPath($url);

        return view('pages.home.list_paket_produk', compact('paket', 'gambar_default', 'slug_nama', 'kategori', 'range_min', 'range_max', 'sorting'));
    }

    public function paket_produk($slug_nama) {
        $paket = Paket::with(['paket_produk' => function($query) {
                        return $query->with(['produk' => function($query) {
                                        return $query->with('produk_galeri');
                                    }]);
                    }])->where('slug_nama', $slug_nama)->first();

                $gambar_default = DB::table('static')->where('nama', 'gambar_produk_default')->first();
                $slug_nama = "";
                return view('pages.home.detail_list_paket_produk', compact('paket', 'gambar_default', 'slug_nama'));
            }

            public function paket_pilihan(Request $request, $url) {
                $paket_pilihan = PaketPilihan::where('url', $url)->first();
                $list_paket = $paket_pilihan->list_paket;
                $list_paket = explode(',', $list_paket);

                $kategori_pelanggan_id = 0;

                $paket = [];
                for ($i = 0; $i < count($list_paket); $i++) {
                    $range_harga = Input::get('range_harga');
                    $range_min = "10000";
                    $range_max = "30000000";
                    $sorting = Input::get('sorting');

                    if (!empty($range_harga)) {
                        $range_harga = explode(';', $range_harga);
                        $range_min = $range_harga[0];
                        $range_max = $range_harga[1];

                        $paket_obj = Paket::where('is_aktif', 1)->where('id', $list_paket[$i])->where('harga_total', '>=', (int) $range_min)->where('harga_total', '<=', (int) $range_max)->first();
                    } else {
                        $paket_obj = Paket::where('is_aktif', 1)->where('id', $list_paket[$i])->first();
                    }

                    if (empty($paket_obj)) {
                        continue;
                    }

                    $paket[$i]['slug_nama'] = $paket_obj->slug_nama;
                    $paket[$i]['nama'] = $paket_obj->nama;
                    $paket[$i]['stok_tersedia'] = $paket_obj->tok - $paket_obj->stok_dipesan;
                    $paket[$i]['harga_total'] = $paket_obj->harga_total;

                    $obj_gambar_produk_default = Konten::where('nama', 'gambar_produk_default')->first();
                    if (empty($obj_gambar_produk_default)) {
                        $gambar_produk_default = "";
                    } else {
                        $gambar_produk_default = $obj_gambar_produk_default->keterangan;
                    }

                    if (!empty($paket_obj->file_gambar)) {
                        $paket[$i]['file_gambar'] = $paket_obj->file_gambar;
                    } else {
                        $paket[$i]['file_gambar'] = $gambar_produk_default;
                    }
                }

                if ($sorting == 2) {
                    $nama = array();
                    foreach ($paket as $key => $row) {
                        $nama[$key] = $row['slug_nama'];
                    }

                    array_multisort($nama, SORT_DESC, SORT_STRING, $paket);
                } elseif ($sorting == 3) {
                    $harga = array();
                    foreach ($paket as $key => $row) {
                        $harga[$key] = $row['harga_total'];
                    }

                    array_multisort($harga, SORT_ASC, $paket);
                } elseif ($sorting == 4) {
                    $harga = array();
                    foreach ($paket as $key => $row) {
                        $harga[$key] = $row['harga_total'];
                    }

                    array_multisort($harga, SORT_DESC, $paket);
                } else {
                    $nama = array();
                    foreach ($paket as $key => $row) {
                        $nama[$key] = $row['slug_nama'];
                    }

                    array_multisort($nama, SORT_ASC, SORT_STRING, $paket);
                }

                $currentPage = LengthAwarePaginator::resolveCurrentPage();
                $col = new Collection($paket);
                $perPage = 15;
                $currentPageSearchResults = $col->slice(($currentPage - 1) * $perPage, $perPage)->all();
                $paket = new LengthAwarePaginator($currentPageSearchResults, count($col), $perPage);
                $url = $request->fullUrl();
                $paket->setPath($url);

                return view('pages.home.paket.list_paket', compact('paket', 'slug_nama', 'kategori', 'range_min', 'range_max', 'sorting', 'cek_kategori'));
            }

        }
        