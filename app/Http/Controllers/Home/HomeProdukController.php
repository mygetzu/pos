<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Gloudemans\Shoppingcart\Facades\Cart;
use Auth;
use Response;
use PDF;
use Hashids\Hashids;
use stdClass;
use Illuminate\Pagination\Paginator;
use Input;
use Session;
use DateTime;
use App\Models\Produk\Produk;
use App\Models\Produk\KategoriProduk;
use App\Models\Pelanggan\Pelanggan;
use App\Models\Transaksi\SOHeader;
use App\Models\Transaksi\SODetail;
use App\Models\Transaksi\SOVoucher;
use App\Models\Transaksi\Invoice;
use App\Models\Transaksi\InvoiceDetail;
use App\Models\Transaksi\BayarInvoice;
use App\Models\Perusahaan;
use App\Models\PaketProduk\Paket;
use App\Models\PaketProduk\PaketProduk;
use App\Models\Referensi\Bank;
use Illuminate\Support\Collection;
use App\Models\AdminEcommerce\MiniBanner;
use App\Models\AdminEcommerce\ProdukPilihan;
use App\Models\AdminEcommerce\PaketPilihan;
use App\Models\AdminEcommerce\Konten;
use App\Models\AdminEcommerce\ProdukBaru;
use App\Models\AdminEcommerce\ProdukAkanDatang;

class HomeProdukController extends Controller
{
    public function semua_kategori(Request $request)
    {
        $kategori_pelanggan_id  = 0;

        $range_harga    = Input::get('range_harga');
        $range_min      = "10000";
        $range_max      = "30000000";
        $sorting        = Input::get('sorting');

        if(Auth::check()){
            $pelanggan              = Pelanggan::with('kategori_pelanggan')->where('user_id', Auth::user()->id)->first();
            if(!empty($pelanggan))
                $kategori_pelanggan_id  = $pelanggan->kategori_id;
        }

        if(!empty($range_harga)){
            $range_harga    = explode(';', $range_harga);
            $range_min      = $range_harga[0];
            $range_max      = $range_harga[1];

            $obj_produk = Produk::with(['produk_galeri', 'promo_diskon', 'promo_cashback', 'promo_hadiah' => function($query) 
            {return $query->with('hadiah');}, 'produk_harga' 
            => function ($query) use ($kategori_pelanggan_id) 
            {$query->where('kategori_pelanggan_id', $kategori_pelanggan_id); }])
            ->where('is_aktif', 1)->where('tmst_produk.harga_retail', '>=', (int)$range_min)->where('tmst_produk.harga_retail', '<=', (int)$range_max)->orderBy('nama', 'asc')->get();
        }
        else{
            $obj_produk = Produk::with(['produk_galeri', 'promo_diskon', 'promo_cashback', 'promo_hadiah' => function($query) 
            {return $query->with('hadiah');}, 'produk_harga' 
            => function ($query) use ($kategori_pelanggan_id) 
            {$query->where('kategori_pelanggan_id', $kategori_pelanggan_id); }])
            ->where('is_aktif', 1)->orderBy('nama', 'asc')->get();
        }
        
        $slug_nama = "";
        $kategori = "Semua Kategori";

        $obj_gambar_produk_default   = Konten::where('nama', 'gambar_produk_default')->first();
        if(empty($obj_gambar_produk_default)){
            $gambar_produk_default = "";
        }
        else{
            $gambar_produk_default  = $obj_gambar_produk_default->keterangan;
        }

        $produk = [];
        foreach ($obj_produk as $key => $val) {
            $produk[$key]['slug_nama']    = $val->slug_nama;
            $produk[$key]['nama']         = $val->nama;
            $produk[$key]['harga_retail'] = $val->harga_retail;

            if(!empty($val->produk_galeri_first->file_gambar)){
                $produk[$key]['file_gambar'] = $val->produk_galeri_first->file_gambar;
            }
            else{
                $produk[$key]['file_gambar'] = $gambar_produk_default;
            }

            $produk[$key]['diskon']             = 0; 
            $produk[$key]['qty_beli_diskon']    = 1;
            $produk[$key]['cashback']           = 0; 
            $produk[$key]['qty_beli_cashback']  = 1;
            $produk[$key]['hadiah']             = "";
            $produk[$key]['qty_beli_hadiah']    = 1;
            $produk[$key]['qty_hadiah']         = 1;
            $produk[$key]['stok_tersedia']      = $val->stok - $val->stok_dipesan;

            if(!empty($pelanggan->kategori_pelanggan)){
                if($pelanggan->kategori_pelanggan->default_diskon > $produk[$key]['diskon']){
                    $produk[$key]['diskon'] = $pelanggan->kategori_pelanggan->default_diskon;
                }
            }
            
            if(!empty($val->produk_harga)){
                if($val->produk_harga->diskon > $produk[$key]['diskon']){
                    $produk[$key]['diskon'] = $val->produk_harga->diskon;
                }
            }
            
            if(!empty($val->promo_diskon)){
                if($val->promo_diskon->diskon > $produk[$key]['diskon']){
                    $produk[$key]['diskon'] = $val->promo_diskon->diskon;
                    $produk[$key]['qty_beli_diskon'] = $val->promo_diskon->qty_beli;
                }
            }

            if(!empty($val->promo_cashback) && empty($val->produk_harga) && empty($pelanggan->kategori_pelanggan))
            {
                $produk[$key]['cashback']           = $val->promo_cashback->cashback;
                $produk[$key]['qty_beli_cashback']  = $val->promo_cashback->qty_beli;
            }
            
            if(!empty($val->promo_hadiah))
            {
                $produk[$key]['hadiah']             = $val->promo_hadiah->hadiah->nama;
                $produk[$key]['qty_beli_hadiah']    = $val->promo_hadiah->qty_beli;
                $produk[$key]['qty_hadiah']         = $val->promo_hadiah->qty_hadiah;
            }

            $produk[$key]['harga_akhir'] = $val->harga_retail;

            if($produk[$key]['qty_beli_diskon'] == 1){
                $produk[$key]['harga_akhir'] = $produk[$key]['harga_akhir'] - ($produk[$key]['harga_akhir']*$produk[$key]['diskon']/100);
            }

            if($produk[$key]['qty_beli_cashback'] == 1){
                $produk[$key]['harga_akhir'] = $produk[$key]['harga_akhir'] - $produk[$key]['cashback'];
            }
        }
        
        if($sorting == 2){
            $nama = array();
            foreach ($produk as $key => $row)
            {
                $nama[$key] = $row['slug_nama'];
            }

            array_multisort($nama, SORT_DESC, SORT_STRING, $produk);
        }
        elseif($sorting == 3){
            $harga = array();
            foreach ($produk as $key => $row)
            {
                $harga[$key] = $row['harga_akhir'];
            }

            array_multisort($harga, SORT_ASC, $produk);
        }
        elseif($sorting == 4){
            $harga = array();
            foreach ($produk as $key => $row)
            {
                $harga[$key] = $row['harga_akhir'];
            }

            array_multisort($harga, SORT_DESC, $produk);
        }
        else{
            $nama = array();
            foreach ($produk as $key => $row)
            {
                $nama[$key] = $row['slug_nama'];
            }

            array_multisort($nama, SORT_ASC, SORT_STRING, $produk);
        }

        $currentPage                = LengthAwarePaginator::resolveCurrentPage();
        $col                        = new Collection($produk);
        $perPage                    = 15;
        $currentPageSearchResults   = $col->slice(($currentPage - 1) * $perPage, $perPage)->all();
        $produk                     = new LengthAwarePaginator($currentPageSearchResults, count($col), $perPage);
        $url = $request->fullUrl();
        $produk->setPath($url);

        // $kategori_produk = KategoriProduk::where('is_aktif', 1)->get();

        // $cek_kategori = [];
        // foreach ($kategori_produk as $key => $value) {
        //     $cek_kategori[$key]['id']       = $value->id;
        //     $cek_kategori[$key]['nama']     = $value->nama;
        //     $cek_kategori[$key]['checked']  = 1;
        // }
        
        return view('pages.home.list_produk', compact('produk', 'slug_nama', 'kategori', 'range_min', 'range_max', 'sorting', 'cek_kategori'));
    }

    public function kategori(Request $request, $slug_nama)
    {
        $kategori_pelanggan_id  = 0;

        if(Auth::check()){
            $pelanggan              = Pelanggan::with('kategori_pelanggan')->where('user_id', Auth::user()->id)->first();
            if(!empty($pelanggan))
                $kategori_pelanggan_id  = $pelanggan->kategori_id;
        }

        $range_harga = Input::get('range_harga');
        $range_min      = "10000";
        $range_max      = "30000000";
        $sorting        = Input::get('sorting');

        if(!empty($range_harga)){
            $range_harga    = explode(';', $range_harga);
            $range_min      = $range_harga[0];
            $range_max      = $range_harga[1];

            $obj_produk = Produk::select('tmst_produk.*')->with(['produk_galeri', 'promo_diskon', 'promo_cashback', 'promo_hadiah' => function($query) 
            {return $query->with('hadiah');}, 'produk_harga' 
            => function ($query) use ($kategori_pelanggan_id) 
            {$query->where('kategori_pelanggan_id', $kategori_pelanggan_id); }])
            ->join('tmst_kategori_produk', 'tmst_kategori_produk.id', '=', 'tmst_produk.kategori_id')
            ->where('tmst_produk.is_aktif', 1)->where('tmst_kategori_produk.slug_nama', $slug_nama)->where('tmst_produk.harga_retail', '>=', (int)$range_min)->where('tmst_produk.harga_retail', '<=', (int)$range_max)->orderBy('tmst_produk.nama', 'asc')->get();
        }
        else{
            $obj_produk = Produk::select('tmst_produk.*')->with(['produk_galeri', 'promo_diskon', 'promo_cashback', 'promo_hadiah' => function($query) 
            {return $query->with('hadiah');}, 'produk_harga' 
            => function ($query) use ($kategori_pelanggan_id) 
            {$query->where('kategori_pelanggan_id', $kategori_pelanggan_id); }])
            ->join('tmst_kategori_produk', 'tmst_kategori_produk.id', '=', 'tmst_produk.kategori_id')
            ->where('tmst_produk.is_aktif', 1)->where('tmst_kategori_produk.slug_nama', $slug_nama)->orderBy('tmst_produk.nama', 'asc')->get();
        }

        $kategori_produk = KategoriProduk::where('slug_nama', $slug_nama)->first();
        $kategori = $kategori_produk->nama;

        $obj_gambar_produk_default   = Konten::where('nama', 'gambar_produk_default')->first();
        if(empty($obj_gambar_produk_default)){
            $gambar_produk_default = "";
        }
        else{
            $gambar_produk_default  = $obj_gambar_produk_default->keterangan;
        }

        $produk = [];
        foreach ($obj_produk as $key => $val) {
            $produk[$key]['slug_nama']    = $val->slug_nama;
            $produk[$key]['nama']         = $val->nama;
            $produk[$key]['harga_retail'] = $val->harga_retail;

            if(!empty($val->produk_galeri_first->file_gambar)){
                $produk[$key]['file_gambar'] = $val->produk_galeri_first->file_gambar;
            }
            else{
                $produk[$key]['file_gambar'] = $gambar_produk_default;
            }

            $produk[$key]['diskon']             = 0; 
            $produk[$key]['qty_beli_diskon']    = 1;
            $produk[$key]['cashback']           = 0; 
            $produk[$key]['qty_beli_cashback']  = 1;
            $produk[$key]['hadiah']             = "";
            $produk[$key]['qty_beli_hadiah']    = 1;
            $produk[$key]['qty_hadiah']         = 1;
            $produk[$key]['stok_tersedia']      = $val->stok - $val->stok_dipesan;

            if(!empty($pelanggan->kategori_pelanggan)){
                if($pelanggan->kategori_pelanggan->default_diskon > $produk[$key]['diskon']){
                    $produk[$key]['diskon'] = $pelanggan->kategori_pelanggan->default_diskon;
                }
            }
            
            if(!empty($val->produk_harga)){
                if($val->produk_harga->diskon > $produk[$key]['diskon']){
                    $produk[$key]['diskon'] = $val->produk_harga->diskon;
                }
            }
            
            if(!empty($val->promo_diskon)){
                if($val->promo_diskon->diskon > $produk[$key]['diskon']){
                    $produk[$key]['diskon'] = $val->promo_diskon->diskon;
                    $produk[$key]['qty_beli_diskon'] = $val->promo_diskon->qty_beli;
                }
            }

            if(!empty($val->promo_cashback) && empty($val->produk_harga) && empty($pelanggan->kategori_pelanggan))
            {
                $produk[$key]['cashback']           = $val->promo_cashback->cashback;
                $produk[$key]['qty_beli_cashback']  = $val->promo_cashback->qty_beli;
            }
            
            if(!empty($val->promo_hadiah))
            {
                $produk[$key]['hadiah']             = $val->promo_hadiah->hadiah->nama;
                $produk[$key]['qty_beli_hadiah']    = $val->promo_hadiah->qty_beli;
                $produk[$key]['qty_hadiah']         = $val->promo_hadiah->qty_hadiah;
            }

            $produk[$key]['harga_akhir'] = $val->harga_retail;

            if($produk[$key]['qty_beli_diskon'] == 1){
                $produk[$key]['harga_akhir'] = $produk[$key]['harga_akhir'] - ($produk[$key]['harga_akhir']*$produk[$key]['diskon']/100);
            }

            if($produk[$key]['qty_beli_cashback'] == 1){
                $produk[$key]['harga_akhir'] = $produk[$key]['harga_akhir'] - $produk[$key]['cashback'];
            }
        }
        
        if($sorting == 2){
            $nama = array();
            foreach ($produk as $key => $row)
            {
                $nama[$key] = $row['slug_nama'];
            }

            array_multisort($nama, SORT_DESC, SORT_STRING, $produk);
        }
        elseif($sorting == 3){
            $harga = array();
            foreach ($produk as $key => $row)
            {
                $harga[$key] = $row['harga_akhir'];
            }

            array_multisort($harga, SORT_ASC, $produk);
        }
        elseif($sorting == 4){
            $harga = array();
            foreach ($produk as $key => $row)
            {
                $harga[$key] = $row['harga_akhir'];
            }

            array_multisort($harga, SORT_DESC, $produk);
        }
        else{
            $nama = array();
            foreach ($produk as $key => $row)
            {
                $nama[$key] = $row['slug_nama'];
            }

            array_multisort($nama, SORT_ASC, SORT_STRING, $produk);
        }

        $currentPage                = LengthAwarePaginator::resolveCurrentPage();
        $col                        = new Collection($produk);
        $perPage                    = 15;
        $currentPageSearchResults   = $col->slice(($currentPage - 1) * $perPage, $perPage)->all();
        $produk                     = new LengthAwarePaginator($currentPageSearchResults, count($col), $perPage);
        $url = $request->fullUrl();
        $produk->setPath($url);

        //load mini banner

        return view('pages.home.list_produk', compact('produk', 'gambar_default', 'slug_nama', 'kategori', 'range_min', 'range_max', 'sorting'));
    }

    public function produk($slug_nama)
    {    
        $kategori_pelanggan_id  = 0;

        if(Auth::check()){
            $pelanggan              = Pelanggan::with('kategori_pelanggan')->where('user_id', Auth::user()->id)->first();
            if(!empty($pelanggan))
                $kategori_pelanggan_id  = $pelanggan->kategori_id;
        }

        $produk = Produk::with(['produk_galeri', 'promo_diskon', 'promo_cashback', 'promo_hadiah' => function($query) 
            {return $query->with('hadiah');}, 'produk_harga' 
            => function ($query) use ($kategori_pelanggan_id) 
            {$query->where('kategori_pelanggan_id', $kategori_pelanggan_id); }])
            ->where('is_aktif', 1)->where('slug_nama', $slug_nama)->orderBy('nama', 'asc')->first();

        $obj_gambar_produk_default   = Konten::where('nama', 'gambar_produk_default')->first();
        if(empty($obj_gambar_produk_default)){
            $gambar_produk_default = "";
        }
        else{
            $gambar_produk_default  = $obj_gambar_produk_default->keterangan;
        }

        $kategori_produk = KategoriProduk::where('id', $produk->kategori_id)->first();

        $kategori = $kategori_produk->nama;

        return view('pages.home.detail_list_produk', compact('produk', 'pelanggan', 'gambar_default', 'kategori', 'produk', 'gambar_produk_default'));
    }

    public function search(Request $request)
    {
        $search = Input::get('search');
        $kategori_pelanggan_id  = 0;
        $sorting = Input::get('sorting');

        if(Auth::check()){
            $pelanggan              = Pelanggan::with('kategori_pelanggan')->where('user_id', Auth::user()->id)->first();
            if(!empty($pelanggan))
                $kategori_pelanggan_id  = $pelanggan->kategori_id;
        }

        $range_harga    = Input::get('range_harga');
        $range_min      = "10000";
        $range_max      = "30000000";

        if(!empty($range_harga)){
            $range_harga    = explode(';', $range_harga);
            $range_min      = $range_harga[0];
            $range_max      = $range_harga[1];

            $obj_produk = Produk::with(['produk_galeri', 'promo_diskon', 'promo_cashback', 'promo_hadiah' => function($query) 
            {return $query->with('hadiah');}, 'produk_harga' 
            => function ($query) use ($kategori_pelanggan_id) 
            {$query->where('kategori_pelanggan_id', $kategori_pelanggan_id); }])
            ->where('is_aktif', 1)->where('nama', 'like', '%'.$search.'%')->where('tmst_produk.harga_retail', '>=', (int)$range_min)->where('tmst_produk.harga_retail', '<=', (int)$range_max)->orderBy('nama', 'asc')->get();
        }else{
            $obj_produk = Produk::with(['produk_galeri', 'promo_diskon', 'promo_cashback', 'promo_hadiah' => function($query) 
            {return $query->with('hadiah');}, 'produk_harga' 
            => function ($query) use ($kategori_pelanggan_id) 
            {$query->where('kategori_pelanggan_id', $kategori_pelanggan_id); }])
            ->where('is_aktif', 1)->where('nama', 'like', '%'.$search.'%')->orderBy('nama', 'asc')->get();
        }

        if(!empty($produk->id)){
            $kategori_produk = KategoriProduk::where('id', $produk[0]->kategori_id)->first();
            $kategori = $kategori_produk->id;
            $slug_nama = $kategori_produk->slug_nama;
        }
        else{
            $kategori = "";
            $slug_nama = "";
        }

        $obj_gambar_produk_default   = Konten::where('nama', 'gambar_produk_default')->first();
        if(empty($obj_gambar_produk_default)){
            $gambar_produk_default = "";
        }
        else{
            $gambar_produk_default  = $obj_gambar_produk_default->keterangan;
        }

        $produk = [];
        foreach ($obj_produk as $key => $val) {
            $produk[$key]['slug_nama']    = $val->slug_nama;
            $produk[$key]['nama']         = $val->nama;
            $produk[$key]['harga_retail'] = $val->harga_retail;

            if(!empty($val->produk_galeri_first->file_gambar)){
                $produk[$key]['file_gambar'] = $val->produk_galeri_first->file_gambar;
            }
            else{
                $produk[$key]['file_gambar'] = $gambar_produk_default;
            }

            $produk[$key]['diskon']             = 0; 
            $produk[$key]['qty_beli_diskon']    = 1;
            $produk[$key]['cashback']           = 0; 
            $produk[$key]['qty_beli_cashback']  = 1;
            $produk[$key]['hadiah']             = "";
            $produk[$key]['qty_beli_hadiah']    = 1;
            $produk[$key]['qty_hadiah']         = 1;
            $produk[$key]['stok_tersedia']      = $val->stok - $val->stok_dipesan;

            if(!empty($pelanggan->kategori_pelanggan)){
                if($pelanggan->kategori_pelanggan->default_diskon > $produk[$key]['diskon']){
                    $produk[$key]['diskon'] = $pelanggan->kategori_pelanggan->default_diskon;
                }
            }
            
            if(!empty($val->produk_harga)){
                if($val->produk_harga->diskon > $produk[$key]['diskon']){
                    $produk[$key]['diskon'] = $val->produk_harga->diskon;
                }
            }
            
            if(!empty($val->promo_diskon)){
                if($val->promo_diskon->diskon > $produk[$key]['diskon']){
                    $produk[$key]['diskon'] = $val->promo_diskon->diskon;
                    $produk[$key]['qty_beli_diskon'] = $val->promo_diskon->qty_beli;
                }
            }

            if(!empty($val->promo_cashback) && empty($val->produk_harga) && empty($pelanggan->kategori_pelanggan))
            {
                $produk[$key]['cashback']           = $val->promo_cashback->cashback;
                $produk[$key]['qty_beli_cashback']  = $val->promo_cashback->qty_beli;
            }
            
            if(!empty($val->promo_hadiah))
            {
                $produk[$key]['hadiah']             = $val->promo_hadiah->hadiah->nama;
                $produk[$key]['qty_beli_hadiah']    = $val->promo_hadiah->qty_beli;
                $produk[$key]['qty_hadiah']         = $val->promo_hadiah->qty_hadiah;
            }

            $produk[$key]['harga_akhir'] = $val->harga_retail;

            if($produk[$key]['qty_beli_diskon'] == 1){
                $produk[$key]['harga_akhir'] = $produk[$key]['harga_akhir'] - ($produk[$key]['harga_akhir']*$produk[$key]['diskon']/100);
            }

            if($produk[$key]['qty_beli_cashback'] == 1){
                $produk[$key]['harga_akhir'] = $produk[$key]['harga_akhir'] - $produk[$key]['cashback'];
            }
        }
        
        if($sorting == 2){
            $nama = array();
            foreach ($produk as $key => $row)
            {
                $nama[$key] = $row['slug_nama'];
            }

            array_multisort($nama, SORT_DESC, SORT_STRING, $produk);
        }
        elseif($sorting == 3){
            $harga = array();
            foreach ($produk as $key => $row)
            {
                $harga[$key] = $row['harga_akhir'];
            }

            array_multisort($harga, SORT_ASC, $produk);
        }
        elseif($sorting == 4){
            $harga = array();
            foreach ($produk as $key => $row)
            {
                $harga[$key] = $row['harga_akhir'];
            }

            array_multisort($harga, SORT_DESC, $produk);
        }
        else{
            $nama = array();
            foreach ($produk as $key => $row)
            {
                $nama[$key] = $row['slug_nama'];
            }

            array_multisort($nama, SORT_ASC, SORT_STRING, $produk);
        }

        $currentPage                = LengthAwarePaginator::resolveCurrentPage();
        $col                        = new Collection($produk);
        $perPage                    = 15;
        $currentPageSearchResults   = $col->slice(($currentPage - 1) * $perPage, $perPage)->all();
        $produk                     = new LengthAwarePaginator($currentPageSearchResults, count($col), $perPage);
        $url = $request->fullUrl();
        $produk->setPath($url);

        return view('pages.home.list_produk', compact('produk', 'search', 'gambar_default', 'kategori', 'slug_nama', 'range_min', 'range_max', 'sorting'));
    }
}
