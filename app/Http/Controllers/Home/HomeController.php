<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Gloudemans\Shoppingcart\Facades\Cart;
use Auth;
use Response;
use PDF;
use Hashids;
use stdClass;
use Illuminate\Pagination\Paginator;
use Input;
use Session;
use DateTime;
use App\Models\Produk\Produk;
use App\Models\Produk\KategoriProduk;
use App\Models\Pelanggan\Pelanggan;
use App\Models\Transaksi\SOHeader;
use App\Models\Transaksi\SODetail;
use App\Models\Transaksi\SOVoucher;
use App\Models\Transaksi\Invoice;
use App\Models\Transaksi\InvoiceDetail;
use App\Models\Transaksi\BayarInvoice;
use App\Models\Perusahaan;
use App\Models\PaketProduk\Paket;
use App\Models\PaketProduk\PaketProduk;
use App\Models\Referensi\Bank;
use Illuminate\Support\Collection;
use App\Models\AdminEcommerce\MiniBanner;
use App\Models\AdminEcommerce\ProdukPilihan;
use App\Models\AdminEcommerce\PaketPilihan;
use App\Models\AdminEcommerce\Konten;
use App\Models\AdminEcommerce\ProdukBaru;
use App\Models\AdminEcommerce\ProdukAkanDatang;

class HomeController extends Controller {
    
    public function index(Request $request) {
        $perusahaan = Perusahaan::first();
        $kategori_pelanggan_id = 0;

        if (Auth::check()) {
            $pelanggan = Pelanggan::with('kategori_pelanggan')->where('user_id', Auth::user()->id)->first();
            if (!empty($pelanggan))
                $kategori_pelanggan_id = $pelanggan->kategori_id;
        }

        $slider_promo = DB::table('tran_slider_promo')->orderBy('item_order', 'asc')->get();
        $mini_banner = MiniBanner::take(4)->get(); //sementara statis, nanti bisa diklik

        $obj_produk_pilihan = ProdukPilihan::orderBy('item_order', 'asc')->get();
        $produk_pilihan = [];
        $obj_gambar_produk_default = Konten::where('nama', 'gambar_produk_default')->first();
        if (empty($obj_gambar_produk_default)) {
            $gambar_produk_default = "";
        } else {
            $gambar_produk_default = $obj_gambar_produk_default->keterangan;
        }

        foreach ($obj_produk_pilihan as $key => $value) {
            $produk_pilihan[$key]['id'] = $value->id;
            $produk_pilihan[$key]['nama'] = $value->nama;
            $produk_pilihan[$key]['url'] = $value->url;
            $produk_pilihan[$key]['file_gambar'] = $value->file_gambar;

            $arr_list_produk = $value->list_produk;
            $arr_list_produk = explode(',', $arr_list_produk);

            $list_produk = [];
            for ($j = 0; $j < count($arr_list_produk); $j++) {
                $obj_produk = Produk::with(['produk_galeri_first', 'promo_diskon', 'promo_cashback', 'promo_hadiah'
                            => function($query) {
                                return $query->with('hadiah');
                            }, 'produk_harga'
                            => function ($query) use ($kategori_pelanggan_id) {
                                $query->where('kategori_pelanggan_id', $kategori_pelanggan_id);
                            }])->where('is_aktif', 1)->where('id', $arr_list_produk[$j])->first();

                $list_produk[$j]['slug_nama'] = $obj_produk->slug_nama;
                $list_produk[$j]['nama'] = $obj_produk->nama;
                $list_produk[$j]['harga_retail'] = $obj_produk->harga_retail;

                if (!empty($obj_produk->produk_galeri_first->file_gambar)) {
                    $list_produk[$j]['file_gambar'] = $obj_produk->produk_galeri_first->file_gambar;
                } else {
                    $list_produk[$j]['file_gambar'] = $gambar_produk_default;
                }

                $list_produk[$j]['diskon'] = 0;
                $list_produk[$j]['qty_beli_diskon'] = 1;
                $list_produk[$j]['cashback'] = 0;
                $list_produk[$j]['qty_beli_cashback'] = 1;
                $list_produk[$j]['hadiah'] = "";
                $list_produk[$j]['qty_beli_hadiah'] = 1;
                $list_produk[$j]['qty_hadiah'] = 1;
                $list_produk[$j]['stok_tersedia'] = $obj_produk->stok - $obj_produk->stok_dipesan;

                if (!empty($pelanggan->kategori_pelanggan)) {
                    if ($pelanggan->kategori_pelanggan->default_diskon > $list_produk[$j]['diskon']) {
                        $list_produk[$j]['diskon'] = $pelanggan->kategori_pelanggan->default_diskon;
                    }
                }

                if (!empty($obj_produk->produk_harga)) {
                    if ($obj_produk->produk_harga->diskon > $list_produk[$j]['diskon']) {
                        $list_produk[$j]['diskon'] = $obj_produk->produk_harga->diskon;
                    }
                }

                if (!empty($obj_produk->promo_diskon)) {
                    if ($obj_produk->promo_diskon->diskon > $list_produk[$j]['diskon']) {
                        $list_produk[$j]['diskon'] = $obj_produk->promo_diskon->diskon;
                        $list_produk[$j]['qty_beli_diskon'] = $obj_produk->promo_diskon->qty_beli;
                    }
                }

                if (!empty($obj_produk->promo_cashback) && empty($obj_produk->produk_harga) && empty($pelanggan->kategori_pelanggan)) {
                    $list_produk[$j]['cashback'] = $obj_produk->promo_cashback->cashback;
                    $list_produk[$j]['qty_beli_cashback'] = $obj_produk->promo_cashback->qty_beli;
                }$kategori_pelanggan_id = 0;
        if(Auth::check()){
            $pelanggan              = Pelanggan::with('kategori_pelanggan')->where('user_id', Auth::user()->id)->first();
            if(!empty($pelanggan))
                $kategori_pelanggan_id  = $pelanggan->kategori_id;
        }

                if (!empty($obj_produk->promo_hadiah)) {
                    $list_produk[$j]['hadiah'] = $obj_produk->promo_hadiah->hadiah->nama;
                    $list_produk[$j]['qty_beli_hadiah'] = $obj_produk->promo_hadiah->qty_beli;
                    $list_produk[$j]['qty_hadiah'] = $obj_produk->promo_hadiah->qty_hadiah;
                }
            }

            $produk_pilihan[$key]['list_produk'] = $list_produk;
        }

        $obj_produk_baru = ProdukBaru::with(['produk' => function($query) use ($kategori_pelanggan_id) {
                        return $query->with(['produk_galeri_first', 'promo_diskon', 'promo_cashback', 'promo_hadiah' => function($query) {
                                        return $query->with('hadiah');
                                    }, 'produk_harga'
                                    => function ($query) use ($kategori_pelanggan_id) {
                                        $query->where('kategori_pelanggan_id', $kategori_pelanggan_id);
                                    }])->where('is_aktif', 1);
                    }
                        ])->get();

                $produk_baru = [];
                foreach ($obj_produk_baru as $key => $val) {
                    $produk_baru[$key]['slug_nama'] = $val->produk->slug_nama;
                    $produk_baru[$key]['nama'] = $val->produk->nama;
                    $produk_baru[$key]['harga_retail'] = $val->produk->harga_retail;

                    if (!empty($val->produk->produk_galeri_first->file_gambar)) {
                        $produk_baru[$key]['file_gambar'] = $val->produk->produk_galeri_first->file_gambar;
                    } else {
                        $produk_baru[$key]['file_gambar'] = $gambar_produk_default;
                    }

                    $produk_baru[$key]['diskon'] = 0;
                    $produk_baru[$key]['qty_beli_diskon'] = 1;
                    $produk_baru[$key]['cashback'] = 0;
                    $produk_baru[$key]['qty_beli_cashback'] = 1;
                    $produk_baru[$key]['hadiah'] = "";
                    $produk_baru[$key]['qty_beli_hadiah'] = 1;
                    $produk_baru[$key]['qty_hadiah'] = 1;
                    $produk_baru[$key]['stok_tersedia'] = $val->produk->stok - $val->produk->stok_dipesan;

                    if (!empty($pelanggan->kategori_pelanggan)) {
                        if ($pelanggan->kategori_pelanggan->default_diskon > $produk_baru[$key]['diskon']) {
                            $produk_baru[$key]['diskon'] = $pelanggan->kategori_pelanggan->default_diskon;
                        }
                    }

                    if (!empty($val->produk->produk_harga)) {
                        if ($val->produk->produk_harga->diskon > $produk_baru[$key]['diskon']) {
                            $produk_baru[$key]['diskon'] = $val->produk->produk_harga->diskon;
                        }
                    }

                    if (!empty($val->produk->promo_diskon)) {
                        if ($val->produk->promo_diskon->diskon > $produk_baru[$key]['diskon']) {
                            $produk_baru[$key]['diskon'] = $val->produk->promo_diskon->diskon;
                            $produk_baru[$key]['qty_beli_diskon'] = $val->produk->promo_diskon->qty_beli;
                        }
                    }

                    if (!empty($val->produk->promo_cashback) && empty($val->produk->produk_harga) && empty($pelanggan->kategori_pelanggan)) {
                        $produk_baru[$key]['cashback'] = $val->produk->promo_cashback->cashback;
                        $produk_baru[$key]['qty_beli_cashback'] = $val->produk->promo_cashback->qty_beli;
                    }

                    if (!empty($val->produk->promo_hadiah)) {
                        $produk_baru[$key]['hadiah'] = $val->produk->promo_hadiah->hadiah->nama;
                        $produk_baru[$key]['qty_beli_hadiah'] = $val->produk->promo_hadiah->qty_beli;
                        $produk_baru[$key]['qty_hadiah'] = $val->produk->promo_hadiah->qty_hadiah;
                    }
                }

                $obj_produk_akan_datang = ProdukAkanDatang::with(['produk' => function($query) use ($kategori_pelanggan_id) {
                                return $query->with(['produk_galeri_first', 'promo_diskon', 'promo_cashback', 'promo_hadiah'
                                            => function($query) {
                                                return $query->with('hadiah');
                                            }, 'produk_harga'
                                            => function ($query) use ($kategori_pelanggan_id) {
                                                $query->where('kategori_pelanggan_id', $kategori_pelanggan_id);
                                            }])->where('is_aktif', 1);
                            }])->get();

                        $produk_akan_datang = [];
                        foreach ($obj_produk_akan_datang as $key => $val) {
                            $produk_akan_datang[$key]['slug_nama'] = $val->produk->slug_nama;
                            $produk_akan_datang[$key]['nama'] = $val->produk->nama;
                            $produk_akan_datang[$key]['harga_retail'] = $val->produk->harga_retail;

                            if (!empty($val->produk->produk_galeri_first->file_gambar)) {
                                $produk_akan_datang[$key]['file_gambar'] = $val->produk->produk_galeri_first->file_gambar;
                            } else {
                                $produk_akan_datang[$key]['file_gambar'] = $gambar_produk_default;
                            }

                            $produk_akan_datang[$key]['diskon'] = 0;
                            $produk_akan_datang[$key]['qty_beli_diskon'] = 1;
                            $produk_akan_datang[$key]['cashback'] = 0;
                            $produk_akan_datang[$key]['qty_beli_cashback'] = 1;
                            $produk_akan_datang[$key]['hadiah'] = "";
                            $produk_akan_datang[$key]['qty_beli_hadiah'] = 1;
                            $produk_akan_datang[$key]['qty_hadiah'] = 1;
                            $produk_akan_datang[$key]['stok_tersedia'] = $val->produk->stok - $val->produk->stok_dipesan;

                            if (!empty($pelanggan->kategori_pelanggan)) {
                                if ($pelanggan->kategori_pelanggan->default_diskon > $produk_akan_datang[$key]['diskon']) {
                                    $produk_akan_datang[$key]['diskon'] = $pelanggan->kategori_pelanggan->default_diskon;
                                }
                            }

                            if (!empty($val->produk->produk_harga)) {
                                if ($val->produk->produk_harga->diskon > $produk_akan_datang[$key]['diskon']) {
                                    $produk_akan_datang[$key]['diskon'] = $val->produk->produk_harga->diskon;
                                }
                            }

                            if (!empty($val->produk->promo_diskon)) {
                                if ($val->produk->promo_diskon->diskon > $produk_akan_datang[$key]['diskon']) {
                                    $produk_akan_datang[$key]['diskon'] = $val->produk->promo_diskon->diskon;
                                    $produk_akan_datang[$key]['qty_beli_diskon'] = $val->produk->promo_diskon->qty_beli;
                                }
                            }

                            if (!empty($val->produk->promo_cashback) && empty($val->produk->produk_harga) && empty($pelanggan->kategori_pelanggan)) {
                                $produk_akan_datang[$key]['cashback'] = $val->produk->promo_cashback->cashback;
                                $produk_akan_datang[$key]['qty_beli_cashback'] = $val->produk->promo_cashback->qty_beli;
                            }

                            if (!empty($val->produk->promo_hadiah)) {
                                $produk_akan_datang[$key]['hadiah'] = $val->produk->promo_hadiah->hadiah->nama;
                                $produk_akan_datang[$key]['qty_beli_hadiah'] = $val->produk->promo_hadiah->qty_beli;
                                $produk_akan_datang[$key]['qty_hadiah'] = $val->produk->promo_hadiah->qty_hadiah;
                            }
                        }

                        // return Response::json($produk_pilihan);
                        return view('pages.home.home', compact('slider_promo', 'mini_banner', 'produk_pilihan', 'produk_baru', 'produk_akan_datang'));
                    }

    public function cart() {
        $cart_content = Cart::content(1);
        return view('pages.home.cart', compact('cart_content'));
    }

    public function review_pesanan(Request $request) {
        if (!Auth::check()) {
            $request->session()->flash('message', 'Anda harus login terlebih dahulu');
            return redirect('/cart');
        }

        $cart_content = Cart::content(1);

        return view('pages.home.review_pesanan', compact('cart_content'));
    }

    public function checkout_home(Request $request) {
        /*         * ******* SO HEADER ******** */

        $nomor_invoice = DB::table('tref_nomor_invoice')->where('id', 2)->first();
        $format_invoice = $nomor_invoice->format;
        $no_invoice = $format_invoice . $this->generate_no_nota();

        date_default_timezone_set("Asia/Jakarta");
        $tanggal = new DateTime();
        $pelanggan = Pelanggan::where('user_id', Auth::user()->id)->first();

        if (empty($pelanggan)) {
            $request->session()->flash('message', 'Anda harus login sebagai pelanggan terlebih dahulu');
            return redirect('/checkout/review_pesanan');
        }

        $so_header = new SOHeader;

        $so_header->no_nota = $no_invoice;
        $so_header->tanggal = $tanggal->format('Y-m-d G:i:s');
        $so_header->pelanggan_id = $pelanggan->id;
        $so_header->due_date = "";
        $so_header->catatan = Input::get('catatan');

        $total_tagihan = 0;

        /*         * ******* SO DETAIL ******** */

        $cart_content = Cart::content(1);

        if (empty($cart_content)) {
            $request->session()->flash('message', 'Keranjang belanja kosong');
            return redirect('/checkout/review_pesanan');
        }

        foreach ($cart_content as $cart) {
            $produk_id = $cart->id;

            if (substr($produk_id, 0, 3) == 'VOU') {
                $so_voucher = new SOVoucher;

                $so_voucher->no_nota = $no_invoice;
                $so_voucher->voucher_id = substr($produk_id, 3);
                $so_voucher->nominal = (int) $cart->price * -1;

                $total_tagihan = $total_tagihan - $so_voucher->nominal;
            } else {
                $so_detail = new SODetail;

                $so_detail->no_nota = $no_invoice;

                if (substr($produk_id, 0, 3) == 'DIS' || substr($produk_id, 0, 3) == 'PKT' || substr($produk_id, 0, 3) == 'HDH' || substr($produk_id, 0, 3) == 'CAS') {
                    $produk_id = substr($produk_id, 3);
                }

                $so_detail->produk_id = $produk_id;
                $so_detail->quantity = $cart->qty;
                $so_detail->harga = $cart->price;

                $total_tagihan = $total_tagihan + ($cart->qty * $cart->price);

                $produk = Produk::where('id', $produk_id)->first();

                $so_detail->deskripsi = $produk->deskripsi;
                $so_detail->save();
            }
        }

        $so_header->total_tagihan = $total_tagihan;
        $so_header->save();

        Cart::destroy();


        /*         * ******* INVOICE DETAIL ******** */

        $invoice_detail = new InvoiceDetail;

        $invoice_detail->invoice_no = $no_invoice;
        $invoice_detail->no_nota = $no_invoice; //disamakan saja
        $invoice_detail->total_tagihan = $total_tagihan;
        $invoice_detail->save();

        /*         * ******* INVOICE ******** */
        $besok = new DateTime('tomorrow');

        $invoice = new Invoice;

        $invoice->invoice_no = $no_invoice;
        $invoice->tanggal_invoice = $so_header->tanggal;
        $invoice->jatuh_tempo = $besok->format('Y-m-d G:i:s');
        $invoice->total_tagihan = $total_tagihan;
        //$Invoice->is_lunas          = 1; //gak ngerti kenapa error
        $invoice->save();

        /*         * ******* BAYAR INVOICE ******** */

        $bayar_invoice = new BayarInvoice;

        $bayar_invoice->invoice_no = $no_invoice;
        $bayar_invoice->bayar_id = "";
        $bayar_invoice->metode = "";
        $bayar_invoice->save();

        /*         * ******* DATA UNTUK INVOICE YANG DITAMPILKAN ******** */

        $so_detail = SODetail::with('produk')->where('no_nota', $no_invoice)->get();
        $perusahaan = Perusahaan::with('kota')->first();

        return view('pages.home.invoice_home', compact('so_header', 'so_detail', 'invoice_detail', 'invoice', 'pelanggan', 'so_voucher', 'perusahaan', 'bayar_invoice'));
    }

    public function generate_no_nota() {
        $mytable = DB::table('tran_so_header')->orderBy('no_nota', 'desc')->first();
        if (empty($mytable)) {
            $num = 0;
        } else {
            //get last_number
            $num = substr($mytable->no_nota, -4);
        }

        $num = $num + 1;
        $num_count = strlen((string) $num);
        $nol = 4 - $num_count;
        $kode = "";
        for ($i = 0; $i < $nol; $i++) {
            $kode = $kode . "0";
        }
        $kode = $kode . $num;

        return $kode;
    }

    public function cara_belanja() {
        $cara_belanja = "";
        $konten = DB::table('tref_konten')->where('nama', 'cara_belanja')->first();

        if (!empty($konten)) {
            $cara_belanja = $konten->keterangan;
        }

        return view('pages.home.cara_belanja', compact('cara_belanja'));
    }

    public function lacak_pesanan() {
        $data['search'] = FALSE;
        if(Input::get('no_invoice') !== NULL){
            unset($data);
            $no_invoice = Input::get('no_invoice');
            $result = DB::table('tran_nota_jual')->where('no_nota', strtoupper(strtolower($no_invoice)))->get();
            if(!empty($result)){
                $data = array(
                    'search' => TRUE,
                    'result' => $result
                );
            } else {
                $data = array(
                    'search' => TRUE,
                    'result' => FALSE
                );
            }
        }
        return view('pages.home.lacak_pesanan', $data);
    }

    public function service_center() {
        $service_center = "";
        $konten = DB::table('tref_konten')->where('nama', 'service_center')->first();

        if (!empty($konten)) {
            $service_center = $konten->keterangan;
        }

        return view('pages.home.service_center', compact('service_center'));
    }

    public function bantuan() {
        return view('pages.home.bantuan');
    }

    public function tentang_kami() {
        $tentang_kami = "";
        $konten = DB::table('tref_konten')->where('nama', 'tentang_kami')->first();

        if (!empty($konten)) {
            $tentang_kami = $konten->keterangan;
        }

        return view('pages.home.tentang_kami', compact('tentang_kami'));
    }

    public function hubungi_kami() {
        return view('pages.home.hubungi_kami');
    }

    public function faqs() {
        return view('pages.home.faqs');
    }

    public function ketentuan_pengguna() {
        return view('pages.home.ketentuan_pengguna');
    }

    public function ketentuan_retur_barang() {
        return view('pages.home.ketentuan_retur_barang');
    }

    public function sistem_pembayaran() {
        return view('pages.home.sistem_pembayaran');
    }

    public function profil_perusahaan() {
        return view('pages.home.profil_perusahaan');
    }

    public function karir() {
        return view('pages.home.karir');
    }

    public function lokasi_toko() {
        return view('pages.home.lokasi_toko');
    }

    public function aplikasi_mobile() {
        return view('pages.home.aplikasi_mobile');
    }

    public function promo() {
        date_default_timezone_set('Asia/Jakarta');
        $get_category = DB::table('tmst_kategori_produk')->where('is_aktif', 1)->get();
        foreach($get_category as $r_category){
            $id = $r_category->id;
            $promo_hadiah[$id] = DB::table('tran_promo_hadiah')
                    ->join('tmst_hadiah', 'tran_promo_hadiah.hadiah_id', '=', 'tmst_hadiah.id')
                    ->join('tmst_produk', 'tran_promo_hadiah.produk_id', '=', 'tmst_produk.id')
                    ->join('tmst_kategori_produk', 'tmst_produk.kategori_id', '=', 'tmst_kategori_produk.id')
                    ->select('tran_promo_hadiah.*', 'tmst_hadiah.*', 'tmst_produk.*', 'tmst_kategori_produk.id')
                    ->where('tran_promo_hadiah.awal_periode', '<=', date('Y-m-d H:i:s'))
                    ->where('tran_promo_hadiah.akhir_periode', '<=', date('Y-m-d H:i:s'))
                    ->where('tmst_hadiah.stok', '>', 0)
                    ->where('tmst_kategori_produk.id', $id)
                    ->get();
            $promo_diskon[$id] = DB::table('tran_promo_diskon')
                    ->join('tmst_produk', 'tran_promo_diskon.produk_id', '=', 'tmst_produk.id')
                    ->join('tmst_kategori_produk', 'tmst_produk.kategori_id', '=', 'tmst_kategori_produk.id')
                    ->select('tran_promo_diskon.*', 'tmst_produk.*', 'tmst_kategori_produk.id')
                    ->where('awal_periode', '<=', date('Y-m-d H:i:s'))
                    ->where('akhir_periode', '<=', date('Y-m-d H:i:s'))
                    ->where('tmst_kategori_produk.id', $id)
                    ->get();
            $promo_cashback[$id] = DB::table('tran_promo_cashback')
                    ->join('tmst_produk', 'tran_promo_cashback.produk_id', '=', 'tmst_produk.id')
                    ->join('tmst_kategori_produk', 'tmst_produk.kategori_id', '=', 'tmst_kategori_produk.id')
                    ->select('tran_promo_cashback.*', 'tmst_produk.*', 'tmst_kategori_produk.id')
                    ->where('awal_periode', '<=', date('Y-m-d H:i:s'))
                    ->where('akhir_periode', '<=', date('Y-m-d H:i:s'))
                    ->where('tmst_kategori_produk.id', $id)
                    ->get();
            if(empty($promo_hadiah[$id])){
                $promo_hadiah[$id] = NULL;
            }
            if(empty($promo_diskon[$id])){
                $promo_diskon[$id] = NULL;
            }
            if(empty($promo_cashback[$id])){
                $promo_cashback[$id] = NULL;
            }
        }

        $data = array(
            'get_category' => $get_category,
            'promo_hadiah' => $promo_hadiah,
            'promo_diskon' => $promo_diskon,
            'promo_cashback' => $promo_cashback
        );

        return view('pages.home.produk.promo', $data);
    }

    public function detail_promo($jenis, $kategori){
        $jenis_promo = $jenis;
        $kategori_produk_promo = $kategori;
        $kategori_produk = DB::table('tmst_kategori_produk')->where('slug_nama', $kategori_produk_promo)->get();
        foreach($kategori_produk as $r_kategori_produk){
            $id_kategori_produk = $r_kategori_produk->id;
        }
        if($jenis_promo == 'diskon'){
            $diskon = DB::table('tran_promo_diskon')
                    ->join('tmst_produk', 'tran_promo_diskon.produk_id', '=', 'tmst_produk.id')
                    ->join('tmst_kategori_produk', 'tmst_produk.kategori_id', '=', 'tmst_kategori_produk.id')
                    ->join('tran_produk_galeri', 'tmst_produk.id', '=', 'tran_produk_galeri.produk_id')
                    ->select('tmst_produk.*', 'tmst_kategori_produk.nama as nama_kategori', 'tmst_kategori_produk.slug_nama as slug_nama_kategori', 'tran_produk_galeri.file_gambar')
                    ->where('tmst_kategori_produk.id', $id_kategori_produk)
                    ->get();
            $data = array(
                'produk_promo' => $diskon
            );
        } elseif($jenis_promo == 'hadiah') {
            $hadiah = DB::table('tran_promo_hadiah')
                    ->join('tmst_hadiah', 'tran_promo_hadiah.hadiah_id', '=', 'tmst_hadiah.id')
                    ->join('tmst_produk', 'tran_promo_hadiah.produk_id', '=', 'tmst_produk.id')
                    ->join('tmst_kategori_produk', 'tmst_produk.kategori_id', '=', 'tmst_kategori_produk.id')
                    ->join('tran_produk_galeri', 'tmst_produk.id', '=', 'tran_produk_galeri.produk_id')
                    ->select('tmst_produk.*', 'tmst_kategori_produk.nama as nama_kategori', 'tmst_kategori_produk.slug_nama as slug_nama_kategori', 'tran_produk_galeri.file_gambar')
                    ->where('tmst_hadiah.stok', '>', 0)
                    ->where('tmst_kategori_produk.id', $id_kategori_produk)
                    ->get();
            $data = array(
                'produk_promo' => $hadiah
            );
        } elseif($jenis_promo == 'cashback') {
            $cashback = DB::table('tran_promo_cashback')
                    ->join('tmst_produk', 'tran_promo_cashback.produk_id', '=', 'tmst_produk.id')
                    ->join('tmst_kategori_produk', 'tmst_produk.kategori_id', '=', 'tmst_kategori_produk.id')
                    ->join('tran_produk_galeri', 'tmst_produk.id', '=', 'tran_produk_galeri.produk_id')
                    ->select('tmst_produk.*', 'tmst_kategori_produk.nama as nama_kategori', 'tmst_kategori_produk.slug_nama as slug_nama_kategori', 'tran_produk_galeri.file_gambar')
                    ->where('tmst_kategori_produk.id', $id_kategori_produk)
                    ->get();
            $data = array(
                'produk_promo' => $cashback
            );
        }
        $data ['category'] = $kategori_produk;
        return view('pages.home.produk.promo_detail', $data);
    }

}
