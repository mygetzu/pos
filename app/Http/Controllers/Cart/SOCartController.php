<?php

namespace App\Http\Controllers\Cart;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Gloudemans\Shoppingcart\Facades\Cart;
use Auth;
use Response;
use PDF;
use Hashids\Hashids;
use stdClass;
use Illuminate\Pagination\Paginator;
use Input;
use Session;
use App\Models\Produk\Produk;
use App\Models\Produk\Hadiah;
use App\Models\Pelanggan\Pelanggan;
use App\Models\PaketProduk\Paket;


class SOCartController extends Controller
{
    public function so_tambah_cart()
    {
        $produk_id      = Input::get('produk_id');
        $pelanggan_id   = Input::get('pelanggan_id');
        $jumlah_beli    = 1;

        $result = $this->cart_sales_order($produk_id, $jumlah_beli, $pelanggan_id);

        if($result == 0){
            echo '<div class="alert alert-danger flat">';
            echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
              echo '<span aria-hidden="true">&times;</span>';
            echo '</button>';
            echo 'Stok Tidak Mencukupi';
            echo '</div>';
        }

        
        $cart_content = Cart::content(1);

        return view('pages.transaksi.ajax_so_admin_cart', compact('cart_content'));
    }

    public function so_update_cart()
    {
        $pelanggan_id   = Input::get('pelanggan_id');
        $total = Input::get('total');
        Cart::instance('temp_cart');
        Cart::destroy();

        for ($i=1; $i < $total; $i++) { 
            $produk_id      = Input::get('id'.$i);
            $jumlah_beli    = Input::get('jumlah'.$i);

            if(substr($produk_id, 0, 3) == 'DIS' || substr($produk_id, 0, 3) == 'PKT' || substr($produk_id, 0, 3) == 'HDH' || substr($produk_id, 0, 3) == 'CAS')
            {
                $produk_id = substr($produk_id, 3);
            }
            
            $result = $this->cart_sales_order($produk_id, $jumlah_beli, $pelanggan_id);

            if($result == 0){
                echo '<div class="alert alert-danger flat">';
                echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
                echo '<span aria-hidden="true">&times;</span>';
                echo '</button>';
                echo 'Stok Tidak Mencukupi';
                echo '</div>';
                
                Cart::instance('default');
            }
            else
            {
                //masukkan nilai temp jadi default
                $cart_temp = Cart::content(1);
                Cart::instance('default');
                Cart::destroy(); //kosongi dulu

                foreach($cart_temp as $row) {
                    $data = array('id'      => $row->id, 
                      'name'        => $row->name, 
                      'qty'         => $row->qty, 
                      'price'       => $row->price, 
                      'options'     => array('size' => 'large'));

                    Cart::add($data);   
                }
            }
        } 

        $cart_content = Cart::content(1);
        
        return view('pages.transaksi.ajax_so_admin_cart', compact('cart_content'));
    }

    public function so_hapus_cart()
    {
        $rowId = Input::get('rowId');

        Cart::remove($rowId);

        $cart_content = Cart::content(1);
        
        return view('pages.transaksi.ajax_so_admin_cart', compact('cart_content'));
    }

    public function cart_sales_order($produk_id, $jumlah_beli, $pelanggan_id)
    {        
        $kategori_pelanggan_id  = 0;

        $pelanggan              = Pelanggan::with('kategori_pelanggan')->where('id', $pelanggan_id)->first();
        if(!empty($pelanggan))
            $kategori_pelanggan_id  = $pelanggan->kategori_id;

        $produk = Produk::with(['produk_galeri', 'promo_diskon', 'promo_cashback', 'promo_hadiah' => function($query) 
            {return $query->with('hadiah');}, 'produk_harga' 
            => function ($query) use ($kategori_pelanggan_id) 
            {$query->where('kategori_pelanggan_id', $kategori_pelanggan_id); }])
            ->where('is_aktif', 1)->where('id', $produk_id)->first();

        //Sales Order tidak perlu cek stok
        //algoritmanya : jumlah beli ditambah hasil hitung jumlah produk yang ada di cart yang punya produk_id yang sama
        // $cek_jumlah     = $jumlah_beli;
        // $cart_content   = Cart::content(1);
        // foreach ($cart_content as $key => $value) {
        //     if(substr($value->id, 0, 3) == 'DIS' || substr($value->id, 0, 3) == 'CAS')
        //         $cek_produk_id = substr($value->id, 3);
        //     else
        //         $cek_produk_id = $value->id;

        //     if($cek_produk_id == $produk_id)
        //         $cek_jumlah = $cek_jumlah + $value->qty;
        // }

        // if($cek_jumlah > (int)$produk->stok-(int)$produk->stok_dipesan)
        // {
        //     return 0; //stok tidak mencukupi maka return
        // }

        $diskon             = 0; 
        $qty_beli_diskon    = 1;
        $cashback           = 0; 
        $qty_beli_cashback  = 1;
        $hadiah             = "";
        $qty_beli_hadiah    = 1;
        $qty_hadiah         = 1;

        if(!empty($pelanggan->kategori_pelanggan)){
            if($pelanggan->kategori_pelanggan->default_diskon > $diskon){
                $diskon = $pelanggan->kategori_pelanggan->default_diskon;
            }
        }
        
        if(!empty($produk->produk_harga)){
            if($produk->produk_harga->diskon > $diskon){
                $diskon = $produk->produk_harga->diskon;
            }
        }
        
        if(!empty($produk->promo_diskon)){
            if($produk->promo_diskon->diskon > $diskon){
                $diskon = $produk->promo_diskon->diskon;
                $qty_beli_diskon= $produk->promo_diskon->qty_beli;
            }
        }

        if(!empty($produk->promo_cashback) && empty($produk->produk_harga) && empty($pelanggan->kategori_pelanggan))
        {
            $cashback           = $produk->promo_cashback->cashback;
            $qty_beli_cashback  = $produk->promo_cashback->qty_beli;
        }
        
        if(!empty($produk->promo_hadiah))
        {
            $hadiah             = $produk->promo_hadiah->hadiah->nama;
            $qty_beli_hadiah    = $produk->promo_hadiah->qty_beli;
            $qty_hadiah         = $produk->promo_hadiah->qty_hadiah;
        }

        //tambah cart dengan algoritma perhitungan selisih beli
        //cek dia punya diskon atau cashback (karena jika sudah mempunyai diskon maka gkboleh memiliki cashback)
        if($diskon != 0)
        {
            $kode_promo         = 'DIS';
            $harga_promo        = $produk->harga_retail-(($produk->harga_retail*(int)$diskon/100));
            $jumlah_beli_promo  = $qty_beli_diskon;
        }
        else if($cashback != 0)
        {
            $kode_promo         = 'CAS';
            $harga_promo        = $produk->harga_retail-(int)$cashback;
            $jumlah_beli_promo  = $qty_beli_cashback;
        }
        else
        {
            $kode_promo         = '';
            $harga_promo        = $produk->harga_retail;
            $jumlah_beli_promo = 1;
        }

        $selisih_jumlah = $jumlah_beli%$jumlah_beli_promo;

        //khusus untuk diskon, id ditambahi string DIS di depan
        //khusus untuk cashback, id ditambahi string CAS di depan
        if($selisih_jumlah == 0) //jika yang dibeli merupakan kelipatan promo, maka simpan harga promo
        {
            $data = array('id'      => $kode_promo.$produk_id, 
                      'name'        => $produk->nama, 
                      'qty'         => $jumlah_beli, 
                      'price'       => $harga_promo, 
                      'options'     => array('size' => 'large'));

            Cart::add($data);
        }
        else //jika tidak, maka simpan dua kali, harga diskon dan harga normal
        { 
            //simpan harga diskon
            $sisa_selisih_jumlah = $jumlah_beli - $selisih_jumlah;
            if($sisa_selisih_jumlah != 0)
            {
                $data = array('id'      => $kode_promo.$produk_id, 
                          'name'        => $produk->nama, 
                          'qty'         => $sisa_selisih_jumlah, 
                          'price'       => $harga_promo,
                          'options'     => array('size' => 'large'));

                Cart::add($data);
            }

            //simpan harga normal
            $data = array('id'      => $produk_id, 
                      'name'        => $produk->nama, 
                      'qty'         => $selisih_jumlah, 
                      'price'       => $produk->harga_retail, 
                      'options'     => array('size' => 'large'));

            Cart::add($data);
        }

        //hadiah tidak ditambahkan ke cart

        return 1;
    }
}
