<?php

namespace App\Http\Controllers\Accounting;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Accounting\JurnalUmumHeader;
use App\Models\Accounting\JurnalUmumDetail;
use App\Models\Pengaturan\PengaturanJurnal;
use Response;
use Illuminate\Support\Facades\DB;
use App\Models\Pengaturan\MasterCOA;
use Input;
use DateTime;
use App\Models\TransaksiPembelian\POHeader;
use App\Models\TransaksiPembelian\PODetail;
use App\Models\TransaksiPembelian\PODiskon;
use App\Models\TransaksiPembelian\RBHeader;
use App\Models\TransaksiPembelian\RBDetail;
use App\Models\TransaksiPembelian\SJMasukHeader;
use App\Models\TransaksiPembelian\SJMasukDetail;
use App\Models\TransaksiPembelian\NotaBeli;
use App\Models\TransaksiPembelian\BayarPembelianHeader;
use App\Models\TransaksiPenjualan\SOHeader;
use App\Models\TransaksiPenjualan\SODetail;
use App\Models\TransaksiPenjualan\SOVoucher;
use App\Models\TransaksiPenjualan\RJHeader;
use App\Models\TransaksiPenjualan\RJDetail;
use App\Models\TransaksiPenjualan\SJKeluarHeader;
use App\Models\TransaksiPenjualan\SJKeluarDetail;
use App\Models\TransaksiPenjualan\NotaJual;
use App\Models\TransaksiPenjualan\BayarPenjualanHeader;
use App\Models\Transaksi\ServiceOrder;
use App\Models\Supplier\ProdukSupplier;
use App\Models\Produk\Produk;
use PDF;

class JurnalController extends Controller
{
    public function jurnal_umum()
    {
        $general['title']       = "Jurnal Umum";
        $general['menu1']       = "Accounting";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 81;

        $jurnal_umum_header = JurnalUmumHeader::orderBy('tanggal', 'desc')->get();

        return view('pages.accounting.jurnal_umum', compact('general', 'jurnal_umum_header'));
    }

    public function tambah_jurnal_umum()
    {
        $general['title']       = "Tambah Jurnal Umum";
        $general['menu1']       = "Accounting";
        $general['menu2']       = "Jurnal Umum";
        $general['menu3']       = $general['title'];
        $general['kode_menu']   = 81;

        //nota_beli (transaksi yang ada 1. total harga sebelum diskon, 2. total diskon, 3. pembayaran tunai, 4. pembayaran rekening, 5. pembayaran tertunda, 6. pembayaran voucher)

        $data = [];
        $indeks = 0;

        $pengaturan_jurnal = PengaturanJurnal::all();

        foreach ($pengaturan_jurnal as $key => $value) {
            switch (true) {
                case ($value->transaksi_id == 1):
                    $nota = NotaBeli::whereNull('no_jurnal')->get();
                    break;
                case ($value->transaksi_id == 2):
                    $nota = NotaBeli::whereNull('no_jurnal')->where('total_diskon', '>', 0)->get();
                    break;
                case ($value->transaksi_id == 3 || $value->transaksi_id == 4 || $value->transaksi_id == 5):
                    $nota = NotaJual::whereNull('no_jurnal')->get();
                    break;
                case ($value->transaksi_id == 6 || $value->transaksi_id == 7):
                    $nota = RBHeader::whereNull('no_jurnal')->get();
                    break;
                case ($value->transaksi_id == 8 || $value->transaksi_id == 9):
                    $nota = RJHeader::whereNull('no_jurnal')->get();
                    break;
                case ($value->transaksi_id == 10):
                    $nota = ServiceOrder::whereNull('no_jurnal')->get();
                    break;
                case ($value->transaksi_id == 11):
                    $nota = BayarPembelianHeader::whereNull('no_jurnal')->get();
                    break;
                 case ($value->transaksi_id == 12):
                    $nota = BayarPenjualanHeader::whereNull('no_jurnal')->get();
                    break;
                default:
                    $nota = "";
                    break;
            }

            if(empty($nota))
                continue;
            
            foreach ($nota as $key2 => $value2) {
                $flag = 1;
                switch ($value->transaksi_id) {
                    case 1:
                        //get total harga sebelum diskon, jadi yang diambil adalah harga itu sendiri
                        $sj_masuk_detail = SJMasukDetail::where('sj_masuk_header_id', $value2->sj_masuk_header_id)->get();
                        $nominal = 0;
                        foreach ($sj_masuk_detail as $value3) {
                            $nominal = $nominal + $value3->harga;
                        }
                        break;
                    case 2:
                        //ambil di nota_beli->total_diskon
                        $nominal = $value2->total_diskon;
                        break;
                    case 3:
                        //get total harga sebelum voucher
                        $sj_keluar_detail  = SJKeluarDetail::where('sj_keluar_header_id', $value2->sj_keluar_header_id)->get();

                        $nominal = 0;
                        foreach ($sj_keluar_detail as $value3) {
                            $nominal = $nominal + $value3->harga;
                        }
                        break;
                    case 4:
                        //hpp perolehan didapat dengan menghitung kuantitas*harga beli supplier
                        //dapatkan detail produk di sj_keluar
                        $sj_keluar_detail  = SJKeluarDetail::where('sj_keluar_header_id', $value2->sj_keluar_header_id)->get();
                        $hpp = 0;
                        foreach ($sj_keluar_detail as $key3 => $value3) {
                            if($value3->jenis_barang_id == 1){
                                $produk = Produk::where('id', $value3->produk_id)->first();

                                $hpp = $hpp + $produk->hpp;
                            }
                        }
                        $nominal = $hpp;
                        break;
                    case 5:
                        //total voucher didapat dari nota jual
                        $nominal = $value2->total_voucher;
                        if(empty($nominal)){
                            $flag = 0;
                            continue;
                        }
                        break;
                    case 6:
                        //get total retur
                        $nominal = $value2->total_retur;
                        break;
                    case 7:
                        //get potongan_retur
                        $nominal = $value2->potongan_retur;
                        break;
                    case 8:
                        //get total retur
                        $nominal = $value2->total_retur;
                        break;
                    case 9:
                        //get potongan_retur
                        $nominal = $value2->potongan_retur;
                        break;
                    case 10:
                        //get jasa_service
                        $nominal = $value2->jasa_service;
                        break;
                    case 11:
                        //get jasa_service
                        $nominal = $value2->nominal;
                        break;
                    case 12:
                        //get jasa_service
                        $nominal = $value2->nominal;
                        break;
                    default:
                        $flag = 0;
                        break;
                }                

                if ($flag == 1) {
                    if($value->transaksi_id == 11 || $value->transaksi_id == 12){
                        $tanggal = new DateTime($value2->tanggal_bayar);

                        $data[$indeks]['referensi'] = $value2->nomor_pembayaran;
                        $data[$indeks]['no_jurnal'] = $value2->nomor_pembayaran.$tanggal->format('d').$tanggal->format('m').$tanggal->format('Y');
                        $data[$indeks]['tanggal']   = $value2->tanggal_bayar;
                    }
                    else{
                        $tanggal = new DateTime($value2->tanggal);

                        $data[$indeks]['referensi'] = $value2->no_nota;
                        $data[$indeks]['no_jurnal'] = $value2->no_nota.$tanggal->format('d').$tanggal->format('m').$tanggal->format('Y');
                        $data[$indeks]['tanggal']   = $value2->tanggal;

                    }

                    
                    $nama_transaksi = DB::table('tref_nama_transaksi')->where('id', $value->transaksi_id)->first();

                    if(!empty($nama_transaksi)){
                        $transaksi  = $nama_transaksi->transaksi;
                        $transaksi  = str_replace('_', ' ', $transaksi);
                        $nama       = $nama_transaksi->nama;

                        $data[$indeks]['transaksi']   = $transaksi.' - '.$nama;
                    }
                    else{
                        $data[$indeks]['transaksi']   = "";
                    }

                    $master_coa = MasterCOA::where('id', $value->akun_id)->first();

                    if(!empty($master_coa)){
                        $data[$indeks]['akun_nama'] = $master_coa->nama;
                        $data[$indeks]['akun_kode'] = $master_coa->kode;
                    }
                    else{
                        $data[$indeks]['akun_nama'] = "";
                        $data[$indeks]['akun_kode'] = "";
                    }

                    if($value->debit_or_kredit == 'D'){
                        $data[$indeks]['debit']     = $nominal;
                        $data[$indeks]['kredit']    = 0;
                    }else{
                        $data[$indeks]['debit']     = 0;
                        $data[$indeks]['kredit']    = $nominal;
                    }

                    $indeks++;
                }
            }
        }

        // return Response::json($data);

        //sort disini
        $sort = array();
        foreach($data as $k=>$v) {
            $sort['referensi'][$k]   = $v['referensi'];
            $sort['debit'][$k]          = $v['debit'];
            $sort['kredit'][$k]         = $v['kredit'];
        }

        if($indeks > 0){
            array_multisort($sort['referensi'], SORT_ASC, $sort['debit'], SORT_DESC, $sort['kredit'], SORT_DESC, $data);
        }

        return view('pages.accounting.tambah_jurnal_umum', compact('general', 'data'));
    }

    public function do_tambah_jurnal_umum(Request $request)
    {
        $total_data = Input::get('total_data');

        if($total_data <= 0){
            return redirect('/jurnal_umum');
        }

        date_default_timezone_set("Asia/Jakarta");
        $tanggal = new DateTime;

        $jurnal_umum_header = new JurnalUmumHeader;
        $jurnal_umum_header->tanggal        = $tanggal;
        $jurnal_umum_header->is_editable    = 1;
        $jurnal_umum_header->save();

        $jurnal_umum_header_id = $jurnal_umum_header->id;

        for ($i=1; $i <= $total_data ; $i++) { 
            $cek = Input::get('cek'.$i);

            if($cek){
                $jurnal_umum_detail = new JurnalUmumDetail;

                $jurnal_umum_detail->jurnal_umum_header_id  = $jurnal_umum_header_id;
                $jurnal_umum_detail->no_jurnal              = Input::get('no_jurnal'.$i);
                $jurnal_umum_detail->tanggal                = Input::get('tanggal'.$i);
                $jurnal_umum_detail->referensi              = Input::get('referensi'.$i);
                $jurnal_umum_detail->akun_kode              = Input::get('akun_kode'.$i);
                $jurnal_umum_detail->akun_nama              = Input::get('akun_nama'.$i);
                $jurnal_umum_detail->debit                  = Input::get('debit'.$i);
                $jurnal_umum_detail->kredit                 = Input::get('kredit'.$i);
                $jurnal_umum_detail->save();

                //isi no_jurnal di transaksi
                $nota_beli = NotaBeli::where('no_nota', $jurnal_umum_detail->referensi)->first();
                if(!empty($nota_beli)){
                    $nota_beli->no_jurnal = $jurnal_umum_detail->no_jurnal;
                    $nota_beli->save();
                }

                $nota_jual = NotaJual::where('no_nota', $jurnal_umum_detail->referensi)->first();
                if(!empty($nota_jual)){
                    $nota_jual->no_jurnal = $jurnal_umum_detail->no_jurnal;
                    $nota_jual->save();
                }

                $rb_header = RBHeader::where('no_nota', $jurnal_umum_detail->referensi)->first();
                if(!empty($rb_header)){
                    $rb_header->no_jurnal = $jurnal_umum_detail->no_jurnal;
                    $rb_header->save();
                }

                $rj_header = RJHeader::where('no_nota', $jurnal_umum_detail->referensi)->first();
                if(!empty($rj_header)){
                    $rj_header->no_jurnal = $jurnal_umum_detail->no_jurnal;
                    $rj_header->save();
                }

                $service_order = ServiceOrder::where('no_nota', $jurnal_umum_detail->referensi)->first();
                if(!empty($service_order)){
                    $service_order->no_jurnal = $jurnal_umum_detail->no_jurnal;
                    $service_order->save();
                }
            }
        }

        $request->session()->flash('message', 'Jurnal Umum berhasil ditambahkan');
        return redirect('/jurnal_umum');
    }

    public function detail_jurnal_umum($id)
    {
        $general['title']       = "Detail Jurnal Umum";
        $general['menu1']       = "Accounting";
        $general['menu2']       = "Jurnal Umum";
        $general['menu3']       = $general['title'];
        $general['kode_menu']   = 81;

        $jurnal_umum_header = JurnalUmumHeader::where('id', $id)->first();
        $jurnal_umum_detail = JurnalUmumDetail::where('jurnal_umum_header_id', $id)->get();

        //algo get last child
        $master_coa     = MasterCOA::all();
        $kode_akun = [];
        $indeks = 0;
        foreach ($master_coa as $val) {
            //cek apakah ada akun yang mempunyai parentnya kode akun ini
            $cek_master_coa = MasterCOA::where('parent', $val->kode)->first();

            if(empty($cek_master_coa->id)){
                $kode_akun[$indeks]['id']   = $val->id;
                $kode_akun[$indeks]['kode'] = $val->kode;
                $kode_akun[$indeks]['nama'] = $val->nama;
                $indeks++;
            }
        }

        return view('pages.accounting.detail_jurnal_umum', compact('general', 'jurnal_umum_header', 'jurnal_umum_detail', 'kode_akun'));
    }

    public function ubah_jurnal_umum_detail(Request $request)
    {
        $jurnal_umum_detail_id  = Input::get('id');
        $akun_kode              = Input::get('akun_kode');
        $debit_or_kredit        = Input::get('d_or_k');
        $nominal                = Input::get('nominal');

        $master_coa = MasterCOA::where('kode', $akun_kode)->first();

        $jurnal_umum_detail = JurnalUmumDetail::where('id', $jurnal_umum_detail_id)->first();
        $jurnal_umum_detail->akun_kode = $akun_kode;
        $jurnal_umum_detail->akun_nama = $master_coa->nama;

        if($debit_or_kredit == "D"){
            $jurnal_umum_detail->debit  = $nominal;
            $jurnal_umum_detail->kredit = 0;
        }
        elseif($debit_or_kredit == "K"){
            $jurnal_umum_detail->debit  = 0;
            $jurnal_umum_detail->kredit = $nominal;
        }

        $jurnal_umum_detail->save();

        $request->session()->flash('message', 'Jurnal Umum berhasil diubah');
        return redirect('/detail_jurnal_umum/'.$jurnal_umum_detail->jurnal_umum_header_id);
    }

    public function laporan_jurnal_umum()
    {
        $general['title']       = "Laporan Jurnal Umum";
        $general['menu1']       = "Accounting";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 82;

        return view('pages.accounting.laporan_jurnal_umum', compact('general'));
    }

    public function ajax_laporan_jurnal_umum()
    {
        $tahun = Input::get('tahun');
        $bulan = Input::get('bulan');


        $jurnal_umum_detail = JurnalUmumDetail::with('master_coa')
            ->whereYear('tanggal', '=', $tahun)
            ->whereMonth('tanggal', '=', $bulan)
            ->orderBy('tanggal', 'asc')
            ->get();

        $first_day = $tahun."-".$bulan."-01";
        $last_day = date("Y-m-t", strtotime($first_day));

        return view('pages.accounting.ajax_laporan_jurnal_umum', compact('jurnal_umum_detail', 'first_day', 'last_day', 'bulan', 'tahun'));
    }

    public function laporan_jurnal_umum_print($bulan, $tahun)
    {
        $jurnal_umum_detail = JurnalUmumDetail::with('master_coa')
            ->whereYear('tanggal', '=', $tahun)
            ->whereMonth('tanggal', '=', $bulan)
            ->orderBy('tanggal', 'asc')
            ->get();

        $first_day = $tahun."-".$bulan."-01";
        $last_day = date("Y-m-t", strtotime($first_day));

        return view('pages.accounting.laporan_jurnal_umum_print', compact('jurnal_umum_detail', 'first_day', 'last_day', 'bulan', 'tahun'));
    }

    public function laporan_jurnal_umum_pdf($bulan, $tahun)
    {
        $jurnal_umum_detail = JurnalUmumDetail::with('master_coa')
            ->whereYear('tanggal', '=', $tahun)
            ->whereMonth('tanggal', '=', $bulan)
            ->orderBy('tanggal', 'asc')
            ->get();

        $first_day = $tahun."-".$bulan."-01";
        $last_day = date("Y-m-t", strtotime($first_day));

        $pdf = PDF::loadView('pages.accounting.laporan_jurnal_umum_print', compact('jurnal_umum_detail', 'first_day', 'last_day', 'bulan', 'tahun'))->setPaper('a4', 'portrait');
        return $pdf->stream();
    }

    public function laporan_buku_besar()
    {
        $general['title']       = "Laporan Buku Besar";
        $general['menu1']       = "Accounting";
        $general['menu2']       = $general['title'];
        $general['kode_menu']   = 83;

        $master_coa     = MasterCOA::all();

        $kode_akun = [];
        $indeks = 0;
        foreach ($master_coa as $val) {
            //cek apakah ada akun yang mempunyai parentnya kode akun ini
            $cek_master_coa = MasterCOA::where('parent', $val->kode)->first();

            if(empty($cek_master_coa->id)){
                $kode_akun[$indeks]['id']   = $val->id;
                $kode_akun[$indeks]['kode'] = $val->kode;
                $kode_akun[$indeks]['nama'] = $val->nama;
                $indeks++;
            }
        }

        return view('pages.accounting.laporan_buku_besar', compact('general', 'kode_akun'));
    }

    public function ajax_laporan_buku_besar()
    {        
        $tahun  = Input::get('tahun');
        $bulan  = Input::get('bulan');
        $akun   = Input::get('akun');

        //dapatkan kode dari akun
        $my_akun    = MasterCOA::where('id', $akun)->first();
        $akun_kode  = $my_akun->kode;
        $akun_nama  = $my_akun->nama;

        $jurnal_umum_detail = JurnalUmumDetail::with('master_coa')
            ->whereYear('tanggal', '=', $tahun)
            ->whereMonth('tanggal', '=', $bulan)
            ->where('akun_kode', $akun_kode)
            ->orderBy('tanggal', 'asc')
            ->get();

        $first_day = $tahun."-".$bulan."-01";
        $last_day = date("Y-m-t", strtotime($first_day));

        return view('pages.accounting.ajax_laporan_buku_besar', compact('jurnal_umum_detail', 'first_day', 'last_day', 'bulan', 'tahun', 'akun_nama', 'akun'));
    }

    public function laporan_buku_besar_print($bulan, $tahun, $akun)
    {
        //dapatkan kode dari akun
        $my_akun    = MasterCOA::where('id', $akun)->first();
        $akun_kode  = $my_akun->kode;
        $akun_nama  = $my_akun->nama;

        $jurnal_umum_detail = JurnalUmumDetail::with('master_coa')
            ->whereYear('tanggal', '=', $tahun)
            ->whereMonth('tanggal', '=', $bulan)
            ->where('akun_kode', $akun_kode)
            ->orderBy('tanggal', 'asc')
            ->get();

        $first_day = $tahun."-".$bulan."-01";
        $last_day = date("Y-m-t", strtotime($first_day));

        return view('pages.accounting.laporan_buku_besar_print', compact('jurnal_umum_detail', 'first_day', 'last_day', 'bulan', 'tahun', 'akun_nama', 'akun'));
    }

    public function laporan_buku_besar_pdf($bulan, $tahun, $akun)
    {
        //dapatkan kode dari akun
        $my_akun    = MasterCOA::where('id', $akun)->first();
        $akun_kode  = $my_akun->kode;
        $akun_nama  = $my_akun->nama;

        $jurnal_umum_detail = JurnalUmumDetail::with('master_coa')
            ->whereYear('tanggal', '=', $tahun)
            ->whereMonth('tanggal', '=', $bulan)
            ->where('akun_kode', $akun_kode)
            ->orderBy('tanggal', 'asc')
            ->get();

        $first_day = $tahun."-".$bulan."-01";
        $last_day = date("Y-m-t", strtotime($first_day));

        $pdf = PDF::loadView('pages.accounting.laporan_buku_besar_print', compact('jurnal_umum_detail', 'first_day', 'last_day', 'bulan', 'tahun', 'akun_nama', 'akun'))->setPaper('a4', 'portrait');
        return $pdf->stream();
    }
}
