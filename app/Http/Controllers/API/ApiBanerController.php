<?php

namespace App\Http\Controllers\API;

use App\Models\AdminEcommerce\MiniBanner;
use App\Models\AdminEcommerce\SliderPromo;
use App\Models\Produk\Produk;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ApiBanerController extends ApiGeneral
{
    public function getSliderPromo() {
        $slider = SliderPromo::orderBy('item_order', 'asc')->get();

        $sliders = [];
        foreach ($slider as $key => $value) {
            $sliders[$key]['nama']          = $value->nama;
            $sliders[$key]['url']           = url('/slider-promo/' . $value->url);
            $sliders[$key]['file_gambar']   = url('/img/banner/' . $value->file_gambar);
            $sliders[$key]['item_order']   = $value->item_order;

            $arr_list_produk = explode(',', $value->list_produk);

            $list_produk = [];
            for ($i = 0; $i < count($arr_list_produk); $i++) {
                $list_produk[$i] = Produk::where('id', $arr_list_produk[$i])->first();
            }
            $sliders[$key]['list_produk']   = $list_produk;
        }

        return response()->json($sliders, 200);
    }

    public function getMiniBanner() {
        $miniBanner = MiniBanner::orderBy('item_order', 'asc')->get();

        $miniBanners = [];
        foreach ($miniBanner as $key => $value) {
            $miniBanners[$key]['nama']          = $value->nama;
            $miniBanners[$key]['url']           = url('/slider-promo/' . $value->url);
            $miniBanners[$key]['file_gambar']   = url('/img/banner/' . $value->file_gambar);
            $miniBanners[$key]['item_order']   = $value->item_order;


            $arr_mini_banner = explode(',', $value->list_produk);

            $list_mini = [];
            for ($i = 0; $i < count($arr_mini_banner); $i++) {
                $list_mini[$i] = Produk::where('id', $arr_mini_banner[$i])->first();
            }
            $miniBanners[$key]['list_produk']   = $list_mini;
        }
        return response()->json($miniBanners, 200);
    }
}
