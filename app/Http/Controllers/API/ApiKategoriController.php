<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;
use App\Models\Produk\KategoriProduk;
use Input;

class ApiKategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kategori_produk = KategoriProduk::where('is_aktif', 1)->get();

        $kategori = [];
        foreach ($kategori_produk as $key => $value) {
            $kategori[$key]['nama']                 = $value->nama;
            $kategori[$key]['deskripsi']            = $value->deskripsi;
            $kategori[$key]['kolom_spesifikasi']    = $value->kolom_spesifikasi;
            $kategori[$key]['file_gambar']          = "http://130.211.105.244/pos/public/img/kategori/" . $value->file_gambar;
        }

        return Response::json($kategori_produk, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $kategori = new KategoriProduk;
        $kategori->nama     = Input::get('nama');
        $kategori->deskripsi= Input::get('deskripsi');
        $kategori->is_aktif = Input::get('is_aktif');

        $success = $kategori->save();

        if (!$success) {
            return Response::json('Error while saving data', 500);
        }

        return Response::json('Success saving data', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kategori = KategoriProduk::find($id);

        if (is_null($kategori)) {
            return Response::json('Data not found', 404);
        }

        return Response::json($kategori, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $kategori = KategoriProduk::find($id);

        if (!is_null(Input::get('nama'))) {
            $kategori->nama     = Input::get('nama');
        }
        if (!is_null(Input::get('slug_nama'))) {
            $kategori->slug_nama= Input::get('slug_nama');
        }
        if (!is_null(Input::get('deskripsi'))) {
            $kategori->deskripsi= Input::get('deskripsi');
        }
        if (!is_null(Input::get('is_aktif'))) {
            $kategori->is_aktif = Input::get('is_aktif');
        }

        $success = $kategori->save();

        if (!$success) {
            return Response::json('Error while updating data', 500);
        }

        return Response::json('Success updating data', 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kategori = KategoriProduk::find($id);

        if (is_null($kategori)) {
            return Response::json('Data not found', 404);
        }

        $success = $kategori->delete();

        if (!success) {
            return Response::json('Error while deleting data', 500);
        }

        return Response::json('Success deleting data', 201);
    }
}
