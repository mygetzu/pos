<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

// include tambahan
use App\Models\Pelanggan\Pelanggan;
use Response;
use Illuminate\Support\Facades\Input;


class ApiPelangganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pelanggan = Pelanggan::all();
        return Response::json($pelanggan, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pelanggan = new Pelanggan;
        $pelanggan->id      = Input::get('id');
        $pelanggan->nama    = Input::get('nama');
        $pelanggan->email   = Input::get('email');
        $pelanggan->alamat  = Input::get('alamat');
        $pelanggan->kota_id = Input::get('kota_id');
        $pelanggan->telp1   = Input::get('telp1');
        $pelanggan->telp2   = Input::get('telp2');
        $pelanggan->kode_pos= Input::get('kode_pos');
        $pelanggan->hp_sales= Input::get('hp_sales');

        $success = $pelanggan->save();

        if (!$success) {
            return Response::json('Error while saving data');
        }

        return Response::json('Success saving data', 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $pelanggan = Pelanggan::find($id);

        if (is_null($pelanggan)) {
            return Response::json('Data nont found', 404);
        }

        return Response::json($pelanggan, 201);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pelanggan = Pelanggan::find($id);

        if (!is_null(Input::get('id'))) {
            $pelanggan->id          = Input::get('id');
        }
        if (!is_null(Input::get('nama'))) {
            $pelanggan->nama        = Input::get('nama');
        }
        if (!is_null(Input::get('email'))) {
            $pelanggan->email       = Input::get('email');
        }
        if (!is_null(Input::get('alamat'))) {
            $pelanggan->alamat      = Input::get('alamat');
        }
        if (!is_null(Input::get('kota_id'))) {
            $pelanggan->telp1       = Input::get('telp1');
        }
        if (!is_null(Input::get('kode_pos'))) {
            $pelanggan->kode_pos    = Input::get('kode_pos');
        }
        if (!is_null(Input::get('nama_sales'))) {
            $pelanggan->nama_sales  = Input::get('nama_sales');
        }
        if (!is_null(Input::get('hp_sales'))) {
            $pelanggan->hp_sales    = Input::get('hp_sales');
        }

        $success = $pelanggan->save();

        if (!$success) {
            return Response::json('Error while saving data');
        }

        return Response::json('Success saving data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
}
