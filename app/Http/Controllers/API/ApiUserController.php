<?php

namespace App\Http\Controllers\API;

use App\Models\Pelanggan\Pelanggan;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use League\OAuth2\Server\Exception\OAuthException;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use Response;
use App\User;
use Auth;
use Validator;
use Illuminate\Support\Facades\Hash;

class ApiUserController extends ApiGeneral
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::all();
        return Response::json($user, 200);
    }


    public function show($id)
    {
        $user = User::find($id);

        if (is_null($user)) {
        	return Response::json('Data not found', 404);
        }

        return Response::json($user, 200);
    }

    public function checkUser($username, $password) {
        // Variabel credential
        $credentials = [
            'email'     => $username,
            'password'  => $password,
        ];

        // Cek Apakah ada credential
        if (Auth::once($credentials)) {
            return Auth::user()->id;
        } else {
            return false;
        }
    }
    public function actionLogin(Request $request) {
        $email      = $request->input('username');

        $user       = User::findByEmail($email);
        if (!$user) {
            return response()->json(['error' => true, 'message' => 'Akun anda tidak terdaftar !']);
        }
        if ($user->is_aktif != 1) {
            return response()->json(['error' => true, 'message' => 'Akun anda dinonaktifkan Administrator. Silahkan hubungi Administrator untuk mengaktifkan kembali']);
        }
        if ($user->hak_akses_id != 2) {
            return response()->json(['error'=> true, 'message' => 'Anda tidak memiliki akses akun ini']);
        }

        $pelanggan  = Pelanggan::with('user')->where('email', $email)->first();
        if (!$pelanggan) {
            return response()->json(['error' => true, 'message' => 'Email anda belum dikonfirmasi oleh admin']);
        } else {
            $pelanggan_id = $pelanggan->id;
        }

        try {
            $response = Authorizer::issueAccessToken();
        } catch (OAuthException $e) {
            $response = false;
        }

        if (!$response) {
            $response['access_token']   = null;
            $response['token_type']     = null;
            $response['expires_in']     = null;
            $response['user']           = null;
            $response['error']          = true;
            $response['message']        = "Email dan Username tidak cocok";

            return response()->json($response, 401);
        } else {
            Authorizer::getChecker()->isValidRequest(true, $response['access_token']);
            $user_id    = Authorizer::getResourceOwnerId();
            $user       = User::find($user_id);

            $user       = User::where('id', $user->id)
                            ->first();

            $user = [
                'id'    => $user_id,
                'nama'  => $user->name,
                'email' => $user->email,
            ];

            $response['expires_time']   = Authorizer::getAccessToken()->getExpireTime();
            $response['error']          = false;
            $response['message']        = "Login berhasil";
            $response['user']           = $user;
            $response['pelanggan_id']   = $pelanggan_id;

            return response()->json($response);
        }
    }

    public function registerUser(Request $request)
    {
        $name_rules = ['name' => 'required'];
        $rules      = array_merge(User::$validation_rules, Pelanggan::$validation_rules, $name_rules);
        unset($rules['nama']);

        $validate = Validator::make($request->all(), $rules);

        if ($validate->fails()) {
            return response()->json(['error' => true, 'message' => 'Validation error']);
        }

        $name               = ucwords($request->input('name'));
        $email              = $request->input('email');
        $password           = Hash::make($request->input('password'));
        $password_confirm   = Hash::make($request->input('password_confirm'));
        $alamat             = ucwords($request->input('alamat'));
        $kota_id            = $request->input('kota_id');
        $kode_pos           = $request->input('kode_pos');
        $telp               = $request->input('telp');
        $hp                 = $request->input('hp');

        $user = DB::table('users')->where('email', $email)->first();

      //return response()->json([$user]);

        if ($user) {
            return response()->json(['error' => true, 'message' => 'Registrasi gagal. Email telah terdaftar !']);
        }

        $save = DB::table('users')->insert(['name' => $name, 'email' => $email, 'password' => $password, 'alamat' => $alamat, 'kota_id' => $kota_id, 'kode_pos' => $kode_pos,
                'telp' => $telp, 'hp' => $hp, 'hak_akses_id' => '0', 'is_aktif' => '1']);

        if (!$save) {
            return response()->json(['error' => true, 'message' => 'Data user gagal disimpan']);
        }

        return response()->json(['message' => 'Data berhasil disimpan']);
    }

    public function editUser(Request $request)
    {
        $id = Authorizer::getResourceOwnerId();

        $name_rules = ['name' => 'required'];
        $rules = array_merge(User::$validation_rules, Pelanggan::$validation_rules, $name_rules);
        unset($rules['email']);
        unset($rules['password']);
        unset($rules['password_confirm']);
        unset($rules['nama']);

        $validate = Validator::make($request->all(), $rules);

        if ($validate->fails()) {
            return response()->json(['error' => true, 'message' => 'Validation error']);
        }

        $name = ucwords($request->input('name'));
        //$email = $request->input('email');
        //$password = Hash::make($request->input('password'));
        $alamat = ucwords($request->input('alamat'));
        $kota_id = $request->input('kota_id');
        $kode_pos = $request->input('kode_pos');
        $telp = $request->input('telp');
        $hp = $request->input('hp');

        $user = DB::table('users')->where('id', $id)->update(['name' => $name, 'alamat' => $alamat, 'kota_id' => $kota_id, 'kode_pos' => $kode_pos,
            'telp' => $telp, 'hp' => $hp]);

        $pelanggan = DB::table('tmst_pelanggan')->where('user_id', $id)->update(['nama' => $name, 'alamat' => $alamat, 'kota_id' => $kota_id, 'kode_pos' => $kode_pos,
            'telp1' => $telp, 'hp_sales' => $hp]);


        if (!$pelanggan && !$user) {
            return response()->json(['error' => true, 'message' => 'Data gagal di ubah']);
        }

        return response()->json (['error' => false, 'message' => 'Data berhasil diubah']);
    }

    public function detailUser() {
        $id = Authorizer::getResourceOwnerId();
        $value = DB::table('users')->where('id', $id)->where('is_aktif', 1)->first();

        $users = [];
        $users['id']                = $value->id;
        $users['nama']              = $value->name;
        $users['email']             = $value->email;
        $users['alamat']            = $value->alamat;
        $users['kota_id']           = $value->kota_id;
        $users['kode_pos']          = $value->kode_pos;
        $users['telp']              = $value->telp;
        $users['hp']                = $value->hp;
        $users['hak_akses_id']      = $value->hak_akses_id;
        $users['tanggal_register']  = $value->tanggal_register;

        if (!$users) {
            return response()->json(['message' => 'Data tidak ditemukan']);
        }

        return response()->json($users);
    }

    public function editPassword(Request $request) {
        $id = Authorizer::getResourceOwnerId();
        $users = User::findById($id);

        $rules = [
            'old_password'      => 'required',
            'new_password'      => 'required',
            'confirm_password'  => 'required|same:new_password'
        ];
        $validate = Validator::make($request->all(), $rules);
        if ($validate->fails()) {
            return response()->json(['error' => true, 'message' => 'Validation Error or Confirm Password doesnt match']);
        }

        $users->password    = Hash::make($request->input('new_password'));
        $edit = $users->save();

        if (!$edit) {
            return response()->json(['error' => true, 'message' => 'Failed to edit password']);
        }
        return response()->json(['error' => false, 'message' => 'sukses edit password']);
    }

    public function lupaPassword(Request $request) {
        $rules = [
            'email' => 'required'
        ];

        $validate = Validator::make($request->all(), $rules);
        if ($validate->fails()) {
            return response()->json(['error' => true, 'message' => 'Validation error !']);
        }

        $email = $request->input('email');

        $users = User::findByEmail($email);

        if (!$users) {
            return response()->json(['error' => true, 'message' => 'Tidak ada email yang cocok']);
        }

        return response()->json(['error' => false, 'message' => 'Email ditemukan']);


    }

}
