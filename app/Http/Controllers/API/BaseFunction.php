<?php
/**
 * Created by PhpStorm.
 * User: Dell
 * Date: 23/01/2017
 * Time: 12:38
 */
    function rupiah($nominal)
    {
        $rupiah =  number_format($nominal,0, ",",".");
        $rupiah = "Rp "  . $rupiah;
        return $rupiah;
    }

    function decimal($nominal)
    {
        $rupiah =  number_format($nominal,0, ",",".");
        return $rupiah;
    }

    function colourCreator($colour, $per)
    {
        $colour = substr( $colour, 1 ); // Removes first character of hex string (#)
        $rgb = ''; // Empty variable
        $per = $per/100*255; // Creates a percentage to work with. Change the middle figure to control colour temperature

        if  ($per < 0 ) // Check to see if the percentage is a negative number
        {
            // DARKER
            $per =  abs($per); // Turns Neg Number to Pos Number
            for ($x=0;$x<3;$x++)
            {
                $c = hexdec(substr($colour,(2*$x),2)) - $per;
                $c = ($c < 0) ? 0 : dechex($c);
                $rgb .= (strlen($c) < 2) ? '0'.$c : $c;
            }
        }
        else
        {
            // LIGHTER
            for ($x=0;$x<3;$x++)
            {
                $c = hexdec(substr($colour,(2*$x),2)) + $per;
                $c = ($c > 255) ? 'ff' : dechex($c);
                $rgb .= (strlen($c) < 2) ? '0'.$c : $c;
            }
        }
        return '#'.$rgb;
    }

    function dibaca($x) {
        $angkaBaca = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");

        switch ($x) {
            case ($x < 12):
                return " " . $angkaBaca[$x];
                break;
            case ($x < 20):
                return $result = Dibaca($x - 10) . " Belas";
                break;
            case ($x < 100):
                $a = Dibaca($x / 10);
                $b = " Puluh ";
                return $a .$b . Dibaca($x % 10);
                break;
            case ($x < 200):
                $a =  " Seratus ";
                return $a . Dibaca($x - 100);
                break;
            case ($x < 1000):
                $a = Dibaca($x / 100);
                $b = " Ratus";
                return $a . $b . Dibaca($x % 100);
                break;
            case ($x < 2000):
                $a =  " Seribu ";
                return $a . Dibaca($x - 1000);
                break;
            case ($x < 1000000):
                $a =  Dibaca($x / 1000);
                $b = " Ribu ";
                return $a . $b . Dibaca($x % 1000);
                break;
            case ($x < 1000000000):
                $a =  Dibaca($x / 1000000);
                $b = " Juta ";
                return $a . $b . Dibaca($x % 1000000);
                break;
            case ($x < 1000000000000):
                $a =  Dibaca($x / 1000000000);
                $y = $x % 1000000000;
                $b =  " Milyar $x % 1000000000=$y";
                return $a . $b . Dibaca($x % 1000000000);
                break;
        }
    }