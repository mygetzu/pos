<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ApiWebViewCaraBelanja extends Controller
{
    private $cara_belanja = "test";
    //
    public function index() {
        $cara_belanja = "";
        $konten = DB::table('tref_konten')->where('nama', 'cara_belanja')->first();

        if(!empty($konten)){
            $cara_belanja = $konten->keterangan;
        }

        return view('pages.web_view.cara_belanja', compact('cara_belanja'));
    }

}
