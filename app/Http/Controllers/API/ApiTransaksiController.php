<?php

namespace App\Http\Controllers\API;

use App\Models\Produk\Produk;
use App\Models\Pelanggan\Pelanggan;
use App\Models\Pelanggan\PelangganCart;
use App\Models\Produk\PromoCashback;
use App\Models\Produk\PromoDiskon;
use App\Models\Produk\PromoHadiah;
use App\Models\Referensi\Kota;
use App\Models\Referensi\MetodePengiriman;
use App\Models\Referensi\Provinsi;
use App\Models\Temporary\TempPembayaran;
use App\User;
use Guzzle\Http\Message\Response;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use PhpParser\Node\Expr\Include_;
use Validator;
use Input;
use League\OAuth2\Server\Exception\OAuthException;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use DateTime;
use App\Models\Temporary\TempAlamatPengiriman;
use App\Models\TransaksiPenjualan\SOHeader;
use App\Models\Transaksi\SODetail;
use App\Models\Transaksi\SOAlamatPengiriman;
use App\Models\Notifikasi;
use App\Models\Transaksi\SOVoucher;
use DB;
use Mail;
use PDF;
use App\Models\TransaksiPenjualan\SOPembayaran;
use App\Models\Perusahaan;
use App\Models\Transaksi\Hadiah;

class ApiTransaksiController extends ApiGeneral
{

    public function showCart() {
        $total = 0;
        $user_id        = $this->getUserLoggedIn()->id;
        $pelanggan      = Pelanggan::where('user_id', $user_id)->first();

        $cart_pelanggan = PelangganCart::with(['produk', 'paket', 'hadiah', 'pelanggan'])->where('pelanggan_id', $pelanggan->id)->get();

        $carts = [];
        foreach ($cart_pelanggan as $key => $value) {
            $carts['produk'][$key]['produk_id']       = $value->produk_id;
            $carts['produk'][$key]['nama_produk']     = $value->produk->nama;
            $carts['produk'][$key]['jumlah']          = $value->jumlah;
            $carts['produk'][$key]['harga_retail']    = $value->harga_akhir;
            $carts['produk'][$key]['harga_lama']      = $value->harga_retail;
            if (isNonEmptyString($value->jenis_promo)) {
                $carts['produk'][$key]['jenis_promo'] = $value->jenis_promo;
            }
            if(!empty($value->produk->produk_galeri_first->file_gambar)){
                $carts['produk'][$key]['file_gambar'] = url('/img/produk/' . $value->produk->produk_galeri_first->file_gambar);
            }
            else{
                $carts['produk'][$key]['file_gambar'] = url('/img/GPDKEH8A5KZ.jpg');
            }

            $total += $value->harga_akhir * $value->jumlah;
        }
        $carts['total'] = $total;
        $carts{'error'} = '';
        $carts['error_description'] = '';

        return response()->json($carts);
    }

    public function addToCart(Request $request) {
        $user_id        = $this->getUserLoggedIn()->id;
        $pelanggan      = Pelanggan::where('user_id', $user_id)->first();
        $pelanggan_id   = $pelanggan->id;

        $rules      = PelangganCart::$validation_rules;
        $validate   = Validator::make($request->all(), $rules);

        if ($validate->fails()) {
            return response()->json(['error' => true, 'message' => 'Validation Error']);
        }

        //$pelanggan_id       = $request->input('pelanggan_id');
        $produk_id          = $request->input('produk_id');
        $jenis_barang_id    = $request->input('jenis_barang_id');
        $jumlah             = $request->input('jumlah');
        $harga_retail       = $request->input('harga_lama');
        $harga_akhir        = $request->input('harga_retail');


        // cek gambar
        $produk     = Produk::with('produk_galeri_first')->where('id', $produk_id)->first();
        if(!empty($produk->produk_galeri_first->file_gambar)) {
            $file_gambar = "http://130.211.105.244/pos/public/img/produk/" . $produk->produk_galeri_first->file_gambar;
        } else {
            $file_gambar = "http://130.211.105.244/pos/public/img/GPDKEH8A5KZ.jpg";
        }

        //Cek apakah ada promo atau tidak

        $promo_diskon   = PromoDiskon::where('produk_id', $produk_id)->first();
        $promo_cashback = PromoCashback::where('produk_id', $produk_id)->first();
        $promo_hadiah   = PromoHadiah::where('produk_id', $produk_id)->first();
        if (!empty($promo_diskon->diskon)) {
            $jenis_promo = "diskon";
        } else if (!empty($promo_cashback->cashback)) {
            $jenis_promo = "cashback";
        } else if (!empty($promo_hadiah->hadiah)) {
            $jenis_promo = "hadiah";
        } else {
            $jenis_promo = "tidak_ada_promo";
        }

        $check = PelangganCart::where('pelanggan_id', $pelanggan_id)->where('produk_id', $produk_id)->first();
        if ($check) {
            $check->jumlah      = $check->jumlah + $jumlah;

            // Cek Stok cukup atau tidak
            if ($check->jumlah > $produk->stok - $produk->stok_dipesan) {
                return response()->json(['error' => 'Stok Kurang', 'message' => 'Stok tidak mencukupi']);
            } else {
                $success = $check->save();
            }
        } else {
            // Cek Stok cukup atau tidak
            if ($jumlah > $produk->stok - $produk->stok_dipesan) {
                return response()->json(['error' => 'Stok Kurang', 'message' => 'Stok tidak mencukupi']);
            } else {
                $cart = new PelangganCart();
                $cart->pelanggan_id     = $pelanggan_id;
                $cart->produk_id        = $produk_id;
                $cart->jenis_barang_id  = $jenis_barang_id;
                $cart->jumlah           = $jumlah;
                $cart->harga_retail     = $harga_retail;
                $cart->harga_akhir      = $harga_akhir;
                $cart->jenis_promo      = $jenis_promo;
                $cart->file_gambar      = $file_gambar;

                $success = $cart->save();
            }
        }
        if (!$success) {
            return response()->json(['error' => true, 'message' => 'Error while saving to cart']);
        }

        return response()->json(['error' => '', 'error_description' => '','message' => 'Sukses saving to cart']);
    }

    public function addJmlPesanCart(Request $request) {
        $user_id        = $this->getUserLoggedIn()->id;
        $pelanggan      = Pelanggan::where('user_id', $user_id)->first();
        $pelanggan_id   = $pelanggan->id;

        $produk_id          = $request->input('produk_id');

        $pelanggan_cart = PelangganCart::where('pelanggan_id', $pelanggan_id)->where('produk_id', $produk_id)->first();
        $jml = $pelanggan_cart->jumlah;

        $success = DB::table('tran_pelanggan_cart')->where('pelanggan_id', $pelanggan_id)->where('produk_id', $produk_id)->update(['jumlah' => $jml+1]);
        if (!$success) {
            return response()->json(['error' => true, 'message' => 'Erorr while updating to cart']);
        }
        $jmlquery = PelangganCart::where('pelanggan_id', $pelanggan_id)->where('produk_id', $produk_id)->first();
        $jmlakhir = $jmlquery->jumlah;
        return response()->json(['error' => '', 'error_description' => '','jumlah' => $jmlakhir,'message' => 'Sukses updating to cart']);
    }

    public function minJmlPesanCart(Request $request) {
        $user_id        = $this->getUserLoggedIn()->id;
        $pelanggan      = Pelanggan::where('user_id', $user_id)->first();
        $pelanggan_id   = $pelanggan->id;

        $produk_id          = $request->input('produk_id');

        $pelanggan_cart = PelangganCart::where('pelanggan_id', $pelanggan_id)->where('produk_id', $produk_id)->first();
        $jml = $pelanggan_cart->jumlah;

        $success = DB::table('tran_pelanggan_cart')->where('pelanggan_id', $pelanggan_id)->where('produk_id', $produk_id)->update(['jumlah' => $jml-1]);
        if ($jml == 1) {
            $success1 = DB::table('tran_pelanggan_cart')->where('pelanggan_id', $pelanggan_id)->where('produk_id', $produk_id)->delete();
        }
        if (!$success && !$success1) {
            return response()->json(['error' => true, 'message' => 'Erorr while updating to cart']);
        }

        $jmlquery = PelangganCart::where('pelanggan_id', $pelanggan_id)->where('produk_id', $produk_id)->first();
        $jmlakhir = $jmlquery->jumlah;
        return response()->json(['error' => '', 'error_description' => '','jumlah' => $jmlakhir, 'message' => 'Sukses updating to cart']);
    }

    public function hapusPesanCart(Request $request) {
        $user_id        = $this->getUserLoggedIn()->id;
        $pelanggan      = Pelanggan::where('user_id', $user_id)->first();
        $pelanggan_id   = $pelanggan->id;

        $produk_id          = $request->input('produk_id');

        $success = DB::table('tran_pelanggan_cart')->where('pelanggan_id', $pelanggan_id)->where('produk_id', $produk_id)->delete();
        if (!$success) {
            return response()->json(['error' => true, 'message' => 'Erorr while Deleting to cart']);
        }

        return response()->json(['error' => '', 'error_description' => '', 'message' => 'Sukses deleting to cart']);
    }

    public function dataPengiriman() {
        $user_id        = $this->getUserLoggedIn()->id;

        if (!empty($user_id)) {
            $pelanggan      = Pelanggan::where('user_id', $user_id)->first();
            $pelanggan_id   = $pelanggan->id;

            $pelanggan_cart = PelangganCart::where('pelanggan_id', $pelanggan_id)->get();

//            return response()->json($pelanggan_cart);

            if (count($pelanggan_cart) == 0) {
                return response()->json(['message' => 'Keranjang anda kosong !']);
            }

            $pelanggan_array  = Pelanggan::where('id', $pelanggan_id)->first();
            $pelanggan_obj  = [];
            $pelanggan_obj['nama']      = $pelanggan_array->nama;
            $pelanggan_obj['email']     = $pelanggan_array->email;
            $pelanggan_obj['alamat']    = $pelanggan_array->alamat;
            $pelanggan_obj['kota_id']   = $pelanggan_array->kota_id;
            //$pelanggan_obj['telp1']     = $pelanggan_array->telp1;
            $pelanggan_obj['kode_pos']  = $pelanggan_array->kode_pos;
            $pelanggan_obj['hp_sales']  = $pelanggan_array->hp_sales;
            //$pelanggan_obj['kategori_id'] =  $pelanggan_array->kategori_id;

            return response()->json($pelanggan_obj);
        }
    }



    public function getMetodePengiriman() {
        $metode = DB::table('tref_metode_pengiriman')->where('is_aktif', 1)->get();

        $metode_pengiriman = [];
        foreach ($metode as $key => $value) {
            $metode_pengiriman[$key]['id']          = $value->id;
            $metode_pengiriman[$key]['nama']        = $value->nama;
            $metode_pengiriman[$key]['tarif_dasar'] = $value->tarif_dasar;
            $metode_pengiriman[$key]['keterangan']  = $value->keterangan;
            $metode_pengiriman[$key]['file_gambar'] = $value->file_gambar;
        }

        if(empty($metode_pengiriman)) {
            return response()->json(['error' => true, 'message' => "Data tidak ada"]);
        }

        return response()->json($metode_pengiriman);
    }

    public function getMetodePembayaran() {
        $metode = DB::table('tref_metode_pembayaran')->where('is_aktif', 1)->where('jenis', 'non_tunai')->get();

        $metode_pembayaran = [];
        foreach ($metode as $key => $value) {
            $metode_pembayaran[$key]['id']          = $value->id;
            $metode_pembayaran[$key]['nama']        = $value->nama;
            $metode_pembayaran[$key]['keterangan']  = $value->keterangan;
            $metode_pembayaran[$key]['file_gambar'] = $value->file_gambar;
        }

        if(empty($metode_pembayaran)) {
            return response()->json(['error' => true, 'message' => "Data tidak ada"]);
        }

        return response()->json($metode_pembayaran);
    }

    public function sendDataPengiriman(Request $request) {
        $rules = [
            'nama'                  => 'required',
            'email'                 => 'required',
            'alamat'                => 'required',
            'provinsi_id'           => 'required',
            'kota_id'               => 'required',
            'kode_pos'              => 'required',
            'hp_sales'              => 'required',
            'metode_pengiriman_id'  => 'required',
        ];

        $validate = Validator::make($request->all(), $rules);

        if ($validate->fails()) {
            return response()->json(['message' => 'Validation Error']);
        }

        date_default_timezone_set("Asia/Jakarta");
        $tanggal        = new DateTime;

        //simpan di tabel temp_alamat_pengiriman
        $temp_alamat_pengiriman = new TempAlamatPengiriman;

        $temp_alamat_pengiriman->tanggal                = $tanggal;
        $temp_alamat_pengiriman->nama                   = $request->input('nama');
        $temp_alamat_pengiriman->email                  = $request->input('email');
        $temp_alamat_pengiriman->alamat                 = $request->input('alamat');
        $temp_alamat_pengiriman->kota_id                = $request->input('kota_id');
        $temp_alamat_pengiriman->kode_pos               = $request->input('kode_pos');
        $temp_alamat_pengiriman->hp                     = $request->input('hp_sales');
        $temp_alamat_pengiriman->metode_pengiriman_id   = $request->input('metode_pengiriman_id');
        $temp_alamat_pengiriman->save();
        $temp_alamat_pengiriman_id = $temp_alamat_pengiriman->id;

        return response()->json(['error' => '', 'error_description' => '','temp_alamat_pengiriman_id' => $temp_alamat_pengiriman_id, 'message' => 'Penyimpanan data pengiriman berhasil']);
    }

    public function sendDataPembayaran(Request $request) {
        $rules = [
            'metode_pembayaran'         => 'required',
            'temp_alamat_pengiriman_id' => 'required'
        ];

        $validate = Validator::make($request->all(), $rules);
        if ($validate->fails()) {
            return response()->json(['message' => 'Validation Error']);
        }

        $metode_pembayaran          = $request->input('metode_pembayaran');
        $temp_alamat_pengiriman_id  = $request->input('temp_alamat_pengiriman_id');

        date_default_timezone_set("Asia/Jakarta");
        $tanggal = new DateTime;

        // Simpan ke temp
        $temp_pembayaran = new TempPembayaran;
        $temp_pembayaran->tanggal               = $tanggal;
        $temp_pembayaran->metode_pembayaran_id  = $metode_pembayaran;
        $temp_pembayaran->save();

        $temp_pembayaran_id = $temp_pembayaran->id;

        return response()->json(['error' => '', 'error_description' => '','temp_pembayaran_id' => $temp_pembayaran_id, 'temp_alamat_pengiriman_id' => $temp_alamat_pengiriman_id, 'message' => 'Penyimpanan data pembayaran berhasil']);
    }

    public function reviewPesanan() {
        $user_id        = $this->getUserLoggedIn()->id;
        $pelanggan      = Pelanggan::where('user_id', $user_id)->first();
        $pelanggan_id   = $pelanggan->id;

        if (!empty($user_id)) {
            $pelanggan_cart     = PelangganCart::with(['produk', 'jenis_barang'])->where('pelanggan_id', $pelanggan_id)->get();

            $total = 0;
            $jml_item = 0;
            $data_cart = [];
            foreach ($pelanggan_cart as $key => $value) {
                $data_cart['produk'][$key]['id']              = $value->produk->id;
                $data_cart['produk'][$key]['nama']            = $value->produk->nama;
                $data_cart['produk'][$key]['jumlah']          = $value->jumlah;
                $data_cart['produk'][$key]['satuan']          = $value->produk->satuan;
                $data_cart['produk'][$key]['harga_lama']      = $value->harga_retail;
                $data_cart['produk'][$key]['harga_retail']    = $value->harga_akhir;
                $data_cart['produk'][$key]['sub_total']       = $value->harga_akhir * $value->jumlah;
                $data_cart['produk'][$key]['jenis_barang_id'] = $value->jenis_barang_id;
                $data_cart['produk'][$key]['jenis_barang']    = $value->jenis_barang->nama;
                $data_cart['produk'][$key]['jenis_promo']     = $value->jenis_promo;

                if ($value->jenis_barang_id == 1) {
                    if ($value->jenis_promo == "hadiah") {
                        $promo_hadiah = PromoHadiah::with(['produk', 'hadiah'])->where('produk_id', $value->produk_id)->first();

                        $data_cart['produk'][$key]['hadiah'] = $promo_hadiah->produk->nama;
                    }
                }

                $jml_item += $value->jumlah;
                $total += $value->harga_akhir * $value->jumlah;
            }
            foreach ($pelanggan_cart as $key => $value) {
                if ($value->jenis_barang_id == 1) {
                    if ($value->jenis_promo == "hadiah") {
                        $promo_hadiah = PromoHadiah::with(['produk', 'hadiah'])->where('produk_id', $value->produk_id)->first();

                        //cek dia beli berapa, dimodulus
                        if(!empty($promo_hadiah)){
                            //cek berapa jumlah hadiah yang bisa didapat, masukkan ke array, SEMENTARA 1 PRODUK 1 JENIS HADIAH
                            //RUMUS DAPAT BERAPA HADIAH
                            $modulus                = $value->jumlah % $promo_hadiah->qty_beli;
                            $jumlah_dapat_hadiah    = (($value->jumlah - $modulus) / $promo_hadiah->qty_beli * $promo_hadiah->qty_hadiah);

                            $data_cart['hadiah'][$value->produk->id]['id']          = $promo_hadiah->hadiah->id;
                            $data_cart['hadiah'][$value->produk->id]['nama']        = $promo_hadiah->hadiah->nama;
                            $data_cart['hadiah'][$value->produk->id]['quantity']    = $jumlah_dapat_hadiah;
                            $data_cart['hadiah'][$value->produk->id]['satuan']      = $promo_hadiah->produk->satuan;

                            //hadiah disini cuma untuk ditampilkan, sedangkan di fungsi proses pesanan, akan dicek lagi hadiah
                            //hal ini untuk menghindari ada kebocoran untuk manipulasi data
                        } else {
                            $data_cart['hadiah'] = null;
                        }
                    }
                }
            }

            $data_cart['jml_item']  = $jml_item;
            $data_cart['total']     = $total;
            $data_cart['error']     = "";
            $data_cart['message']   = "Parse data sukses";

            return response()->json($data_cart);
        }
    }

    public function prosesCheckout (Request $request) {
        $rules = [
            'temp_alamat_pengiriman_id' => 'required',
            'temp_pembayaran_id'        => 'required',
        ];

        $validate = Validator::make($request->all(), $rules);
        if ($validate->fails()) {
            return response()->json(['message' => 'Validation Error']);
        }

        $temp_alamat_pengiriman_id  = $request->input('temp_alamat_pengiriman_id');
        $temp_pembayaran_id         = $request->input('temp_pembayaran_id');
        $catatan                    = $request->input('catatan');
//        $hadiah                     = $request->input('hadiah');
        $hadiah = [];
        $kode_voucher               = $request->input('kode_voucher');

        $user_id = $this->getUserLoggedIn()->id;

        if(!empty($user_id)){
            $pelanggan = Pelanggan::where('user_id', $user_id)->first();
            $pelanggan_id = $pelanggan->id;
        }
        else{
            $pelanggan = Pelanggan::where('id', 1)->first();
        }

        //generate no_nota untuk sales order
        date_default_timezone_set("Asia/Jakarta");
        $tanggal        = new DateTime;

        $template_no_invoice  = DB::table('tref_nomor_invoice')->where('id', 5)->first();
        $no_invoice = $template_no_invoice->format;

        $cek_dd = strpos($no_invoice, 'DD');
        if(!empty($cek_dd))
        {
            $no_invoice = str_replace('DD', $tanggal->format('d'), $no_invoice);
        }

        $cek_mm = strpos($no_invoice, 'MM');
        if(!empty($cek_mm))
        {
            $no_invoice = str_replace('MM', $tanggal->format('m'), $no_invoice);
        }

        $cek_yyyy = strpos($no_invoice, 'YYYY');
        if(!empty($cek_yyyy))
        {
            $no_invoice = str_replace('YYYY', $tanggal->format('Y'), $no_invoice);
        }

        //cek maksimal 10
        $number = '##########';

        for ($i=9; $i > 0; $i--) {
            $number     = substr($number, 0, $i);
            $cek_number = strpos($no_invoice, $number);
            $nol        = "";
            if(!empty($cek_number))
            {
                $so_header      = SOHeader::orderBy('id', 'desc')->first();
                if(empty($so_header))
                    $get_num = 1;
                else
                    $get_num        = (int)$so_header->id + 1;

                for ($j=0; $j < $i - count($get_num); $j++) {
                    $nol = $nol."0";
                }

                $get_num = $nol.$get_num;
                $no_invoice = str_replace($number, $get_num, $no_invoice);
                break;
            }
        }

        /* SO HEADER */

        $so_header = new SOHeader;
        $so_header->no_sales_order          = $no_invoice;
        $so_header->pelanggan_id            = $pelanggan_id;
        $so_header->tanggal                 = $tanggal;
        $so_header->catatan                 = $catatan;
        $so_header->flag_sj_keluar          = 0;
        $so_header->ppn                     = 0; //ppn di set nol
        $so_header->save();
        $so_header_id = $so_header->id;

        /* SO Detail */
        $cart_content   = PelangganCart::where('pelanggan_id', $pelanggan_id)->get();
        $total_tagihan  = 0;
        $array_produk_id = []; //digunakan untuk mengumpulkan produk id
        $indeks = 0;

        foreach ($cart_content as $cart) {
            $id             = $cart->id;
            $jenis_barang_id= $cart->jenis_barang_id;
            $so_detail      = new SODetail;

            $so_detail->so_header_id     = $so_header_id;

            //cek apakah dia paket, hadiah atau produk
            //ada 3 macam jenis_barang dapat dilihat ditabel tref_jenis_barang
            //1. produk, 21. hadiah, 3. paket produk
            if($jenis_barang_id == 3){
                $paket_id                   = $cart->produk_id;
                $so_detail->produk_id       = $paket_id;
                $so_detail->jumlah          = $cart->jumlah;
                $so_detail->harga           = $cart->harga_akhir;
                $so_detail->jenis_barang_id = 3; //id untuk jenis barang paket

                $total_tagihan = $total_tagihan + ($cart->jumlah * $cart->harga_akhir);

                $paket = Paket::where('id', $paket_id)->first();

                $so_detail->deskripsi = $paket->nama;

                //di tabel paket, tambah nilai stok dipesan
                $paket->stok_dipesan = $paket->stok_dipesan + $cart->jumlah;
                $paket->save();
            } else {
                $produk_id = $cart->produk_id;

                $so_detail->produk_id       = $cart->produk_id;
                $so_detail->jumlah          = $cart->jumlah;
                $so_detail->harga           = $cart->harga_akhir;
                $so_detail->jenis_barang_id = 1; //id untuk jenis barang produk

                $total_tagihan = $total_tagihan + ($cart->jumlah * $cart->harga_akhir);

                $produk = Produk::find($produk_id);

                $so_detail->deskripsi = $produk->deskripsi;

                //di tabel produk, tambah nilai stok dipesan
                $produk->stok_dipesan = $produk->stok_dipesan + $cart->jumlah;

                $produk->save();

                //cek
                $cek_array = 0;
                foreach ($array_produk_id as $value) {
                    if($value['id'] == $produk_id){
                        $value['quantity'] = $value['quantity'] + $cart->jumlah;
                        $cek_array = 1;
                    }
                }



                if($cek_array == 0){
                    $array_produk_id[$indeks]['id']         = $produk_id;
                    $array_produk_id[$indeks]['quantity']   = $cart->jumlah;
                    $indeks++;
                }

            }

            $so_detail->save();
        }

        //cek apakah dia dapat hadiah, jika ada tambahkan, pengecekan dilakukan dikeseluruhan isi cart agar tdk terpengaruh dg split diskon dan cashback
        foreach ($array_produk_id as $value) {

            $produk = Produk::with('promo_hadiah')->where('id', $value['id'])->first();
            //cek dia beli berapa, dimodulus
            if(!empty($produk->promo_hadiah)){
                //cek berapa jumlah hadiah yang bisa didapat, masukkan ke array, SEMENTARA 1 PRODUK 1 JENIS HADIAH
                //RUMUS DAPAT BERAPA HADIAH
                $modulus                = $value['quantity'] % $produk->promo_hadiah->qty_beli;
                $jumlah_dapat_hadiah    = (($value['quantity'] - $modulus) / $produk->promo_hadiah->qty_beli * $produk->promo_hadiah->qty_hadiah);

                $hadiah[$indeks]['id']          = $produk->promo_hadiah->hadiah->id;
                $hadiah[$indeks]['nama']        = $produk->promo_hadiah->hadiah->nama;
                $hadiah[$indeks]['quantity']    = $jumlah_dapat_hadiah;
                $indeks++;

                $so_detail = new SODetail;

                $so_detail->no_nota         = $no_invoice;
                $so_detail->produk_id       = $produk->promo_hadiah->hadiah->id;
                $so_detail->jumlah          = $jumlah_dapat_hadiah;
                $so_detail->harga           = 0;
                $so_detail->jenis_barang_id = 2;
                $so_detail->deskripsi       = $produk->promo_hadiah->hadiah->deskripsi;
                $so_detail->save();

                //di tabel hadiah, tambah nilai stok dipesan
                $hadiah = Hadiah::where('id', $produk->promo_hadiah->hadiah->id)->first();
                $hadiah->stok_dipesan = $hadiah->stok_dipesan + $jumlah_dapat_hadiah;
                $hadiah->save();
            }
        }


        /* SO Voucher */

        if(empty($kode_voucher)){
            $count_kode_voucher = 0;
        }
        else{
            $kode_voucher       = explode(',', $kode_voucher);
            $count_kode_voucher = count($kode_voucher);
        }

        for ($i=0; $i < $count_kode_voucher; $i++) {
            $voucher = Voucher::where('kode', $kode_voucher[$i])->where('is_aktif', 1)->where('awal_periode', '<=', $tanggal->format('Y-m-d'))->where('akhir_periode', '>=', $tanggal->format('Y-m-d'))->first();

            if(!empty($voucher)){
                if(empty($voucher->pelanggan_id_target) || $voucher->pelanggan_id_target == $pelanggan->id){
                    $so_voucher = new SOVoucher;
                    $so_voucher->so_header_id   = $so_header_id;
                    $so_voucher->voucher_id     = $voucher->id;
                    $so_voucher->nominal        = $voucher->nominal;
                    $so_voucher->save();

                    $total_tagihan = $total_tagihan - $voucher->nominal;

                    //update tabel tmst_vouhcer
                    $voucher->tanggal_dipakai       = $tanggal;
                    $voucher->pelanggan_id_pakai    = $pelanggan->id;

                    if($voucher->is_sekali_pakai == 1){
                        $voucher->is_aktif = 0;
                    }

                    $voucher->save();
                }
            }
        }

        $so_header = SOHeader::where('id', $so_header_id)->first();
        $so_header->total_tagihan   = $total_tagihan;
        $so_header->save();

        $del_pelanggan_cart = PelangganCart::where('pelanggan_id', $pelanggan_id)->first();
        if (!empty($del_pelanggan_cart)) {
            $del_pelanggan_cart->delete();
        }


        /* SO Alamat Pengiriman */
        $so_alamat_pengiriman   = new SOAlamatPengiriman;
        $temp_alamat_pengiriman = TempAlamatPengiriman::where('id', $temp_alamat_pengiriman_id)->first();
        if (!empty($temp_alamat_pengiriman)) {
            $so_alamat_pengiriman->so_header_id         = $so_header_id;
            $so_alamat_pengiriman->nama                 = $temp_alamat_pengiriman->nama;
            $so_alamat_pengiriman->email                = $temp_alamat_pengiriman->email;
            $so_alamat_pengiriman->alamat               = $temp_alamat_pengiriman->alamat;
            $so_alamat_pengiriman->kota_id              = $temp_alamat_pengiriman->kota_id;
            $so_alamat_pengiriman->kode_pos             = $temp_alamat_pengiriman->kode_pos;
            $so_alamat_pengiriman->hp                   = $temp_alamat_pengiriman->hp;
            $so_alamat_pengiriman->metode_pengiriman_id = $temp_alamat_pengiriman->metode_pengiriman_id;

            $so_alamat_pengiriman->save();
            $temp_alamat_pengiriman->delete();
        } else {
            return response()->json($temp_alamat_pengiriman);
        }

        /* UNTUK ECOMMERCE BAYAR DISIMPAN DI SO_PEMBAYARAN */
        $temp_pembayaran = TempPembayaran::where('id', $temp_pembayaran_id)->first();

        $so_pembayaran = new SOPembayaran;
        $so_pembayaran->so_header_id            = $so_header_id;
        $so_pembayaran->metode_pembayaran_id    = $temp_pembayaran->metode_pembayaran_id;
        $so_pembayaran->bank_id                 = $temp_pembayaran->bank_id;
        $so_pembayaran->save();

        //create notif
        $notifikasi = new Notifikasi;

        $notifikasi->kode                   = 'sales_order';
        $notifikasi->pesan                  = 'Order Baru oleh akun '.$pelanggan->nama;
        $notifikasi->hak_akses_id_target    = 1;
        $notifikasi->link                   = "sales_order";
        $notifikasi->is_aktif               = 1;
        $notifikasi->save();


        $judul                  = "INVOICE";
        $so_header              = SOHeader::with('pelanggan')->where('id', $so_header_id)->first();
        $so_detail              = SODetail::with('produk', 'hadiah', 'paket')->where('so_header_id', $so_header_id)->get();
        $so_voucher             = SOVoucher::where('so_header_id', $so_header_id)->get();
        $so_alamat_pengiriman   = SOAlamatPengiriman::with('kota')->where('so_header_id', $so_header_id)->first();
        //$bayar_invoice_penjualan  = BayarInvoicePenjualan::where('invoice_penjualan_id', $invoice_penjualan_id)->first();

        $perusahaan     = Perusahaan::with('kota')->first();

        $total_voucher = 0;
        foreach ($so_voucher as $key => $value) {
            $total_voucher = $total_voucher + $value->nominal;
        }

        date_default_timezone_set("Asia/Jakarta");
        $print_time = new DateTime;

        $fileName   = $judul.'_'.$perusahaan->nama.'_'.$print_time->format('Ymd').'_'.$print_time->format('Gis').'.pdf';
        $filePath   = base_path().'/public/pdf/';

        $pdf = PDF::loadView('pages.transaksi.pelanggan_invoice_pdf', compact('judul', 'so_header', 'so_detail', 'so_voucher', 'so_alamat_pengiriman', 'perusahaan', 'print_time'))->setPaper('a4', 'landscape')->save($filePath.$fileName);;
        $mailAttachment = $filePath.$fileName;

        //kirim email
        Mail::send('pages.checkout.email_invoice_pelanggan', ['so_header' => $so_header, 'perusahaan' => $perusahaan, 'so_alamat_pengiriman' => $so_alamat_pengiriman], function ($mail) use ($so_alamat_pengiriman, $mailAttachment)
        {
            $mail->from('noreplay@pos.com', "noreplay");
            $mail->to($so_alamat_pengiriman->email, $so_alamat_pengiriman->nama);
            $mail->subject('Email Pesanan POS');
            $mail->attach($mailAttachment);
        });

        return response()->json(['so_header_id' => $so_header_id,'error' => '', 'error_description' => '', 'message' => 'Transaksi sukses']);

    }

    public function getInvoice($so_header_id) {
        $invoice = [];
        $judul                  = "INVOICE";
        $so_header              = SOHeader::with('pelanggan')->where('id', $so_header_id)->first();
        $so_detail_obj          = SODetail::where('so_header_id', $so_header_id)->get();
        $so_voucher             = SOVoucher::where('so_header_id', $so_header_id)->get();
        $so_alamat_pengiriman   = SOAlamatPengiriman::with(['kota'])->where('so_header_id', $so_header_id)->first();
//        $hadiah                 = Hadiah::
//        $bayar_invoice_penjualan  = BayarInvoicePenjualan::where('invoice_penjualan_id', $so_header->invoice_penjualan_id)->first();
        $perusahaan     = Perusahaan::with('kota')->first();


        $terbilang = $so_header->total_tagihan;

        $invoice['judul']                                = $judul;
        $invoice['perusahaan']['nama']                   = $perusahaan->nama;
        $invoice['perusahaan']['alamat']                 = $perusahaan->alamat;
        $invoice['perusahaan']['kota']                   = $perusahaan->kota->nama;
        $invoice['perusahaan']['telp']                   = $perusahaan->telp;
        $invoice['perusahaan']['email']                  = $perusahaan->email;

        $invoice['header']['no_transaksi']               = $so_header->no_sales_order;
        $invoice['header']['tanggal']                    = $so_header->tanggal;
        $invoice['header']['pelanggan']                  = $so_header->pelanggan->nama;
        $invoice['header']['alamat_pengiriman']          = $so_alamat_pengiriman->alamat . " - " . $so_alamat_pengiriman->kota->nama;
        $invoice['header']['hp']                         = $so_alamat_pengiriman->hp;

        //dapatkan detail data produk, paket, hadiah di SODetail
        $total_akhir = 0;
        $jml_item = 0;
        foreach ($so_detail_obj as $key => $value) {
            if($value->jenis_barang_id == 1){
                $produk = Produk::where('id', $value->produk_id)->first();
                $invoice['detail'][$key]['nama']    = $produk->nama;
                $invoice['detail'][$key]['satuan']  = $produk->satuan;
            }
            elseif($value->jenis_barang_id == 2) {
                $hadiah = Hadiah::where('id', $value->produk_id)->first();
                $invoice['detail'][$key]['nama']    = $hadiah->nama;
                $invoice['detail'][$key]['satuan']  = $hadiah->satuan;
            }
            elseif($value->jenis_barang_id == 3) {
                $paket = Paket::where('id', $value->produk_id)->first();
                $invoice['detail'][$key]['nama']    = $paket->nama;
                $invoice['detail'][$key]['satuan']  = "paket";
            }

            $invoice['detail'][$key]['harga']       = $value->harga;
            $invoice['detail'][$key]['jumlah']      = $value->jumlah;
            $invoice['detail'][$key]['sub_total']       = $value->harga * $value->jumlah;

            $total_akhir += $value->harga * $value->jumlah;
            $jml_item    += $value->jumlah;
        }

//        foreach ($so_voucher as $item => $value) {
//            $invoice['voucher'][$item]['nominal']        = $value->nominal;
//        }

        $total_voucher = 0;
        foreach ($so_voucher as $key => $value) {
            $total_voucher = $total_voucher + $value->nominal;
        }

        $invoice['total_voucher']                        = $total_voucher;
        $invoice['catatan']                              = $so_header->catatan;
        $invoice['jml_item']                             = $jml_item;
        $invoice['total']                                = $total_akhir;
        $invoice['terbilang']                            = $this->dibacaTerbilang($terbilang);

        $invoice['error']            = "";
        $invoice['error_description']= "";

        return response()->json($invoice);
    }

    public function cekVoucherHome()
    {
        $total_bayar    = Input::get('total_bayar');
        $kode_voucher   = Input::get('kode_voucher');

        $pelanggan = Pelanggan::where('user_id', Auth::user()->id)->first();

        date_default_timezone_set("Asia/Jakarta");
        $tanggal        = new DateTime;

        //cek apakah voucher masih berlaku dan diperuntukkan untuk pelanggan tsb
        $voucher = Voucher::where('kode', $kode_voucher)->where('is_aktif', 1)->where('awal_periode', '<=', $tanggal->format('Y-m-d'))->where('akhir_periode', '>=', $tanggal->format('Y-m-d'))->first();

        $myvoucher['kode']      = "";
        $myvoucher['nominal']   = "";

        if(!empty($voucher)){
            if($total_bayar >= $voucher->nominal){
                if(empty($voucher->pelanggan_id_target) || $voucher->pelanggan_id_target == $pelanggan->id){
                    $myvoucher['kode']      = $voucher->kode;
                    $myvoucher['nominal']   = $voucher->nominal;
                }
            }
        }

        return Response::json($myvoucher);
    }

}
