<?php
/**
 * Created by PhpStorm.
 * User: Dell
 * Date: 07/01/2017
 * Time: 11:02
 */
include "ApiGeneral.php";
use LucaDegasperi\OAuth2Server\Authorizer;


/* * ********************************************* ALL API CONTROLLER **************************************************
 * ****************************************************************************************************************** */

Route::group(array('prefix' => 'api'), function() {
    // API General
    Route::resource('/api-kategori-produk', 'API\ApiKategoriController', array('except' => array('create', 'edit', 'update', 'destroy', 'store')));
    Route::get('/api-kota', 'API\ApiGeneral@getListAllKota');
    Route::get('/api-kota/{id_prov}', 'API\ApiGeneral@getListKota');
    Route::get('/api-provinsi', 'API\ApiGeneral@getListProvinsi');
    Route::get('/api-provinsi/{kota_id}', 'API\ApiGeneral@getProvinsiFromKota');
    Route::get('/webview-cara-belanja', 'API\ApiWebViewCaraBelanja@index');

    // API PRODUK
//    Route::group(['prefix' => '/api-produk'], function () {
//        $user_id = Authorizer::getResourceOwnerId();
//        if (empty($user_id)) {
//            Route::get('/produk-pilihan', 'API\ApiProdukController@getProdukPilihan');
//        } else {
//            Route::get('/produk-pilihan', 'API\ApiProdukController@getProdukPilihan');
//        }
//    });

    Route::get('/api-produk', 'API\ApiProdukController@getAllProduk');
    Route::get('/api-produk/produk-baru', 'API\ApiProdukController@getProdukBaru');
    Route::get('/api-produk/produk-pilihan', 'API\ApiProdukController@getProdukPilihan');
    Route::get('/api-produk/produk-akan-datang', 'API\ApiProdukController@getProdukAkanDatang');
    Route::get('/api-produk/promo-diskon', 'API\ApiProdukController@getPromoDiskon');
    Route::get('/api-produk/promo-diskon/{kategori_id}', 'API\ApiProdukController@getPromoDiskonByKategori');
    Route::get('/api-produk/promo-cashback', 'API\ApiProdukController@getPromoCashback');
    Route::get('/api-produk/promo-cashback/{kategori_id}', 'API\ApiProdukController@getPromoCashbackByKategori');
    Route::get('/api-produk/promo-hadiah', 'API\ApiProdukController@getPromoHadiah');
    Route::get('/api-produk/promo-hadiah/{kategori_id}', 'API\ApiProdukController@getPromoHadiahByKategori');
    Route::get('/api-produk/paket-produk', 'API\ApiProdukController@getPaketProduk');
    Route::get('/api-produk/{id}', 'API\ApiProdukController@showById');
    Route::get('/api-produk/kategori/{kategori_id}', 'API\ApiProdukController@getProdukByKategori');

    // API PELANGGAN
    Route::resource('/api-pelanggan', 'API\ApiPelangganController');

    // API Banner
    Route::get('/api-banner/slider', 'API\ApiBanerController@getSliderPromo');
    Route::get('/api-banner/mini-banner', 'API\ApiBanerController@getMiniBanner');

    // API Login
    Route::post('/access_token', function() {
        return Response::json(Authorizer::issueAccessToken());
    });

    // Api untuk User
    Route::post('/user/register', 'API\ApiUserController@registerUser');
    Route::post('/user/login', 'API\ApiUserController@actionLogin');
    Route::group(['middleware' => 'oauth'], function () {
        Route::get('/user/detail-user', 'API\ApiUserController@detailUser');
        Route::post('/user/edit-user', 'API\ApiUserController@editUser');
        Route::post('/user/edit-password', 'API\ApiUserController@editPassword');
        Route::post('/user/lupa-password', 'API\ApiUserController@lupaPassword');
        Route::get('/user/logout', 'API\ApiGeneral@actionLogout');
    });


    // API Transaksi
    Route::group(['middleware' => 'oauth'], function() {
        Route::post('/trans/cart/add', 'API\ApiTransaksiController@addToCart');
        Route::get('/trans/cart', 'API\ApiTransaksiController@showCart');
        Route::post('/trans/cart/del-pesanan', 'API\ApiTransaksiController@hapusPesanCart');
        Route::post('/trans/cart/add-jumlah-pesanan', 'API\ApiTransaksiController@addJmlPesanCart');
        Route::post('/trans/cart/min-jumlah-pesanan', 'API\ApiTransaksiController@minJmlPesanCart');
        Route::get('/trans/data-pengiriman', 'API\ApiTransaksiController@dataPengiriman');
        Route::post('/trans/send-data-pengiriman', 'API\ApiTransaksiController@sendDataPengiriman');
        Route::post('/trans/send-data-pembayaran', 'API\ApiTransaksiController@sendDataPembayaran');
        Route::get('/trans/review-pesanan', 'API\ApiTransaksiController@reviewPesanan');
        Route::post('/trans/proses-checkout', 'API\ApiTransaksiController@prosesCheckout');
        Route::get('/trans/invoice/{so_header_id}', 'API\ApiTransaksiController@getInvoice');
    });
    Route::get('/trans/metode-pengiriman', 'API\ApiTransaksiController@getMetodePengiriman');
    Route::get('/trans/metode-pembayaran', 'API\ApiTransaksiController@getMetodePembayaran');
});