<?php

namespace App\Http\Controllers\API;

use App\Models\PaketProduk\Paket;
use App\Models\AdminEcommerce\ProdukAkanDatang;
use App\Models\AdminEcommerce\ProdukBaru;
use App\Models\AdminEcommerce\ProdukPilihan;
use App\Models\Produk\PromoCashback;
use App\Models\Produk\PromoDiskon;
use App\Models\Produk\PromoHadiah;
use Illuminate\Http\Request;
use App\Models\Produk\ParameterSpesifikasi;

use App\Models\Pelanggan\Pelanggan;
use App\Models\AdminEcommerce\Konten;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use LucaDegasperi\OAuth2Server\Authorizer;
use Response;
use App\Models\Produk\Produk;
use Input;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ApiProdukController extends ApiGeneral
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllProduk()
    {
        $kategori_pelanggan_id  = 0;
        $gambar_default = DB::table('static')->where('nama', 'gambar_produk_default')->first();

        if(Auth::check()){
            $pelanggan              = Pelanggan::with('kategori_pelanggan')->where('user_id', Auth::user()->id)->first();
            if(!empty($pelanggan))
                $kategori_pelanggan_id  = $pelanggan->kategori_id;
        }

        $obj_produk = Produk::with(
            [ 'produk_galeri', 'promo_diskon', 'promo_cashback', 'promo_hadiah' => function($query) {
                return $query->with('hadiah');
            }, 'produk_harga' => function ($query) use ($kategori_pelanggan_id) {
                $query->where('kategori_pelanggan_id', $kategori_pelanggan_id);
            }])->where('is_aktif', 1)
            ->orderBy('nama', 'asc')->take(8)->get();

        $produk = [];
        foreach ($obj_produk as $key => $value) {
            $produk[$key]['id']                = $value->id;
            $produk[$key]['kode']         = $value->kode;
            $produk[$key]['nama']         = $value->nama;
            $produk[$key]['harga_retail'] = $value->harga_retail;

            if(!empty($value->produk_galeri_first->file_gambar)){
                $produk[$key]['file_gambar'] = "http://130.211.105.244/pos/public/img/produk/" . $value->produk_galeri_first->file_gambar;
            }
            else{
                $produk[$key]['file_gambar'] = "http://130.211.105.244/pos/public/img/GPDKEH8A5KZ.jpg";
            }

            $produk[$key]['harga_lama']         = $value->harga_retail;
            $produk[$key]['diskon']             = 0; 
            $produk[$key]['qty_beli_diskon']    = 1;
            $produk[$key]['cashback']           = 0; 
            $produk[$key]['qty_beli_cashback']  = 1;
            $produk[$key]['hadiah']             = "";
            $produk[$key]['qty_beli_hadiah']    = 1;
            $produk[$key]['qty_hadiah']         = 1;
            $produk[$key]['stok_tersedia']      = $value->stok - $value->stok_dipesan;

            if(!empty($pelanggan->kategori_pelanggan)){
                if($pelanggan->kategori_pelanggan->default_diskon > $produk[$key]['diskon']){
                    $produk[$key]['diskon'] = $pelanggan->kategori_pelanggan->default_diskon;
                    $produk[$key]['harga_retail'] = $value->harga_retail - ($value->harga_retail * $pelanggan->kategori_pelanggan->default_diskon / 100);
                }
            }
            
            if(!empty($value->produk_harga)){
                if($value->produk_harga->diskon > $produk[$key]['diskon']){
                    $produk[$key]['diskon']     = $value->produk_harga->diskon;
                    $produk[$key]['harga_retail'] = $value->harga_retail - ($value->harga_retail * $value->produk_harga->diskon / 100);
                }
            }
            
            if(!empty($value->promo_diskon)){
                if($value->promo_diskon->diskon > $produk[$key]['diskon']){
                    $produk[$key]['diskon'] = $value->promo_diskon->diskon;
                    $produk[$key]['qty_beli_diskon'] = $value->promo_diskon->qty_beli;
                    $produk[$key]['harga_retail'] = $value->harga_retail - ($value->harga_retail * $value->promo_diskon->diskon / 100);
                }
            }

            if(!empty($value->promo_cashback) && empty($value->produk_harga) && empty($pelanggan->kategori_pelanggan))
            {
                $produk[$key]['cashback']           = $value->promo_cashback->cashback;
                $produk[$key]['qty_beli_cashback']  = $value->promo_cashback->qty_beli;
                $produk[$key]['harga_retail']       = $value->harga_retail - $value->promo_cashback->cashback;
            }
            
            if(!empty($value->promo_hadiah))
            {
                $produk[$key]['hadiah']             = $value->promo_hadiah->hadiah->nama;
                $produk[$key]['qty_beli_hadiah']    = $value->promo_hadiah->qty_beli;
                $produk[$key]['qty_hadiah']         = $value->promo_hadiah->qty_hadiah;
            }

            // Deskripsi dan Detail Produk
            $produk[$key]['satuan']       = $value->satuan;
            $produk[$key]['berat']        = $value->berat;
            $produk[$key]['spesifikasi']  = $value->spesifikasi;
            $produk[$key]['deskripsi']    = strip_tags(str_replace(array("\r", "\n", "&nbsp;"), array('.',' ', ' '), $value->deskripsi));
            $produk[$key]['hpp']          = $value->hpp;
            $produk[$key]['kategori_id']  = $value->kategori_id;
            $produk[$key]['kategori_nama']= $value->kategori_produk->nama;
            $produk[$key]['jenis_barang_id'] = $value->jenis_barang_id;

            $spesifikasi = ParameterSpesifikasi::with(['nilai_spesifikasi'])->where('kategori_produk_id', $value->kategori_id)->get();

            $produk[$key]['spesifikasi'] = array_map([$this, 'transformResponse'], $spesifikasi->toArray());
        }
        return Response::json($produk, 200);   
    }

    public function getProdukByKategori($kategori_id) {
        $kategori_pelanggan_id  = 0;
        $gambar_default = DB::table('static')->where('nama', 'gambar_produk_default')->first();

        if(Auth::check()){
            $pelanggan              = Pelanggan::with('kategori_pelanggan')->where('user_id', Auth::user()->id)->first();
            if(!empty($pelanggan))
                $kategori_pelanggan_id  = $pelanggan->kategori_id;
        }

        $obj_produk = Produk::where('kategori_id', $kategori_id)->with(
            [ 'produk_galeri', 'promo_diskon', 'promo_cashback', 'promo_hadiah' => function($query) {
                return $query->with('hadiah');
            }, 'produk_harga' => function ($query) use ($kategori_pelanggan_id) {
                $query->where('kategori_pelanggan_id', $kategori_pelanggan_id);
            }])->where('is_aktif', 1)
            ->orderBy('nama', 'asc')->take(8)->get();

        $produk = [];
        foreach ($obj_produk as $key => $value) {
            $produk[$key]['id']           = $value->id;
            $produk[$key]['kode']         = $value->kode;
            $produk[$key]['nama']         = $value->nama;
            $produk[$key]['harga_retail'] = $value->harga_retail;

            if(!empty($value->produk_galeri_first->file_gambar)){
                $produk[$key]['file_gambar'] = "http://130.211.105.244/pos/public/img/produk/" . $value->produk_galeri_first->file_gambar;
            }
            else{
                $produk[$key]['file_gambar'] = "http://130.211.105.244/pos/public/img/GPDKEH8A5KZ.jpg";
            }

            $produk[$key]['harga_lama']         = $value->harga_retail;
            $produk[$key]['diskon']             = 0;
            $produk[$key]['qty_beli_diskon']    = 1;
            $produk[$key]['cashback']           = 0;
            $produk[$key]['qty_beli_cashback']  = 1;
            $produk[$key]['hadiah']             = "";
            $produk[$key]['qty_beli_hadiah']    = 1;
            $produk[$key]['qty_hadiah']         = 1;
            $produk[$key]['stok_tersedia']      = $value->stok - $value->stok_dipesan;

            if(!empty($pelanggan->kategori_pelanggan)){
                if($pelanggan->kategori_pelanggan->default_diskon > $produk[$key]['diskon']){
                    $produk[$key]['diskon'] = $pelanggan->kategori_pelanggan->default_diskon;;
                    $produk[$key]['harga_retail'] = $value->harga_retail - ($value->harga_retail * $pelanggan->kategori_pelanggan->default_diskon / 100);
                }
            }

            if(!empty($value->produk_harga)){
                if($value->produk_harga->diskon > $produk[$key]['diskon']){
                    $produk[$key]['diskon'] = $value->produk_harga->diskon;
                    $produk[$key]['harga_retail'] = $value->harga_retail - ($value->harga_retail * $value->produk_harga->diskon / 100);
                }
            }

            if(!empty($value->promo_diskon)){
                if($value->promo_diskon->diskon > $produk[$key]['diskon']){
                    $produk[$key]['diskon'] = $value->promo_diskon->diskon;
                    $produk[$key]['qty_beli_diskon'] = $value->promo_diskon->qty_beli;
                    $produk[$key]['harga_retail'] = $value->harga_retail - ($value->harga_retail * $value->promo_diskon->diskon / 100);
                }
            }

            if(!empty($value->promo_cashback) && empty($value->produk_harga) && empty($pelanggan->kategori_pelanggan))
            {
                $produk[$key]['cashback']           = $value->promo_cashback->cashback;
                $produk[$key]['qty_beli_cashback']  = $value->promo_cashback->qty_beli;
                $produk[$key]['harga_retail']       = $value->harga_retail - $value->promo_cashback->cashback;
            }

            if(!empty($value->promo_hadiah))
            {
                $produk[$key]['hadiah']             = $value->promo_hadiah->hadiah->nama;
                $produk[$key]['qty_beli_hadiah']    = $value->promo_hadiah->qty_beli;
                $produk[$key]['qty_hadiah']         = $value->promo_hadiah->qty_hadiah;
            }

            // Deskripsi dan Detail Produk
            $produk[$key]['satuan']       = $value->satuan;
            $produk[$key]['berat']        = $value->berat;
            $produk[$key]['spesifikasi']  = $value->spesifikasi;
            $produk[$key]['deskripsi']    = strip_tags(str_replace(array("\r", "\n", "&nbsp;"), array('.',' ', ' '), $value->deskripsi));;
            $produk[$key]['hpp']          = $value->hpp;
            $produk[$key]['kategori_id']  = $value->kategori_id;
            $produk[$key]['kategori_nama']= $value->kategori_produk->nama;
            $produk[$key]['jenis_barang_id'] = $value->jenis_barang_id;

            $spesifikasi = ParameterSpesifikasi::with(['nilai_spesifikasi'])->where('kategori_produk_id', $value->kategori_id)->get();

            $produk[$key]['spesifikasi'] = array_map([$this, 'transformResponse'], $spesifikasi->toArray());
        }

        return Response::json($produk, 200);
    }

    public function getProdukOrderAscending() {
        $produk = Produk::orderBy('nama', 'asc')->get();
        return response()->json($produk, 200);
    }

    public function getProdukOrderDescending() {
        $produk = Produk::orderBy('nama', 'desc')->get();
        return response()->json($produk, 200);
    }

    public function getProdukOrderRateUp() {
        $produk = Produk::orderBy('harga_retail', 'asc')->get();
        return response()->json($produk, 200);
    }

    public function getProdukOrderRateDown() {
        $produk = Produk::orderBy('harga_retail', 'desc')->get();
        return response()->json($produk, 200);
    }

    public function getProdukFilterPriceBetween($low, $high) {
        $produk = Produk::whereBetween('harga_retail', array($low, $high))->get();
        return response()->json($produk, 200);
    }

    public function getProdukFilterSpec() {
        
    }

    public function getProdukPilihan() {
//        $user_id        = $this->getUserLoggedIn()->id;
        $kategori_pelanggan_id = 0;
        if (!empty($user_id)) {
            $pelanggan              = Pelanggan::with('kategori_pelanggan')->where('user_id', $user_id)->first();
            if(!empty($pelanggan)) {
                $kategori_pelanggan_id  = $pelanggan->kategori_id;
            }
        }

        $obj_produk_pilihan = ProdukPilihan::orderBy('item_order', 'asc')->get();
        $produk_pilihan = [];
        $obj_gambar_produk_default = Konten::where('nama', 'gambar_produk_default')->first();
        if (empty($obj_gambar_produk_default)) {
            $gambar_produk_default = "";
        } else {
            $gambar_produk_default = $obj_gambar_produk_default->keterangan;
        }

        foreach ($obj_produk_pilihan as $key => $value) {
            $produk_pilihan[$key]['id'] = $value->id;
            $produk_pilihan[$key]['nama'] = $value->nama;
            $produk_pilihan[$key]['url'] = $value->url;
            $produk_pilihan[$key]['file_gambar'] = $value->file_gambar;

            $arr_list_produk = $value->list_produk;
            $arr_list_produk = explode(',', $arr_list_produk);

            $list_produk = [];
            for ($j = 0; $j < count($arr_list_produk); $j++) {
                $obj_produk = Produk::with(['produk_galeri_first', 'promo_diskon', 'promo_cashback',
                    'promo_hadiah' => function ($query) {
                        return $query->with('hadiah');
                    },
                    'produk_harga' => function ($query) use ($kategori_pelanggan_id) {
                        $query->where('kategori_pelanggan_id', $kategori_pelanggan_id);
                    }])->where('is_aktif', 1)->where('id', $arr_list_produk[$j])->first();

                $list_produk[$j]['slug_nama'] = $obj_produk->slug_nama;
                $list_produk[$j]['nama'] = $obj_produk->nama;
                $list_produk[$j]['harga_retail'] = $obj_produk->harga_retail;

                if (!empty($obj_produk->produk_galeri_first->file_gambar)) {
                    $list_produk[$j]['file_gambar_first'] = url('/img/produk/' . $obj_produk->produk_galeri_first->file_gambar);
                } else {
                    $list_produk[$j]['file_gambar_first'] = url('/img/' . $gambar_produk_default);
                }

                $list_produk[$j]['diskon'] = 0;
                $list_produk[$j]['qty_beli_diskon'] = 1;
                $list_produk[$j]['cashback'] = 0;
                $list_produk[$j]['qty_beli_cashback'] = 1;
                $list_produk[$j]['hadiah'] = "";
                $list_produk[$j]['qty_beli_hadiah'] = 1;
                $list_produk[$j]['qty_hadiah'] = 1;
                $list_produk[$j]['stok_tersedia'] = $obj_produk->stok - $obj_produk->stok_dipesan;

                if (!empty($pelanggan->kategori_pelanggan)) {
                    if ($pelanggan->kategori_pelanggan->default_diskon > $list_produk[$j]['diskon']) {
                        $list_produk[$j]['diskon'] = $pelanggan->kategori_pelanggan->default_diskon;
                    }
                }

                if (!empty($obj_produk->produk_harga)) {
                    if ($obj_produk->produk_harga->diskon > $list_produk[$j]['diskon']) {
                        $list_produk[$j]['diskon'] = $obj_produk->produk_harga->diskon;
                    }
                }

                if (!empty($obj_produk->promo_diskon)) {
                    if ($obj_produk->promo_diskon->diskon > $list_produk[$j]['diskon']) {
                        $list_produk[$j]['diskon'] = $obj_produk->promo_diskon->diskon;
                        $list_produk[$j]['qty_beli_diskon'] = $obj_produk->promo_diskon->qty_beli;
                    }
                }

                if (!empty($obj_produk->promo_cashback) && empty($obj_produk->produk_harga) && empty($pelanggan->kategori_pelanggan)) {
                    $list_produk[$j]['cashback'] = $obj_produk->promo_cashback->cashback;
                    $list_produk[$j]['qty_beli_cashback'] = $obj_produk->promo_cashback->qty_beli;
                }

                if (!empty($obj_produk->promo_hadiah)) {
                    $list_produk[$j]['hadiah'] = $obj_produk->promo_hadiah->hadiah->nama;
                    $list_produk[$j]['qty_beli_hadiah'] = $obj_produk->promo_hadiah->qty_beli;
                    $list_produk[$j]['qty_hadiah'] = $obj_produk->promo_hadiah->qty_hadiah;
                }
            }

            $produk_pilihan[$key]['list_produk'] = $list_produk;
        }

        return Response::json($produk_pilihan, 200);
    }

    public function getProdukBaru() {
//        $user_id        = $this->getUserLoggedIn()->id;
        $kategori_pelanggan_id = 0;
        if (!empty($user_id)) {
            $pelanggan              = Pelanggan::with('kategori_pelanggan')->where('user_id', $user_id)->first();
            if(!empty($pelanggan)) {
                $kategori_pelanggan_id  = $pelanggan->kategori_id;
            }
        }


        $produk = ProdukBaru::with(
            ['produk' => function ($query) {
                return $query->with(['promo_diskon', 'promo_cashback', 'produk_galeri',
                    'promo_hadiah' => function($query1) {
                        return $query1->with('hadiah');
                    }
//                    , 'produk_harga' => function ($query) use ($kategori_pelanggan_id) {
//                        return $query->where('kategori_pelanggan_id', $kategori_pelanggan_id);
//                    }
                ]);
            }])->orderBy('produk_id')->take(10)->get();

        $produks =[];
        foreach ($produk as $key => $value) {

            $produks[$key]['id']            = $value->produk_id;
            $produks[$key]['kode']          = $value->produk->kode;
            $produks[$key]['nama']          = $value->produk->nama;
            $produks[$key]['satuan']        = $value->produk->satuan;
            $produks[$key]['berat']         = $value->produk->berat;
            $produks[$key]['harga_retail']  = $value->produk->harga_retail;
            $produks[$key]['stok_tersedia'] = $value->produk->stok - $value->produk->stok_dipesan;
            $produks[$key]['kategori_id']   = $value->produk->kategori_id;
            $produks[$key]['tanggal_input'] = $value->tanggal_input;

            if(!empty($value->produk->produk_galeri_first->file_gambar)){
                $produks[$key]['file_gambar'] = "http://130.211.105.244/pos/public/img/produk/" . $value->produk->produk_galeri_first->file_gambar;
            }
            else{
                $produks[$key]['file_gambar'] = "http://130.211.105.244/pos/public/img/GPDKEH8A5KZ.jpg";
            }

            $produks[$key]['harga_lama']         = $value->harga_retail;
            $produks[$key]['diskon']             = 0;
            $produks[$key]['qty_beli_diskon']    = 1;
            $produks[$key]['cashback']           = 0;
            $produks[$key]['qty_beli_cashback']  = 1;
            $produks[$key]['hadiah']             = "";
            $produks[$key]['qty_beli_hadiah']    = 1;
            $produks[$key]['qty_hadiah']         = 1;
            $produks[$key]['stok_tersedia']      = $value->stok - $value->stok_dipesan;

            if(!empty($pelanggan->kategori_pelanggan)){
                if($pelanggan->kategori_pelanggan->default_diskon > $produk[$key]['diskon']){
                    $produks[$key]['diskon'] = $pelanggan->kategori_pelanggan->default_diskon;;
                    $produks[$key]['harga_retail'] = $value->harga_retail - ($value->harga_retail * $pelanggan->kategori_pelanggan->default_diskon / 100);
                }
            }

            if(!empty($value->produk_harga)){
                if($value->produk_harga->diskon > $produk[$key]['diskon']){
                    $produks[$key]['diskon'] = $value->produk_harga->diskon;
                    $produks[$key]['harga_retail'] = $value->harga_retail - ($value->harga_retail * $value->produk_harga->diskon / 100);
                }
            }

            if(!empty($value->promo_diskon)){
                if($value->promo_diskon->diskon > $produk[$key]['diskon']){
                    $produks[$key]['diskon'] = $value->promo_diskon->diskon;
                    $produks[$key]['qty_beli_diskon'] = $value->promo_diskon->qty_beli;
                    $produks[$key]['harga_retail'] = $value->harga_retail - ($value->harga_retail * $value->promo_diskon->diskon / 100);
                }
            }

//            if(!empty($value->promo_cashback) && empty($value->produk_harga) && empty($pelanggan->kategori_pelanggan))
            if(!empty($value->promo_cashback))
            {
                $produks[$key]['cashback']           = $value->promo_cashback->cashback;
                $produks[$key]['qty_beli_cashback']  = $value->promo_cashback->qty_beli;
                $produks[$key]['harga_retail']       = $value->harga_retail - $value->promo_cashback->cashback;
            }

            if(!empty($value->promo_hadiah))
            {
                $produks[$key]['hadiah']             = $value->promo_hadiah->hadiah->nama;
                $produks[$key]['qty_beli_hadiah']    = $value->promo_hadiah->qty_beli;
                $produks[$key]['qty_hadiah']         = $value->promo_hadiah->qty_hadiah;
            }
        }

        return Response::json($produks, 200);
    }

    public function getProdukAkanDatang() {
//        $user_id        = $this->getUserLoggedIn()->id;
        $kategori_pelanggan_id = 0;
        if (!empty($user_id)) {
            $pelanggan              = Pelanggan::with('kategori_pelanggan')->where('user_id', $user_id)->first();
            if(!empty($pelanggan)) {
                $kategori_pelanggan_id  = $pelanggan->kategori_id;
            }
        }

        $produk = ProdukAkanDatang::with([
            'produk' => function($query) use ($kategori_pelanggan_id) {
                return $query->with(['produk_galeri_first', 'promo_diskon', 'promo_cashback',
                    'promo_hadiah' => function($query) {
                        return $query->with('hadiah');
                    }
    //                    ,'produk_harga' => function ($query) use ($kategori_pelanggan_id) {
    //                        $query->where('kategori_pelanggan_id', $kategori_pelanggan_id);
    //                    }
                ])->where('is_aktif', 1);
            }])->get();

        $produks =[];
        foreach ($produk as $key => $value) {

            $produks[$key]['id']            = $value->produk_id;
            $produks[$key]['kode']          = $value->produk->kode;
            $produks[$key]['nama']          = $value->produk->nama;
            $produks[$key]['satuan']        = $value->produk->satuan;
            $produks[$key]['berat']         = $value->produk->berat;
            $produks[$key]['harga_retail']  = $value->produk->harga_retail;
            $produks[$key]['stok_tersedia'] = $value->produk->stok - $value->produk->stok_dipesan;
            $produks[$key]['kategori_id']   = $value->produk->kategori_id;
            $produks[$key]['tanggal_input'] = $value->tanggal_input;
            if(!empty($value->produk->produk_galeri_first->file_gambar)){
                $produks[$key]['file_gambar'] = url('/img/produk/' . $value->produk->produk_galeri_first->file_gambar);
            }
            else{
                $produks[$key]['file_gambar'] = url('/img/GPDKEH8A5KZ.jpg');
            }
        }

        return Response::json($produks, 200);
    }

    public function getPromoDiskon() {
        $produk = PromoDiskon::with(['produk'])
            ->orderBy('produk_id')
            ->get();

        if (empty($produk[0]->produk)) {
            return response()->json(['error' => true, 'message' => 'Produk tidak ada yang cocok']);
        }

        $produks = [];
        foreach ($produk as $key => $value) {
            $produks[$key]['id']            = $value->id;
            $produks[$key]['kode']          = $value->produk->kode;
            $produks[$key]['nama']          = $value->produk->nama;
            $produks[$key]['satuan']        = $value->produk->satuan;
            $produks[$key]['berat']         = $value->produk->berat;
            $produks[$key]['harga_retail']  = $value->produk->harga_retail - ($value->produk->harga_retail * $value->diskon / 100);
            $produks[$key]['harga_lama']    = $value->produk->harga_retail;
            $produks[$key]['stok_tersedia'] = $value->produk->stok - $value->produk->stok_dipesan;
            $produks[$key]['kategori_id']   = $value->produk->kategori_id;

            $produks[$key]['qty_beli']      = $value->qty_beli;
            $produks[$key]['diskon']        = $value->diskon;
            $produks[$key]['awal_periode']  = $value->awal_periode;
            $produks[$key]['akhir_periode'] = $value->akhir_periode;

            if(!empty($value->produk->produk_galeri_first->file_gambar)){
                $produks[$key]['file_gambar'] = "http://130.211.105.244/pos/public/img/produk/" . $value->produk->produk_galeri_first->file_gambar;
            }
            else{
                $produks[$key]['file_gambar'] = "http://130.211.105.244/pos/public/img/GPDKEH8A5KZ.jpg";
            }
        }

        if (!$produks) {
            return Response::json(['error' => '', 'message' => 'Data tidak ada']);
        } else {
            return Response::json($produks, 200);
        }
    }


    public function getPromoDiskonByKategori($kategori_id) {
        $produk = PromoDiskon::with(['produk' => function($query) use ($kategori_id) {
                return $query->where('kategori_id', $kategori_id);
            }])->orderBy('produk_id')
            ->get();

        if (empty($produk[0]->produk)) {
            return response()->json(['error' => true, 'message' => 'Produk tidak ada yang cocok']);
        }

        $produks = [];
        foreach ($produk as $key => $value) {
            $produks[$key]['id']            = $value->id;
            $produks[$key]['kode']          = $value->produk->kode;
            $produks[$key]['nama']          = $value->produk->nama;
            $produks[$key]['satuan']        = $value->produk->satuan;
            $produks[$key]['berat']         = $value->produk->berat;
            $produks[$key]['harga_retail']  = $value->produk->harga_retail - ($value->produk->harga_retail * $value->diskon / 100);
            $produks[$key]['harga_lama']    = $value->produk->harga_retail;
            $produks[$key]['stok_tersedia'] = $value->produk->stok - $value->produk->stok_dipesan;
            $produks[$key]['kategori_id']   = $value->produk->kategori_id;

            $produks[$key]['qty_beli']      = $value->qty_beli;
            $produks[$key]['diskon']        = $value->diskon;
            $produks[$key]['awal_periode']  = $value->awal_periode;
            $produks[$key]['akhir_periode'] = $value->akhir_periode;

            if(!empty($value->produk->produk_galeri_first->file_gambar)){
                $produks[$key]['file_gambar'] = "http://130.211.105.244/pos/public/img/produk/" . $value->produk->produk_galeri_first->file_gambar;
            }
            else{
                $produks[$key]['file_gambar'] = "http://130.211.105.244/pos/public/img/GPDKEH8A5KZ.jpg";
            }
        }

        if (!$produks) {
            return Response::json(['error' => '', 'message' => 'Data tidak ada']);
        } else {
            return Response::json($produks, 200);
        }
    }

    public function getPromoCashback() {
        $produk = PromoCashback::with('produk')->orderBy('produk_id')->take(10)->get();

        if (empty($produk[0]->produk)) {
            return response()->json(['error' => true, 'message' => 'Produk tidak ada yang cocok']);
        }

        $produks = [];
        foreach ($produk as $key => $value) {
            $produks[$key]['id']            = $value->id;
            $produks[$key]['kode']          = $value->produk->kode;
            $produks[$key]['nama']          = $value->produk->nama;
            $produks[$key]['satuan']        = $value->produk->satuan;
            $produks[$key]['berat']         = $value->produk->berat;
            $produks[$key]['harga_retail']  = $value->produk->harga_retail;
            $produks[$key]['stok_tersedia'] = $value->produk->stok - $value->produk->stok_dipesan;
            $produks[$key]['kategori_id']   = $value->produk->kategori_id;

            $produks[$key]['qty_beli']      = $value->qty_beli;
            $produks[$key]['cashback']      = $value->cashback;
            $produks[$key]['awal_periode']  = $value->awal_periode;
            $produks[$key]['akhir_periode'] = $value->akhir_periode;

            if(!empty($value->produk->produk_galeri_first->file_gambar)){
                $produks[$key]['file_gambar'] = "http://130.211.105.244/pos/public/img/produk/" . $value->produk->produk_galeri_first->file_gambar;
            }
            else{
                $produks[$key]['file_gambar'] = "http://130.211.105.244/pos/public/img/GPDKEH8A5KZ.jpg";
            }
        }
        if (!$produks) {
            return Response::json(['error' => '', 'message' => 'Data tidak ada']);
        } else {
            return Response::json($produks, 200);
        }
    }

    public function getPromoCashbackByKategori($kategori_id) {
        $produk = PromoCashback::with(['produk' => function($query) use ($kategori_id) {
            return $query->where('kategori_id', $kategori_id);
        }])->orderBy('produk_id')->take(10)->get();

        if (empty($produk[0]->produk)) {
            return response()->json(['error' => true, 'message' => 'Produk tidak ada yang cocok']);
        }

        $produks = [];
        foreach ($produk as $key => $value) {
            $produks[$key]['id']            = $value->id;
            $produks[$key]['kode']          = $value->produk->kode;
            $produks[$key]['nama']          = $value->produk->nama;
            $produks[$key]['satuan']        = $value->produk->satuan;
            $produks[$key]['berat']         = $value->produk->berat;
            $produks[$key]['harga_retail']  = $value->produk->harga_retail;
            $produks[$key]['stok_tersedia'] = $value->produk->stok - $value->produk->stok_dipesan;
            $produks[$key]['kategori_id']   = $value->produk->kategori_id;

            $produks[$key]['qty_beli']      = $value->qty_beli;
            $produks[$key]['cashback']      = $value->cashback;
            $produks[$key]['awal_periode']  = $value->awal_periode;
            $produks[$key]['akhir_periode'] = $value->akhir_periode;

            if(!empty($value->produk->produk_galeri_first->file_gambar)){
                $produks[$key]['file_gambar'] = "http://130.211.105.244/pos/public/img/produk/" . $value->produk->produk_galeri_first->file_gambar;
            }
            else{
                $produks[$key]['file_gambar'] = "http://130.211.105.244/pos/public/img/GPDKEH8A5KZ.jpg";
            }
        }
        if (!$produks) {
            return Response::json(['error' => '', 'message' => 'Data tidak ada']);
        } else {
            return Response::json($produks, 200);
        }
    }

    public function getPromoHadiah() {
        $produk = PromoHadiah::with(['produk', 'hadiah'])
            ->orderBy('produk_id')
            ->get();

        if (empty($produk[0]->produk)) {
            return response()->json(['error' => true, 'message' => 'Produk tidak ada yang cocok']);
        }

        $produks = [];
        foreach ($produk as $key => $value) {
            $produks[$key]['id']            = $value->id;
            $produks[$key]['produk_id']     = $value->produk_id;
            $produks[$key]['nama']          = $value->produk->nama;
            $produks[$key]['satuan']        = $value->produk->satuan;
            $produks[$key]['berat']         = $value->produk->berat;
            $produks[$key]['harga_retail']  = $value->produk->harga_retail;
            $produks[$key]['stok_tersedia'] = $value->produk->stok - $value->produk->stok_dipesan;
            $produks[$key]['kategori_id']   = $value->produk->kategori_id;

            $produks[$key]['qty_beli']      = $value->qty_beli;
            $produks[$key]['qty_hadiah']    = $value->qty_hadiah;
            $produks[$key]['awal_periode']  = $value->awal_periode;
            $produks[$key]['akhir_periode'] = $value->akhir_periode;

            $produks[$key]['nama_hadiah'] = $value->hadiah->nama;
            $produks[$key]['satuan_hadiah'] = $value->hadiah->satuan;
            $produks[$key]['berat_hadiah'] = $value->hadiah->berat;
            $produks[$key]['deskripsi_hadiah'] = $value->hadiah->deskripsi;
            $produks[$key]['file_gambar_hadiah'] = $value->hadiah->file_gambar;
            if(!empty($value->produk->produk_galeri_first->file_gambar)) {
                $produks[$key]['file_gambar'] = url('/img/produk/' . $value->produk->produk_galeri_first->file_gambar);
            } else {
                $produks[$key]['file_gambar'] = url('/img/GPDKEH8A5KZ.jpg');
            }

        }
        if (!$produks) {
            return Response::json(['error' => '', 'message' => 'Data tidak ada']);
        } else {
            return Response::json($produks, 200);
        }
    }


    public function getPromoHadiahByKategori($kategori_id) {
        $produk = PromoHadiah::with(['hadiah', 'produk' => function($query) use ($kategori_id) {
            return $query->where('kategori_id', $kategori_id);
        }])->orderBy('produk_id')
            ->get();

        if (empty($produk[0]->produk)) {
            return response()->json(['error' => true, 'message' => 'Produk tidak ada yang cocok']);
        }

        $produks = [];
        foreach ($produk as $key => $value) {
            $produks[$key]['id']            = $value->id;
            $produks[$key]['produk_id']     = $value->produk_id;
            $produks[$key]['nama']          = $value->produk->nama;
            $produks[$key]['satuan']        = $value->produk->satuan;
            $produks[$key]['berat']         = $value->produk->berat;
            $produks[$key]['harga_retail']  = $value->produk->harga_retail;
            $produks[$key]['stok_tersedia'] = $value->produk->stok - $value->produk->stok_dipesan;
            $produks[$key]['kategori_id']   = $value->produk->kategori_id;

            $produks[$key]['qty_beli']      = $value->qty_beli;
            $produks[$key]['qty_hadiah']    = $value->qty_hadiah;
            $produks[$key]['awal_periode']  = $value->awal_periode;
            $produks[$key]['akhir_periode'] = $value->akhir_periode;

            $produks[$key]['nama_hadiah'] = $value->hadiah->nama;
            $produks[$key]['satuan_hadiah'] = $value->hadiah->satuan;
            $produks[$key]['berat_hadiah'] = $value->hadiah->berat;
            $produks[$key]['deskripsi_hadiah'] = $value->hadiah->deskripsi;
            $produks[$key]['file_gambar_hadiah'] = $value->hadiah->file_gambar;
            if(!empty($value->produk->produk_galeri_first->file_gambar)) {
                $produks[$key]['file_gambar'] = url('/img/produk/' . $value->produk->produk_galeri_first->file_gambar);
            } else {
                $produks[$key]['file_gambar'] = url('/img/GPDKEH8A5KZ.jpg');
            }

        }
        if (!$produks) {
            return Response::json(['error' => '', 'message' => 'Data tidak ada']);
        } else {
            return Response::json($produks, 200);
        }
    }

    public function getPaketProduk() {
        $produk = Paket::with(
            ['paket_produk' => function($query) {
                return $query->with(['produk']);
            }
            ])->get();

        return Response::json($produk, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showById($id)
    {
        $kategori_pelanggan_id  = 0;
//        $gambar_default = DB::table('static')->where('nama', 'gambar_produk_default')->first();

        if(Auth::check()){
            $pelanggan   = Pelanggan::with('kategori_pelanggan')->where('user_id', Auth::user()->id)->first();
            if(!empty($pelanggan))
                $kategori_pelanggan_id  = $pelanggan->kategori_id;
        }

        $produk = Produk::where('id', $id)->with(
            [ 'produk_galeri', 'produk_galeri_first', 'promo_diskon', 'promo_cashback', 'kategori_produk', 'promo_hadiah' => function($query) {
                return $query->with('hadiah');
            }, 'produk_harga' => function ($query) use ($kategori_pelanggan_id) {
                $query->where('kategori_pelanggan_id', $kategori_pelanggan_id);
            }])->where('is_aktif', 1)
            ->orderBy('nama', 'asc')->first();

        $obj_produk = [];
        $obj_produk['id']           = $id;
        $obj_produk['kode']         = $produk->kode;
        $obj_produk['nama']         = $produk->nama;
        $obj_produk['harga_retail'] = $produk->harga_retail;

        // untuk menampilkan semua gambar -> slider
        $file_gambar = $produk->produk_galeri;
        $url = url('/');

        $arr_file_gambar = [];
        foreach ($file_gambar as $key => $value) {
            $arr_file_gambar[$key] = $url . "/img/produk/" . $value->file_gambar;
        }
        $obj_produk['file_gambar'] = $arr_file_gambar;
        $file_gambar_first = "";
        if ($produk->produk_galeri_first->file_gambar != "") {
            $file_gambar_first      = url('/img/produk/' . $produk->produk_galeri_first->file_gambar);
        } else {
            $file_gambar_first      = url('/img/GPDKEH8A5KZ.jpg');
        }
        $obj_produk['file_gambar_first']  = $file_gambar_first;

        $obj_produk['harga_lama']         = $produk->harga_retail;
        $obj_produk['diskon']             = 0;
        $obj_produk['qty_beli_diskon']    = 1;
        $obj_produk['cashback']           = 0;
        $obj_produk['qty_beli_cashback']  = 1;
        $obj_produk['hadiah']             = "";
        $obj_produk['qty_beli_hadiah']    = 1;
        $obj_produk['qty_hadiah']         = 1;
        $obj_produk['stok_tersedia']      = $produk->stok - $produk->stok_dipesan;

        if(!empty($pelanggan->kategori_pelanggan)){
            if($pelanggan->kategori_pelanggan->default_diskon > $produk['diskon']){
                $obj_produk['diskon'] = $pelanggan->kategori_pelanggan->default_diskon;
                $obj_produk['harga_retail'] = $produk->harga_retail - ($produk->harga_retail * $pelanggan->kategori_pelanggan->default_diskon / 100);
            }
        }

        if(!empty($produk->produk_harga)){
            if($produk->produk_harga->diskon > $obj_produk['diskon']){
                $obj_produk['diskon'] = $produk->produk_harga->diskon;
                $obj_produk['harga_retail'] = $produk->harga_retail - ($produk->harga_retail * $produk->produk_harga->diskon / 100);
            }
        }

        if(!empty($produk->promo_diskon)){
            if($produk->promo_diskon->diskon > $obj_produk['diskon']){
                $obj_produk['diskon'] = $produk->promo_diskon->diskon;
                $obj_produk['qty_beli_diskon'] = $produk->promo_diskon->qty_beli;
                $obj_produk['harga_retail'] = $produk->harga_retail - ($produk->harga_retail * $produk->promo_diskon->diskon / 100);
            }
        }

        if(!empty($produk->promo_cashback) && empty($produk->produk_harga) && empty($pelanggan->kategori_pelanggan))
        {
            $obj_produk['cashback']           = $produk->promo_cashback->cashback;
            $obj_produk['qty_beli_cashback']  = $produk->promo_cashback->qty_beli;
            $obj_produk['harga_retail']       = $produk->harga_retail - $produk->promo_cashback->cashback;
        }

        if(!empty($produk->promo_hadiah))
        {
            $obj_produk['hadiah']             = $produk->promo_hadiah->hadiah->nama;
            $obj_produk['qty_beli_hadiah']    = $produk->promo_hadiah->qty_beli;
            $obj_produk['qty_hadiah']         = $produk->promo_hadiah->qty_hadiah;
        }

        // Deskripsi dan Detail Produk
        $obj_produk['satuan']       = $produk->satuan;
        $obj_produk['berat']        = $produk->berat;
        $obj_produk['spesifikasi']  = $produk->spesifikasi;
        $obj_produk['deskripsi']    = strip_tags(str_replace(array("\r", "\n", "&nbsp;"), array('.',' ', ' '), $produk->deskripsi));
        $obj_produk['hpp']          = $produk->hpp;
        $obj_produk['kategori_id']  = $produk->kategori_id;
        $obj_produk['kategori_nama']= $produk->kategori_produk->nama;
        $obj_produk['jenis_barang_id'] = $produk->jenis_barang_id;

        $spesifikasi = ParameterSpesifikasi::with(['nilai_spesifikasi' => function($query) use ($id)
        {
            return $query->where('produk_id', $id);
        }])->where('kategori_produk_id', $produk->kategori_id)->get();

        $obj_produk['spesifikasi'] = array_map([$this, 'transformResponse'], $spesifikasi->toArray());

        return Response::json($obj_produk, 200);

    }

    private function transformResponse($spesifikasi) {
        return [
            'nama'  => $spesifikasi['nama'],
            'nilai_spesifikasi' => [
                'nilai'     => $spesifikasi['nilai_spesifikasi']['nilai'],
                'satuan'    => $spesifikasi['nilai_spesifikasi']['satuan']
            ]
        ];
    }
}
