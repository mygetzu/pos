<?php

namespace App\Http\Controllers\API;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Response;
use Illuminate\Support\Facades\Auth;
use Authorizer;

class ApiGeneral extends Controller
{
    public function getListKota($id_prov) {
        $kota = DB::table('tref_kota')->where('provinsi_kode', $id_prov)->get();

        return Response::json($kota, 200);
    }

    public function getListAllKota() {
        $kota = DB::table('tref_kota')->get();

        return Response::json($kota, 200);
    }

    public function getListProvinsi() {
        $provinsi = DB::table('tref_provinsi')->get();

        return response()->json($provinsi);
    }

    public function getProvinsiFromKota($kota_id) {
        $kota = DB::table('tref_kota')->where('id', $kota_id)->first();
        $provinsi_id = $kota->provinsi_kode;

        if (!$provinsi_id) {
            return response()->json(['error' => true, 'message' => 'Data tidak ada']);
        }

        return response()->json(['provinsi_kode' => $provinsi_id]);
    }

    // Controller Login
    protected function getUserLoggedIn() {
        $user = User::find(Authorizer::getResourceOwnerId());
        return $user;
    }

    public function actionLogout() {
        Authorizer::getChecker()->getAccessToken()->expire();

        return response()->json(['error' => '', 'error_description' => '','message' => 'You have logged out']);
    }

    public function dibacaTerbilang($x)
    {
        $angkaBaca = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");

        switch ($x) {
            case ($x < 12):
                return " " . $angkaBaca[$x];
                break;
            case ($x < 20):
                return $result = $this->dibacaTerbilang($x - 10) . " Belas";
                break;
            case ($x < 100):
                $a = $this->dibacaTerbilang($x / 10);
                $b = " Puluh ";
                return $a . $b . $this->dibacaTerbilang($x % 10);
                break;
            case ($x < 200):
                $a = " Seratus ";
                return $a . $this->dibacaTerbilang($x - 100);
                break;
            case ($x < 1000):
                $a = $this->dibacaTerbilang($x / 100);
                $b = " Ratus";
                return $a . $b . $this->dibacaTerbilang($x % 100);
                break;
            case ($x < 2000):
                $a = " Seribu ";
                return $a . $this->dibacaTerbilang($x - 1000);
                break;
            case ($x < 1000000):
                $a = $this->dibacaTerbilang($x / 1000);
                $b = " Ribu ";
                return $a . $b . $this->dibacaTerbilang($x % 1000);
                break;
            case ($x < 1000000000):
                $a = $this->dibacaTerbilang($x / 1000000);
                $b = " Juta ";
                return $a . $b . $this->dibacaTerbilang($x % 1000000);
                break;
            case ($x < 1000000000000):
                $a = $this->dibacaTerbilang($x / 1000000000);
                $y = $x % 1000000000;
                $b = " Milyar $x % 1000000000=$y";
                return $a . $b . $this->dibacaTerbilang($x % 1000000000);
                break;
        }
    }
}
