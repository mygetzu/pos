<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\DB;
use Input;
use Auth;
use Validator;
use Illuminate\Http\RedirectResponse;
use App\Http\Controllers\AuthenticatesUsers;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use App\User;
use Session;
use Hash;
use Response;
use Hashids\Hashids;
use Mail;
use Illuminate\Support\Facades\Lang;
use DateTime;
use App\Models\Notifikasi;
use App\Models\Produk\Produk;
use App\Models\Produk\Hadiah;
use App\Models\Pelanggan\Pelanggan;
use App\Models\PaketProduk\Paket;

class UserController extends Controller {

    public function registrasi() {
        Auth::check();
        $provinsi = DB::table('tref_provinsi')->orderBy('nama')->get();
        $is_register = 1;

        return view('auth.register', compact('provinsi', 'is_register'));
    }

    public function do_registrasi(Request $request) {
        Auth::check();
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
            'alamat' => 'required|min:4',
            'password_confirmation' => 'required',
            'provinsi' => 'required',
            'kota_id' => 'required',
            'kode_pos' => 'required'
        ]);

        $nama = ucwords(Input::get('name'));
        $email = Input::get('email');
        $password = Input::get('password');
        $alamat = ucwords(Input::get('alamat'));
        $kota_id = Input::get('kota_id');
        $kode_pos = Input::get('kode_pos');
        $telp = Input::get('telp');
        $hp = Input::get('hp');

        $telp = str_replace("_", "", $telp);
        $hp = str_replace("_", "", $hp);

        DB::table('users')->insert(['name' => $nama, 'email' => $email, 'password' => bcrypt($password), 'alamat' => $alamat, 'kota_id' => $kota_id, 'kode_pos' => $kode_pos, 'telp' => $telp, 'hp' => $hp, 'hak_akses_id' => '0', 'is_aktif' => '1']);

        Cart::destroy();

        //kirim notifikasi ke admin
        $notifikasi = new Notifikasi;

        $notifikasi->kode = 'registrasi';
        $notifikasi->pesan = '<i class="fa fa-user"> Registrasi baru akun ' . $nama . '</i>';
        $notifikasi->hak_akses_id_target = 1;
        $notifikasi->link = "data_user";
        $notifikasi->is_aktif = 1;
        $notifikasi->save();

        return view('pages.home.registrasi_sukses');
    }

    public function login() {
        Auth::check();

        return view('auth.login');
    }

    public function do_login(Request $request) {
        $validator = Validator::make($request->all(), [
                    'email' => 'required|email|max:255',
                    'password' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('login')
                            ->withErrors($validator) // send back all errors to the login form
                            ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
        } else {
            $userdata = array(
                'email' => Input::get('email'),
                'password' => Input::get('password')
            );

            // attempt to do the login
            if (Auth::attempt($userdata)) {

                //cek apakah sudah mempunyai hak akses atau belum, jika sudah maka login sukses
                //jika belum maka login gagal
                //jika status = 0 login gagal
                $user = DB::table('users')->where('id', Auth::user()->id)->first();

                if ($user->hak_akses_id == 0 || empty($user->hak_akses_id)) {
                    Auth::logout();
                    return $this->sendFailedLoginResponse($request);
                } else {
                    if ($user->is_aktif != 1) {
                        Auth::logout();
                        return $this->sendFailedLoginResponse($request);
                    }

                    if ($user->hak_akses_id == 2) {
                        $cek_pelanggan = DB::table('tmst_pelanggan')->where('user_id', Auth::user()->id)->first();

                        //cek jika dia punya default diskon yang lebih besar dari nol maka destroy cart
                        $cek_kategori_pelanggan = DB::table('tmst_kategori_pelanggan')->where('id', $cek_pelanggan->kategori_id)->first();

                        $pelanggan_id = $cek_pelanggan->id;

                        if (!empty($cek_kategori_pelanggan)) {
                            if ((int) $cek_kategori_pelanggan->default_diskon > 0) {
                                //disini algo menyesuaikan harga, 
                                $old_cart = Cart::content(1);
                                Cart::instance('temp_cart');
                                Cart::destroy();

                                foreach ($old_cart as $row) {
                                    $produk_id = $row->id;
                                    $jumlah_beli = $row->qty;

                                    if (substr($produk_id, 0, 3) == 'DIS' || substr($produk_id, 0, 3) == 'CAS') {
                                        $produk_id = substr($produk_id, 3);
                                    }

                                    if (substr($produk_id, 0, 3) == 'PKT') {
                                        $data = array('id' => $row->id,
                                            'name' => $row->name,
                                            'qty' => $row->qty,
                                            'price' => $row->price,
                                            'options' => array('size' => 'large'));

                                        Cart::add($data);
                                    } else {
                                        $result = $this->cart_sales_order($produk_id, $jumlah_beli, $pelanggan_id);
                                    }
                                }

                                //masukkan nilai temp jadi default
                                $cart_temp = Cart::content(1);
                                Cart::instance('default');
                                Cart::destroy(); //kosongi dulu

                                foreach ($cart_temp as $row) {
                                    $data = array('id' => $row->id,
                                        'name' => $row->name,
                                        'qty' => $row->qty,
                                        'price' => $row->price,
                                        'options' => array('size' => 'large'));

                                    Cart::add($data);
                                }
                            }
                        }
                    }

                    //Cart::destroy();
                    //login sukses
                    $link = Input::get('link');

                    if (!empty($link)) {
                        return redirect('/' . $link);
                    } else {
                        return redirect('/');
                    }
                }
            } else {

                // validation not successful, send back to form 
                //return redirect('auth/login');
                return $this->sendFailedLoginResponse($request);
            }
        }
    }

    public function cart_sales_order($produk_id, $jumlah_beli, $pelanggan_id) {
        $kategori_pelanggan_id = 0;

        $pelanggan = Pelanggan::with('kategori_pelanggan')->where('id', $pelanggan_id)->first();
        if (!empty($pelanggan))
            $kategori_pelanggan_id = $pelanggan->kategori_id;

        $produk = Produk::with(['produk_galeri', 'promo_diskon', 'promo_cashback', 'promo_hadiah' => function($query) {
                                return $query->with('hadiah');
                            }, 'produk_harga'
                            => function ($query) use ($kategori_pelanggan_id) {
                                $query->where('kategori_pelanggan_id', $kategori_pelanggan_id);
                            }])
                        ->where('is_aktif', 1)->where('id', $produk_id)->first();

        //cek stok
        //algoritmanya : jumlah beli ditambah hasil hitung jumlah produk yang ada di cart yang punya produk_id yang sama
        $cek_jumlah = $jumlah_beli;
        $cart_content = Cart::content(1);
        foreach ($cart_content as $key => $value) {
            if (substr($value->id, 0, 3) == 'DIS' || substr($value->id, 0, 3) == 'CAS')
                $cek_produk_id = substr($value->id, 3);
            else
                $cek_produk_id = $value->id;

            if ($cek_produk_id == $produk_id)
                $cek_jumlah = $cek_jumlah + $value->qty;
        }

        if ($cek_jumlah > (int) $produk->stok - (int) $produk->stok_dipesan) {
            return 0; //stok tidak mencukupi maka return
        }

        $diskon = 0;
        $qty_beli_diskon = 1;
        $cashback = 0;
        $qty_beli_cashback = 1;
        $hadiah = "";
        $qty_beli_hadiah = 1;
        $qty_hadiah = 1;

        if (!empty($pelanggan->kategori_pelanggan)) {
            if ($pelanggan->kategori_pelanggan->default_diskon > $diskon) {
                $diskon = $pelanggan->kategori_pelanggan->default_diskon;
            }
        }

        if (!empty($produk->produk_harga)) {
            if ($produk->produk_harga->diskon > $diskon) {
                $diskon = $produk->produk_harga->diskon;
            }
        }

        if (!empty($produk->promo_diskon)) {
            if ($produk->promo_diskon->diskon > $diskon) {
                $diskon = $produk->promo_diskon->diskon;
                $qty_beli_diskon = $produk->promo_diskon->qty_beli;
            }
        }

        if (!empty($produk->promo_cashback) && empty($produk->produk_harga) && empty($pelanggan->kategori_pelanggan)) {
            $cashback = $produk->promo_cashback->cashback;
            $qty_beli_cashback = $produk->promo_cashback->qty_beli;
        }

        if (!empty($produk->promo_hadiah)) {
            $hadiah = $produk->promo_hadiah->hadiah->nama;
            $qty_beli_hadiah = $produk->promo_hadiah->qty_beli;
            $qty_hadiah = $produk->promo_hadiah->qty_hadiah;
        }

        //tambah cart dengan algoritma perhitungan selisih beli
        //cek dia punya diskon atau cashback (karena jika sudah mempunyai diskon maka gkboleh memiliki cashback)
        if ($diskon != 0) {
            $kode_promo = 'DIS';
            $harga_promo = $produk->harga_retail - (($produk->harga_retail * (int) $diskon / 100));
            $jumlah_beli_promo = $qty_beli_diskon;
        } else if ($cashback != 0) {
            $kode_promo = 'CAS';
            $harga_promo = $produk->harga_retail - (int) $cashback;
            $jumlah_beli_promo = $qty_beli_cashback;
        } else {
            $kode_promo = '';
            $harga_promo = $produk->harga_retail;
            $jumlah_beli_promo = 1;
        }

        $selisih_jumlah = $jumlah_beli % $jumlah_beli_promo;

        //khusus untuk diskon, id ditambahi string DIS di depan
        //khusus untuk cashback, id ditambahi string CAS di depan
        if ($selisih_jumlah == 0) { //jika yang dibeli merupakan kelipatan promo, maka simpan harga promo
            $data = array('id' => $kode_promo . $produk_id,
                'name' => $produk->nama,
                'qty' => $jumlah_beli,
                'price' => $harga_promo,
                'options' => array('size' => 'large'));

            Cart::add($data);
        } else { //jika tidak, maka simpan dua kali, harga diskon dan harga normal
            //simpan harga diskon
            $sisa_selisih_jumlah = $jumlah_beli - $selisih_jumlah;
            if ($sisa_selisih_jumlah != 0) {
                $data = array('id' => $kode_promo . $produk_id,
                    'name' => $produk->nama,
                    'qty' => $sisa_selisih_jumlah,
                    'price' => $harga_promo,
                    'options' => array('size' => 'large'));

                Cart::add($data);
            }

            //simpan harga normal
            $data = array('id' => $produk_id,
                'name' => $produk->nama,
                'qty' => $selisih_jumlah,
                'price' => $produk->harga_retail,
                'options' => array('size' => 'large'));

            Cart::add($data);
        }

        //hadiah tidak ditambahkan ke cart

        return 1;
    }

    protected function sendFailedLoginResponse(Request $request) {
        return redirect()->back()
                        ->withInput($request->only($this->loginUsername(), 'remember'))
                        ->withErrors([
                            $this->loginUsername() => $this->getFailedLoginMessage(),
        ]);
    }

    protected function getFailedLoginMessage() {
        return Lang::has('auth.failed') ? Lang::get('auth.failed') : 'These credentials do not match our records.';
    }

    public function loginUsername() {
        return property_exists($this, 'username') ? $this->username : 'email';
    }

    public function logout() {
        Auth::logout();
        Session::flush();
        return redirect('/');
    }

    public function get_kota($kode_provinsi) {
        $kota = DB::table('tref_kota')->where('provinsi_kode', $kode_provinsi)->orderBy('nama')->get();

        echo "<option value=''>-- Pilih Kabupaten/Kota --</option>";
        foreach ($kota as $val) {
            echo "<option value=" . $val->id . ">" . $val->nama . "</option>";
        }
    }

    public function get_kota2($kode_provinsi) {
        $kota = DB::table('tref_kota')->select('id', 'kode', 'nama')->where('provinsi_kode', $kode_provinsi)->orderBy('nama')->get();

        return Response::json($kota);
    }

    public function get_id_provinsi($kode_prov) {
        $prov = DB::table('tref_provinsi')->where('kode', $kode_prov)->first();

        echo $prov->id;
    }

    public function akun_Saya() {
        $general['title'] = "Akun Saya";
        $general['menu1'] = "Akun dan Pelanggan";
        $general['menu2'] = $general['title'];
        $general['kode_menu'] = 31;

        $user = DB::table('users')->where('id', Auth::user()->id)->first();
        $my_kota = DB::table('tref_kota')->select('tref_kota.nama', 'tref_kota.id as kota_id', 'tref_kota.provinsi_kode', 'tref_provinsi.id as provinsi_id')->join('tref_provinsi', 'tref_provinsi.kode', '=', 'tref_kota.provinsi_kode')->where('tref_kota.id', $user->kota_id)->first();
        $provinsi = DB::table('tref_provinsi')->get();

        return view('pages.user.akun_saya', compact('general', 'user', 'my_kota', 'provinsi'));
    }

    public function ubah_akun(Request $request) {
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'alamat' => 'required',
            'provinsi' => 'required',
            'kota' => 'required',
            'kode_pos' => 'required',
        ]);

        $id = Input::get('id');
        $name = ucwords(Input::get('name'));
        $email = Input::get('email');
        $alamat = ucwords(Input::get('alamat'));
        $kota_id = Input::get('kota');
        $kode_pos = Input::get('kode_pos');
        $telp = Input::get('telp');
        $hp = Input::get('hp');

        $telp = str_replace("_", "", $telp);
        $hp = str_replace("_", "", $hp);

        DB::table('users')->where('id', $id)->update(['name' => $name, 'email' => $email, 'alamat' => $alamat, 'kota_id' => $kota_id, 'kode_pos' => $kode_pos, 'telp' => $telp, 'hp' => $hp]);

        $request->session()->flash('message1', 'Akun berhasil diperbarui');
        return redirect('/akun_saya');
    }

    public function ubah_password_admin(Request $request) {
        $id = Input::get('id');
        $pass_lama = Input::get('password_lama');
        $pass_baru = Input::get('password');
        $pass_baru2 = Input::get('password_confirmation');

        $mypass = DB::table('users')->select('password')->where('id', $id)->first();

        $this->validate($request, [
            'password_lama' => 'required',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
        ]);

        $is_user = Hash::check($pass_lama, $mypass->password);

        if ($is_user == false) {
            $request->session()->flash('message_error', 'Password Lama Tidak Sesuai, Ubah Password Gagal');
            return redirect('/akun_saya');
        } else {
            $user = Auth::user();
            $user->password = Hash::make($pass_baru);
            $user->save();
            $request->session()->flash('message_password', 'Password Berhasil Diperbarui');
            return redirect('/akun_saya');
        }
    }

    public function data_user() {
        $general['title'] = "Data Akun";
        $general['menu1'] = "Akun dan Pelanggan";
        $general['menu2'] = $general['title'];
        $general['kode_menu'] = 32;

        $data_user = DB::select('SELECT * FROM(SELECT u.id, 
        (SELECT nama FROM tmst_kategori_pelanggan WHERE id=p.kategori_id) AS kategori, u.is_aktif, 
        u.name, u.email, u.tanggal_register, h.nama AS hak_akses 
        FROM users u, hak_akses h, tmst_pelanggan p WHERE u.hak_akses_id=h.id AND p.user_id=u.id
        UNION
        SELECT u.id, "-"  AS kategori, u.is_aktif, u.name, u.email, u.tanggal_register, h.nama AS hak_akses 
        FROM users u, hak_akses h WHERE u.hak_akses_id=h.id
        UNION
        SELECT id, "-" AS kategori, is_aktif, NAME, email, "-" AS tanggal_register, 0 AS hak_akses FROM users
        ) t
        GROUP BY id ORDER BY id DESC
                ');

        $hak_akses = DB::table('hak_akses')->get();

        $kategori_pelanggan = DB::table('tmst_kategori_pelanggan')->get();

        $provinsi = DB::table('tref_provinsi')->orderBy('nama')->get();

        return view('pages.user.data_user', compact('general', 'data_user', 'hak_akses', 'kategori_pelanggan', 'provinsi'));
    }

    public function ubah_status_user($user_id, Request $request) {
        $data = DB::table('users')->where('id', $user_id)->first();

        if ($data->is_aktif == 1) {
            $status = 0;
        } else {
            $status = 1;
        }

        DB::table('users')->where('id', $user_id)->update(['is_aktif' => $status]);

        $request->session()->flash('message', 'Status user berhasil diperbarui');
        return redirect('/data_user');
    }

    public function reset_password($id) {
        $chars = "0123456789abcdefghijklmnopqrstuvwxyz";
        $pass = "";
        for ($i = 0; $i < 6; $i++) {
            $pass .= $chars[mt_rand(0, strlen($chars) - 1)];
        }

        $password = Hash::make($pass);

        DB::table('users')->where('id', $id)->update(['password' => $password]);

        $user = DB::table('users')->where('id', $id)->first();

        //email password
        $data = ['nama' => $user->name, 'email' => $user->email, 'password' => $pass];

        Mail::send('emails.password_reset', $data, function ($mail) use ($user) {
            $mail->from('noreply@pos.com', "noreply");
            $mail->to($user->email, $user->name);
            $mail->subject('Reset Password Akun POS');
        });
    }

    public function get_user_data($id) {
        if ($id == '0') {
            $data_user = DB::select('SELECT * FROM(
            SELECT u.id, (SELECT nama FROM tmst_kategori_pelanggan WHERE id=p.kategori_id) AS kategori, u.is_aktif, u.name, u.email, u.tanggal_register, h.nama AS hak_akses FROM users u, hak_akses h, tmst_pelanggan p WHERE u.hak_akses_id=h.id AND p.user_id=u.id
            UNION
            SELECT u.id, "-"  AS kategori, u.is_aktif, u.name, u.email, u.tanggal_register, h.nama AS hak_akses FROM users u, hak_akses h WHERE u.hak_akses_id=h.id
                UNION
                SELECT id, "-" AS kategori, is_aktif, name, email, "-" AS tanggal_register, 0 AS hak_akses FROM users
                ) t
                GROUP BY id ORDER BY id DESC');

            $hak_akses = DB::table('hak_akses')->get();
        } else if ($id == '-1') {
            $data_user = DB::select('SELECT id, is_aktif, name, email, "-" as tanggal_register, 0 AS hak_akses FROM users WHERE users.hak_akses_id = 0 GROUP BY id ORDER BY id desc');

            $hak_akses = DB::table('hak_akses')->get();
        } else if ($id == '1') {
            $data_user = DB::select('SELECT id, name, is_aktif, email, tanggal_register, hak_akses_id AS hak_akses FROM users WHERE users.hak_akses_id = 1 GROUP BY id ORDER BY id desc');

            $hak_akses = DB::table('hak_akses')->get();
        } else if ($id == '2') {
            $data_user = DB::select('SELECT * FROM(
            SELECT u.id, u.name, u.email, u.tanggal_register, u.is_aktif, u.hak_akses_id AS hak_akses, kp.nama AS kategori, p.id AS pelanggan_id 
            FROM users u, tmst_pelanggan p, tmst_kategori_pelanggan kp WHERE u.hak_akses_id = 2 AND u.id=p.user_id AND kp.id=p.kategori_id 
            UNION
            SELECT u.id, u.name, u.email, u.tanggal_register, u.is_aktif, u.hak_akses_id AS hak_akses, "-" AS kategori, p.id AS pelanggan_id 
            FROM users u, tmst_pelanggan p WHERE u.hak_akses_id = 2 AND u.id=p.user_id
            ) t
            GROUP BY id ORDER BY id DESC');

            $hak_akses = DB::table('hak_akses')->get();
        } else if($id == '3') {
            $data_user = DB::select('SELECT id, name, is_aktif, email, tanggal_register, hak_akses_id AS hak_akses FROM users WHERE users.hak_akses_id = 3 GROUP BY id ORDER BY id desc');
        }

        $kategori_pelanggan = DB::table('tmst_kategori_pelanggan')->get();

        return view('pages.user.ajax_data_user', compact('data_user', 'hak_akses', 'id', 'kategori_pelanggan'));
    }

    public function generate_password($jumlah) {
        $chars = "0123456789abcdefghijklmnopqrstuvwxyz";
        $res = "";
        for ($i = 0; $i < $jumlah; $i++) {
            $res .= $chars[mt_rand(0, strlen($chars) - 1)];
        }
        return $res;
    }

    public function konfirmasi_user(Request $request) {
        $this->validate($request, [
            'hak_akses' => 'required',
        ]);

        $id = Input::get('id');
        $nama = ucwords(Input::get('nama'));
        $hak_akses = Input::get('hak_akses');
        $now = new DateTime();
        $user = DB::table('users')->where('id', $id)->first();
        
        if($hak_akses == 3){
            DB::table('users')->where('id', $id)->update(['hak_akses_id' => 3]);
            $request->session()->flash('message', 'User berhasil dikonfirmasi');
            return redirect('/data_user');
        }

        //jika dikonfirmasi sebagai tmst_pelanggan maka perlu insert data tmst_pelanggan dan user_tmst_pelanggan
        if ($hak_akses == 2) {
            $kategori_pelanggan = Input::get('pil_kategori');
            date_default_timezone_set("Asia/Jakarta");

            DB::table('tmst_pelanggan')->insert(['user_id' => $user->id, 'nama' => $user->name, 'email' => $user->email, 'alamat' => $user->alamat, 'kota_id' => $user->kota_id, 'kode_pos' => $user->kode_pos, 'telp1' => $user->telp, 'hp_sales' => $user->hp, 'kategori_id' => $kategori_pelanggan]);
        }


        $password = "";

        if (empty($user->password)) {
            $password = $this->generate_password(8); //generate password 8 digit

            DB::table('users')->where('id', $id)->update(['password' => bcrypt($password), 'hak_akses_id' => $hak_akses, 'tanggal_register' => $now]);
            $ha = DB::table('hak_akses')->where('id', $hak_akses)->first();
        } else {
            DB::table('users')->where('id', $id)->update(['hak_akses_id' => $hak_akses, 'tanggal_register' => $now]);
            $ha = DB::table('hak_akses')->where('id', $hak_akses)->first();
        }

        //email konfirmasi
        $data = ['nama' => $user->name, 'hak_akses' => $ha->nama, 'password' => $password];

        Mail::send('emails.konfirmasi_user', $data, function ($mail) use ($user) {
            $mail->from('noreplay@pos.com', "noreplay");
            $mail->to($user->email, $user->name);
            $mail->subject('Email Konfirmasi POS');
        });

        $request->session()->flash('message', 'User berhasil dikonfirmasi');
        return redirect('/data_user');
    }

    public function konfirmasi_user2(Request $request) {
        $this->validate($request, [
            'hidden_hak_akses' => 'required',
        ]);

        Auth::check();

        $id = Input::get('hidden_id');
        $nama = ucwords(Input::get('hidden_nama'));
        $hak_akses = Input::get('hidden_hak_akses');
        $now = new DateTime();
        $user = DB::table('users')->where('id', $id)->first();

        //jika dikonfirmasi sebagai tmst_pelanggan maka perlu insert data tmst_pelanggan dan user_tmst_pelanggan
        if ($hak_akses == 2) {

            $kategori_pelanggan = Input::get('hidden_kategori_pelanggan');
            date_default_timezone_set("Asia/Jakarta");

            DB::table('tmst_pelanggan')->insert(['user_id' => $user->id, 'nama' => $user->name, 'email' => $user->email, 'alamat' => $user->alamat, 'kota_id' => $user->kota_id, 'kode_pos' => $user->kode_pos, 'telp1' => $user->telp, 'hp_sales' => $user->hp, 'kategori_id' => $kategori_pelanggan]);
        }

        DB::table('users')->where('id', $id)->update(['hak_akses_id' => $hak_akses, 'tanggal_register' => $now]);
        $ha = DB::table('hak_akses')->where('id', $hak_akses)->first();

        //email konfirmasi
        $data = ['nama' => $user->name, 'hak_akses' => $ha->nama];

        Mail::send('emails.konfirmasi_user', $data, function ($mail) use ($user) {
            $mail->from('noreplay@pos.com', "noreplay");
            $mail->to($user->email, $user->name);
            $mail->subject('Email Konfirmasi POS');
        });

        $request->session()->flash('message', 'User berhasil dikonfirmasi');
        return redirect('/data_user');
    }

    public function detail_user($id) {
        $user = DB::table('users')->where('id', $id)->first();

        $general['title'] = $user->name;
        $general['menu1'] = "Akun dan Pelanggan";
        $general['menu2'] = "Data Akun";
        $general['menu3'] = $general['title'];
        $general['kode_menu'] = 32;

        $provinsi = DB::table('tref_provinsi')->get();
        $my_kota = DB::table('tref_kota')->select('tref_kota.nama', 'tref_kota.id as kota_id', 'tref_kota.provinsi_kode', 'tref_provinsi.id as provinsi_id')->join('tref_provinsi', 'tref_provinsi.kode', '=', 'tref_kota.provinsi_kode')->where('tref_kota.id', $user->kota_id)->first();

        return view('pages.user.detail_user', compact('general', 'user', 'provinsi', 'my_kota'));
    }

    public function ubah_user(Request $request) {
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'alamat' => 'required',
            'provinsi' => 'required',
            'kota' => 'required',
            'kode_pos' => 'required',
        ]);

        $id = Input::get('id');
        $name = Input::get('name');
        $email = Input::get('email');
        $alamat = Input::get('alamat');
        $kota_id = Input::get('kota');
        $kode_pos = Input::get('kode_pos');
        $telp = Input::get('telp');
        $hp = Input::get('hp');

        $telp = str_replace("_", "", $telp);
        $hp = str_replace("_", "", $hp);

        DB::table('users')->where('id', $id)->update(['name' => $name, 'email' => $email, 'alamat' => $alamat, 'kota_id' => $kota_id, 'kode_pos' => $kode_pos, 'telp' => $telp, 'hp' => $hp]);

        $user = DB::table('users')->where('id', $id)->first();

        //jika user merupakan tmst_pelanggan, maka ubah pula isi tabel tmst_pelanggan
        if ($user->id == '2') {
            DB::table('tmst_pelanggan')->where('user_id', $id)->update(['nama' => $name, 'email' => $email, 'alamat' => $alamat, 'kota_id' => $kota_id, 'kode_pos' => $kode_pos, 'telp1' => $telp, 'hp_sales' => $hp]);
        }

        $request->session()->flash('message', 'Akun berhasil diperbarui');
        return redirect('/detail_user/' . $id);
    }

    public function tambah_akun(Request $request) {
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'alamat' => 'required|min:4',
            'provinsi' => 'required',
            'kota_id' => 'required',
            'kode_pos' => 'required'
        ]);

        $nama = ucwords(Input::get('name'));
        $email = Input::get('email');
        $alamat = ucwords(Input::get('alamat'));
        $kota_id = Input::get('kota_id');
        $kode_pos = Input::get('kode_pos');
        $telp = Input::get('telp');
        $hp = Input::get('hp');

        $telp = str_replace("_", "", $telp);
        $hp = str_replace("_", "", $hp);

        DB::table('users')->insert(['name' => $nama, 'email' => $email, 'alamat' => $alamat, 'kota_id' => $kota_id, 'kode_pos' => $kode_pos, 'telp' => $telp, 'hp' => $hp, 'hak_akses_id' => '0', 'is_aktif' => '1']);

        //kirim notifikasi ke admin
        $notifikasi = new Notifikasi;

        $notifikasi->kode = 'registrasi';
        $notifikasi->pesan = '<i class="fa fa-user"> Registrasi baru akun ' . $nama . '</i>';
        $notifikasi->hak_akses_id_target = 1;
        $notifikasi->link = "data_user";
        $notifikasi->is_aktif = 1;
        $notifikasi->save();

        $request->session()->flash('message', 'Akun berhasil ditambah');
        return redirect('/data_user');
    }

}
