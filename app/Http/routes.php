<?php

require "Controllers/API/routes_api.php";

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

/* * ********************************* Route Auth ********************************************* */
Route::get('/registrasi', 'UserController@registrasi');
Route::post('/registrasi', 'UserController@do_registrasi');
Route::get('/login', 'UserController@login');
Route::post('/login', 'UserController@do_login');
Route::get('/logout', 'UserController@logout');
Route::get('/home', 'Home\HomeController@index');

Route::get('/lupa_password', 'UserController@lupa_password');
Route::post('/lupa_password', 'UserController@do_lupa_password');
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

Route::get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
Route::post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
Route::post('password/reset', 'Auth\PasswordController@reset');

Route::get('/', 'Home\HomeController@index');
Route::post('/get_kota/{kode_provinsi}', 'UserController@get_kota');
Route::post('/get_kota2/{kode_provinsi}', 'UserController@get_kota2');
Route::post('/get_kota_pelanggan/{kode_provinsi}', 'UserController@get_kota2');
Route::post('/get_id_provinsi/{kode_prov}', 'UserController@get_id_provinsi');
Route::post('/reset_password/{id}', 'UserController@reset_password');

Route::get('/cart', 'Home\HomeController@cart');
Route::get('/search', 'Home\HomeProdukController@search');
// Route::get('/checkout/review_pesanan', 'Home\HomeController@review_pesanan');
Route::post('/checkout_home', 'Home\HomeController@checkout_home');

/* * ********* CART  ********** */
Route::post('/home_tambah_cart', 'CartController@home_tambah_cart');
Route::post('/home_ubah_cart', 'CartController@home_ubah_cart');
Route::get('/home_hapus_cart/{rowid}', 'CartController@home_hapus_cart');
Route::post('/home_tambah_cart_paket', 'CartController@home_tambah_cart_paket');

/* * ********* CHECKOUT  ********** */
Route::get('/checkout/data_pengiriman', 'CheckoutController@data_pengiriman');
Route::post('/checkout/data_pembayaran', 'CheckoutController@data_pembayaran');
Route::post('/checkout/review_pesanan', 'CheckoutController@review_pesanan');
Route::post('/checkout/proses_pesanan', 'CheckoutController@proses_pesanan');
Route::get('/checkout/invoice_pelanggan/{no_nota?}', 'CheckoutController@invoice_pelanggan')->where('no_nota', '(.*)');
Route::post('/cek_voucher_home', 'CheckoutController@cek_voucher_home');

/* * ********* Halaman Navigasi Ecommerce  ********** */
Route::get('/semua_kategori', 'Home\HomeProdukController@semua_kategori');
Route::get('/kategori/{slug_nama}', 'Home\HomeProdukController@kategori');

Route::get('/promo_paket_produk', 'Home\HomePaketController@promo_paket_produk');
Route::get('/list_paket_produk', 'Home\HomePaketController@list_paket_produk');

Route::get('/produk/{slug_nama}', 'Home\HomeProdukController@produk');

Route::get('/promo', 'Home\HomeController@promo');
Route::get('/promo/{jenis}/{kategori}', 'Home\HomeController@detail_promo');
Route::get('/paket_produk/{slug_nama}', 'Home\HomePaketController@paket_produk');
Route::get('/cara_belanja', 'Home\HomeController@cara_belanja');
Route::get('/lacak_pesanan', 'Home\HomeController@lacak_pesanan');
Route::get('/service_center', 'Home\HomeController@service_center');
Route::get('/bantuan', 'Home\HomeController@bantuan');
Route::get('/tentang_kami', 'Home\HomeController@tentang_kami');
Route::get('/hubungi_kami', 'Home\HomeController@hubungi_kami');
Route::get('/faqs', 'Home\HomeController@faqs');
Route::get('/ketentuan_pengguna', 'Home\HomeController@ketentuan_pengguna');
Route::get('/ketentuan_retur_barang', 'Home\HomeController@ketentuan_retur_barang');
Route::get('/sistem_pembayaran', 'Home\HomeController@sistem_pembayaran');
Route::get('/profil_perusahaan', 'Home\HomeController@profil_perusahaan');
Route::get('/karir', 'Home\HomeController@karir');
Route::get('/lokasi_toko', 'Home\HomeController@lokasi_toko');
Route::get('/aplikasi_mobile', 'Home\HomeController@aplikasi_mobile');


Route::post('/get_log_chat', 'ChattingController@get_log_chat');
Route::post('/get_notif', 'ChattingController@get_notif');
Route::get('/show_chat/{id}', 'ChattingController@show_chat');
Route::post('/save_log_chat', 'ChattingController@save_log_chat');
Route::post('/delete_pelanggan_notif/{id}', 'ChattingController@delete_pelanggan_notif');
Route::post('/delete_notif', 'ChattingController@delete_notif');
Route::post('/save_notif', 'ChattingController@save_notif');

Route::get('/slider-promo/{url}', 'AdminEcommerceController@slider_promo');
Route::get('/produk-pilihan/{url}', 'AdminEcommerceController@produk_pilihan');

Route::get('/paket-pilihan/{url}', 'Home\HomePaketController@paket_pilihan');

Route::group(['middleware' => 'auth3'], function () { //hanya untuk pelanggan
    /*     * * AKUN PELANGGAN ** */
    Route::get('/akun_pelanggan', 'Pelanggan\PelangganController@akun_pelanggan');
    Route::post('/ubah_akun_pelanggan', 'Pelanggan\PelangganController@ubah_akun_pelanggan');
    Route::get('/password_pelanggan', 'Pelanggan\PelangganController@password_pelanggan');
    Route::post('/ubah_password_pelanggan', 'Pelanggan\PelangganController@ubah_password_pelanggan');
    Route::get('/pesanan', 'Pelanggan\PelangganController@pesanan');
    Route::get('/detail_pesanan/{no_nota?}', 'Pelanggan\PelangganController@detail_pesanan')->where('no_nota', '(.*)');
    Route::get('/konfirmasi_pembayaran/{id}', 'Pelanggan\PelangganController@konfirmasi_pembayaran');
    Route::post('/do_konfirmasi_pembayaran', 'Pelanggan\PelangganController@do_konfirmasi_pembayaran');
});

Route::group(['middleware' => 'auth2'], function () {  //hanya untuk admin panel

    /*     * ********* BERANDA ADMIN  ********** */
    Route::get('/beranda_admin', 'AdminController@beranda_admin');
    Route::get('/perusahaan', 'AdminController@perusahaan');
    Route::post('/ubah_perusahaan', 'AdminController@ubah_perusahaan');
    Route::post('/tambah_cabang', 'AdminController@tambah_cabang');
    Route::post('/ubah_cabang', 'AdminController@ubah_cabang');
    Route::post('/hapus_cabang', 'AdminController@hapus_cabang');
    Route::get('/pengaturan_umum', 'AdminController@pengaturan_umum');
    Route::post('/tambah_bank', 'AdminController@tambah_bank');
    Route::get('/ubah_status_bank/{id}', 'AdminController@ubah_status_bank');
    Route::post('/ubah_bank', 'AdminController@ubah_bank');
    Route::post('/tambah_metode_pembayaran', 'AdminController@tambah_metode_pembayaran');
    Route::post('/ubah_metode_pembayaran', 'AdminController@ubah_metode_pembayaran');
    Route::get('/ubah_status_metode_pembayaran/{id}', 'AdminController@ubah_status_metode_pembayaran');
    Route::post('/ubah_nomor_invoice', 'AdminController@ubah_nomor_invoice');
    Route::get('/ubah_status_metode_pengiriman/{id}', 'AdminController@ubah_status_metode_pengiriman');
    Route::post('/tambah_metode_pengiriman', 'AdminController@tambah_metode_pengiriman');
    Route::post('/ubah_metode_pengiriman', 'AdminController@ubah_metode_pengiriman');
    Route::post('/ubah_bank', 'AdminController@ubah_bank');

    //ChattingController
    Route::get('/chat', 'ChattingController@chat');
    Route::post('/get_chat_admin', 'ChattingController@get_chat_admin');


    /*     * ********* PENGATURAN ********** */

    Route::get('/ecommerce', 'PengaturanController@ecommerce');
    Route::post('/ubah_banner_promo/{id}', 'PengaturanController@ubah_banner_promo');
    Route::get('/ubah_status_halaman_promo/{id}', 'PengaturanController@ubah_status_halaman_promo');


    Route::post('/set_order_halaman_promo', 'PengaturanController@set_order_halaman_promo');
    Route::post('/tambah_banner_promo', 'PengaturanController@tambah_banner_promo');
    Route::post('/hapus_halaman_promo', 'PengaturanController@hapus_halaman_promo');
    Route::get('/tambah_root_coa', 'PengaturanController@tambah_root_coa');
    Route::post('/do_tambah_root_coa', 'PengaturanController@do_tambah_root_coa');
    Route::get('/get_row_child_coa', 'PengaturanController@get_row_child_coa');
    Route::get('/tambah_child_coa', 'PengaturanController@tambah_child_coa');
    Route::post('/tambah_coa', 'PengaturanController@tambah_coa');
    Route::post('/tambah_produk_tampil_di_beranda', 'PengaturanController@tambah_produk_tampil_di_beranda');
    Route::post('/hapus_produk_tampil_di_beranda', 'PengaturanController@hapus_produk_tampil_di_beranda');


    Route::post('/tambah_cara_belanja', 'PengaturanController@tambah_cara_belanja');


    /*     * ********* AKUN DAN PELANGGAN  ********** */
    //UserController
    Route::get('/akun_saya', 'UserController@akun_saya');
    Route::post('/ubah_akun', 'UserController@ubah_akun');
    Route::post('/ubah_password_admin', 'UserController@ubah_password_admin');
    Route::get('/data_user', 'UserController@data_user');
    Route::get('/ubah_status_user/{id}', 'UserController@ubah_status_user');
    Route::post('/get_user_data/{id}', 'UserController@get_user_data');
    Route::post('/konfirmasi_user', 'UserController@konfirmasi_user');
    Route::post('/konfirmasi_user2', 'UserController@konfirmasi_user2');
    Route::get('/detail_user/{id}', 'UserController@detail_user');
    Route::post('/ubah_user', 'UserController@ubah_user');
    Route::post('/tambah_akun', 'UserController@tambah_akun');


    /*     * ********* PELANGGAN  ********** */
    //KategoriPelangganController
    Route::get('/kategori_pelanggan', 'Pelanggan\KategoriPelangganController@kategori_pelanggan');
    Route::get('/tambah_kategori_pelanggan', 'Pelanggan\KategoriPelangganController@tambah_kategori_pelanggan');
    Route::post('/do_tambah_kategori_pelanggan', 'Pelanggan\KategoriPelangganController@do_tambah_kategori_pelanggan');
    Route::get('/detail_kategori_pelanggan/{id}', 'Pelanggan\KategoriPelangganController@detail_kategori_pelanggan');
    Route::post('/ubah_kategori_pelanggan', 'Pelanggan\KategoriPelangganController@ubah_kategori_pelanggan');
    Route::post('/get_kategori_produk_harga_promo', 'Pelanggan\KategoriPelangganController@get_kategori_produk_harga_promo');
    Route::post('/tambah_produk_harga', 'Pelanggan\KategoriPelangganController@tambah_produk_harga');
    Route::post('/ubah_produk_harga', 'Pelanggan\KategoriPelangganController@ubah_produk_harga');
    Route::post('/hapus_produk_harga', 'Pelanggan\KategoriPelangganController@hapus_produk_harga');
    Route::post('/get_produk_harga', 'Pelanggan\KategoriPelangganController@get_produk_harga');

    //PelangganController
    Route::get('/pelanggan', 'Pelanggan\PelangganController@pelanggan');
    Route::post('/tambah_kategori_untuk_pelanggan', 'Pelanggan\PelangganController@tambah_kategori_untuk_pelanggan');
    Route::post('/ubah_kategori_untuk_pelanggan', 'Pelanggan\PelangganController@ubah_kategori_untuk_pelanggan');
    Route::get('/transaksi_pelanggan/{id}', 'Pelanggan\PelangganController@transaksi_pelanggan');
    Route::get('/transaksi_pelanggan_detail/{no_nota?}', 'Pelanggan\PelangganController@transaksi_pelanggan_detail')->where('no_nota', '(.*)');

    //HakAksesController
    Route::get('/hak_akses', 'HakAksesController@hak_akses');
    Route::get('/tambah_hak_akses', 'HakAksesController@tambah_hak_akses');
    Route::post('/do_tambah_hak_akses', 'HakAksesController@do_tambah_hak_akses');
    Route::post('/ubah_hak_akses', 'HakAksesController@ubah_hak_akses');
    Route::post('/hapus_hak_akses', 'HakAksesController@hapus_hak_akses');

    /*     * ********* PRODUK  ********** */
    //SupplierController
    Route::get('/supplier', 'Produk\SupplierController@supplier');
    Route::get('/tambah_supplier', 'Produk\SupplierController@tambah_supplier');
    Route::post('/do_tambah_supplier', 'Produk\SupplierController@do_tambah_supplier');
    Route::get('/ubah_status_supplier/{id}', 'Produk\SupplierController@ubah_status_supplier');
    Route::get('/detail_supplier/{id}', 'Produk\SupplierController@detail_supplier');
    Route::post('/ubah_supplier', 'Produk\SupplierController@ubah_supplier');
    Route::post('/tambah_rekening_supplier', 'Produk\SupplierController@tambah_rekening_supplier');
    Route::post('/ubah_rekening_supplier', 'Produk\SupplierController@ubah_rekening_supplier');
    Route::post('/hapus_rekening_supplier', 'Produk\SupplierController@hapus_rekening_supplier');

    //KategoriProdukController
    Route::get('/kategori_produk', 'Produk\KategoriProdukController@kategori_produk');
    Route::get('/ubah_status_kategori_produk/{id}', 'Produk\KategoriProdukController@ubah_status_kategori_produk');
    Route::get('/tambah_kategori_produk', 'Produk\KategoriProdukController@tambah_kategori_produk');
    Route::post('/do_tambah_kategori_produk', 'Produk\KategoriProdukController@do_tambah_kategori_produk');
    Route::post('/ubah_kategori_produk', 'Produk\KategoriProdukController@ubah_kategori_produk');
    Route::post('/ubah_icon_kategori_produk', 'Produk\KategoriProdukController@ubah_icon_kategori_produk');

    //GudangController
    Route::get('/gudang', 'Produk\GudangController@gudang');
    Route::get('/tambah_gudang', 'Produk\GudangController@tambah_gudang');
    Route::post('/do_tambah_gudang', 'Produk\GudangController@do_tambah_gudang');
    Route::get('/ubah_status_gudang/{id}', 'Produk\GudangController@ubah_status_gudang');
    Route::post('/hapus_gudang', 'Produk\GudangController@hapus_gudang');
    Route::get('/detail_gudang/{id}', 'Produk\GudangController@detail_gudang');
    Route::post('/ubah_gudang', 'Produk\GudangController@ubah_gudang');
    Route::post('/ubah_nomor_mutasi_gudang', 'Produk\GudangController@ubah_nomor_mutasi_gudang');

    //ProdukController
    Route::get('/produk', 'Produk\ProdukController@produk');
    Route::get('/ubah_status_produk/{id}', 'Produk\ProdukController@ubah_status_produk');
    Route::get('/tambah_produk', 'Produk\ProdukController@tambah_produk');
    Route::post('/do_tambah_produk', 'Produk\ProdukController@do_tambah_produk');
    Route::post('/do_tambah_produk_by_ajax', 'Produk\ProdukController@do_tambah_produk_by_ajax');
    Route::get('/detail_produk/{id}', 'Produk\ProdukController@detail_produk');
    Route::post('/ubah_produk', 'Produk\ProdukController@ubah_produk');
    Route::post('/tambah_serial_number', 'Produk\ProdukController@tambah_serial_number');
    Route::post('/hapus_serial_number', 'Produk\ProdukController@hapus_serial_number');
    Route::post('/tambah_promo_diskon', 'Produk\ProdukController@tambah_promo_diskon');
    Route::post('/ubah_promo_diskon', 'Produk\ProdukController@ubah_promo_diskon');
    Route::post('/hapus_promo_diskon', 'Produk\ProdukController@hapus_promo_diskon');
    Route::post('/tambah_promo_cashback', 'Produk\ProdukController@tambah_promo_cashback');
    Route::post('/ubah_promo_cashback', 'Produk\ProdukController@ubah_promo_cashback');
    Route::post('/hapus_promo_cashback', 'Produk\ProdukController@hapus_promo_cashback');
    Route::post('/tambah_promo_hadiah', 'Produk\ProdukController@tambah_promo_hadiah');
    Route::post('/ubah_promo_hadiah', 'Produk\ProdukController@ubah_promo_hadiah');
    Route::post('/hapus_promo_hadiah', 'Produk\ProdukController@hapus_promo_hadiah');
    Route::post('/tambah_produk_supplier', 'Produk\ProdukController@tambah_produk_supplier');
    Route::post('/hapus_produk_supplier', 'Produk\ProdukController@hapus_produk_supplier');
    Route::post('/tambah_produk_galeri/{produk_id}', 'Produk\ProdukController@tambah_produk_galeri');
    Route::Post('/ubah_produk_galeri', 'Produk\ProdukController@ubah_produk_galeri');
    Route::post('/hapus_produk_galeri', 'Produk\ProdukController@hapus_produk_galeri');
    Route::post('/get_produk', 'Produk\ProdukController@get_produk');
    Route::post('/get_kolom_spesifikasi', 'Produk\ProdukController@get_kolom_spesifikasi');

    //HadiahController
    Route::get('/hadiah', 'Produk\HadiahController@hadiah');
    Route::get('/tambah_hadiah', 'Produk\HadiahController@tambah_hadiah');
    Route::post('/do_tambah_hadiah', 'Produk\HadiahController@do_tambah_hadiah');
    Route::get('/detail_hadiah/{id}', 'Produk\HadiahController@detail_hadiah');
    Route::post('/ubah_hadiah', 'Produk\HadiahController@ubah_hadiah');
    Route::post('/tambah_hadiah_supplier', 'Produk\HadiahController@tambah_hadiah_supplier');
    Route::post('/ubah_gambar_hadiah/{id}', 'Produk\HadiahController@ubah_gambar_hadiah');
    Route::post('/do_tambah_hadiah_by_ajax', 'Produk\HadiahController@do_tambah_hadiah_by_ajax');

    //PaketProdukController
    Route::get('/paket_produk', 'Produk\PaketProdukController@paket_produk');
    Route::get('/tambah_paket_produk', 'Produk\PaketProdukController@tambah_paket_produk');
    Route::post('/list_add_produk/{id}', 'Produk\PaketProdukController@list_add_produk');
    Route::post('/do_tambah_paket_produk', 'Produk\PaketProdukController@do_tambah_paket_produk');
    Route::get('/ubah_status_paket/{id}', 'Produk\PaketProdukController@ubah_status_paket');
    Route::get('/detail_paket_produk/{id}', 'Produk\PaketProdukController@detail_paket_produk');
    Route::post('/ubah_paket_produk', 'Produk\PaketProdukController@ubah_paket_produk');
    Route::post('/hapus_paket_produk', 'Produk\PaketProdukController@hapus_paket_produk');

    /*     * ***************** STOK ****************** */
    Route::get('/penyesuaian_stok', 'Stok\PenyesuaianStokController@penyesuaian_stok');
    Route::get('/tambah_penyesuaian_stok', 'Stok\PenyesuaianStokController@tambah_penyesuaian_stok');
    Route::post('/penyesuaian_stok_get_produk', 'Stok\PenyesuaianStokController@penyesuaian_stok_get_produk');
    Route::post('/penyesuaian_stok_get_serial_number', 'Stok\PenyesuaianStokController@penyesuaian_stok_get_serial_number');
    Route::post('/penyesuaian_stok_cek_serial_number', 'Stok\PenyesuaianStokController@penyesuaian_stok_cek_serial_number');
    Route::post('/do_tambah_penyesuaian_stok', 'Stok\PenyesuaianStokController@do_tambah_penyesuaian_stok');
    Route::get('/detail_penyesuaian_stok/{id}', 'Stok\PenyesuaianStokController@detail_penyesuaian_stok');

    Route::get('/mutasi_stok', 'StokController@mutasi_stok');

    Route::get('/penyesuaian_stok_gudang/{id}', 'StokController@penyesuaian_stok_gudang');
    Route::post('/do_penyesuaian_stok', 'StokController@do_penyesuaian_stok');
    Route::post('/receive_penyesuaian_stok', 'StokController@receive_penyesuaian_stok');
    Route::post('/get_gudang_tujuan', 'StokController@get_gudang_tujuan');
    Route::post('/do_mutasi_stok', 'StokController@do_mutasi_stok');
    Route::post('/receive_mutasi_stok', 'StokController@receive_mutasi_stok');
    Route::get('/stok_opname', 'StokController@stok_opname');

    Route::post('/get_produk_by_gudang_id/{id}', 'StokController@get_produk_by_gudang_id');
    Route::post('/get_id_stok_opname', 'StokController@get_id_stok_opname');
    Route::post('/get_detail_stok_opname', 'StokController@get_detail_stok_opname');
    // Route::get('/detail_stok_opname/{id}', 'StokController@detail_stok_opname');

    Route::get('/tambah_mutasi_stok', 'StokController@tambah_mutasi_stok');
    Route::post('/mutasi_stok_get_produk', 'StokController@mutasi_stok_get_produk');
    Route::post('/mutasi_stok_get_serial_number', 'StokController@mutasi_stok_get_serial_number');
    Route::post('/do_tambah_mutasi_stok', 'StokController@do_tambah_mutasi_stok');
    Route::get('/no_mutasi_stok/{id}', 'StokController@no_mutasi_stok');
    Route::get('/detail_mutasi_stok/{id}', 'StokController@detail_mutasi_stok');
    Route::get('/sj_mutasi_stok/{id}', 'StokController@sj_mutasi_stok');
    Route::get('/sj_mutasi_stok_print/{id}', 'StokController@sj_mutasi_stok_print');
    Route::get('/sj_mutasi_stok_pdf/{id}', 'StokController@sj_mutasi_stok_pdf');
    Route::post('/get_format_no_mutasi', 'StokController@get_format_no_mutasi');

    /* StokOpnameController */
    Route::get('/stok_opname', 'Stok\StokOpnameController@stok_opname');
    Route::get('/tambah_stok_opname', 'Stok\StokOpnameController@tambah_stok_opname');
    Route::post('/stok_opname_get_produk', 'Stok\StokOpnameController@stok_opname_get_produk');
    Route::post('/stok_opname_get_serial_number', 'Stok\StokOpnameController@stok_opname_get_serial_number');
    Route::post('/do_tambah_stok_opname', 'Stok\StokOpnameController@do_tambah_stok_opname');
    Route::get('/detail_stok_opname/{id}', 'Stok\StokOpnameController@detail_stok_opname');

    /*     * ***************** LAPORAN ****************** */
    /* LaporanStokController */
    Route::get('/laporan_stok', 'Laporan\LaporanStokController@laporan_stok');
    Route::post('/do_tambah_laporan_stok', 'Laporan\LaporanStokController@do_tambah_laporan_stok');
    Route::get('/laporan_stok_cetak/{id}', 'Laporan\LaporanStokController@laporan_stok_cetak');
    Route::get('/laporan_stok_pdf/{id}', 'Laporan\LaporanStokController@laporan_stok_pdf');

    /* KartuStokController */
    Route::get('/kartu_stok', 'Laporan\KartuStokController@kartu_stok');
    Route::post('/kartu_stok_produk_gudang', 'Laporan\KartuStokController@kartu_stok_produk_gudang');
    Route::post('/do_tambah_kartu_stok', 'Laporan\KartuStokController@do_tambah_kartu_stok');
    Route::get('/kartu_stok_cetak/{id}', 'Laporan\KartuStokController@kartu_stok_cetak');
    Route::get('/kartu_stok_pdf/{id}', 'Laporan\KartuStokController@kartu_stok_pdf');


    // Route::get('/tambah_laporan_stok', 'LaporanController@tambah_laporan_stok');


    Route::get('/laporan_penjualan', 'LaporanController@laporan_penjualan');
    Route::get('/detail_laporan_stok/{id}', 'LaporanController@detail_laporan_stok');
    // Route::get('/tambah_kartu_stok', 'LaporanController@tambah_kartu_stok');


    Route::get('/detail_kartu_stok/{id}', 'LaporanController@detail_kartu_stok');
    Route::post('/buat_laporan_penjualan', 'LaporanController@buat_laporan_penjualan');

    //laporan_pembelian
    Route::get('/laporan_pembelian', 'LaporanController@laporan_pembelian');
    Route::post('/buat_laporan_pembelian', 'LaporanController@buat_laporan_pembelian');
    Route::get('/laporan_surat_jalan_masuk', 'LaporanController@laporan_surat_jalan_masuk');
    Route::post('/buat_laporan_surat_jalan_masuk', 'LaporanController@buat_laporan_surat_jalan_masuk');
    Route::get('/laporan_surat_jalan_keluar', 'LaporanController@laporan_surat_jalan_keluar');
    Route::post('/buat_laporan_surat_jalan_keluar', 'LaporanController@buat_laporan_surat_jalan_keluar');
    Route::get('/laporan_surat_jalan_masuk_cetak/{tanggal_awal}/{tanggal_akhir}', 'LaporanController@laporan_surat_jalan_masuk_cetak');
    Route::get('/laporan_surat_jalan_masuk_pdf/{tanggal_awal}/{tanggal_akhir}', 'LaporanController@laporan_surat_jalan_masuk_pdf');
    Route::get('/laporan_surat_jalan_keluar_cetak/{tanggal_awal}/{tanggal_akhir}', 'LaporanController@laporan_surat_jalan_keluar_cetak');
    Route::get('/laporan_surat_jalan_keluar_pdf/{tanggal_awal}/{tanggal_akhir}', 'LaporanController@laporan_surat_jalan_keluar_pdf');
    Route::get('/laporan_servis', 'LaporanController@laporan_servis');
    Route::post('/buat_laporan_servis', 'LaporanController@buat_laporan_servis');
    Route::get('/laporan_pembayaran_supplier', 'LaporanController@laporan_pembayaran_supplier');
    Route::post('/buat_laporan_pembayaran_supplier', 'LaporanController@buat_laporan_pembayaran_supplier');
    Route::get('/laporan_pembayaran_pelanggan', 'LaporanController@laporan_pembayaran_pelanggan');
    route::post('/buat_laporan_pembayaran_pelanggan', 'LaporanController@buat_laporan_pembayaran_pelanggan');


    /*     * ***************** PENGATURAN ****************** */

    // Route::get('/tambah_master_coa', 'PengaturanController@tambah_master_coa');
    // Route::post('/do_tambah_master_coa', 'PengaturanController@do_tambah_master_coa');
    // Route::post('/get_parent', 'PengaturanController@get_parent');
    // Route::post('/get_count_child', 'PengaturanController@get_count_child');
    // Route::post('/urutan_master_coa', 'PengaturanController@urutan_master_coa');
    // Route::post('/ubah_master_coa', 'PengaturanController@ubah_master_coa');
    Route::get('/group_account', 'PengaturanController@group_account');

    /* MasterCoaController */
    Route::get('/master_coa', 'Pengaturan\MasterCoaController@master_coa');
    Route::post('/get_data_akun_baru', 'Pengaturan\MasterCoaController@get_data_akun_baru');
    Route::post('/tambah_master_coa', 'Pengaturan\MasterCoaController@tambah_master_coa');
    Route::post('/ubah_master_coa', 'Pengaturan\MasterCoaController@ubah_master_coa');
    Route::post('/hapus_master_coa', 'Pengaturan\MasterCoaController@hapus_master_coa');

    /* PengaturanJurnalController */
    Route::get('/pengaturan_jurnal', 'Pengaturan\PengaturanJurnalController@pengaturan_jurnal');
    Route::post('/tambah_pengaturan_jurnal', 'Pengaturan\PengaturanJurnalController@tambah_pengaturan_jurnal');
    Route::post('/ubah_pengaturan_jurnal', 'Pengaturan\PengaturanJurnalController@ubah_pengaturan_jurnal');
    Route::post('/hapus_pengaturan_jurnal', 'Pengaturan\PengaturanJurnalController@hapus_pengaturan_jurnal');

    /*     * ***************** ACCOUNTING ****************** */
    Route::get('/jurnal_umum', 'Accounting\JurnalController@jurnal_umum');
    Route::get('/tambah_jurnal_umum', 'Accounting\JurnalController@tambah_jurnal_umum');
    Route::post('/do_tambah_jurnal_umum', 'Accounting\JurnalController@do_tambah_jurnal_umum');
    Route::get('/detail_jurnal_umum/{id}', 'Accounting\JurnalController@detail_jurnal_umum');
    Route::post('/ubah_jurnal_umum_detail', 'Accounting\JurnalController@ubah_jurnal_umum_detail');
    Route::get('/laporan_jurnal_umum', 'Accounting\JurnalController@laporan_jurnal_umum');
    Route::post('/ajax_laporan_jurnal_umum', 'Accounting\JurnalController@ajax_laporan_jurnal_umum');
    Route::get('/laporan_jurnal_umum_print/{bulan}/{tahun}', 'Accounting\JurnalController@laporan_jurnal_umum_print');
    Route::get('/laporan_jurnal_umum_pdf/{bulan}/{tahun}', 'Accounting\JurnalController@laporan_jurnal_umum_pdf');
    Route::get('/laporan_buku_besar', 'Accounting\JurnalController@laporan_buku_besar');
    Route::post('/ajax_laporan_buku_besar', 'Accounting\JurnalController@ajax_laporan_buku_besar');
    Route::get('/laporan_buku_besar_print/{bulan}/{tahun}/{akun}', 'Accounting\JurnalController@laporan_buku_besar_print');
    Route::get('/laporan_buku_besar_pdf/{bulan}/{tahun}/{akun}', 'Accounting\JurnalController@laporan_buku_besar_pdf');

    /* LAIN LAIN */
    Route::post('/nonaktif_notifikasi', 'AdminController@nonaktif_notifikasi');

    /*     * ********* TRANSAKSI ********** */
    //PurchaseOrderController
    Route::get('/purchase_order', 'Transaksi\PurchaseOrderController@purchase_order');
    Route::get('/tambah_purchase_order', 'Transaksi\PurchaseOrderController@tambah_purchase_order');
    Route::post('/get_po_produk', 'Transaksi\PurchaseOrderController@get_po_produk');
    Route::post('/po_proses', 'Transaksi\PurchaseOrderController@po_proses');


    Route::get('/nota_purchase_order/{id}', 'Transaksi\PurchaseOrderController@nota_purchase_order');
    Route::get('/nota_purchase_order_print/{id}', 'Transaksi\PurchaseOrderController@nota_purchase_order_print');
    Route::get('/nota_purchase_order_pdf/{id}', 'Transaksi\PurchaseOrderController@nota_purchase_order_pdf');
    Route::get('/detail_purchase_order/{id}', 'Transaksi\PurchaseOrderController@detail_purchase_order');
    Route::post('/ubah_format_no_invoice_po', 'Transaksi\PurchaseOrderController@ubah_format_no_invoice_po');
    Route::post('/pengaturan_po', 'Transaksi\PurchaseOrderController@pengaturan_po');

    Route::post('/po_tambah_cart', 'CartController@po_tambah_cart');
    Route::post('/po_update_cart', 'CartController@po_update_cart');
    Route::post('/po_hapus_cart', 'CartController@po_hapus_cart');

    /* SuratJalanMasukController */
    Route::get('/surat_jalan_masuk', 'Transaksi\SuratJalanMasukController@surat_jalan_masuk');
    Route::get('/tambah_surat_jalan_masuk', 'Transaksi\SuratJalanMasukController@tambah_surat_jalan_masuk');
    Route::post('/sjm_get_produk', 'Transaksi\SuratJalanMasukController@sjm_get_produk');
    Route::post('/sjm_get_po_detail', 'Transaksi\SuratJalanMasukController@sjm_get_po_detail');
    Route::post('/sjm_get_arr_po_detail', 'Transaksi\SuratJalanMasukController@sjm_get_arr_po_detail');
    Route::post('/sjm_get_arr_serial_number', 'Transaksi\SuratJalanMasukController@sjm_get_arr_serial_number');
    Route::post('/surat_jalan_masuk_proses', 'Transaksi\SuratJalanMasukController@surat_jalan_masuk_proses');
    Route::get('/nota_surat_jalan_masuk/{id}', 'Transaksi\SuratJalanMasukController@nota_surat_jalan_masuk');
    Route::get('/nota_surat_jalan_masuk_print/{id}', 'Transaksi\SuratJalanMasukController@nota_surat_jalan_masuk_print');
    Route::get('/nota_surat_jalan_masuk_pdf/{id}', 'Transaksi\SuratJalanMasukController@nota_surat_jalan_masuk_pdf');
    Route::post('/hapus_surat_jalan_masuk', 'Transaksi\SuratJalanMasukController@hapus_surat_jalan_masuk');

    /* NotaBeliController */
    Route::get('/nota_beli', 'Transaksi\NotaBeliController@nota_beli');
    Route::get('/tambah_nota_beli', 'Transaksi\NotaBeliController@tambah_nota_beli');
    Route::post('/nb_get_data', 'Transaksi\NotaBeliController@nb_get_data');
    Route::post('/do_tambah_nota_beli', 'Transaksi\NotaBeliController@do_tambah_nota_beli');
    Route::get('/invoice_nota_beli/{id}', 'Transaksi\NotaBeliController@invoice_nota_beli');
    Route::get('/invoice_nota_beli_print/{id}', 'Transaksi\NotaBeliController@invoice_nota_beli_print');
    Route::get('/invoice_nota_beli_pdf/{id}', 'Transaksi\NotaBeliController@invoice_nota_beli_pdf');
    Route::get('/pembayaran_nota_beli/{id}', 'Transaksi\NotaBeliController@pembayaran_nota_beli');
    Route::post('/hapus_nota_beli', 'Transaksi\NotaBeliController@hapus_nota_beli');

    //ReturBeliController
    Route::get('/retur_beli', 'Transaksi\ReturBeliController@retur_beli');
    route::get('/tambah_retur_beli', 'Transaksi\ReturBeliController@tambah_retur_beli');
    Route::post('/get_nota_supplier', 'Transaksi\ReturBeliController@get_nota_supplier');
    Route::post('/get_po_detail', 'Transaksi\ReturBeliController@get_po_detail');
    Route::post('/retur_beli_tambah_cart', 'Transaksi\ReturBeliController@retur_beli_tambah_cart');
    Route::post('/retur_beli_checkout', 'Transaksi\ReturBeliController@retur_beli_checkout');
    Route::post('/ubah_format_no_invoice_rb', 'Transaksi\ReturBeliController@ubah_format_no_invoice_rb');
    Route::get('/nota_retur_beli/{id}', 'Transaksi\ReturBeliController@nota_retur_beli');
    Route::get('/detail_retur_beli/{id}', 'Transaksi\ReturBeliController@detail_retur_beli');
    Route::get('/nota_retur_beli_print/{id}', 'Transaksi\ReturBeliController@nota_retur_beli_print');
    Route::get('/nota_retur_beli_pdf/{id}', 'Transaksi\ReturBeliController@nota_retur_beli_pdf');

    //BayarPembelianController
    Route::get('/pembayaran_supplier', 'Transaksi\BayarPembelianController@pembayaran_supplier');
    Route::get('/tambah_pembayaran_supplier', 'Transaksi\BayarPembelianController@tambah_pembayaran_supplier');
    Route::post('/get_rekening_supplier', 'Transaksi\BayarPembelianController@get_rekening_supplier');
    Route::post('/do_pembayaran_supplier', 'Transaksi\BayarPembelianController@do_pembayaran_supplier');
    Route::post('/hapus_bayar_pembelian', 'Transaksi\BayarPembelianController@hapus_bayar_pembelian');

    //SalesOrderController
    Route::get('/sales_order', 'Transaksi\SalesOrderController@sales_order');
    Route::get('/tambah_sales_order', 'Transaksi\SalesOrderController@tambah_sales_order');
    Route::post('/ubah_format_no_nota_sales_order', 'Transaksi\SalesOrderController@ubah_format_no_nota_sales_order');
    Route::get('/nota_sales_order/{id}', 'Transaksi\SalesOrderController@nota_sales_order');
    Route::get('/nota_sales_order_print/{id}', 'Transaksi\SalesOrderController@nota_sales_order_print');
    Route::get('/nota_sales_order_pdf/{id}', 'Transaksi\SalesOrderController@nota_sales_order_pdf');
    Route::get('/detail_sales_order/{id}', 'Transaksi\SalesOrderController@detail_sales_order');
    Route::post('/get_so_produk', 'Transaksi\SalesOrderController@get_so_produk');
    Route::get('/nota_packing_list/{id}', 'Transaksi\SalesOrderController@nota_packing_list');
    Route::get('/nota_packing_list_print/{id}', 'Transaksi\SalesOrderController@nota_packing_list_print');
    Route::get('/nota_packing_list_pdf/{id}', 'Transaksi\SalesOrderController@nota_packing_list_pdf');
    Route::post('/pengaturan_so', 'Transaksi\SalesOrderController@pengaturan_so');

    Route::post('/so_tambah_cart', 'Cart\SOCartController@so_tambah_cart');
    Route::post('/so_update_cart', 'Cart\SOCartController@so_update_cart');
    Route::post('/so_hapus_cart', 'Cart\SOCartController@so_hapus_cart');

    Route::post('/so_proses', 'Transaksi\SalesOrderController@so_proses');

    /* SuratJalanKeluarController */
    Route::get('/surat_jalan_keluar', 'Transaksi\SuratJalanKeluarController@surat_jalan_keluar');
    Route::get('/tambah_surat_jalan_keluar', 'Transaksi\SuratJalanKeluarController@tambah_surat_jalan_keluar');
    Route::post('/sjk_get_so_detail', 'Transaksi\SuratJalanKeluarController@sjk_get_so_detail');
    Route::post('/sjm_get_arr_so_detail', 'Transaksi\SuratJalanKeluarController@sjm_get_arr_so_detail');
    Route::post('/sjk_get_arr_packing_list', 'Transaksi\SuratJalanKeluarController@sjk_get_arr_packing_list');
    Route::post('/sjk_get_produk_gudang', 'Transaksi\SuratJalanKeluarController@sjk_get_produk_gudang');
    Route::post('/get_so_produk_serial_number', 'Transaksi\SuratJalanKeluarController@get_so_produk_serial_number');

    Route::get('/nota_surat_jalan_keluar/{id}', 'Transaksi\SuratJalanKeluarController@nota_surat_jalan_keluar');
    Route::get('/nota_surat_jalan_keluar_print/{id}', 'Transaksi\SuratJalanKeluarController@nota_surat_jalan_keluar_print');
    Route::get('/nota_surat_jalan_keluar_pdf/{id}', 'Transaksi\SuratJalanKeluarController@nota_surat_jalan_keluar_pdf');
    Route::post('/sjk_get_no_surat_jalan', 'Transaksi\SuratJalanKeluarController@sjk_get_no_surat_jalan');

    Route::post('/surat_jalan_keluar_proses', 'Transaksi\SuratJalanKeluarController@surat_jalan_keluar_proses');
    Route::get('/lanjut_entri_surat_jalan_keluar/{id}', 'Transaksi\SuratJalanKeluarController@lanjut_entri_surat_jalan_keluar');
    Route::post('/hapus_surat_jalan_keluar', 'Transaksi\SuratJalanKeluarController@hapus_surat_jalan_keluar');

    /* NotaJualController */
    Route::get('/nota_jual', 'Transaksi\NotaJualController@nota_jual');
    Route::get('/tambah_nota_jual', 'Transaksi\NotaJualController@tambah_nota_jual');
    Route::post('/nj_get_data', 'Transaksi\NotaJualController@nj_get_data');
    Route::post('/do_tambah_nota_jual', 'Transaksi\NotaJualController@do_tambah_nota_jual');
    Route::get('/invoice_nota_jual/{id}', 'Transaksi\NotaJualController@invoice_nota_jual');
    Route::get('/invoice_nota_jual_print/{id}', 'Transaksi\NotaJualController@invoice_nota_jual_print');
    Route::get('/invoice_nota_jual_pdf/{id}', 'Transaksi\NotaJualController@invoice_nota_jual_pdf');
    Route::post('/hapus_nota_jual', 'Transaksi\NotaJualController@hapus_nota_jual');

    //ReturJualController
    Route::get('/retur_jual', 'Transaksi\ReturJualController@retur_jual');
    Route::get('/tambah_retur_jual', 'Transaksi\ReturJualController@tambah_retur_jual');
    Route::post('/retur_jual_tambah_cart', 'Transaksi\ReturJualController@retur_jual_tambah_cart');
    Route::post('/get_so_detail', 'Transaksi\ReturJualController@get_so_detail');
    Route::post('/get_nota_pelanggan', 'Transaksi\ReturJualController@get_nota_pelanggan');
    Route::post('/retur_cart_hapus', 'Transaksi\ReturJualController@retur_cart_hapus');
    Route::post('/retur_jual_checkout', 'Transaksi\ReturJualController@retur_jual_checkout');
    Route::post('/ubah_format_no_invoice_rj', 'Transaksi\ReturJualController@ubah_format_no_invoice_rj');
    Route::get('/nota_retur_jual/{id}', 'Transaksi\ReturJualController@nota_retur_jual');
    Route::get('/detail_retur_jual/{id}', 'Transaksi\ReturJualController@detail_retur_jual');
    Route::get('/nota_retur_jual_print/{id}', 'Transaksi\ReturJualController@nota_retur_jual_print');
    Route::get('/nota_retur_jual_pdf/{id}', 'Transaksi\ReturJualController@nota_retur_jual_pdf');

    //ServiceOrderController
    Route::get('/servis', 'Transaksi\ServiceOrderController@servis');
    Route::get('/servis/pilih_teknisi/{id}', 'Transaksi\ServiceOrderController@pilih_teknisi');
    Route::post('/servis/update_teknisi', 'Transaksi\ServiceOrderController@update_teknisi');
    Route::get('/tambah_servis', 'Transaksi\ServiceOrderController@tambah_servis');
    Route::post('/servis_checkout', 'Transaksi\ServiceOrderController@servis_checkout');
    Route::get('/detail_servis/{id}', 'Transaksi\ServiceOrderController@detail_servis');
    Route::post('/servis_selesai', 'Transaksi\ServiceOrderController@servis_selesai');
    Route::get('/servis_harga/{id}', 'Transaksi\ServiceOrderController@servis_harga');
    Route::post('/servis_harga_submit', 'Transaksi\ServiceOrderController@servis_harga_submit');
    Route::get('/servis_ambil', 'Transaksi\ServiceOrderController@servis_ambil');
    Route::post('/tambah_pelanggan_baru_servis', 'Transaksi\ServiceOrderController@tambah_pelanggan_baru_servis');
    Route::post('/ubah_format_no_nota_servis', 'Transaksi\ServiceOrderController@ubah_format_no_nota_servis');
    Route::get('/nota_service_order/{id}', 'Transaksi\ServiceOrderController@nota_service_order');
    Route::get('/invoice_service_order/{id}', 'Transaksi\ServiceOrderController@invoice_service_order');
    Route::get('/nota_service_order_print/{id}', 'Transaksi\ServiceOrderController@nota_service_order_print');
    Route::get('/invoice_service_order_print/{id}', 'Transaksi\ServiceOrderController@invoice_service_order_print');
    Route::get('/nota_service_order_pdf/{id}', 'Transaksi\ServiceOrderController@nota_service_order_pdf');
    Route::get('/invoice_service_order_pdf/{id}', 'Transaksi\ServiceOrderController@invoice_service_order_pdf');

    //BayarPenjualanController
    Route::get('/pembayaran_pelanggan', 'Transaksi\BayarPenjualanController@pembayaran_pelanggan');
    Route::get('/tambah_pembayaran_pelanggan', 'Transaksi\BayarPenjualanController@tambah_pembayaran_pelanggan');
    Route::post('/do_pembayaran_pelanggan', 'Transaksi\BayarPenjualanController@do_pembayaran_pelanggan');
    Route::post('/hapus_bayar_penjualan', 'Transaksi\BayarPenjualanController@hapus_bayar_penjualan');

    //VoucherController
    Route::get('/voucher', 'Transaksi\VoucherController@voucher');
    Route::get('/tambah_voucher', 'Transaksi\VoucherController@tambah_voucher');
    Route::post('/generate_kode_voucher', 'Transaksi\VoucherController@generate_kode_voucher');
    Route::post('/do_tambah_voucher', 'Transaksi\VoucherController@do_tambah_voucher');
    Route::get('/detail_voucher/{id}', 'Transaksi\VoucherController@detail_voucher');

    /* AdminEcommerceController */
    //promo creator
    Route::get('/promo_creator', 'AdminEcommerceController@promo_creator');
    Route::post('/tambah_slider_promo', 'AdminEcommerceController@tambah_slider_promo');
    Route::post('/tambah_data_produk_baru', 'AdminEcommerceController@tambah_data_produk_baru');
    Route::post('/hapus_data_produk_baru', 'AdminEcommerceController@hapus_data_produk_baru');
    Route::post('/tambah_data_produk_akan_datang', 'AdminEcommerceController@tambah_data_produk_akan_datang');
    Route::post('/hapus_data_produk_akan_datang', 'AdminEcommerceController@hapus_data_produk_akan_datang');
    Route::post('/hapus_slider_promo', 'AdminEcommerceController@hapus_slider_promo');
    Route::post('/set_order_slider_promo', 'AdminEcommerceController@set_order_slider_promo');
    Route::post('/simpan_mini_banner', 'AdminEcommerceController@simpan_mini_banner');
    Route::post('/hapus_mini_banner', 'AdminEcommerceController@hapus_mini_banner');
    Route::post('/tambah_produk_pilihan', 'AdminEcommerceController@tambah_produk_pilihan');
    Route::post('/hapus_produk_pilihan', 'AdminEcommerceController@hapus_produk_pilihan');
    Route::post('/tambah_paket_pilihan', 'AdminEcommerceController@tambah_paket_pilihan');

    //konten
    Route::get('/konten', 'AdminEcommerceController@konten');
    Route::post('/tentang_toko', 'AdminEcommerceController@tentang_toko');
    Route::post('/tata_cara', 'AdminEcommerceController@tata_cara');
    Route::post('/form_bantuan', 'AdminEcommerceController@form_bantuan');
    Route::post('/upload_gambar_produk_default', 'AdminEcommerceController@upload_gambar_produk_default');

    Route::post('/tambah_pelanggan_baru', 'Transaksi\SalesOrderController@tambah_pelanggan_baru');

    Route::get('/transaksi_jasa', 'Transaksi\TransaksiJasaController@transaksi_jasa');
    Route::get('/tambah_transaksi_jasa', 'Transaksi\TransaksiJasaController@tambah_transaksi_jasa');
    Route::post('/ubah_format_no_transaksi_jasa', 'Transaksi\TransaksiJasaController@ubah_format_no_transaksi_jasa');
    Route::post('/do_tambah_transaksi_jasa', 'Transaksi\TransaksiJasaController@do_tambah_transaksi_jasa');
    Route::get('/nota_transaksi_jasa/{id}', 'Transaksi\TransaksiJasaController@nota_transaksi_jasa');
    Route::get('/nota_transaksi_jasa_print/{id}', 'Transaksi\TransaksiJasaController@nota_transaksi_jasa_print');
    Route::get('/nota_transaksi_jasa_pdf/{id}', 'Transaksi\TransaksiJasaController@nota_transaksi_jasa_pdf');

    /*-- Service --*/
    //Beranda
    Route::get('/teknisi/beranda', 'TeknisiController@beranda_teknisi');

    //Data Akun
    Route::get('/teknisi/akun', 'TeknisiController@data_akun');

    //Servis Produk
    Route::get('/teknisi/produk_belum_servis', 'Service\ServiceController@data_service');
    Route::get('/teknisi/produk_sedang_servis', 'Service\ServiceController@data_service');
    Route::get('/teknisi/produk_selesai_servis', 'Service\ServiceController@data_service');

    //Detail Servis Produk
    Route::get('/teknisi/service_detail/{id}', 'Service\ServiceController@servis_detail');

    //Proses Perbaikan Produk
    //Proses pengisian data kerusakan
    Route::get('/teknisi/service_proses/{id}', 'Service\ServiceController@servis_proses');
    Route::post('/teknisi/service_submit', 'Service\ServiceController@servis_submit');
    Route::get('/teknisi/service_delete', 'Service\ServiceController@servis_delete');
    Route::get('/teknisi/service_cancel', 'Service\ServiceController@servis_delete');
    //Proses perbaikan, teknisi mengecek perbaikan
    Route::get('/teknisi/service_action/{id}', 'Service\ServiceController@servis_proses');
    Route::get('/teknisi/service_checked', 'Service\ServiceController@servis_checked');
});

//Pelanggan memberikan jawaban servis
Route::get('/service_confirm', 'Transaksi\ServiceOrderController@servis_confirm');

Route::get('/cek_email', 'Transaksi\ServiceOrderController@cek_email');
Route::get('/coba_mail', 'Transaksi\ServiceOrderController@coba_mail');

Route::get('/testing', function () {
    return view('pages.home.registrasi_sukses');
});

Route::group(['prefix' => 'api'], function () {
    Route::get('/produk', 'API\HomeController@actionIndex');
});

Route::get('/coba_sendmail', 'Service\ServiceController@coba_email');