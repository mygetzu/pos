<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Route;

class AuthNotForCustomer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('login');
            }
        }
        else
        {
            //cek hak akses
            if(Auth::user()->hak_akses_id == 2)
            {
                return redirect('/');
            }
            // elseif(Auth::user()->hak_akses_id != 1){
            //     $my_route = Route::getCurrentRoute()->getPath();

            //     $hak_akses = DB::table('hak_akses')->where('id', Auth::user()->hak_akses_id)->first();

            //     $cek = 1;
            //     $menu_akses = explode('-', $hak_akses->menu_akses);

            //     for ($i=0; $i < count($menu_akses); $i++) { 
            //         $menu = DB::table('menu')->where('id', $menu_akses[$i])->first();
            //         if($menu->route == $my_route){
            //             $cek = 1;
            //             break;
            //         }
            //     }

            //     if($cek == 0){
            //         return redirect('/');
            //     }
            // }
        }

        return $next($request);
    }
}
