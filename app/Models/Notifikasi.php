<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notifikasi extends Model
{
    protected $table = 'notifikasi';
    protected $fillable = [
        'kode',
        'pesan',
        'user_id_target',
        'hak_akses_id_target',
        'link',
    ];
}
