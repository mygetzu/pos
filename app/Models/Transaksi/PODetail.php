<?php

namespace App\Models\Transaksi;

use Illuminate\Database\Eloquent\Model;
use App\Models\Transaksi\POHeader;
use App\Models\Produk\Produk;
use App\Models\Gudang\Gudang;
use App\Models\Produk\Hadiah;
use App\Models\PaketProduk\Paket;

class PODetail extends Model
{
    protected $table = 'tran_po_detail';
    protected $fillable = [
        'po_header_id',
        'no_nota',
        'gudang_id',
        'produk_id',
        'jenis_barang_id',
        'quantity',
        'serial_number',
        'deskripsi',
        'harga',
    ];

    public function po_header()
    {
        return $this->belongsTo(POHeader::class, 'no_nota');
    }

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'produk_id');
    }

    public function hadiah()
    {
        return $this->belongsTo(Hadiah::class, 'produk_id');
    }

    public function paket()
    {
        return $this->belongsTo(Paket::class, 'produk_id');
    }

    public function gudang()
    {
        return $this->belongsTo(Gudang::class, 'gudang_id');
    }
}
