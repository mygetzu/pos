<?php

namespace App\Models\Transaksi;

use App\Models\Master\KategoriPelanggan;
use App\Models\Master\Produk;
use Illuminate\Database\Eloquent\Model;

class ProdukHarga extends Model
{
    protected $table    = 'trans_produk_harga';
    protected $fillable = ['produk_id', 'kategori_pelanggan_id', 'diskon', 'awal_periode', 'akhir_periode'];

    public function produk() {
        return $this->belongsTo(Produk::class, 'produk_id');
    }

    public function kategori_pelanggan() {
        return $this->belongsTo(KategoriPelanggan::class, 'kategori_pelanggan_id');
    }
}