<?php

namespace App\Models\Transaksi;

use Illuminate\Database\Eloquent\Model;
use App\Models\Transaksi\SOHeader;
use App\Models\Produk\Produk;
use App\Models\Gudang\Gudang;

class SODetail extends Model
{
    protected $table = 'tran_so_detail';
    protected $fillable = [
        'id',
        'so_header_id',
        'no_nota',
        'gudang_id',
        'produk_id',
        'jenis_barang_id',
        'quantity',
        'serial_number',
        'deskripsi',
        'harga',
    ];

    public function so_header()
    {
        return $this->belongsTo(SOHeader::class, 'no_nota');
    }

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'produk_id', 'id');
    }

    public function paket()
    {
        return $this->belongsTo(Produk::class, 'produk_id', 'id');
    }

    public function hadiah()
    {
        return $this->belongsTo(Produk::class, 'produk_id', 'id');
    }

    public function gudang()
    {
        return $this->belongsTo(Gudang::class, 'gudang_id');
    }
}
