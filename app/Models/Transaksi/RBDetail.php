<?php

namespace App\Models\Transaksi;

use Illuminate\Database\Eloquent\Model;
use App\Models\Produk\Produk;
use App\Models\Produk\Hadiah;
use App\Models\PaketProduk\Paket;
use App\Models\Gudang\Gudang;

class RBDetail extends Model
{
    protected $table = 'tran_rb_detail';
    protected $fillable = [
        'id',
        'rb_header_id',
        'no_nota',
        'gudang_id',
        'produk_id',
        'jenis_barang_id',
        'quantity',
        'serial_number',
        'deskripsi',
        'harga',
    ];

    public function rb_header()
    {
        return $this->belongsTo(RBHeader::class, 'no_nota');
    }

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'produk_id', 'id');
    }

    public function paket()
    {
        return $this->belongsTo(Paket::class, 'produk_id', 'id');
    }

    public function hadiah()
    {
        return $this->belongsTo(Hadiah::class, 'produk_id', 'id');
    }

    public function gudang()
    {
        return $this->belongsTo(Gudang::class, 'gudang_id');
    }
}
