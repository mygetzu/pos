<?php

namespace App\Models\Transaksi;

use Illuminate\Database\Eloquent\Model;
use App\Models\Supplier\Supplier;

class RBHeader extends Model
{
    protected $table = 'tran_rb_header';
    protected $fillable = [
        'no_nota',
        'supplier_id',
        'tanggal',
        'total_retur',
        'potongan_retur',
        'total_tagihan',
        'catatan',
        'no_jurnal',
    ];

    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'supplier_id');
    }

    public function rb_detail()
    {
        return $this->hasMany(RBDetail::class, 'no_nota');
    }

    public function bayar_pembelian_header()
    {
        return $this->hasOne(BayarPembelianHeader::class, 'nota_id');
    }
}
