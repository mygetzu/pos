<?php

namespace App\Models\Transaksi;

use Illuminate\Database\Eloquent\Model;
use App\Models\Transaksi\POHeader;

class PODiskon extends Model
{
    protected $table = 'tran_po_diskon';
    protected $fillable = [
        'po_header_id',
        'no_nota',
        'nominal',
        'is_dipakai',
    ];

    public function po_header()
    {
        return $this->belongsTo(POHeader::class, 'no_nota');
    }
}
