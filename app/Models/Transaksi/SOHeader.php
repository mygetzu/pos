<?php

namespace App\Models\Transaksi;

use Illuminate\Database\Eloquent\Model;
use App\Models\Transaksi\SODetail;
use App\Models\Transaksi\InvoiceDetail;
use App\Models\Pelanggan\Pelanggan;

class SOHeader extends Model
{
    protected $table = 'tran_so_header';
    protected $fillable = [
        'no_nota',
        'pelanggan_id',
        'tanggal',
        'due_date',
        'catatan',
        'total_tagihan',
        'no_jurnal',
    ];

    public function so_detail()
    {
        return $this->hasMany(SODetail::class, 'no_nota');
    }

    public function so_voucher()
    {
        return $this->hasMany(SOVoucher::class, 'no_nota');
    }

    public function invoice_detail()
    {
        return $this->belongsTo(InvoiceDetail::class, 'no_nota');
    }

    public function pelanggan()
    {
        return $this->belongsTo(Pelanggan::class, 'pelanggan_id');
    }
}
