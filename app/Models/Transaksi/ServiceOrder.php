<?php

namespace App\Models\Transaksi;

use Illuminate\Database\Eloquent\Model;
use App\Models\Transaksi\InvoiceDetail;
use App\Models\Pelanggan\Pelanggan;

class ServiceOrder extends Model
{
    protected $table = 'tran_service_order';
    protected $fillable = [
        'no_nota',
        'pelanggan_id',
        'teknisi_id',
        'model_produk',
        'serial_number',
        'kelengkapan_baterai',
        'kelengkapan_adapter',
        'kelengkapan_memory',
        'kelengkapan_harddisk',
        'kelengkapan_other',
        'kelengkapan_odd',
        'kelengkapan_scrb',
        'kelengkapan_wifi',
        'kelengkapan_modem',
        'kelengkapan_lan',
        'kelengkapan_bluetooth',
        'status_garansi_produk',
        'keluhan_pelanggan',
        'deskripsi_produk',
        'kode_verifikasi',
        'tanggal_diterima',
        'tanggal_cek',
        'tanggal_acc',
        'tanggal_diperbaiki',
        'tanggal_selesai',
        'tanggal_diambil',
        'garansi_service',
        'is_check',
        'flag_case',
        'status_service',
        'catatan',
        'no_jurnal',
    ];

    public function pelanggan()
    {
        return $this->belongsTo(Pelanggan::class, 'pelanggan_id');
    }

    public function invoice_detail()
    {
        return $this->belongsTo(InvoiceDetail::class, 'no_nota');
    }
}
