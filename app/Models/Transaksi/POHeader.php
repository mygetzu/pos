<?php

namespace App\Models\Transaksi;

use Illuminate\Database\Eloquent\Model;
use App\Models\Transaksi\PODetail;
use App\Models\Transaksi\PODiskon;
use App\Models\Supplier\Supplier;

class POHeader extends Model
{
    protected $table = 'tran_po_header';
    protected $fillable = [
        'id',
        'no_purchase_order',
        'supplier_id',
        'tanggal',
        'due_date',
        'catatan',
        'total_tagihan',
        'is_sj_lengkap',
        'syarat_ketentuan',
        'ppn',
    ];

    public function po_detail()
    {
        return $this->hasMany(PODetail::class, 'no_nota');
    }

    public function po_diskon()
    {
        return $this->hasMany(PODiskon::class, 'no_nota');
    }

    public function invoice_detail()
    {
        return $this->belongsTo(InvoiceDetail::class, 'no_nota');
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'supplier_id');
    }
}
