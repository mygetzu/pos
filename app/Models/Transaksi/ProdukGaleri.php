<?php

namespace App\Models\Transaksi;

use App\Models\Master\Produk;
use Illuminate\Database\Eloquent\Model;

class ProdukGaleri extends Model
{
    protected $table        = 'tran_produk_galeri';
    protected $fillable     = ['produk_id', 'produk_gambar'];

    public function produk() {
        return $this->belongsTo(Produk::class, 'produk_id');
    }
}
