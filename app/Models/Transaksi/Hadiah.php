<?php

namespace App\Models\Transaksi;

use App\Models\Master\Produk;
use Illuminate\Database\Eloquent\Model;

class Hadiah extends Model
{
    protected $table        = 'trans_promo_hadiah';
    protected $fillable     = ['produk_id', 'hadiah_id', 'kode_promo', 'qty_beli', 'qty_hadiah', 'awal_periode', 'akhir_periode'];

    public function produk() {
        return $this->belongsTo(Produk::class, 'produk_id');
    }
}