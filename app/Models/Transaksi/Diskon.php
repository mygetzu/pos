<?php

namespace App\Models\Transaksi;

use App\Models\Master\Produk;
use Illuminate\Database\Eloquent\Model;

class Diskon extends Model
{
    protected $table        = 'tran_promo_diskon';
    protected $fillable     = ['produk_id', 'kode_promo', 'qty_beli', 'diskon', 'awal_periode', 'akhir_periode'];

    public function produk() {
        return $this->belongsTo(Produk::class, 'produk_id');
    }
}
