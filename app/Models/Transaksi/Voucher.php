<?php

namespace App\Models\Transaksi;

use Illuminate\Database\Eloquent\Model;
use App\Models\TransaksiPenjualan\SOVoucher;

class Voucher extends Model
{
    protected $table = 'tmst_voucher';
    protected $fillable = [
        'id',
        'kode',
        'nominal',
        'awal_periode',
        'akhir_periode',
        'tanggal_dipakai',
        'is_sekali_pakai',
        'is_aktif',
        'pelanggan_id_target',
        'pelanggan_id_pakai',
    ];

    public function so_voucher()
    {
        return $this->hasMany(SOVoucher::class, 'voucher_id');
    }
}
