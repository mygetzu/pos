<?php

namespace App\Models\Transaksi;

use Illuminate\Database\Eloquent\Model;

class SOHeader extends Model
{
    protected $table = 'tran_so_pembayaran';
    protected $fillable = [
        'so_header_id',
        'tanggal',
        'metode_pembayaran_id',
        'bank_id',
        'nomor_pembayaran',
        'nominal',
    ];
}