<?php

namespace App\Models\Transaksi;

use App\Models\Master\Produk;
use Illuminate\Database\Eloquent\Model;

class Cashback extends Model
{
    protected $table    = 'tran_promo_cashback';
    protected $fillable = ['produk_id', 'kode_promo', 'qty_beli', 'cashback', 'awal_periode', 'akhir_periode'];

    public function produk() {
        return $this->belongsTo(Produk::class, 'produk_id');
    }
}
