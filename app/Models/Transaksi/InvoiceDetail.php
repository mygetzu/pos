<?php

namespace App\Models\Transaksi;

use Illuminate\Database\Eloquent\Model;
use App\Models\Transaksi\Invoice;
use App\Models\Transaksi\SOHeader;

class InvoiceDetail extends Model
{
    protected $table = 'tran_invoice_detail';
    protected $fillable = [
        'invoice_no',
        'no_nota',
        'total_tagihan',
        'status',
    ];

    public function invoice()
    {
        return $this->belongsTo(Invoice::class, 'invoice_no', 'invoice_no');
    }

    public function so_header()
    {
        return $this->hasOne(SOHeader::class, 'no_nota', 'no_nota');
    }

    public function po_header()
    {
        return $this->hasOne(POHeader::class, 'no_nota', 'no_nota');
    }

    public function service_order()
    {
        return $this->hasOne(ServiceOrder::class, 'no_nota', 'no_nota');
    }
}
