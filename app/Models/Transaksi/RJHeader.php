<?php

namespace App\Models\Transaksi;

use Illuminate\Database\Eloquent\Model;
use App\Models\Pelanggan\Pelanggan;

class RJHeader extends Model
{
    protected $table = 'tran_rj_header';
    protected $fillable = [
        'id',
        'no_nota',
        'pelanggan_id',
        'tanggal',
        'total_retur',
        'potongan_retur',
        'total_tagihan',
        'catatan',
        'no_jurnal',
    ];

    public function pelanggan()
    {
        return $this->belongsTo(Pelanggan::class, 'pelanggan_id');
    }

    public function rj_detail()
    {
        return $this->hasMany(RJDetail::class, 'no_nota');
    }

    public function bayar_penjualan_header()
    {
        return $this->hasOne(BayarPenjualanHeader::class, 'nota_id');
    }
}
