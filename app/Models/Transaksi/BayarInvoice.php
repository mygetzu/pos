<?php

namespace App\Models\Transaksi;

use Illuminate\Database\Eloquent\Model;
use App\Models\Transaksi\Invoice;

class BayarInvoice extends Model
{
    protected $table = 'tran_bayar_invoice';
    protected $fillable = [
        'invoice_no',
        'bayar_id',
        'tanggal_bayar',
        'metode',
        'nomor_pembayaran',
        'nominal',
        'bank_id',
    ];

    public function invoice()
    {
        return $this->belongsTo(Invoice::class, 'invoice_no', 'invoice_no');
    }
}
