<?php

namespace App\Models\Transaksi;

use Illuminate\Database\Eloquent\Model;
use App\Models\Transaksi\InvoiceDetail;
use App\Models\Transaksi\BayarInvoice;

class Invoice extends Model
{
    protected $table = 'tran_invoice';
    protected $fillable = [
        'invoice_no',
        'tanggal_invoice',
        'jatuh_tempo',
        'total_tagihan',
        'is_lunas',
    ];

    public function invoice_detail()
    {
        return $this->hasOne(InvoiceDetail::class, 'invoice_no', 'invoice_no');
    }

    public function bayar_invoice()
    {
        return $this->hasOne(BayarInvoice::class, 'no_nota', 'no_nota');
    }
}
