<?php

namespace App\Models\Transaksi;

use Illuminate\Database\Eloquent\Model;
use App\Models\Transaksi\SOHeader;

class SOVoucher extends Model
{
    protected $table = 'tran_so_voucher';
    protected $fillable = [
        'id',
        'so_header_id',
        'no_nota',
        'voucher_id',
        'nominal',
    ];

    public function so_header()
    {
        return $this->belongsTo(SOHeader::class, 'no_nota');
    }
}
