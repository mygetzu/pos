<?php

namespace App\Models\Gudang;

use Illuminate\Database\Eloquent\Model;
use App\Models\PaketProduk\Paket;

class PaketGudang extends Model
{
    protected $table = 'tran_paket_gudang';
    protected $fillable = [
        'paket_id',
        'gudang_id',
        'stok',
    ];

    public function gudang()
    {
        return $this->belongsTo(Gudang::class, 'gudang_id');
    }

    public function paket()
    {
        return $this->belongsTo(Paket::class, 'paket_id');
    }
}
