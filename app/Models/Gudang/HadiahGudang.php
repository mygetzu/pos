<?php

namespace App\Models\Gudang;

use Illuminate\Database\Eloquent\Model;
use App\Models\Produk\Hadiah;

class HadiahGudang extends Model
{
    protected $table = 'tran_hadiah_gudang';
    protected $fillable = [
        'hadiah_id',
        'gudang_id',
        'stok',
    ];

    public function gudang()
    {
        return $this->belongsTo(Gudang::class, 'gudang_id');
    }

    public function hadiah()
    {
        return $this->belongsTo(Hadiah::class, 'hadiah_id');
    }
}
