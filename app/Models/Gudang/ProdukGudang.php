<?php

namespace App\Models\Gudang;

use Illuminate\Database\Eloquent\Model;
use App\Models\Produk\Produk;

class ProdukGudang extends Model
{
    protected $table = 'tran_produk_gudang';
    protected $fillable = [
        'produk_id',
        'gudang_id',
        'stok',
    ];

    public function gudang()
    {
        return $this->belongsTo(Gudang::class, 'gudang_id');
    }

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'produk_id');
    }
}
