<?php

namespace App\Models\Gudang;

use Illuminate\Database\Eloquent\Model;
use App\Models\Produk\ProdukSerialNumber;
use App\Models\Stok\MutasiStokHeader;
use App\Models\Stok\StokOpnameHeader;
use App\Models\Laporan\LaporanStokHeader;
use App\Models\Laporan\KartuStokHeader;
use App\Models\TransaksiPembelian\SJMasukDetail;
use App\Models\TransaksiPembelian\RBDetail;
use App\Models\TransaksiPenjualan\SJKeluarDetail;
use App\Models\TransaksiPenjualan\RJDetail;
use App\Models\Stok\PenyesuaianStokHeader;

class Gudang extends Model
{
    protected $table = 'tmst_gudang';
    protected $fillable = [
        'nama',
        'alamat',
        'kota_id',
        'telp',
        'supervisor',
        'hp_supervisor',
        'longitude',
        'latitude',
        'is_aktif',
        'format_no_mutasi',
    ];

    public function produk_gudang()
    {
        return $this->hasMany(ProdukGudang::class, 'gudang_id');
    }

    public function produk_serial_number()
    {
        return $this->hasMany(ProdukSerialNumber::class, 'gudang_id');
    }

    public function hadiah_gudang()
    {
        return $this->hasMany(HadiahGudang::class, 'gudang_id');
    }

    public function mutasi_stok_header_asal()
    {
        return $this->hasMany(MutasiStokHeader::class, 'gudang_id_asal');
    }

    public function mutasi_stok_header_tujuan()
    {
        return $this->hasMany(MutasiStokHeader::class, 'gudang_id_tujuan');
    }

    public function stok_opname_header()
    {
        return $this->hasMany(StokOpnameHeader::class, 'gudang_id');
    }

    public function laporan_stok_header()
    {
        return $this->hasMany(LaporanStokHeader::class, 'gudang_id');
    }

    public function kartu_stok_header()
    {
        return $this->hasMany(KartuStokHeader::class, 'gudang_id');
    }

    public function sj_masuk_detail()
    {
        return $this->hasMany(SJMasukDetail::class, 'gudang_id');
    }

    public function rb_detail()
    {
        return $this->hasMany(RBDetail::class, 'gudang_id');
    }

    public function sj_keluar_detail()
    {
        return $this->hasMany(SJKeluarDetail::class, 'gudang_id');
    }

    public function rj_detail()
    {
        return $this->hasMany(RJDetail::class, 'gudang_id');
    }

    public function penyesuaian_stok_header()
    {
        return $this->hasMany(PenyesuaianStokHeader::class, 'gudang_id');
    }
}
