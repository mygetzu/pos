<?php

namespace App\Models\PaketProduk;

use Illuminate\Database\Eloquent\Model;
use App\Models\Produk\Produk;

class PaketProduk extends Model
{
    protected $table = 'tran_paket_produk';
    protected $fillable = [
        'paket_id',
        'produk_id',
        'qty_produk',
    ];

    public function paket()
    {
        return $this->belongsTo(Paket::class, 'paket_id');
    }

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'produk_id');
    }
}
