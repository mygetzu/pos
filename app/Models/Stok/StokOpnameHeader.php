<?php

namespace App\Models\Stok;

use Illuminate\Database\Eloquent\Model;
use App\Models\Gudang\Gudang;

class StokOpnameHeader extends Model
{
    protected $table = 'tran_stok_opname_header';
    protected $fillable = [
        'id',
        'no_stok_opname',
        'gudang_id',
        'tanggal',
        'total_stok',
        'approval_by_user_id',
        'received_by_user_id',
    ];

    public function stok_opname_detail()
    {
        return $this->hasMany(StokOpnameDetail::class, 'stok_opname_header_id');
    }

    public function gudang()
    {
        return $this->belongsTo(Gudang::class, 'gudang_id');
    }
}
