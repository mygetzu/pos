<?php

namespace App\Models\Stok;

use Illuminate\Database\Eloquent\Model;
use App\Models\Produk\Produk;

class StokOpnameDetail extends Model
{
    protected $table = 'tran_stok_opname_detail';
    protected $fillable = [
        'id',
        'stok_opname_header_id',
        'no_stok_opname',
        'produk_id',
        'serial_number',
        'jumlah',
    ];

    public function stok_opname_header()
    {
        return $this->belongsTo(StokOpnameHeader::class, 'stok_opname_header_id');
    }

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'produk_id');
    }
}
