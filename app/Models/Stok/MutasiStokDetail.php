<?php

namespace App\Models\Stok;

use Illuminate\Database\Eloquent\Model;
use App\Models\Produk\Produk;

class MutasiStokDetail extends Model
{
    protected $table = 'tran_mutasi_stok_detail';
    protected $fillable = [
        'id',
        'mutasi_stok_header_id',
        'no_mutasi_stok',
        'produk_id',
        'serial_number',
        'jumlah',
    ];

    public function mutasi_stok_detail()
    {
        return $this->hasMany(MutasiStokDetail::class, 'mutasi_stok_header_id');
    }

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'produk_id');
    }
}
