<?php

namespace App\Models\Stok;

use Illuminate\Database\Eloquent\Model;
use App\Models\Gudang\Gudang;

class MutasiStokHeader extends Model
{
    protected $table = 'tran_mutasi_stok_header';
    protected $fillable = [
        'id',
        'no_mutasi_stok',
        'gudang_id_asal',
        'gudang_id_tujuan',
        'tanggal',
        'approval_by_user_id',
        'received_by_user_id',
    ];

    public function mutasi_stok_detail()
    {
        return $this->hasMany(MutasiStokDetail::class, 'mutasi_stok_header_id');
    }

    public function gudang_asal()
    {
        return $this->belongsTo(Gudang::class, 'gudang_id_asal');
    }

    public function gudang_tujuan()
    {
        return $this->belongsTo(Gudang::class, 'gudang_id_tujuan');
    }
}
