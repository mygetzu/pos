<?php

namespace App\Models\Stok;

use Illuminate\Database\Eloquent\Model;
use App\Models\Produk\Produk;

class PenyesuaianStokDetail extends Model
{
    protected $table = 'tran_penyesuaian_stok_detail';
    protected $fillable = [
        'id',
        'penyesuaian_stok_header_id',
        'produk_id',
        'serial_number',
        'stok_awal',
        'stok_ubah',
        'stok_baru'
    ];

    public function penyesuaian_stok_header()
    {
        return $this->belongsTo(PenyesuaianStokHeader::class, 'penyesuaian_stok_header_id');
    }

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'produk_id');
    }
}
