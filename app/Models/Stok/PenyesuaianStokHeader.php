<?php

namespace App\Models\Stok;

use Illuminate\Database\Eloquent\Model;
use App\Models\Gudang\Gudang;
use App\User;

class PenyesuaianStokHeader extends Model
{
    protected $table = 'tran_penyesuaian_stok_header';
    protected $fillable = [
        'id',
        'no_penyesuaian_stok',
        'gudang_id',
        'tanggal',
        'keterangan',
        'approval_by_user_id',
        'received_by_user_id',
    ];

    public function gudang()
    {
        return $this->belongsTo(Gudang::class, 'gudang_id');
    }

    public function penyesuaian_stok_detail()
    {
        return $this->hasMany(PenyesuaianStokDetail::class, 'penyesuaian_stok_header_id');
    }

    public function approval_by_user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function received_by_user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
