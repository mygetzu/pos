<?php

namespace App\Models\Laporan;

use Illuminate\Database\Eloquent\Model;
use App\Models\Referensi\SumberData;

class KartuStokDetail extends Model
{
    protected $table = 'tran_kartu_stok_detail';
    protected $fillable = [
        'id',
        'kartu_stok_header_id',
        'sumber_data_id',
        'supplier_or_pelanggan',
        'no_nota',
        'serial_number',
        'tanggal',
        'jumlah',
        'harga',
    ];

    public function kartu_stok_header()
    {
        return $this->belongsTo(LaporanStokHeader::class, 'kartu_stok_header_id');
    }

    public function sumber_data()
    {
        return $this->belongsTo(SumberData::class, 'sumber_data_id');
    }
}
