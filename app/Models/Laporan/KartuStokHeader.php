<?php

namespace App\Models\Laporan;

use Illuminate\Database\Eloquent\Model;
use App\Models\Gudang\Gudang;
use App\Models\Produk\Produk;

class KartuStokHeader extends Model
{
    protected $table = 'tran_kartu_stok_header';
    protected $fillable = [
        'id',
        'gudang_id',
        'produk_id',
        'tanggal_awal',
        'tanggal_akhir',
        'total_stok',
    ];

    public function kartu_stok_detail()
    {
        return $this->hasMany(KartuStokDetail::class, 'kartu_stok_header_id');
    }

    public function gudang()
    {
        return $this->belongsTo(Gudang::class, 'gudang_id');
    }

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'produk_id');
    }
}
