<?php

namespace App\Models\Laporan;

use Illuminate\Database\Eloquent\Model;
use App\Models\Gudang\Gudang;

class LaporanStokHeader extends Model
{
    protected $table = 'tran_laporan_stok_header';
    protected $fillable = [
        'id',
        'gudang_id',
        'tanggal',
        'total_stok',
    ];

    public function laporan_stok_detail()
    {
        return $this->hasMany(LaporanStokDetail::class, 'laporan_stok_header_id');
    }

    public function gudang()
    {
        return $this->belongsTo(Gudang::class, 'gudang_id');
    }
}
