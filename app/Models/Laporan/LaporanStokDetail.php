<?php

namespace App\Models\Laporan;

use Illuminate\Database\Eloquent\Model;
use App\Models\Produk\Produk;

class LaporanStokDetail extends Model
{
    protected $table = 'tran_laporan_stok_detail';
    protected $fillable = [
        'id',
        'laporan_stok_header_id',
        'produk_id',
        'jumlah',
    ];

    public function laporan_stok_header()
    {
        return $this->belongsTo(LaporanStokHeader::class, 'laporan_stok_header_id');
    }

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'produk_id');
    }
}
