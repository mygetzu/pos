<?php

namespace App\Models\Laporan;

use Illuminate\Database\Eloquent\Model;

class JurnalUmum extends Model
{
    protected $table = 'tran_jurnal_umum';
    protected $fillable = [
        'tanggal',
        'akun_kode',
        'akun_nama',
        'referensi',
        'debet',
        'kredit',
    ];
}
