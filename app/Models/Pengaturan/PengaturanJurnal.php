<?php

namespace App\Models\Pengaturan;

use Illuminate\Database\Eloquent\Model;
use App\Models\Pengaturan\NamaTransaksi;
use App\Models\Pengaturan\MasterCOA;

class PengaturanJurnal extends Model
{
    protected $table = 'tran_pengaturan_jurnal';
    protected $fillable = [
        'transaksi_id',
        'akun_id',
        'debet_or_kredit',
    ];

    public function master_coa()
    {
        return $this->belongsTo(MasterCOA::class, 'akun_id');
    }

    public function nama_transaksi()
    {
        return $this->belongsTo(NamaTransaksi::class, 'transaksi_id');
    }
}
