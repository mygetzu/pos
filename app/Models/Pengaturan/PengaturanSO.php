<?php

namespace App\Models\Pengaturan;

use Illuminate\Database\Eloquent\Model;

class PengaturanSO extends Model
{
    protected $table = 'tref_pengaturan_so';
    protected $fillable = [
        'syarat_ketentuan',
        'ppn',
    ];
}
