<?php

namespace App\Models\Pengaturan;

use Illuminate\Database\Eloquent\Model;
use App\Models\Pengaturan\PengaturanJurnal;

class NamaTransaksi extends Model
{
    protected $table = 'tref_nama_transaksi';
    protected $fillable = [
        'nama',
        'transaksi',
    ];

    public function pengaturan_jurnal()
    {
        return $this->hasMany(PengaturanJurnal::class, 'transaksi_id');
    }
}
