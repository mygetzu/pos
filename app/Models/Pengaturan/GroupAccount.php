<?php

namespace App\Models\Pengaturan;

use Illuminate\Database\Eloquent\Model;

class GroupAccount extends Model
{
    protected $table = 'group_account';
    protected $fillable = [
        'kode',
        'nama',
        'debit_or_kredit',
    ];
}
