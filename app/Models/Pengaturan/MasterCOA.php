<?php

namespace App\Models\Pengaturan;

use Illuminate\Database\Eloquent\Model;
use App\Models\Pengaturan\PengaturanJurnal;
use App\Models\Accounting\JurnalUmumDetail;

class MasterCOA extends Model
{
    protected $table = 'tmst_master_coa';
    protected $fillable = [
        'kode',
        'nama',
        'level',
        'parent',
    ];

    public function pengaturan_jurnal()
    {
        return $this->hasOne(PengaturanJurnal::class, 'akun_id');
    }

    public function jurnal_umum_detail()
    {
        return $this->hasOne(JurnalUmumDetail::class, 'akun_kode', 'kode');
    }
}
