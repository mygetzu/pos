<?php

namespace App\Models\Pengaturan;

use Illuminate\Database\Eloquent\Model;

class PengaturanPO extends Model
{
    protected $table = 'tref_pengaturan_po';
    protected $fillable = [
        'alamat_pengiriman',
        'syarat_ketentuan',
        'ppn',
    ];
}
