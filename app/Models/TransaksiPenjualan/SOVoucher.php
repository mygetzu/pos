<?php

namespace App\Models\TransaksiPenjualan;

use Illuminate\Database\Eloquent\Model;
use App\Models\Transaksi\Voucher;

class SOVoucher extends Model
{
    protected $table = 'tran_so_voucher';
    protected $fillable = [
        'id',
        'so_header_id',
        'voucher_id',
        'nominal',
    ];

    public function so_header()
    {
        return $this->belongsTo(SOHeader::class, 'so_header_id');
    }

    public function voucher()
    {
        return $this->belongsTo(Voucher::class, 'voucher_id');
    }
}
