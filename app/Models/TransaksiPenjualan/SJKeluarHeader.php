<?php

namespace App\Models\TransaksiPenjualan;

use Illuminate\Database\Eloquent\Model;
use App\Models\Pelanggan\Pelanggan;

class SJKeluarHeader extends Model
{
    protected $table = 'tran_sj_keluar_header';
    protected $fillable = [
        'id',
        'so_header_id',
        'no_surat_jalan',
        'pelanggan_id',
        'tanggal',
        'ppn',
        'total_harga',
        'total_voucher',
        'total_tagihan',
    ];

    public function so_header()
    {
        return $this->belongsTo(SOHeader::class, 'so_header_id');
    }

    public function pelanggan()
    {
        return $this->belongsTo(Pelanggan::class, 'pelanggan_id');
    }

    public function sj_keluar_detail()
    {
        return $this->hasMany(SJKeluarDetail::class, 'sj_keluar_header_id');
    }

    public function nota_jual()
    {
        return $this->hasOne(NotaJual::class, 'sj_keluar_header_id');
    }
}
