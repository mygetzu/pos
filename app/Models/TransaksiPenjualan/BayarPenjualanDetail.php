<?php

namespace App\Models\TransaksiPenjualan;

use Illuminate\Database\Eloquent\Model;
use App\Models\Referensi\MetodePembayaran;

class BayarPenjualanDetail extends Model
{
    protected $table = 'tran_bayar_penjualan_detail';
    protected $fillable = [
        'id',
        'bayar_penjualan_header_id',
        'metode_pembayaran_id',
        'nomor_pembayaran',
        'nominal',
        'bank',
    ];

    public function bayar_penjualan_header()
    {
        return $this->belongsTo(BayarPenjualanHeader::class, 'bayar_penjualan_header_id');
    }

    public function metode_pembayaran()
    {
        return $this->belongsTo(MetodePembayaran::class, 'metode_pembayaran_id');
    }
}
