<?php

namespace App\Models\TransaksiPenjualan;

use Illuminate\Database\Eloquent\Model;
use App\Models\Pelanggan\Pelanggan;

class RJHeader extends Model
{
    protected $table = 'tran_rj_header';
    protected $fillable = [
        'id',
        'nota_jual_id',
        'no_nota',
        'pelanggan_id',
        'tanggal',
        'potongan_retur',
        'total_retur',
        'catatan',
        'no_jurnal',
        'is_lunas',
    ];

    public function nota_jual()
    {
        return $this->belongsTo(NotaJual::class, 'nota_jual_id');
    }

    public function pelanggan()
    {
        return $this->belongsTo(Pelanggan::class, 'pelanggan_id');
    }

    public function rj_detail()
    {
        return $this->hasMany(RJDetail::class, 'rj_header_id');
    }

    public function bayar_penjualan()
    {
        return $this->hasOne(BayarPenjualan::class, 'nota_id');
    }
}
