<?php

namespace App\Models\TransaksiPenjualan;

use App\User;
use Illuminate\Database\Eloquent\Model;
use App\Models\Pelanggan\Pelanggan;

class ServiceOrder extends Model
{
    protected $table = 'tran_service_order';
    protected $fillable = [
        'no_nota',
        'pelanggan_id',
        'teknisi_id',
        'model_produk',
        'serial_number',
        'status_garansi_produk',
        'keluhan_pelanggan',
        'deskripsi_produk',
        'kode_verifikasi',
        'garansi_service',
        'tanggal_diterima',
        'tanggal_cek',
        'tanggal_acc',
        'tanggal_diperbaiki',
        'tanggal_selesai',
        'tanggal_diambil',
        'is_check',
        'flag_acc',
        'flag_case',
        'is_lunas',
        'status_service',
        'catatan',
        'no_jurnal',
    ];

    public function pelanggan()
    {
        return $this->belongsTo(Pelanggan::class, 'pelanggan_id');
    }

    public function teknisi(){
        return $this->belongsTo(User::class, 'teknisi_id');
    }

    public function bayar_penjualan_header()
    {
        return $this->hasOne(BayarPenjualanHeader::class, 'nota_id');
    }
}
