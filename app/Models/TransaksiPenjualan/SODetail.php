<?php

namespace App\Models\TransaksiPenjualan;

use Illuminate\Database\Eloquent\Model;
use App\Models\Produk\Produk;
use App\Models\Produk\Hadiah;
use App\Models\PaketProduk\Paket;

class SODetail extends Model
{
    protected $table = 'tran_so_detail';
    protected $fillable = [
        'id',
        'so_header_id',
        'produk_id',
        'jenis_barang_id',
        'jumlah',
        'jumlah_terkirim',
        'deskripsi',
        'harga',
    ];

    public function so_header()
    {
        return $this->belongsTo(SOHeader::class, 'so_header_id');
    }

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'produk_id');
    }

    public function hadiah()
    {
        return $this->belongsTo(Hadiah::class, 'produk_id');
    }

    public function paket()
    {
        return $this->belongsTo(Paket::class, 'produk_id');
    }
}
