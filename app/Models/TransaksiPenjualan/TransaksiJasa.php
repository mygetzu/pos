<?php

namespace App\Models\TransaksiPenjualan;

use Illuminate\Database\Eloquent\Model;
use App\Models\Pelanggan\Pelanggan;

class TransaksiJasa extends Model
{
    protected $table = 'tran_transaksi_jasa';
    protected $fillable = [
        'id',
        'no_nota',
        'pelanggan_id',
        'tanggal',
        'deskripsi',
        'biaya',
        'no_jurnal',
        'flag',
        'is_lunas',
    ];

    public function pelanggan()
    {
        return $this->belongsTo(Pelanggan::class, 'pelanggan_id');
    }

    public function bayar_penjualan_header()
    {
        return $this->hasOne(BayarPenjualanHeader::class, 'nota_id');
    }
}
