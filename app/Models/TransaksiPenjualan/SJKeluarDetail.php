<?php

namespace App\Models\TransaksiPenjualan;

use Illuminate\Database\Eloquent\Model;
use App\Models\Produk\Produk;
use App\Models\Produk\Hadiah;
use App\Models\PaketProduk\Paket;
use App\Models\Gudang\Gudang;

class SJKeluarDetail extends Model
{
    protected $table = 'tran_sj_keluar_detail';
    protected $fillable = [
        'id',
        'sj_keluar_header_id',
        'produk_id',
        'jenis_barang_id',
        'gudang_id',
        'serial_number',
        'jumlah',
        'harga',
    ];

    public function sj_keluar_header()
    {
        return $this->belongsTo(SJKeluarHeader::class, 'sj_keluar_header_id');
    }

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'produk_id');
    }

    public function hadiah()
    {
        return $this->belongsTo(Hadiah::class, 'produk_id');
    }

    public function paket()
    {
        return $this->belongsTo(Paket::class, 'produk_id');
    }

    public function gudang()
    {
        return $this->belongsTo(Paket::class, 'gudang_id');
    }
}
