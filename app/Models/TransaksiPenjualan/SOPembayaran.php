<?php

namespace App\Models\TransaksiPenjualan;

use Illuminate\Database\Eloquent\Model;

class SOPembayaran extends Model
{
    protected $table = 'tran_so_pembayaran';
    protected $fillable = [
        'id',
        'so_header_id',
        'tanggal',
        'metode_pembayaran_id',
        'bank_id',
        'nomor_pembayaran',
        'nominal',
    ];

    public function so_header()
    {
        return $this->belongsTo(SOHeader::class, 'so_header_id');
    }
}
