<?php

namespace App\Models\TransaksiPenjualan;

use Illuminate\Database\Eloquent\Model;
use App\Models\Pelanggan\Pelanggan;

class NotaJual extends Model
{
    protected $table = 'tran_nota_jual';
    protected $fillable = [
        'id',
        'sj_keluar_header_id',
        'no_nota',
        'pelanggan_id',
        'tanggal',
        'no_jurnal',
        'is_lunas',
        'ppn',
        'total_harga',
        'total_voucher',
        'total_tagihan',
    ];

    public function sj_keluar_header()
    {
        return $this->belongsTo(SJKeluarHeader::class, 'sj_keluar_header_id');
    }

    public function pelanggan()
    {
        return $this->belongsTo(Pelanggan::class, 'pelanggan_id');
    }

    public function bayar_penjualan_header()
    {
        return $this->hasOne(BayarPenjualanHeader::class, 'nota_id');
    }

    public function rj_header()
    {
        return $this->hasMany(RJHeader::class, 'nota_jual_id');
    }
}
