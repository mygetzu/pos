<?php

namespace App\Models\TransaksiPenjualan;

use Illuminate\Database\Eloquent\Model;
use App\Models\Referensi\Kota;
use App\Models\Referensi\MetodePengiriman;

class SOAlamatPengiriman extends Model
{
    protected $table = 'tran_so_alamat_pengiriman';
    protected $fillable = [
        'id',
        'so_header_id',
        'nama',
        'email',
        'alamat',
        'kota_id',
        'kode_pos',
        'hp',
        'metode_pengiriman_id',
    ];

    public function so_header()
    {
        return $this->belongsTo(SOHeader::class, 'so_header_id');
    }

    public function kota()
    {
        return $this->belongsTo(Kota::class, 'kota_id');
    }

    public function metode_pengiriman()
    {
        return $this->belongsTo(MetodePengiriman::class, 'metode_pengiriman_id');
    }
}
