<?php

namespace App\Models\TransaksiPenjualan;

use Illuminate\Database\Eloquent\Model;
use App\Models\Pelanggan\Pelanggan;

class SOHeader extends Model
{
    protected $table = 'tran_so_header';
    protected $fillable = [
        'id',
        'no_sales_order',
        'pelanggan_id',
        'tanggal',
        'due_date',
        'catatan',
        'total_tagihan',
        'is_sj_lengkap',
        'ppn',
    ];

    public function pelanggan()
    {
        return $this->belongsTo(Pelanggan::class, 'pelanggan_id');
    }

    public function sj_keluar_header()
    {
        return $this->hasMany(SJKeluarHeader::class, 'so_header_id');
    }

    public function so_detail()
    {
        return $this->hasMany(SODetail::class, 'so_header_id');
    }

    public function so_alamat_pengiriman()
    {
        return $this->hasOne(SOAlamatPengiriman::class, 'so_header_id');
    }

    public function so_voucher()
    {
        return $this->hasMany(SOVoucher::class, 'so_header_id');
    }

    public function so_pembayaran()
    {
        return $this->hasMany(SOPembayaran::class, 'so_header_id');
    }
}
