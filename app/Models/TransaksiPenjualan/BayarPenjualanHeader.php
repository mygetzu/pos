<?php

namespace App\Models\TransaksiPenjualan;

use Illuminate\Database\Eloquent\Model;
use App\Models\Pelanggan\Pelanggan;

class BayarPenjualanHeader extends Model
{
    protected $table = 'tran_bayar_penjualan_header';
    protected $fillable = [
        'id',
        'nota_id',
        'jenis',
        'pelanggan_id',
        'tanggal',
        'total_tagihan',
        'total_pembayaran',
        'keterangan',
        'no_jurnal',
    ];

    public function bayar_penjualan_detail()
    {
        return $this->hasMany(BayarPenjualanDetail::class, 'bayar_penjualan_header_id');
    }

    public function nota_jual()
    {
        return $this->belongsTo(NotaJual::class, 'nota_id');
    }

    public function pelanggan()
    {
        return $this->belongsTo(Pelanggan::class, 'pelanggan_id');
    }

    public function rj_header()
    {
        return $this->belongsTo(RJHeader::class, 'nota_id');
    }

    public function service_order()
    {
        return $this->belongsTo(ServiceOrder::class, 'nota_id');
    }

    public function transaksi_jasa()
    {
        return $this->belongsTo(TransaksiJasa::class, 'nota_id');
    }
}
