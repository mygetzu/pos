<?php

namespace App\Models\Pelanggan;

use Illuminate\Database\Eloquent\Model;
use App\Models\Pelanggan\Pelanggan;
use App\Models\Pelanggan\ProdukHarga;

class KategoriPelanggan extends Model
{
    protected $table = 'tmst_kategori_pelanggan';
    protected $fillable = [
        'nama',
        'deskripsi',
        'default_diskon',
    ];

    public function pelanggan()
    {
        return $this->hasMany(Pelanggan::class, 'kategori_id');
    }

    public function produk_harga()
    {
        return $this->hasMany(ProdukHarga::class, 'kategori_pelanggan_id');
    }
}
