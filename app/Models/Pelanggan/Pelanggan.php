<?php

namespace App\Models\Pelanggan;

use App\User;
use Illuminate\Database\Eloquent\Model;
use App\Models\Pelanggan\KategoriPelanggan;
use App\Models\Referensi\Kota;
use App\Models\TransaksiPenjualan\SOHeader;
use App\Models\TransaksiPenjualan\SJKeluarHeader;
use App\Models\TransaksiPenjualan\NotaJual;
use App\Models\TransaksiPenjualan\RJHeader;
use App\Models\TransaksiPenjualan\ServiceOrder;
use App\Models\TransaksiPenjualan\BayarPenjualanHeader;
use App\Models\TransaksiPenjualan\TransksiJasa;
use App\Models\TransaksiService\InvoiceHeader;

class Pelanggan extends Model
{
    protected $table = 'tmst_pelanggan';
    protected $fillable = [
        'user_id',
        'nama',
        'email',
        'alamat',
        'kota_id',
        'telp1',
        'telp2',
        'kode_pos',
        'nama_sales',
        'hp_sales',
        'deskripsi',
        'kategori_id',
    ];

    public static $validation_rules  = [
        'nama'      => 'required|max:255',
        'alamat'    => 'required|min:5',
        'kota_id'   => 'required',
        'kode_pos'  => 'required',
    ];

    public function kategori_pelanggan()
    {
        return $this->belongsTo(KategoriPelanggan::class, 'kategori_id');
    }

    public function kota()
    {
        return $this->belongsTo(Kota::class, 'kota_id');
    }

    public function so_header()
    {
        return $this->hasMany(SOHeader::class, 'pelanggan_id');
    }

    public function sj_keluar_header()
    {
        return $this->hasMany(SJKeluarHeader::class, 'pelanggan_id');
    }

    public function nota_jual()
    {
        return $this->hasMany(NotaJual::class, 'pelanggan_id');
    }

    public function rj_header()
    {
        return $this->hasMany(RJHeader::class, 'pelanggan_id');
    }

    public function service_order()
    {
        return $this->hasMany(ServiceOrder::class, 'pelanggan_id');
    }

    public function service_invoice_header()
    {
        return $this->hasMany(InvoiceHeader::class, 'pelanggan_id');
    }

    public function bayar_penjualan_header()
    {
        return $this->hasMany(BayarPenjualanHeader::class, 'pelanggan_id');
    }

    public function transaksi_jasa()
    {
        return $this->hasMany(TransksiJasa::class, 'pelanggan_id');
    }

    public function pelanggan_cart()
    {
        return $this->hasMany(PelangganCart::class, 'pelanggan_id');
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }
}
