<?php

namespace App\Models\Pelanggan;

use App\Models\Referensi\JenisBarang;
use Illuminate\Database\Eloquent\Model;
use App\Models\Produk\Produk;
use App\Models\Produk\Hadiah;
use App\Models\PaketProduk\Paket;

class PelangganCart extends Model
{
    protected $table = 'tran_pelanggan_cart';
    protected $fillable = [
        'id',
        'pelanggan_id',
        'produk_id',
        'jenis_barang_id',
        'jumlah',
        'harga_retail',
        'harga_akhir',
        'jenis_promo'
    ];

    public static $validation_rules = [
        'produk_id'         => 'required',
        'jenis_barang_id'   => 'required',
        'jumlah'            => 'required',
        'harga_lama'        => 'required',
        'harga_retail'      => 'required',
    ];

    public function pelanggan()
    {
        return $this->belongsTo(Pelanggan::class, 'pelanggan_id');
    }

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'produk_id');
    }

    public function hadiah()
    {
        return $this->belongsTo(Hadiah::class, 'produk_id');
    }

    public function paket()
    {
        return $this->belongsTo(Paket::class, 'produk_id');
    }

    public function jenis_barang() {
        return $this->belongsTo(JenisBarang::class, 'jenis_barang_id');
    }
}
