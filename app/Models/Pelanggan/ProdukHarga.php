<?php

namespace App\Models\Pelanggan;

use Illuminate\Database\Eloquent\Model;
use App\Models\Produk\Produk;
use App\Models\Pelanggan\KategoriPelanggan;


class ProdukHarga extends Model
{
    protected $table = 'tran_produk_harga';
    protected $fillable = [
        'produk_id',
        'kategori_pelanggan_id',
        'diskon',
        'awal_periode',
        'akhir_periode',
    ];

    public function scopePeriode($query)
    {
        return $query->where('awal_periode' , '<=', date("Y-m-d"))->where('akhir_periode' , '>=', date("Y-m-d"));
    }
    
    public function scopeKategoriPelanggan($query, $kategori_pelanggan_id)
    {
        return $query->where('kategori_pelanggan_id', $kategori_pelanggan_id);
    }

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'produk_id');
    }

    public function kategori_pelanggan()
    {
        return $this->belongsTo(KategoriPelanggan::class, 'kategori_pelanggan_id');
    }
}
