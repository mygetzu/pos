<?php

namespace App\Models\AdminEcommerce;

use Illuminate\Database\Eloquent\Model;

class Konten extends Model
{
    protected $table = 'tref_konten';
    protected $fillable = [
        'id',
        'nama',
        'keterangan',
    ];
}
