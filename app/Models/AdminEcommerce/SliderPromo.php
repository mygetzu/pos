<?php

namespace App\Models\AdminEcommerce;

use Illuminate\Database\Eloquent\Model;

class SliderPromo extends Model
{
    protected $table = 'tran_slider_promo';
    protected $fillable = [
        'id',
        'nama',
        'url',
        'file_gambar',
        'list_produk',
        'item_order',
    ];
}
