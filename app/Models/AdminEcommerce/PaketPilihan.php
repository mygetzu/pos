<?php

namespace App\Models\AdminEcommerce;

use Illuminate\Database\Eloquent\Model;

class PaketPilihan extends Model
{
    protected $table = 'tran_paket_pilihan';
    protected $fillable = [
        'id',
        'nama',
        'url',
        'file_gambar',
        'list_produk',
        'item_order',
    ];
}
