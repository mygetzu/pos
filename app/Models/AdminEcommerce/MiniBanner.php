<?php

namespace App\Models\AdminEcommerce;

use Illuminate\Database\Eloquent\Model;

class MiniBanner extends Model
{
    protected $table = 'tran_mini_banner';
    protected $fillable = [
        'id',
        'nama',
        'url',
        'file_gambar',
        'list_produk',
        'item_order',
    ];
}
