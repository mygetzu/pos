<?php

namespace App\Models\AdminEcommerce;

use Illuminate\Database\Eloquent\Model;
use App\Models\Produk\Produk;

class ProdukAkanDatang extends Model
{
    protected $table = 'tran_produk_akan_datang';
    protected $fillable = [
        'produk_id',
        'tanggal_input',
    ];

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'produk_id');
    }
}
