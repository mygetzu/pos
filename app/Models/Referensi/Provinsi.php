<?php

namespace App\Models\Referensi;

use Illuminate\Database\Eloquent\Model;
use App\Models\Referensi\Kota;

class Provinsi extends Model
{
    protected $table = 'tref_provinsi';
    protected $fillable = [
        'kode',
        'nama',
    ];

    public function kota()
    {
        return $this->hasMany(Kota::class, 'provinsi_kode', 'kode');
    }
}
