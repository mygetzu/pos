<?php

namespace App\Models\Referensi;

use Illuminate\Database\Eloquent\Model;
use App\Models\Referensi\Provinsi;
use App\Models\Pelanggan\Pelanggan;
use App\Models\Perusahaan;
use App\Models\Cabang;
use App\Models\Supplier\Supplier;
use App\Models\TransaksiPembelian\POAlamatPengiriman;
use App\Models\TransaksiPenjualan\SOAlamatPengiriman;

class Kota extends Model
{
    protected $table = 'tref_kota';
    protected $fillable = [
        'kode',
        'provinsi_kode',
        'nama',
    ];

    public function provinsi()
    {
        return $this->belongsTo(Provinsi::class, 'provinsi_kode', 'kode');
    }

    public function pelanggan()
    {
        return $this->hasMany(Pelanggan::class, 'kota_id');
    }

    public function Perusahaan()
    {
        return $this->hasOne(Perusahaan::class, 'kota_id');
    }

    public function cabang()
    {
        return $this->hasMany(Cabang::class, 'kota_id');
    }

    public function supplier()
    {
        return $this->hasMany(Supplier::class, 'kota_id');
    }

    public function po_alamat_pengiriman()
    {
        return $this->hasMany(POAlamatPengiriman::class, 'kota_id');
    }

    public function so_alamat_pengiriman()
    {
        return $this->hasMany(SOAlamatPengiriman::class, 'kota_id');
    }
}
