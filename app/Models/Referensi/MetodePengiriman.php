<?php

namespace App\Models\Referensi;

use Illuminate\Database\Eloquent\Model;
use App\Models\TransaksiPembelian\POAlamatPengiriman;
use App\Models\TransaksiPenjualan\SOAlamatPengiriman;

class MetodePengiriman extends Model
{
    protected $table = 'tref_metode_pengiriman';
    protected $fillable = [
        'nama',
        'tarif_dasar',
        'keterangan',
        'is_aktif',
        'file_gambar',
    ];

    public function po_alamat_pengiriman()
    {
        return $this->hasMany(POAlamatPengiriman::class, 'metode_pengiriman_id');
    }

    public function so_alamat_pengiriman()
    {
        return $this->hasMany(SOAlamatPengiriman::class, 'metode_pengiriman_id');
    }
}
