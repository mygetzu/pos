<?php

namespace App\Models\Referensi;

use Illuminate\Database\Eloquent\Model;
use App\Models\TransaksiPembelian\BayarInvoicePembelian;
use App\Models\TransaksiPenjualan\BayarInvoicePenjualan;

class Bank extends Model
{
    protected $table = 'tref_bank';
    protected $fillable = [
        'kode',
        'nama',
        'nama_singkat',
        'keterangan',
        'is_aktif',
        'file_gambar',
    ];

    public function bayar_invoice_pembelian()
    {
        return $this->hasMany(BayarInvoicePembelian::class, 'bank_id');
    }

    public function bayar_invoice_penjualan()
    {
        return $this->hasMany(BayarInvoicePenjualan::class, 'bank_id');
    }
}
