<?php

namespace App\Models\Referensi;

use Illuminate\Database\Eloquent\Model;
use App\Models\TransaksiPembelian\BayarPembelianDetail;
use App\Models\TransaksiPenjualan\BayarPenjualanDetail;

class MetodePembayaran extends Model
{
    protected $table = 'tref_metode_pembayaran';
    protected $fillable = [
        'nama',
        'keterangan',
        'is_aktif',
        'file_gambar',
        'jenis',
    ];

    public function bayar_pembelian_detail()
    {
        return $this->hasMany(BayarPembelianDetail::class, 'metode_pembayaran_id');
    }

    public function bayar_penjualan_detail()
    {
        return $this->hasMany(BayarPenjualanDetail::class, 'metode_pembayaran_id');
    }
}
