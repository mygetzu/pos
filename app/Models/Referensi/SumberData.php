<?php

namespace App\Models\Referensi;

use Illuminate\Database\Eloquent\Model;
use App\Models\Laporan\KartuStokDetail;

class SumberData extends Model
{
    protected $table = 'tref_sumber_data';
    protected $fillable = [
        'id',
        'nama',
    ];

    public function kartu_stok_detail()
    {
        return $this->hasMany(KartuStokDetail::class, 'sumber_data_id');
    }
}
