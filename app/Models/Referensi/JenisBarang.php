<?php

namespace App\Models\Referensi;

use App\Models\Pelanggan\PelangganCart;
use Illuminate\Database\Eloquent\Model;
use App\Models\Produk\Produk;
use App\Models\Produk\Hadiah;
use App\Models\PaketProduk\Paket;

class JenisBarang extends Model
{
    protected $table = 'tref_jenis_barang';
    protected $fillable = [
        'id',
        'nama',
        'keterangan',
    ];

    public function produk()
    {
        return $this->hasMany(Produk::class, 'jenis_barang_id');
    }

    public function hadiah()
    {
        return $this->hasMany(Hadiah::class, 'jenis_barang_id');
    }

    public function paket()
    {
        return $this->hasMany(Paket::class, 'jenis_barang_id');
    }

    public function pelanggan_cart() {
        return $this->hasMany(PelangganCart::class, 'jenis_barang_id');
    }
}
