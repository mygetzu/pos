<?php

namespace App\Models\TransaksiService;

use Illuminate\Database\Eloquent\Model;
use App\Models\Transaksi\ServiceOrder;
use App\Models\Pelanggan\Pelanggan;

class InvoiceHeader extends Model {

    protected $table = 'tran_service_invoice_header';
    protected $fillable = [
        'service_order_id',
        'teknisi_id',
        'pelanggan_id',
        'harga_total',
        'catatan',
        'is_submit',
        'is_repair',
        'is_lunas'
    ];

    public function pelanggan()
    {
        return $this->belongsTo(Pelanggan::class, 'pelanggan_id');
    }

    public function invoice_detail() {
        return $this->hasMany(ServiceOrder::class, 'service_invoice_header_id');
    }

}
