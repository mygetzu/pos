<?php

namespace App\Models\TransaksiService;

use Illuminate\Database\Eloquent\Model;
use App\Models\TransaksiService\InvoiceHeader;

class InvoiceDetail extends Model {

    protected $table = 'tran_service_invoice_detail';
    protected $fillable = [
        'service_invoice_header_id',
        'tindakan',
        'biaya',
        'is_repair'
    ];

    public function service_invoice_header() {
        return $this->belongsTo(InvoiceHeader::class, 'service_invoice_header_id');
    }

}
