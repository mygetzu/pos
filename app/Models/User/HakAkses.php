<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class HakAkses extends Model
{
    protected $table = 'hak_akses';
    protected $fillable = [
        'nama',
        'deskripsi',
        'menu_akses',
    ];
}
