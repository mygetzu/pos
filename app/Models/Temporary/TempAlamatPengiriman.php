<?php

namespace App\Models\Temporary;

use Illuminate\Database\Eloquent\Model;

class TempAlamatPengiriman extends Model
{
    protected $table = 'temp_alamat_pengiriman';
    protected $fillable = [
        'no_nota',
        'tanggal',
        'nama',
        'alamat',
        'kota_id',
        'kode_pos',
        'hp',
        'metode_pengiriman_id',
    ];
}
