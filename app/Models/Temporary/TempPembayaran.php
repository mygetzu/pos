<?php

namespace App\Models\Temporary;

use Illuminate\Database\Eloquent\Model;

class TempPembayaran extends Model
{
    protected $table = 'temp_pembayaran';
    protected $fillable = [
        'no_nota',
        'tanggal',
        'metode_pembayaran_id',
        'bayar_id',
        'nomor_pembayaran',
        'bank_id',
    ];
}
