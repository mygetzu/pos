<?php

namespace App\Models\Temporary;

use Illuminate\Database\Eloquent\Model;

class TempSOVoucher extends Model
{
    protected $table = 'temp_so_voucher';
    protected $fillable = [
        'no_nota',
        'voucher_id',
    ];
}
