<?php

namespace App\Models\TransaksiPembelian;

use Illuminate\Database\Eloquent\Model;
use App\Models\Supplier\Supplier;

class RBHeader extends Model
{
    protected $table = 'tran_rb_header';
    protected $fillable = [
        'id',
        'nota_beli_id',
        'no_nota',
        'supplier_id',
        'tanggal',
        'potongan_retur',
        'total_retur',
        'catatan',
        'no_jurnal',
        'is_lunas',
    ];

    public function nota_beli()
    {
        return $this->belongsTo(NotaBeli::class, 'nota_beli_id');
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'supplier_id');
    }

    public function rb_detail()
    {
        return $this->hasMany(RBDetail::class, 'rb_header_id');
    }

    public function bayar_pembelian()
    {
        return $this->hasOne(BayarPembelian::class, 'nota_id');
    }
}
