<?php

namespace App\Models\TransaksiPembelian;

use Illuminate\Database\Eloquent\Model;
use App\Models\Produk\Produk;
use App\Models\Produk\Hadiah;
use App\Models\PaketProduk\Paket;
use App\Models\Gudang\Gudang;

class RBDetail extends Model
{
    protected $table = 'tran_rb_detail';
    protected $fillable = [
        'id',
        'rb_header_id',
        'produk_id',
        'jenis_barang_id',
        'gudang_id',
        'serial_number',
        'jumlah',
        'deskripsi',
        'harga',
    ];

    public function rb_header()
    {
        return $this->belongsTo(RBHeader::class, 'rb_header_id');
    }

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'produk_id');
    }

    public function hadiah()
    {
        return $this->belongsTo(Hadiah::class, 'produk_id');
    }

    public function paket()
    {
        return $this->belongsTo(Paket::class, 'produk_id');
    }

    public function gudang()
    {
        return $this->belongsTo(Gudang::class, 'gudang_id');
    }
}
