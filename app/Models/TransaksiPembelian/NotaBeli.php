<?php

namespace App\Models\TransaksiPembelian;

use Illuminate\Database\Eloquent\Model;
use App\Models\Supplier\Supplier;

class NotaBeli extends Model
{
    protected $table = 'tran_nota_beli';
    protected $fillable = [
        'id',
        'sj_masuk_header_id',
        'no_nota',
        'supplier_id',
        'tanggal',
        'no_jurnal',
        'ppn',
        'is_lunas',
        'total_harga',
        'total_diskon',
        'total_tagihan',
    ];

    public function sj_masuk_header()
    {
        return $this->belongsTo(SJMasukHeader::class, 'sj_masuk_header_id');
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'supplier_id');
    }

    public function rb_header()
    {
        return $this->hasMany(RBHeader::class, 'nota_beli_id');
    }
    
    public function bayar_pembelian_header()
    {
        return $this->hasOne(BayarPembelianHeader::class, 'nota_id');
    }
}
