<?php

namespace App\Models\TransaksiPembelian;

use Illuminate\Database\Eloquent\Model;
use App\Models\Referensi\Kota;
use App\Models\Referensi\MetodePengiriman;

class POAlamatPengiriman extends Model
{
    protected $table = 'tran_po_alamat_pengiriman';
    protected $fillable = [
        'id',
        'po_header_id',
        'nama',
        'email',
        'alamat',
        'kota_id',
        'kode_pos',
        'hp',
        'metode_pengiriman_id',
    ];

    public function po_header()
    {
        return $this->belongsTo(POHeader::class, 'po_header_id');
    }

    public function kota()
    {
        return $this->belongsTo(Kota::class, 'kota_id');
    }

    public function metode_pengiriman()
    {
        return $this->belongsTo(MetodePengiriman::class, 'metode_pengiriman_id');
    }
}
