<?php

namespace App\Models\TransaksiPembelian;

use Illuminate\Database\Eloquent\Model;
use App\Models\Referensi\MetodePembayaran;

class BayarPembelianDetail extends Model
{
    protected $table = 'tran_bayar_pembelian_detail';
    protected $fillable = [
        'id',
        'bayar_pembelian_header_id',
        'metode_pembayaran_id',
        'nomor_pembayaran',
        'nominal',
        'bank',
    ];

    public function bayar_pembelian_header()
    {
        return $this->belongsTo(BayarPembelianHeader::class, 'bayar_pembelian_header_id');
    }

    public function metode_pembayaran()
    {
        return $this->belongsTo(MetodePembayaran::class, 'metode_pembayaran_id');
    }
}
