<?php

namespace App\Models\TransaksiPembelian;

use Illuminate\Database\Eloquent\Model;
use App\Models\Supplier\Supplier;

class BayarPembelianHeader extends Model
{
    protected $table = 'tran_bayar_pembelian_header';
    protected $fillable = [
        'id',
        'nota_id',
        'jenis',
        'supplier_id',
        'tanggal',
        'total_tagihan',
        'total_pembayaran',
        'keterangan',
        'no_jurnal',
    ];

    public function bayar_pembelian_detail()
    {
        return $this->hasMany(BayarPembelianDetail::class, 'bayar_pembelian_header_id');
    }

    public function nota_beli()
    {
        return $this->belongsTo(NotaBeli::class, 'nota_id');
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'supplier_id');
    }

    public function rb_header()
    {
        return $this->belongsTo(RBHeader::class, 'nota_id');
    }
}
