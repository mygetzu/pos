<?php

namespace App\Models\TransaksiPembelian;

use Illuminate\Database\Eloquent\Model;

class PODiskon extends Model
{
    protected $table = 'tran_po_diskon';
    protected $fillable = [
        'id',
        'po_header_id',
        'nominal',
    ];

    public function po_header()
    {
        return $this->belongsTo(POHeader::class, 'po_header_id');
    }
}
