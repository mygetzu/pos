<?php

namespace App\Models\TransaksiPembelian;

use Illuminate\Database\Eloquent\Model;
use App\Models\Supplier\Supplier;

class POHeader extends Model
{
    protected $table = 'tran_po_header';
    protected $fillable = [
        'id',
        'no_purchase_order',
        'supplier_id',
        'tanggal',
        'due_date',
        'catatan',
        'total_tagihan',
        'is_sj_lengkap',
        'ppn',
    ];

    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'supplier_id');
    }

    public function po_detail()
    {
        return $this->hasMany(PODetail::class, 'po_header_id');
    }

    public function po_alamat_pengiriman()
    {
        return $this->hasOne(POAlamatPengiriman::class, 'po_header_id');
    }

    public function po_diskon()
    {
        return $this->hasMany(PODiskon::class, 'po_header_id');
    }

    public function sj_masuk_header()
    {
        return $this->hasMany(SJMasukHeader::class, 'po_header_id');
    }
}
