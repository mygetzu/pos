<?php

namespace App\Models\TransaksiPembelian;

use Illuminate\Database\Eloquent\Model;
use App\Models\Supplier\Supplier;

class SJMasukHeader extends Model
{
    protected $table = 'tran_sj_masuk_header';
    protected $fillable = [
        'id',
        'po_header_id',
        'no_surat_jalan',
        'supplier_id',
        'tanggal',
        'ppn',
        'total_harga',
        'total_diskon',
        'total_tagihan',
    ];

    public function po_header()
    {
        return $this->belongsTo(POHeader::class, 'po_header_id');
    }

    public function sj_masuk_detail()
    {
        return $this->hasMany(SJMasukDetail::class, 'sj_masuk_header_id');
    }

    public function nota_beli()
    {
        return $this->hasOne(NotaBeli::class, 'sj_masuk_header_id');
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'supplier_id');
    }
}
