<?php

namespace App\Models\Supplier;

use Illuminate\Database\Eloquent\Model;

class RekeningSupplier extends Model
{
    protected $table = 'tran_rekening_supplier';
    protected $fillable = [
        'id',
        'nama_bank',
        'nomor_rekening',
        'atas_nama',
        'nama_uang',
        'supplier_id',
    ];

    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'supplier_id');
    }
}
