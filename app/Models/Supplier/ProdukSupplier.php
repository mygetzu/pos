<?php

namespace App\Models\Supplier;

use Illuminate\Database\Eloquent\Model;
use App\Models\Produk\Produk;

class ProdukSupplier extends Model
{
    protected $table = 'tran_produk_supplier';
    protected $fillable = [
        'id',
        'produk_id',
        'supplier_id',
        'harga_terakhir',
        'tanggal_terakhir',
    ];

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'produk_id');
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'supplier_id');
    }
}
