<?php

namespace App\Models\Supplier;

use Illuminate\Database\Eloquent\Model;
use App\Models\Referensi\Kota;
use App\Models\TransaksiPembelian\POHeader;
use App\Models\TransaksiPembelian\SJMasukHeader;
use App\Models\TransaksiPembelian\NotaBeli;
use App\Models\TransaksiPembelian\RBHeader;
use App\Models\TransaksiPembelian\BayarPembelianHeader;

class Supplier extends Model
{
    protected $table = 'tmst_supplier';
    protected $fillable = [
        'nama',
        'alamat',
        'kota_id',
        'telp1',
        'telp2',
        'nama_sales',
        'hp_sales',
        'deskripsi',
        'umur_hutang',
        'is_aktif',
    ];

    public function kota()
    {
        return $this->belongsTo(Kota::class, 'kota_id');
    }

    public function rekening_supplier()
    {
        return $this->hasMany(RekeningSupplier::class, 'supplier_id');
    }

    public function invoice_pembelian()
    {
        return $this->hasMany(InvoicePembelian::class, 'supplier_id');
    }

    public function po_header()
    {
        return $this->hasMany(POHeader::class, 'supplier_id');
    }

    public function sj_masuk_header()
    {
        return $this->hasMany(SJMasukHeader::class, 'supplier_id');
    }

    public function nota_beli()
    {
        return $this->hasMany(NotaBeli::class, 'supplier_id');
    }

    public function rb_header()
    {
        return $this->hasMany(RBHeader::class, 'supplier_id');
    }

    public function bayar_pembelian_header()
    {
        return $this->hasMany(BayarPembelianHeader::class, 'supplier_id');
    }

    public function produk_supplier()
    {
        return $this->hasMany(ProdukSupplier::class, 'supplier_id');
    }
}
