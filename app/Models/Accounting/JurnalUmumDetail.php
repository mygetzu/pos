<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Model;
use App\Models\Pengaturan\MasterCOA;

class JurnalUmumDetail extends Model
{
    protected $table = 'tran_jurnal_umum_detail';
    protected $fillable = [
        'id',
        'jurnal_umum_header_id',
        'no_jurnal',
        'tanggal',
        'akun_kode',
        'akun_nama',
        'referensi',
        'debit',
        'kredit',
    ];

    public function jurnal_umum_header()
    {
        return $this->belongsTo(JurnalUmumHeader::class, 'jurnal_umum_header_id');
    }

    public function master_coa()
    {
        return $this->belongsTo(MasterCOA::class, 'akun_kode', 'kode');
    }
}
