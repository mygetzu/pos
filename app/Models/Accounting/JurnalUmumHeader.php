<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Model;
use App\Models\Pengaturan\MasterCOA;

class JurnalUmumHeader extends Model
{
    protected $table = 'tran_jurnal_umum_header';
    protected $fillable = [
        'id',
        'tanggal',
        'is_editable',
    ];

    public function jurnal_umum_detail()
    {
        return $this->hasMany(JurnalUmumDetail::class, 'jurnal_umum_header_id');
    }
}
