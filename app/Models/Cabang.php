<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Referensi\Kota;

class Cabang extends Model
{
    protected $table = 'tmst_cabang_perusahaan';
    protected $fillable = [
        'nama',
        'alamat',
        'kota_id',
        'telp',
        'kontak_person',
        'perusahaan_id',
    ];

    public function kota()
    {
        return $this->belongsTo(Kota::class, 'kota_id');
    }

    public function perusahaan()
    {
        return $this->belongsTo(Perusahaan::class, 'perusahaan_id');
    }
}
