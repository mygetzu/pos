<?php

namespace App\Models\Master;

use App\Models\Transaksi\ProdukHarga;
use Illuminate\Database\Eloquent\Model;

class KategoriPelanggan extends Model
{
    protected $table    = 'tmst_kategori_pelanggan';
    protected $fillable = ['nama', 'deskripsi', 'default_diskon'];

    public function harga_produk() {
        return $this->hasMany(ProdukHarga::class, 'kategori_pelanggan_id');
    }
}
