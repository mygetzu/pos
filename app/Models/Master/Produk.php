<?php

namespace App\Models\Master;

use App\Models\Transaksi\Cashback;
use App\Models\Transaksi\Diskon;
use App\Models\Transaksi\Hadiah;
use App\Models\Transaksi\ProdukGaleri;
use App\Models\Transaksi\ProdukHarga;
use App\Produk\KategoriProduk;
use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    protected $table = 'tmst_produk';
    protected $fillable = ['kode', 'nama', 'slug_nama', 'satuan', 'berat', 'deskripsi',
                            'harga_retail', 'stok', 'stok_dipesan', 'is_aktif', 'kategori_id'];

    public function kategori() {
        return $this->belongsTo(KategoriProduk::class, 'kategori_id');
    }

    public function cashback() {
        return $this->hasMany(Cashback::class, 'produk_id');
    }

    public function hadiah() {
        return $this->hasMany(Hadiah::class, 'produk_id');
    }

    public function galeri() {
        return $this->hasMany(ProdukGaleri::class, 'produk_id');
    }

    public function diskon() {
        return $this->hasMany(Diskon::class, 'produk_id');
    }

    public function harga_produk() {
        return $this->hasMany(ProdukHarga::class, 'produk_id');
    }

}