<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Referensi\Kota;

class Perusahaan extends Model
{
    protected $table = 'tmst_perusahaan';
    protected $fillable = [
        'nama',
        'alamat',
        'kota_id',
        'telp',
        'kontak_person',
        'bidang_usaha',
        'jam_operasional',
        'email',
        'expired',
        'pos_id',
    ];

    public function kota()
    {
        return $this->belongsTo(Kota::class, 'kota_id');
    }

    public function cabang()
    {
        return $this->hasMany(Cabang::class, 'perusahaan_id');
    }
}
