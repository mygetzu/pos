<?php

namespace App\Models\Produk;

use Illuminate\Database\Eloquent\Model;

class NilaiSpesifikasi extends Model
{
    protected $table = 'tran_nilai_spesifikasi';
    protected $fillable = [
        'id',
        'produk_id',
        'parameter_spesifikasi_id',
        'nilai',
        'satuan',
    ];

    public function parameter_spesifikasi()
    {
        return $this->belongsTo(ParameterSpesifikasi::class, 'parameter_spesifikasi_id');
    }

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'parameter_spesifikasi_id');
    }
}
