<?php

namespace App\Models\Produk;

use Illuminate\Database\Eloquent\Model;
use App\Models\Pelanggan\KategoriPelanggan;
use App\Models\Pelanggan\ProdukHarga;
use App\Models\PaketProduk\PaketProduk;
use App\Models\Gudang\ProdukGudang;
use App\Models\Stok\MutasiStokDetail;
use App\Models\Laporan\LaporanStokDetail;
use App\Models\Laporan\KartuStokHeader;
use App\Models\TransaksiPembelian\PODetail;
use App\Models\TransaksiPembelian\SJMasukDetail;
use App\Models\TransaksiPembelian\RBDetail;
use App\Models\TransaksiPenjualan\SODetail;
use App\Models\TransaksiPenjualan\SJKeluarDetail;
use App\Models\TransaksiPenjualan\RJDetail;
use App\Models\Supplier\ProdukSupplier;
use App\Models\AdminEcommerce\ProdukBaru;
use App\Models\AdminEcommerce\ProdukAkanDatang;
use App\Models\Pelanggan\PelangganCart;
use App\Models\Referensi\JenisBarang;
use App\Models\Stok\PenyesuaianStokDetail;

class Produk extends Model
{
    protected $table = 'tmst_produk';
    protected $fillable = [
        'kode',
        'nama',
        'slug_nama',
        'satuan',
        'berat',
        'deskripsi',
        'harga_retail',
        'hpp',
        'stok',
        'stok_dipesan',
        'is_aktif',
        'kategori_id',
        'jenis_barang_id',
    ];

    public function kategori_produk()
    {
        return $this->belongsTo(KategoriProduk::class, 'kategori_id');
    }

    public function produk_galeri()
    {
        return $this->hasMany(ProdukGaleri::class, 'produk_id');
    }

    public function produk_galeri_first()
    {
        return $this->hasOne(ProdukGaleri::class, 'produk_id');
    }

    public function promo_diskon()
    {
        return $this->hasOne(PromoDiskon::class, 'produk_id')->periode();
    }

    public function promo_cashback()
    {
        return $this->hasOne(PromoCashback::class, 'produk_id')->periode();
    }

    public function promo_hadiah()
    {
        return $this->hasOne(PromoHadiah::class, 'produk_id')->periode();
    }

    public function produk_harga()
    {
        return $this->hasOne(ProdukHarga::class, 'produk_id')->periode();
    }

    public function paket_produk()
    {
        return $this->hasMany(PaketProduk::class, 'produk_id');
    }

    public function produk_gudang()
    {
        return $this->hasMany(ProdukGudang::class, 'produk_id');
    }

    public function produk_serial_number()
    {
        return $this->hasMany(ProdukSerialNumber::class, 'produk_id');
    }

    public function mutasi_stok_detail()
    {
        return $this->hasMany(MutasiStokDetail::class, 'produk_id');
    }

    public function laporan_stok_detail()
    {
        return $this->hasMany(LaporanStokDetail::class, 'produk_id');
    }

    public function kartu_stok_header()
    {
        return $this->hasMany(KartuStokHeader::class, 'produk_id');
    }

    public function po_detail()
    {
        return $this->hasMany(PODetail::class, 'produk_id');
    }

    public function sj_masuk_detail()
    {
        return $this->hasMany(SJMasukDetail::class, 'produk_id');
    }

    public function rb_detail()
    {
        return $this->hasMany(RBDetail::class, 'produk_id');
    }

    public function so_detail()
    {
        return $this->hasMany(SODetail::class, 'produk_id');
    }

    public function sj_keluar_detail()
    {
        return $this->hasMany(SJKeluarDetail::class, 'produk_id');
    }

    public function rj_detail()
    {
        return $this->hasMany(RJDetail::class, 'produk_id');
    }

    public function produk_supplier()
    {
        return $this->hasMany(ProdukSupplier::class, 'produk_id');
    }

    public function nilai_spesifikasi()
    {
        return $this->hasMany(NilaiSpesifikasi::class, 'produk_id');
    }

    public function produk_baru()
    {
        return $this->hasOne(ProdukBaru::class, 'produk_id');
    }

    public function produk_akan_datang()
    {
        return $this->hasOne(ProdukAkanDatang::class, 'produk_id');
    }

    public function pelanggan_cart()
    {
        return $this->hasMany(PelangganCart::class, 'produk_id');
    }

    public function jenis_barang()
    {
        return $this->belongsTo(JenisBarang::class, 'jenis_barang_id');
    }

    public function penyesuaian_stok_detail()
    {
        return $this->hasMany(PenyesuaianStokDetail::class, 'produk_id');
    }
}
