<?php

namespace App\Models\Produk;

use Illuminate\Database\Eloquent\Model;
use App\Models\Produk\PromoHadiah;
use App\Models\Gudang\Gudang;
use App\Models\TransaksiPembelian\PODetail;
use App\Models\TransaksiPembelian\SJMasukDetail;
use App\Models\TransaksiPembelian\RBDetail;
use App\Models\TransaksiPenjualan\SODetail;
use App\Models\TransaksiPenjualan\SJKeluarDetail;
use App\Models\TransaksiPenjualan\RJDetail;
use App\Models\Pelanggan\PelangganCart;
use App\Models\Referensi\JenisBarang;

class Hadiah extends Model
{
    protected $table = 'tmst_hadiah';
    protected $fillable = [
        'kode',
        'nama',
        'satuan',
        'berat',
        'deskripsi',
        'stok',
        'stok_dipesan',
        'file_gambar',
        'is_aktif',
        'jenis_barang_id',
    ];

    public function promo_hadiah()
    {
        return $this->hasMany(PromoHadiah::class, 'hadiah_id')->periode();
    }

    public function hadiah_gudang()
    {
        return $this->hasMany(Gudang::class, 'hadiah_id');
    }

    public function po_detail()
    {
        return $this->hasMany(PODetail::class, 'produk_id');
    }

    public function sj_masuk_detail()
    {
        return $this->hasMany(SJMasukDetail::class, 'produk_id');
    }

    public function rb_detail()
    {
        return $this->hasMany(RBDetail::class, 'produk_id');
    }

    public function so_detail()
    {
        return $this->hasMany(SODetail::class, 'produk_id');
    }

    public function sj_keluar_detail()
    {
        return $this->hasMany(SJKeluarDetail::class, 'produk_id');
    }

    public function rj_detail()
    {
        return $this->hasMany(RJDetail::class, 'produk_id');
    }

    public function pelanggan_cart()
    {
        return $this->hasMany(PelangganCart::class, 'produk_id');
    }

    public function jenis_barang()
    {
        return $this->belongsTo(JenisBarang::class, 'jenis_barang_id');
    }
}
