<?php

namespace App\Models\Produk;

use Illuminate\Database\Eloquent\Model;

class ParameterSpesifikasi extends Model
{
    protected $table = 'tran_parameter_spesifikasi';
    protected $fillable = [
        'id',
        'kategori_produk_id',
        'nama',
    ];

    public function kategori_produk()
    {
        return $this->belongsTo(KategoriProduk::class, 'kategori_produk_id');
    }

    public function nilai_spesifikasi()
    {
        return $this->hasOne(NilaiSpesifikasi::class, 'parameter_spesifikasi_id');
    }
}
