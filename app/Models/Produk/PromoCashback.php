<?php

namespace App\Models\Produk;

use Illuminate\Database\Eloquent\Model;
use App\Models\Produk\Produk;

class PromoCashback extends Model
{
    protected $table = 'tran_promo_cashback';
    protected $fillable = [
        'produk_id',
        'kode_promo',
        'qty_beli',
        'cashback',
        'awal_periode',
        'akhir_periode',
    ];

    public function scopePeriode($query)
    {
        return $query->where('awal_periode' , '<=', date("Y-m-d"))->where('akhir_periode' , '>=', date("Y-m-d"));
    }

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'produk_id');
    }
}
