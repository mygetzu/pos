<?php

namespace App\Models\Produk;

use Illuminate\Database\Eloquent\Model;
use App\Models\Produk\Produk;

class KategoriProduk extends Model
{
    protected $table = 'tmst_kategori_produk';
    protected $fillable = [
        'nama',
        'slug_nama',
        'deskripsi',
        'kolom_spesifikasi',
        'is_aktif',
        'file_gambar',
    ];

    public function produk()
    {
        return $this->hasMany(Produk::class, 'kategori_id');
    }

    public function parameter_spesifikasi()
    {
        return $this->hasMany(ParameterSpesifikasi::class, 'kategori_produk_id');
    }
}
