<?php

namespace App\Models\Produk;

use Illuminate\Database\Eloquent\Model;

class ProdukPilihan extends Model
{
    protected $table = 'tran_produk_tampil_di_beranda';
    protected $fillable = [
        'id',
        'produk_id',
        'tanggal_input',
        'created_at',
        'updated_at'
    ];

    public function produk() {
        return $this->belongsTo(Produk::class, 'produk_id');
    }
}
