<?php

namespace App\Models\Produk;

use Illuminate\Database\Eloquent\Model;
use App\Models\Produk\Produk;
use App\Models\Produk\Hadiah;

class PromoHadiah extends Model
{
    protected $table = 'tran_promo_hadiah';
    protected $fillable = [
        'produk_id',
        'hadiah_id',
        'kode_promo',
        'qty_beli',
        'qty_hadiah',
        'awal_periode',
        'akhir_periode',
    ];

    public function scopePeriode($query)
    {
        return $query->where('awal_periode' , '<=', date("Y-m-d"))->where('akhir_periode' , '>=', date("Y-m-d"));
    }

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'produk_id');
    }

    public function hadiah()
    {
        return $this->belongsTo(Hadiah::class, 'hadiah_id');
    }
}
