<?php

namespace App\Models\Produk;

use Illuminate\Database\Eloquent\Model;
use App\Models\Produk\Produk;

class ProdukGaleri extends Model
{
    protected $table = 'tran_produk_galeri';
    protected $fillable = [
        'produk_id',
        'file_gambar',
    ];

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'produk_id');
    }
}
