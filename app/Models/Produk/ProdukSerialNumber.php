<?php

namespace App\Models\Produk;

use Illuminate\Database\Eloquent\Model;
use App\Models\Gudang\Gudang;

class ProdukSerialNumber extends Model
{
    protected $table = 'tran_produk_serial_number';
    protected $fillable = [
        'produk_id',
        'gudang_id',
        'serial_number',
        'stok',
        'keterangan',
    ];

    public function gudang()
    {
        return $this->belongsTo(Gudang::class, 'gudang_id');
    }

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'produk_id');
    }
}
