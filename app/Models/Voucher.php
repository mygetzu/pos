<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    protected $table = 'tmst_voucher';
    protected $fillable = [
        'kode',
        'nominal',
        'awal_periode',
        'akhir_periode',
        'tanggal_dipakai',
        'is_sekali_pakai',
        'pelanggan_id_target',
        'pelanggan_id_pakai',
    ];
}
